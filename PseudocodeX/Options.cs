﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace PseudocodeX
{
    public partial class Options : Form
    {
        string DefaultSearchBoxText;
        string DefaultText;
        public Options()
        {
            InitializeComponent();
            DefaultSearchBoxText = SearchBox.Text;
            DefaultText = this.Text;
            foreach (Controls.Development.ImageListBoxItem lb in listBox1.Items)
            {
                lista.Add(lb);
            }
        }

        private void Options_Load(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = 0;
        }

        private void checkBox18_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void treeView1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

        }
        Control ctrl;
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText)
            {
                if (listBox1.SelectedIndex == 0)
                {
                    listBox1.SelectedIndex = 1;
                    Application0Startup.BringToFront();
                }
                if (listBox1.SelectedIndex == 1) Application0Startup.BringToFront();
                if (listBox1.SelectedIndex == 2) Application0Interface.BringToFront();
                if (listBox1.SelectedIndex == 3) Application0Sync.BringToFront();
                if (listBox1.SelectedIndex == 4) Application0FileAssociations.BringToFront();
                if (listBox1.SelectedIndex == 5) Application0ExtensionManager.BringToFront();
                if (listBox1.SelectedIndex == 6) Application0Codeboard.BringToFront();
                if (listBox1.SelectedIndex == 7) Application0ImportExportSettings.BringToFront();
                if (listBox1.SelectedIndex == 8)
                {
                    listBox1.SelectedIndex = 9;
                    Editor0AutoSave.BringToFront();
                }
                if (listBox1.SelectedIndex == 9) Editor0AutoSave.BringToFront();
                if (listBox1.SelectedIndex == 10) Editor0VisualExperience.BringToFront();
                if (listBox1.SelectedIndex == 11) Editor0Findreplace.BringToFront();
                if (listBox1.SelectedIndex == 12)
                {
                    listBox1.SelectedIndex = 13;
                    CodeConversion0Headers.BringToFront();
                }
                if (listBox1.SelectedIndex == 13) CodeConversion0Headers.BringToFront();
                if (listBox1.SelectedIndex == 14) CodeConversion0LogicSchema.BringToFront();
                if (listBox1.SelectedIndex == 15) Debugging.BringToFront();
                if (listBox1.SelectedIndex == 16) ErrorReporting.BringToFront();
                if (listBox1.SelectedIndex == 17) AutomaticUpdates.BringToFront();
                if (listBox1.SelectedIndex == 18) ResetSettings.BringToFront();
            }
            else
            {
                try
                {
                    if (ctrl != null) ctrl.Font = new Font(ctrl.Font, FontStyle.Regular);
                    if (controale[listBox1.SelectedIndex].Parent.GetType() == typeof(GroupBox))
                    {
                        controale[listBox1.SelectedIndex].Parent.Parent.BringToFront();
                        /*controale[listBox1.SelectedIndex].Visible = false;
                        Application.DoEvents();
                        Thread.Sleep(200);
                        controale[listBox1.SelectedIndex].Visible = true;
                        Application.DoEvents();
                        Thread.Sleep(100);
                        controale[listBox1.SelectedIndex].Visible = false;
                        Application.DoEvents();
                        Thread.Sleep(200);
                        controale[listBox1.SelectedIndex].Visible = true;*/
                        controale[listBox1.SelectedIndex].Font = new Font(controale[listBox1.SelectedIndex].Font, FontStyle.Underline);
                    }
                    else
                    {
                        controale[listBox1.SelectedIndex].Parent.BringToFront();
                        /*controale[listBox1.SelectedIndex].Visible = false;
                        Application.DoEvents();
                        Thread.Sleep(200);
                        controale[listBox1.SelectedIndex].Visible = true;
                        Application.DoEvents();
                        Thread.Sleep(100);
                        controale[listBox1.SelectedIndex].Visible = false;
                        Application.DoEvents();
                        Thread.Sleep(200);
                        controale[listBox1.SelectedIndex].Visible = true;*/
                        controale[listBox1.SelectedIndex].Font = new Font(controale[listBox1.SelectedIndex].Font, FontStyle.Underline);
                    }
                    ctrl = controale[listBox1.SelectedIndex];
                }
                catch { }
            }

        }
        Controls.Development.ImageListBox.ImageListBoxItemCollection lista = new Controls.Development.ImageListBox.ImageListBoxItemCollection(new Controls.Development.ImageListBox());
        List<Control> controale = new List<Control>();
        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                try
                {
                    listBox1.SelectedIndex = 0;
                }
                catch { }
                listBox1.Focus();
                e.SuppressKeyPress = true;
                return;
            }
            listBox1.Tag = SearchBox.Text;
            if (SearchBox.Text != "" && SearchBox.Text != DefaultSearchBoxText)
            {
                listBox1.HorizontalScrollbar = true;
                listBox1.Items.Clear();
                controale.Clear();
                int i = 0;
                foreach (Control ct in this.Controls)
                {
                    if (ct.GetType() == typeof(Panel))
                    {
                        foreach(Control con in ct.Controls)
                        {
                            if (con.GetType() == typeof(GroupBox))
                            {
                                foreach (Control ctrl in con.Controls)
                                {
                                    if ((ctrl.GetType() == typeof(Label) || ctrl.GetType() == typeof(CheckBox) || ctrl.GetType() == typeof(Button) || ctrl.GetType() == typeof(ComboBox) || ctrl.GetType() == typeof(LinkLabel)) && ctrl.Text.ToLower().Contains(SearchBox.Text.ToLower()))
                                    {
                                        listBox1.Items.Add(new Controls.Development.ImageListBoxItem(ct.Tag.ToString() + " → " + con.Text.ToString() + " → " + ctrl.Text.ToString()));
                                        controale.Add(ctrl);
                                        i++;
                                    }
                                }
                            }
                            else if ((con.GetType() == typeof(Label) || con.GetType() == typeof(CheckBox) || con.GetType() == typeof(Button) || con.GetType() == typeof(ComboBox) || con.GetType() == typeof(LinkLabel)) && con.Text.ToLower().Contains(SearchBox.Text.ToLower()))
                            {
                                listBox1.Items.Add(new Controls.Development.ImageListBoxItem(ct.Tag.ToString() + " → " + con.Text.ToString()));
                                controale.Add(con);
                                i++;
                            }
                        }
                    }
                }
                this.Text = this.Tag.ToString() + " (" + i + ")";
                try
                {
                    listBox1.SelectedIndex = 0;
                }
                catch { }
                DisplayHScroll();
            }
            else
            {
                this.Text = DefaultText;
                //SearchBox.Text = DefaultSearchBoxText;
                listBox1.Items.Clear();
                foreach (Controls.Development.ImageListBoxItem lb in lista)
                {
                    listBox1.Items.Add(lb);
                }
                listBox1.SelectedIndex = 1;
                DisplayHScroll();
            }
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText)
            {
                SearchBox.Text = "";
                this.Text = DefaultText;
                DisplayHScroll();
            }
            if (e.KeyCode == Keys.Enter) e.SuppressKeyPress = true;
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                SearchBox.Text = DefaultSearchBoxText;
                this.Text = DefaultText;
                DisplayHScroll();
            }
        }

        private void SearchBox_Enter(object sender, EventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText) SearchBox.Text = "";
        }

        private void SearchBox_Leave(object sender, EventArgs e)
        {
            try
            {
                if (listBox1.Focused == false || (listBox1.Focused == true && !listBox1.Items[0].Text.Contains("→")))
                {
                    this.Text = DefaultText;
                    SearchBox.Text = DefaultSearchBoxText;
                    listBox1.Items.Clear();
                    foreach (Controls.Development.ImageListBoxItem lb in lista)
                    {
                        listBox1.Items.Add(lb);
                    }
                    listBox1.SelectedIndex = 0;
                    DisplayHScroll();
                }
            }
            catch
            {
                if (listBox1.Focused == false)
                {
                    this.Text = DefaultText;
                    SearchBox.Text = DefaultSearchBoxText;
                    listBox1.Items.Clear();
                    foreach (Controls.Development.ImageListBoxItem lb in lista)
                    {
                        listBox1.Items.Add(lb);
                    }
                    listBox1.SelectedIndex = 0;
                    DisplayHScroll();
                }
            }
        }
        private void DisplayHScroll()
        {
            // Make no partial items are displayed vertically.
            listBox1.IntegralHeight = true;



            // Display a horizontal scroll bar.
            listBox1.HorizontalScrollbar = true;

            // Create a Graphics object to use when determining the size of the largest item in the ListBox.
            Graphics g = listBox1.CreateGraphics();
            int maxim = 0;
            foreach (Controls.Development.ImageListBoxItem it in listBox1.Items)
            {
                if (maxim < (int)g.MeasureString(it.ToString(), listBox1.Font).Width) maxim = (int)g.MeasureString(it.ToString(), listBox1.Font).Width;
            }
            // Determine the size for HorizontalExtent using the MeasureString method using the last item in the list. 
            // Set the HorizontalExtent property.
            listBox1.HorizontalExtent = maxim;
            g.Dispose();
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
