
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public partial class frmAppearanceEditor
{


	private AppearanceControl.AppearanceProperties _ap = null;
	public frmAppearanceEditor(AppearanceControl.AppearanceProperties ap)
	{
		InitializeComponent();
		_ap = ap;
		this.CustomizableMenuStrip1.Appearance.CustomAppearance = ap;
		this.CustomizableStatusStrip1.Appearance.CustomAppearance = ap;
		this.CustomizableToolStrip1.Appearance.CustomAppearance = ap;
		this.PropertyGrid1.SelectedObject = ap;
	}

	public AppearanceControl.AppearanceProperties CustomAppearance {
		get { return _ap; }
	}

	private void OK_Button_Click(System.Object sender, System.EventArgs e)
	{
		this.DialogResult = System.Windows.Forms.DialogResult.OK;
		this.Close();
	}

	private void Load_Button_Click(System.Object sender, System.EventArgs e)
	{
		using (OpenFileDialog ofd = new OpenFileDialog()) {
			ofd.Title = "Select XML File.";
			ofd.Filter = "XML Files (*.xml)|*.xml|All Files|*.*";

			if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				this.LoadAppearance(ofd.FileName);
				CustomizableMenuStrip1.Invalidate();
				CustomizableToolStrip1.Invalidate();
				CustomizableStatusStrip1.Invalidate();
			}
		}
	}

	private void Save_Button_Click(System.Object sender, System.EventArgs e)
	{
		using (SaveFileDialog sfd = new SaveFileDialog()) {
			sfd.Title = "Select XML File.";
			sfd.Filter = "XML Files (*.xml)|*.xml|All Files|*.*";

			if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				this.SaveAppearance(sfd.FileName, AppearanceControl1);
			}
		}
	}

	public void LoadAppearance(string xmlFile)
	{
		try {
			using (FileStream fs = new FileStream(xmlFile, FileMode.Open, FileAccess.Read, FileShare.None)) {
				XmlSerializer ser = new XmlSerializer(typeof(AppearanceControl.AppearanceProperties));

				AppearanceControl.AppearanceProperties ap = default(AppearanceControl.AppearanceProperties);
				ap = (AppearanceControl.AppearanceProperties)ser.Deserialize(fs);
				_ap = ap;
				this.CustomizableMenuStrip1.Appearance.CustomAppearance = ap;
				this.CustomizableStatusStrip1.Appearance.CustomAppearance = ap;
				this.CustomizableToolStrip1.Appearance.CustomAppearance = ap;
				this.PropertyGrid1.SelectedObject = ap;
			}
		} catch (Exception ex) {
			MessageBox.Show(ex.Message);
		}
	}

	public void LoadFromStream(Stream stream)
	{
		try {
			XmlSerializer ser = new XmlSerializer(typeof(AppearanceControl.AppearanceProperties));

			AppearanceControl.AppearanceProperties ap = default(AppearanceControl.AppearanceProperties);
			ap = (AppearanceControl.AppearanceProperties)ser.Deserialize(stream);
			ap.SetAppearanceControl(AppearanceControl1);
			_ap = ap;
			this.AppearanceControl1.CustomAppearance = _ap;

			stream.Close();
		} catch (Exception ex) {
			MessageBox.Show(ex.Message);
		}
	}

	public void SaveAppearance(string xmlFile, AppearanceControl ac)
	{
		try {
			using (FileStream fs = new FileStream(xmlFile, FileMode.Create, FileAccess.Write, FileShare.None)) {
				XmlSerializer ser = new XmlSerializer(typeof(AppearanceControl.AppearanceProperties));
				ser.Serialize(fs, ac.CustomAppearance);
				fs.Close();
			}
		} catch (Exception ex) {
			MessageBox.Show(ex.ToString());
		}
	}

	private void PropertyGrid1_PropertyValueChanged(System.Object s, System.Windows.Forms.PropertyValueChangedEventArgs e)
	{
		this.CustomizableMenuStrip1.Invalidate();
		this.CustomizableStatusStrip1.Invalidate();
		this.CustomizableToolStrip1.Invalidate();
	}
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
