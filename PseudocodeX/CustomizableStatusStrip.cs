
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

public class CustomizableStatusStrip : StatusStrip
{

	public event EventHandler AppearanceControlChanged;

	public CustomizableStatusStrip() : base()
	{
	}

	private AppearanceControl _Appearance;
	public AppearanceControl Appearance {
		get { return _Appearance; }
		set {
			_Appearance = value;
			if (value != null) {
				this.Renderer = value.Renderer;
			}
			this.Invalidate();
			this.OnAppearanceControlChanged(EventArgs.Empty);
		}
	}

	protected virtual void OnAppearanceControlChanged(EventArgs e)
	{
		if (this.Appearance != null) {
			this.Appearance.AppearanceChanged += AppearanceControl_AppearanceChanged;
			this.Appearance.Disposed += AppearanceControl_Disposed;
			this.Renderer = this.Appearance.Renderer;
		} else {
			this.Renderer = new ToolStripProfessionalRenderer();
		}
		this.Invalidate();

		if (AppearanceControlChanged != null) {
			AppearanceControlChanged(this, e);
		}
	}

	private void AppearanceControl_Disposed(object sender, EventArgs e)
	{
		this.Appearance = null;
		this.OnAppearanceControlChanged(EventArgs.Empty);
	}

	private void AppearanceControl_AppearanceChanged(object sender, EventArgs e)
	{
		this.Renderer = this.Appearance.Renderer;
		this.Invalidate();
	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
