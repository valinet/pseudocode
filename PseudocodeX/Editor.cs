﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace PseudocodeX
{
    public partial class Editor : DockContent
    {
        public Editor()
        {
            InitializeComponent();
        }
        
        private void Editor_Load(object sender, EventArgs e)
        {
            //Colorare cod tema inchisa
            pseudocode.Lexing.Lexer = ScintillaNET.Lexer.Cpp;
            pseudocode.Lexing.Keywords[0] = "citeste scrie daca atunci altfel sfarsit_daca cat_timp sfarsit_cat_timp pentru sfarsit_pentru executa pana_cand si sau intreg caracter sir real div mod diferit";
            pseudocode.Lexing.LineCommentPrefix = "//";
            pseudocode.Styles[pseudocode.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].ForeColor = System.Drawing.Color.White;
            pseudocode.Styles[pseudocode.Lexing.StyleNameMap["NUMBER"]].ForeColor = System.Drawing.Color.FromArgb(102, 177, 168);
            pseudocode.Styles[pseudocode.Lexing.StyleNameMap["WORD"]].ForeColor = System.Drawing.Color.FromArgb(86, 156, 214);
            pseudocode.Styles[pseudocode.Lexing.StyleNameMap["WORD2"]].ForeColor = System.Drawing.Color.FromArgb(86, 156, 214);
            pseudocode.Styles[pseudocode.Lexing.StyleNameMap["STRING"]].ForeColor = System.Drawing.Color.FromArgb(214, 157, 133);
            pseudocode.Styles[pseudocode.Lexing.StyleNameMap["CHARACTER"]].ForeColor = System.Drawing.Color.FromArgb(86, 156, 214);
            pseudocode.Styles[pseudocode.Lexing.StyleNameMap["PREPROCESSOR"]].ForeColor = System.Drawing.Color.Brown;
            pseudocode.Styles[pseudocode.Lexing.StyleNameMap["OPERATOR"]].ForeColor = System.Drawing.Color.Gray;
            pseudocode.Styles[pseudocode.Lexing.StyleNameMap["IDENTIFIER"]].ForeColor = System.Drawing.Color.White;
            pseudocode.Styles[pseudocode.Lexing.StyleNameMap["COMMENT"]].ForeColor = System.Drawing.Color.Green; 
            pseudocode.Styles.LineNumber.BackColor = Color.FromArgb(64, 64, 64);
            pseudocode.Styles.LineNumber.ForeColor = Color.Silver;
            //Sfarsit colorare cod tema inchisa
        }

        private void pseudocode_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
