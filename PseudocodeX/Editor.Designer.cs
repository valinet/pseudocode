﻿namespace PseudocodeX
{
    partial class Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pseudocode = new ScintillaNET.Scintilla();
            this.converter = new ScintillaNET.Scintilla();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pseudocode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.converter)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Gray;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pseudocode);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.converter);
            this.splitContainer1.Size = new System.Drawing.Size(811, 562);
            this.splitContainer1.SplitterDistance = 397;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 0;
            // 
            // pseudocode
            // 
            this.pseudocode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pseudocode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pseudocode.Caret.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pseudocode.Caret.CurrentLineBackgroundColor = System.Drawing.Color.DimGray;
            this.pseudocode.Caret.HighlightCurrentLine = true;
            this.pseudocode.ConfigurationManager.CustomLocation = "Scintilla.XML";
            this.pseudocode.ConfigurationManager.Language = "default";
            this.pseudocode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pseudocode.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pseudocode.ForeColor = System.Drawing.Color.White;
            this.pseudocode.Location = new System.Drawing.Point(0, 0);
            this.pseudocode.Margins.Margin0.Width = 30;
            this.pseudocode.Name = "pseudocode";
            this.pseudocode.Size = new System.Drawing.Size(397, 562);
            this.pseudocode.Styles.BraceBad.Size = 9F;
            this.pseudocode.Styles.BraceLight.Size = 9F;
            this.pseudocode.Styles.ControlChar.Size = 9F;
            this.pseudocode.Styles.Default.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pseudocode.Styles.Default.Size = 9F;
            this.pseudocode.Styles.IndentGuide.Size = 9F;
            this.pseudocode.Styles.LastPredefined.Size = 9F;
            this.pseudocode.Styles.LineNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pseudocode.Styles.LineNumber.ForeColor = System.Drawing.Color.Silver;
            this.pseudocode.Styles.LineNumber.Size = 9F;
            this.pseudocode.Styles.Max.Size = 9F;
            this.pseudocode.TabIndex = 0;
            this.pseudocode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pseudocode_KeyDown);
            // 
            // converter
            // 
            this.converter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.converter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.converter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.converter.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.converter.ForeColor = System.Drawing.Color.White;
            this.converter.Location = new System.Drawing.Point(0, 0);
            this.converter.Margins.Margin0.Width = 30;
            this.converter.Name = "converter";
            this.converter.Size = new System.Drawing.Size(413, 562);
            this.converter.Styles.BraceBad.Size = 9F;
            this.converter.Styles.BraceLight.Size = 9F;
            this.converter.Styles.ControlChar.Size = 9F;
            this.converter.Styles.Default.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.converter.Styles.Default.Size = 9F;
            this.converter.Styles.IndentGuide.Size = 9F;
            this.converter.Styles.LastPredefined.Size = 9F;
            this.converter.Styles.LineNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.converter.Styles.LineNumber.ForeColor = System.Drawing.Color.Silver;
            this.converter.Styles.LineNumber.Size = 9F;
            this.converter.Styles.Max.Size = 9F;
            this.converter.TabIndex = 1;
            // 
            // Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 562);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Editor";
            this.Text = "Neintitulat";
            this.Load += new System.EventHandler(this.Editor_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pseudocode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.converter)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private ScintillaNET.Scintilla pseudocode;
        private ScintillaNET.Scintilla converter;



    }
}