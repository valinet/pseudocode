﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using WeifenLuo.WinFormsUI.Docking;

namespace PseudocodeX
{
    public partial class MainForm : Form
    {

        private FormWindowState mLastState;

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetProcessDPIAware();
        string DefaultSearchBoxText;
        //Glow wpfwindow = new Glow();
        public MainForm()
        {
            SetProcessDPIAware();
            InitializeComponent();
            DefaultSearchBoxText = SearchBox.Text;
            mLastState = this.WindowState;
            New();
            listBox1.Tag = "";
            //listBox1.DrawMode = DrawMode.OwnerDrawFixed;
        }
        public void New()
        {
            this.Controls.OfType<MdiClient>().ElementAt(0).BackColor = Color.Black;
            MdiClient client = Controls.OfType<MdiClient>().First();
            client.Dock = DockStyle.Fill;
            client.Top = toolStrip1.Bottom;
            client.BringToFront();
            Editor ed = new Editor();
            ed.Show(dockPanel, DockState.Document);
            dockPanel.BringToFront();
            Top.BringToFront();
            Left.BringToFront();
            Right.BringToFront();
            Bottom.BringToFront();
            TopCover.BringToFront();
            BottomDown.BringToFront();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
        }

        protected override void OnClientSizeChanged(EventArgs e)
        {
            if (this.WindowState != mLastState)
            {
                mLastState = this.WindowState;
                OnWindowStateChanged(e);
            }
            base.OnClientSizeChanged(e);
        }
        protected void OnWindowStateChanged(EventArgs e)
        {

        }

        private void customizeToolbarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.toolStrip1.ImageSizeSelection = true;
            this.toolStrip1.ListViewDisplayStyle = ToolStripCustomCtrls.ListViewDisplayStyle.List;
            this.toolStrip1.Customize("RO");
        }

        private void dockPanel_ActiveContentChanged(object sender, EventArgs e)
        {

        }

        private void dockPanel_Paint(object sender, PaintEventArgs e)
        {
        }

        private void newPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            New();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            New();
        }

        /// <summary>
        /// Gets a list of all ToolStripMenuItems
        /// </summary>
        /// <param name="menuStrip">The menu strip control</param>
        /// <returns>List of all ToolStripMenuItems</returns>
        public static List<ToolStripMenuItem> GetItems(MenuStrip menuStrip)
        {
            List<ToolStripMenuItem> myItems = new List<ToolStripMenuItem>();
            foreach (ToolStripMenuItem i in menuStrip.Items)
            {
                GetMenuItems(i, myItems);
            }
            return myItems;
        }
        private static string menuPath = "";
        private static void GetParentItem(ToolStripItem item)
        {
            if (item.OwnerItem != null) GetParentItem(item.OwnerItem);
            if (item.Text.ToUpper() == item.Text && Regex.Matches(item.Text, @"[a-zA-Z]").Count != 0) menuPath += item.Text.Substring(1, 1) + item.Text.ToLower().Substring(2, item.Text.Length - 2) + " → ";
            else menuPath += item.Text + " → ";
        }
        /// <summary>
        /// Gets the menu items.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="items">The items.</param>
        private static void GetMenuItems(ToolStripMenuItem item, List<ToolStripMenuItem> items)
        {
            item.Tag = "";
            menuPath = "";
            GetParentItem(item);
            if (menuPath.EndsWith(" → ")) menuPath = menuPath.Substring(0, menuPath.Length - 3);
            if (menuPath.StartsWith("&")) menuPath = menuPath.Substring(1, menuPath.Length - 1);
            item.Tag = menuPath;
            if (item.HasDropDownItems == false) items.Add(item);
            foreach (ToolStripItem i in item.DropDownItems)
            {
                if (i is ToolStripMenuItem)
                {
                    GetMenuItems((ToolStripMenuItem)i, items);
                }
            }
        }

        private void SearchBox_Enter(object sender, EventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText) SearchBox.Text = "";

        }

        private void SearchBox_Leave(object sender, EventArgs e)
        {
            if (SearchPanel.Focused == false && listBox1.Focused == false)
            {
                SearchBox.Text = DefaultSearchBoxText;
                SearchPanel.Visible = false;
            }
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter) e.SuppressKeyPress = true;
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                SearchBox.Text = DefaultSearchBoxText;
                SearchPanel.Visible = false;
            }
        }

        private void SearchBox_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        List<ToolStripMenuItem> myItems;
        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                try
                {
                    listBox1.SelectedIndex = 0;
                }
                catch{}
                listBox1.Focus();
                e.SuppressKeyPress = true;
                return;
            }
            listBox1.Tag = SearchBox.Text;
            if (SearchBox.Text != "" && SearchBox.Text != DefaultSearchBoxText)
            {
                SearchPanel.Visible = true;
                SearchPanel.BringToFront();
                ImageList im = new ImageList();
                im.ColorDepth = ColorDepth.Depth32Bit;
                listBox1.ImageList = im;
                listBox1.Items.Clear();
                int i = 0;
                myItems = GetItems(this.menuStrip1);
                List<ToolStripMenuItem> usedItems = new List<ToolStripMenuItem>();
                foreach (var item in myItems.ToList())
                {
                    if (item.Text.ToLower().Contains(SearchBox.Text.ToLower()))
                    {
                        if (item.Image != null)
                        {
                            im.Images.Add(item.Image);
                            listBox1.Items.Add(new Controls.Development.ImageListBoxItem(item.Tag.ToString(), i));
                        }
                        else listBox1.Items.Add(new Controls.Development.ImageListBoxItem(item.Tag.ToString()));
                        i++;
                        usedItems.Add(item);
                    }
                }
                myItems = usedItems;
                SearchText.Text = SearchText.Tag.ToString() + " (" + i + ")";
                panel2.Left = SearchText.Left + SearchText.Width + 5;
                /*try
                {
                    listBox1.SelectedIndex = 0;
                }
                catch { }*/

            }
            else
            {
                SearchPanel.Visible = false;
            }
        }

        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            /*e.DrawBackground();
           
            Graphics g = e.Graphics;
            Brush br = new SolidBrush(Color.FromArgb(253, 244, 191));
            Brush brush = ((e.State & DrawItemState.Selected) == DrawItemState.Selected) ?
                          br : new SolidBrush(e.BackColor);
            g.FillRectangle(brush, e.Bounds);
            e.Graphics.DrawString(listBox1.Items[e.Index].ToString(), e.Font,
                     new SolidBrush(e.ForeColor), e.Bounds, StringFormat.GenericDefault);
            e.DrawFocusRectangle();     */ 
        }

        private void listBox1_MouseHover(object sender, EventArgs e)
        {

        }

        private void listBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Point point = listBox1.PointToClient(Cursor.Position);
            int index = listBox1.IndexFromPoint(point);
            if (index < 0) return;
            listBox1.SelectedIndex = index;
        }

        private void listBox1_MouseLeave(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = -1;
        }

        private void listBox1_Leave(object sender, EventArgs e)
        {
            if (SearchPanel.Focused == false && SearchText.Focused == false)
            {
                SearchBox.Text = DefaultSearchBoxText;
                SearchPanel.Visible = false;
            }
        }

        private void SearchPanel_Leave(object sender, EventArgs e)
        {
            if (SearchText.Focused == false && listBox1.Focused == false)
            {
                SearchBox.Text = DefaultSearchBoxText;
                SearchPanel.Visible = false;
            }
        }

        private void SearchPanel_VisibleChanged(object sender, EventArgs e)
        {
            
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            myItems[listBox1.SelectedIndex].PerformClick();
        }

        private void SearchPanel_Enter(object sender, EventArgs e)
        {
            listBox1.Focus();
        }

        private void SearchPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void listBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                myItems[listBox1.SelectedIndex].PerformClick();
            }
        }

        private void listBox1_MouseEnter(object sender, EventArgs e)
        {
            listBox1.Focus();
        }

        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Options op = new Options();
            op.ShowDialog();
        }

        private void openPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Open op = new Open();
            op.ShowDialog();
        }
    }
    public class MyRenderer : ToolStripRenderer
    {

        protected override void OnRenderArrow(ToolStripArrowRenderEventArgs e)
        {
            e.ArrowColor = Color.White;
            base.OnRenderArrow(e);
        }

    }
}
