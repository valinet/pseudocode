﻿namespace PseudocodeX
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            this.MaximizeWhite = new System.Windows.Forms.Button();
            this.RestoreWhite = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new CustomizableToolStrip();
            this.appearanceControl1 = new AppearanceControl();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton13 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton14 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton15 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton16 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton17 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripButton18 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton19 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton20 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton21 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton22 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton23 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton24 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton25 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton26 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton27 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton28 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton29 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton30 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton31 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton32 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton33 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton34 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton35 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton36 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton37 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton38 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton39 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton40 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton41 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton42 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton43 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton44 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton45 = new System.Windows.Forms.ToolStripButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new CustomizableMenuStrip();
            this.fILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveLogicSchemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.openPseudocodeUsingAcodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharePseudocodeUsingAcodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.printPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printConvertedCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printLogicSchemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.recoverLostunsavedFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.removePersonalInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.codeboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eDITToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoActionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.jumpToLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jumpToBreakpointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jumpToBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.replaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedFindToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vIEWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertedCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logicSchemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.zoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripSeparator();
            this.statisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cONVERTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toBasicClassicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toPascalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toJavaScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toVBScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripSeparator();
            this.toLogicSchemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dEBUGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compileAndRunToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.justCompileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startDebuggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripSeparator();
            this.continueDebuggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.watchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendCommandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripSeparator();
            this.addremoveBreakpointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addremoveWatchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripSeparator();
            this.endPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tOOLSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripSeparator();
            this.createApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripSeparator();
            this.testCurrentConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extensionsManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrativeConfigurationsManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem33 = new System.Windows.Forms.ToolStripSeparator();
            this.customizeToolbarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wINDOWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripSeparator();
            this.closeAllTabsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeCurrentTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripSeparator();
            this.casacdeWindowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileWindowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showWindowsSideBySideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripSeparator();
            this.restoreDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maximizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripSeparator();
            this.hELPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpSupportAndTipsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripSeparator();
            this.contextualHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.whatsThisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlineAssistanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.valiNetOnTheInternetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripSeparator();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.valiNetCustomerExperienceImprovementProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.licenseAgreementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderPseudocodeOnADVDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transparentControl1 = new PseudocodeX.TransparentControl();
            this.Top = new System.Windows.Forms.Panel();
            this.Right = new System.Windows.Forms.Panel();
            this.Left = new System.Windows.Forms.Panel();
            this.Bottom = new System.Windows.Forms.Panel();
            this.dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.TopCover = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new CustomizableStatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.BottomDown = new System.Windows.Forms.Panel();
            this.listBox1 = new Controls.Development.ImageListBox();
            this.SearchPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SearchText = new System.Windows.Forms.LinkLabel();
            this.appearanceControl2 = new AppearanceControl();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SearchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MaximizeWhite
            // 
            this.MaximizeWhite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.MaximizeWhite, "MaximizeWhite");
            this.MaximizeWhite.FlatAppearance.BorderSize = 0;
            this.MaximizeWhite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.MaximizeWhite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.MaximizeWhite.ForeColor = System.Drawing.Color.Black;
            this.MaximizeWhite.Name = "MaximizeWhite";
            this.MaximizeWhite.UseVisualStyleBackColor = false;
            // 
            // RestoreWhite
            // 
            this.RestoreWhite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.RestoreWhite, "RestoreWhite");
            this.RestoreWhite.FlatAppearance.BorderSize = 0;
            this.RestoreWhite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.RestoreWhite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.RestoreWhite.ForeColor = System.Drawing.Color.Black;
            this.RestoreWhite.Name = "RestoreWhite";
            this.RestoreWhite.UseVisualStyleBackColor = false;
            // 
            // SearchBox
            // 
            resources.ApplyResources(this.SearchBox, "SearchBox");
            this.SearchBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.SearchBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SearchBox.ForeColor = System.Drawing.Color.White;
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Enter += new System.EventHandler(this.SearchBox_Enter);
            this.SearchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SearchBox_KeyDown);
            this.SearchBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SearchBox_KeyPress);
            this.SearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SearchBox_KeyUp);
            this.SearchBox.Leave += new System.EventHandler(this.SearchBox_Leave);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Appearance = this.appearanceControl1;
            this.toolStrip1.BackColor = System.Drawing.Color.Black;
            this.toolStrip1.DefaultButton = true;
            this.toolStrip1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageSizeSelection = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripSeparator1,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripSeparator2,
            this.toolStripButton9,
            this.toolStripButton10,
            this.toolStripButton11,
            this.toolStripSeparator3,
            this.toolStripButton12,
            this.toolStripButton13,
            this.toolStripButton14,
            this.toolStripButton15,
            this.toolStripButton16,
            this.toolStripSeparator4,
            this.toolStripButton17,
            this.toolStripButton18,
            this.toolStripSeparator5,
            this.toolStripButton19,
            this.toolStripButton20,
            this.toolStripButton21,
            this.toolStripButton22,
            this.toolStripButton23,
            this.toolStripSeparator6,
            this.toolStripButton24,
            this.toolStripButton25,
            this.toolStripButton26,
            this.toolStripButton27,
            this.toolStripSeparator7,
            this.toolStripButton28,
            this.toolStripButton29,
            this.toolStripButton30,
            this.toolStripSeparator8,
            this.toolStripButton31,
            this.toolStripSeparator9,
            this.toolStripButton32,
            this.toolStripButton33,
            this.toolStripButton34,
            this.toolStripButton35,
            this.toolStripButton36,
            this.toolStripButton37,
            this.toolStripButton38,
            this.toolStripButton39,
            this.toolStripButton40,
            this.toolStripSeparator10,
            this.toolStripButton41,
            this.toolStripButton42,
            this.toolStripButton43,
            this.toolStripSeparator11,
            this.toolStripButton44,
            this.toolStripButton45});
            this.toolStrip1.LargeIcons = false;
            this.toolStrip1.LargeImageList = this.imageList1;
            this.toolStrip1.LargeImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.ListViewDisplayStyle = ToolStripCustomCtrls.ListViewDisplayStyle.Tiles;
            resources.ApplyResources(this.toolStrip1, "toolStrip1");
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RoundedEdges = false;
            this.toolStrip1.SmallImageScalingSize = new System.Drawing.Size(16, 16);
            this.toolStrip1.UserData = ((ToolStripCustomCtrls.ToolStripData)(resources.GetObject("toolStrip1.UserData")));
            // 
            // appearanceControl1
            // 
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.Background = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intBackground = -12566464;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intBorderHighlight = -12566464;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientBegin = -12566464;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientEnd = -12566464;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientMiddle = -12566464;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intHighlight = -3878683;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intPressedBackground = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intSelectedBackground = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(128)))), ((int)(((byte)(62)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(128)))), ((int)(((byte)(62)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intBorder = -16777088;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intBorderHighlight = -13410648;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientBegin = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientEnd = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientMiddle = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intHighlight = -6771246;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorder = -8355712;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorderHighlight = -8355712;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientBegin = -12566464;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientEnd = -12566464;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientMiddle = -12566464;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intHighlight = -3878683;
            this.appearanceControl1.CustomAppearance.GripAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.GripAppearance.intDark = -1;
            this.appearanceControl1.CustomAppearance.GripAppearance.intLight = -1;
            this.appearanceControl1.CustomAppearance.GripAppearance.Light = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.intGradientBegin = -16777216;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.intGradientEnd = -16777216;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.intGradientMiddle = -16777216;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientBegin = -3416586;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientEnd = -9266217;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientMiddle = -6175239;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intBorder = -16777216;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intPressedGradientBegin = -12566464;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intPressedGradientEnd = -12566464;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intPressedGradientMiddle = -12566464;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intSelected = -12566464;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intSelectedGradientBegin = -12566464;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intSelectedGradientEnd = -12566464;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.PressedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.PressedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.PressedGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.Selected = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.SelectedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.SelectedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.intBorder = -1;
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.intGradientBegin = -16777216;
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.intGradientEnd = -16777216;
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.intGradientBegin = -16777216;
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.intGradientEnd = -16777216;
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.intGradientMiddle = -16777216;
            this.appearanceControl1.CustomAppearance.RaftingContainerAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.RaftingContainerAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.RaftingContainerAppearance.intGradientBegin = -16777216;
            this.appearanceControl1.CustomAppearance.RaftingContainerAppearance.intGradientEnd = -16777216;
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.intDark = -1;
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.intLight = -1;
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.Light = System.Drawing.Color.White;
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.intGradientBegin = -16777216;
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.intGradientEnd = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.ContentPanelGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.ContentPanelGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.DropDownBackground = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intBorder = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intContentPanelGradientBegin = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intContentPanelGradientEnd = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intDropDownBackground = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intGradientBegin = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intGradientEnd = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intGradientMiddle = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intPanelGradientBegin = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intPanelGradientEnd = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.PanelGradientBegin = System.Drawing.Color.Black;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.PanelGradientEnd = System.Drawing.Color.Black;
            this.appearanceControl1.Preset = AppearanceControl.enumPresetStyles.Custom;
            this.appearanceControl1.Renderer.RoundedEdges = false;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton1, "toolStripButton1");
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton2, "toolStripButton2");
            this.toolStripButton2.Name = "toolStripButton2";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton3, "toolStripButton3");
            this.toolStripButton3.Name = "toolStripButton3";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton4, "toolStripButton4");
            this.toolStripButton4.Name = "toolStripButton4";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton5, "toolStripButton5");
            this.toolStripButton5.Name = "toolStripButton5";
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton6, "toolStripButton6");
            this.toolStripButton6.Name = "toolStripButton6";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton7, "toolStripButton7");
            this.toolStripButton7.Name = "toolStripButton7";
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton8, "toolStripButton8");
            this.toolStripButton8.Name = "toolStripButton8";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton9, "toolStripButton9");
            this.toolStripButton9.Name = "toolStripButton9";
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton10, "toolStripButton10");
            this.toolStripButton10.Name = "toolStripButton10";
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton11, "toolStripButton11");
            this.toolStripButton11.Name = "toolStripButton11";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // toolStripButton12
            // 
            this.toolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton12, "toolStripButton12");
            this.toolStripButton12.Name = "toolStripButton12";
            // 
            // toolStripButton13
            // 
            this.toolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton13, "toolStripButton13");
            this.toolStripButton13.Name = "toolStripButton13";
            // 
            // toolStripButton14
            // 
            this.toolStripButton14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton14, "toolStripButton14");
            this.toolStripButton14.Name = "toolStripButton14";
            // 
            // toolStripButton15
            // 
            this.toolStripButton15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton15, "toolStripButton15");
            this.toolStripButton15.Name = "toolStripButton15";
            // 
            // toolStripButton16
            // 
            this.toolStripButton16.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton16, "toolStripButton16");
            this.toolStripButton16.Name = "toolStripButton16";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            // 
            // toolStripButton17
            // 
            this.toolStripButton17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton17, "toolStripButton17");
            this.toolStripButton17.Name = "toolStripButton17";
            // 
            // toolStripButton18
            // 
            this.toolStripButton18.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton18, "toolStripButton18");
            this.toolStripButton18.Name = "toolStripButton18";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
            // 
            // toolStripButton19
            // 
            this.toolStripButton19.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton19, "toolStripButton19");
            this.toolStripButton19.Name = "toolStripButton19";
            // 
            // toolStripButton20
            // 
            this.toolStripButton20.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton20, "toolStripButton20");
            this.toolStripButton20.Name = "toolStripButton20";
            // 
            // toolStripButton21
            // 
            this.toolStripButton21.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton21, "toolStripButton21");
            this.toolStripButton21.Name = "toolStripButton21";
            // 
            // toolStripButton22
            // 
            this.toolStripButton22.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton22, "toolStripButton22");
            this.toolStripButton22.Name = "toolStripButton22";
            // 
            // toolStripButton23
            // 
            this.toolStripButton23.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton23, "toolStripButton23");
            this.toolStripButton23.Name = "toolStripButton23";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            resources.ApplyResources(this.toolStripSeparator6, "toolStripSeparator6");
            // 
            // toolStripButton24
            // 
            this.toolStripButton24.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton24, "toolStripButton24");
            this.toolStripButton24.Name = "toolStripButton24";
            // 
            // toolStripButton25
            // 
            this.toolStripButton25.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton25, "toolStripButton25");
            this.toolStripButton25.Name = "toolStripButton25";
            // 
            // toolStripButton26
            // 
            this.toolStripButton26.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton26, "toolStripButton26");
            this.toolStripButton26.Name = "toolStripButton26";
            // 
            // toolStripButton27
            // 
            this.toolStripButton27.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton27, "toolStripButton27");
            this.toolStripButton27.Name = "toolStripButton27";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            resources.ApplyResources(this.toolStripSeparator7, "toolStripSeparator7");
            // 
            // toolStripButton28
            // 
            this.toolStripButton28.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton28, "toolStripButton28");
            this.toolStripButton28.Name = "toolStripButton28";
            // 
            // toolStripButton29
            // 
            this.toolStripButton29.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton29, "toolStripButton29");
            this.toolStripButton29.Name = "toolStripButton29";
            // 
            // toolStripButton30
            // 
            this.toolStripButton30.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton30, "toolStripButton30");
            this.toolStripButton30.Name = "toolStripButton30";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            resources.ApplyResources(this.toolStripSeparator8, "toolStripSeparator8");
            // 
            // toolStripButton31
            // 
            this.toolStripButton31.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton31, "toolStripButton31");
            this.toolStripButton31.Name = "toolStripButton31";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            resources.ApplyResources(this.toolStripSeparator9, "toolStripSeparator9");
            // 
            // toolStripButton32
            // 
            this.toolStripButton32.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.toolStripButton32, "toolStripButton32");
            this.toolStripButton32.Name = "toolStripButton32";
            // 
            // toolStripButton33
            // 
            this.toolStripButton33.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton33, "toolStripButton33");
            this.toolStripButton33.Name = "toolStripButton33";
            // 
            // toolStripButton34
            // 
            this.toolStripButton34.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton34, "toolStripButton34");
            this.toolStripButton34.Name = "toolStripButton34";
            // 
            // toolStripButton35
            // 
            this.toolStripButton35.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton35, "toolStripButton35");
            this.toolStripButton35.Name = "toolStripButton35";
            // 
            // toolStripButton36
            // 
            this.toolStripButton36.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton36, "toolStripButton36");
            this.toolStripButton36.Name = "toolStripButton36";
            // 
            // toolStripButton37
            // 
            this.toolStripButton37.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton37, "toolStripButton37");
            this.toolStripButton37.Name = "toolStripButton37";
            // 
            // toolStripButton38
            // 
            this.toolStripButton38.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton38, "toolStripButton38");
            this.toolStripButton38.Name = "toolStripButton38";
            // 
            // toolStripButton39
            // 
            this.toolStripButton39.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton39, "toolStripButton39");
            this.toolStripButton39.Name = "toolStripButton39";
            // 
            // toolStripButton40
            // 
            this.toolStripButton40.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton40, "toolStripButton40");
            this.toolStripButton40.Name = "toolStripButton40";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            resources.ApplyResources(this.toolStripSeparator10, "toolStripSeparator10");
            // 
            // toolStripButton41
            // 
            this.toolStripButton41.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton41, "toolStripButton41");
            this.toolStripButton41.Name = "toolStripButton41";
            // 
            // toolStripButton42
            // 
            this.toolStripButton42.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton42, "toolStripButton42");
            this.toolStripButton42.Name = "toolStripButton42";
            // 
            // toolStripButton43
            // 
            this.toolStripButton43.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton43, "toolStripButton43");
            this.toolStripButton43.Name = "toolStripButton43";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            resources.ApplyResources(this.toolStripSeparator11, "toolStripSeparator11");
            // 
            // toolStripButton44
            // 
            this.toolStripButton44.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton44, "toolStripButton44");
            this.toolStripButton44.Name = "toolStripButton44";
            // 
            // toolStripButton45
            // 
            this.toolStripButton45.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.toolStripButton45, "toolStripButton45");
            this.toolStripButton45.Name = "toolStripButton45";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "document.png");
            // 
            // menuStrip1
            // 
            this.menuStrip1.Appearance = this.appearanceControl1;
            this.menuStrip1.BackColor = System.Drawing.Color.Black;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fILEToolStripMenuItem,
            this.eDITToolStripMenuItem,
            this.vIEWToolStripMenuItem,
            this.cONVERTToolStripMenuItem,
            this.dEBUGToolStripMenuItem,
            this.tOOLSToolStripMenuItem,
            this.wINDOWToolStripMenuItem,
            this.hELPToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // fILEToolStripMenuItem
            // 
            this.fILEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newPseudocodeToolStripMenuItem,
            this.openPseudocodeToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.saveAllToolStripMenuItem,
            this.saveLogicSchemaToolStripMenuItem,
            this.toolStripMenuItem1,
            this.openPseudocodeUsingAcodeToolStripMenuItem,
            this.sharePseudocodeUsingAcodeToolStripMenuItem,
            this.toolStripMenuItem2,
            this.printPseudocodeToolStripMenuItem,
            this.printConvertedCodeToolStripMenuItem,
            this.printLogicSchemeToolStripMenuItem,
            this.toolStripMenuItem3,
            this.recoverLostunsavedFilesToolStripMenuItem,
            this.exportCodeToolStripMenuItem,
            this.toolStripMenuItem4,
            this.removePersonalInformationToolStripMenuItem,
            this.propertiesToolStripMenuItem,
            this.toolStripMenuItem5,
            this.codeboardToolStripMenuItem,
            this.exitPseudocodeToolStripMenuItem});
            this.fILEToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fILEToolStripMenuItem.Name = "fILEToolStripMenuItem";
            resources.ApplyResources(this.fILEToolStripMenuItem, "fILEToolStripMenuItem");
            // 
            // newPseudocodeToolStripMenuItem
            // 
            this.newPseudocodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.newPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.newPseudocodeToolStripMenuItem, "newPseudocodeToolStripMenuItem");
            this.newPseudocodeToolStripMenuItem.Name = "newPseudocodeToolStripMenuItem";
            this.newPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.newPseudocodeToolStripMenuItem_Click);
            // 
            // openPseudocodeToolStripMenuItem
            // 
            this.openPseudocodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.openPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.openPseudocodeToolStripMenuItem, "openPseudocodeToolStripMenuItem");
            this.openPseudocodeToolStripMenuItem.Name = "openPseudocodeToolStripMenuItem";
            this.openPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.openPseudocodeToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.saveToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.saveToolStripMenuItem, "saveToolStripMenuItem");
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.saveAsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.saveAsToolStripMenuItem, "saveAsToolStripMenuItem");
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            // 
            // saveAllToolStripMenuItem
            // 
            this.saveAllToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.saveAllToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.saveAllToolStripMenuItem, "saveAllToolStripMenuItem");
            this.saveAllToolStripMenuItem.Name = "saveAllToolStripMenuItem";
            // 
            // saveLogicSchemaToolStripMenuItem
            // 
            this.saveLogicSchemaToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.saveLogicSchemaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.saveLogicSchemaToolStripMenuItem, "saveLogicSchemaToolStripMenuItem");
            this.saveLogicSchemaToolStripMenuItem.Name = "saveLogicSchemaToolStripMenuItem";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            // 
            // openPseudocodeUsingAcodeToolStripMenuItem
            // 
            this.openPseudocodeUsingAcodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.openPseudocodeUsingAcodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.openPseudocodeUsingAcodeToolStripMenuItem, "openPseudocodeUsingAcodeToolStripMenuItem");
            this.openPseudocodeUsingAcodeToolStripMenuItem.Name = "openPseudocodeUsingAcodeToolStripMenuItem";
            // 
            // sharePseudocodeUsingAcodeToolStripMenuItem
            // 
            this.sharePseudocodeUsingAcodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.sharePseudocodeUsingAcodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.sharePseudocodeUsingAcodeToolStripMenuItem, "sharePseudocodeUsingAcodeToolStripMenuItem");
            this.sharePseudocodeUsingAcodeToolStripMenuItem.Name = "sharePseudocodeUsingAcodeToolStripMenuItem";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            // 
            // printPseudocodeToolStripMenuItem
            // 
            this.printPseudocodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.printPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.printPseudocodeToolStripMenuItem, "printPseudocodeToolStripMenuItem");
            this.printPseudocodeToolStripMenuItem.Name = "printPseudocodeToolStripMenuItem";
            // 
            // printConvertedCodeToolStripMenuItem
            // 
            this.printConvertedCodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.printConvertedCodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.printConvertedCodeToolStripMenuItem, "printConvertedCodeToolStripMenuItem");
            this.printConvertedCodeToolStripMenuItem.Name = "printConvertedCodeToolStripMenuItem";
            // 
            // printLogicSchemeToolStripMenuItem
            // 
            this.printLogicSchemeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.printLogicSchemeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.printLogicSchemeToolStripMenuItem, "printLogicSchemeToolStripMenuItem");
            this.printLogicSchemeToolStripMenuItem.Name = "printLogicSchemeToolStripMenuItem";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
            // 
            // recoverLostunsavedFilesToolStripMenuItem
            // 
            this.recoverLostunsavedFilesToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.recoverLostunsavedFilesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.recoverLostunsavedFilesToolStripMenuItem, "recoverLostunsavedFilesToolStripMenuItem");
            this.recoverLostunsavedFilesToolStripMenuItem.Name = "recoverLostunsavedFilesToolStripMenuItem";
            // 
            // exportCodeToolStripMenuItem
            // 
            this.exportCodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.exportCodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.exportCodeToolStripMenuItem, "exportCodeToolStripMenuItem");
            this.exportCodeToolStripMenuItem.Name = "exportCodeToolStripMenuItem";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
            // 
            // removePersonalInformationToolStripMenuItem
            // 
            this.removePersonalInformationToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.removePersonalInformationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.removePersonalInformationToolStripMenuItem, "removePersonalInformationToolStripMenuItem");
            this.removePersonalInformationToolStripMenuItem.Name = "removePersonalInformationToolStripMenuItem";
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.propertiesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.propertiesToolStripMenuItem, "propertiesToolStripMenuItem");
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            resources.ApplyResources(this.toolStripMenuItem5, "toolStripMenuItem5");
            // 
            // codeboardToolStripMenuItem
            // 
            this.codeboardToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.codeboardToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.codeboardToolStripMenuItem, "codeboardToolStripMenuItem");
            this.codeboardToolStripMenuItem.Name = "codeboardToolStripMenuItem";
            // 
            // exitPseudocodeToolStripMenuItem
            // 
            this.exitPseudocodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.exitPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.exitPseudocodeToolStripMenuItem, "exitPseudocodeToolStripMenuItem");
            this.exitPseudocodeToolStripMenuItem.Name = "exitPseudocodeToolStripMenuItem";
            // 
            // eDITToolStripMenuItem
            // 
            this.eDITToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoActionToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripMenuItem6,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.selectAllToolStripMenuItem,
            this.deleteSelectionToolStripMenuItem,
            this.toolStripMenuItem7,
            this.jumpToLineToolStripMenuItem,
            this.goToDefinitionToolStripMenuItem,
            this.renameVariableToolStripMenuItem,
            this.jumpToBreakpointToolStripMenuItem,
            this.jumpToBookmarkToolStripMenuItem,
            this.toolStripMenuItem8,
            this.findToolStripMenuItem,
            this.replaceToolStripMenuItem,
            this.advancedFindToolStripMenuItem});
            this.eDITToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.eDITToolStripMenuItem.Name = "eDITToolStripMenuItem";
            resources.ApplyResources(this.eDITToolStripMenuItem, "eDITToolStripMenuItem");
            // 
            // undoActionToolStripMenuItem
            // 
            this.undoActionToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.undoActionToolStripMenuItem, "undoActionToolStripMenuItem");
            this.undoActionToolStripMenuItem.Name = "undoActionToolStripMenuItem";
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.redoToolStripMenuItem, "redoToolStripMenuItem");
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            resources.ApplyResources(this.toolStripMenuItem6, "toolStripMenuItem6");
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.cutToolStripMenuItem, "cutToolStripMenuItem");
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.copyToolStripMenuItem, "copyToolStripMenuItem");
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.pasteToolStripMenuItem, "pasteToolStripMenuItem");
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.selectAllToolStripMenuItem, "selectAllToolStripMenuItem");
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            // 
            // deleteSelectionToolStripMenuItem
            // 
            this.deleteSelectionToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.deleteSelectionToolStripMenuItem, "deleteSelectionToolStripMenuItem");
            this.deleteSelectionToolStripMenuItem.Name = "deleteSelectionToolStripMenuItem";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            resources.ApplyResources(this.toolStripMenuItem7, "toolStripMenuItem7");
            // 
            // jumpToLineToolStripMenuItem
            // 
            this.jumpToLineToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.jumpToLineToolStripMenuItem, "jumpToLineToolStripMenuItem");
            this.jumpToLineToolStripMenuItem.Name = "jumpToLineToolStripMenuItem";
            // 
            // goToDefinitionToolStripMenuItem
            // 
            this.goToDefinitionToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.goToDefinitionToolStripMenuItem, "goToDefinitionToolStripMenuItem");
            this.goToDefinitionToolStripMenuItem.Name = "goToDefinitionToolStripMenuItem";
            // 
            // renameVariableToolStripMenuItem
            // 
            this.renameVariableToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.renameVariableToolStripMenuItem.Name = "renameVariableToolStripMenuItem";
            resources.ApplyResources(this.renameVariableToolStripMenuItem, "renameVariableToolStripMenuItem");
            // 
            // jumpToBreakpointToolStripMenuItem
            // 
            this.jumpToBreakpointToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.jumpToBreakpointToolStripMenuItem, "jumpToBreakpointToolStripMenuItem");
            this.jumpToBreakpointToolStripMenuItem.Name = "jumpToBreakpointToolStripMenuItem";
            // 
            // jumpToBookmarkToolStripMenuItem
            // 
            this.jumpToBookmarkToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.jumpToBookmarkToolStripMenuItem, "jumpToBookmarkToolStripMenuItem");
            this.jumpToBookmarkToolStripMenuItem.Name = "jumpToBookmarkToolStripMenuItem";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            resources.ApplyResources(this.toolStripMenuItem8, "toolStripMenuItem8");
            // 
            // findToolStripMenuItem
            // 
            this.findToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.findToolStripMenuItem, "findToolStripMenuItem");
            this.findToolStripMenuItem.Name = "findToolStripMenuItem";
            // 
            // replaceToolStripMenuItem
            // 
            this.replaceToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.replaceToolStripMenuItem, "replaceToolStripMenuItem");
            this.replaceToolStripMenuItem.Name = "replaceToolStripMenuItem";
            // 
            // advancedFindToolStripMenuItem
            // 
            this.advancedFindToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.advancedFindToolStripMenuItem, "advancedFindToolStripMenuItem");
            this.advancedFindToolStripMenuItem.Name = "advancedFindToolStripMenuItem";
            // 
            // vIEWToolStripMenuItem
            // 
            this.vIEWToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.convertedCodeToolStripMenuItem,
            this.logicSchemeToolStripMenuItem,
            this.toolStripMenuItem9,
            this.zoomToolStripMenuItem,
            this.toolStripMenuItem16,
            this.statisticsToolStripMenuItem});
            this.vIEWToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.vIEWToolStripMenuItem.Name = "vIEWToolStripMenuItem";
            resources.ApplyResources(this.vIEWToolStripMenuItem, "vIEWToolStripMenuItem");
            // 
            // convertedCodeToolStripMenuItem
            // 
            this.convertedCodeToolStripMenuItem.Checked = true;
            this.convertedCodeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.convertedCodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.convertedCodeToolStripMenuItem.Name = "convertedCodeToolStripMenuItem";
            resources.ApplyResources(this.convertedCodeToolStripMenuItem, "convertedCodeToolStripMenuItem");
            // 
            // logicSchemeToolStripMenuItem
            // 
            this.logicSchemeToolStripMenuItem.Checked = true;
            this.logicSchemeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.logicSchemeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.logicSchemeToolStripMenuItem.Name = "logicSchemeToolStripMenuItem";
            resources.ApplyResources(this.logicSchemeToolStripMenuItem, "logicSchemeToolStripMenuItem");
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            resources.ApplyResources(this.toolStripMenuItem9, "toolStripMenuItem9");
            // 
            // zoomToolStripMenuItem
            // 
            this.zoomToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zoomInToolStripMenuItem,
            this.zoomOutToolStripMenuItem,
            this.toolStripMenuItem10,
            this.toolStripMenuItem17,
            this.toolStripMenuItem18,
            this.toolStripMenuItem19,
            this.toolStripMenuItem12,
            this.toolStripMenuItem13,
            this.toolStripMenuItem14,
            this.toolStripMenuItem15,
            this.toolStripMenuItem11,
            this.customizeToolStripMenuItem});
            this.zoomToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.zoomToolStripMenuItem.Name = "zoomToolStripMenuItem";
            resources.ApplyResources(this.zoomToolStripMenuItem, "zoomToolStripMenuItem");
            // 
            // zoomInToolStripMenuItem
            // 
            this.zoomInToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.zoomInToolStripMenuItem.Name = "zoomInToolStripMenuItem";
            resources.ApplyResources(this.zoomInToolStripMenuItem, "zoomInToolStripMenuItem");
            // 
            // zoomOutToolStripMenuItem
            // 
            this.zoomOutToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.zoomOutToolStripMenuItem.Name = "zoomOutToolStripMenuItem";
            resources.ApplyResources(this.zoomOutToolStripMenuItem, "zoomOutToolStripMenuItem");
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            resources.ApplyResources(this.toolStripMenuItem10, "toolStripMenuItem10");
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            resources.ApplyResources(this.toolStripMenuItem17, "toolStripMenuItem17");
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            resources.ApplyResources(this.toolStripMenuItem18, "toolStripMenuItem18");
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            resources.ApplyResources(this.toolStripMenuItem19, "toolStripMenuItem19");
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            resources.ApplyResources(this.toolStripMenuItem12, "toolStripMenuItem12");
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            resources.ApplyResources(this.toolStripMenuItem13, "toolStripMenuItem13");
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            resources.ApplyResources(this.toolStripMenuItem14, "toolStripMenuItem14");
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            resources.ApplyResources(this.toolStripMenuItem15, "toolStripMenuItem15");
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            resources.ApplyResources(this.toolStripMenuItem11, "toolStripMenuItem11");
            // 
            // customizeToolStripMenuItem
            // 
            this.customizeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            resources.ApplyResources(this.customizeToolStripMenuItem, "customizeToolStripMenuItem");
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            resources.ApplyResources(this.toolStripMenuItem16, "toolStripMenuItem16");
            // 
            // statisticsToolStripMenuItem
            // 
            this.statisticsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.statisticsToolStripMenuItem, "statisticsToolStripMenuItem");
            this.statisticsToolStripMenuItem.Name = "statisticsToolStripMenuItem";
            // 
            // cONVERTToolStripMenuItem
            // 
            this.cONVERTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toCToolStripMenuItem,
            this.toBasicClassicToolStripMenuItem,
            this.toPascalToolStripMenuItem,
            this.toCToolStripMenuItem1,
            this.toJavaScriptToolStripMenuItem,
            this.toVBScriptToolStripMenuItem,
            this.toolStripMenuItem20,
            this.toLogicSchemeToolStripMenuItem});
            this.cONVERTToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.cONVERTToolStripMenuItem.Name = "cONVERTToolStripMenuItem";
            resources.ApplyResources(this.cONVERTToolStripMenuItem, "cONVERTToolStripMenuItem");
            // 
            // toCToolStripMenuItem
            // 
            this.toCToolStripMenuItem.Checked = true;
            this.toCToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toCToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toCToolStripMenuItem.Name = "toCToolStripMenuItem";
            resources.ApplyResources(this.toCToolStripMenuItem, "toCToolStripMenuItem");
            // 
            // toBasicClassicToolStripMenuItem
            // 
            this.toBasicClassicToolStripMenuItem.Checked = true;
            this.toBasicClassicToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toBasicClassicToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toBasicClassicToolStripMenuItem.Name = "toBasicClassicToolStripMenuItem";
            resources.ApplyResources(this.toBasicClassicToolStripMenuItem, "toBasicClassicToolStripMenuItem");
            // 
            // toPascalToolStripMenuItem
            // 
            this.toPascalToolStripMenuItem.Checked = true;
            this.toPascalToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toPascalToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toPascalToolStripMenuItem.Name = "toPascalToolStripMenuItem";
            resources.ApplyResources(this.toPascalToolStripMenuItem, "toPascalToolStripMenuItem");
            // 
            // toCToolStripMenuItem1
            // 
            this.toCToolStripMenuItem1.Checked = true;
            this.toCToolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toCToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.toCToolStripMenuItem1.Name = "toCToolStripMenuItem1";
            resources.ApplyResources(this.toCToolStripMenuItem1, "toCToolStripMenuItem1");
            // 
            // toJavaScriptToolStripMenuItem
            // 
            this.toJavaScriptToolStripMenuItem.Checked = true;
            this.toJavaScriptToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toJavaScriptToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toJavaScriptToolStripMenuItem.Name = "toJavaScriptToolStripMenuItem";
            resources.ApplyResources(this.toJavaScriptToolStripMenuItem, "toJavaScriptToolStripMenuItem");
            // 
            // toVBScriptToolStripMenuItem
            // 
            this.toVBScriptToolStripMenuItem.Checked = true;
            this.toVBScriptToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toVBScriptToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toVBScriptToolStripMenuItem.Name = "toVBScriptToolStripMenuItem";
            resources.ApplyResources(this.toVBScriptToolStripMenuItem, "toVBScriptToolStripMenuItem");
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            resources.ApplyResources(this.toolStripMenuItem20, "toolStripMenuItem20");
            // 
            // toLogicSchemeToolStripMenuItem
            // 
            this.toLogicSchemeToolStripMenuItem.Checked = true;
            this.toLogicSchemeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toLogicSchemeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toLogicSchemeToolStripMenuItem.Name = "toLogicSchemeToolStripMenuItem";
            resources.ApplyResources(this.toLogicSchemeToolStripMenuItem, "toLogicSchemeToolStripMenuItem");
            // 
            // dEBUGToolStripMenuItem
            // 
            this.dEBUGToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compileAndRunToolStripMenuItem,
            this.justCompileToolStripMenuItem,
            this.startDebuggingToolStripMenuItem,
            this.toolStripMenuItem21,
            this.continueDebuggingToolStripMenuItem,
            this.watchesToolStripMenuItem,
            this.sendCommandToolStripMenuItem,
            this.toolStripMenuItem22,
            this.addremoveBreakpointToolStripMenuItem,
            this.addremoveWatchToolStripMenuItem,
            this.toggleBookmarkToolStripMenuItem,
            this.toolStripMenuItem23,
            this.endPseudocodeToolStripMenuItem,
            this.restartPseudocodeToolStripMenuItem});
            this.dEBUGToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.dEBUGToolStripMenuItem.Name = "dEBUGToolStripMenuItem";
            resources.ApplyResources(this.dEBUGToolStripMenuItem, "dEBUGToolStripMenuItem");
            // 
            // compileAndRunToolStripMenuItem
            // 
            this.compileAndRunToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.compileAndRunToolStripMenuItem, "compileAndRunToolStripMenuItem");
            this.compileAndRunToolStripMenuItem.Name = "compileAndRunToolStripMenuItem";
            // 
            // justCompileToolStripMenuItem
            // 
            this.justCompileToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.justCompileToolStripMenuItem, "justCompileToolStripMenuItem");
            this.justCompileToolStripMenuItem.Name = "justCompileToolStripMenuItem";
            // 
            // startDebuggingToolStripMenuItem
            // 
            this.startDebuggingToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.startDebuggingToolStripMenuItem, "startDebuggingToolStripMenuItem");
            this.startDebuggingToolStripMenuItem.Name = "startDebuggingToolStripMenuItem";
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            resources.ApplyResources(this.toolStripMenuItem21, "toolStripMenuItem21");
            // 
            // continueDebuggingToolStripMenuItem
            // 
            this.continueDebuggingToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.continueDebuggingToolStripMenuItem, "continueDebuggingToolStripMenuItem");
            this.continueDebuggingToolStripMenuItem.Name = "continueDebuggingToolStripMenuItem";
            // 
            // watchesToolStripMenuItem
            // 
            this.watchesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.watchesToolStripMenuItem, "watchesToolStripMenuItem");
            this.watchesToolStripMenuItem.Name = "watchesToolStripMenuItem";
            // 
            // sendCommandToolStripMenuItem
            // 
            this.sendCommandToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.sendCommandToolStripMenuItem, "sendCommandToolStripMenuItem");
            this.sendCommandToolStripMenuItem.Name = "sendCommandToolStripMenuItem";
            // 
            // toolStripMenuItem22
            // 
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            resources.ApplyResources(this.toolStripMenuItem22, "toolStripMenuItem22");
            // 
            // addremoveBreakpointToolStripMenuItem
            // 
            this.addremoveBreakpointToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.addremoveBreakpointToolStripMenuItem, "addremoveBreakpointToolStripMenuItem");
            this.addremoveBreakpointToolStripMenuItem.Name = "addremoveBreakpointToolStripMenuItem";
            // 
            // addremoveWatchToolStripMenuItem
            // 
            this.addremoveWatchToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.addremoveWatchToolStripMenuItem.Name = "addremoveWatchToolStripMenuItem";
            resources.ApplyResources(this.addremoveWatchToolStripMenuItem, "addremoveWatchToolStripMenuItem");
            // 
            // toggleBookmarkToolStripMenuItem
            // 
            this.toggleBookmarkToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.toggleBookmarkToolStripMenuItem, "toggleBookmarkToolStripMenuItem");
            this.toggleBookmarkToolStripMenuItem.Name = "toggleBookmarkToolStripMenuItem";
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            resources.ApplyResources(this.toolStripMenuItem23, "toolStripMenuItem23");
            // 
            // endPseudocodeToolStripMenuItem
            // 
            this.endPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.endPseudocodeToolStripMenuItem, "endPseudocodeToolStripMenuItem");
            this.endPseudocodeToolStripMenuItem.Name = "endPseudocodeToolStripMenuItem";
            // 
            // restartPseudocodeToolStripMenuItem
            // 
            this.restartPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.restartPseudocodeToolStripMenuItem, "restartPseudocodeToolStripMenuItem");
            this.restartPseudocodeToolStripMenuItem.Name = "restartPseudocodeToolStripMenuItem";
            // 
            // tOOLSToolStripMenuItem
            // 
            this.tOOLSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationToolStripMenuItem,
            this.menuSearchToolStripMenuItem,
            this.toolStripMenuItem24,
            this.createApplicationToolStripMenuItem,
            this.insertModuleToolStripMenuItem,
            this.toolStripMenuItem25,
            this.testCurrentConfigurationToolStripMenuItem,
            this.extensionsManagerToolStripMenuItem,
            this.administrativeConfigurationsManagerToolStripMenuItem,
            this.toolStripMenuItem33,
            this.customizeToolbarToolStripMenuItem});
            this.tOOLSToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.tOOLSToolStripMenuItem.Name = "tOOLSToolStripMenuItem";
            resources.ApplyResources(this.tOOLSToolStripMenuItem, "tOOLSToolStripMenuItem");
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.configurationToolStripMenuItem, "configurationToolStripMenuItem");
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // menuSearchToolStripMenuItem
            // 
            this.menuSearchToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.menuSearchToolStripMenuItem.Name = "menuSearchToolStripMenuItem";
            resources.ApplyResources(this.menuSearchToolStripMenuItem, "menuSearchToolStripMenuItem");
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            resources.ApplyResources(this.toolStripMenuItem24, "toolStripMenuItem24");
            // 
            // createApplicationToolStripMenuItem
            // 
            this.createApplicationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.createApplicationToolStripMenuItem, "createApplicationToolStripMenuItem");
            this.createApplicationToolStripMenuItem.Name = "createApplicationToolStripMenuItem";
            // 
            // insertModuleToolStripMenuItem
            // 
            this.insertModuleToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.insertModuleToolStripMenuItem, "insertModuleToolStripMenuItem");
            this.insertModuleToolStripMenuItem.Name = "insertModuleToolStripMenuItem";
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            resources.ApplyResources(this.toolStripMenuItem25, "toolStripMenuItem25");
            // 
            // testCurrentConfigurationToolStripMenuItem
            // 
            this.testCurrentConfigurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.testCurrentConfigurationToolStripMenuItem.Name = "testCurrentConfigurationToolStripMenuItem";
            resources.ApplyResources(this.testCurrentConfigurationToolStripMenuItem, "testCurrentConfigurationToolStripMenuItem");
            // 
            // extensionsManagerToolStripMenuItem
            // 
            this.extensionsManagerToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.extensionsManagerToolStripMenuItem.Name = "extensionsManagerToolStripMenuItem";
            resources.ApplyResources(this.extensionsManagerToolStripMenuItem, "extensionsManagerToolStripMenuItem");
            // 
            // administrativeConfigurationsManagerToolStripMenuItem
            // 
            this.administrativeConfigurationsManagerToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.administrativeConfigurationsManagerToolStripMenuItem.Name = "administrativeConfigurationsManagerToolStripMenuItem";
            resources.ApplyResources(this.administrativeConfigurationsManagerToolStripMenuItem, "administrativeConfigurationsManagerToolStripMenuItem");
            // 
            // toolStripMenuItem33
            // 
            this.toolStripMenuItem33.Name = "toolStripMenuItem33";
            resources.ApplyResources(this.toolStripMenuItem33, "toolStripMenuItem33");
            // 
            // customizeToolbarToolStripMenuItem
            // 
            this.customizeToolbarToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.customizeToolbarToolStripMenuItem.Name = "customizeToolbarToolStripMenuItem";
            resources.ApplyResources(this.customizeToolbarToolStripMenuItem, "customizeToolbarToolStripMenuItem");
            this.customizeToolbarToolStripMenuItem.Click += new System.EventHandler(this.customizeToolbarToolStripMenuItem_Click);
            // 
            // wINDOWToolStripMenuItem
            // 
            this.wINDOWToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.wINDOWToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newWindowToolStripMenuItem,
            this.toolStripMenuItem28,
            this.closeAllTabsToolStripMenuItem,
            this.closeCurrentTabToolStripMenuItem,
            this.toolStripMenuItem26,
            this.casacdeWindowsToolStripMenuItem,
            this.tileWindowsToolStripMenuItem,
            this.showWindowsSideBySideToolStripMenuItem,
            this.toolStripMenuItem27,
            this.restoreDownToolStripMenuItem,
            this.moveToolStripMenuItem,
            this.sizeToolStripMenuItem,
            this.minimizeToolStripMenuItem,
            this.maximizeToolStripMenuItem,
            this.toolStripMenuItem29});
            this.wINDOWToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.wINDOWToolStripMenuItem.Name = "wINDOWToolStripMenuItem";
            resources.ApplyResources(this.wINDOWToolStripMenuItem, "wINDOWToolStripMenuItem");
            // 
            // newWindowToolStripMenuItem
            // 
            this.newWindowToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.newWindowToolStripMenuItem.Name = "newWindowToolStripMenuItem";
            resources.ApplyResources(this.newWindowToolStripMenuItem, "newWindowToolStripMenuItem");
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            resources.ApplyResources(this.toolStripMenuItem28, "toolStripMenuItem28");
            // 
            // closeAllTabsToolStripMenuItem
            // 
            this.closeAllTabsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.closeAllTabsToolStripMenuItem.Name = "closeAllTabsToolStripMenuItem";
            resources.ApplyResources(this.closeAllTabsToolStripMenuItem, "closeAllTabsToolStripMenuItem");
            // 
            // closeCurrentTabToolStripMenuItem
            // 
            this.closeCurrentTabToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.closeCurrentTabToolStripMenuItem.Name = "closeCurrentTabToolStripMenuItem";
            resources.ApplyResources(this.closeCurrentTabToolStripMenuItem, "closeCurrentTabToolStripMenuItem");
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            resources.ApplyResources(this.toolStripMenuItem26, "toolStripMenuItem26");
            // 
            // casacdeWindowsToolStripMenuItem
            // 
            this.casacdeWindowsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.casacdeWindowsToolStripMenuItem.Name = "casacdeWindowsToolStripMenuItem";
            resources.ApplyResources(this.casacdeWindowsToolStripMenuItem, "casacdeWindowsToolStripMenuItem");
            // 
            // tileWindowsToolStripMenuItem
            // 
            this.tileWindowsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.tileWindowsToolStripMenuItem.Name = "tileWindowsToolStripMenuItem";
            resources.ApplyResources(this.tileWindowsToolStripMenuItem, "tileWindowsToolStripMenuItem");
            // 
            // showWindowsSideBySideToolStripMenuItem
            // 
            this.showWindowsSideBySideToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.showWindowsSideBySideToolStripMenuItem.Name = "showWindowsSideBySideToolStripMenuItem";
            resources.ApplyResources(this.showWindowsSideBySideToolStripMenuItem, "showWindowsSideBySideToolStripMenuItem");
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            resources.ApplyResources(this.toolStripMenuItem27, "toolStripMenuItem27");
            // 
            // restoreDownToolStripMenuItem
            // 
            this.restoreDownToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.restoreDownToolStripMenuItem.Name = "restoreDownToolStripMenuItem";
            resources.ApplyResources(this.restoreDownToolStripMenuItem, "restoreDownToolStripMenuItem");
            // 
            // moveToolStripMenuItem
            // 
            this.moveToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.moveToolStripMenuItem.Name = "moveToolStripMenuItem";
            resources.ApplyResources(this.moveToolStripMenuItem, "moveToolStripMenuItem");
            // 
            // sizeToolStripMenuItem
            // 
            this.sizeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.sizeToolStripMenuItem.Name = "sizeToolStripMenuItem";
            resources.ApplyResources(this.sizeToolStripMenuItem, "sizeToolStripMenuItem");
            // 
            // minimizeToolStripMenuItem
            // 
            this.minimizeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.minimizeToolStripMenuItem.Name = "minimizeToolStripMenuItem";
            resources.ApplyResources(this.minimizeToolStripMenuItem, "minimizeToolStripMenuItem");
            // 
            // maximizeToolStripMenuItem
            // 
            this.maximizeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.maximizeToolStripMenuItem.Name = "maximizeToolStripMenuItem";
            resources.ApplyResources(this.maximizeToolStripMenuItem, "maximizeToolStripMenuItem");
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            resources.ApplyResources(this.toolStripMenuItem29, "toolStripMenuItem29");
            // 
            // hELPToolStripMenuItem
            // 
            this.hELPToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.hELPToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpSupportAndTipsToolStripMenuItem,
            this.toolStripMenuItem30,
            this.contextualHelpToolStripMenuItem,
            this.whatsThisToolStripMenuItem,
            this.onlineAssistanceToolStripMenuItem,
            this.valiNetOnTheInternetToolStripMenuItem,
            this.toolStripMenuItem31,
            this.checkForUpdatesToolStripMenuItem,
            this.valiNetCustomerExperienceImprovementProgramToolStripMenuItem,
            this.licenseAgreementToolStripMenuItem,
            this.orderPseudocodeOnADVDToolStripMenuItem,
            this.toolStripMenuItem32,
            this.aboutToolStripMenuItem});
            this.hELPToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.hELPToolStripMenuItem.Name = "hELPToolStripMenuItem";
            resources.ApplyResources(this.hELPToolStripMenuItem, "hELPToolStripMenuItem");
            // 
            // helpSupportAndTipsToolStripMenuItem
            // 
            this.helpSupportAndTipsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.helpSupportAndTipsToolStripMenuItem, "helpSupportAndTipsToolStripMenuItem");
            this.helpSupportAndTipsToolStripMenuItem.Name = "helpSupportAndTipsToolStripMenuItem";
            // 
            // toolStripMenuItem30
            // 
            this.toolStripMenuItem30.Name = "toolStripMenuItem30";
            resources.ApplyResources(this.toolStripMenuItem30, "toolStripMenuItem30");
            // 
            // contextualHelpToolStripMenuItem
            // 
            this.contextualHelpToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.contextualHelpToolStripMenuItem.Name = "contextualHelpToolStripMenuItem";
            resources.ApplyResources(this.contextualHelpToolStripMenuItem, "contextualHelpToolStripMenuItem");
            // 
            // whatsThisToolStripMenuItem
            // 
            this.whatsThisToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.whatsThisToolStripMenuItem.Name = "whatsThisToolStripMenuItem";
            resources.ApplyResources(this.whatsThisToolStripMenuItem, "whatsThisToolStripMenuItem");
            // 
            // onlineAssistanceToolStripMenuItem
            // 
            this.onlineAssistanceToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.onlineAssistanceToolStripMenuItem.Name = "onlineAssistanceToolStripMenuItem";
            resources.ApplyResources(this.onlineAssistanceToolStripMenuItem, "onlineAssistanceToolStripMenuItem");
            // 
            // valiNetOnTheInternetToolStripMenuItem
            // 
            this.valiNetOnTheInternetToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.valiNetOnTheInternetToolStripMenuItem.Name = "valiNetOnTheInternetToolStripMenuItem";
            resources.ApplyResources(this.valiNetOnTheInternetToolStripMenuItem, "valiNetOnTheInternetToolStripMenuItem");
            // 
            // toolStripMenuItem31
            // 
            this.toolStripMenuItem31.Name = "toolStripMenuItem31";
            resources.ApplyResources(this.toolStripMenuItem31, "toolStripMenuItem31");
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.checkForUpdatesToolStripMenuItem, "checkForUpdatesToolStripMenuItem");
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            // 
            // valiNetCustomerExperienceImprovementProgramToolStripMenuItem
            // 
            this.valiNetCustomerExperienceImprovementProgramToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.valiNetCustomerExperienceImprovementProgramToolStripMenuItem.Name = "valiNetCustomerExperienceImprovementProgramToolStripMenuItem";
            resources.ApplyResources(this.valiNetCustomerExperienceImprovementProgramToolStripMenuItem, "valiNetCustomerExperienceImprovementProgramToolStripMenuItem");
            // 
            // licenseAgreementToolStripMenuItem
            // 
            this.licenseAgreementToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.licenseAgreementToolStripMenuItem.Name = "licenseAgreementToolStripMenuItem";
            resources.ApplyResources(this.licenseAgreementToolStripMenuItem, "licenseAgreementToolStripMenuItem");
            // 
            // orderPseudocodeOnADVDToolStripMenuItem
            // 
            this.orderPseudocodeOnADVDToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.orderPseudocodeOnADVDToolStripMenuItem.Name = "orderPseudocodeOnADVDToolStripMenuItem";
            resources.ApplyResources(this.orderPseudocodeOnADVDToolStripMenuItem, "orderPseudocodeOnADVDToolStripMenuItem");
            // 
            // toolStripMenuItem32
            // 
            this.toolStripMenuItem32.Name = "toolStripMenuItem32";
            resources.ApplyResources(this.toolStripMenuItem32, "toolStripMenuItem32");
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            // 
            // transparentControl1
            // 
            resources.ApplyResources(this.transparentControl1, "transparentControl1");
            this.transparentControl1.Name = "transparentControl1";
            // 
            // Top
            // 
            resources.ApplyResources(this.Top, "Top");
            this.Top.Name = "Top";
            // 
            // Right
            // 
            resources.ApplyResources(this.Right, "Right");
            this.Right.Name = "Right";
            // 
            // Left
            // 
            resources.ApplyResources(this.Left, "Left");
            this.Left.Name = "Left";
            // 
            // Bottom
            // 
            resources.ApplyResources(this.Bottom, "Bottom");
            this.Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Bottom.Name = "Bottom";
            // 
            // dockPanel
            // 
            resources.ApplyResources(this.dockPanel, "dockPanel");
            this.dockPanel.Name = "dockPanel";
            dockPanelGradient1.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dockPanelGradient1.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            tabGradient1.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            tabGradient1.TextColor = System.Drawing.Color.White;
            autoHideStripSkin1.TabGradient = tabGradient1;
            autoHideStripSkin1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            tabGradient2.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            tabGradient2.TextColor = System.Drawing.Color.White;
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.Color.Black;
            dockPanelGradient2.StartColor = System.Drawing.Color.Black;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            tabGradient3.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            tabGradient3.TextColor = System.Drawing.Color.White;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            dockPaneStripSkin1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.SystemColors.Control;
            tabGradient5.StartColor = System.Drawing.SystemColors.Control;
            tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.Color.Black;
            dockPanelGradient3.StartColor = System.Drawing.Color.Black;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.SystemColors.InactiveCaption;
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.TextColor = System.Drawing.SystemColors.InactiveCaptionText;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.Transparent;
            tabGradient7.StartColor = System.Drawing.Color.Transparent;
            tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            this.dockPanel.Skin = dockPanelSkin1;
            this.dockPanel.SupportDeeplyNestedContent = true;
            this.dockPanel.ActiveContentChanged += new System.EventHandler(this.dockPanel_ActiveContentChanged);
            this.dockPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.dockPanel_Paint);
            // 
            // TopCover
            // 
            resources.ApplyResources(this.TopCover, "TopCover");
            this.TopCover.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TopCover.Name = "TopCover";
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Appearance = this.appearanceControl1;
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            resources.ApplyResources(this.toolStripStatusLabel1, "toolStripStatusLabel1");
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            // 
            // BottomDown
            // 
            resources.ApplyResources(this.BottomDown, "BottomDown");
            this.BottomDown.BackColor = System.Drawing.Color.Black;
            this.BottomDown.Name = "BottomDown";
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            resources.ApplyResources(this.listBox1, "listBox1");
            this.listBox1.ForeColor = System.Drawing.Color.White;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Name = "listBox1";
            this.listBox1.Click += new System.EventHandler(this.listBox1_Click);
            this.listBox1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBox1_DrawItem);
            this.listBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.listBox1_KeyUp);
            this.listBox1.Leave += new System.EventHandler(this.listBox1_Leave);
            this.listBox1.MouseEnter += new System.EventHandler(this.listBox1_MouseEnter);
            this.listBox1.MouseLeave += new System.EventHandler(this.listBox1_MouseLeave);
            this.listBox1.MouseHover += new System.EventHandler(this.listBox1_MouseHover);
            this.listBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseMove);
            // 
            // SearchPanel
            // 
            resources.ApplyResources(this.SearchPanel, "SearchPanel");
            this.SearchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.SearchPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SearchPanel.Controls.Add(this.panel1);
            this.SearchPanel.Controls.Add(this.panel2);
            this.SearchPanel.Controls.Add(this.SearchText);
            this.SearchPanel.Controls.Add(this.listBox1);
            this.SearchPanel.Name = "SearchPanel";
            this.SearchPanel.VisibleChanged += new System.EventHandler(this.SearchPanel_VisibleChanged);
            this.SearchPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.SearchPanel_Paint);
            this.SearchPanel.Enter += new System.EventHandler(this.SearchPanel_Enter);
            this.SearchPanel.Leave += new System.EventHandler(this.SearchPanel_Leave);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // SearchText
            // 
            resources.ApplyResources(this.SearchText, "SearchText");
            this.SearchText.LinkColor = System.Drawing.Color.White;
            this.SearchText.Name = "SearchText";
            this.SearchText.TabStop = true;
            this.SearchText.Tag = "Rezultatele căutării";
            // 
            // appearanceControl2
            // 
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intBackground = -16273;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intBorderHighlight = -13410648;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientBegin = -8294;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientEnd = -22964;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientMiddle = -15500;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intHighlight = -3878683;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intPressedBackground = -98242;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intSelectedBackground = -98242;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intBorder = -16777088;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intBorderHighlight = -13410648;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientBegin = -98242;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientEnd = -8294;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientMiddle = -20115;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intHighlight = -6771246;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorder = -16777088;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorderHighlight = -16777088;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientBegin = -34;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientEnd = -13432;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientMiddle = -7764;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intHighlight = -3878683;
            this.appearanceControl2.CustomAppearance.GripAppearance.intDark = -14204554;
            this.appearanceControl2.CustomAppearance.GripAppearance.intLight = -1;
            this.appearanceControl2.CustomAppearance.GripAppearance.Light = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(254)))));
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(254)))));
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(254)))));
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.intGradientBegin = -854786;
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.intGradientEnd = -854786;
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.intGradientMiddle = -854786;
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Revealed.intGradientBegin = -3416586;
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Revealed.intGradientEnd = -9266217;
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Revealed.intGradientMiddle = -6175239;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(195)))), ((int)(((byte)(101)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intBorder = -1719451;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intPressedGradientBegin = -1380097;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intPressedGradientEnd = -1380097;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intPressedGradientMiddle = -1380097;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intSelected = -133953;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intSelectedGradientBegin = -133953;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intSelectedGradientEnd = -133953;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.PressedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.PressedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.PressedGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.Selected = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(244)))), ((int)(((byte)(191)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.SelectedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(244)))), ((int)(((byte)(191)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.SelectedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(244)))), ((int)(((byte)(191)))));
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.intBorder = -2696215;
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.intGradientBegin = -2696215;
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.intGradientEnd = -2696215;
            this.appearanceControl2.CustomAppearance.OverflowButtonAppearance.intGradientBegin = -8408582;
            this.appearanceControl2.CustomAppearance.OverflowButtonAppearance.intGradientEnd = -16763503;
            this.appearanceControl2.CustomAppearance.OverflowButtonAppearance.intGradientMiddle = -11370544;
            this.appearanceControl2.CustomAppearance.RaftingContainerAppearance.intGradientBegin = -6373643;
            this.appearanceControl2.CustomAppearance.RaftingContainerAppearance.intGradientEnd = -3876102;
            this.appearanceControl2.CustomAppearance.SeparatorAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(195)))), ((int)(((byte)(203)))));
            this.appearanceControl2.CustomAppearance.SeparatorAppearance.intDark = -4275253;
            this.appearanceControl2.CustomAppearance.SeparatorAppearance.intLight = -919041;
            this.appearanceControl2.CustomAppearance.StatusStripAppearance.intGradientBegin = -6373643;
            this.appearanceControl2.CustomAppearance.StatusStripAppearance.intGradientEnd = -3876102;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intBorder = -12885604;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intContentPanelGradientBegin = -6373643;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intContentPanelGradientEnd = -3876102;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intDropDownBackground = -592138;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intGradientBegin = -1839105;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intGradientEnd = -8674080;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intGradientMiddle = -3415556;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intPanelGradientBegin = -6373643;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intPanelGradientEnd = -3876102;
            this.appearanceControl2.Preset = AppearanceControl.enumPresetStyles.Custom;
            this.appearanceControl2.Renderer.RoundedEdges = true;
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.SearchPanel);
            this.Controls.Add(this.BottomDown);
            this.Controls.Add(this.Bottom);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.TopCover);
            this.Controls.Add(this.Right);
            this.Controls.Add(this.Left);
            this.Controls.Add(this.Top);
            this.Controls.Add(this.dockPanel);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.RestoreWhite);
            this.Controls.Add(this.MaximizeWhite);
            this.Controls.Add(this.transparentControl1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Deactivate += new System.EventHandler(this.Form1_Deactivate);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.SearchPanel.ResumeLayout(false);
            this.SearchPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MainMenu mainMenu1;
        private AppearanceControl appearanceControl1;
        private TransparentControl transparentControl1;
        private CustomizableMenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveLogicSchemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openPseudocodeUsingAcodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharePseudocodeUsingAcodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem printPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printConvertedCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printLogicSchemeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem recoverLostunsavedFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem removePersonalInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem codeboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eDITToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoActionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem jumpToLineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jumpToBreakpointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jumpToBookmarkToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem findToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem replaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancedFindToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vIEWToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertedCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logicSchemeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem zoomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem statisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cONVERTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toBasicClassicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toPascalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toJavaScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toVBScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem toLogicSchemeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dEBUGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compileAndRunToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem justCompileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startDebuggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem continueDebuggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem watchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendCommandToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem addremoveBreakpointToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem endPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tOOLSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem createApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertModuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem25;
        private System.Windows.Forms.ToolStripMenuItem testCurrentConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extensionsManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrativeConfigurationsManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wINDOWToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem closeAllTabsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeCurrentTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem casacdeWindowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tileWindowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showWindowsSideBySideToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem restoreDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maximizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem29;
        private AppearanceControl appearanceControl2;
        private System.Windows.Forms.Button MaximizeWhite;
        private System.Windows.Forms.Button RestoreWhite;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox SearchBox;
        private CustomizableToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.ToolStripButton toolStripButton12;
        private System.Windows.Forms.ToolStripButton toolStripButton13;
        private System.Windows.Forms.ToolStripButton toolStripButton14;
        private System.Windows.Forms.ToolStripButton toolStripButton15;
        private System.Windows.Forms.ToolStripButton toolStripButton16;
        private System.Windows.Forms.ToolStripButton toolStripButton19;
        private System.Windows.Forms.ToolStripButton toolStripButton20;
        private System.Windows.Forms.ToolStripButton toolStripButton21;
        private System.Windows.Forms.ToolStripButton toolStripButton22;
        private System.Windows.Forms.ToolStripButton toolStripButton23;
        private System.Windows.Forms.ToolStripButton toolStripButton24;
        private System.Windows.Forms.ToolStripButton toolStripButton25;
        private System.Windows.Forms.ToolStripButton toolStripButton26;
        private System.Windows.Forms.ToolStripButton toolStripButton27;
        private System.Windows.Forms.ToolStripButton toolStripButton28;
        private System.Windows.Forms.ToolStripButton toolStripButton29;
        private System.Windows.Forms.ToolStripButton toolStripButton30;
        private System.Windows.Forms.ToolStripButton toolStripButton31;
        private System.Windows.Forms.ToolStripButton toolStripButton32;
        private System.Windows.Forms.ToolStripButton toolStripButton33;
        private System.Windows.Forms.ToolStripButton toolStripButton34;
        private System.Windows.Forms.ToolStripButton toolStripButton35;
        private System.Windows.Forms.ToolStripButton toolStripButton36;
        private System.Windows.Forms.ToolStripButton toolStripButton37;
        private System.Windows.Forms.ToolStripButton toolStripButton38;
        private System.Windows.Forms.ToolStripButton toolStripButton39;
        private System.Windows.Forms.ToolStripButton toolStripButton40;
        private System.Windows.Forms.ToolStripButton toolStripButton41;
        private System.Windows.Forms.ToolStripButton toolStripButton42;
        private System.Windows.Forms.ToolStripButton toolStripButton43;
        private System.Windows.Forms.ToolStripButton toolStripButton44;
        private System.Windows.Forms.ToolStripMenuItem toggleBookmarkToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripButton17;
        private System.Windows.Forms.ToolStripDropDownButton toolStripButton18;
        private System.Windows.Forms.ToolStripButton toolStripButton45;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem33;
        private System.Windows.Forms.ToolStripMenuItem customizeToolbarToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel Top;
        private System.Windows.Forms.Panel Left;
        private System.Windows.Forms.Panel Right;
        private System.Windows.Forms.Panel Bottom;
        private System.Windows.Forms.ToolStripMenuItem renameVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addremoveWatchToolStripMenuItem;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
        private System.Windows.Forms.Panel TopCover;
        private System.Windows.Forms.ToolStripMenuItem hELPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpSupportAndTipsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem30;
        private System.Windows.Forms.ToolStripMenuItem contextualHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem whatsThisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlineAssistanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem valiNetOnTheInternetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem31;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem valiNetCustomerExperienceImprovementProgramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem licenseAgreementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderPseudocodeOnADVDToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem32;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private CustomizableStatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Panel BottomDown;
        private Controls.Development.ImageListBox listBox1;
        private System.Windows.Forms.Panel SearchPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.LinkLabel SearchText;
        private System.Windows.Forms.Panel panel1;
    }
}

