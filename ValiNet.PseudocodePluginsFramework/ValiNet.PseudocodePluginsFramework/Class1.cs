﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValiNet.PseudocodePluginsFramework
{
    public interface VPlugin
    {
        string Name { get; }
        string AssemblyName { get; }
        void About();
        void ExtensionOptions();
        string Perform(string actionID, string contextInformation);
    }
}
