// Creat pentru Pseudocod 2014
// Actualizat la 08.11.2013
// Drepturi de autor (C) 2006-2014 ValiNet Romania.  Toate drepturile rezervate.
// 
// Aplicația Pseudocod, conceptul, codul sursă, ideile, iconița ^_^, Codeboard™ și
// orice alte materiale adiționale furnizate împreună cu acest produs sunt proprietatea
// ValiNet Romania sau a respectivilor lor autori și sunt folosite sub licență.
// 
// Consultați Opțiuni - Despre pentru mai multe detalii.
// 
// Tocmai ați învățat despre una din funcțiile editorului: comentariile - introduceți
// două slash-uri la începutul fiecărui rând, apoi scrieți orice doriți. Executorul
// nu va lua în considerare acea linie.
// 
// În continuare, acest cod a fost comentat ori de câte ori s-a considerat necesar
// pentru a se explica funcționalitatea acestuia. După ce parcurgeți acest pseudocod,
// apăsați butonul Rulare sau tasta F5 sau tasta F9 pentru a-l executa pe computer,
// ca și cum ar fi o aplicație.
// 
// I) Instrucțiunile de bază
//
// Există mai multe instrucțiuni cu care sigur veți lucra în pseudocod: instrucțiunile
// de bază, denumite și instrucțiuni fundamentale. Acestea sunt declarațiile de variabile
// și instrucțiunile citeste si scrie. Pentru detalii despre sintaxa lor, consultați
// Biblioteca de asistență a Pseudocod (Apăsați F1 pentru a o deschide oricând).
// 
// Mai jos este exemplificată declararea unui întreg cu numele a:
intreg a
// Acum, se declara si un numar real in pseudocod, cu numele b:
real b
// Pana acum, nimic nu s-ar fi afisat pe ecran daca pseudocodul ar fi fost executat.
// Aceste variabile au fost doar declarate, nu si utilizate. În continuare, le vom
// atribui valori pe care utilizatorul le va introduce de la tastatură. Pentru a face
// acest lucru, folosim instrucțiunea citeste
citeste a, b
// Observatie: Daca nu s-au introdus de la tastatura numere, executia pseudocodului
// se va opri aici cu o eroare: s-a incercat atribuirea unor valori invalide
// intregului a si realului b.
// 
// Puteam, de asemenea, sa scriem doua instructiuni citeste, una pentru fiecare
// variabila. Nu este nicio diferenta; puteti incerca si aceasta varianta. Decomentati
// urmatoarele doua linii si comentati ultima linie de cod pentru a observa comportamentul.
//citeste a
//citeste b
// Acum ca variabilele a si b au fost atribuite, haideti sa le afisam utilizatorului.
// Pentru asta folosim instructiunea scrie:
scrie a, b
// De asmenea, puteam folosi instructiunea in modul urmator, cu acelasi efect:
//scrie a
//scrie b
// Felicitari, ati invatat instructiunile de baza din Pseudocod.
// 
// II) Operatii aritmetice
// 
// In mod evident, in pseudocod se pot face foarte usor operatii aritmetice.
// Mai jos, vom afisa pe ecran pe rand rezultatul unor tuturor operatiilor aritmetice
// suportate de Pseudocod, aplicate pe numrele b si a. Rezultatul va fi stocat intr-o
// variabila noua, c.
// 
real c
// 1. Adunarea
c = b + a
scrie "Rezultat adunare: " + c
// 2. Scaderea
c = b - a
scrie "Rezultat scadere: " + c
// 3. Inmultirea
c = b * a
scrie "Rezultat inmultire: " + c
// 4. Impartirea
c = b div a
scrie "Rezultat impartire: " + c
// Observatie: Daca a este 0, atunci executia se va termina aici, iar Pseudocod va afisa
// o eroare. Impartirea cu 0 nu exista, deci executia pseudocodului se va opri.
// 5. Partea intreaga
c = [b]
scrie "Rezultat parte intreaga: " + c
// Nu mai este nevoie de [a], deoarece a este numar intreg.
//6. Ridicarea la putere
c = b ^ a
scrie "Rezultat ridicare la putere: " + c
// 7. Restul impartirii lui b la a
c = b mod a
scrie "Rezultat rest: " + c
// Observatie: Daca a este 0, atunci executia se va termina aici, iar Pseudocod va afisa
// o eroare. Impartirea cu 0 nu exista, deci executia pseudocodului se va opri.
// Felicitari, ati invatat operatiile aritmetice de baza din Pseudocod.
// 
// III) Structuri
// 
// Pseudocod suporta patru tipuri de structuri iterative: daca, pentru, cat_timp, executa
// Mai jos este exemplificata folosirea fiecarei instructiuni, exeplicandu-se de asemenea
// comportamentul acestora
// 
// 1. Structura DACA
// Exemplul de mai jos verifica daca a = b si instiinteaza utilizatorul de rezultatul
// verificarii.
daca a = b
    scrie "Numerele " + a + " si " + b + " sunt egale."
sfarsit_daca
// Structura DACA poate fi extinsa cu ALTFEL, care specifica prelucrari ce se vor executa
// daca conditia nu este indeplinita. In exemplul urmator, vom instiinta utilizatorul atat
// daca numerele sunt egale, dar si daca sunt diferite.
daca a = b
    scrie "Numerele " + a + " si " + b + " sunt egale."
altfel
    scrie "Numerele " + a + " si " + b + " nu sunt egale."
sfarsit_daca
// 
// 2. Structura PENTRU
// Exemplul de mai jos adauga 1 la numarul c, care este setat in prealabil la valoarea 0,
// de 10 ori. Rezultatul final va fi 10.
c = 0
pentru i = 1, 10
    c = c + 1
sfarsit_pentru
scrie "Rezultat pentru: " + c
// 
// 3. Structura CAT_TIMP
// Exemplul de mai jos adauga 1 la numarul c, care este setat in prealabil la valoarea 0,
// cat timp c este diferit de 10.
c = 0
cat_timp c != 10
    c = c + 1
sfarsit_cat_timp
scrie "Rezultat cat_timp: " + c
// 
// 4. Structura EXECUTA
// Exemplul de mai jos adauga 1 la numarul c, care este setat in prealabil la valoarea 0,
// pana cand c va fi egal cu 10.
c = 0
executa
    c = c + 1
pana_cand c = 10
scrie "Rezultat executa: " + c
// 
// Felicitari, ati invatat despre structurile din Pseudocod.
// In continuare, va invitam sa incercati un exemplu de cod de la Depozitul de Cod (faceti
// click sau atingeti Deschidere, apoi alegeti Depozitul de Cod) sau sa creati chiar dvs.
// un pseudocod nou (apasati sau atingeti butonul Nou din bara de comenzi).
scrie "Multumim pentru parcurgerea acestui ghid pentru invatarea tehnicilor de baza despre limbajul pseudocod."

<properties>
language:1