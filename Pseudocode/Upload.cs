﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLimit.CloudComputing.SharpBox;
using AppLimit.CloudComputing.SharpBox.StorageProvider.DropBox;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Pseudocode
{
    public partial class Upload : Form
    {
        string filename;
        string NoBrowserInstalled;
        string AppName;
        string Missing;
        string ServiceUnavailable;
        string Success;
        public Upload(string file)
        {
            InitializeComponent();
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            filename = file;
            if (Properties.Settings.Default.UILang == 0)
            {
                NoBrowserInstalled = "There is no Internet browser installed on this computer. In order to use this feature, a browser has to be installed on your machine. Visit www.browserchoice.eu to download a web browser for your computer if you are based in the European Union or, if located in any other country, visit www.whatbrowser.org.";
                AppName = "Pseudocode";
                Missing = "Some mandatory fields are blank or the CAPTCHA verification failed.\n\nPlease review the issues and try again.";
                ServiceUnavailable = "This service is unavailable right now.\n\nDebug information: ";
                Success = "Your application has been successfully sent.\n\nThank you for enroling your code with the Code Repository!";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                NoBrowserInstalled = "Niciun browser Web nu este instalat pe acest computer.\n\nPentru a utiliza această caracteristică, un browser trebuie să fie instalat pe computer. Pentru a descărca un browser, vizitați www.browserchoice.eu dacă sunteți în Uniunea Europeană sau www.whatbrowser.org dacă sunteți din oricare altă țară.";
                AppName = "Pseudocod";
                Missing = "Câmpuri obligatorii sunt goale sau protecția AntiSpam nu s-a verificat.\n\nRevizuiți problemele și încercați din nou.";
                ServiceUnavailable = "Serviciul nu e disponibil în acest moment.\n\nInformații de debug: ";
                Success = "Aplicația dvs. a fost expediată cu succes.\n\nVă mulțumim pentru colaborarea și sprijinul acordat Depozitului de Cod!";
            }
            if (Properties.Settings.Default.AppName != "")
            {
                AppName = Properties.Settings.Default.AppName;
            }
            string CloseText = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                CloseText = "Close (Alt+F4)";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                CloseText = "Închidere (Alt+F4)";
            }
          //  toolTip1.SetToolTip(Close, CloseText);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Visible = true;
            progressBar1.Visible = true;
            button22.Enabled = false;
            button1.Enabled = false;
            button2.Enabled = false;
            button6.Enabled = false;
            this.ControlBox = false;
            backgroundWorker1.RunWorkerAsync();
        }
        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                var dropBoxStorage = new CloudStorage();
                var dropBoxConfig = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox);
                ICloudStorageAccessToken accessToken;
                StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                string t = sr.ReadToEnd();
                sr.Close();
                StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                sw.Write(EncryptDecrypt.DecryptString(t));
                sw.Close();
                using (var fs = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat", FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    accessToken = dropBoxStorage.DeserializeSecurityToken(fs);
                }
                StreamWriter sww = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                sww.Write(t);
                sww.Close();
                var storageToken = dropBoxStorage.Open(dropBoxConfig, accessToken);
                //pictureBox2.Image.Dispose();
                //File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic");
                dropBoxStorage.UploadFile(filename, "/");
                DoOnUIThread(delegate()
    {
        this.Close();
    });
            }
            catch
            {
                DoOnUIThread(delegate()
                {
                    label1.Visible = false;
                    progressBar1.Visible = false;
                    button22.Enabled = true;
                    button1.Enabled = true;
                    button2.Enabled = true;
                    button6.Enabled = true;
                });
            }

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process p = Process.Start("https://www.dropbox.com/home/Apps/InfoEdu");
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                {
                    CasetaDeMesaj(this, NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }



        private void button22_Click(object sender, EventArgs e)
        {
            button22.BackColor = Color.DimGray;
            button1.BackColor = Color.Gray;
            panel1.Visible = false;
            this.AcceptButton = button2;
        }
        static string captcha;
        private void Upload_Load(object sender, EventArgs e)
        {
            // Kreiramo bmp file (sliku koja ce se kasnije prikazati u pictureBox1 kontroli).
            Bitmap bmp = new Bitmap(1240, 60);
            Graphics gImage = Graphics.FromImage(bmp);
            gImage.FillRectangle(Brushes.Wheat, 0, 0, bmp.Width, bmp.Height);

            // Odredjujemo font, boju teksta i deklarisemo DrawString metodu.
            Font CAPTCHAfont = new Font("Chiller", 40);
            Brush cetka = Brushes.Black;
            captcha = GetRandomString();
            gImage.DrawString(captcha, CAPTCHAfont, cetka, 0, 0);

            // Dodajemo linije preko teksta, da je (SPAM)programima teze procitati tekst.
            Pen p = new Pen(Color.Black, 3);
            gImage.DrawLine(p, 230, 15, 10, 15);
            gImage.DrawLine(p, 230, 30, 10, 30);
            gImage.DrawLine(p, 230, 45, 10, 45);


            // Dodjeljujemo ime bmp fajlu.
            //string imeFajla = "VALI";

            // Sacuvavamo bmp file.


            //Prikazujemo bmp file u pictureBox kontroli.
            pictureBox1.Image = bmp;
           /* panel1.BackColor = Color.White;
            panel3.BackColor = Color.White;
            button6.BackColor = Color.FromArgb(255, 192, 255);
            button2.BackColor = Color.FromArgb(255, 192, 255);
            button22.BackColor = Color.FromArgb(224, 224, 224);
            button3.BackColor = Color.FromArgb(255, 192, 255);*/
            //Close.BackColor = Color.FromArgb(199, 80, 80);
        }
        public static string GetRandomString()
        {
            string path = Path.GetRandomFileName();
            path = path.Replace(".", ""); // Remove period.
            return path;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Kreiramo bmp file (sliku koja ce se kasnije prikazati u pictureBox1 kontroli).
            Bitmap bmp = new Bitmap(1240, 60);
            Graphics gImage = Graphics.FromImage(bmp);
            gImage.FillRectangle(Brushes.Wheat, 0, 0, bmp.Width, bmp.Height);

            // Odredjujemo font, boju teksta i deklarisemo DrawString metodu.
            Font CAPTCHAfont = new Font("Chiller", 40);
            Brush cetka = Brushes.Black;
            captcha = GetRandomString();
            gImage.DrawString(captcha, CAPTCHAfont, cetka, 0, 0);

            // Dodajemo linije preko teksta, da je (SPAM)programima teze procitati tekst.
            Pen p = new Pen(Color.Black, 3);
            gImage.DrawLine(p, 230, 15, 10, 15);
            gImage.DrawLine(p, 230, 30, 10, 30);
            gImage.DrawLine(p, 230, 45, 10, 45);


            // Dodjeljujemo ime bmp fajlu.
            //string imeFajla = "VALI";

            // Sacuvavamo bmp file.


            //Prikazujemo bmp file u pictureBox kontroli.
            pictureBox1.Image = bmp;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "" | textBox2.Text == "" | textBox3.Text == "" | textBox6.Text == "" | textBox7.Text == "") & textBox6.Text != captcha & !textBox7.Text.Contains('@'))
            {
                CasetaDeMesaj(this, Missing, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                button22.Enabled = false;
                button1.Enabled = false;
                button3.Enabled = false;
                button6.Enabled = false;
                label2.Visible = true;
                progressBar2.Location = new Point(26, 1069);
                progressBar2.Visible = true;
                backgroundWorker2.RunWorkerAsync();
            }
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                GmailAccount acc = new GmailAccount("valinetmail", "vali2011net", textBox1.Text);
                acc.SendMessage("vali@valinet.ro", "[Code Repository Submission] " + textBox1.Text + " by " + textBox2.Text, "Name: " + textBox1.Text + "\r\nAuthor: " + textBox2.Text + "\r\nDescription: " + textBox3.Text + "\r\nAditional information: " + textBox4.Text + "\r\nDependencies: " + textBox5.Text + "\r\n" + "Submitted on: " + DateTime.Now.ToString(), filename);

                DoOnUIThread(delegate()
                {
                    CasetaDeMesaj(this, Success, "Feedback", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                });
            }
            catch (Exception ex)
            {

                DoOnUIThread(delegate()
                {
                    CasetaDeMesaj(this, ServiceUnavailable, "Feedback", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    label2.Visible = false;
                    progressBar2.Visible = false;
                    button22.Enabled = true;
                    button1.Enabled = true;
                    button3.Enabled = true;
                    button6.Enabled = true;
                });
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            button1.BackColor = Color.DimGray;
            button22.BackColor = Color.Gray;
            panel1.Visible = true;
            this.AcceptButton = null;
        }

        private void panel1_MouseEnter(object sender, EventArgs e)
        {
            panel1.Focus();
        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Upload_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }
        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Upload_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
        }
    }
}
