﻿namespace Pseudocode
{
    partial class Config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Config));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.applicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.terminateNowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.killMeWithErrorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crashApplicationonNonUIThreadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rememberPositionAtStartupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languageIntegerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoSaveIntervalIntegerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automaticallySignInAtApplicationStartupBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userInterfaceLanguageIntegerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomLevelIntegerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicationNameIntegerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uiCodeConversionUpdateIntervalIntegerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.welcomeDialogWasShownUponSignInBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateServerStringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.useVisualBarInTheEditorBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalSchemeBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automaticallyRestoreLostFilesIfTheApplicationCrashesBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureIIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attemptToContinueExecutionIfTheApplicationCrashesBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showCommandBarBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableQuickSearchingInTheMenusOfTheMainWindowBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableTheValiNetCrashReporterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicationHasCrashedBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automaticallyPopulateTheFindCtrlFWindowWithDataFromTheEditorBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automaticallyPopulateTheFieldsInTheCreateEXEWizardWithSavedDataBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showConvertedCodeBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attemptToReportTheErrorToMicrosoftErrorReportingInAditionToValiNetBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showStatusBarBolleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showVisualScrollBarBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultLanguageInWhichPseudocodeFilesAreWrittenIntegerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minGWPathStringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedCommandsPanelDimensionsIntegerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableAeroShakeInTheFrameHostWindowBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableFrameHostWindowBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.policyRegardingHowTheApplicationRendersTheUIIntegerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runtimePolicyRegardingWetherTheApplicationEnablesTheImprovedUIBooleanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.experimentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emptyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runtimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emptyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Close = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.menuStrip1.ForeColor = System.Drawing.Color.Black;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applicationToolStripMenuItem,
            this.configureToolStripMenuItem,
            this.configureIIToolStripMenuItem,
            this.experimentsToolStripMenuItem,
            this.runtimeToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Name = "menuStrip1";
            this.toolTip1.SetToolTip(this.menuStrip1, resources.GetString("menuStrip1.ToolTip"));
            // 
            // applicationToolStripMenuItem
            // 
            resources.ApplyResources(this.applicationToolStripMenuItem, "applicationToolStripMenuItem");
            this.applicationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.restartApplicationToolStripMenuItem,
            this.toolStripMenuItem2,
            this.terminateNowToolStripMenuItem,
            this.killMeWithErrorToolStripMenuItem,
            this.crashApplicationonNonUIThreadToolStripMenuItem});
            this.applicationToolStripMenuItem.Name = "applicationToolStripMenuItem";
            // 
            // restartApplicationToolStripMenuItem
            // 
            resources.ApplyResources(this.restartApplicationToolStripMenuItem, "restartApplicationToolStripMenuItem");
            this.restartApplicationToolStripMenuItem.Name = "restartApplicationToolStripMenuItem";
            this.restartApplicationToolStripMenuItem.Click += new System.EventHandler(this.restartApplicationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            // 
            // terminateNowToolStripMenuItem
            // 
            resources.ApplyResources(this.terminateNowToolStripMenuItem, "terminateNowToolStripMenuItem");
            this.terminateNowToolStripMenuItem.Name = "terminateNowToolStripMenuItem";
            this.terminateNowToolStripMenuItem.Click += new System.EventHandler(this.terminateNowToolStripMenuItem_Click);
            // 
            // killMeWithErrorToolStripMenuItem
            // 
            resources.ApplyResources(this.killMeWithErrorToolStripMenuItem, "killMeWithErrorToolStripMenuItem");
            this.killMeWithErrorToolStripMenuItem.Name = "killMeWithErrorToolStripMenuItem";
            this.killMeWithErrorToolStripMenuItem.Click += new System.EventHandler(this.killMeWithErrorToolStripMenuItem_Click);
            // 
            // crashApplicationonNonUIThreadToolStripMenuItem
            // 
            resources.ApplyResources(this.crashApplicationonNonUIThreadToolStripMenuItem, "crashApplicationonNonUIThreadToolStripMenuItem");
            this.crashApplicationonNonUIThreadToolStripMenuItem.Name = "crashApplicationonNonUIThreadToolStripMenuItem";
            this.crashApplicationonNonUIThreadToolStripMenuItem.Click += new System.EventHandler(this.crashApplicationonNonUIThreadToolStripMenuItem_Click);
            // 
            // configureToolStripMenuItem
            // 
            resources.ApplyResources(this.configureToolStripMenuItem, "configureToolStripMenuItem");
            this.configureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rememberPositionAtStartupToolStripMenuItem,
            this.languageIntegerToolStripMenuItem,
            this.autoSaveIntervalIntegerToolStripMenuItem,
            this.automaticallySignInAtApplicationStartupBooleanToolStripMenuItem,
            this.userInterfaceLanguageIntegerToolStripMenuItem,
            this.zoomLevelIntegerToolStripMenuItem,
            this.applicationNameIntegerToolStripMenuItem,
            this.uiCodeConversionUpdateIntervalIntegerToolStripMenuItem,
            this.welcomeDialogWasShownUponSignInBooleanToolStripMenuItem,
            this.warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem,
            this.updateServerStringToolStripMenuItem,
            this.upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem,
            this.downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem,
            this.useVisualBarInTheEditorBooleanToolStripMenuItem,
            this.displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalSchemeBooleanToolStripMenuItem,
            this.showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolStripMenuItem,
            this.automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem,
            this.automaticallyRestoreLostFilesIfTheApplicationCrashesBooleanToolStripMenuItem});
            this.configureToolStripMenuItem.Name = "configureToolStripMenuItem";
            // 
            // rememberPositionAtStartupToolStripMenuItem
            // 
            resources.ApplyResources(this.rememberPositionAtStartupToolStripMenuItem, "rememberPositionAtStartupToolStripMenuItem");
            this.rememberPositionAtStartupToolStripMenuItem.Name = "rememberPositionAtStartupToolStripMenuItem";
            this.rememberPositionAtStartupToolStripMenuItem.Click += new System.EventHandler(this.rememberPositionAtStartupToolStripMenuItem_Click);
            // 
            // languageIntegerToolStripMenuItem
            // 
            resources.ApplyResources(this.languageIntegerToolStripMenuItem, "languageIntegerToolStripMenuItem");
            this.languageIntegerToolStripMenuItem.Name = "languageIntegerToolStripMenuItem";
            this.languageIntegerToolStripMenuItem.Click += new System.EventHandler(this.languageIntegerToolStripMenuItem_Click);
            // 
            // autoSaveIntervalIntegerToolStripMenuItem
            // 
            resources.ApplyResources(this.autoSaveIntervalIntegerToolStripMenuItem, "autoSaveIntervalIntegerToolStripMenuItem");
            this.autoSaveIntervalIntegerToolStripMenuItem.Name = "autoSaveIntervalIntegerToolStripMenuItem";
            this.autoSaveIntervalIntegerToolStripMenuItem.Click += new System.EventHandler(this.autoSaveIntervalIntegerToolStripMenuItem_Click);
            // 
            // automaticallySignInAtApplicationStartupBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.automaticallySignInAtApplicationStartupBooleanToolStripMenuItem, "automaticallySignInAtApplicationStartupBooleanToolStripMenuItem");
            this.automaticallySignInAtApplicationStartupBooleanToolStripMenuItem.Name = "automaticallySignInAtApplicationStartupBooleanToolStripMenuItem";
            this.automaticallySignInAtApplicationStartupBooleanToolStripMenuItem.Click += new System.EventHandler(this.automaticallySignInAtApplicationStartupBooleanToolStripMenuItem_Click);
            // 
            // userInterfaceLanguageIntegerToolStripMenuItem
            // 
            resources.ApplyResources(this.userInterfaceLanguageIntegerToolStripMenuItem, "userInterfaceLanguageIntegerToolStripMenuItem");
            this.userInterfaceLanguageIntegerToolStripMenuItem.Name = "userInterfaceLanguageIntegerToolStripMenuItem";
            this.userInterfaceLanguageIntegerToolStripMenuItem.Click += new System.EventHandler(this.userInterfaceLanguageIntegerToolStripMenuItem_Click);
            // 
            // zoomLevelIntegerToolStripMenuItem
            // 
            resources.ApplyResources(this.zoomLevelIntegerToolStripMenuItem, "zoomLevelIntegerToolStripMenuItem");
            this.zoomLevelIntegerToolStripMenuItem.Name = "zoomLevelIntegerToolStripMenuItem";
            this.zoomLevelIntegerToolStripMenuItem.Click += new System.EventHandler(this.zoomLevelIntegerToolStripMenuItem_Click);
            // 
            // applicationNameIntegerToolStripMenuItem
            // 
            resources.ApplyResources(this.applicationNameIntegerToolStripMenuItem, "applicationNameIntegerToolStripMenuItem");
            this.applicationNameIntegerToolStripMenuItem.Name = "applicationNameIntegerToolStripMenuItem";
            this.applicationNameIntegerToolStripMenuItem.Click += new System.EventHandler(this.applicationNameIntegerToolStripMenuItem_Click);
            // 
            // uiCodeConversionUpdateIntervalIntegerToolStripMenuItem
            // 
            resources.ApplyResources(this.uiCodeConversionUpdateIntervalIntegerToolStripMenuItem, "uiCodeConversionUpdateIntervalIntegerToolStripMenuItem");
            this.uiCodeConversionUpdateIntervalIntegerToolStripMenuItem.Name = "uiCodeConversionUpdateIntervalIntegerToolStripMenuItem";
            this.uiCodeConversionUpdateIntervalIntegerToolStripMenuItem.Click += new System.EventHandler(this.uiCodeConversionUpdateIntervalIntegerToolStripMenuItem_Click);
            // 
            // welcomeDialogWasShownUponSignInBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.welcomeDialogWasShownUponSignInBooleanToolStripMenuItem, "welcomeDialogWasShownUponSignInBooleanToolStripMenuItem");
            this.welcomeDialogWasShownUponSignInBooleanToolStripMenuItem.Name = "welcomeDialogWasShownUponSignInBooleanToolStripMenuItem";
            this.welcomeDialogWasShownUponSignInBooleanToolStripMenuItem.Click += new System.EventHandler(this.welcomeDialogWasShownUponSignInBooleanToolStripMenuItem_Click);
            // 
            // warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem, "warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem");
            this.warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem.Name = "warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem";
            this.warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem.Click += new System.EventHandler(this.warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem_Click);
            // 
            // updateServerStringToolStripMenuItem
            // 
            resources.ApplyResources(this.updateServerStringToolStripMenuItem, "updateServerStringToolStripMenuItem");
            this.updateServerStringToolStripMenuItem.Name = "updateServerStringToolStripMenuItem";
            this.updateServerStringToolStripMenuItem.Click += new System.EventHandler(this.updateServerStringToolStripMenuItem_Click);
            // 
            // upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem, "upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem");
            this.upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem.Name = "upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem";
            this.upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem.Click += new System.EventHandler(this.upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem_Click);
            // 
            // downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem, "downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem");
            this.downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem.Name = "downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem";
            this.downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem.Click += new System.EventHandler(this.downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem_Click);
            // 
            // useVisualBarInTheEditorBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.useVisualBarInTheEditorBooleanToolStripMenuItem, "useVisualBarInTheEditorBooleanToolStripMenuItem");
            this.useVisualBarInTheEditorBooleanToolStripMenuItem.Name = "useVisualBarInTheEditorBooleanToolStripMenuItem";
            this.useVisualBarInTheEditorBooleanToolStripMenuItem.Click += new System.EventHandler(this.useVisualBarInTheEditorBooleanToolStripMenuItem_Click);
            // 
            // displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalSchemeBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalSchemeBooleanToolStripMenuItem, "displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalScheme" +
        "BooleanToolStripMenuItem");
            this.displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalSchemeBooleanToolStripMenuItem.Name = "displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalScheme" +
    "BooleanToolStripMenuItem";
            this.displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalSchemeBooleanToolStripMenuItem.Click += new System.EventHandler(this.displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalSchemeBooleanToolStripMenuItem_Click);
            // 
            // showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolStripMenuItem
            // 
            resources.ApplyResources(this.showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolStripMenuItem, "showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolS" +
        "tripMenuItem");
            this.showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolStripMenuItem.Name = "showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolS" +
    "tripMenuItem";
            this.showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolStripMenuItem.Click += new System.EventHandler(this.showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolStripMenuItem_Click);
            // 
            // automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem, "automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem");
            this.automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem.Name = "automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem";
            this.automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem.Click += new System.EventHandler(this.automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem_Click);
            // 
            // automaticallyRestoreLostFilesIfTheApplicationCrashesBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.automaticallyRestoreLostFilesIfTheApplicationCrashesBooleanToolStripMenuItem, "automaticallyRestoreLostFilesIfTheApplicationCrashesBooleanToolStripMenuItem");
            this.automaticallyRestoreLostFilesIfTheApplicationCrashesBooleanToolStripMenuItem.Name = "automaticallyRestoreLostFilesIfTheApplicationCrashesBooleanToolStripMenuItem";
            // 
            // configureIIToolStripMenuItem
            // 
            resources.ApplyResources(this.configureIIToolStripMenuItem, "configureIIToolStripMenuItem");
            this.configureIIToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.attemptToContinueExecutionIfTheApplicationCrashesBooleanToolStripMenuItem,
            this.showCommandBarBooleanToolStripMenuItem,
            this.enableQuickSearchingInTheMenusOfTheMainWindowBooleanToolStripMenuItem,
            this.disableTheValiNetCrashReporterToolStripMenuItem,
            this.applicationHasCrashedBooleanToolStripMenuItem,
            this.automaticallyPopulateTheFindCtrlFWindowWithDataFromTheEditorBooleanToolStripMenuItem,
            this.automaticallyPopulateTheFieldsInTheCreateEXEWizardWithSavedDataBooleanToolStripMenuItem,
            this.showConvertedCodeBooleanToolStripMenuItem,
            this.attemptToReportTheErrorToMicrosoftErrorReportingInAditionToValiNetBooleanToolStripMenuItem,
            this.showStatusBarBolleanToolStripMenuItem,
            this.showVisualScrollBarBooleanToolStripMenuItem,
            this.defaultLanguageInWhichPseudocodeFilesAreWrittenIntegerToolStripMenuItem,
            this.minGWPathStringToolStripMenuItem,
            this.advancedCommandsPanelDimensionsIntegerToolStripMenuItem,
            this.enableAeroShakeInTheFrameHostWindowBooleanToolStripMenuItem,
            this.disableFrameHostWindowBooleanToolStripMenuItem,
            this.policyRegardingHowTheApplicationRendersTheUIIntegerToolStripMenuItem,
            this.runtimePolicyRegardingWetherTheApplicationEnablesTheImprovedUIBooleanToolStripMenuItem});
            this.configureIIToolStripMenuItem.Name = "configureIIToolStripMenuItem";
            // 
            // attemptToContinueExecutionIfTheApplicationCrashesBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.attemptToContinueExecutionIfTheApplicationCrashesBooleanToolStripMenuItem, "attemptToContinueExecutionIfTheApplicationCrashesBooleanToolStripMenuItem");
            this.attemptToContinueExecutionIfTheApplicationCrashesBooleanToolStripMenuItem.Name = "attemptToContinueExecutionIfTheApplicationCrashesBooleanToolStripMenuItem";
            // 
            // showCommandBarBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.showCommandBarBooleanToolStripMenuItem, "showCommandBarBooleanToolStripMenuItem");
            this.showCommandBarBooleanToolStripMenuItem.Name = "showCommandBarBooleanToolStripMenuItem";
            // 
            // enableQuickSearchingInTheMenusOfTheMainWindowBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.enableQuickSearchingInTheMenusOfTheMainWindowBooleanToolStripMenuItem, "enableQuickSearchingInTheMenusOfTheMainWindowBooleanToolStripMenuItem");
            this.enableQuickSearchingInTheMenusOfTheMainWindowBooleanToolStripMenuItem.Name = "enableQuickSearchingInTheMenusOfTheMainWindowBooleanToolStripMenuItem";
            // 
            // disableTheValiNetCrashReporterToolStripMenuItem
            // 
            resources.ApplyResources(this.disableTheValiNetCrashReporterToolStripMenuItem, "disableTheValiNetCrashReporterToolStripMenuItem");
            this.disableTheValiNetCrashReporterToolStripMenuItem.Name = "disableTheValiNetCrashReporterToolStripMenuItem";
            // 
            // applicationHasCrashedBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.applicationHasCrashedBooleanToolStripMenuItem, "applicationHasCrashedBooleanToolStripMenuItem");
            this.applicationHasCrashedBooleanToolStripMenuItem.Name = "applicationHasCrashedBooleanToolStripMenuItem";
            // 
            // automaticallyPopulateTheFindCtrlFWindowWithDataFromTheEditorBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.automaticallyPopulateTheFindCtrlFWindowWithDataFromTheEditorBooleanToolStripMenuItem, "automaticallyPopulateTheFindCtrlFWindowWithDataFromTheEditorBooleanToolStripMenuI" +
        "tem");
            this.automaticallyPopulateTheFindCtrlFWindowWithDataFromTheEditorBooleanToolStripMenuItem.Name = "automaticallyPopulateTheFindCtrlFWindowWithDataFromTheEditorBooleanToolStripMenuI" +
    "tem";
            // 
            // automaticallyPopulateTheFieldsInTheCreateEXEWizardWithSavedDataBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.automaticallyPopulateTheFieldsInTheCreateEXEWizardWithSavedDataBooleanToolStripMenuItem, "automaticallyPopulateTheFieldsInTheCreateEXEWizardWithSavedDataBooleanToolStripMe" +
        "nuItem");
            this.automaticallyPopulateTheFieldsInTheCreateEXEWizardWithSavedDataBooleanToolStripMenuItem.Name = "automaticallyPopulateTheFieldsInTheCreateEXEWizardWithSavedDataBooleanToolStripMe" +
    "nuItem";
            // 
            // showConvertedCodeBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.showConvertedCodeBooleanToolStripMenuItem, "showConvertedCodeBooleanToolStripMenuItem");
            this.showConvertedCodeBooleanToolStripMenuItem.Name = "showConvertedCodeBooleanToolStripMenuItem";
            // 
            // attemptToReportTheErrorToMicrosoftErrorReportingInAditionToValiNetBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.attemptToReportTheErrorToMicrosoftErrorReportingInAditionToValiNetBooleanToolStripMenuItem, "attemptToReportTheErrorToMicrosoftErrorReportingInAditionToValiNetBooleanToolStri" +
        "pMenuItem");
            this.attemptToReportTheErrorToMicrosoftErrorReportingInAditionToValiNetBooleanToolStripMenuItem.Name = "attemptToReportTheErrorToMicrosoftErrorReportingInAditionToValiNetBooleanToolStri" +
    "pMenuItem";
            // 
            // showStatusBarBolleanToolStripMenuItem
            // 
            resources.ApplyResources(this.showStatusBarBolleanToolStripMenuItem, "showStatusBarBolleanToolStripMenuItem");
            this.showStatusBarBolleanToolStripMenuItem.Name = "showStatusBarBolleanToolStripMenuItem";
            // 
            // showVisualScrollBarBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.showVisualScrollBarBooleanToolStripMenuItem, "showVisualScrollBarBooleanToolStripMenuItem");
            this.showVisualScrollBarBooleanToolStripMenuItem.Name = "showVisualScrollBarBooleanToolStripMenuItem";
            // 
            // defaultLanguageInWhichPseudocodeFilesAreWrittenIntegerToolStripMenuItem
            // 
            resources.ApplyResources(this.defaultLanguageInWhichPseudocodeFilesAreWrittenIntegerToolStripMenuItem, "defaultLanguageInWhichPseudocodeFilesAreWrittenIntegerToolStripMenuItem");
            this.defaultLanguageInWhichPseudocodeFilesAreWrittenIntegerToolStripMenuItem.Name = "defaultLanguageInWhichPseudocodeFilesAreWrittenIntegerToolStripMenuItem";
            this.defaultLanguageInWhichPseudocodeFilesAreWrittenIntegerToolStripMenuItem.Click += new System.EventHandler(this.languageIntegerToolStripMenuItem_Click);
            // 
            // minGWPathStringToolStripMenuItem
            // 
            resources.ApplyResources(this.minGWPathStringToolStripMenuItem, "minGWPathStringToolStripMenuItem");
            this.minGWPathStringToolStripMenuItem.Name = "minGWPathStringToolStripMenuItem";
            this.minGWPathStringToolStripMenuItem.Click += new System.EventHandler(this.minGWPathStringToolStripMenuItem_Click);
            // 
            // advancedCommandsPanelDimensionsIntegerToolStripMenuItem
            // 
            resources.ApplyResources(this.advancedCommandsPanelDimensionsIntegerToolStripMenuItem, "advancedCommandsPanelDimensionsIntegerToolStripMenuItem");
            this.advancedCommandsPanelDimensionsIntegerToolStripMenuItem.Name = "advancedCommandsPanelDimensionsIntegerToolStripMenuItem";
            // 
            // enableAeroShakeInTheFrameHostWindowBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.enableAeroShakeInTheFrameHostWindowBooleanToolStripMenuItem, "enableAeroShakeInTheFrameHostWindowBooleanToolStripMenuItem");
            this.enableAeroShakeInTheFrameHostWindowBooleanToolStripMenuItem.Name = "enableAeroShakeInTheFrameHostWindowBooleanToolStripMenuItem";
            // 
            // disableFrameHostWindowBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.disableFrameHostWindowBooleanToolStripMenuItem, "disableFrameHostWindowBooleanToolStripMenuItem");
            this.disableFrameHostWindowBooleanToolStripMenuItem.Name = "disableFrameHostWindowBooleanToolStripMenuItem";
            // 
            // policyRegardingHowTheApplicationRendersTheUIIntegerToolStripMenuItem
            // 
            resources.ApplyResources(this.policyRegardingHowTheApplicationRendersTheUIIntegerToolStripMenuItem, "policyRegardingHowTheApplicationRendersTheUIIntegerToolStripMenuItem");
            this.policyRegardingHowTheApplicationRendersTheUIIntegerToolStripMenuItem.Name = "policyRegardingHowTheApplicationRendersTheUIIntegerToolStripMenuItem";
            // 
            // runtimePolicyRegardingWetherTheApplicationEnablesTheImprovedUIBooleanToolStripMenuItem
            // 
            resources.ApplyResources(this.runtimePolicyRegardingWetherTheApplicationEnablesTheImprovedUIBooleanToolStripMenuItem, "runtimePolicyRegardingWetherTheApplicationEnablesTheImprovedUIBooleanToolStripMen" +
        "uItem");
            this.runtimePolicyRegardingWetherTheApplicationEnablesTheImprovedUIBooleanToolStripMenuItem.Name = "runtimePolicyRegardingWetherTheApplicationEnablesTheImprovedUIBooleanToolStripMen" +
    "uItem";
            // 
            // experimentsToolStripMenuItem
            // 
            resources.ApplyResources(this.experimentsToolStripMenuItem, "experimentsToolStripMenuItem");
            this.experimentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.emptyToolStripMenuItem});
            this.experimentsToolStripMenuItem.Name = "experimentsToolStripMenuItem";
            // 
            // emptyToolStripMenuItem
            // 
            resources.ApplyResources(this.emptyToolStripMenuItem, "emptyToolStripMenuItem");
            this.emptyToolStripMenuItem.Name = "emptyToolStripMenuItem";
            // 
            // runtimeToolStripMenuItem
            // 
            resources.ApplyResources(this.runtimeToolStripMenuItem, "runtimeToolStripMenuItem");
            this.runtimeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.emptyToolStripMenuItem1});
            this.runtimeToolStripMenuItem.Name = "runtimeToolStripMenuItem";
            // 
            // emptyToolStripMenuItem1
            // 
            resources.ApplyResources(this.emptyToolStripMenuItem1, "emptyToolStripMenuItem1");
            this.emptyToolStripMenuItem1.Name = "emptyToolStripMenuItem1";
            // 
            // helpToolStripMenuItem
            // 
            resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            // 
            // aboutToolStripMenuItem
            // 
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Name = "label1";
            this.toolTip1.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Config_MouseDown);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Name = "button1";
            this.toolTip1.SetToolTip(this.button1, resources.GetString("button1.ToolTip"));
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.ForeColor = System.Drawing.Color.Black;
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, resources.GetString("pictureBox1.ToolTip"));
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Config_MouseDown);
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.ForeColor = System.Drawing.Color.Black;
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox2, resources.GetString("pictureBox2.ToolTip"));
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Config_MouseDown);
            // 
            // Close
            // 
            resources.ApplyResources(this.Close, "Close");
            this.Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Close.FlatAppearance.BorderSize = 0;
            this.Close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.Close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.Close.ForeColor = System.Drawing.Color.Black;
            this.Close.Name = "Close";
            this.toolTip1.SetToolTip(this.Close, resources.GetString("Close.ToolTip"));
            this.Close.UseVisualStyleBackColor = false;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.numericUpDown3);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.numericUpDown2);
            this.panel1.Controls.Add(this.comboBox2);
            this.panel1.Controls.Add(this.numericUpDown1);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Name = "panel1";
            this.toolTip1.SetToolTip(this.panel1, resources.GetString("panel1.ToolTip"));
            // 
            // textBox3
            // 
            resources.ApplyResources(this.textBox3, "textBox3");
            this.textBox3.BackColor = System.Drawing.Color.White;
            this.textBox3.ForeColor = System.Drawing.Color.Black;
            this.textBox3.Name = "textBox3";
            this.toolTip1.SetToolTip(this.textBox3, resources.GetString("textBox3.ToolTip"));
            // 
            // numericUpDown3
            // 
            resources.ApplyResources(this.numericUpDown3, "numericUpDown3");
            this.numericUpDown3.BackColor = System.Drawing.Color.White;
            this.numericUpDown3.ForeColor = System.Drawing.Color.Black;
            this.numericUpDown3.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.numericUpDown3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.toolTip1.SetToolTip(this.numericUpDown3, resources.GetString("numericUpDown3.ToolTip"));
            this.numericUpDown3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // textBox2
            // 
            resources.ApplyResources(this.textBox2, "textBox2");
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.ForeColor = System.Drawing.Color.Black;
            this.textBox2.Name = "textBox2";
            this.toolTip1.SetToolTip(this.textBox2, resources.GetString("textBox2.ToolTip"));
            // 
            // numericUpDown2
            // 
            resources.ApplyResources(this.numericUpDown2, "numericUpDown2");
            this.numericUpDown2.BackColor = System.Drawing.Color.White;
            this.numericUpDown2.ForeColor = System.Drawing.Color.Black;
            this.numericUpDown2.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            -2147483648});
            this.numericUpDown2.Name = "numericUpDown2";
            this.toolTip1.SetToolTip(this.numericUpDown2, resources.GetString("numericUpDown2.ToolTip"));
            this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // comboBox2
            // 
            resources.ApplyResources(this.comboBox2, "comboBox2");
            this.comboBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.ForeColor = System.Drawing.Color.Black;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            resources.GetString("comboBox2.Items"),
            resources.GetString("comboBox2.Items1")});
            this.comboBox2.Name = "comboBox2";
            this.toolTip1.SetToolTip(this.comboBox2, resources.GetString("comboBox2.ToolTip"));
            // 
            // numericUpDown1
            // 
            resources.ApplyResources(this.numericUpDown1, "numericUpDown1");
            this.numericUpDown1.BackColor = System.Drawing.Color.White;
            this.numericUpDown1.ForeColor = System.Drawing.Color.Black;
            this.numericUpDown1.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.toolTip1.SetToolTip(this.numericUpDown1, resources.GetString("numericUpDown1.ToolTip"));
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // comboBox1
            // 
            resources.ApplyResources(this.comboBox1, "comboBox1");
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.ForeColor = System.Drawing.Color.Black;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            resources.GetString("comboBox1.Items"),
            resources.GetString("comboBox1.Items1"),
            resources.GetString("comboBox1.Items2"),
            resources.GetString("comboBox1.Items3"),
            resources.GetString("comboBox1.Items4"),
            resources.GetString("comboBox1.Items5")});
            this.comboBox1.Name = "comboBox1";
            this.toolTip1.SetToolTip(this.comboBox1, resources.GetString("comboBox1.ToolTip"));
            // 
            // richTextBox1
            // 
            resources.ApplyResources(this.richTextBox1, "richTextBox1");
            this.richTextBox1.BackColor = System.Drawing.Color.White;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.richTextBox1.ForeColor = System.Drawing.Color.Black;
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.ShortcutsEnabled = false;
            this.toolTip1.SetToolTip(this.richTextBox1, resources.GetString("richTextBox1.ToolTip"));
            this.richTextBox1.MouseEnter += new System.EventHandler(this.richTextBox1_MouseEnter);
            // 
            // textBox1
            // 
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Name = "textBox1";
            this.toolTip1.SetToolTip(this.textBox1, resources.GetString("textBox1.ToolTip"));
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.toolTip1.SetToolTip(this.button2, resources.GetString("button2.ToolTip"));
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.toolTip1.SetToolTip(this.button3, resources.GetString("button3.ToolTip"));
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Config
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Config";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Config_FormClosing);
            this.Load += new System.EventHandler(this.Config_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Config_MouseDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem configureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runtimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rememberPositionAtStartupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languageIntegerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoSaveIntervalIntegerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automaticallySignInAtApplicationStartupBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userInterfaceLanguageIntegerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomLevelIntegerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicationNameIntegerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uiCodeConversionUpdateIntervalIntegerToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem experimentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emptyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emptyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem applicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem terminateNowToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem killMeWithErrorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem welcomeDialogWasShownUponSignInBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolStripMenuItem updateServerStringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem useVisualBarInTheEditorBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalSchemeBooleanToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBox1;
        //private DevComponents.DotNetBar.Controls.SwitchButton switchButton4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        //private DevComponents.DotNetBar.Controls.SwitchButton switchButton1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
       // private DevComponents.DotNetBar.Controls.SwitchButton switchButton2;
       // private DevComponents.DotNetBar.Controls.SwitchButton switchButton3;
        private System.Windows.Forms.TextBox textBox3;
       // private DevComponents.DotNetBar.Controls.SwitchButton switchButton5;
      //  private DevComponents.DotNetBar.Controls.SwitchButton switchButton6;
      //  private DevComponents.DotNetBar.Controls.SwitchButton switchButton7;
      //  private DevComponents.DotNetBar.Controls.SwitchButton switchButton8;
        private System.Windows.Forms.ToolStripMenuItem showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crashApplicationonNonUIThreadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automaticallyRestoreLostFilesIfTheApplicationCrashesBooleanToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripMenuItem configureIIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancedCommandsPanelDimensionsIntegerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableAeroShakeInTheFrameHostWindowBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disableFrameHostWindowBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem policyRegardingHowTheApplicationRendersTheUIIntegerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showCommandBarBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableQuickSearchingInTheMenusOfTheMainWindowBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disableTheValiNetCrashReporterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicationHasCrashedBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automaticallyPopulateTheFindCtrlFWindowWithDataFromTheEditorBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automaticallyPopulateTheFieldsInTheCreateEXEWizardWithSavedDataBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showConvertedCodeBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attemptToReportTheErrorToMicrosoftErrorReportingInAditionToValiNetBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showStatusBarBolleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showVisualScrollBarBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem defaultLanguageInWhichPseudocodeFilesAreWrittenIntegerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minGWPathStringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runtimePolicyRegardingWetherTheApplicationEnablesTheImprovedUIBooleanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attemptToContinueExecutionIfTheApplicationCrashesBooleanToolStripMenuItem;
    }
}