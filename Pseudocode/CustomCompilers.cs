﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class CustomCompilers : Form
    {
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();

        MainForm mf;
        string AppName;
        public CustomCompilers(MainForm m, string app)
        {
            InitializeComponent();
            InitializeLists();
            mf = m;
            AppName = app;
            listView1.Focus();
        }
        public void InitializeLists()
        {
            comboBox1.Items.Clear();
            listView1.Items.Clear();
            comboBox1.Items.Add("Local Debugger");
            comboBox1.Items.Add("Built-in Web Browser");
            listView1.Items.Add(new ListViewItem(new string[] { "Local Debugger", "C++", Properties.Settings.Default.MinGWPath, "%1", "cpp" }));
            listView1.Items.Add(new ListViewItem(new string[] { "Built-in Web Browser", "JavaScript", Application.ExecutablePath, "%1", "html" }));
            foreach (string s in Properties.Settings.Default.Compilers)
            {
                string[] t = s.Split(';');
                comboBox1.Items.Add(t[0]);
                string lang = "";
                switch (t[1])
                {
                    case "CPP": lang = "C++"; break;
                    case "PASCAL": lang = "Pascal"; break;
                    case "C#": lang = "C#"; break;
                    case "JAVASCRIPT": lang = "JavaScript"; break;
                    case "VB6": lang = "Visual Basic 6"; break;
                }
                listView1.Items.Add(new ListViewItem(new string[] { t[0], lang, t[2], t[3], t[4] }));
            }
            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            listView1.Items[0].Selected = true;
            comboBox1.SelectedIndex = Properties.Settings.Default.DefaultDebugMode - 1;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            int lang = 0;
            switch (listView1.SelectedItems[0].SubItems[1].Text)
            {
                case "C++": lang = 0; break;
                case "Pascal": lang = 1; break;
                case "C#": lang = 2; break;
                case "JavaScript": lang = 3; break;
                case "Visual Basic 6": lang = 4; break;
            }
            AddCompiler ac = new AddCompiler(listView1.SelectedItems[0].SubItems[0].Text, lang, listView1.SelectedItems[0].SubItems[2].Text, listView1.SelectedItems[0].SubItems[3].Text, mf, AppName, listView1.SelectedItems[0].SubItems[4].Text);
            ac.ShowDialog();
            InitializeLists();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string delete = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                delete = "Are you sure you would like to delete the selected favorite?";
            }
            else if (Properties.Settings.Default.UILang == 1)
            {
                delete = "Sigur eliminați site-ul selectat din lista de favorite?";
            }
            if (CasetaDeMesaj(mf, delete, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                listView1.SelectedItems[0].Remove();
                Properties.Settings.Default.Compilers.Clear();
                foreach (ListViewItem s in listView1.Items)
                {
                    string lang = "";
                    switch (s.SubItems[1].Text)
                    {
                        case "C++": lang = "CPP"; break;
                        case "Pascal": lang = "PASCAL"; break;
                        case "C#": lang = "C#"; break;
                        case "JavaScript": lang = "JAVASCRIPT"; break;
                        case "Visual Basic 6": lang = "VB6"; break;
                    }
                    if (s.SubItems[0].Text != "Local Debugger" && s.SubItems[0].Text != "Built-in Web Browser") Properties.Settings.Default.Compilers.Add(s.SubItems[0].Text + ";" + lang + ";" + s.SubItems[2].Text + ";" + s.SubItems[3].Text + ";" + s.SubItems[4].Text);
                }
                Properties.Settings.Default.DefaultDebugMode = 1;
                Properties.Settings.Default.Save();
                comboBox1.SelectedIndex = 0;
                InitializeLists();
            }
        }
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CustomCompilers_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.DefaultDebugMode = comboBox1.SelectedIndex + 1;
            Properties.Settings.Default.Save();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int index = listView1.SelectedItems[0].Index;
            MoveListViewItems(listView1, MoveDirection.Up);
            Properties.Settings.Default.Compilers.Clear();
            foreach (ListViewItem s in listView1.Items)
            {
                string lang = "";
                switch (s.SubItems[1].Text)
                {
                    case "C++": lang = "CPP"; break;
                    case "Pascal": lang = "PASCAL"; break;
                    case "C#": lang = "C#"; break;
                    case "JavaScript": lang = "JAVASCRIPT"; break;
                    case "Visual Basic 6": lang = "VB6"; break;
                }
                if (s.SubItems[0].Text != "Local Debugger" && s.SubItems[0].Text != "Built-in Web Browser") Properties.Settings.Default.Compilers.Add(s.SubItems[0].Text + ";" + lang + ";" + s.SubItems[2].Text + ";" + s.SubItems[3].Text + ";" + s.SubItems[4].Text);
            }
            Properties.Settings.Default.Save();
            try
            {
                listView1.Items[index - 1].Selected = true;
            }
            catch
            {
                System.Media.SystemSounds.Beep.Play();
            }
            /*comboBox1.Items.Clear();
            comboBox1.Items.Add("Local Debugger");
            comboBox1.Items.Add("Built-in Web Browser");
            foreach (string s in Properties.Settings.Default.Compilers)
            {
                string[] t = s.Split(';');
                comboBox1.Items.Add(t[0]);
            }
            if (listView1.Items[index - 1].Text == "Local Debugger")
            {
                comboBox1.SelectedIndex = 0;
                Properties.Settings.Default.DefaultDebugMode = 1;
            }
            else if (listView1.Items[index - 1].Text == "Built-in Web Browser")
            {
                comboBox1.SelectedIndex = 1;
                Properties.Settings.Default.DefaultDebugMode = 2;
            }
            else
            {
                int x = 0;
                foreach (string s in Properties.Settings.Default.Compilers)
                {
                    string[] t = s.Split(';');
                    if (t[0] == listView1.Items[index - 1].Text)
                    {
                        comboBox1.SelectedIndex = index - 1;
                        break;
                    }
                    x++;
                }
                Properties.Settings.Default.DefaultDebugMode = x;
            }*/

            listView1.Focus();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int index = listView1.SelectedItems[0].Index;
            MoveListViewItems(listView1, MoveDirection.Down);
            Properties.Settings.Default.Compilers.Clear();
            foreach (ListViewItem s in listView1.Items)
            {
                string lang = "";
                switch (s.SubItems[1].Text)
                {
                    case "C++": lang = "CPP"; break;
                    case "Pascal": lang = "PASCAL"; break;
                    case "C#": lang = "C#"; break;
                    case "JavaScript": lang = "JAVASCRIPT"; break;
                    case "Visual Basic 6": lang = "VB6"; break;
                }
                if (s.SubItems[0].Text != "Local Debugger" && s.SubItems[0].Text != "Built-in Web Browser") Properties.Settings.Default.Compilers.Add(s.SubItems[0].Text + ";" + lang+ ";" + s.SubItems[2].Text + ";" + s.SubItems[3].Text + ";" + s.SubItems[4].Text);
            }
            Properties.Settings.Default.Save();
            try
            {
                listView1.Items[index + 1].Selected = true;
            }
            catch {
                System.Media.SystemSounds.Beep.Play();
            }
            /*comboBox1.Items.Clear();
            comboBox1.Items.Add("Local Debugger");
            comboBox1.Items.Add("Built-in Web Browser");
            foreach (string s in Properties.Settings.Default.Compilers)
            {
                string[] t = s.Split(';');
                comboBox1.Items.Add(t[0]);
            }
            if (listView1.Items[index + 1].Text == "Local Debugger")
            {
                comboBox1.SelectedIndex = 0;
                Properties.Settings.Default.DefaultDebugMode = 1;
            }
            else if (listView1.Items[index + 1].Text == "Built-in Web Browser")
            {
                comboBox1.SelectedIndex = 1;
                Properties.Settings.Default.DefaultDebugMode = 2;
            }
            else
            {
                int x = 0;
                foreach (string s in Properties.Settings.Default.Compilers)
                {
                    string[] t = s.Split(';');
                    if (t[0] == listView1.Items[index + 1].Text)
                    {
                        comboBox1.SelectedIndex = index + 1;
                        break;
                    }
                    x++;
                }
                Properties.Settings.Default.DefaultDebugMode = x;
            }*/
            listView1.Focus();
        }

        private enum MoveDirection { Up = -1, Down = 1 };

        private static void MoveListViewItems(ListView sender, MoveDirection direction)
        {
            int dir = (int)direction;
            int opp = dir * -1;

            bool valid = sender.SelectedItems.Count > 0 &&
                            ((direction == MoveDirection.Down && (sender.SelectedItems[sender.SelectedItems.Count - 1].Index < sender.Items.Count - 1))
                        || (direction == MoveDirection.Up && (sender.SelectedItems[0].Index > 0)));

            if (valid)
            {
                foreach (ListViewItem item in sender.SelectedItems)
                {
                    int index = item.Index + dir;
                    sender.Items.RemoveAt(item.Index);
                    sender.Items.Insert(index, item);
                }
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
