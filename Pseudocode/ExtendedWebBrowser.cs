﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;

namespace Pseudocode
{
    [ComImport, TypeLibType(TypeLibTypeFlags.FHidden),
    InterfaceType(ComInterfaceType.InterfaceIsIDispatch),
    Guid("34A715A0-6587-11D0-924A-0020AFC7AC4D")]
    public interface DWebBrowserEvents2
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ppDisp">
        /// An interface pointer that, optionally, receives the IDispatch interface
        /// pointer of a new WebBrowser object or an InternetExplorer object.
        /// </param>
        /// <param name="Cancel">
        /// value that determines whether the current navigation should be canceled
        /// </param>
        /// <param name="dwFlags">
        /// The flags from the NWMF enumeration that pertain to the new window
        /// See http://msdn.microsoft.com/en-us/library/bb762518(VS.85).aspx.
        /// </param>
        /// <param name="bstrUrlContext">
        /// The URL of the page that is opening the new window.
        /// </param>
        /// <param name="bstrUrl">The URL that is opened in the new window.</param>
        [DispId(0x111)]
        void NewWindow3(
            [In, Out, MarshalAs(UnmanagedType.IDispatch)] ref object ppDisp,
            [In, Out] ref bool Cancel,
            [In] uint dwFlags,
            [In, MarshalAs(UnmanagedType.BStr)] string bstrUrlContext,
            [In, MarshalAs(UnmanagedType.BStr)] string bstrUrl);
    }
    public partial class WebBrowserEx : WebBrowser
    {
        #region enums
        public enum OLECMDID
        {
            // ...
            OLECMDID_ZOOM = 19,
            OLECMDID_OPTICAL_ZOOM = 63,
            OLECMDID_OPTICAL_GETZOOMRANGE = 64,
            // ...
        }

        public enum OLECMDEXECOPT
        {
            // ...
            OLECMDEXECOPT_DONTPROMPTUSER,
            // ...
        }

        public enum OLECMDF
        {
            // ...
            OLECMDF_SUPPORTED = 1
        }
        #endregion

        #region IWebBrowser2
        [ComImport, /*SuppressUnmanagedCodeSecurity,*/
         TypeLibType(TypeLibTypeFlags.FOleAutomation |
                     TypeLibTypeFlags.FDual |
                     TypeLibTypeFlags.FHidden),
         Guid("D30C1661-CDAF-11d0-8A3E-00C04FC9E26E")]
        public interface IWebBrowser2
        {
            [DispId(100)]
            void GoBack();
            [DispId(0x65)]
            void GoForward();
            [DispId(0x66)]
            void GoHome();
            [DispId(0x67)]
            void GoSearch();
            [DispId(0x68)]
            void Navigate([In] string Url,
                          [In] ref object flags,
                          [In] ref object targetFrameName,
                          [In] ref object postData,
                          [In] ref object headers);
            [DispId(-550)]
            void Refresh();
            [DispId(0x69)]
            void Refresh2([In] ref object level);
            [DispId(0x6a)]
            void Stop();
            [DispId(200)]
            object Application
            {
                [return:
                 MarshalAs(UnmanagedType.IDispatch)]
                get;
            }
            [DispId(0xc9)]
            object Parent
            {
                [return:
                 MarshalAs(UnmanagedType.IDispatch)]
                get;
            }
            [DispId(0xca)]
            object Container
            {
                [return:
                 MarshalAs(UnmanagedType.IDispatch)]
                get;
            }
            [DispId(0xcb)]
            object Document
            {
                [return:
                 MarshalAs(UnmanagedType.IDispatch)]
                get;
            }
            [DispId(0xcc)]
            bool TopLevelContainer { get; }
            [DispId(0xcd)]
            string Type { get; }
            [DispId(0xce)]
            int Left { get; set; }
            [DispId(0xcf)]
            int Top { get; set; }
            [DispId(0xd0)]
            int Width { get; set; }
            [DispId(0xd1)]
            int Height { get; set; }
            [DispId(210)]
            string LocationName { get; }
            [DispId(0xd3)]
            string LocationURL { get; }
            [DispId(0xd4)]
            bool Busy { get; }
            [DispId(300)]
            void Quit();
            [DispId(0x12d)]
            void ClientToWindow(out int pcx, out int pcy);
            [DispId(0x12e)]
            void PutProperty([In] string property,
                             [In] object vtValue);
            [DispId(0x12f)]
            object GetProperty([In] string property);
            [DispId(0)]
            string Name { get; }
            [DispId(-515)]
            int HWND { get; }
            [DispId(400)]
            string FullName { get; }
            [DispId(0x191)]
            string Path { get; }
            [DispId(0x192)]
            bool Visible { get; set; }
            [DispId(0x193)]
            bool StatusBar { get; set; }
            [DispId(0x194)]
            string StatusText { get; set; }
            [DispId(0x195)]
            int ToolBar { get; set; }
            [DispId(0x196)]
            bool MenuBar { get; set; }
            [DispId(0x197)]
            bool FullScreen { get; set; }
            [DispId(500)]
            void Navigate2([In] ref object URL,
                           [In] ref object flags,
                           [In] ref object targetFrameName,
                           [In] ref object postData,
                           [In] ref object headers);
            [DispId(0x1f5)]
            OLECMDF QueryStatusWB([In] OLECMDID cmdID);
            [DispId(0x1f6)]
            void ExecWB([In] OLECMDID cmdID,
                        [In] OLECMDEXECOPT cmdexecopt,
                        ref object pvaIn, IntPtr pvaOut);
            [DispId(0x1f7)]
            void ShowBrowserBar([In] ref object pvaClsid,
                                [In] ref object pvarShow,
                                [In] ref object pvarSize);
            [DispId(-525)]
            WebBrowserReadyState ReadyState { get; }
            [DispId(550)]
            bool Offline { get; set; }
            [DispId(0x227)]
            bool Silent { get; set; }
            [DispId(0x228)]
            bool RegisterAsBrowser { get; set; }
            [DispId(0x229)]
            bool RegisterAsDropTarget { get; set; }
            [DispId(0x22a)]
            bool TheaterMode { get; set; }
            [DispId(0x22b)]
            bool AddressBar { get; set; }
            [DispId(0x22c)]
            bool Resizable { get; set; }
        }
        #endregion

        private IWebBrowser2 axIWebBrowser2;


        AxHost.ConnectionPointCookie cookie;
        DWebBrowserEvent2Helper helper;
        [Browsable(true)]
        public event EventHandler<WebBrowserNewWindowEventArgs> NewWindow3;
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public WebBrowserEx()
        {
        }
        /// <summary>
        /// Associates the underlying ActiveX control with a client that can 
        /// handle control events including NewWindow3 event.
        /// </summary>
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        protected override void CreateSink()
        {
            base.CreateSink();

            helper = new DWebBrowserEvent2Helper(this);
            cookie = new AxHost.ConnectionPointCookie(
                this.ActiveXInstance, helper, typeof(DWebBrowserEvents2));
        }
        /// <summary>
        /// Releases the event-handling client attached in the CreateSink method
        /// from the underlying ActiveX control
        /// </summary>
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        protected override void DetachSink()
        {
            if (cookie != null)
            {
                cookie.Disconnect();
                cookie = null;
            }
            base.DetachSink();
        }
        /// <summary>
        ///  Raises the NewWindow3 event.
        /// </summary>
        protected virtual void OnNewWindow3(WebBrowserNewWindowEventArgs e)
        {
            if (this.NewWindow3 != null)
            {
                this.NewWindow3(this, e);
            }
        }


        /// <summary>
        /// Fires when downloading is completed
        /// </summary>
        /// <remarks>
        /// Here you could start monitoring for script errors. 
        /// </remarks>
        public event EventHandler DownloadComplete;
        /// <summary>
        /// Raises the <see cref="DownloadComplete"/> event
        /// </summary>
        /// <param name="e">Empty <see cref="EventArgs"/></param>
        protected virtual void OnDownloadComplete(EventArgs e)
        {
            if (DownloadComplete != null)
                DownloadComplete(this, e);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == (int)0x210)
            {
                int lp = m.LParam.ToInt32();
                int wp = m.WParam.ToInt32();

                int X = wp & 0xFFFF;
                int Y = (wp >> 16) & 0xFFFF;
                if (X == (int)0x2)
                    ((Form)this.Parent).Close();
            }
            base.WndProc(ref m);
        }



        private class DWebBrowserEvent2Helper : StandardOleMarshalObject, DWebBrowserEvents2
        {
            private WebBrowserEx parent;
            public DWebBrowserEvent2Helper(WebBrowserEx parent)
            {
                this.parent = parent;
            }
            /// <summary>
            /// Raise the NewWindow3 event.
            /// If an instance of WebBrowser2EventHelper is associated with the underlying
            /// ActiveX control, this method will be called When the NewWindow3 event was
            /// fired in the ActiveX control.
            /// </summary>
            public void NewWindow3(ref object ppDisp, ref bool Cancel, uint dwFlags,
                string bstrUrlContext, string bstrUrl)
            {
                var e = new WebBrowserNewWindowEventArgs(bstrUrl, Cancel);
                this.parent.OnNewWindow3(e);
                Cancel = e.Cancel;
            }
        }
        protected override void AttachInterfaces(object nativeActiveXObject)
        {
            base.AttachInterfaces(nativeActiveXObject);
            this.axIWebBrowser2 = (IWebBrowser2)nativeActiveXObject;
        }

        protected override void DetachInterfaces()
        {
            base.DetachInterfaces();
            this.axIWebBrowser2 = null;
        }

        public void Zoom(int factor)
        {
            object pvaIn = factor;
            try
            {
                this.axIWebBrowser2.ExecWB(OLECMDID.OLECMDID_OPTICAL_ZOOM,
                   OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER,
                   ref pvaIn, IntPtr.Zero);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Changes browser text size .
        /// </summary>
        /// <param name="textSize"></param>
        private void ChangeTextSize(int textSize)
        {
            try
            {
                this.axIWebBrowser2.ExecWB(OLECMDID.OLECMDID_ZOOM,
                     OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER,
                     textSize, IntPtr.Zero);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // source : http://support.microsoft.com/kb/304103
        // the valid range for the zoom level is 0 through 4, 
        // in which 0 is smallest and 4 is largest
        private const int MIN_TEXT_SIZE = 0;
        private const int MAX_TEXT_SIZE = 4;
        private const int DEFAULT_TEXT_SIZE = 2;

        /*
        /// <summary>
        /// Returns the current size of text .
        /// NOTE : DOES NOT WORK ! Always returns zero !
        /// </summary>
        /// <returns></returns>
        private int GetTextSize()
        {           
            object pvaIn = 0;
            this.axIWebBrowser2.ExecWB (OLECMDID.OLECMDID_ZOOM, 
            OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER, IntPtr.Zero, pvaIn );
            return (int)pvaIn;
        }
        */

        /// <summary>
        /// Returns True if text size can be increased .
        /// </summary>
        public bool CanZoomIn
        {
            get { return textSize < MAX_TEXT_SIZE; }
        }

        /// <summary>
        /// Returns True if text size can be decreased .
        /// </summary>
        public bool CanZoomOut
        {
            get { return textSize > MIN_TEXT_SIZE; }
        }
        /// <summary>
        /// Current text size ( default is 2 ) .
        /// </summary>
        private int textSize = DEFAULT_TEXT_SIZE;

        /// <summary>
        /// Increases text size . 
        /// </summary>
        public void ZoomIn()
        {
            if (textSize < MAX_TEXT_SIZE)
            {
                textSize++;
                this.ChangeTextSize(textSize);
            }
        }

        /// <summary>
        /// Decreases text size .
        /// </summary>
        public void ZoomOut()
        {
            if (textSize > MIN_TEXT_SIZE)
            {
                textSize--;
                this.ChangeTextSize(textSize);
            }
        }

        public void ZoomText(int factor)
        {
            this.ChangeTextSize(factor);
        }
    }
    public class WebBrowserNewWindowEventArgs : EventArgs
    {
        public String Url { get; set; }
        public Boolean Cancel { get; set; }
        public WebBrowserNewWindowEventArgs(String url, Boolean cancel)
        {
            this.Url = url;
            this.Cancel = cancel;
        }
    }
}