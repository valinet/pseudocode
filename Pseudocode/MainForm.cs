﻿using AppLimit.CloudComputing.SharpBox;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
//using System.Windows.Forms.Integration;
using ToolStripCustomCtrls;
using ValiNet.PseudocodePluginsFramework;
using WeifenLuo.WinFormsUI.Docking;

namespace Pseudocode
{
    public partial class MainForm : Form//, IMessageFilter
    {

        private FormWindowState mLastState;


        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetProcessDPIAware();
        string DefaultSearchBoxText;
        string AppName;
        public string NewTabTitle;
        bool wasDownloaded = false;
        string NoBrowserInstalled;
        string AlreadySignedIn;
        string ErrorDownloadingCode;
        string Downloaded;
        string NoSuchFile;
        string ZoomText;
        string BackOp;
        string ENToRO;
        string ROToEN;
        string NothingToRecover;
        string SuccessfullyInstalled;
        string ErrorInstalled;
        string OverwriteInstall;
        string RestartNeeded;
        string CannotOpen;
        string EraseCoverage;
        string AnalizeCovergaeData;
        string NoCoverageData;
        string NotAvailableUILang;
        string CloseT;
        string RestoreT;
        string MaximizeT;
        string MinimizeT;
        public string ConnectedAs;
        public Splash spl;
        public bool done = false;
        public Help hp;
        string addToFavorites;
        string invalidName;
        string runText;
        //Glow wpfwindow = new Glow();
        private void ShowSplashScreen()
        {
            spl = new Splash();
            /*if (Properties.Settings.Default.Position == true)
            {
                if (Properties.Settings.Default.WindowsSize != new Size(0, 0))
                {
                    spl.Size = Properties.Settings.Default.WindowsSize;
                }
                if (Properties.Settings.Default.WindowsPos != new Point(0, 0))
                {
                    spl.Location = Properties.Settings.Default.WindowsPos;
                }
                if (IsOnScreen(this) == false)
                {
                    spl.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
                    spl.Size = new Size(1258, 697);
                }
                if (Properties.Settings.Default.IsMaximized == true)
                {

                    this.WindowState = FormWindowState.Maximized;
                }
                if (!IsMaximised)
                {
                    spl.Location = new Point(spl.Location.X, spl.Location.Y + Close.Height - 5);
                }
            }*/
         //   spl.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            spl.Show();
            spl.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            while (done == false)
            {
                Application.DoEvents();
            }
            spl.Close();
            this.spl.Dispose();
        }
        Dictionary<string, VPlugin> _Plugins;

        Color menuStrip1BackColor;
        Color menuStrip1ForeColor;
        Color toolStripButtonForeColor;
        Color themeColor;
        Color topCoverBackColor;
        Color searchBoxBackColor;
        Color searchBoxForeColor;
        Color sc11;
        Color sc12;
        Color sc21;
        Color sc22;
        Color sc23;
        Color sc31;
        Color sc32;
        Color sc33;
        Color thisBackColor;
        protected override void OnPaint(PaintEventArgs e)
        {
            RaisePaintEvent(this, e); //remove the base.OnPaint(e)
        }
        const int SC_MAXIMIZE = 0xF030;
        const int SC_MOVE = 0xF010;
        Point systemMenuPos;
        public bool unminimizedCode = false;
        [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        public static extern int SetWindowTheme(IntPtr hWnd, String pszSubAppName, String pszSubIdList);
        protected override void OnHandleCreated(EventArgs e)
        {
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                SetWindowTheme(this.Handle, "", "");
            }
            base.OnHandleCreated(e);
        }
        protected override void WndProc(ref Message m)
        {
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                const int wmNcHitTest = 0x84;
                const int htBottomLeft = 16;
                const int htBottomRight = 17;
                if (m.Msg == wmNcHitTest && !IsMaximised)
                {
                    int borderSize = 5;
                    int x = (int)(m.LParam.ToInt64() & 0xFFFF);
                    int y = (int)((m.LParam.ToInt64() & 0xFFFF0000) >> 16);
                    Point pt = PointToClient(new Point(x, y));
                    Size clientSize = Size;
                    if (pt.X >= clientSize.Width - borderSize && pt.Y >= clientSize.Height - borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(htBottomRight);

                        return;
                    }
                    if (pt.X >= clientSize.Width - borderSize && pt.Y >= borderSize && pt.Y < clientSize.Height - borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(11); //htRight = 11

                        return;
                    }
                    if (pt.X > borderSize && pt.Y >= clientSize.Height - borderSize && pt.X < clientSize.Width - borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(15); //htBottom = 15

                        return;
                    }
                    if (pt.X <= borderSize && pt.Y >= clientSize.Height - borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(16); //htBottomLeft

                        return;
                    }
                    if (pt.X <= borderSize && pt.Y >= borderSize && pt.Y >= 0 && pt.Y < clientSize.Height - borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(10); //htLeft = 10

                        return;
                    }
                    if (pt.X > borderSize && pt.Y <= borderSize && pt.X <= clientSize.Width - 5 && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(12); //htTop = 12

                        return;
                    }
                    if (pt.X > 0 && pt.X <= borderSize && pt.Y <= borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(13); //htTopLeft = 13

                        return;
                    }
                    if (pt.X >= this.Width - borderSize && pt.Y <= borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(14); //htTopRight = 14

                        return;
                    }
                }
                if (m.Msg == 0x313)
                {
                    systemMenuPos = PointToClient(Cursor.Position);
                    showSystemMenuTimer.Interval = 1;
                    showSystemMenuTimer.Enabled = true;
                    return;
                }
                if (m.Msg == 0x0083)
                {
                    return;
                }
                if (m.Msg == 0x000C)
                {
                    DefWndProc(ref m);
                    m.Result = (IntPtr)1;
                    return;
                }
                if (m.Msg == 0x0086)
                {
                    if (this.WindowState == FormWindowState.Minimized) DefWndProc(ref m);
                    m.Result = (IntPtr)1;
                    return;
                }
                /*if (m.Msg == 6 && m.WParam.ToInt32() == 1)
                {
                    if (Control.FromHandle(m.LParam) == null)
                    {
                        if (this.WindowState == FormWindowState.Normal)
                        {
                            try
                            {
                                wpfwindow.Visibility = System.Windows.Visibility.Visible;
                                SetForegroundWindow(wpfwindow.GetHandle());
                                this.TopMost = true;
                                this.TopMost = false;
                                wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                            }
                            catch { }
                        }
                    }
                }*/
                /*if (m.Msg == 0x1C)
                {
                    if (m.WParam != IntPtr.Zero)
                    {
                        if (this.WindowState == FormWindowState.Normal)
                        {
                            try
                            {
                                wpfwindow.Visibility = System.Windows.Visibility.Visible;
                                SetForegroundWindow(wpfwindow.GetHandle());
                                SetForegroundWindow(this.Handle);
                                wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                            }
                            catch { }
                        }
                    }
                }*/
             /*   if (m.Msg == WM_SYSCOMMAND)
                {
                    if ((int)m.WParam == SC_MAXIMIZE)
                    {
                        wpfwindow.Visibility = System.Windows.Visibility.Visible;
                        wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                    }
                    if ((int)m.WParam == 0xF020) //SC_MINIMIZE
                    {
                        wpfwindow.Visibility = System.Windows.Visibility.Visible;
                        wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                    }
                }*/
            }
            if (Properties.Settings.Default.DisableEnhancedUI == false)
            {
                const int wmNcHitTest = 0x84;
                const int htBottomLeft = 16;
                const int htBottomRight = 17;
                if (m.Msg == wmNcHitTest && !IsMaximised)
                {
                    int borderSize = 5;
                    int x = (int)(m.LParam.ToInt64() & 0xFFFF);
                    int y = (int)((m.LParam.ToInt64() & 0xFFFF0000) >> 16);
                    Point pt = PointToClient(new Point(x, y));
                    Size clientSize = Size;
                    if (pt.X >= clientSize.Width - borderSize && pt.Y >= clientSize.Height - borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(htBottomRight);

                        return;
                    }
                    if (pt.X >= clientSize.Width - borderSize && pt.Y >= borderSize && pt.Y < clientSize.Height - borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(11); //htRight = 11

                        return;
                    }
                    if (pt.X > borderSize && pt.Y >= clientSize.Height - borderSize && pt.X < clientSize.Width - borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(15); //htBottom = 15

                        return;
                    }
                    if (pt.X <= borderSize && pt.Y >= clientSize.Height - borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(16); //htBottomLeft

                        return;
                    }
                    if (pt.X <= borderSize && pt.Y >= borderSize && pt.Y >= 0 && pt.Y < clientSize.Height - borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(10); //htLeft = 10

                        return;
                    }
                    if (pt.X > borderSize && pt.Y <= borderSize && pt.X <= clientSize.Width - 105 && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(12); //htTop = 12

                        return;
                    }
                    if (pt.X > 0 && pt.X <= borderSize && pt.Y <= borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(13); //htTopLeft = 13

                        return;
                    }
                    if (pt.X >= this.Width - borderSize && pt.Y <= borderSize && clientSize.Height >= borderSize)
                    {
                        m.Result = (IntPtr)(14); //htTopRight = 14

                        return;
                    }


                }
                if (m.Msg == 0x313)
                {
                    systemMenuPos = PointToClient(Cursor.Position);
                    showSystemMenuTimer.Interval = 1;
                    showSystemMenuTimer.Enabled = true;
                    return;
                }
                if (m.Msg == 0x1C)
                {
                    if (m.WParam != IntPtr.Zero)
                    {
                        SetForegroundWindow(glow.wpfwindow.GetHandle());
                        SetForegroundWindow(glow.Handle);
                        SetForegroundWindow(this.Handle);
                    }
                }
                if (m.Msg == 6 && m.WParam.ToInt32() == 1 && !IsMaximised)
                {
                    /*    SetForegroundWindow(glow.wpfwindow.GetHandle());
                        SetForegroundWindow(glow.Handle);
                        SetForegroundWindow(this.Handle);*/
                    if (Control.FromHandle(m.LParam) == null)
                    {
                        if (unminimizedCode) this.Opacity = 0;
                        //glow.Snap();
                        //BeginControlUpdate(this);
                        // BeginControlUpdate(glow);
                        // glow.ShowInTaskbar = true;
                        //  glow.WindowState = FormWindowState.Minimized;
                        glow.Show();
                        //    glow.ShowInTaskbar = false;
                        //  glow.WindowState = FormWindowState.Normal;
                        glow.Refresh();

                        //  EndControlUpdate(glow);
                        /* if (this.FormBorderStyle != System.Windows.Forms.FormBorderStyle.None)
                         {
                             this.Height += 39;
                             this.Width += 13;
                         }*/
                        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

                        this.OnResizeEnd(new EventArgs());
                        if (unminimizedCode) this.Opacity = 1;
                        //   EndControlUpdate(this);

                    }
                }
                if (m.Msg == WM_SYSCOMMAND)
                {
                    if ((int)m.WParam == SC_MAXIMIZE)
                    {
                        if (IsMaximised) Restore.PerformClick();
                        else glow.Restore.PerformClick();
                        return;
                    }
                    if ((int)m.WParam == 0xF020) //SC_MINIMIZE
                    {
                        if (IsMaximised) Minimize.PerformClick();
                        else glow.Minimize.PerformClick();
                        return;
                    }
                    /* int command = m.WParam.ToInt32() & 0xfff0;
                     if (command == SC_MOVE)
                     {
                         if (glow.WindowState == FormWindowState.Maximized) return;
                     }*/
                }
            }
            else
            {
                if (m.Msg == 0x313)
                {
                    systemMenuPos = PointToClient(Cursor.Position);
                    showSystemMenuTimer.Interval = 1;
                    showSystemMenuTimer.Enabled = true;
                    return;
                }
            }
            base.WndProc(ref m);
        }
        /// <summary>
        /// An application sends the WM_SETREDRAW message to a window to allow changes in that 
        /// window to be redrawn or to prevent changes in that window from being redrawn.
        /// </summary>
        private const int WM_SETREDRAW = 11;
        /// <summary>
        /// Suspends painting for the target control. Do NOT forget to call EndControlUpdate!!!
        /// </summary>
        /// <param name="control">visual control</param>
        public static void BeginControlUpdate(Control control)
        {
            Message msgSuspendUpdate = Message.Create(control.Handle, WM_SETREDRAW, IntPtr.Zero,
                  IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgSuspendUpdate);
        }

        /// <summary>
        /// Resumes painting for the target control. Intended to be called following a call to BeginControlUpdate()
        /// </summary>
        /// <param name="control">visual control</param>
        public static void EndControlUpdate(Control control)
        {
            // Create a C "true" boolean as an IntPtr
            IntPtr wparam = new IntPtr(1);
            Message msgResumeUpdate = Message.Create(control.Handle, WM_SETREDRAW, wparam,
                  IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgResumeUpdate);
            control.Invalidate();
            control.Refresh();
        }

        public static IntPtr SetWindowLongPtr(IntPtr hWnd, int nIndex, IntPtr dwNewLong)
        {
            if (IntPtr.Size == 8)
                return SetWindowLongPtr64(hWnd, nIndex, dwNewLong);
            else
                return new IntPtr(SetWindowLong32(hWnd, nIndex, dwNewLong.ToInt32()));
        }

        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        private static extern int SetWindowLong32(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr")]
        private static extern IntPtr SetWindowLongPtr64(IntPtr hWnd, int nIndex, IntPtr dwNewLong);
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);

        [Flags()]
        private enum SetWindowPosFlags : uint
        {
            /// <summary>If the calling thread and the thread that owns the window are attached to different input queues, 
            /// the system posts the request to the thread that owns the window. This prevents the calling thread from 
            /// blocking its execution while other threads process the request.</summary>
            /// <remarks>SWP_ASYNCWINDOWPOS</remarks>
            AsynchronousWindowPosition = 0x4000,
            /// <summary>Prevents generation of the WM_SYNCPAINT message.</summary>
            /// <remarks>SWP_DEFERERASE</remarks>
            DeferErase = 0x2000,
            /// <summary>Draws a frame (defined in the window's class description) around the window.</summary>
            /// <remarks>SWP_DRAWFRAME</remarks>
            DrawFrame = 0x0020,
            /// <summary>Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to
            /// the window, even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE
            /// is sent only when the window's size is being changed.</summary>
            /// <remarks>SWP_FRAMECHANGED</remarks>
            FrameChanged = 0x0020,
            /// <summary>Hides the window.</summary>
            /// <remarks>SWP_HIDEWINDOW</remarks>
            HideWindow = 0x0080,
            /// <summary>Does not activate the window. If this flag is not set, the window is activated and moved to the
            /// top of either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter 
            /// parameter).</summary>
            /// <remarks>SWP_NOACTIVATE</remarks>
            DoNotActivate = 0x0010,
            /// <summary>Discards the entire contents of the client area. If this flag is not specified, the valid 
            /// contents of the client area are saved and copied back into the client area after the window is sized or 
            /// repositioned.</summary>
            /// <remarks>SWP_NOCOPYBITS</remarks>
            DoNotCopyBits = 0x0100,
            /// <summary>Retains the current position (ignores X and Y parameters).</summary>
            /// <remarks>SWP_NOMOVE</remarks>
            IgnoreMove = 0x0002,
            /// <summary>Does not change the owner window's position in the Z order.</summary>
            /// <remarks>SWP_NOOWNERZORDER</remarks>
            DoNotChangeOwnerZOrder = 0x0200,
            /// <summary>Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to
            /// the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent 
            /// window uncovered as a result of the window being moved. When this flag is set, the application must 
            /// explicitly invalidate or redraw any parts of the window and parent window that need redrawing.</summary>
            /// <remarks>SWP_NOREDRAW</remarks>
            DoNotRedraw = 0x0008,
            /// <summary>Same as the SWP_NOOWNERZORDER flag.</summary>
            /// <remarks>SWP_NOREPOSITION</remarks>
            DoNotReposition = 0x0200,
            /// <summary>Prevents the window from receiving the WM_WINDOWPOSCHANGING message.</summary>
            /// <remarks>SWP_NOSENDCHANGING</remarks>
            DoNotSendChangingEvent = 0x0400,
            /// <summary>Retains the current size (ignores the cx and cy parameters).</summary>
            /// <remarks>SWP_NOSIZE</remarks>
            IgnoreResize = 0x0001,
            /// <summary>Retains the current Z order (ignores the hWndInsertAfter parameter).</summary>
            /// <remarks>SWP_NOZORDER</remarks>
            IgnoreZOrder = 0x0004,
            /// <summary>Displays the window.</summary>
            /// <remarks>SWP_SHOWWINDOW</remarks>
            ShowWindow = 0x0040,
        }
        public void ThemeCode()
        {
            //theme code
            toolTip1.SetToolTip(Close, CloseT);
            toolTip1.SetToolTip(Restore, MaximizeT);
            toolTip1.SetToolTip(Minimize, MinimizeT);
            if (Properties.Settings.Default.UITheme == 0)
            {
                Title.BackColor = menuStrip1BackColor;
                Close.BackgroundImage = CloseBlack.BackgroundImage;
                if (this.WindowState == FormWindowState.Maximized)
                {
                    Restore.BackgroundImage = RestoreBlack.BackgroundImage;
                }
                if (this.WindowState == FormWindowState.Normal)
                {
                    Restore.BackgroundImage = MaximizeBlack.BackgroundImage;
                }
                Minimize.BackgroundImage = MinimizeBlack.BackgroundImage;
                UserPanel.BackColor = menuStrip1BackColor;
                label27.ForeColor = menuStrip1ForeColor;
                menuStrip1.Appearance = blackApparance1;
                menuStrip1.BackColor = menuStrip1BackColor;
                hELPToolStripMenuItem.BackColor = menuStrip1BackColor;
                fILEToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                eDITToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                vIEWToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                cONVERTToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                dEBUGToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                tOOLSToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                hELPToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                fILEToolStripMenuItem1.ForeColor = menuStrip1ForeColor;
                eDITToolStripMenuItem1.ForeColor = menuStrip1ForeColor;
                vIEWToolStripMenuItem1.ForeColor = menuStrip1ForeColor;
                fAVORITESToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                tOOLSToolStripMenuItem1.ForeColor = menuStrip1ForeColor;
                hELPToolStripMenuItem1.ForeColor = menuStrip1ForeColor;
                recoverLostunsavedFilesToolStripMenuItem.ForeColor = menuStrip1ForeColor;

                foreach (ToolStripMenuItem toolItem in GetItems(menuStrip1))
                {
                    toolItem.ForeColor = menuStrip1ForeColor;
                }
                foreach (ToolStripItem a in toolStripButton32.DropDownItems)
                {
                    ToolStripMenuItem b = a as ToolStripMenuItem;
                    if (b != null) b.ForeColor = menuStrip1ForeColor;
                }
                runUsingBuiltinBrowserToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                runUsingLocalDebuggerToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                manageCustomCompilersToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                textSizeToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                zoomToolStripMenuItem1.ForeColor = menuStrip1ForeColor;
                exportCodeToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                zoomToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                limbaDeScriereToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                translatePseudocodeToolStripMenuItem.ForeColor = menuStrip1ForeColor;
                toolStrip1.Appearance = blackApparance1;
                customizableToolStrip1.Appearance = blackApparance1;
                toolStripButton32.ForeColor = toolStripButtonForeColor;
                toolStrip1.BackColor = themeColor;
                customizableToolStrip1.BackColor = themeColor;
                Left.BackColor = themeColor;
                Right.BackColor = themeColor;
                Bottom.BackColor = themeColor;
                Top.BackColor = themeColor;
                TopCover.BackColor = topCoverBackColor;
                SearchBox.BackColor = searchBoxBackColor;
                SearchBox.ForeColor = searchBoxForeColor;
                toolStripButton30.BackColor = searchBoxBackColor;
                toolStripButton30.ForeColor = Color.White;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.StartColor = sc11;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.EndColor = sc12;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.StartColor = sc21;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.EndColor = sc22;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.TextColor = sc23;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.StartColor = sc31;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.EndColor = sc32;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.TextColor = sc33;

                menuIcon.BackColor = Color.Black;
                this.BackColor = thisBackColor;
                if (Properties.Settings.Default.DisableEnhancedUI == false)
                {

                    try
                    {
                        glow.BackColor = thisBackColor;
                        glow.Title.BackColor = thisBackColor;
                        glow.Close.BackgroundImage = glow.CloseBlack.BackgroundImage;
                        glow.Restore.BackgroundImage = glow.RestoreBlack.BackgroundImage;
                        glow.Minimize.BackgroundImage = glow.MinimizeBlack.BackgroundImage;
                    }
                    catch { }
                }
            }
            if (Properties.Settings.Default.UITheme == 1)
            {
                Title.BackColor = Color.FromArgb(214, 219, 233); ;
                Close.BackgroundImage = CloseLight.BackgroundImage;
                if (this.WindowState == FormWindowState.Maximized)
                {
                    Restore.BackgroundImage = RestoreLight.BackgroundImage;
                }
                if (this.WindowState == FormWindowState.Normal)
                {
                    Restore.BackgroundImage = MaximizeLight.BackgroundImage;
                } 
                Minimize.BackgroundImage = MinimizeLight.BackgroundImage;
                
                menuIcon.BackColor = Color.FromArgb(214, 219, 233);
                UserPanel.BackColor = Color.FromArgb(214, 219, 233); ;
                label27.ForeColor = Color.Black;
                menuStrip1.Appearance = appearanceControl2;
                appearanceControl2.Preset = AppearanceControl.enumPresetStyles.Office2007;
                menuStrip1.BackColor = Color.FromArgb(214, 219, 233);
                hELPToolStripMenuItem.BackColor = Color.FromArgb(214, 219, 233);
                fILEToolStripMenuItem.ForeColor = Color.Black;
                eDITToolStripMenuItem.ForeColor = Color.Black;
                vIEWToolStripMenuItem.ForeColor = Color.Black;
                cONVERTToolStripMenuItem.ForeColor = Color.Black;
                dEBUGToolStripMenuItem.ForeColor = Color.Black;
                tOOLSToolStripMenuItem.ForeColor = Color.Black;
                hELPToolStripMenuItem.ForeColor = Color.Black;
                fILEToolStripMenuItem1.ForeColor = Color.Black;
                eDITToolStripMenuItem1.ForeColor = Color.Black;
                vIEWToolStripMenuItem1.ForeColor = Color.Black;
                fAVORITESToolStripMenuItem.ForeColor = Color.Black;
                tOOLSToolStripMenuItem1.ForeColor = Color.Black;
                hELPToolStripMenuItem1.ForeColor = Color.Black;
                recoverLostunsavedFilesToolStripMenuItem.ForeColor = Color.Black;

                foreach (ToolStripMenuItem toolItem in GetItems(menuStrip1))
                {
                    toolItem.ForeColor = Color.Black;
                }
                foreach (ToolStripItem a in toolStripButton32.DropDownItems)
                {
                    ToolStripMenuItem b = a as ToolStripMenuItem;
                    if (b != null) b.ForeColor = Color.Black;
                }
                runUsingBuiltinBrowserToolStripMenuItem.ForeColor = Color.Black;
                runUsingLocalDebuggerToolStripMenuItem.ForeColor = Color.Black;
                manageCustomCompilersToolStripMenuItem.ForeColor = Color.Black;
                textSizeToolStripMenuItem.ForeColor = Color.Black;
                zoomToolStripMenuItem1.ForeColor = Color.Black;
                exportCodeToolStripMenuItem.ForeColor = Color.Black;
                zoomToolStripMenuItem.ForeColor = Color.Black;
                limbaDeScriereToolStripMenuItem.ForeColor = Color.Black;
                translatePseudocodeToolStripMenuItem.ForeColor = Color.Black;
                toolStrip1.Appearance = whiteApparence1;
                customizableToolStrip1.Appearance = whiteApparence1;
                toolStripButton32.ForeColor = Color.Black;
                toolStrip1.BackColor = Color.FromArgb(214, 219, 233);
                customizableToolStrip1.BackColor = Color.FromArgb(214, 219, 233);
                Left.BackColor = Color.FromArgb(214, 219, 233);
                Right.BackColor = Color.FromArgb(214, 219, 233);
                Bottom.BackColor = Color.FromArgb(214, 219, 233);
                Top.BackColor = Color.FromArgb(214, 219, 233);
                TopCover.BackColor = Color.White;
                SearchBox.BackColor = Color.White;
                SearchBox.ForeColor = Color.Black;
                toolStripButton30.BackColor = Color.White;
                toolStripButton30.ForeColor = Color.Black;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.StartColor = Color.FromArgb(214, 219, 233);
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.EndColor = Color.FromArgb(214, 219, 233);
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.StartColor = Color.White;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.EndColor = Color.White;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.TextColor = Color.Black;
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.StartColor = Color.FromArgb(211, 211, 211);
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.EndColor = Color.FromArgb(211, 211, 211);
                dockPanel.Skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.TextColor = Color.Black;


                this.BackColor = Color.FromArgb(214, 219, 233);
                if (Properties.Settings.Default.DisableEnhancedUI == false)
                {

                    try
                    {
                        glow.BackColor = Color.FromArgb(214, 219, 233);
                        glow.Title.BackColor = Color.FromArgb(214, 219, 233);
                        glow.Close.BackgroundImage = glow.CloseLight.BackgroundImage;
                        glow.Restore.BackgroundImage = glow.RestoreLight.BackgroundImage;
                        glow.Minimize.BackgroundImage = glow.MinimizeLight.BackgroundImage;
                    }
                    catch { }
                }
            }

            //end theme code
        }
        public GlowHost glow;
        public Glow wpfwindow;
        int borderSizeX;
        int[] zoomLevels;
        public int zoomLevel = 3;
        public MainForm(GlowHost gl)
        {

            zoomLevels = new int[] { 0, 50, 75, 100, 125, 150, 175, 200, 250, 300, 400 };
            zoomLevel = 3;
            glow = gl;
            try
            {
                string executablePath = Environment.GetCommandLineArgs()[0];
                string executableName = System.IO.Path.GetFileName(executablePath);

                RegistryKey registrybrowser = Registry.CurrentUser.OpenSubKey
                   (@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", true);

                if (registrybrowser == null)
                {
                    RegistryKey registryFolder = Registry.CurrentUser.OpenSubKey
                        (@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl", true);
                    registrybrowser = registryFolder.CreateSubKey("FEATURE_BROWSER_EMULATION");
                }
                registrybrowser.SetValue(executableName, 0x2AF9, RegistryValueKind.DWord); //emulate Internet Explorer 11 in WebBrowser control
                registrybrowser.Close();
            }
            catch { }
            // Other available values for IE emulation
            //
            // 11001 (0x2AF9)
            // Internet Explorer 11. Webpages are displayed in IE11 edge mode, regardless of the !DOCTYPE directive.
            //
            // 11000 (0x2AF8)
            // IE11. Webpages containing standards-based !DOCTYPE directives are displayed in IE11 edge mode. Default value for IE11.
            //
            // 10001 (0x2711)	
            // Internet Explorer 10. Webpages are displayed in IE10 Standards mode, regardless of the !DOCTYPE directive.
            //
            // 10000 (0x02710)	
            // Internet Explorer 10. Webpages containing standards-based !DOCTYPE directives are displayed in IE10 Standards mode. Default value for Internet Explorer 10.
            //
            // 9999 (0x270F)	
            // Windows Internet Explorer 9. Webpages are displayed in IE9 Standards mode, regardless of the !DOCTYPE directive.
            //
            // 9000 (0x2328)	
            // Internet Explorer 9. Webpages containing standards-based !DOCTYPE directives are displayed in IE9 mode. Default value for Internet Explorer 9.
            // Important  In Internet Explorer 10, Webpages containing standards-based !DOCTYPE directives are displayed in IE10 Standards mode.
            //
            // 8888 (0x22B8)	
            // Webpages are displayed in IE8 Standards mode, regardless of the !DOCTYPE directive.
            // 
            // 8000 (0x1F40)	
            // Webpages containing standards-based !DOCTYPE directives are displayed in IE8 mode. Default value for Internet Explorer 8
            // Important  In Internet Explorer 10, Webpages containing standards-based !DOCTYPE directives are displayed in IE10 Standards mode.
            // 
            // 7000 (0x1B58)	
            // Webpages containing standards-based !DOCTYPE directives are displayed in IE7 Standards mode. Default value for applications hosting the WebBrowser Control.
            //
            // EOF
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            InitializeComponent();
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                wpfwindow = new Glow();
                wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
                System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(wpfwindow);
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.Padding = new Padding(5, 5, 5, 5);
                try
                {
                    SetWindowLongPtr(this.Handle, -16, (IntPtr)(0x80000000L | 0x00C00000L | 0x00040000L | 0x00010000L | 0x00020000L | 0x00080000L));
                    SetWindowPos(this.Handle, IntPtr.Zero, 0, 0, 0, 0, SetWindowPosFlags.FrameChanged | SetWindowPosFlags.IgnoreMove | SetWindowPosFlags.IgnoreResize);
                }
                catch { }
            }
            else
            {
                this.Padding = new Padding(0);
                Close.Visible = false;
                Restore.Visible = false;
                Minimize.Visible = false;
                menuIcon.Visible = false;
            }
            int en = 0;
            //this.DoubleBuffered = true;
           /* if (Properties.Settings.Default.Position == true)
            {
                if (Properties.Settings.Default.WindowsSize != new Size(0, 0))
                {
                  //  glow.Size = Properties.Settings.Default.WindowsSize;
                    this.Size = Properties.Settings.Default.WindowsSize;
                }
                if (Properties.Settings.Default.WindowsPos != new Point(0, 0))
                {
                   // glow.Location = Properties.Settings.Default.WindowsPos;
                    this.Location = Properties.Settings.Default.WindowsPos;
                }
                if (IsOnScreen(this) == false)
                {
                   // glow.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
                 //   glow.Size = new Size(1258, 697);
                    this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
                    this.Size = new Size(1258, 697);
                }
                oldSize = this.Size;
                oldLoc = this.Location;
                /*if (Properties.Settings.Default.IsMaximized == true)
                {

                    glow.Restore.PerformClick();
                }*/
              /*  if (!IsMaximised)
                {
                    glow.Location = new Point(this.Location.X, this.Location.Y - Close.Height- 5);

                    this.OnResize(new EventArgs());
                }
            }
            else
            {
                this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
                this.Size = new Size(1258, 697);
                if (!IsMaximised)
                {
                    glow.Location = new Point(this.Location.X, this.Location.Y - Close.Height - 5);

                    this.OnResize(new EventArgs());
                }
            }*/
            menuStrip1BackColor = menuStrip1.BackColor;
            menuStrip1ForeColor = fILEToolStripMenuItem.ForeColor;
            toolStripButtonForeColor = toolStripButton32.ForeColor;
            themeColor = Color.Black;
            topCoverBackColor = TopCover.BackColor;
            searchBoxBackColor = SearchBox.BackColor;
            searchBoxForeColor = SearchBox.ForeColor;
            sc11 = dockPanel.Skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.StartColor;
            sc12 = dockPanel.Skin.DockPaneStripSkin.DocumentGradient.DockStripGradient.EndColor;
            sc21 = dockPanel.Skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.StartColor;
            sc22 = dockPanel.Skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.EndColor;
            sc23 = dockPanel.Skin.DockPaneStripSkin.DocumentGradient.ActiveTabGradient.TextColor;
            sc31 = dockPanel.Skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.StartColor;
            sc32 = dockPanel.Skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.EndColor;
            sc33 = dockPanel.Skin.DockPaneStripSkin.DocumentGradient.InactiveTabGradient.TextColor;
            thisBackColor = this.BackColor;
            ThemeCode();


            zoom = new Zoom(this);
            //Application.AddMessageFilter(this);
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(this.ShowSplashScreen));
            if (Properties.Settings.Default.UILang == 0) thread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) thread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            thread.Start();
            if (Properties.Settings.Default.UpdateInt != 0)
            {
                Loop.Interval = Properties.Settings.Default.UpdateInt;
            }
            if (Properties.Settings.Default.UILang == 0)
            {
                NewTabTitle = "Untitled";
                NoBrowserInstalled = "There is no Internet browser installed on this computer.\n\nIn order to use this feature, a browser has to be installed on your machine. Visit www.browserchoice.eu to download a web browser for your computer if you are based in the European Union or, if located in any other country, visit www.whatbrowser.org.";
                AlreadySignedIn = "You have already signed in into the application.\n\nIn order to sign out, click on your picture in the main window, then choose Sign out and switch to another account.";
                ErrorDownloadingCode = "An error has occured when trying to download the #code.\n\nMake sure you are connected to the Internet, check the #code for typing errors and try again in a little while.\nError description: ";
                Downloaded = "(unsaved on the local storage)";
                NoSuchFile = "The specified file was not found on the disk.\n\nThe file may have been deleted or moved.";
                ZoomText = "Insert a new value for the zoom level, ranging from -10 to +20.";
                BackOp = "The application is waiting for a background operation to complete. \n\nYou'll be able to perform the requested operation in a few moments. Report this to Democratic Instruments if this happens frequently during your work time.";
                ENToRO = "Do you want to translate this file?\n\nPseudocode will translate this file from English (United Kingdom) to română. The keywords will be translated to the new language and also the file's Language attribute will be set to the value of the new language. Please note that the file will be automatically saved after this process completes. Make a copy of the file if you wish to keep the original.";
                ROToEN = "Do you want to translate this file?\n\nPseudocode will translate this file from română to English (United Kingdom). The keywords will be translated to the new language and also the file's Language attribute will be set to the value of the new language. Please note that the file will be automatically saved after this process completes. Make a copy of the file if you wish to keep the original.";
                NothingToRecover = "There are no files to be recovered available.";
               // SuccessfullyInstalled = "Do you want to restart the application?\n\nThe package has been successfully installed, but the application has to be restarted. Save your work before proceeding.";
                SuccessfullyInstalled = "Installation of the opened packages has finished. Check Tools - Extensions and Updates for information about the available packages.";
                ErrorInstalled = "An error has occured while attempting to install the package.\n\n";
                OverwriteInstall = "A package with the same name is already installed.\n\nUninstall that first before attempting to install this replacement.";
                RestartNeeded = "The application will restart. Save your work before continuing.";
                CannotOpen = "The following file is not a valid pseudocode file and therefor cannot be opened in the application.\n\n";
                EraseCoverage = "Code coverage data has been erased for the current pseudocode.\n\nCompile and run the pseudocode file again in order to generate new data.";
                AnalizeCovergaeData = "Do you want to continue?\n\nThis will open the file containing coverage data in the Notepad external text editor. The file is designed for advanced users and contains only raw C++ code.";
                NoCoverageData = "There is no coverage data available right now.\n\nCompile and run the pseudocode file in order to be able to view coverage data for the current pseudocode.";
                NotAvailableUILang = "This feature is not available in the current display language.";
                CloseT = "Close (Alt+F4)";
                RestoreT = "Restore down";
                MaximizeT = "Maximize";
                MinimizeT = "Minimize";
                ConnectedAs = "Cloud settings configuration for: ";
                addToFavorites = "Type the name you would like this page to be listed in your favorites list.";
                invalidName = "A file name cannot contain any of the following:\n\nForbidden characters: \\ / :  * ? \" < > | \nForbidden patterns: any filename conatining only the dot ('.') character.\nForbidden filenames (excluding extension): CON, PRN, AUX, CLOCK$, NUL, COM0, COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8, COM9, LPT0, LPT1, LPT2, LPT3, LPT4, LPT5, LPT6, LPT7, LPT8, and LPT9.";
                runText = "Execute";
            }
            else
            {
                NewTabTitle = "Neintitulat";
                NoBrowserInstalled = "Niciun browser Web nu este instalat pe acest computer.\n\nPentru a utiliza această caracteristică, un browser trebuie să fie instalat pe computer. Pentru a descărca un browser, vizitați www.browserchoice.eu dacă sunteți în Uniunea Europeană sau www.whatbrowser.org dacă sunteți din oricare altă țară.";
                AlreadySignedIn = "Ați făcut deja sign in în aplicație.\n\nPentru a face sign out, faceți clic pe imaginea dvs. în fereastra principală și apoi alegeți Sign out și comutare la alt cont.";
                ErrorDownloadingCode = "A apărut o eroare la descărcarea #codului.\n\nVerificați conexiunea la Internet, revizuiți #codul pentru eventuale greșeli de scriere și încercați din nou mai târziu.\nDescriere eroare: ";
                Downloaded = "(nesalvat local)";
                NoSuchFile = "Nu s-a găsit fișierul specificat pe disc.\n\nAcesta ar fi putut să fi fost mutat sau șters.";
                ZoomText = "Inserați o valoare naturală pentru nivelul de zoom, din intervalul -10 până la +20.";
                BackOp = "Aplicația așteaptă finalizarea unei operații în fundal.\n\nVeți putea efectua operația solicitată în câteva momente. Raportați acest lucru la Democratic Instruments dacă se întâmplă des în timpul lucrului cu aplicația.";
                ENToRO = "Sigur traduceți acest fișier?\n\nPseudocode va traduce acest fișier din English (United Kingdom) în română. Cuvintele-cheie vor fi traduse în noua limbă, iar proprietatea Limbă a fișierului pseudocod va fi setată la noua limbă. De asemenea, fișierul va fi salvat odată ce acest proces va fi finalizat. Faceți o copie a fișierului pseudocod dacă doriți să păstrați originalul.";
                ROToEN = "Sigur traduceți acest fișier?\n\nPseudocode va traduce acest fișier din română în English (United Kingdom). Cuvintele-cheie vor fi traduse în noua limbă, iar proprietatea Limbă a fișierului pseudocod va fi setată la noua limbă. De asemenea, fișierul va fi salvat odată ce acest proces va fi finalizat. Faceți o copie a fișierului pseudocod dacă doriți să păstrați originalul.";
                NothingToRecover = "Nu există fișiere disponibile pentru a fi recuperate.";
              //  SuccessfullyInstalled = "Reporniți aplicația?\n\nPachetul s-a instalat cu succes, însă aplicația trebuie repornită pentru a încărca noul pachet. Salvați-vă munca înainte de a continua.";
                SuccessfullyInstalled = "Instalarea pachetelor deschise s-a finalizat. Mergeți la Unelte - Extensii  și Actualizări pentru a verifica starea pachetelor instalate.";
                ErrorInstalled = "S-a produs o eroare la instalarea pachetului.\n\n";
                OverwriteInstall = "Un pachet cu un nume similar este deja instalat.\n\nDezinstalați acel pachet înainte de a încerca instalarea acestui înlocuitor.";
                RestartNeeded = "Aplicația va reporni. Salvați-vă munca înainte de a continua.";
                CannotOpen = "Următorul fișier nu este un fișier pseudocod valid și nu poate fi deschis în cadrul aplicației.";
                EraseCoverage = "Datele coverage au fost șterse pentru pseudocodul curent.\n\nCompilați și executați din nou fișierul pseudocod pentru a genera date noi.";
                AnalizeCovergaeData = "Continuați?\n\nAceasta va deschide fișierul cu datele coverage în editorul text extern Notepad. Acest fișier este destinat numai utilizatorilor avansați și conține cod C++ brut.";
                NoCoverageData = "Nu există date coverage colectate.\n\nCompilați și executați pseudocodul pentru a putea vedea datele coverage în cadrul aplicației.";
                NotAvailableUILang = "Caracteristica solicitată nu este disponibilă în limba curentă a interfeței.";
                CloseT = "Închidere (Alt+F4)";
                RestoreT = "Restabilire jos";
                MaximizeT = "Maximizare";
                MinimizeT = "Minimizare";
                ConnectedAs = "Configurări sincronizare cloud pentru: ";
                addToFavorites = "Scrieți numele după care doriți să regăsiți pagina curentă în lista favoritelor.";
                invalidName = "Un nume de fișier nu poate conține niciunul din următoarele caractere:\n\nCaractere interzise: \\ / :  * ? \" < > | \nPatternuri interzise: orice nume de fi;ier ce con'ine numai caracterul punct ('.').\nNume de fișier interzise (excluzând extensia): CON, PRN, AUX, CLOCK$, NUL, COM0, COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8, COM9, LPT0, LPT1, LPT2, LPT3, LPT4, LPT5, LPT6, LPT7, LPT8, and LPT9.";
                runText = "Executare";
            }
            //load plugins
            try
            {
                _Plugins = new Dictionary<string, VPlugin>();
                //ICollection<IPlugin> plugins = PluginLoader.LoadPlugins("Plugins");
                ICollection<VPlugin> plugins = GenericPluginLoader<VPlugin>.LoadPlugins(Application.StartupPath);
                try
                {
                    foreach (var item in plugins)
                    {
                        _Plugins.Add(item.Name, item);
                    }
                }
                catch { }
            }
            catch { }
            //end load plugins
            if (Properties.Settings.Default.UpgradeRequired)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.UpgradeRequired = false;
                Properties.Settings.Default.Save();
            }
            /*if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
            {
                var dropBoxStorage = new CloudStorage();
                var dropBoxConfig = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox);
                ICloudStorageAccessToken accessToken;
                using (var fs = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat", FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    accessToken = dropBoxStorage.DeserializeSecurityToken(fs);
                }
                try
                {
                    var storageToken = dropBoxStorage.Open(dropBoxConfig, accessToken);
                    dropBoxStorage.DownloadFile("/user.config", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
                    string text = System.IO.File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.config");
                    string[] rez = text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                    Properties.Settings.Default.Position = Convert.ToBoolean(rez[0]);
                    Properties.Settings.Default.Language = Convert.ToInt32(rez[1]);
                    Properties.Settings.Default.AutoSaveInterval = Convert.ToInt32(rez[2]);
                    Properties.Settings.Default.Zoom = Convert.ToInt32(rez[3]);
                    Properties.Settings.Default.WasWelcomeShown = Convert.ToBoolean(rez[4]);
                    Properties.Settings.Default.DownloadImages = Convert.ToBoolean(rez[5]);
                    Properties.Settings.Default.UseBar = Convert.ToBoolean(rez[6]);
                    Properties.Settings.Default.Save();
                    wasDownloaded = true;
                }
                catch { }
            }*/

            DefaultSearchBoxText = SearchBox.Text;
            mLastState = this.WindowState;
            //New();
            listBox1.Tag = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
                if (Properties.Settings.Default.AppName != "") AppName = Properties.Settings.Default.AppName;
                else AppName = "Pseudocode";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
                if (Properties.Settings.Default.AppName != "") AppName = Properties.Settings.Default.AppName;
                else AppName = "Pseudocode";
            }

        }
        private void Mf_Click(object sender, EventArgs e)
        {
            //  if (ActiveMdiChild is Form1)
            //  {
            try
            {
                ToolStripMenuItem ts = (ToolStripMenuItem)sender;
                StreamReader sr = new StreamReader(ts.Tag.ToString());
                string[] st = sr.ReadToEnd().Split('\n');
                sr.Close();
                foreach (string s in st)
                {
                    if (s.StartsWith("URL ="))
                    {
                        ShowBrowser(s.Replace("URL = ", ""));
                        break;
                    }
                }
            }
            catch
            {

            }
            // }
            // else if (ActiveMdiChild is Browser)
            // {
            //  Browser br = (Browser)ActiveMdiChild;
            // br.webBrowser1.Navigate()
            //  }
        }
        bool alreadyFocused;
        private void ToolStripButton30_Leave(object sender, EventArgs e)
        {
            alreadyFocused = false;
        }

        private void ToolStripButton30_MouseUp(object sender, MouseEventArgs e)
        {
            // Select all text only if the mouse isn't down.
            // This makes tabbing to the textbox give focus.
            if (MouseButtons == MouseButtons.None && toolStripButton30.SelectionLength == 0)
            {
                this.toolStripButton30.SelectAll();
                alreadyFocused = true;
            }
        }

        private void ToolStripButton30_GotFocus(object sender, EventArgs e)
        {
            // Web browsers like Google Chrome select the text on mouse up.
            // They only do it if the textbox isn't already focused,
            // and if the user hasn't selected all text.
            if (!alreadyFocused && toolStripButton30.SelectionLength == 0)
            {
                alreadyFocused = true;
                this.toolStripButton30.SelectAll();
            }

        }

        public void New(string title)
        {
            this.Controls.OfType<MdiClient>().ElementAt(0).BackColor = Color.Black;
            MdiClient client = Controls.OfType<MdiClient>().First();
            client.Dock = DockStyle.Top;
            client.Dock = DockStyle.Fill;
            client.Top = toolStrip1.Bottom;
            client.BringToFront();
            Form1 ed = new Form1();
            if (ed.PseudocodeLang == 0)
            {
                ed.fileProperties.Add("language:0");
                fromEnglishCodeToolStripMenuItem.Checked = true;
                fromRomanianPseudoccodeToolStripMenuItem.Checked = false;
                userdefinedToolStripMenuItem.Checked = false;
            }
            if (ed.PseudocodeLang == 1)
            {
                ed.fileProperties.Add("language:1");
                fromEnglishCodeToolStripMenuItem.Checked = false;
                fromRomanianPseudoccodeToolStripMenuItem.Checked = true;
                userdefinedToolStripMenuItem.Checked = false;
            }
            if (ed.PseudocodeLang == 2)
            {
                ed.fileProperties.Add("language:2");
                fromEnglishCodeToolStripMenuItem.Checked = false;
                fromRomanianPseudoccodeToolStripMenuItem.Checked = false;
                userdefinedToolStripMenuItem.Checked = true;
            }
            ed.FormTitle = title;
            ed.Show(dockPanel, DockState.Document);
            ed.pseudocodeEd.Focus();
            ed.lastLine = -1;
            dockPanel.BringToFront();
            Top.BringToFront();
            Left.BringToFront();
            Right.BringToFront();
            Bottom.BringToFront();
            Bottom.Visible = true;
            TopCover.BringToFront();
            BottomDown.BringToFront();
            if (Properties.Settings.Default.ShowStatusBar == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.statusBar1.Visible = true;
                    frm.BottomDown.Visible = true;
                    frm.BottomDown.Top = frm.Height - frm.statusBar1.Height - 2;
                }
                statusBarToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.statusBar1.Visible = false;
                    frm.BottomDown.Visible = false;

                }
                statusBarToolStripMenuItem.Checked = false;
            }
            if (Properties.Settings.Default.ShowAdvancedPanel == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer3.Panel2Collapsed = false;
                    frm.splitContainer3.Panel2.Show();
                }
                advancedCommandsPanelToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer3.Panel2Collapsed = true;
                    frm.splitContainer3.Panel2.Hide();
                }
                advancedCommandsPanelToolStripMenuItem.Checked = false;
            }
            if (Properties.Settings.Default.ShowConvertedCode == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer2.Panel2Collapsed = false;
                    frm.splitContainer2.Panel2.Show();
                }
                convertedCodeLogicSchemaToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer2.Panel2Collapsed = true;
                    frm.splitContainer2.Panel2.Hide();
                }
                convertedCodeLogicSchemaToolStripMenuItem.Checked = false;
            }
            if (Properties.Settings.Default.ShowCommandBar == true)
            {
                toolStrip1.Visible = true;
                commandBarToolStripMenuItem.Checked = true;
            }
            else
            {
                toolStrip1.Visible = false;
                commandBarToolStripMenuItem.Checked = false;
            }
            UpdateUXLines();

            if (Properties.Settings.Default.ShowQuickSearch == true)
            {
                quickMenuSearchBoxToolStripMenuItem.Checked = true;
                SearchBox.Visible = true;
            }
            else
            {
                quickMenuSearchBoxToolStripMenuItem.Checked = false;
                SearchBox.Visible = false;
            }
            if (Properties.Settings.Default.ShowVisualScrollBar == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer4.Panel2Collapsed = false;
                    frm.splitContainer4.Panel2.Show();
                    frm.pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Horizontal;
                }
                visualScrollBarToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer4.Panel2Collapsed = true;
                    frm.splitContainer4.Panel2.Hide();
                    frm.pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Both;
                }
                visualScrollBarToolStripMenuItem.Checked = false;
            }
            switch (Properties.Settings.Default.Zoom)
            {
                case 0:
                    {
                        zoom100.Checked = true;
                        break;
                    }
                case 1:
                    {
                        zoom200.Checked = true;
                        break;
                    }
                case 2:
                    {
                        zoom300.Checked = true;
                        break;
                    }
                case 3:
                    {
                        zoom400.Checked = true;
                        break;
                    }
                case -1:
                    {
                        zoom75.Checked = true;
                        break;
                    }
                case -2:
                    {
                        zoom50.Checked = true;
                        break;
                    }
                case -3:
                    {
                        zoom25.Checked = true;
                        break;
                    }
                default:
                    {
                        customizeToolStripMenuItem.Checked = true;
                        break;
                    }
            }
            UpdateMenus();
            while (toolStripButton32.DropDownItems.Count >= 6) toolStripButton32.DropDownItems.RemoveAt(5);
            AddCompilers();

        }



        public bool IsOnScreen(Form form)
        {
            Screen[] screens = Screen.AllScreens;
            foreach (Screen screen in screens)
            {
                Rectangle formRectangle = new Rectangle(form.Left, form.Top, form.Width, form.Height);

                if (screen.WorkingArea.Contains(formRectangle))
                {
                    return true;
                }
            }

            return false;
        }
        public static void UnZip(string zipFile, string folderPath)
        {
            if (!File.Exists(zipFile))
                throw new FileNotFoundException();

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            Shell32.Shell objShell = new Shell32.Shell();
            Shell32.Folder destinationFolder = objShell.NameSpace(folderPath);
            Shell32.Folder sourceFile = objShell.NameSpace(zipFile);

            foreach (var file in sourceFile.Items())
            {
                destinationFolder.CopyHere(file, 4 | 16);
            }
        }
        private static void DirectoryCopy(
        string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }


            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                file.CopyTo(temppath, true);
            }

            // If copySubDirs is true, copy the subdirectories.
            if (copySubDirs)
            {

                foreach (DirectoryInfo subdir in dirs)
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine(destDirName, subdir.Name);

                    // Copy the subdirectories.
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
        bool wasMoved = false;
        float dx, dy;
        public void AddFavorites()
        {
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode")) Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode");
            foreach (string file in Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode"))
            {
                ToolStripMenuItem mf = new ToolStripMenuItem();
                mf.Text = Path.GetFileNameWithoutExtension(file); //.FullName;
                mf.Tag = new FileInfo(file).FullName;
                if (Properties.Settings.Default.UITheme == 1) mf.ForeColor = Color.Black;
                else mf.ForeColor = Color.White;
                fAVORITESToolStripMenuItem.DropDownItems.Add(mf);
                mf.Click += Mf_Click;
            }
        }
        public void AddCompilers()
        {
            int ic = 0;
            toolStripMenuItem55.Visible = false;
            foreach (string file in Properties.Settings.Default.Compilers)
            {
                toolStripMenuItem55.Visible = true;
                ToolStripMenuItem mf = new ToolStripMenuItem();
                string[] s = file.Split(';');
                mf.Text = s[0]; //.FullName;
                mf.Tag = ic;
                if (Properties.Settings.Default.UITheme == 1) mf.ForeColor = Color.Black;
                else mf.ForeColor = Color.White;
                toolStripButton32.DropDownItems.Add(mf);
                mf.Click += Mf_Click1;
                ic++;
            }
            if (Properties.Settings.Default.DefaultDebugMode == 1)
            {
                runUsingLocalDebuggerToolStripMenuItem.PerformClick();
            }
            else if (Properties.Settings.Default.DefaultDebugMode == 2)
            {
                runUsingBuiltinBrowserToolStripMenuItem.PerformClick();
            }
            else
            {
                toolStripButton32.DropDownItems[Convert.ToInt32(Properties.Settings.Default.DefaultDebugMode) + 2].PerformClick();
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            if (Properties.Settings.Default.Compilers == null) Properties.Settings.Default.Compilers = new System.Collections.Specialized.StringCollection();
            AddCompilers();
            toolStripButton30.GotFocus += ToolStripButton30_GotFocus;
            toolStripButton30.MouseUp += ToolStripButton30_MouseUp;
            toolStripButton30.Leave += ToolStripButton30_Leave;
            AddFavorites();
            Graphics g = this.CreateGraphics();
            try
            {
                dx = g.DpiX;
                dy = g.DpiY;
            }
            finally
            {
                g.Dispose();
            }
            UpdateChecker.RunWorkerAsync();
            if (!File.Exists(Application.StartupPath + "\\custom-keywords.txt")) userdefinedToolStripMenuItem.Visible = false;
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                try
                {
                    for (int i = 1; i < args.Length; i++)
                    {
                        if (args[i].EndsWith("psc"))
                        {
                            System.IO.TextReader tw = new System.IO.StreamReader(args[i]);
                            FileInfo fi = new FileInfo(args[i]);
                            New(Path.GetFileNameWithoutExtension(fi.Name));
                            Form1 frm = ActiveMdiChild as Form1;
                            string citit = tw.ReadToEnd();
                            string[] parcurgere = citit.Split('\n');
                            bool incepProp = false;
                            frm.fileProperties.Clear();
                            for (int x = 0; x < parcurgere.Length; x++)
                            {
                                if (parcurgere[x] == "<properties>") incepProp = true;
                                if (incepProp == true && parcurgere[x] != "<properties>") frm.fileProperties.Add(parcurgere[x]);
                                else if (parcurgere[x] != "<properties>")
                                    if (x == 0) frm.pseudocodeEd.Text += parcurgere[x];
                                    else frm.pseudocodeEd.Text += "\n" + parcurgere[x];
                            }
                            //frm.pseudocodeEd.Text = tw.ReadToEnd();
                            ExTag oldEX = (ExTag)frm.pseudocodeEd.Tag;
                            ExTag exTag = new ExTag();
                            exTag.Add("issaved", true);
                            exTag.Add("filename", args[i]);
                            exTag.Add("uniqueString", oldEX.Get("uniqueString"));
                            frm.pseudocodeEd.Tag = exTag;
                            tw.Close();

                            //this.Text = Path.GetFileNameWithoutExtension(fi.Name);
                            System.Collections.Specialized.StringCollection col = Properties.Settings.Default.RecentFiles;
                            bool isAdded = false;
                            foreach (string s in col)
                            {
                                if (s == args[i])
                                {
                                    isAdded = true;
                                }
                            }
                            if (isAdded == false) col.Add(args[i]);
                            Properties.Settings.Default.RecentFiles = col;
                            Properties.Settings.Default.Save();
                            AddFileToRecentFilesList(args[i]);


                            bool close = frm.LanguageIdentification(true);
                            if (close == true) New(NewTabTitle + " " + (dockPanel.Panes.Count + 1).ToString());
                        }
                        else if (args[i].EndsWith("pscpackage"))
                        {
                            try
                            {
                                File.Move(args[i], Path.GetDirectoryName(args[i]) + "\\" + Path.GetFileNameWithoutExtension(args[i]) + ".zip");
                                if (Directory.Exists(Application.StartupPath + "\\ext-temp")) Directory.Delete(Application.StartupPath + "\\ext-temp", true);
                                Directory.CreateDirectory(Application.StartupPath + "\\ext-temp");
                                UnZip(Path.GetDirectoryName(args[i]) + "\\" + Path.GetFileNameWithoutExtension(args[i]) + ".zip", Application.StartupPath + "\\ext-temp");
                                string folder = Directory.GetDirectories(Application.StartupPath + "\\ext-temp")[0];
                                StreamReader sr = new StreamReader(folder + "\\" + "version.txt");
                                string version = Regex.Split(sr.ReadToEnd(), "\r\n")[0];
                                sr.Close();
                                StreamReader sr2 = new StreamReader(Application.StartupPath + "\\package-list.txt");
                                string pkglist = sr2.ReadToEnd();
                                string[] pkg = Regex.Split(pkglist, "\r\n");
                                sr2.Close();
                                string oldversion = "";
                                try
                                {
                                    StreamReader sr3 = new StreamReader(Application.StartupPath + "\\" + Path.GetFileName(folder) + "\\version.txt");
                                    oldversion = Regex.Split(sr3.ReadToEnd(), "\r\n")[0];
                                    sr3.Close();
                                }
                                catch
                                {

                                }
                                bool already = false;
                                foreach (string st in pkg)
                                {
                                    if (st == Path.GetFileName(folder))
                                    {
                                        DialogResult dr = MessageBox.Show(OverwriteInstall + "\n\n" + st, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        already = true;
                                        goto aici;
                                        //if (dr == System.Windows.Forms.DialogResult.No) return;
                                        //already = true;
                                        //break;
                                    }
                                }
                                DirectoryCopy(Application.StartupPath + "\\ext-temp" + "\\" + Path.GetFileName(folder), Application.StartupPath + "\\" + Path.GetFileName(folder), true);
                                Directory.Delete(Application.StartupPath + "\\ext-temp", true);
                                // if (already == false)
                                // {
                                StreamWriter sw = new StreamWriter(Application.StartupPath + "\\package-list.txt");
                                sw.Write(pkglist + "\r\n" + Path.GetFileName(folder));
                                sw.Close();
                            // }
                            aici:
                                File.Move(Path.GetDirectoryName(args[i]) + "\\" + Path.GetFileNameWithoutExtension(args[i]) + ".zip", args[i]);
                                bool canRestart = true;
                                for (int j = i + 1; j < args.Length; j++)
                                {
                                    if (args[j].EndsWith("pscpackage")) canRestart = false;
                                }
                                if (i == args.Length - 1 && canRestart == true && already == true) New(NewTabTitle + " " + (dockPanel.Panes.Count + 1).ToString());
                                if (already == false)
                                {
                                    if (canRestart == true)
                                    {
                                        New(NewTabTitle + " " + (dockPanel.Panes.Count + 1).ToString());
                                        DialogResult dr2 = MessageBox.Show(new Form() { TopMost = true }, SuccessfullyInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        /*if (dr2 == System.Windows.Forms.DialogResult.Yes)
                                        {
                                            Process.Start(Application.ExecutablePath);
                                            Process.GetCurrentProcess().Kill();
                                        }*/
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ErrorInstalled + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                New(NewTabTitle + " " + (dockPanel.Panes.Count + 1).ToString());

                            }

                        }
                        else
                        {
                            CasetaDeMesaj(this, CannotOpen, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                catch
                {
                }
            }
            else
            {
                New(NewTabTitle + " " + (dockPanel.Panes.Count + 1).ToString());
            }
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
            {
                try
                {
                    //UserPanel.Visible = true; //TEMP
                    OnClientSizeChanged(e);
                    using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                    {
                        pictureBox2.Image = Image.FromStream(stream);
                    }
                    pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                    using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                    {
                        pictureBox3.Image = Image.FromStream(stream);
                    }
                    pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
                    label27.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                    label28.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                    signInToolStripMenuItem.Text = ConnectedAs + File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                    label27.Left = 15;
                    UserPanel.Width = label27.Width + 25 + pictureBox2.Width;
                    if (SearchBox.Visible) UserPanel.Left = SearchBox.Left - UserPanel.Width - 3;
                    else UserPanel.Left = this.Width - UserPanel.Width - 8;
                    UserPanel.BringToFront();
                    wasMoved = true;
                }
                catch { }
            }
            if (Properties.Settings.Default.ShowStatusBar == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.statusBar1.Visible = true;
                    frm.BottomDown.Visible = true;
                    frm.BottomDown.Top = frm.Height - frm.statusBar1.Height - 2;
                }
                statusBarToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.statusBar1.Visible = false;
                    frm.BottomDown.Visible = false;

                }
                statusBarToolStripMenuItem.Checked = false;
            }
            if (Properties.Settings.Default.ShowAdvancedPanel == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer3.Panel2Collapsed = false;
                    frm.splitContainer3.Panel2.Show();
                }
                advancedCommandsPanelToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer3.Panel2Collapsed = true;
                    frm.splitContainer3.Panel2.Hide();
                }
                advancedCommandsPanelToolStripMenuItem.Checked = false;
            }
            if (Properties.Settings.Default.ShowConvertedCode == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer2.Panel2Collapsed = false;
                    frm.splitContainer2.Panel2.Show();
                }
                convertedCodeLogicSchemaToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer2.Panel2Collapsed = true;
                    frm.splitContainer2.Panel2.Hide();
                }
                convertedCodeLogicSchemaToolStripMenuItem.Checked = false;
            }
            if (Properties.Settings.Default.ShowCommandBar == true)
            {
                toolStrip1.Visible = true;
                commandBarToolStripMenuItem.Checked = true;
            }
            else
            {
                toolStrip1.Visible = false;
                commandBarToolStripMenuItem.Checked = false;
            }
            UpdateUXLines();

            if (Properties.Settings.Default.ShowQuickSearch == true)
            {
                quickMenuSearchBoxToolStripMenuItem.Checked = true;
                SearchBox.Visible = true;
            }
            else
            {
                quickMenuSearchBoxToolStripMenuItem.Checked = false;
                SearchBox.Visible = false;
            }
            if (Properties.Settings.Default.ShowVisualScrollBar == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer4.Panel2Collapsed = false;
                    frm.splitContainer4.Panel2.Show();
                    frm.pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Horizontal;
                }
                visualScrollBarToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer4.Panel2Collapsed = true;
                    frm.splitContainer4.Panel2.Hide();
                    frm.pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Both;
                }
                visualScrollBarToolStripMenuItem.Checked = false;
            }
            switch (Properties.Settings.Default.Zoom)
            {
                case 0:
                    {
                        zoom100.Checked = true;
                        break;
                    }
                case 1:
                    {
                        zoom200.Checked = true;
                        break;
                    }
                case 2:
                    {
                        zoom300.Checked = true;
                        break;
                    }
                case 3:
                    {
                        zoom400.Checked = true;
                        break;
                    }
                case -1:
                    {
                        zoom75.Checked = true;
                        break;
                    }
                case -2:
                    {
                        zoom50.Checked = true;
                        break;
                    }
                case -3:
                    {
                        zoom25.Checked = true;
                        break;
                    }
                default:
                    {
                        customizeToolStripMenuItem.Checked = true;
                        break;
                    }
            }
            LoadUserSettings();
            string[] files = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\AutoSave");
            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                if (fi.LastWriteTime < DateTime.Now.AddDays(-4))
                {
                    //MessageBox.Show(fi.FullName);
                    fi.Delete();
                }
            }
            files = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\AutoSave");
            foreach (string file in files)
            {
                ToolStripMenuItem mf = new ToolStripMenuItem();
                mf.Text = new FileInfo(file).Name; //.FullName;
                mf.Tag = new FileInfo(file).FullName;
                if (Properties.Settings.Default.UITheme == 1) mf.ForeColor = Color.Black;
                else mf.ForeColor = Color.White;
                recoverLostunsavedFilesToolStripMenuItem.DropDownItems.Add(mf);
                mf.Click += mf_Click;
            }
            toolStrip1.UserData = MainToolStripUserSettings;
            if (toolStripButton32.Visible == true) toolStripButton32.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            // if (toolStrip1.Visible) toolStrip1.BringToFront();
            // dockPanel.BringToFront();

            if (Properties.Settings.Default.Position == true)
            {
                if (Properties.Settings.Default.WindowsSize != new Size(0, 0))
                {
                    if (Properties.Settings.Default.DisableEnhancedUI == false)
                    {
                        this.Size = glow.Size = Properties.Settings.Default.WindowsSize;
                    }
                    else
                    {
                        //this.Padding = new Padding(0, 0, 0, 0);
                        this.Size = Properties.Settings.Default.WindowsSize;
                    }
                }
                if (Properties.Settings.Default.WindowsPos != new Point(0, 0))
                {
                    this.Location = Properties.Settings.Default.WindowsPos;
                    
                }
                if (IsOnScreen(this) == false)
                {
                    this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
                    if (Properties.Settings.Default.DisableEnhancedUI == false)
                    {
                        this.Size = glow.Size = new Size(1258, 697);
                    }
                    else
                    {
                        this.Size = new Size(1258, 697);
                    }
                }
                oldSize = this.Size;
                oldLoc = this.Location;
               /* if (Properties.Settings.Default.IsMaximized == true)
                {
                    glow.Restore.PerformClick();
                    glow.Size = Properties.Settings.Default.WindowsSize;
                }*/
                if (Properties.Settings.Default.DisableEnhancedUI == false)
                {

                    if (!IsMaximised)
                    {
                        glow.doNotAlterLocation = true;
                        glow.Location = new Point(this.Location.X, this.Location.Y - Close.Height - 5);
                        glow.doNotAlterLocation = false;
                        //this.OnResize(e);
                    }
                }
                else
                {
                    if (Properties.Settings.Default.IsMaximized == true)
                    {

                        this.WindowState = FormWindowState.Maximized;
                    }
                }
            }
            else
            {
                this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
                this.Size = new Size(1258, 697);
                if (Properties.Settings.Default.DisableEnhancedUI == false)
                {

                    if (!IsMaximised)
                    {
                        glow.Location = new Point(this.Location.X, this.Location.Y - Close.Height - 5);

                        // this.OnResize(e);
                    }
                }
            }

            /*glow.Visible = true;
            this.Visible = true;
            glow.TopMost = true;
            this.TopMost = true;
            glow.TopMost = false;
            this.TopMost = false;
            glow.Opacity = 1;
            this.Opacity = 1;*/

            DateTime beg = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime now = DateTime.Now;
            TimeSpan ts = now - beg;
            int decValue = Convert.ToInt32(ts.TotalSeconds);
            if (Properties.Settings.Default.EnableNewUI == true) UpdateBorder(decValue);

            if (Properties.Settings.Default.DisableEnhancedUI == true) done = true;
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                wpfwindow.Left = this.Location.X - 10;
                wpfwindow.Top = this.Location.Y - 10;
                wpfwindow.Width = this.Width + 20;
                wpfwindow.Height = this.Height + 20;
                if (Properties.Settings.Default.DynamicColor == true) wpfwindow.Foreground = border;
                else wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                wpfwindow.Show();
            }
            SetForegroundWindow(this.Handle);
            OnResize(e);
            
            Form1 frma = ActiveMdiChild as Form1;
            frma.pseudocodeEd.Focus();
            //MessageBox.Show(frma.splitContainer3.SplitterDistance.ToString());
            backgroundWorker1.RunWorkerAsync();
            base.OnLoad(e);
        }



        private void Form1_Load(object sender, EventArgs e)
        {
        }

        void mf_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem ts = (ToolStripMenuItem)sender;
            New(Path.GetFileName(ts.Tag.ToString()));
            System.IO.TextReader tw = new System.IO.StreamReader(ts.Tag.ToString());

            // write a line of text to the file
            Form1 frm = ActiveMdiChild as Form1;
            frm.pseudocodeEd.Text = tw.ReadToEnd();

            // close the stream
            tw.Close();
        }
        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("shell32.dll")]
        public static extern void SHAddToRecentDocs(UInt32 uFlags,
            [MarshalAs(UnmanagedType.LPWStr)] String pv);

        enum ShellAddRecentDocs
        {
            SHARD_PIDL = 0x00000001,
            SHARD_PATHA = 0x00000002,
            SHARD_PATHW = 0x00000003
        }
        void AddFileToRecentFilesList(string fileName)
        {
            SHAddToRecentDocs((uint)ShellAddRecentDocs.SHARD_PATHW, fileName);
        }
        private void Form1_Deactivate(object sender, EventArgs e)
        {
        }
        public void UpdateTitle()
        {
            if (UserPanel.Visible == true)
            {
                if (ActiveMdiChild is Form1)
                {
                    Title.Left = menuIcon.Width + fILEToolStripMenuItem.Width + eDITToolStripMenuItem.Width + vIEWToolStripMenuItem.Width + cONVERTToolStripMenuItem.Width + dEBUGToolStripMenuItem.Width + tOOLSToolStripMenuItem.Width + hELPToolStripMenuItem.Width + borderSizeX + 4;
                    if (SearchBox.Visible)
                        Title.Width = this.Width - menuIcon.Width - fILEToolStripMenuItem.Width - eDITToolStripMenuItem.Width - vIEWToolStripMenuItem.Width - cONVERTToolStripMenuItem.Width - dEBUGToolStripMenuItem.Width - tOOLSToolStripMenuItem.Width - hELPToolStripMenuItem.Width - SearchBox.Width - UserPanel.Width - Close.Width - Restore.Width - Minimize.Width - borderSizeX * 2 - 20;
                    else
                        Title.Width = this.Width - menuIcon.Width - fILEToolStripMenuItem.Width - eDITToolStripMenuItem.Width - vIEWToolStripMenuItem.Width - cONVERTToolStripMenuItem.Width - dEBUGToolStripMenuItem.Width - tOOLSToolStripMenuItem.Width - hELPToolStripMenuItem.Width - UserPanel.Width - Close.Width - Restore.Width - Minimize.Width - borderSizeX * 2 - 20;
                }
                else if (ActiveMdiChild is Browser)
                {
                    Title.Left = menuIcon.Width + fILEToolStripMenuItem1.Width + eDITToolStripMenuItem1.Width + vIEWToolStripMenuItem1.Width + fAVORITESToolStripMenuItem.Width + tOOLSToolStripMenuItem1.Width + hELPToolStripMenuItem1.Width + borderSizeX + 4;
                    if (SearchBox.Visible)
                        Title.Width = this.Width - menuIcon.Width - fILEToolStripMenuItem1.Width - eDITToolStripMenuItem1.Width - vIEWToolStripMenuItem1.Width - fAVORITESToolStripMenuItem.Width - tOOLSToolStripMenuItem1.Width - hELPToolStripMenuItem1.Width - SearchBox.Width - UserPanel.Width - Close.Width - Restore.Width - Minimize.Width - borderSizeX * 2 - 20;
                    else
                        Title.Width = this.Width - menuIcon.Width - fILEToolStripMenuItem1.Width - eDITToolStripMenuItem1.Width - vIEWToolStripMenuItem1.Width - fAVORITESToolStripMenuItem.Width - tOOLSToolStripMenuItem1.Width - hELPToolStripMenuItem1.Width - UserPanel.Width - Close.Width - Restore.Width - Minimize.Width - borderSizeX * 2 - 20;
                }

            }
            if (UserPanel.Visible == false)
            {
                if (ActiveMdiChild is Form1)
                {
                    Title.Left = menuIcon.Width + fILEToolStripMenuItem.Width + eDITToolStripMenuItem.Width + vIEWToolStripMenuItem.Width + cONVERTToolStripMenuItem.Width + dEBUGToolStripMenuItem.Width + tOOLSToolStripMenuItem.Width + hELPToolStripMenuItem.Width + borderSizeX + 4;
                    if (SearchBox.Visible)
                        Title.Width = this.Width - menuIcon.Width - fILEToolStripMenuItem.Width - eDITToolStripMenuItem.Width - vIEWToolStripMenuItem.Width - cONVERTToolStripMenuItem.Width - dEBUGToolStripMenuItem.Width - tOOLSToolStripMenuItem.Width - hELPToolStripMenuItem.Width - SearchBox.Width - 4 - Close.Width - Restore.Width - Minimize.Width - borderSizeX * 2 - 20;
                    else
                        Title.Width = this.Width - menuIcon.Width - fILEToolStripMenuItem.Width - eDITToolStripMenuItem.Width - vIEWToolStripMenuItem.Width - cONVERTToolStripMenuItem.Width - dEBUGToolStripMenuItem.Width - tOOLSToolStripMenuItem.Width - hELPToolStripMenuItem.Width - 4 - Close.Width - Restore.Width - Minimize.Width - borderSizeX * 2 - 20;
                }
                else if (ActiveMdiChild is Browser)
                {
                    Title.Left = menuIcon.Width + fILEToolStripMenuItem1.Width + eDITToolStripMenuItem1.Width + vIEWToolStripMenuItem1.Width + fAVORITESToolStripMenuItem.Width + tOOLSToolStripMenuItem1.Width + hELPToolStripMenuItem1.Width + borderSizeX + 4;
                    if (SearchBox.Visible)
                        Title.Width = this.Width - menuIcon.Width - fILEToolStripMenuItem1.Width - eDITToolStripMenuItem1.Width - vIEWToolStripMenuItem1.Width - fAVORITESToolStripMenuItem.Width - tOOLSToolStripMenuItem1.Width - hELPToolStripMenuItem1.Width - SearchBox.Width - 4 - Close.Width - Restore.Width - Minimize.Width - borderSizeX * 2 - 20;
                    else
                        Title.Width = this.Width - menuIcon.Width - fILEToolStripMenuItem1.Width - eDITToolStripMenuItem1.Width - vIEWToolStripMenuItem1.Width - fAVORITESToolStripMenuItem.Width - tOOLSToolStripMenuItem1.Width - hELPToolStripMenuItem1.Width - 4 - Close.Width - Restore.Width - Minimize.Width - borderSizeX * 2 - 20;
                }
            }
        }
        protected override void OnClientSizeChanged(EventArgs e)
        {
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                //Loop.Enabled = false;
                if (this.Width < 825 && Properties.Settings.Default.ShowQuickSearch == true)
                {
                    SearchBox.Visible = false;
                }
                else if (Properties.Settings.Default.ShowQuickSearch == true)
                {
                    SearchBox.Visible = true;
                }

                if (Debugger.IsAttached) borderSizeX = SystemInformation.HorizontalResizeBorderThickness * 2;
                else borderSizeX = SystemInformation.HorizontalResizeBorderThickness;
                UpdateTitle();
                try
                {
                    using (Graphics g = this.CreateGraphics())
                    {
                        if (g.MeasureString(this.Text, Title.Font).Width + 2 > Title.Width)
                        {
                            string a = this.Text;
                            while (g.MeasureString(a, Title.Font).Width + 2 > Title.Width) a = a.Substring(0, a.Length - 1);
                            if (a != "") Title.Text = a + "...";
                            else Title.Text = "";
                        }
                        else Title.Text = this.Text;
                    }
                }
                catch { }
                if (this.WindowState == FormWindowState.Minimized)
                {


                    if (Properties.Settings.Default.DisableEnhancedUI == false)
                    {
                        nuIntra = true;
                        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                        glow.HideGlow(); nuIntra = false;
                    }
                    //this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

                }
                if (this.WindowState != mLastState)
                {
                    mLastState = this.WindowState;
                    OnWindowStateChanged(e);
                }
                /*   if (this.WindowState == FormWindowState.Normal && !IsMaximised && nuIntra == false)
                   {
                       oldSize = this.Size;
                       oldLoc = this.Location;
                   }*/
                Close.Left = this.Width - Close.Width - 8;
                Restore.Left = this.Width - Close.Width - Restore.Width - 8;
                Minimize.Left = this.Width - Close.Width - Restore.Width - Minimize.Width - 8;
                SearchBox.Left = Minimize.Left - SearchBox.Width - 2 - 8;
                if (SearchBox.Visible) UserPanel.Left = SearchBox.Left - UserPanel.Width - 3 - 8;
                else UserPanel.Left = this.Width - UserPanel.Width - 8 - 8;
                menuIcon.Visible = true;
                Title.Visible = true;
                Title.BringToFront();
            }
            base.OnClientSizeChanged(e);
        }
        [DllImport("user32.dll")]
        static extern int SetWindowRgn(IntPtr hWnd, IntPtr hRgn, bool bRedraw);
        [DllImport("gdi32.dll")]
        static extern IntPtr CreateRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);
        private void unTop_Tick(object sender, EventArgs e)
        {
            unTop.Enabled = false;
            this.TopMost = false;
            nuAcum = false;
        }
        protected void OnWindowStateChanged(EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                toolTip1.SetToolTip(Restore, RestoreT);
                if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = RestoreBlack.BackgroundImage;
                if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = RestoreLight.BackgroundImage;
                wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
                wpfwindow.Visibility = System.Windows.Visibility.Hidden;
                SetWindowRgn(this.Handle, CreateRectRgn(borderSizeX, borderSizeX, Screen.GetWorkingArea(this).Width + borderSizeX, Screen.GetWorkingArea(this).Height + borderSizeX), true);
                this.Padding = new Padding(borderSizeX);
                MaximizeCode();
                //this.Opacity = 1;

            }
            else
            {
                if (this.WindowState != FormWindowState.Minimized)
                {
                    toolTip1.SetToolTip(Restore, MaximizeT);
                    if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = MaximizeBlack.BackgroundImage;
                    if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = MaximizeLight.BackgroundImage;
                    SetWindowRgn(this.Handle, CreateRectRgn(0, 0, Screen.GetWorkingArea(this).Width, Screen.GetWorkingArea(this).Height), true);
                    this.Padding = new Padding(5);
                    NormalCode(e);
                    try
                    {
                        wpfwindow.Visibility = System.Windows.Visibility.Visible;
                        SetForegroundWindow(wpfwindow.GetHandle());
                        this.TopMost = true;
                        if (Properties.Settings.Default.DynamicColor == true) wpfwindow.Foreground = border;
                        else                         wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                        unTop.Enabled = true;
                        wpfwindow.Show();
                    }
                    catch { }
                }
                else
                {
                    wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
                    wpfwindow.Visibility = System.Windows.Visibility.Hidden;
                    //this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;

                }
            }
        }

       /* protected override void WndProc(ref Message message)
        {
            const int WM_NCHITTEST = 0x0084;

            if (message.Msg == WM_NCHITTEST && WindowState == FormWindowState.Maximized)
                return;

            base.WndProc(ref message);
        }*/
        /*const int WS_MINIMIZEBOX = 0x20000;
        const int WS_MAXIMIZEBOX = 0x10000;
        const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams param = base.CreateParams;
                param.Style |= (WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);

                return param;
            }
        }*/
        const int WS_MINIMIZEBOX = 0x20000;
        const int CS_DBLCLKS = 0x8;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style |= WS_MINIMIZEBOX;
                cp.ClassStyle |= CS_DBLCLKS;
                return cp;
            }
        }
        Size oldSize = new Size(0,0);
        Point oldLoc = new Point(0, 0);
        Point posOldSearchBox = new Point(0, 0);
        Point posOldUP = new Point(0, 0);
        public bool IsMaximised = false;
        bool doNotdoCode = false;
        public bool nuIntra = false;
        public void MaximizeCode()
        {
         //   this.Padding = new Padding(0);
           // Loop.Enabled = true;
            //IsMaximised = true;
          /*  if (Properties.Settings.Default.DisableEnhancedUI == false)
            {
                glow.HideGlow();
                glow.Hide();
            }*/
        //    doNotdoCode = true;
      //      this.WindowState = FormWindowState.Normal;
        //    doNotdoCode = false;
        /*    TaskbarTest.Taskbar tb = new TaskbarTest.Taskbar();
            if (tb.AutoHide == true)
            {
                this.Size = new Size(Screen.GetWorkingArea(this).Width, Screen.GetWorkingArea(this).Height - 1);
            }
            else
            {
                this.Size = new Size(Screen.GetWorkingArea(this).Width, Screen.GetWorkingArea(this).Height);
            }*/
         //   this.Location = new Point(Screen.GetWorkingArea(this).X, Screen.GetWorkingArea(this).Y);
          // Close.Visible = true;
          //  Restore.Visible = true;
          //  Minimize.Visible = true;

        }
        public void NormalCode(EventArgs e)
        {
            if (Properties.Settings.Default.DisableEnhancedUI == false)
            {
                glow.WindowState = FormWindowState.Normal;
                this.Padding = new Padding(5, 1, 5, 5);
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.TopMost = true;
                glow.Show();
                glow.ShowGlow();
                this.TopMost = false;
                SetForegroundWindow(glow.Handle);
                SetForegroundWindow(this.Handle);
                this.OnResize(e);
            }
            else
            {
                
                /*if (doNotdoCode == false)
                {
                    if (oldSize.Width != 0)
                    {
                        this.Location = oldLoc;
                        this.Size = oldSize;
                    }
                }
                XPResizeTimer.Enabled = true;*/
            }
       //     Title.Visible = false;
        //    menuIcon.Visible = false;
       //     Close.Visible = false;
       //     Restore.Visible = false;
        //    Minimize.Visible = false;
           /* Close.Left = this.Width - Close.Width - 8;
            Restore.Left = this.Width - Close.Width - Restore.Width - 8;
            Minimize.Left = this.Width - Close.Width - Restore.Width - Minimize.Width - 8;
            SearchBox.Left = Minimize.Left - SearchBox.Width - 2 - 8;
            if (SearchBox.Visible) UserPanel.Left = SearchBox.Left - UserPanel.Width - 3 - 8;
            else UserPanel.Left = this.Width - UserPanel.Width - 8 - 8;
            menuIcon.Visible = true;
            Title.Visible = true;
            Title.BringToFront();*/
            /*if (doNotdoCode == false)
            {
                if (oldSize.Width != 0)
                {
                    nuIntra = true;
                    //this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                    nuIntra = false;
                  //  this.Location = oldLoc;
                   // this.Size = oldSize;
                    //oldSize = new Size(0, 0);
                    //oldLoc = this.Location;

                }
            }*/
        }
        private void XPResizeTimer_Tick(object sender, EventArgs e)
        {
            nuIntra = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            nuIntra = false;
            XPResizeTimer.Enabled = false;
        }
        private void customizeToolbarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.toolStrip1.ImageSizeSelection = true;
            this.toolStrip1.ListViewDisplayStyle = ToolStripCustomCtrls.ListViewDisplayStyle.List;
            this.toolStrip1.Customize("RO");
        }

        private void dockPanel_ActiveContentChanged(object sender, EventArgs e)
        {

        }

        private void dockPanel_Paint(object sender, PaintEventArgs e)
        {
        }

        private void newPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            New(NewTabTitle + " " + (MdiChildren.Count() + 1).ToString());
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            newPseudocodeToolStripMenuItem.PerformClick();
        }

        /// <summary>
        /// Gets a list of all ToolStripMenuItems
        /// </summary>
        /// <param name="menuStrip">The menu strip control</param>
        /// <returns>List of all ToolStripMenuItems</returns>
        public static List<ToolStripMenuItem> GetItems(MenuStrip menuStrip)
        {
            List<ToolStripMenuItem> myItems = new List<ToolStripMenuItem>();
            foreach (ToolStripMenuItem i in menuStrip.Items)
            {
                GetMenuItems(i, myItems);
            }
            return myItems;
        }
        private static string menuPath = "";
        private static void GetParentItem(ToolStripItem item)
        {
            if (item.OwnerItem != null) GetParentItem(item.OwnerItem);
            if (item.Text.ToUpper() == item.Text && Regex.Matches(item.Text, @"[a-zA-Z]").Count != 0) menuPath += item.Text.Substring(1, 1) + item.Text.ToLower().Substring(2, item.Text.Length - 2) + " → ";
            else menuPath += item.Text + " → ";
        }
        /// <summary>
        /// Gets the menu items.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="items">The items.</param>
        private static void GetMenuItems(ToolStripMenuItem item, List<ToolStripMenuItem> items)
        {
            item.Tag = "";
            menuPath = "";
            GetParentItem(item);
            if (menuPath.EndsWith(" → ")) menuPath = menuPath.Substring(0, menuPath.Length - 3);
            if (menuPath.StartsWith("&")) menuPath = menuPath.Substring(1, menuPath.Length - 1);
            item.Tag = menuPath;
            if (item.HasDropDownItems == false) items.Add(item);
            foreach (ToolStripItem i in item.DropDownItems)
            {
                if (i is ToolStripMenuItem)
                {
                    GetMenuItems((ToolStripMenuItem)i, items);
                }
            }
        }
        
        private void SearchBox_Enter(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) SearchBox.ForeColor = Color.White;

            if (SearchBox.Text == DefaultSearchBoxText) SearchBox.Text = "";

        }

        private void SearchBox_Leave(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) SearchBox.ForeColor = Color.Silver ;

            if (SearchPanel.Focused == false && listBox1.Focused == false)
            {
                SearchBox.Text = DefaultSearchBoxText;
                SearchPanel.Visible = false;
            }
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter) e.SuppressKeyPress = true;
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                SearchBox.Text = DefaultSearchBoxText;
                SearchPanel.Visible = false;
            }
        }

        private void SearchBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText) SearchBox.Text = "";
        }
        List<ToolStripMenuItem> myItems;
        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                try
                {
                    listBox1.SelectedIndex = 0;
                }
                catch{}
                listBox1.Focus();
                e.SuppressKeyPress = true;
                return;
            }
            listBox1.Tag = SearchBox.Text;
            if (SearchBox.Text != "" && SearchBox.Text != DefaultSearchBoxText)
            {
                SearchPanel.Visible = true;
                SearchPanel.BringToFront();
                ImageList im = new ImageList();
                im.ColorDepth = ColorDepth.Depth32Bit;
                listBox1.ImageList = im;
                listBox1.Items.Clear();
                int i = 0;
                int l = 0;
                myItems = GetItems(this.menuStrip1);
                List<ToolStripMenuItem> usedItems = new List<ToolStripMenuItem>();
                foreach (var item in myItems.ToList())
                {
                    if (item.Text.ToLower().Contains(SearchBox.Text.ToLower()) && item.Available && item.OwnerItem.Available)
                    {
                        if (item.Image != null)
                        {
                            im.Images.Add(item.Image);
                            listBox1.Items.Add(new Controls.Development.ImageListBoxItem(item.Tag.ToString().Replace("&",""), l));
                            l++;
                        }
                        else listBox1.Items.Add(new Controls.Development.ImageListBoxItem(item.Tag.ToString().Replace("&", "")));
                        i++;
                        usedItems.Add(item);
                    }
                }
                myItems = usedItems;
                SearchText.Text = SearchText.Tag.ToString() + " (" + i + ")";
                panel2.Left = SearchText.Left + SearchText.Width + 5;
                /*try
                {
                    listBox1.SelectedIndex = 0;
                }
                catch { }*/

            }
            else
            {
                SearchPanel.Visible = false;
            }
        }

        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            /*e.DrawBackground();
           
            Graphics g = e.Graphics;
            Brush br = new SolidBrush(Color.FromArgb(253, 244, 191));
            Brush brush = ((e.State & DrawItemState.Selected) == DrawItemState.Selected) ?
                          br : new SolidBrush(e.BackColor);
            g.FillRectangle(brush, e.Bounds);
            e.Graphics.DrawString(listBox1.Items[e.Index].ToString(), e.Font,
                     new SolidBrush(e.ForeColor), e.Bounds, StringFormat.GenericDefault);
            e.DrawFocusRectangle();     */ 
        }

        private void listBox1_MouseHover(object sender, EventArgs e)
        {

        }

        private void listBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Point point = listBox1.PointToClient(Cursor.Position);
            int index = listBox1.IndexFromPoint(point);
            if (index < 0) return;
            listBox1.SelectedIndex = index;
        }

        private void listBox1_MouseLeave(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = -1;
        }

        private void listBox1_Leave(object sender, EventArgs e)
        {
            if (SearchPanel.Focused == false && SearchText.Focused == false)
            {
                SearchBox.Text = DefaultSearchBoxText;
                SearchPanel.Visible = false;
            }
        }

        private void SearchPanel_Leave(object sender, EventArgs e)
        {
            if (SearchText.Focused == false && listBox1.Focused == false)
            {
                SearchBox.Text = DefaultSearchBoxText;
                SearchPanel.Visible = false;
            }
        }

        private void SearchPanel_VisibleChanged(object sender, EventArgs e)
        {
            
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            myItems[listBox1.SelectedIndex].PerformClick();
        }

        private void SearchPanel_Enter(object sender, EventArgs e)
        {
            listBox1.Focus();
        }

        private void SearchPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void listBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                myItems[listBox1.SelectedIndex].PerformClick();
            }
        }

        private void listBox1_MouseEnter(object sender, EventArgs e)
        {
            listBox1.Focus();
        }



        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //plugin executor
            foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
            {
                if (_Plugins.ContainsKey(pair.Key))
                {
                    VPlugin plugin = _Plugins[pair.Key];
                    string rez = plugin.Perform("com.valinet.pseudocode.tools.options", "");
                    if (rez == "true")
                    {
                        return;
                    }
                }
            }
            int fost = Properties.Settings.Default.UITheme;
            //plugin executor finish
            Options op = new Options(this);
            op.ShowDialog();
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                if (Properties.Settings.Default.DynamicColor == true)
                {
                    DateTime beg = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    DateTime now = DateTime.Now;
                    TimeSpan ts = now - beg;
                    int decValue = Convert.ToInt32(ts.TotalSeconds);
                    UpdateBorder(decValue);
                    colorTimer.Enabled = true;
                }
                else
                {
                    colorTimer.Enabled = false;
                    wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                }
            }
            else colorTimer.Enabled = false;
            ThemeCode();
            if (Properties.Settings.Default.UITheme != fost)
            {
                this.Refresh();
                Loop.Enabled = true;
                if (ActiveMdiChild is Form1)
                {
                    Form1 frm = ActiveMdiChild as Form1;
                    frm.updateScroll = true;
                }
            }
            foreach (Form frmx in MdiChildren)
                if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.ThemeCode();
                frm.tabControl3.Refresh();
                frm.splitContainer4.Refresh();
                frm.semiTransparentPanel1.Refresh();
            }
            if (Properties.Settings.Default.ShowStatusBar == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.statusBar1.Visible = true;
                    frm.BottomDown.Visible = true;
                    frm.BottomDown.Top = frm.Height - frm.statusBar1.Height - 2;
                }
                statusBarToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.statusBar1.Visible = false;
                    frm.BottomDown.Visible = false;

                }
                statusBarToolStripMenuItem.Checked = false;
            }
            if (Properties.Settings.Default.ShowAdvancedPanel == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer3.Panel2Collapsed = false;
                    frm.splitContainer3.Panel2.Show();
                }
                advancedCommandsPanelToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer3.Panel2Collapsed = true;
                    frm.splitContainer3.Panel2.Hide();
                }
                advancedCommandsPanelToolStripMenuItem.Checked = false;
            }
            if (Properties.Settings.Default.ShowConvertedCode == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer2.Panel2Collapsed = false;
                    frm.splitContainer2.Panel2.Show();
                }
                convertedCodeLogicSchemaToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer2.Panel2Collapsed = true;
                    frm.splitContainer2.Panel2.Hide();
                }
                convertedCodeLogicSchemaToolStripMenuItem.Checked = false;
            }
            if (Properties.Settings.Default.ShowCommandBar == true)
            {
                if (ActiveMdiChild is Form1) toolStrip1.Visible = true;
                commandBarToolStripMenuItem.Checked = true;
            }
            else
            {
                if (ActiveMdiChild is Form1) toolStrip1.Visible = false;
                commandBarToolStripMenuItem.Checked = false;
            }
            UpdateUXLines();

            if (Properties.Settings.Default.ShowQuickSearch == true)
            {
                quickMenuSearchBoxToolStripMenuItem.Checked = true;
                SearchBox.Visible = true;
            }
            else
            {
                quickMenuSearchBoxToolStripMenuItem.Checked = false;
                SearchBox.Visible = false;
            }
            if (Properties.Settings.Default.ShowVisualScrollBar == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer4.Panel2Collapsed = false;
                    frm.splitContainer4.Panel2.Show();
                    frm.pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Horizontal;
                }
                visualScrollBarToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer4.Panel2Collapsed = true;
                    frm.splitContainer4.Panel2.Hide();
                    frm.pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Both;
                }
                visualScrollBarToolStripMenuItem.Checked = false;
            }
        }

        private void openPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
           // if (ActiveMdiChild is Form1)
          //  {
                newPseudocodeToolStripMenuItem.PerformClick();
                Application.DoEvents();
                Form1 frm = ActiveMdiChild as Form1;
                frm.button5.PerformClick();
          //  }
        }

        private void toCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form[] frma = this.MdiChildren;
            foreach (Form frmx in frma)
            {
                if (frmx is Form1)
                {
                    Form1 frm = frmx as Form1;
                    if (frm != null)
                    {
                        frm.button23.PerformClick();
                    }
                }
            }
        }

        private void toBasicClassicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form[] frma = this.MdiChildren;
            foreach (Form frmx in frma)
            {
                if (frmx is Form1)
                {
                    Form1 frm = frmx as Form1;
                    if (frm != null)
                    {
                        frm.button22.PerformClick();
                    }
                }
            }
        }

        private void toPascalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form[] frma = this.MdiChildren;
            foreach (Form frmx in frma)
            {
                if (frmx is Form1)
                {
                    Form1 frm = frmx as Form1;
                    if (frm != null)
                    {
                        frm.button25.PerformClick();
                    }
                }
            }
        }

        private void toCToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form[] frma = this.MdiChildren;
            foreach (Form frmx in frma)
            {
                if (frmx is Form1)
                {
                    Form1 frm = frmx as Form1;
                    if (frm != null)
                    {
                        frm.button24.PerformClick();
                    }
                }
            }
        }

        private void toJavaScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form[] frma = this.MdiChildren;
            foreach (Form frmx in frma)
            {
                if (frmx is Form1)
                {
                    Form1 frm = frmx as Form1;
                    if (frm != null)
                    {
                        frm.button26.PerformClick();
                    }
                }
            }
        }

        private void toLogicSchemeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form[] frma = this.MdiChildren;
            foreach (Form frmx in frma)
            {
                if (frmx is Form1)
                {
                    Form1 frm = frmx as Form1;
                    if (frm != null)
                    {
                        frm.button27.PerformClick();
                    }
                }
            }
        }

        private void openPseudocodeUsingAcodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //plugin executor
            foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
            {
                if (_Plugins.ContainsKey(pair.Key))
                {
                    VPlugin plugin = _Plugins[pair.Key];
                    string rez = plugin.Perform("com.valinet.pseudocode.open-code-tag", "");
                    if (rez == "true")
                    {
                        return;
                    }
                }
            }
            //plugin executor finish
            UseCode uc = new UseCode(this);
            DialogResult nc = uc.ShowDialog();
            if (nc == System.Windows.Forms.DialogResult.Cancel)
            {
                if (uc.Tag != null) if (uc.Tag.ToString() != "") CasetaDeMesaj(this, ErrorDownloadingCode + uc.Tag, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if (nc == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    if (Properties.Settings.Default.LastCode == "paste not found")
                    {
                        if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(this, "The #code does not exist or does not point to a valid pseudocode file.\n\nOperation has been aborted.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        if (Properties.Settings.Default.UILang == 1) CasetaDeMesaj(this, "#Codul specificat de dvs. nu există sau nu conduce către un pseudocod valid.\n\nOperația a fost anulată.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    //System.IO.TextReader tw = new System.IO.StreamReader(openFileDialog1.FileName);
                    New("#" + Properties.Settings.Default.LastCode + " " + Downloaded);
                    Application.DoEvents();
                    Form1 frm = ActiveMdiChild as Form1;
                    frm.pseudocodeEd.Text = uc.Tag.ToString();
                    frm.pseudocodeEd.UndoRedo.EmptyUndoBuffer();
                    ExTag oldEX = (ExTag)frm.pseudocodeEd.Tag;
                    ExTag exTag = new ExTag();
                    exTag.Add("issaved", false);
                    exTag.Add("filename", "");
                    exTag.Add("uniqueString", oldEX.Get("uniqueString"));
                    frm.pseudocodeEd.Tag = exTag;
                    frm.Text = "#" + Properties.Settings.Default.LastCode + " " + Downloaded;
                    //Form1.superTabControl1.Tabs[Form1.superTabControl1.Tabs.Count - 1].Text = "#" + Properties.Settings.Default.LastCode + " " + Downloaded;
                    frm.flagReident = true;

                }
                catch
                {
                    CasetaDeMesaj(this, NoSuchFile, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //col.Remove(openFileDialog1.FileName);
                    this.Close();
                }
            }

        }

        private void sharePseudocodeUsingAcodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //plugin executor
            foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
            {
                if (_Plugins.ContainsKey(pair.Key))
                {
                    VPlugin plugin = _Plugins[pair.Key];
                    string rez = plugin.Perform("com.valinet.pseudocode.share-code-tag", "");
                    if (rez == "true")
                    {
                        return;
                    }
                }
            }
            //plugin executor finish
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                if (frm != null)
                {
                    frm.button21.PerformClick();
                }
            }
        }
        /*   bool wasMouseRight = false;
           public bool PreFilterMessage(ref Message m)
           {
               // Trap WM_RBUTTONDOWN
               if (m.Msg == 0x0204)
               {
                   wasMouseRight = true;
               }
               else wasMouseRight = false;
               return false;
           }*/
        public void UpdateMenus()
        {
            if (ActiveMdiChild is Form1)
            {
                fILEToolStripMenuItem.Visible = true;
                eDITToolStripMenuItem.Visible = true;
                vIEWToolStripMenuItem.Visible = true;
                cONVERTToolStripMenuItem.Visible = true;
                dEBUGToolStripMenuItem.Visible = true;
                tOOLSToolStripMenuItem.Visible = true;
                hELPToolStripMenuItem.Visible = true;
                fILEToolStripMenuItem1.Visible = false;
                eDITToolStripMenuItem1.Visible = false;
                vIEWToolStripMenuItem1.Visible = false;
                fAVORITESToolStripMenuItem.Visible = false;
                tOOLSToolStripMenuItem1.Visible = false;
                hELPToolStripMenuItem1.Visible = false;
                customizableToolStrip1.Visible = false;
                if (Properties.Settings.Default.ShowCommandBar) toolStrip1.Visible = true;
            }
            else if (ActiveMdiChild is Browser)
            {
                fILEToolStripMenuItem.Visible = false;
                eDITToolStripMenuItem.Visible = false;
                vIEWToolStripMenuItem.Visible = false;
                cONVERTToolStripMenuItem.Visible = false;
                dEBUGToolStripMenuItem.Visible = false;
                tOOLSToolStripMenuItem.Visible = false;
                hELPToolStripMenuItem.Visible = false;
                fILEToolStripMenuItem1.Visible = true;
                eDITToolStripMenuItem1.Visible = true;
                vIEWToolStripMenuItem1.Visible = true;
                fAVORITESToolStripMenuItem.Visible = true;
                tOOLSToolStripMenuItem1.Visible = true;
                hELPToolStripMenuItem1.Visible = true;
                customizableToolStrip1.Visible = true;
                toolStrip1.Visible = false;

            }
        }
        private void dockPanel_ActivePaneChanged(object sender, EventArgs e)
        {
            UpdateMenus();
            UpdateTitle();
            //this.Text = ActiveMdiChild.Text + " - " + AppName;
            try
            {
                time = 0;
                this.Text = ActiveMdiChild.Text + " - " + AppName;
                Form1 frm = ActiveMdiChild as Form1;
                if (hp != null)
                {
                    if (!SearchBox.Focused && !UserDropPanel.Focused && !frm.pseudocodeEd.FindReplace.Window.Visible && !fILEToolStripMenuItem.Pressed && !eDITToolStripMenuItem.Pressed && !vIEWToolStripMenuItem.Pressed && !cONVERTToolStripMenuItem.Pressed && !dEBUGToolStripMenuItem.Pressed && !tOOLSToolStripMenuItem.Pressed && !hELPToolStripMenuItem.Pressed && !frm.contextMenuStrip1.Visible && !frm.contextMenuStrip2.Visible && !frm.contextMenuStrip3.Visible && !frm.contextMenuStrip4.Visible && !frm.contextMenuStrip5.Visible && !frm.contextMenuStrip6.Visible && !hp.Visible && !zoom.Visible && wpfwindow.Visibility != System.Windows.Visibility.Visible && !toolStripButton32.Pressed)
                    {
                        frm.pseudocodeEd.Focus();
                    }
                }
                else
                {
                    if (!SearchBox.Focused && !UserDropPanel.Focused && !frm.pseudocodeEd.FindReplace.Window.Visible && !fILEToolStripMenuItem.Pressed && !eDITToolStripMenuItem.Pressed && !vIEWToolStripMenuItem.Pressed && !cONVERTToolStripMenuItem.Pressed && !dEBUGToolStripMenuItem.Pressed && !tOOLSToolStripMenuItem.Pressed && !hELPToolStripMenuItem.Pressed && !frm.contextMenuStrip1.Visible && !frm.contextMenuStrip2.Visible && !frm.contextMenuStrip3.Visible && !frm.contextMenuStrip4.Visible && !frm.contextMenuStrip5.Visible && !frm.contextMenuStrip6.Visible && !zoom.Visible && wpfwindow.Visibility != System.Windows.Visibility.Visible && !toolStripButton32.Pressed)
                    {
                        frm.pseudocodeEd.Focus();
                    }
                }
                if (frm.PseudocodeLang == 0)
                {
                    fromEnglishCodeToolStripMenuItem.Checked = true;
                    fromRomanianPseudoccodeToolStripMenuItem.Checked = false;
                    userdefinedToolStripMenuItem.Checked = false;
                }
                if (frm.PseudocodeLang == 1)
                {
                    fromEnglishCodeToolStripMenuItem.Checked = false;
                    fromRomanianPseudoccodeToolStripMenuItem.Checked = true;
                    userdefinedToolStripMenuItem.Checked = false;
                }
            }
            catch { }

        }
        private void UserPanel_Click(object sender, EventArgs e)
        {

                try
                {
                    UserDropPanel.Top = pictureBox2.Top + pictureBox2.Height;
                    UserDropPanel.Visible = true;
                    UserDropPanel.BringToFront();
                    UserDropPanel.Height = 0;
                    UserDropPanel.Focus();

                    userTimer.Enabled = true;
                }
                catch { }
            
        }
        private void userTimer_Tick(object sender, EventArgs e)
        {
            UserDropPanel.Visible = true;
            UserDropPanel.BringToFront();
            UserDropPanel.Height += (int)(22 * ((float)dx / 96));
            if (UserDropPanel.Height % 11 == 0)
            {
                UserDropPanel.Top += 1;
            }
            if (UserDropPanel.Height == (int)(154 * ((float)dx / 96)))
            {
                userTimer.Enabled = false;
            }
            pictureBox4.LostFocus += UserDropPanel_LostFocus;
            //UserDropPanel.LostFocus += UserDropPanel_LostFocus;
        }

        void UserDropPanel_LostFocus(object sender, EventArgs e)
        {
            UserDropPanel.Visible = false;
        }
        private void menuSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchBox.Focus();
        }
        bool showInfo = false;
        private void UpdateChecker_DoWork(object sender, DoWorkEventArgs e)
        {
            int nr_up = 0;
            int nr_err = 0;
            StreamReader sr = new StreamReader(Application.StartupPath + "\\" + "package-list.txt");
            string[] text = Regex.Split(sr.ReadToEnd(), "\r\n");
            sr.Close();
            System.Net.WebClient wc = new System.Net.WebClient();
            foreach (string it in text)
            {
                if (it != "" && it != null)
                {
                    try
                    {
                        StreamReader sr2 = new StreamReader(Application.StartupPath + "\\" + it + "\\version.txt");
                        string[] infos = Regex.Split(sr2.ReadToEnd(), "\r\n");
                        sr2.Close();
                        try
                        {
                            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                        }
                        catch { }
                        try
                        {
                            if (it == "com.valinet.pseudocode") wc.DownloadFile(Properties.Settings.Default.ServerPath + "/" + it + "/version.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                            else wc.DownloadFile(infos[2] + "/" + it + "/version.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");

                        StreamReader srx = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                        string[] citit = Regex.Split(srx.ReadToEnd(), "\n");
                        srx.Close();
                        if (citit[0] != infos[0])
                        {
                            if (citit[0].StartsWith("0") || citit[0].StartsWith("1") || citit[0].StartsWith("2") || citit[0].StartsWith("3") || citit[0].StartsWith("4") || citit[0].StartsWith("5") || citit[0].StartsWith("6") || citit[0].StartsWith("7") || citit[0].StartsWith("8") || citit[0].StartsWith("9"))
                            {
                                nr_up++;
                            }
                            else
                            {
                                nr_err++;
                            }
                        }
                        else
                        {
                            // nr_up++;
                        }
                        }
                        catch { nr_err++; }
                    }
                    catch
                    {
                        nr_err++;
                    }
                }
            }
            DoOnUIThread(delegate()
            {
                if (nr_err != 0 && showInfo == true)
                {
                    if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(this, "Unable to check for updates for one of your installed packages.\n\nUse Extensions and Updates to manually resolve the problems regarding the packages' repositories.", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else CasetaDeMesaj(this, "Nu s-a putut verifica dacă există actualizări pentru unul sau mai multe pachete instalate.\n\nFolosiți Extensii și Actualizări pentru a corecta manual erorile legate de depozitele de la care provin pachetele.", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (nr_up == 0)
                {
                    if (showInfo == true)
                    {
                        if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(this, "Good news!\n\nAll installed packages are updated!", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else CasetaDeMesaj(this, "Vești bune!\n\nToate pachetele instalate sunt la zi!", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    UpdateFound up = new UpdateFound(this, nr_up);
                    if (up.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        ManageRepos ex = new ManageRepos(this);
                        ex.ShowDialog();
                    }
                }

            });
            /*System.Net.WebClient wc = new System.Net.WebClient();
            try
            {
                wc.DownloadFile(Properties.Settings.Default.ServerPath + Properties.Settings.Default.UILang.ToString() + "/update.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\update.txt");

                System.IO.TextReader tw = new System.IO.StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\update.txt", Encoding.UTF8);
                string text = tw.ReadToEnd();
                tw.Close();
                string[] info = System.Text.RegularExpressions.Regex.Split(text, "\n");
                string text2 = "";
                for (int i = 3; i < info.Length; i++)
                {
                    text2 = text2 + info[i] + "\r\n";
                }
                if (Application.ProductVersion != info[0])
                {
                    DoOnUIThread(delegate()
                    {
                        Update(info[0], text2, info[2], info[1]);
                    });

                    //Update(info[0], text2, info[2], info[1]);
                }
                else
                {
                    DoOnUIThread(delegate()
                    {
                        if (showInfo == true)
                        {
                            showInfo = false;
                            if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(this, "Good news!\n\nThe ValiNet product you are using is updated!", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else CasetaDeMesaj(this, "Vești bune!\n\nProdusul ValiNet folosit de dvs. este la zi!", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    });
                }
            }
            catch {

                DoOnUIThread(delegate()
                {
                    if (showInfo == true)
                    {
                        showInfo = false;
                        if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(this, "You are offline.\n\nWe cannot check for updates if there is no Internet connection. Connect to a network and try again.", AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else CasetaDeMesaj(this, "Conexiunea la Internet pare să fie offline.\n\nNu se pot căuta actualizări fără o conexiune la Internet. Conectați-vă la o rețea și încercați din nou.", AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                });
            }
            showInfo = false;*/
        }
        public bool Update(string a1, string a2, string a3, string a4)
        {
            Update up = new Update(a1, a2, a3, a4);
            up.Show();
            return true;
        }
        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }
        public void UpdateUXLines()
        {
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                if (this.WindowState == FormWindowState.Normal)
                {
                    if (Properties.Settings.Default.ShowCommandBar == true)
                    {
                        Top.Height = 2;
                        TopCover.Height = 2;
                        if (ActiveMdiChild is Form1) Top.Top = toolStrip1.Top + toolStrip1.Height;
                        else if (ActiveMdiChild is Browser) Top.Top = customizableToolStrip1.Top + toolStrip1.Height;
                        if (ActiveMdiChild is Form1) TopCover.Top = menuStrip1.Height + toolStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height + 2;
                        else if (ActiveMdiChild is Browser) TopCover.Top = menuStrip1.Height + customizableToolStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height + 2;
                    }
                    else
                    {
                        Top.Height = 2;
                        TopCover.Height = 2;
                        if (ActiveMdiChild is Form1) Top.Top = menuStrip1.Top + menuStrip1.Height;
                        else if (ActiveMdiChild is Browser) Top.Top = customizableToolStrip1.Top + customizableToolStrip1.Height;
                        if (ActiveMdiChild is Form1) TopCover.Top = menuStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height + 2;
                        else if (ActiveMdiChild is Browser) TopCover.Top = menuStrip1.Height + customizableToolStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height + 2;

                    }
                    if (Properties.Settings.Default.UITheme == 1) Right.BackColor = Color.FromArgb(221, 221, 221);
                    else Right.BackColor = Color.Black;
                    Right.Left = this.ClientSize.Width - 6;
                    Bottom.Top = this.Height - 6;
                }
                else
                {
                    if (Properties.Settings.Default.ShowCommandBar == true)
                    {
                        Top.Height = 2;
                        TopCover.Height = 2;
                        if (ActiveMdiChild is Form1) Top.Top = toolStrip1.Top + toolStrip1.Height;
                        else if (ActiveMdiChild is Browser) Top.Top = customizableToolStrip1.Top + toolStrip1.Height;
                        if (ActiveMdiChild is Form1) TopCover.Top = menuStrip1.Height + toolStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height + 6;
                        else if (ActiveMdiChild is Browser) TopCover.Top = menuStrip1.Height + customizableToolStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height + 6;
                    }
                    else
                    {
                        Top.Height = 2;
                        TopCover.Height = 2;
                        if (ActiveMdiChild is Form1) Top.Top = menuStrip1.Top + menuStrip1.Height;
                        else if (ActiveMdiChild is Browser) Top.Top = customizableToolStrip1.Top + customizableToolStrip1.Height;
                        if (ActiveMdiChild is Form1) TopCover.Top = menuStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height + 6;
                        else if (ActiveMdiChild is Browser) TopCover.Top = menuStrip1.Height + customizableToolStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height + 6;
                    }
                    if (Properties.Settings.Default.UITheme == 1) Right.BackColor = Color.FromArgb(221, 221, 221);
                    else Right.BackColor = Color.Black;
                    Right.Left = this.ClientSize.Width - 9;
                    Bottom.Top = this.Height - 9;
                }
            }
            else
            {
                if (Properties.Settings.Default.ShowCommandBar == true)
                {
                    Top.Height = 2;
                    TopCover.Height = 2;
                    if (ActiveMdiChild is Form1) Top.Top = menuStrip1.Height + toolStrip1.Height;
                    else if (ActiveMdiChild is Browser) Top.Top = menuStrip1.Height + customizableToolStrip1.Height;
                    if (ActiveMdiChild is Form1) TopCover.Top = menuStrip1.Height + toolStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height - 2;
                    else if (ActiveMdiChild is Browser) TopCover.Top = menuStrip1.Height + customizableToolStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height - 2;
                }
                else
                {
                    Top.Height = 2;
                    TopCover.Height = 2;
                    if (ActiveMdiChild is Form1) Top.Top = menuStrip1.Height;
                    else if (ActiveMdiChild is Browser) Top.Top = menuStrip1.Height + customizableToolStrip1.Height;
                    if (ActiveMdiChild is Form1) TopCover.Top = menuStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height - 2;
                    else if (ActiveMdiChild is Browser) TopCover.Top = menuStrip1.Height + customizableToolStrip1.Height + dockPanel.Size.Height - ActiveMdiChild.Height - 2;
                }
                if (Properties.Settings.Default.UITheme == 1) Right.BackColor = Color.FromArgb(221, 221, 221);
                else Right.BackColor = Color.Black;
                if (this.WindowState == FormWindowState.Maximized) Right.Left = this.ClientSize.Width - (int)(1 * ((float)dx / 96));
                else Right.Left = this.ClientSize.Width - (int)(1 * ((float)dx / 96));
            }
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat") & Properties.Settings.Default.AutoSignIn == true)
            {

                DoOnUIThread(delegate()
                {
                    //UserPanel.Visible = true; //TEMP
                    OnClientSizeChanged(e);
                    //       superTabControl1.Dock = DockStyle.None;
                    //      superTabControl1.Width = this.Width - UserPanel.Width;

                });
                var dropBoxStorage = new CloudStorage();
                var dropBoxConfig = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox);
                ICloudStorageAccessToken accessToken;
                StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                string t = sr.ReadToEnd();
               // MessageBox.Show(t);
                sr.Close();
                string a = EncryptDecrypt.DecryptString(t);
                StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                sw.Write(a);
                sw.Close();
              //  MessageBox.Show(EncryptDecrypt.DecryptString(EncryptDecrypt.DecryptString(t)));
                                    try
                    {
                using (var fs = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat", FileMode.Open, FileAccess.Read, FileShare.None))
                {

                        accessToken = dropBoxStorage.DeserializeSecurityToken(fs);

                }
                StreamWriter sww = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                sww.Write(t);
                sww.Close();
                try
                {

                    var storageToken = dropBoxStorage.Open(dropBoxConfig, accessToken);
                }
                catch { }
                    }
                                    catch
                                    {
                                        if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\token.dat")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\token.dat");
                                        if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.dat")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.dat");
                                        if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.pic")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.pic");
                                        if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.config")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.config");
                                        if (Properties.Settings.Default.UILang == 0) System.Windows.Forms.MessageBox.Show(new Form() { TopMost = true }, "The application is unable to connect to your Dropbox account. Press OK to restart Pseudocode.", "Pseudocode", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                                        if (Properties.Settings.Default.UILang == 1) System.Windows.Forms.MessageBox.Show(new Form(){TopMost = true}, "Aplicația nu s-a putut conecta la contul dvs. Dropbox. Apăsați OK pentru a reporni Pseudocode.", "Pseudocode", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                                        Process.Start(Application.ExecutablePath);
                                        Process.GetCurrentProcess().Kill();
                                    }
                try
                {

                    dropBoxStorage.DownloadFile("/user.pic", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
                    DoOnUIThread(delegate()
                    {
                        try
                        {
                            using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                            {
                                pictureBox2.Image = Image.FromStream(stream);
                            }
                            //pictureBox2.Image = Image.FromFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic");
                            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                            using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                            {
                                pictureBox3.Image = Image.FromStream(stream);
                            }
                            //pictureBox3.Image = Image.FromFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic");
                            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
                        }
                        catch { }
                    });
                }
                catch { }
                try
                {
                    dropBoxStorage.DownloadFile("/user.dat", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
                    DoOnUIThread(delegate()
                    {
                        label27.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                        label28.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                        signInToolStripMenuItem.Text = ConnectedAs + File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                        if (wasMoved == false)
                        {
                            label27.Left = 15;
                            UserPanel.Width = label27.Width + 25 + pictureBox2.Width;
                            if (SearchBox.Visible) UserPanel.Left = SearchBox.Left - UserPanel.Width - 3;
                            else UserPanel.Left = this.Width - UserPanel.Width - 8;
                            UserPanel.BringToFront();
                        }
                        linkLabel5.Visible = true;
                    });
                }
                catch { }
                try
                {
                    if (wasDownloaded == false)
                    {
                        dropBoxStorage.DownloadFile("/user.config", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
                        string text = System.IO.File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.config");
                        string[] rez = text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                        Properties.Settings.Default.Position = Convert.ToBoolean(rez[0]);
                        Properties.Settings.Default.Language = Convert.ToInt32(rez[1]);
                        Properties.Settings.Default.AutoSaveInterval = Convert.ToInt32(rez[2]);
                        Properties.Settings.Default.Zoom = Convert.ToInt32(rez[3]);

                        Properties.Settings.Default.Save();
                        if (Properties.Settings.Default.Language == 1)
                        {
                            //lang = "CPP";
                            DoOnUIThread(delegate()
                            {
                                if (ActiveMdiChild is Form1)
                                {
                                    Form1 frm = ActiveMdiChild as Form1;
                                    frm.button23.PerformClick();
                                }
                            });
                        }
                        if (Properties.Settings.Default.Language == 2)
                        {
                            //lang = "VB6";
                            DoOnUIThread(delegate()
                            {
                            if (ActiveMdiChild is Form1)
                            {
                                Form1 frm = ActiveMdiChild as Form1;
                                    frm.button22.PerformClick();
                                }
                                
                            });
                        }
                        if (Properties.Settings.Default.Language == 3)
                        {
                            //lang = "PAS";
                            DoOnUIThread(delegate()
                            {
                                if (ActiveMdiChild is Form1)
                                {
                                    Form1 frm = ActiveMdiChild as Form1;
                                    frm.button25.PerformClick();
                                }
                            });
                        }
                        if (Properties.Settings.Default.Language == 4)
                        {
                            //lang = "C#";
                            DoOnUIThread(delegate()
                            {
                            if (ActiveMdiChild is Form1)
                            {
                                Form1 frm = ActiveMdiChild as Form1;
                                    frm.button24.PerformClick();
                                }
                                
                            });
                        }
                        if (Properties.Settings.Default.Language == 5)
                        {
                            //lang = "JAVASCRIPT";
                            DoOnUIThread(delegate()
                            {
                            if (ActiveMdiChild is Form1)
                            {
                                Form1 frm = ActiveMdiChild as Form1;
                                    frm.button26.PerformClick();
                                }
                               
                            });
                        }
                        if (Properties.Settings.Default.Language == 6)
                        {
                            //lang = "VBSCRIPT";
                            DoOnUIThread(delegate()
                            {
                            if (ActiveMdiChild is Form1)
                            {
                                Form1 frm = ActiveMdiChild as Form1;
                                    frm.button27.PerformClick();
                                }
                                
                            });
                        }
                        DoOnUIThread(delegate()
                        {
                            //          for (int i = 0; i < superTabControl1.Tabs.Count; i++)
                            {
                                if (ActiveMdiChild is Form1)
                                {
                                    Form1 frm = ActiveMdiChild as Form1;
                                    frm.pseudocodeEd.ZoomFactor = Properties.Settings.Default.Zoom;
                                    frm.converterEd.ZoomFactor = Properties.Settings.Default.Zoom;
                                    //  sliderItem1.Value = Properties.Settings.Default.Zoom;
                                }
                              
                            }
                        });
                        if (ActiveMdiChild is Form1)

                        {
                            Form1 frma = ActiveMdiChild as Form1;
                            frma.autosaveint = (long)Properties.Settings.Default.AutoSaveInterval * 60;
                        }

                        Properties.Settings.Default.WasWelcomeShown = Convert.ToBoolean(rez[4]);
                        Properties.Settings.Default.DownloadImages = Convert.ToBoolean(rez[5]);
                        Properties.Settings.Default.UseBar = Convert.ToBoolean(rez[6]);
                        Properties.Settings.Default.Save();
                        Properties.Settings.Default.ShowStatusBar = Convert.ToBoolean(rez[7]);
                        Properties.Settings.Default.ShowAdvancedPanel = Convert.ToBoolean(rez[8]);
                        Properties.Settings.Default.ShowCommandBar = Convert.ToBoolean(rez[9]);
                        Properties.Settings.Default.ShowConvertedCode = Convert.ToBoolean(rez[10]);
                        Properties.Settings.Default.ShowVisualScrollBar = Convert.ToBoolean(rez[11]);
                        Properties.Settings.Default.AlwaysLoadProperties = Convert.ToBoolean(rez[12]);
                        Properties.Settings.Default.ShowQuickSearch = Convert.ToBoolean(rez[13]);
                        Properties.Settings.Default.PopulateFind = Convert.ToBoolean(rez[14]);
                        //Properties.Settings.Default.PseudocodeLang = Convert.ToBoolean(rez[7]);
                        Properties.Settings.Default.Save();
                                                    DoOnUIThread(delegate()
                            {
                        if (Properties.Settings.Default.ShowStatusBar == true)
                        {
                            foreach (Form frmx in MdiChildren) if (frmx is Form1)
                            {
                                Form1 frm = (Form1)frmx;
                                frm.statusBar1.Visible = true;
                                frm.BottomDown.Visible = true;
                                frm.BottomDown.Top = frm.Height - frm.statusBar1.Height - 2;
                            }
                            statusBarToolStripMenuItem.Checked = true;
                        }
                        else
                        {
                            foreach (Form frmx in MdiChildren) if (frmx is Form1)
                            {
                                Form1 frm = (Form1)frmx;
                                frm.statusBar1.Visible = false;
                                frm.BottomDown.Visible = false;

                            }
                            statusBarToolStripMenuItem.Checked = false;
                        }
                        if (Properties.Settings.Default.ShowAdvancedPanel == true)
                        {
                            foreach (Form frmx in MdiChildren) if (frmx is Form1)
                            {
                                Form1 frm = (Form1)frmx;
                                frm.splitContainer3.Panel2Collapsed = false;
                                frm.splitContainer3.Panel2.Show();
                            }
                            advancedCommandsPanelToolStripMenuItem.Checked = true;
                        }
                        else
                        {
                            foreach (Form frmx in MdiChildren) if (frmx is Form1)
                            {
                                Form1 frm = (Form1)frmx;
                                frm.splitContainer3.Panel2Collapsed = true;
                                frm.splitContainer3.Panel2.Hide();
                            }
                            advancedCommandsPanelToolStripMenuItem.Checked = false;
                        }
                        if (Properties.Settings.Default.ShowConvertedCode == true)
                        {
                            foreach (Form frmx in MdiChildren) if (frmx is Form1)
                            {
                                Form1 frm = (Form1)frmx;
                                frm.splitContainer2.Panel2Collapsed = false;
                                frm.splitContainer2.Panel2.Show();
                            }
                            convertedCodeLogicSchemaToolStripMenuItem.Checked = true;
                        }
                        else
                        {
                            foreach (Form frmx in MdiChildren) if (frmx is Form1)
                            {
                                Form1 frm = (Form1)frmx;
                                frm.splitContainer2.Panel2Collapsed = true;
                                frm.splitContainer2.Panel2.Hide();
                            }
                            convertedCodeLogicSchemaToolStripMenuItem.Checked = false;
                        }
                        if (Properties.Settings.Default.ShowCommandBar == true)
                        {
                                    if (ActiveMdiChild is Form1) toolStrip1.Visible = true;
                            commandBarToolStripMenuItem.Checked = true;
                        }
                        else
                        {
                                    if (ActiveMdiChild is Form1) toolStrip1.Visible = false;
                            commandBarToolStripMenuItem.Checked = false;
                        }

                                UpdateUXLines();

                        if (Properties.Settings.Default.ShowQuickSearch == true)
                        {
                            quickMenuSearchBoxToolStripMenuItem.Checked = true;
                            SearchBox.Visible = true;
                        }
                        else
                        {
                            quickMenuSearchBoxToolStripMenuItem.Checked = false;
                            SearchBox.Visible = false;
                        }
                        if (Properties.Settings.Default.ShowVisualScrollBar == true)
                        {
                            foreach (Form frmx in MdiChildren) if (frmx is Form1)
                            {
                                Form1 frm = (Form1)frmx;
                                frm.splitContainer4.Panel2Collapsed = false;
                                frm.splitContainer4.Panel2.Show();
                                frm.pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Horizontal;
                            }
                            visualScrollBarToolStripMenuItem.Checked = true;
                        }
                        else
                        {
                            foreach (Form frmx in MdiChildren) if (frmx is Form1)
                            {
                                Form1 frm = (Form1)frmx;
                                frm.splitContainer4.Panel2Collapsed = true;
                                frm.splitContainer4.Panel2.Hide();
                                frm.pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Both;
                            }
                            visualScrollBarToolStripMenuItem.Checked = false;
                        }
                        Properties.Settings.Default.SplitterDistance = Convert.ToInt32(rez[15]);
                        Properties.Settings.Default.UITheme = Convert.ToInt32(rez[16]);
                        ThemeCode();
                                foreach (Form frmx in MdiChildren)
                                    if (frmx is Form1)
                                    {
                                        Form1 frm = (Form1)frmx;
                                        frm.ThemeCode();
                            frm.tabControl3.Refresh();
                            frm.splitContainer4.Refresh();
                            frm.semiTransparentPanel1.Refresh();
                        }
                        OnClientSizeChanged(e);
                            });
                        Properties.Settings.Default.DynamicColor = Convert.ToBoolean(rez[17]);
                        Properties.Settings.Default.Save();
                    }
                }
                catch { }
            }
            else
            {
                DoOnUIThread(delegate()
                {
                    UserPanel.Visible = false;

                    //Redim();
                });
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SetForegroundWindow(this.Handle);
            if (ActiveMdiChild is Form1)

            {
                Form1 frmxa = ActiveMdiChild as Form1;
                if (frmxa.backgroundWorker1.IsBusy || frmxa.backgroundWorker2.IsBusy || frmxa.backgroundWorker3.IsBusy || frmxa.backgroundWorker4.IsBusy || frmxa.backgroundWorker5.IsBusy)
                {
                    CasetaDeMesaj(this, BackOp, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }

            // if (e.CloseReason == CloseReason..UserClosing)
            // {
                foreach (Form frm in this.MdiChildren)
                {
                    if (frm is Form1)
                    {
                        Form1 frmx = frm as Form1;
                        frmx.ReadyToClose = true;
                        frm.Close();
                        if (frm.IsDisposed == false)
                        {
                            e.Cancel = true;
                            frmx.ReadyToClose = false;
                            return;
                        }
                    }
                }

            // }
            if (Properties.Settings.Default.AutoSignIn == false && File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
            {
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
            }
            if (this.WindowState == FormWindowState.Maximized)
                Properties.Settings.Default.IsMaximized = true;
            else
            {
                Properties.Settings.Default.WindowsSize = this.Size;
                Properties.Settings.Default.WindowsPos = this.Location;
                Properties.Settings.Default.IsMaximized = false;
            }
            Properties.Settings.Default.Save();
            SaveUserSettings();
            // try
            //   {
            //    SetWindowLongPtr(this.Handle, -16, (IntPtr)(0x80000000L | 0x00C00000L | 0x00040000L | 0x00010000L | 0x00020000L));
            //  SetWindowPos(this.Handle, IntPtr.Zero, 0, 0, 0, 0, SetWindowPosFlags.FrameChanged | SetWindowPosFlags.IgnoreMove | SetWindowPosFlags.IgnoreResize);
            //  }
            //  catch { }
            if (wpfwindow != null) wpfwindow.Visibility = System.Windows.Visibility.Hidden;
            //Application.DoEvents();
            this.Hide();
            WrapUp.Enabled = true;
        }

        private void startupTimer_Tick(object sender, EventArgs e)
        {
            startupTimer.Enabled = false;
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
            {
                //plugin executor
                foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
                {
                    if (_Plugins.ContainsKey(pair.Key))
                    {
                        VPlugin plugin = _Plugins[pair.Key];
                        string rez = plugin.Perform("com.valinet.pseudocode.tools.sign-in", "");
                        if (rez == "true")
                        {
                            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
                            {
                                DoOnUIThread(delegate()
                                {
                                    OnClientSizeChanged(e);
                                    SignOut(e);
                                });
                            }
                            return;
                        }
                    }
                }
                //plugin executor finish
            }
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
            {
                if (Properties.Settings.Default.AutoSignIn == true && Properties.Settings.Default.WasWelcomeShown == true)
                {
                    if (Properties.Settings.Default.DisableEnhancedUI == false)
                    {
                        done = true;
                        glow.ContinueCode();
                        glow.Opacity = 1;
                        if (Properties.Settings.Default.DynamicColor == true) glow.wpfwindow.Foreground = border;
                        else                         glow.wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                        this.Opacity = 1;
                        SetForegroundWindow(this.Handle);
                    }
                    Sign sg = new Sign(this, false);
                    sg.StartPosition = FormStartPosition.CenterScreen;
                    sg.ShowDialog();
                }
                try
                {
                    backgroundWorker1.RunWorkerAsync();
                }
                catch { }
            }
            if (Properties.Settings.Default.ErrorMessage != "")
            {
                CasetaDeMesaj(this, Properties.Settings.Default.ErrorMessage, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Properties.Settings.Default.ErrorMessage = "";
                Properties.Settings.Default.Save();
            }
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.pseudocodeEd.Focus();
            }
            
            if (!Directory.Exists(Application.StartupPath + "\\org.mingw.mingw"))
            {
                if (Properties.Settings.Default.MinGWPath == "" || !Directory.Exists(Properties.Settings.Default.MinGWPath))
                {
                    if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        Properties.Settings.Default.MinGWPath = folderBrowserDialog1.SelectedPath;
                        Properties.Settings.Default.Save();
                    }
                    else Process.GetCurrentProcess().Kill();
                }
            }
            else
            {
                if (Properties.Settings.Default.MinGWPath == "")
                {
                    Properties.Settings.Default.MinGWPath = Application.StartupPath + "\\org.mingw.mingw";
                    Properties.Settings.Default.Save();
                }
            }
        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }

        private void UserDropPanel_Leave(object sender, EventArgs e)
        {
            UserDropPanel.Visible = false;
        }
        public ToolStripData MainToolStripUserSettings;
        public void LoadUserSettings()
        {
            try
            {
                IsolatedStorageFileStream fs = new IsolatedStorageFileStream
                        ("Toolbar-Settings.cfg", FileMode.OpenOrCreate);
                BinaryFormatter bf = new BinaryFormatter();

                if (fs.Length > 0)
                {
                    try
                    {
                        MainToolStripUserSettings = (ToolStripData)bf.Deserialize(fs);
                        //deserialize other user preferences
                    }
                    catch (Exception e)
                    {
                        //serialization exception caught            
                    }
                }
                else
                {
                    MainToolStripUserSettings = new ToolStripData();
                }

                fs.Close();
            }
            catch { }
        }

        public void SaveUserSettings()
        {
           // try
          //  {
                IsolatedStorageFileStream fs = new IsolatedStorageFileStream
                        ("Toolbar-Settings.cfg", FileMode.Create);
                BinaryFormatter bf = new BinaryFormatter();

                if (MainToolStripUserSettings != null)
                {
                    try
                    {
                        bf.Serialize(fs, MainToolStripUserSettings);
                        //serialize other user preferences
                    }
                    catch (Exception e)
                    {
                        //serialization exception caught
                    }
                }
                fs.Close();
          //  }
          //  catch { }
        }
        public void AccSet()
        {
            UserDropPanel.Visible = false;


            Sign sg = new Sign(this, true);
            sg.ShowDialog();
            //label27.Left = pictureBox2.Left;
            try
            {
                using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                {
                    pictureBox2.Image = Image.FromStream(stream);
                    pictureBox3.Image = Image.FromStream(stream);
                }
            }
            catch { }
            label27.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
            label28.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
            signInToolStripMenuItem.Text = ConnectedAs + File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
            label27.Left = 15;
            UserPanel.Width = label27.Width + 25 + pictureBox2.Width;
            if (SearchBox.Visible) UserPanel.Left = SearchBox.Left - UserPanel.Width - 3;
            else UserPanel.Left = this.Width - UserPanel.Width - 8;
            UserPanel.BringToFront();
            //else UserPanel.Left = SearchBox.Left  - UserPanel.Width - 3;
            linkLabel5.Visible = true;

        }
        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AccSet();
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process p = Process.Start("https://www.dropbox.com/account#settings");
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                {
                    CasetaDeMesaj(this, NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            UserDropPanel.Visible = false;
        }
        void SignOut(EventArgs e)
        {
            UserDropPanel.Visible = false;
            OnClientSizeChanged(e);
            WaitSignOut ws = new WaitSignOut(this, false);
            if (ws.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {


                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                UserPanel.Visible = false;

                //Sign sg = new Sign(this, false);
                //sg.ShowDialog();
                try
                {

                    backgroundWorker1.RunWorkerAsync();
                }
                catch { }
            }
        }
        private void button47_Click(object sender, EventArgs e)
        {
            SignOut(e);
        }

        private void SearchBox_Click(object sender, EventArgs e)
        {

        }
        public void ManageCloudData()
        {
            try
            {
                Process p = Process.Start("https://www.dropbox.com/home/Apps/InfoEdu");
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                {
                    CasetaDeMesaj(this, NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }
        private void signInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //plugin executor
            foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
            {
                if (_Plugins.ContainsKey(pair.Key))
                {
                    VPlugin plugin = _Plugins[pair.Key];
                    string rez = plugin.Perform("com.valinet.pseudocode.tools.sign-in", "");
                    if (rez == "true")
                    {
                        if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat")) button47.PerformClick();
                        return;
                    }
                }
            }
            //plugin executor finish
            if (signInToolStripMenuItem.Text.StartsWith(ConnectedAs) == true || UserPanel.Visible == true)
            {
                //panel12.Visible = false;
                // button48.BackColor = Color.White;
                // label31.BackColor = Color.White;
                // CasetaDeMesaj(this, AlreadySignedIn, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                AccountSettings ac = new AccountSettings();
                //ac.linkLabel5.Visible = linkLabel5.Visible;
                ac.label28.Text = label28.Text;
                ac.pictureBox3.Image = pictureBox3.Image;
                DialogResult dr = ac.ShowDialog();
                if (dr == DialogResult.Abort)
                {
                    AccSet();
                }
                else if (dr == DialogResult.Retry)
                {
                    try
                    {
                        Process p = Process.Start("https://www.dropbox.com/account#settings");
                    }
                    catch (System.ComponentModel.Win32Exception noBrowser)
                    {
                        if (noBrowser.ErrorCode == -2147467259)
                        {
                            CasetaDeMesaj(this, NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    UserDropPanel.Visible = false;
                }
                else if (dr == DialogResult.Ignore)
                {
                    button47.PerformClick();
                }
                else if (dr == DialogResult.No)
                {
                    ManageCloudData();
                }
            }
            else
            {
                //plugin executor
                /*foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
                {
                    if (_Plugins.ContainsKey(pair.Key))
                    {
                        VPlugin plugin = _Plugins[pair.Key];
                        bool rez = plugin.UserIsAboutToSignIntoDropbox();
                        if (rez == true)
                        {
                            return;
                        }
                    }
                }*/
                //plugin executor finish
           //     panel12.Visible = false;
            //    button48.BackColor = Color.White;
            //    label31.BackColor = Color.White;
                Properties.Settings.Default.AutoSignIn = true;
                Properties.Settings.Default.Save();
                Sign sg = new Sign(this, false);
                sg.ShowDialog();
                backgroundWorker1.RunWorkerAsync();

            }
        }
        public int time = 0;
        private void Loop_Tick(object sender, EventArgs e)
        {
            TopCover.Width = this.Width - (int)((this.Padding.Right + 1) * ((float)dx / 96)) - 5;
            TopCover.Left = this.Padding.Left + 1;
       //     if (IsMaximised) Right.Left = this.Width - (int)(1 * ((float)dx / 96));
       //     else Right.Left = this.Width - (int)(17 * ((float)dx / 96));
            Right.BringToFront();
            if (ActiveMdiChild is Form1)

            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.Loop();
                time++;
                if (time == 5)
                {
                    foreach (Form frmx in MdiChildren)
                        if (frmx is Form1)
                        {
                            Form1 frmt = (Form1)frmx;
                            frmt.AutoSalvare();
                            frmt.timer = 0;
                        }
                    Loop.Enabled = false;
                }
            }
            
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button1.PerformClick();
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)

            {
                Form1 frm = ActiveMdiChild as Form1;

                frm.button4.PerformClick();
            }
        }

        private void saveLogicSchemaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button59.PerformClick();
            }
        }

        private void printPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)

            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button7.PerformClick();
            }
        }

        private void printConvertedCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button30.PerformClick();
            }
        }

        private void printLogicSchemeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button60.PerformClick();
            }
        }

        private void recoverLostunsavedFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (recoverLostunsavedFilesToolStripMenuItem.HasDropDownItems == false)
                CasetaDeMesaj(this, NothingToRecover, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //Form1 frm = ActiveMdiChild as Form1;
            //frm.button8.PerformClick();
        }

        private void toBasicClassicScriptFilevbsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button29.PerformClick();
            }
        }

        private void toCSourceFilecppToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button31.PerformClick();
            }
        }

        private void toCSourceFilecsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button32.PerformClick();
            }
        }

        private void toPascalSourceFilepasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button33.PerformClick();
            }
        }

        private void toJavaScriptpoweredWebPagehtmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button34.PerformClick();
            }
        }

        private void exitPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void undoActionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button56.PerformClick();
            }
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button55.PerformClick();
            }
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button13.PerformClick();
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button14.PerformClick();
            }
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button12.PerformClick();
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button18.PerformClick();
            }
        }

        private void copyCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button15.PerformClick();
            }
        }

        private void jumpToLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button19.PerformClick();
            }
        }

        private void goToDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.goToDefinitionToolStripMenuItem.PerformClick();
            }
        }

        private void renameVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.renameVariableToolStripMenuItem.PerformClick();
            }
        }

        private void jumpToBreakpointToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                if (Properties.Settings.Default.PopulateFind)
                {
                    if (frm.pseudocodeEd.Selection.Length == 0)
                    {
                        var pos = frm.pseudocodeEd.NativeInterface.GetCurrentPos();
                        string oldname = frm.pseudocodeEd.GetWordFromPosition(pos);
                        frm.pseudocodeEd.FindReplace.Window.cboFindF.Text = oldname;
                    }
                    else
                    {
                        if (frm.pseudocodeEd.Selection.Range.StartingLine == frm.pseudocodeEd.Selection.Range.EndingLine) frm.pseudocodeEd.FindReplace.Window.cboFindF.Text = frm.pseudocodeEd.Selection.Text;

                    }
                }
                frm.pseudocodeEd.FindReplace.ShowFind();
            }
        }

        private void replaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                if (Properties.Settings.Default.PopulateFind)
                {
                    if (frm.pseudocodeEd.Selection.Length == 0)
                    {
                        var pos = frm.pseudocodeEd.NativeInterface.GetCurrentPos();
                        string oldname = frm.pseudocodeEd.GetWordFromPosition(pos);
                        frm.pseudocodeEd.FindReplace.Window.cboFindR.Text = oldname;
                    }
                    else
                    {
                        if (frm.pseudocodeEd.Selection.Range.StartingLine == frm.pseudocodeEd.Selection.Range.EndingLine) frm.pseudocodeEd.FindReplace.Window.cboFindR.Text = frm.pseudocodeEd.Selection.Text;

                    }
                    frm.pseudocodeEd.FindReplace.Window.btnReplaceAll.Enabled = true;
                    frm.pseudocodeEd.FindReplace.Window.btnReplaceNext.Enabled = true;
                    frm.pseudocodeEd.FindReplace.Window.btnReplacePrevious.Enabled = true;
                }
                frm.pseudocodeEd.FindReplace.ShowReplace();
            }
        }

        private void advancedFindToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.pseudocodeEd.FindReplace.ShowFind();
            }
        }

        private void statisticsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.buttonItem2.PerformClick();
            }
        }

        private void compileAndRunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button11.PerformClick();
            }
        }

        private void justCompileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.pseudocodeEd.Focus();
                SendKeys.Send("{F6}");
            }
        }

        private void startDebuggingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.pseudocodeEd.Focus();
                SendKeys.Send("{F8}");
            }
        }

        private void continueDebuggingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* Form1 frm = ActiveMdiChild as Form1;
             frm.tabControl3.SelectedIndex = 1;
             frm.textBox6.Focus();*/
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.pseudocodeEd.Focus();
                SendKeys.Send("{F7}");
            }
        }

        private void watchesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.tabControl3.SelectedIndex = 2;
            }
        }

        private void sendCommandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.tabControl3.SelectedIndex = 1;
                frm.textBox6.Focus();
            }
        }

        private void addremoveBreakpointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.addremoveBreakpointOnThisLineToolStripMenuItem.PerformClick();
            }
        }

        private void addremoveWatchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.addremoveWatchOnThisVariableToolStripMenuItem.PerformClick();
            }
        }

        private void endPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button65.PerformClick();
            }
        }

        private void restartPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button65.PerformClick();
                frm.pseudocodeEd.Focus();
                SendKeys.Send("{F8}");
            }
        }

        private void createApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button20.PerformClick();
            }
        }

        private void insertModuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button65.PerformClick();
            }
        }

        private void extensionsManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
          /*  foreach (Control ctl in this.Controls)
            {
                if (ctl.BackColor == Color.Black) ctl.BackColor = Color.White;
                if (ctl.ForeColor == Color.White) ctl.ForeColor = Color.Black;
                if (ctl.BackColor == Color.FromArgb(64,64,64)) ctl.ForeColor = Color.White;
                if (ctl.BackColor == Color.Gray) ctl.BackColor = Color.White;
                if (ctl.BackColor == Color.Silver) ctl.BackColor = Color.White;

 
            }
            foreach (Control ctl in ActiveMdiChild.Controls)
            {
                if (ctl.BackColor == Color.Black) ctl.BackColor = Color.White;
                if (ctl.ForeColor == Color.White) ctl.ForeColor = Color.Black;
                if (ctl.BackColor == Color.FromArgb(64, 64, 64)) ctl.ForeColor = Color.White;
                if (ctl.BackColor == Color.Gray) ctl.BackColor = Color.White;
                if (ctl.BackColor == Color.Silver) ctl.BackColor = Color.White;


            }*/
            //return;
            ManageRepos ex = new ManageRepos(this);
            ex.ShowDialog();
        }

        private void feedbackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //panel12.Visible = false;
            ScreenCapture sc = new ScreenCapture();
            // capture entire screen, and save it to a file
            Image img = sc.CaptureScreen();
            // display image in a Picture control named imageDisplay
            this.imageDisplay.Image = img;
            // capture this window, and save it
            sc.CaptureWindowToFile(this.Handle, Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.png", System.Drawing.Imaging.ImageFormat.Png);
            Feedback mail = new Feedback(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.png");
            mail.ShowDialog();
        }

        private void checkForUpdatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                showInfo = true;
                UpdateChecker.RunWorkerAsync();
            }
            catch { }
        }

        private void licenseAgreementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowBrowser("http://valinet.ro/pseudocodelicense");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutX about = new AboutX();
            about.ShowDialog();
        }

        private void dockPanel_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void closeFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.ReadyToClose = true;
                frm.Close();
                if (dockPanel.Panes.Count == 0) New(NewTabTitle + " " + (dockPanel.Panes.Count + 1).ToString());
            }
            else if (ActiveMdiChild is Browser)
            {
                closePageToolStripMenuItem.PerformClick();
            }
        }

        private void saveAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form frmx in MdiChildren) if (frmx is Form1)
            {
                Form1 frm = (Form1)frmx;
                frm.button1.PerformClick();
            }
        }

        private void deleteSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button17.PerformClick();
            }
        }

        private void helpSupportAndTipsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.pseudocodeEd.Focus();
                SendKeys.Send("{F1}");
            }
        }

        private void addremoveBreakpointsOnLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.addBreakpointsOnAllLinesToolStripMenuItem.PerformClick();
            }
        }

        private void advancedCommandsPanelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            advancedCommandsPanelToolStripMenuItem.Checked = !advancedCommandsPanelToolStripMenuItem.Checked;
            Properties.Settings.Default.ShowAdvancedPanel = advancedCommandsPanelToolStripMenuItem.Checked;
            if (Properties.Settings.Default.ShowAdvancedPanel == true)
            {
                foreach (Form frmx in MdiChildren) if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.tabControl3.Visible = true;
                    frm.splitContainer3.Panel2Collapsed = false;
                    frm.splitContainer3.Panel2.Show();
                }
                advancedCommandsPanelToolStripMenuItem.Checked = true;
                
            }
            else
            {
                foreach (Form frmx in MdiChildren) if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.tabControl3.Visible = false;
                    frm.splitContainer3.Panel2Collapsed = true;
                    frm.splitContainer3.Panel2.Hide();
                }
                advancedCommandsPanelToolStripMenuItem.Checked = false;
            }
        }

        private void statusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusBarToolStripMenuItem.Checked = !statusBarToolStripMenuItem.Checked;
            Properties.Settings.Default.ShowStatusBar = statusBarToolStripMenuItem.Checked;
            if (Properties.Settings.Default.ShowStatusBar == true)
            {
                foreach (Form frmx in MdiChildren) if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.statusBar1.Visible = true;
                    frm.BottomDown.Visible = true;
                    frm.BottomDown.Top = frm.Height - frm.statusBar1.Height - 2;
                }
                statusBarToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren) if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.statusBar1.Visible = false;
                    frm.BottomDown.Visible = false;

                }
                statusBarToolStripMenuItem.Checked = false;
            }

        }

        private void commandBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commandBarToolStripMenuItem.Checked = !commandBarToolStripMenuItem.Checked;
            Properties.Settings.Default.ShowCommandBar = commandBarToolStripMenuItem.Checked;
            if (Properties.Settings.Default.ShowCommandBar == true)
            {
                toolStrip1.Visible = true;
                commandBarToolStripMenuItem.Checked = true;
            }
            else
            {
                toolStrip1.Visible = false;
                commandBarToolStripMenuItem.Checked = false;
            }
            UpdateUXLines();


        }

        private void convertedCodeLogicSchemaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            convertedCodeLogicSchemaToolStripMenuItem.Checked = !convertedCodeLogicSchemaToolStripMenuItem.Checked;
            Properties.Settings.Default.ShowConvertedCode = convertedCodeLogicSchemaToolStripMenuItem.Checked;
            if (Properties.Settings.Default.ShowConvertedCode == true)
            {
                foreach (Form frmx in MdiChildren) if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.splitContainer2.Panel2Collapsed = false;
                    frm.splitContainer2.Panel2.Show();
                }
                convertedCodeLogicSchemaToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren) if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.splitContainer2.Panel2Collapsed = true;
                    frm.splitContainer2.Panel2.Hide();
                }
                convertedCodeLogicSchemaToolStripMenuItem.Checked = false;
            }
        }

        private void zoom100_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Zoom = 0;
            foreach (Form frmx in MdiChildren) if (frmx is Form1)
            {
                Form1 frm = (Form1)frmx;
                frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                    if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                frm.pseudocodeEd.ZoomFactorChanged -= frm.pseudocodeEd_ZoomFactorChanged;
                frm.pseudocodeEd.ZoomFactor = 0;
                frm.converterEd.ZoomFactor = 0;
                frm.pseudocodeEd.ZoomFactorChanged += frm.pseudocodeEd_ZoomFactorChanged;
            }
            zoom25.Checked = false;
            zoom50.Checked = false;
            zoom75.Checked = false;
            zoom100.Checked = true;
            zoom200.Checked = false;
            zoom300.Checked = false;
            zoom400.Checked = false;
            customizeToolStripMenuItem.Checked = false;
        }

        private void zoomInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form frmx in MdiChildren) if (frmx is Form1)
            {
                Form1 frm = (Form1)frmx;
                frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                    if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                frm.pseudocodeEd.ZoomFactorChanged -= frm.pseudocodeEd_ZoomFactorChanged;
                frm.pseudocodeEd.ZoomIn();
                frm.converterEd.ZoomIn();
                frm.pseudocodeEd.ZoomFactorChanged += frm.pseudocodeEd_ZoomFactorChanged;
                Properties.Settings.Default.Zoom = frm.pseudocodeEd.ZoomFactor;
            }
            zoom25.Checked = false;
            zoom50.Checked = false;
            zoom75.Checked = false;
            zoom100.Checked = false;
            zoom200.Checked = false;
            zoom300.Checked = false;
            zoom400.Checked = false;
            customizeToolStripMenuItem.Checked = false;
            switch (Properties.Settings.Default.Zoom)
            {
                case 0:
                    {
                        zoom100.Checked = true;
                        break;
                    }
                case 1:
                    {
                        zoom200.Checked = true;
                        break;
                    }
                case 2:
                    {
                        zoom300.Checked = true;
                        break;
                    }
                case 3:
                    {
                        zoom400.Checked = true;
                        break;
                    }
                case -1:
                    {
                        zoom75.Checked = true;
                        break;
                    }
                case -2:
                    {
                        zoom50.Checked = true;
                        break;
                    }
                case -3:
                    {
                        zoom25.Checked = true;
                        break;
                    }
                default:
                    {
                        customizeToolStripMenuItem.Checked = true;
                        break;
                    }
            }
        }

        private void zoomOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form frmx in MdiChildren) if (frmx is Form1)
            {
                Form1 frm = (Form1)frmx;
                frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                    if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                frm.pseudocodeEd.ZoomFactorChanged -= frm.pseudocodeEd_ZoomFactorChanged;
                frm.pseudocodeEd.ZoomOut();
                frm.converterEd.ZoomOut();
                frm.pseudocodeEd.ZoomFactorChanged += frm.pseudocodeEd_ZoomFactorChanged;
                Properties.Settings.Default.Zoom = frm.pseudocodeEd.ZoomFactor;
            }
            zoom25.Checked = false;
            zoom50.Checked = false;
            zoom75.Checked = false;
            zoom100.Checked = false;
            zoom200.Checked = false;
            zoom300.Checked = false;
            zoom400.Checked = false;
            customizeToolStripMenuItem.Checked = false;
            switch (Properties.Settings.Default.Zoom)
            {
                case 0:
                    {
                        zoom100.Checked = true;
                        break;
                    }
                case 1:
                    {
                        zoom200.Checked = true;
                        break;
                    }
                case 2:
                    {
                        zoom300.Checked = true;
                        break;
                    }
                case 3:
                    {
                        zoom400.Checked = true;
                        break;
                    }
                case -1:
                    {
                        zoom75.Checked = true;
                        break;
                    }
                case -2:
                    {
                        zoom50.Checked = true;
                        break;
                    }
                case -3:
                    {
                        zoom25.Checked = true;
                        break;
                    }
                default:
                    {
                        customizeToolStripMenuItem.Checked = true;
                        break;
                    }
            }
        }

        private void zoom300_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Zoom = 2;
                foreach (Form frmx in MdiChildren) if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                    if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                frm.pseudocodeEd.ZoomFactorChanged -= frm.pseudocodeEd_ZoomFactorChanged;
                frm.pseudocodeEd.ZoomFactor = 2;
                frm.converterEd.ZoomFactor = 2;
                frm.pseudocodeEd.ZoomFactorChanged += frm.pseudocodeEd_ZoomFactorChanged;
            }
            zoom25.Checked = false;
            zoom50.Checked = false;
            zoom75.Checked = false;
            zoom100.Checked = false;
            zoom200.Checked = false;
            zoom300.Checked = true;
            zoom400.Checked = false;
            customizeToolStripMenuItem.Checked = false;
        }

        private void zoom200_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Zoom = 1;
            foreach (Form frmx in MdiChildren)
                if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                    if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                frm.pseudocodeEd.ZoomFactorChanged -= frm.pseudocodeEd_ZoomFactorChanged;
                frm.pseudocodeEd.ZoomFactor = 1;
                frm.converterEd.ZoomFactor = 1;
                frm.pseudocodeEd.ZoomFactorChanged += frm.pseudocodeEd_ZoomFactorChanged;
            }
            zoom25.Checked = false;
            zoom50.Checked = false;
            zoom75.Checked = false;
            zoom100.Checked = false;
            zoom200.Checked = true;
            zoom300.Checked = false;
            zoom400.Checked = false;
            customizeToolStripMenuItem.Checked = false;
        }

        private void zoom75_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Zoom = -1;
            foreach (Form frmx in MdiChildren)
                if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                    if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                frm.pseudocodeEd.ZoomFactorChanged -= frm.pseudocodeEd_ZoomFactorChanged;
                frm.pseudocodeEd.ZoomFactor = -1;
                frm.converterEd.ZoomFactor = -1;
                frm.pseudocodeEd.ZoomFactorChanged += frm.pseudocodeEd_ZoomFactorChanged;
            }
            zoom25.Checked = false;
            zoom50.Checked = false;
            zoom75.Checked = true;
            zoom100.Checked = false;
            zoom200.Checked = false;
            zoom300.Checked = false;
            zoom400.Checked = false;
            customizeToolStripMenuItem.Checked = false;
        }

        private void zoom50_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Zoom = -2;
            foreach (Form frmx in MdiChildren)
                if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                    if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                frm.pseudocodeEd.ZoomFactorChanged -= frm.pseudocodeEd_ZoomFactorChanged;
                frm.pseudocodeEd.ZoomFactor = -2;
                frm.converterEd.ZoomFactor = -2;
                frm.pseudocodeEd.ZoomFactorChanged += frm.pseudocodeEd_ZoomFactorChanged;
            }
            zoom25.Checked = false;
            zoom50.Checked = true;
            zoom75.Checked = false;
            zoom100.Checked = false;
            zoom200.Checked = false;
            zoom300.Checked = false;
            zoom400.Checked = false;
            customizeToolStripMenuItem.Checked = false;
        }

        private void zoom25_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Zoom = -3;
            foreach (Form frmx in MdiChildren)
                if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                    if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                frm.pseudocodeEd.ZoomFactorChanged -= frm.pseudocodeEd_ZoomFactorChanged;
                frm.pseudocodeEd.ZoomFactor = -3;
                frm.converterEd.ZoomFactor = -3;
                frm.pseudocodeEd.ZoomFactorChanged += frm.pseudocodeEd_ZoomFactorChanged;
            }
            zoom25.Checked = true;
            zoom50.Checked = false;
            zoom75.Checked = false;
            zoom100.Checked = false;
            zoom200.Checked = false;
            zoom300.Checked = false;
            zoom400.Checked = false;
            customizeToolStripMenuItem.Checked = false;
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Zoom = 3;
            foreach (Form frmx in MdiChildren)
                if (frmx is Form1)
                {
                    Form1 frm = (Form1)frmx;
                    frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                    if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                frm.pseudocodeEd.ZoomFactorChanged -= frm.pseudocodeEd_ZoomFactorChanged;
                frm.pseudocodeEd.ZoomFactor = 3;
                frm.converterEd.ZoomFactor = 3;
                frm.pseudocodeEd.ZoomFactorChanged += frm.pseudocodeEd_ZoomFactorChanged;
            }
            zoom25.Checked = false;
            zoom50.Checked = false;
            zoom75.Checked = false;
            zoom100.Checked = false;
            zoom200.Checked = false;
            zoom300.Checked = false;
            zoom400.Checked = true;
            customizeToolStripMenuItem.Checked = false;
        }
        Zoom zoom;
        private void customizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frmt = (Form1)frmx;
                        frmt.pseudocodeEd.ZoomFactorChanged -= frmt.pseudocodeEd_ZoomFactorChanged;
                    }
                zoom.trackBar1.ValueChanged -= zoom.trackBar1_ValueChanged;
                zoom.trackBar1.Value = frm.pseudocodeEd.ZoomFactor;
                zoom.trackBar1.ValueChanged += zoom.trackBar1_ValueChanged;
                zoom.Show();
            }
            /*string xS = Microsoft.VisualBasic.Interaction.InputBox(ZoomText, AppName, Properties.Settings.Default.Zoom.ToString());
            try
            {
                int x = Convert.ToInt32(xS);
                Properties.Settings.Default.Zoom = x;
                foreach (Form1 frm in MdiChildren)
                {
                    frm.pseudocodeEd.ZoomFactor = x;
                    frm.converterEd.ZoomFactor = x;
                }
                zoom25.Checked = false;
                zoom50.Checked = false;
                zoom75.Checked = false;
                zoom100.Checked = false;
                zoom200.Checked = false;
                zoom300.Checked = false;
                zoom400.Checked = false;
                customizeToolStripMenuItem.Checked = true;
            }
            catch { }*/
        }

        private void visualScrollBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            visualScrollBarToolStripMenuItem.Checked = !visualScrollBarToolStripMenuItem.Checked;
            Properties.Settings.Default.ShowVisualScrollBar = visualScrollBarToolStripMenuItem.Checked;
            if (Properties.Settings.Default.ShowVisualScrollBar == true)
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer4.Panel2Collapsed = false;
                    frm.splitContainer4.Panel2.Show();
                    frm.pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Horizontal;
                }
                visualScrollBarToolStripMenuItem.Checked = true;
            }
            else
            {
                foreach (Form frmx in MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.splitContainer4.Panel2Collapsed = true;
                    frm.splitContainer4.Panel2.Hide();
                    frm.pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Both;
                }
                visualScrollBarToolStripMenuItem.Checked = false;
            }
        }

        private void quickMenuSearchBoxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            quickMenuSearchBoxToolStripMenuItem.Checked = !quickMenuSearchBoxToolStripMenuItem.Checked;
            Properties.Settings.Default.ShowQuickSearch = quickMenuSearchBoxToolStripMenuItem.Checked;
            if (Properties.Settings.Default.ShowQuickSearch == true)
            {
                SearchBox.Visible = true;
               // statusBarToolStripMenuItem.Checked = true;
            }
            else
            {
                SearchBox.Visible = false;
               // statusBarToolStripMenuItem.Checked = false;
            }
        }

        private void uploadPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.button10.PerformClick();
            }
        }

        private void toolStripButton12_Click(object sender, EventArgs e)
        {
            uploadPseudocodeToolStripMenuItem.PerformClick();
        }

        private void ceEsteAceastaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Try to cast the sender to a ToolStripItem
            ToolStripItem menuItem = sender as ToolStripItem;
            if (menuItem != null)
            {
                // Retrieve the ContextMenuStrip that owns this ToolStripItem
                ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
                if (owner != null)
                {
                    // Get the control that is displaying this context menu
                    Control sourceControl = owner.SourceControl;
                    if (sourceControl.Name == "toolStrip1")
                    {
                        if (ActiveMdiChild is Form1)
                        {
                            Form1 frm = ActiveMdiChild as Form1;
                            frm.WhatsThisF("command-bar");
                        }

                    }
                    if (sourceControl.Name == "menuStrip1")
                    {
                        if (ActiveMdiChild is Form1)
                        {
                            Form1 frm = ActiveMdiChild as Form1;
                            frm.WhatsThisF("menu-bar");
                        }
                    }
                    if (sourceControl.Name == "SearchBox")
                    {
                        if (ActiveMdiChild is Form1)
                        {
                            Form1 frm = ActiveMdiChild as Form1;
                            frm.WhatsThisF("search-bar");
                        }
                    }
                }
            }
        }

        private void fromEnglishCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fromEnglishCodeToolStripMenuItem.Checked = true;
            fromRomanianPseudoccodeToolStripMenuItem.Checked = false;
            userdefinedToolStripMenuItem.Checked = false;
            if (fromEnglishCodeToolStripMenuItem.Checked)
            {
                Form1 frm = (Form1)ActiveMdiChild;
                frm.IntegerKeyword = "integer";
                frm.DoubleKeyword = "real";
                frm.CharKeyword = "charcater";
                frm.StringKeyword = "string";
                frm.DivKeyword = "div";
                frm.ModKeyword = "mod";
                frm.DifferentKeyword = "different";
                frm.AndKeyword = "and";
                frm.OrKeyword = "or";
                frm.ReadKeyword = "read";
                frm.WriteKeyword = "write";
                frm.IfKeyword = "if";
                frm.ThenKeyword = "then";
                frm.ElseKeyword = "else";
                frm.EndIfKeyword = "end_if";
                frm.WhileKeyword = "while";
                frm.EndWhileKeyword = "end_while";
                frm.ForKeyword = "for";
                frm.EndForKeyword = "end_for";
                frm.DoKeyword = "do";
                frm.LoopUntilKeyword = "loop_until";
                frm.InputText = "Use the keyboard to type charcaters which will represent the value of the following variable: ";
                frm.YES = "YES";
                frm.NO = "NO";
                frm.str[0] = frm.IntegerKeyword;
                frm.str[1] = frm.DoubleKeyword;
                frm.str[2] = frm.CharKeyword;
                frm.str[3] = frm.StringKeyword;
                frm.str[4] = frm.DivKeyword;
                frm.str[5] = frm.ModKeyword;
                frm.str[6] = frm.DifferentKeyword;
                frm.str[7] = frm.AndKeyword;
                frm.str[8] = frm.OrKeyword;
                frm.str[9] = frm.ReadKeyword;
                frm.str[10] = frm.WriteKeyword;
                frm.str[11] = frm.IfKeyword;
                frm.str[12] = frm.ThenKeyword;
                frm.str[13] = frm.ElseKeyword;
                frm.str[14] = frm.EndIfKeyword;
                frm.str[15] = frm.WhileKeyword;
                frm.str[16] = frm.EndWhileKeyword;
                frm.str[17] = frm.ForKeyword;
                frm.str[18] = frm.EndForKeyword;
                frm.str[19] = frm.DoKeyword;
                frm.str[20] = frm.LoopUntilKeyword;
                frm.pseudocodeEd.Lexing.Keywords[0] = frm.ReadKeyword + " " + frm.WriteKeyword + " " + frm.IfKeyword + " " + frm.ThenKeyword + " " + frm.ElseKeyword + " " + frm.EndIfKeyword + " " + frm.WhileKeyword + " " + frm.EndWhileKeyword + " " + frm.ForKeyword + " " + frm.EndForKeyword + " " + frm.DoKeyword + " " + frm.LoopUntilKeyword;
                frm.pseudocodeEd.Lexing.Keywords[1] = frm.IntegerKeyword + " " + frm.DoubleKeyword + " " + frm.CharKeyword + " " + frm.StringKeyword + " " + frm.DivKeyword + " " + frm.ModKeyword + " " + frm.DifferentKeyword + " " + frm.AndKeyword + " " + frm.OrKeyword;
                frm.VisualScrollBar.Lexing.Keywords[0] = frm.ReadKeyword + " " + frm.WriteKeyword + " " + frm.IfKeyword + " " + frm.ThenKeyword + " " + frm.ElseKeyword + " " + frm.EndIfKeyword + " " + frm.WhileKeyword + " " + frm.EndWhileKeyword + " " + frm.ForKeyword + " " + frm.EndForKeyword + " " + frm.DoKeyword + " " + frm.LoopUntilKeyword;
                frm.VisualScrollBar.Lexing.Keywords[1] = frm.IntegerKeyword + " " + frm.DoubleKeyword + " " + frm.CharKeyword + " " + frm.StringKeyword + " " + frm.DivKeyword + " " + frm.ModKeyword + " " + frm.DifferentKeyword + " " + frm.AndKeyword + " " + frm.OrKeyword;
                frm.PseudocodeLang = 0;
                frm.converterEd.Text = frm.Compile(frm.pseudocodeEd.Text, frm.lang);
                frm.flagReident = true;
                for (int i = 0; i <= frm.fileProperties.Count - 1; i++)
                {
                    if (frm.fileProperties[i].StartsWith("language"))
                    {
                        frm.fileProperties[i] = "language:0";
                    }
                }
            }
        }

        private void fromRomanianPseudoccodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fromRomanianPseudoccodeToolStripMenuItem.Checked = true;
            fromEnglishCodeToolStripMenuItem.Checked = false;
            userdefinedToolStripMenuItem.Checked = false;
            if (fromRomanianPseudoccodeToolStripMenuItem.Checked)
            {
                Form1 frm = (Form1)ActiveMdiChild;
                frm.IntegerKeyword = "intreg";
                frm.DoubleKeyword = "real";
                frm.CharKeyword = "caracter";
                frm.StringKeyword = "sir";
                frm.DivKeyword = "div";
                frm.ModKeyword = "mod";
                frm.DifferentKeyword = "diferit";
                frm.AndKeyword = "si";
                frm.OrKeyword = "sau";
                frm.ReadKeyword = "citeste";
                frm.WriteKeyword = "scrie";
                frm.IfKeyword = "daca";
                frm.ThenKeyword = "atunci";
                frm.ElseKeyword = "altfel";
                frm.EndIfKeyword = "sfarsit_daca";
                frm.WhileKeyword = "cat_timp";
                frm.EndWhileKeyword = "sfarsit_cat_timp";
                frm.ForKeyword = "pentru";
                frm.EndForKeyword = "sfarsit_pentru";
                frm.DoKeyword = "executa";
                frm.LoopUntilKeyword = "pana_cand";
                frm.InputText = "Introduceti de la tastatura caractere. Acestea vor reprezenta continutul variabilei ";
                frm.YES = "DA";
                frm.NO = "NU";
                frm.str[0] = frm.IntegerKeyword;
                frm.str[1] = frm.DoubleKeyword;
                frm.str[2] = frm.CharKeyword;
                frm.str[3] = frm.StringKeyword;
                frm.str[4] = frm.DivKeyword;
                frm.str[5] = frm.ModKeyword;
                frm.str[6] = frm.DifferentKeyword;
                frm.str[7] = frm.AndKeyword;
                frm.str[8] = frm.OrKeyword;
                frm.str[9] = frm.ReadKeyword;
                frm.str[10] = frm.WriteKeyword;
                frm.str[11] = frm.IfKeyword;
                frm.str[12] = frm.ThenKeyword;
                frm.str[13] = frm.ElseKeyword;
                frm.str[14] = frm.EndIfKeyword;
                frm.str[15] = frm.WhileKeyword;
                frm.str[16] = frm.EndWhileKeyword;
                frm.str[17] = frm.ForKeyword;
                frm.str[18] = frm.EndForKeyword;
                frm.str[19] = frm.DoKeyword;
                frm.str[20] = frm.LoopUntilKeyword;
                frm.pseudocodeEd.Lexing.Keywords[0] = frm.ReadKeyword + " " + frm.WriteKeyword + " " + frm.IfKeyword + " " + frm.ThenKeyword + " " + frm.ElseKeyword + " " + frm.EndIfKeyword + " " + frm.WhileKeyword + " " + frm.EndWhileKeyword + " " + frm.ForKeyword + " " + frm.EndForKeyword + " " + frm.DoKeyword + " " + frm.LoopUntilKeyword;
                frm.pseudocodeEd.Lexing.Keywords[1] = frm.IntegerKeyword + " " + frm.DoubleKeyword + " " + frm.CharKeyword + " " + frm.StringKeyword + " " + frm.DivKeyword + " " + frm.ModKeyword + " " + frm.DifferentKeyword + " " + frm.AndKeyword + " " + frm.OrKeyword;
                frm.VisualScrollBar.Lexing.Keywords[0] = frm.ReadKeyword + " " + frm.WriteKeyword + " " + frm.IfKeyword + " " + frm.ThenKeyword + " " + frm.ElseKeyword + " " + frm.EndIfKeyword + " " + frm.WhileKeyword + " " + frm.EndWhileKeyword + " " + frm.ForKeyword + " " + frm.EndForKeyword + " " + frm.DoKeyword + " " + frm.LoopUntilKeyword;
                frm.VisualScrollBar.Lexing.Keywords[1] = frm.IntegerKeyword + " " + frm.DoubleKeyword + " " + frm.CharKeyword + " " + frm.StringKeyword + " " + frm.DivKeyword + " " + frm.ModKeyword + " " + frm.DifferentKeyword + " " + frm.AndKeyword + " " + frm.OrKeyword;
                frm.PseudocodeLang = 1;
                frm.converterEd.Text = frm.Compile(frm.pseudocodeEd.Text, frm.lang);
                frm.flagReident = true;
                for (int i = 0; i <= frm.fileProperties.Count - 1; i++)
                {
                    if (frm.fileProperties[i].StartsWith("language"))
                    {
                        frm.fileProperties[i] = "language:1";
                    }
                }
            }
        }

        private void englishToRomanianPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CasetaDeMesaj(this, ENToRO, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                Form1 frm = (Form1)ActiveMdiChild;
                string[] prelucrare = Regex.Split(frm.pseudocodeEd.Text, "\n");
                frm.pseudocodeEd.UndoRedo.BeginUndoAction();
                frm.pseudocodeEd.Text = "";
                for (int i = 0; i < prelucrare.Length; i++)
                {
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("integer")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "integer", "intreg");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("real")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "real", "real");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("character")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "character", "caracter");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("string")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "string", "sir");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("read")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "read", "citeste");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("write")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "write", "scrie");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("if") && prelucrare[i].TrimStart().TrimEnd().EndsWith("then")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "then", "atunci");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("if")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "if", "daca");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("else")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "else", "altfel");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("end_if")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "end_if", "sfarsit_daca");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("while") && prelucrare[i].TrimStart().TrimEnd().EndsWith("do")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "do", "executa");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("while")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "while", "cat_timp");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("end_while")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "end_while", "sfarsit_cat_timp");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("for") && prelucrare[i].TrimStart().TrimEnd().EndsWith("do")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "do", "executa");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("for")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "for", "pentru");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("end_for")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "end_for", "sfarsit_pentru");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("do")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "do", "executa");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("loop_until")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "loop_until", "pana_cand");
                    if (i == 0) frm.pseudocodeEd.Text += prelucrare[i];
                    else frm.pseudocodeEd.Text += "\n" + prelucrare[i];
                }
                
                frm.RedenumireVariabila("and", "si");
                frm.RedenumireVariabila("or", "sau");
                frm.RedenumireVariabila("different", "diferit");

                fromRomanianPseudoccodeToolStripMenuItem.PerformClick();
                    frm.Salvare();
                    frm.pseudocodeEd.UndoRedo.EndUndoAction();
            }
        }

        private void romanianToEnglishPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CasetaDeMesaj(this, ROToEN, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                Form1 frm = (Form1)ActiveMdiChild;
                string[] prelucrare = Regex.Split(frm.pseudocodeEd.Text, "\n");
                frm.pseudocodeEd.UndoRedo.BeginUndoAction();
                frm.pseudocodeEd.Text = "";
                for (int i = 0; i < prelucrare.Length; i++)
                {
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("intreg")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "intreg", "integer");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("real")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "real", "real");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("caracter")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "caracter", "character");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("sir")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "sir", "string");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("citeste")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "citeste", "read");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("scrie")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "scrie", "write");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("daca") && prelucrare[i].TrimStart().TrimEnd().EndsWith("atunci")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "atunci", "then");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("daca")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "daca", "if");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("altfel")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "altfel", "else");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("sfarsit_daca")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "sfarsit_daca", "end_if");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("cat_timp") && prelucrare[i].TrimStart().TrimEnd().EndsWith("executa")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "executa", "do");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("cat_timp")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "cat_timp", "while");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("sfarsit_cat_timp")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "sfarsit_cat_timp", "end_while");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("pentru") && prelucrare[i].TrimStart().TrimEnd().EndsWith("executa")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "executa", "do");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("pentru")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "pentru", "for");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("sfarsit_pentru")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "sfarsit_pentru", "end_for");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("executa")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "executa", "do");
                    if (prelucrare[i].TrimStart().TrimEnd().StartsWith("pana_cand")) prelucrare[i] = Regex.Replace(prelucrare[i].TrimStart().TrimEnd(), "pana_cand", "loop_until");
                    if (i == 0) frm.pseudocodeEd.Text += prelucrare[i];
                    else frm.pseudocodeEd.Text += "\n" + prelucrare[i];
                }
                frm.RedenumireVariabila("si", "and");
                frm.RedenumireVariabila("sau", "or");
                frm.RedenumireVariabila("diferit", "different");
              //  fromRomanianPseudoccodeToolStripMenuItem.Checked = true;
              //  fromEnglishCodeToolStripMenuItem.Checked = false;
               // if (fromRomanianPseudoccodeToolStripMenuItem.Checked)
              //  {

                fromEnglishCodeToolStripMenuItem.PerformClick();
                    frm.Salvare();
                    frm.pseudocodeEd.UndoRedo.EndUndoAction();
               // }
            }

        }

        private void signInNow_Tick(object sender, EventArgs e)
        {

        }

        private void analizeExecutedLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                if (frm.lista_executii != null && frm.lista_executii.Count != 0)
                {
                    if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.coverage")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.coverage");
                    StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.coverage");
                    int i = 1;
                    string[] str = Regex.Split(frm.pseudocodeEd.Text, "\n");
                    sw.WriteLine("{0} \t {1} \t {2}", "Nr. crt. ", "Nr. Executari".PadRight(22, ' '), "Linie de cod");
                    sw.WriteLine("{0} \t {1} \t {2}", "---------", "-------------".PadRight(22, '-'), "------------");
                    foreach (string st in str)
                    {
                        int a = frm.corespA[i - 1];
                        //this.Text = (a + 6).ToString();
                        string b = "";
                        if (frm.pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(frm.IfKeyword) ||
                            frm.pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(frm.ElseKeyword) ||
                            frm.pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(frm.ForKeyword) ||
                            frm.pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(frm.WhileKeyword) ||
                            frm.pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(frm.DoKeyword))
                            b = frm.lista_executii[a + (int)frm.nr_linii + 6][0];
                        else b = frm.lista_executii[a + (int)frm.nr_linii + 6 + 1][0];
                        if (b == "#####") b = "0";
                        if (b == "-") b = frm.NotInApp;

                        sw.WriteLine("{0} \t {1} \t {2}", i.ToString().PadRight(7, ' '), b.ToString().PadRight(22, ' '), frm.pseudocodeEd.Lines[i - 1].Text);
                        i++;
                    }
                    sw.Close();
                    try
                    {
                        Process.Start("notepad.exe", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.coverage");
                    }
                    catch
                    {

                    }
                }
                else
                {
                    CasetaDeMesaj(this, NoCoverageData, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return;
                if (frm.lista_executii != null && frm.lista_executii.Count != 0)
                {
                    DialogResult dr = CasetaDeMesaj(this, AnalizeCovergaeData, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dr == DialogResult.Yes)
                    {
                        if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp.gcov"))
                        {
                            Process.Start("notepad.exe", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp.gcov");
                        }
                        else CasetaDeMesaj(this, NoCoverageData, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                    CasetaDeMesaj(this, NoCoverageData, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void clearCoverageDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.lista_executii = null;
                if (Properties.Settings.Default.ShowOldVBScript == false) frm.CreateMap();
                CasetaDeMesaj(this, EraseCoverage, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void codeboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                Form1 frm = ActiveMdiChild as Form1;
                if (frm.pseudocodeEd.Text == "")
                {
                    frm.ShowCodeboard();
                }
                else
                {
                    newPseudocodeToolStripMenuItem.PerformClick();
                    Application.DoEvents();
                    Form1 frm2 = ActiveMdiChild as Form1;
                    frm2.ShowCodeboard();
                }
            }
        }

        private void userdefinedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fromEnglishCodeToolStripMenuItem.Checked = false;
            fromRomanianPseudoccodeToolStripMenuItem.Checked = false;
            userdefinedToolStripMenuItem.Checked = true;
            if (fromEnglishCodeToolStripMenuItem.Checked)
            {
                Form1 frm = (Form1)ActiveMdiChild;
                StreamReader rd = new StreamReader(Application.StartupPath + "\\custom-keywords.txt");
                string[] st = Regex.Split(rd.ReadToEnd(), "\r\n");
                rd.Close();
                frm.IntegerKeyword = st[0];
                frm.DoubleKeyword = st[1];
                frm.CharKeyword = st[2];
                frm.StringKeyword = st[3];
                frm.DivKeyword = st[4];
                frm.ModKeyword = st[5];
                frm.DifferentKeyword = st[6];
                frm.AndKeyword = st[7];
                frm.OrKeyword = st[8];
                frm.ReadKeyword = st[9];
                frm.WriteKeyword = st[10];
                frm.IfKeyword = st[11];
                frm.ThenKeyword = st[12];
                frm.ElseKeyword = st[13];
                frm.EndIfKeyword = st[14];
                frm.WhileKeyword = st[15];
                frm.EndWhileKeyword = st[16];
                frm.ForKeyword = st[17];
                frm.EndForKeyword = st[18];
                frm.DoKeyword = st[19];
                frm.LoopUntilKeyword = st[20];
                frm.InputText = st[21];
                frm.YES = st[22];
                frm.NO = st[23];
                frm.str[0] = frm.IntegerKeyword;
                frm.str[1] = frm.DoubleKeyword;
                frm.str[2] = frm.CharKeyword;
                frm.str[3] = frm.StringKeyword;
                frm.str[4] = frm.DivKeyword;
                frm.str[5] = frm.ModKeyword;
                frm.str[6] = frm.DifferentKeyword;
                frm.str[7] = frm.AndKeyword;
                frm.str[8] = frm.OrKeyword;
                frm.str[9] = frm.ReadKeyword;
                frm.str[10] = frm.WriteKeyword;
                frm.str[11] = frm.IfKeyword;
                frm.str[12] = frm.ThenKeyword;
                frm.str[13] = frm.ElseKeyword;
                frm.str[14] = frm.EndIfKeyword;
                frm.str[15] = frm.WhileKeyword;
                frm.str[16] = frm.EndWhileKeyword;
                frm.str[17] = frm.ForKeyword;
                frm.str[18] = frm.EndForKeyword;
                frm.str[19] = frm.DoKeyword;
                frm.str[20] = frm.LoopUntilKeyword;
                frm.pseudocodeEd.Lexing.Keywords[0] = frm.ReadKeyword + " " + frm.WriteKeyword + " " + frm.IfKeyword + " " + frm.ThenKeyword + " " + frm.ElseKeyword + " " + frm.EndIfKeyword + " " + frm.WhileKeyword + " " + frm.EndWhileKeyword + " " + frm.ForKeyword + " " + frm.EndForKeyword + " " + frm.DoKeyword + " " + frm.LoopUntilKeyword;
                frm.pseudocodeEd.Lexing.Keywords[1] = frm.IntegerKeyword + " " + frm.DoubleKeyword + " " + frm.CharKeyword + " " + frm.StringKeyword + " " + frm.DivKeyword + " " + frm.ModKeyword + " " + frm.DifferentKeyword + " " + frm.AndKeyword + " " + frm.OrKeyword;
                frm.VisualScrollBar.Lexing.Keywords[0] = frm.ReadKeyword + " " + frm.WriteKeyword + " " + frm.IfKeyword + " " + frm.ThenKeyword + " " + frm.ElseKeyword + " " + frm.EndIfKeyword + " " + frm.WhileKeyword + " " + frm.EndWhileKeyword + " " + frm.ForKeyword + " " + frm.EndForKeyword + " " + frm.DoKeyword + " " + frm.LoopUntilKeyword;
                frm.VisualScrollBar.Lexing.Keywords[1] = frm.IntegerKeyword + " " + frm.DoubleKeyword + " " + frm.CharKeyword + " " + frm.StringKeyword + " " + frm.DivKeyword + " " + frm.ModKeyword + " " + frm.DifferentKeyword + " " + frm.AndKeyword + " " + frm.OrKeyword;
                frm.PseudocodeLang = 2;
                frm.converterEd.Text = frm.Compile(frm.pseudocodeEd.Text, frm.lang);
                frm.flagReident = true;
                for (int i = 0; i <= frm.fileProperties.Count - 1; i++)
                {
                    if (frm.fileProperties[i].StartsWith("language"))
                    {
                        frm.fileProperties[i] = "language:2";
                    }
                }
            }

        }

        private void uploadProblemToInfoarenaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Form1)
            {
                if (Properties.Settings.Default.UILang == 0)
                {
                    CasetaDeMesaj(this, NotAvailableUILang, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Form1 frm = ActiveMdiChild as Form1;
                    string text = frm.Compile(frm.pseudocodeEd.Text, "CPP", true);
                    UploadToInfoarena ui = new UploadToInfoarena(this, text);
                    ui.ShowDialog();
                }
            }
        }

        private void ShowCodeboardTimer_Tick(object sender, EventArgs e)
        {
            ShowCodeboardTimer.Enabled = false;
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length <= 1)
            {
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat") && Properties.Settings.Default.WasWelcomeShown == false)
                {
                    if (Properties.Settings.Default.DisableEnhancedUI == false)
                    {
                        done = true;
                        glow.ContinueCode();
                        glow.Opacity = 1;
                        if (Properties.Settings.Default.DynamicColor == true) glow.wpfwindow.Foreground = border;
                        else                         glow.wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                        this.Opacity = 1;
                    }
                    MessageBoxI msg = new MessageBoxI(this, "", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    msg.Show();
                    msg.panel1.Visible = false;
                    Welcome wel = new Welcome(ActiveMdiChild as Form1, true, true);
                    wel.ShowDialog();
                    msg.Close();
                    //SetForegroundWindow(this.Handle);
                }
                if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat") && Properties.Settings.Default.WasWelcomeShown == false)
                {
                    if (Properties.Settings.Default.DisableEnhancedUI == false)
                    {
                        done = true;
                        glow.ContinueCode();
                        glow.Opacity = 1;
                        if (Properties.Settings.Default.DynamicColor == true) glow.wpfwindow.Foreground = border;
                        else                         glow.wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                        this.Opacity = 1;
                    }
                    MessageBoxI msg = new MessageBoxI(this, "", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    msg.Show();
                    msg.panel1.Visible = false;
                    Welcome wel = new Welcome(ActiveMdiChild as Form1, false, true);
                    wel.ShowDialog();
                    msg.Close();
                    //SetForegroundWindow(this.Handle);
                }
            }
        }

        public void ResizeErrorList(int how, IntPtr handle)
        {
            if (how != 0)
            {
                foreach (Form frmx in MdiChildren)
                {
                    try
                    {
                        Form1 frm = (Form1)frmx;

                        if (handle != frm.Handle)
                        {
                            frm.splitContainer3.SplitterMoved -= frm.splitContainer3_SplitterMoved;
                            frm.splitContainer3.SplitterDistance = frm.splitContainer3.Height - how - 4;
                            frm.splitContainer3.SplitterMoved += frm.splitContainer3_SplitterMoved;
                        }
                    }
                    catch { }
                }
                Properties.Settings.Default.SplitterDistance = how;
                Properties.Settings.Default.Save();
            }
        }

        private void tESTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IsMaximised = false;
            OnWindowStateChanged(e);
            
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Restore_Click(object sender, EventArgs e)
        {
          /*  this.ResizeBegin -= MainForm_ResizeBegin;
            this.Resize -= MainForm_Resize;
            this.ResizeEnd -= MainForm_ResizeEnd;
            NormalCode(e);
            this.ResizeBegin += MainForm_ResizeBegin;
            this.Resize += MainForm_Resize;
            this.ResizeEnd += MainForm_ResizeEnd;
            IsMaximised = false;*/
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
            }
            try
            {
                Form1 frm = ActiveMdiChild as Form1;
                frm.pseudocodeEd.Focus();
            }
            catch { }
         //   glow.CallSize(e);
        }

        private void Minimize_Click(object sender, EventArgs e)
        {
            nuIntra = true;
           // this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.WindowState = FormWindowState.Minimized;
            //this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            nuIntra = false;
        }

        private void Close_MouseEnter(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Close.BackgroundImage = CloseBlackHover.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Close.BackgroundImage = CloseLightHover.BackgroundImage;

        }

        private void Close_MouseDown(object sender, MouseEventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Close.BackgroundImage = CloseBlackPressed.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Close.BackgroundImage = CloseLightPressed.BackgroundImage;
            if (e.Button == System.Windows.Forms.MouseButtons.Right && e.Clicks == 1)
            {
                systemMenuPos = PointToClient(Cursor.Position);
                showSystemMenuTimer.Interval = 1;
                showSystemMenuTimer.Enabled = true;
            }
        }

        private void Close_MouseLeave(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Close.BackgroundImage = CloseBlack.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Close.BackgroundImage = CloseLight.BackgroundImage;

        }

        private void Restore_MouseEnter(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = RestoreBlackHover.BackgroundImage;
                if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = RestoreLightHover.BackgroundImage;
            }
            if (this.WindowState == FormWindowState.Normal)
            {
                if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = MaximizeBlackHover.BackgroundImage;
                if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = MaximizeLightHover.BackgroundImage;
            }
        }

        private void Restore_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = RestoreBlackPressed.BackgroundImage;
                if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = RestoreLightPressed.BackgroundImage;
            }
            if (this.WindowState == FormWindowState.Normal)
            {
                if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = MaximizeBlackPressed.BackgroundImage;
                if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = MaximizeLightPressed.BackgroundImage;
            }
            if (e.Button == System.Windows.Forms.MouseButtons.Right && e.Clicks == 1)
            {
                systemMenuPos = PointToClient(Cursor.Position);
                showSystemMenuTimer.Interval = 1;
                showSystemMenuTimer.Enabled = true;
            }
        }

        private void Restore_MouseLeave(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = RestoreBlack.BackgroundImage;
                if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = RestoreLight.BackgroundImage;
            }
            if (this.WindowState == FormWindowState.Normal)
            {
                if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = MaximizeBlack.BackgroundImage;
                if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = MaximizeLight.BackgroundImage;
            }

        }

        private void Minimize_MouseEnter(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Minimize.BackgroundImage = MinimizeBlackHover.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Minimize.BackgroundImage = MinimizeLightHover.BackgroundImage;
        }

        private void Minimize_MouseDown(object sender, MouseEventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Minimize.BackgroundImage = MinimizeBlackPressed.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Minimize.BackgroundImage = MinimizeLightPressed.BackgroundImage;
            if (e.Button == System.Windows.Forms.MouseButtons.Right && e.Clicks == 1)
            {
                systemMenuPos = PointToClient(Cursor.Position);
                showSystemMenuTimer.Interval = 1;
                showSystemMenuTimer.Enabled = true;
            }
        }

        private void Minimize_MouseLeave(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Minimize.BackgroundImage = MinimizeBlack.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Minimize.BackgroundImage = MinimizeLight.BackgroundImage;
        }
        bool nuAcum = false;
        private void MainForm_Activated(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal && nuAcum == false)
            {
                
                try
                {
                    nuAcum = true;
                    //wpfwindow.Visibility = System.Windows.Visibility.Visible;
                    SetForegroundWindow(wpfwindow.GetHandle());
                    this.TopMost = true;
                    if (Properties.Settings.Default.DynamicColor == true) wpfwindow.Foreground = border;
                    else                     wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                    unTop.Enabled = true;

                }
                catch { }
            }
          /*  if (this.WindowState == FormWindowState.Minimized && IsMaximised)
            {
                nuIntra = true;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                nuIntra = false;
            }*/
        }
        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("user32.dll")]
        public static extern int TrackPopupMenu(int hMenu, int wFlags, int x, int y, int nReserved, int hwnd, ref RECT lprc);
        [DllImport("user32.dll")]
        static extern uint TrackPopupMenuEx(IntPtr hmenu, uint fuFlags, int x, int y,
           IntPtr hwnd, IntPtr lptpm);
        const long TPMLEFTBUTTON = 0x0000L;
        const long TPMRETURNCMD = 0x0100L;
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }
        private const int WM_SYSCOMMAND = 0x0112;
        private const int TPM_RIGHTBUTTON = 0x0002;
        private const int TPM_RETURNCMD = 0x0100;
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        //Declare the variables
        bool drag;
        int mousex;
        int mousey;
        private void menuStrip1_MouseDown(object sender, MouseEventArgs e)
        {
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                if (e.Clicks == 2 && e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    /*SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
                    SendMessage(Handle, 0x00A2, HT_CAPTION, 0);
                    SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
                    SendMessage(Handle, 0x00A2, HT_CAPTION, 0);*/

                    Restore.PerformClick();
                    return;
                }
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    drag = true;
                    //Sets the variable drag to true.
                    //mousex = System.Windows.Forms.Cursor.Position.X - this.Location.X;
                    //Sets variable mousex
                    //mousey = System.Windows.Forms.Cursor.Position.Y - this.Location.Y;
                    //Sets variable mousey

                }
                if (e.Button == System.Windows.Forms.MouseButtons.Right && e.Clicks == 1)
                {
                    systemMenuPos = PointToClient(Cursor.Position);
                    showSystemMenuTimer.Interval = 1;
                    showSystemMenuTimer.Enabled = true;
                }
            }
        }

        private void menuStrip1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                //If drag is set to true then move the form accordingly.
                if (drag)
                {
                   // Cursor.Position = new Point(Cursor.Position.X, Cursor.Position.Y + 15);
                    //Restore.PerformClick();
                    ReleaseCapture();
                    if (Properties.Settings.Default.DisableEnhancedUI == false)
                    {
                        SendMessage(glow.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
                    }
                    else
                    {
                        SendMessage(this.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
                    }
                    //this.Location = new Point(System.Windows.Forms.Cursor.Position.Y - mousey, System.Windows.Forms.Cursor.Position.X - mousex);
                }
            }
        }

        private void menuStrip1_MouseUp(object sender, MouseEventArgs e)
        {
            //drag = false;
        }
        public void ShowSystemMenu(Point x)
        {
            // need to get handle of window


            //translate mouse cursor porition to screen coordinates
            Point p = PointToScreen(x);

            IntPtr callingTaskBarWindow = this.Handle;
            IntPtr wMenu = GetSystemMenu(this.Handle, false);
            // Display the menu
            uint command = TrackPopupMenuEx(wMenu,
                (uint)(TPMLEFTBUTTON | TPMRETURNCMD), (int)p.X, (int)p.Y, callingTaskBarWindow, IntPtr.Zero);
            if (command == 0)
                return;

            PostMessage(this.Handle, WM_SYSCOMMAND, new IntPtr(command), IntPtr.Zero);
        }
        private void menuIcon_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks == 1)
            {
                ShowSystemMenu(new Point(5, menuIcon.Height + 5));
            }
            if (e.Clicks == 2 && e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Close();
            }
        }

        private void MainForm_TextChanged(object sender, EventArgs e)
        {
           // glow.Title.Text = glow.Location.ToString();
           // return;
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                using (Graphics g = this.CreateGraphics())
                {
                    if (g.MeasureString(this.Text, Title.Font).Width + 2 > Title.Width)
                    {
                        string a = this.Text;
                        while (g.MeasureString(a, Title.Font).Width + 2 > Title.Width) a = a.Substring(0, a.Length - 1);
                        if (a != "") Title.Text = a + "...";
                        else Title.Text = "";
                    }
                    else Title.Text = this.Text;
                }
            }
            if (Properties.Settings.Default.DisableEnhancedUI == false)
            {
                using (Graphics g = this.CreateGraphics())
                {
                    if (g.MeasureString(this.Text, glow.Title.Font).Width + 2 > glow.Title.Width)
                    {
                        string a = this.Text;
                        while (g.MeasureString(a, glow.Title.Font).Width + 2 > glow.Title.Width) a = a.Substring(0, a.Length - 1);
                        glow.Title.Text = a + "...";
                    }
                    else glow.Title.Text = this.Text;
                }
            }
        }

        private void MainForm_LocationChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.WindowState == FormWindowState.Normal && !IsMaximised && nuIntra == false)
                {
                    wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
                    oldLoc = this.Location;
                }
            }
            catch { }
        }

        private void SearchBox_MouseEnter(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) SearchBox.ForeColor = Color.White;
        }

        private void SearchBox_MouseLeave(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0 && !SearchBox.Focused) SearchBox.ForeColor = Color.Silver;
        }

        public void MainForm_Resize(object sender, EventArgs e)
        {

          //  if (resizeWorker.IsBusy == false) resizeWorker.RunWorkerAsync();
        }
        private void resizeWorker_DoWork(object sender, DoWorkEventArgs e)
        {
           // if (Properties.Settings.Default.DisableEnhancedUI == false)
          //  {
                if (!IsMaximised)
                {
                    DoOnUIThread(delegate()
                    {
                    wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
                    });
                    System.Threading.Thread.Sleep(500);
                   // SetControlPropertyThreadSafe(wpfwindow, "Visibility", new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0)));
                    //SetControlPropertyThreadSafe(glow, "Size", this.Size);
                    //System.Threading.Thread.Sleep(10);
                    /*DoOnUIThread(delegate()
                    {
                        glow.Location = new Point(this.Location.X, this.Location.Y - Close.Height - 5);
                        glow.Size = this.Size;
                    });*/
                }
            //}
        }
        public delegate void SetControlPropertyThreadSafeDelegate(Control control, string propertyName, object propertyValue);

        public static void SetControlPropertyThreadSafe(Control control, string propertyName, object propertyValue)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new SetControlPropertyThreadSafeDelegate(SetControlPropertyThreadSafe), new object[] { control, propertyName, propertyValue });
            }
            else
            {
                control.GetType().InvokeMember(propertyName, BindingFlags.SetProperty, null, control, new object[] { propertyValue });
            }
        }
        public void MainForm_ResizeBegin(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                BorderLoop.Enabled = false;
                wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
            }
            if (Properties.Settings.Default.DisableEnhancedUI == false)
            {
                if (!IsMaximised)
                {
                    glow.nuLua = true;
                    /*  this.Resize -= this.MainForm_Resize;
                      this.ResizeBegin -= this.MainForm_ResizeBegin;
                      this.ResizeEnd -= this.MainForm_ResizeEnd;*/
                    this.Size = glow.Size;
                    /* this.Resize += this.MainForm_Resize;
                     this.ResizeBegin += this.MainForm_ResizeBegin;
                     this.ResizeEnd += this.MainForm_ResizeEnd;*/
                    //      glow.TopMost = true;
                    //      this.TopMost = true;
                    glow.doNotAlterLocation = true;

                    glow.HideGlow();
                }
            }
        }
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        public void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.EnableNewUI == true)
            {
                wpfwindow.Left = this.Location.X - 10;
                wpfwindow.Top = this.Location.Y - 10;
                wpfwindow.Width = this.Width + 20;
                wpfwindow.Height = this.Height + 20;
                if (Properties.Settings.Default.DynamicColor == true) wpfwindow.Foreground = border;
                else                 wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                BorderLoop.Enabled = true;
            }
            if (Properties.Settings.Default.DisableEnhancedUI == false)
            {
                if (!IsMaximised)
                {
                    glow.Location = new Point(this.Location.X, this.Location.Y - Close.Height - 5);
                    glow.Size = this.Size;
                    glow.nuLua = false;
                    glow.doNotAlterLocation = false;
                    if (this.WindowState == FormWindowState.Normal && nuIntra == false)
                        glow.ShowGlow();
                    if (this.WindowState == FormWindowState.Normal && !IsMaximised && nuIntra == false)
                    {
                        oldSize = this.Size;
                        oldLoc = this.Location;
                    }
                    //      glow.TopMost = false;
                    //      this.TopMost = false;
                }
            }
        }

        private void SearchBox_VisibleChanged(object sender, EventArgs e)
        {
            if (SearchBox.Visible) UserPanel.Left = SearchBox.Left - UserPanel.Width - 3;
            else UserPanel.Left = this.Width - UserPanel.Width - 8;


        }

        private void UserDropPanel_VisibleChanged(object sender, EventArgs e)
        {
            if (SearchBox.Visible) UserDropPanel.Left = SearchBox.Left - UserDropPanel.Width - 3;
            else UserDropPanel.Left = this.Width - UserDropPanel.Width - 8;
        }

        private void showSystemMenuTimer_Tick(object sender, EventArgs e)
        {
            ShowSystemMenu(systemMenuPos);
            showSystemMenuTimer.Enabled = false;
        }

        private void BorderLoop_Tick(object sender, EventArgs e)
        {
            try
            {
                if (Properties.Settings.Default.EnableNewUI == true)
                {
                    if (this.WindowState == FormWindowState.Normal && wpfwindow.Visibility == System.Windows.Visibility.Visible && (wpfwindow.Height != this.Height + 20 || wpfwindow.Width != this.Width + 20))
                    {
                        wpfwindow.Left = this.Location.X - 10;
                        wpfwindow.Top = this.Location.Y - 10;
                        wpfwindow.Width = this.Width + 20;
                        wpfwindow.Height = this.Height + 20;
                        if (Properties.Settings.Default.DynamicColor == true) wpfwindow.Foreground = border;
                        else wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
                    }
                }
                UpdateUXLines();

            }
            catch { }

        }

        private void WrapUp_Tick(object sender, EventArgs e)
        {
            string Content = "";
            Content += Properties.Settings.Default.Position.ToString() + "\r\n";
            Content += Properties.Settings.Default.Language.ToString() + "\r\n";
            Content += Properties.Settings.Default.AutoSaveInterval.ToString() + "\r\n";
            Content += Properties.Settings.Default.Zoom.ToString() + "\r\n";
            Content += Properties.Settings.Default.WasWelcomeShown.ToString() + "\r\n";
            Content += Properties.Settings.Default.DownloadImages.ToString() + "\r\n";
            Content += Properties.Settings.Default.UseBar.ToString() + "\r\n";
            Content += Properties.Settings.Default.ShowStatusBar.ToString() + "\r\n";
            Content += Properties.Settings.Default.ShowAdvancedPanel.ToString() + "\r\n";
            Content += Properties.Settings.Default.ShowCommandBar.ToString() + "\r\n";
            Content += Properties.Settings.Default.ShowConvertedCode.ToString() + "\r\n";
            Content += Properties.Settings.Default.ShowVisualScrollBar.ToString() + "\r\n";
            Content += Properties.Settings.Default.AlwaysLoadProperties.ToString() + "\r\n";
            Content += Properties.Settings.Default.ShowQuickSearch.ToString() + "\r\n";
            Content += Properties.Settings.Default.PopulateFind.ToString() + "\r\n";
            Content += Properties.Settings.Default.SplitterDistance.ToString() + "\r\n";
            Content += Properties.Settings.Default.UITheme.ToString() + "\r\n";
            Content += Properties.Settings.Default.DynamicColor.ToString() + "\r\n";
            //Content += Properties.Settings.Default.PseudocodeLang.ToString() + "\r\n";
            try
            {
                System.IO.File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.config", Content);
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
                {
                    var dropBoxStorage = new CloudStorage();
                    var dropBoxConfig = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox);
                    ICloudStorageAccessToken accessToken;
                    StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                    string t = sr.ReadToEnd();
                    sr.Close();
                    string a = EncryptDecrypt.DecryptString(t);
                    StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                    sw.Write(a);
                    sw.Close();
                    //MessageBox.Show(EncryptDecrypt.DecryptString(EncryptDecrypt.DecryptString(t)));
                    using (var fs = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat", FileMode.Open, FileAccess.Read, FileShare.None))
                    {
                        accessToken = dropBoxStorage.DeserializeSecurityToken(fs);
                    }
                    StreamWriter sww = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                    sww.Write(t);
                    sww.Close();
                    var storageToken = dropBoxStorage.Open(dropBoxConfig, accessToken);
                    //pictureBox2.Image.Dispose();
                    //File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic");
                    dropBoxStorage.UploadFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.config", "/");
                }
            }
            catch
            {
                //Application.RemoveMessageFilter(this);
                //this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                Process.GetCurrentProcess().Kill();
            }
            //Application.RemoveMessageFilter(this);
            //this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            Process.GetCurrentProcess().Kill();
        }

        private void MainForm_Move(object sender, EventArgs e)
        {

        }
        public void ShowBrowser(string Url)
        {
            Browser br = new Browser(this, AppName, Url);
            br.Show(dockPanel, DockState.Document);
            UpdateMenus();
        }
        System.Windows.Media.SolidColorBrush border;
        private void colorTimer_Tick(object sender, EventArgs e)
        {
            DateTime beg = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime now = DateTime.Now;
            TimeSpan ts = now - beg;
            int colorT = Convert.ToInt32(ts.TotalSeconds);
            if (colorT % 240 == 0)
            {
                UpdateBorder(colorT);
            }
        }
        public void UpdateBorder(int decValue)
        {
            /*DateTime beg = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime now = DateTime.Now;
            TimeSpan ts = now - beg;
            int decValue = Convert.ToInt32(ts.TotalSeconds);*/
            if (wpfwindow != null)
            {
                int cat = decValue / 240;//87600;
                border = HSVtoRGB(360 - cat, 100, 100);
                wpfwindow.Foreground = border;
            }
            /*string hexValue = decValue.ToString("X");
            int decAgain = int.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
            Color cl = System.Drawing.Color.FromArgb(decAgain);
            border = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, cl.R, cl.G, cl.B));
            wpfwindow.Foreground = border;*/

        }

        private void newBrowserWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newPseudocodeToolStripMenuItem.PerformClick();
        }

        private void newBrowserWindowToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ShowBrowser(Properties.Settings.Default.HomePage);
        }

        private void duplicateBrowserWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                ShowBrowser(b.webBrowser1.Url.ToString());
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openPseudocodeToolStripMenuItem.PerformClick();
        }

        private void savePageAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.ShowSaveAsDialog();
            }
        }

        private void closePageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.Close();
            }
        }

        private void pageSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.ShowPageSetupDialog();
            }
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.ShowPrintDialog();
            }
        }

        private void printPreviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.ShowPrintPreviewDialog();
            }
        }

        private void quitPseudocodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exitPseudocodeToolStripMenuItem.PerformClick();
        }

        private void cutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.Document.ExecCommand("Cut", false, null);
            }
        }

        private void copyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.Document.ExecCommand("Copy", false, null);
            }
        }

        private void pasteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.Document.ExecCommand("Paste", false, null);
            }
        }

        private void selectAllToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.Document.ExecCommand("SelectAll", false, null);
            }
        }

        private void findonThisPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.Select();
                SendKeys.Send("^f");
            }
        }

        private void navigateBackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.GoBack();
            }
        }

        private void navigateForwardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.GoForward();
            }
        }

        private void openHomePageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.Navigate("http://www.valinet.ro");
            }
        }

        private void stopLoadingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.Stop();
            }
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                b.webBrowser1.Refresh();
            }
        }

        private void sourceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                String source = (Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\source.txt");
                System.IO.StreamWriter writer = System.IO.File.CreateText(source);
                writer.Write(b.webBrowser1.DocumentText);
                writer.Close();
                System.Diagnostics.Process.Start("notepad.exe", source);
            }
        }

        private void configurationToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            configurationToolStripMenuItem.PerformClick();
        }

        private void menuSearchToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            menuSearchToolStripMenuItem.PerformClick();
        }

        private void packageManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            extensionsManagerToolStripMenuItem.PerformClick();
        }

        private void helpSupportAndTipsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser b = ActiveMdiChild as Browser;
                string lang2 = "ro";
                if (Properties.Settings.Default.UILang == 0) lang2 = "en";
                if (Properties.Settings.Default.UILang == 1) lang2 = "ro";
                b.webBrowser1.Navigate(Application.StartupPath + "\\com.valinet.pseudocode.help\\" + lang2 + "\\Default\\webframe.html");
            }
        }

        private void feedbackToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            feedbackToolStripMenuItem.PerformClick();
        }

        private void checkForUpdatesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            checkForUpdatesToolStripMenuItem.PerformClick();
        }

        private void licenseAgreementToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            licenseAgreementToolStripMenuItem.PerformClick();
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            aboutToolStripMenuItem.PerformClick();
        }

        private void toolStripButton30_Click(object sender, EventArgs e)
        {
            
        }

        private void zoomInToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                if (zoomLevel <= 0)
                {
                    if (zoomLevel > -50 && zoomLevel <= 0) zoomLevel = 1;
                    if (zoomLevel > -75 && zoomLevel <= -50) zoomLevel = 2;
                    if (zoomLevel > -100 && zoomLevel <= -75) zoomLevel = 3;
                    if (zoomLevel > -125 && zoomLevel <= -100) zoomLevel = 4;
                    if (zoomLevel > -150 && zoomLevel <= -125) zoomLevel = 5;
                    if (zoomLevel > -175 && zoomLevel <= -150) zoomLevel = 6;
                    if (zoomLevel > -200 && zoomLevel <= -170) zoomLevel = 7;
                    if (zoomLevel > -250 && zoomLevel <= -200) zoomLevel = 8;
                    if (zoomLevel > -300 && zoomLevel <= -250) zoomLevel = 9;
                    if (zoomLevel > -400 && zoomLevel <= -300) zoomLevel = 10;
                    if (zoomLevel <= -400) zoomLevel = 11;
                }
                zoomLevel += 1;
                if (zoomLevel > 10) zoomLevel = 10;
                Browser br = (Browser)ActiveMdiChild;
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
                switch (zoomLevel)
                {
                    case 10: br.webBrowser1.Zoom(400); toolStripMenuItem44.Checked = true; break;
                    case 9: br.webBrowser1.Zoom(300); toolStripMenuItem45.Checked = true; break;
                    case 8: br.webBrowser1.Zoom(250); toolStripMenuItem46.Checked = true; break;
                    case 7: br.webBrowser1.Zoom(200); toolStripMenuItem47.Checked = true; break;
                    case 6: br.webBrowser1.Zoom(175); toolStripMenuItem48.Checked = true; break;
                    case 5: br.webBrowser1.Zoom(150); toolStripMenuItem49.Checked = true; break;
                    case 4: br.webBrowser1.Zoom(125); toolStripMenuItem50.Checked = true; break;
                    case 3: br.webBrowser1.Zoom(100); toolStripMenuItem51.Checked = true; break;
                    case 2: br.webBrowser1.Zoom(75); toolStripMenuItem52.Checked = true; break;
                    case 1: br.webBrowser1.Zoom(50); toolStripMenuItem53.Checked = true; break;
                }
            }
        }

        private void zoomOutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                if (zoomLevel <= 0)
                {
                    if (zoomLevel > -50 && zoomLevel <= 0) zoomLevel = 1;
                    if (zoomLevel > -75 && zoomLevel <= -50) zoomLevel = 2;
                    if (zoomLevel > -100 && zoomLevel <= -75) zoomLevel = 3;
                    if (zoomLevel > -125 && zoomLevel <= -100) zoomLevel = 4;
                    if (zoomLevel > -150 && zoomLevel <= -125) zoomLevel = 5;
                    if (zoomLevel > -175 && zoomLevel <= -150) zoomLevel = 6;
                    if (zoomLevel > -200 && zoomLevel <= -170) zoomLevel = 7;
                    if (zoomLevel > -250 && zoomLevel <= -200) zoomLevel = 8;
                    if (zoomLevel > -300 && zoomLevel <= -250) zoomLevel = 9;
                    if (zoomLevel > -400 && zoomLevel <= -300) zoomLevel = 10;
                    if (zoomLevel <= -400) zoomLevel = 11;
                }
                zoomLevel -= 1;
                if (zoomLevel < 1) zoomLevel = 1;
                Browser br = (Browser)ActiveMdiChild;
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
                switch (zoomLevel)
                {
                    case 10: br.webBrowser1.Zoom(400); toolStripMenuItem44.Checked = true; break;
                    case 9: br.webBrowser1.Zoom(300); toolStripMenuItem45.Checked = true; break;
                    case 8: br.webBrowser1.Zoom(250); toolStripMenuItem46.Checked = true; break;
                    case 7: br.webBrowser1.Zoom(200); toolStripMenuItem47.Checked = true; break;
                    case 6: br.webBrowser1.Zoom(175); toolStripMenuItem48.Checked = true; break;
                    case 5: br.webBrowser1.Zoom(150); toolStripMenuItem49.Checked = true; break;
                    case 4: br.webBrowser1.Zoom(125); toolStripMenuItem50.Checked = true; break;
                    case 3: br.webBrowser1.Zoom(100); toolStripMenuItem51.Checked = true; break;
                    case 2: br.webBrowser1.Zoom(75); toolStripMenuItem52.Checked = true; break;
                    case 1: br.webBrowser1.Zoom(50); toolStripMenuItem53.Checked = true; break;
                }
            }
        }

        private void toolStripMenuItem44_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                zoomLevel = 10;
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Zoom(400);
                toolStripMenuItem44.Checked = true;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
            }
        }

        private void toolStripMenuItem45_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                zoomLevel = 9;
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Zoom(300);
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = true;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
            }
        }

        private void toolStripMenuItem46_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                zoomLevel = 8;
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Zoom(250);
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = true;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
            }
        }

        private void toolStripMenuItem47_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                zoomLevel = 7;
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Zoom(200);
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = true;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
            }
        }

        private void toolStripMenuItem48_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                zoomLevel = 6;
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Zoom(175);
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = true;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
            }
        }

        private void toolStripMenuItem49_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                zoomLevel = 5;
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Zoom(150);
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = true;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
            }
        }

        private void toolStripMenuItem50_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                zoomLevel = 4;
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Zoom(125);
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = true;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
            }
        }

        private void toolStripMenuItem51_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                zoomLevel = 3;
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Zoom(100);
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = true;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
            }
        }

        private void toolStripMenuItem52_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                zoomLevel = 2;
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Zoom(75);
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = true;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
            }
        }

        private void toolStripMenuItem53_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                zoomLevel = 1;
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Zoom(50);
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = true;
                customizeToolStripMenuItem1.Checked = false;
            }
        }

        private void customizeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                ZoomWB zoom = new ZoomWB(this);
                zoom.ShowDialog();
                Browser br = (Browser)ActiveMdiChild;
                toolStripMenuItem44.Checked = false;
                toolStripMenuItem45.Checked = false;
                toolStripMenuItem46.Checked = false;
                toolStripMenuItem47.Checked = false;
                toolStripMenuItem48.Checked = false;
                toolStripMenuItem49.Checked = false;
                toolStripMenuItem50.Checked = false;
                toolStripMenuItem51.Checked = false;
                toolStripMenuItem52.Checked = false;
                toolStripMenuItem53.Checked = false;
                customizeToolStripMenuItem1.Checked = false;
                if (zoomLevel <= 0)
                {
                    br.webBrowser1.Zoom(0 - zoomLevel); customizeToolStripMenuItem1.Checked = true; 
                }
                else switch (zoomLevel)
                {
                    case 10: br.webBrowser1.Zoom(400); toolStripMenuItem44.Checked = true; break;
                    case 9: br.webBrowser1.Zoom(300); toolStripMenuItem45.Checked = true; break;
                    case 8: br.webBrowser1.Zoom(250); toolStripMenuItem46.Checked = true; break;
                    case 7: br.webBrowser1.Zoom(200); toolStripMenuItem47.Checked = true; break;
                    case 6: br.webBrowser1.Zoom(175); toolStripMenuItem48.Checked = true; break;
                    case 5: br.webBrowser1.Zoom(150); toolStripMenuItem49.Checked = true; break;
                    case 4: br.webBrowser1.Zoom(125); toolStripMenuItem50.Checked = true; break;
                    case 3: br.webBrowser1.Zoom(100); toolStripMenuItem51.Checked = true; break;
                    case 2: br.webBrowser1.Zoom(75); toolStripMenuItem52.Checked = true; break;
                    case 1: br.webBrowser1.Zoom(50); toolStripMenuItem53.Checked = true; break;
                }
            }
        }

        private void smallestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.ZoomText(0);
            }
        }

        private void smallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.ZoomText(1);
            }
        }

        private void defaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.ZoomText(2);
            }
        }

        private void largeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.ZoomText(3);
            }
        }

        private void largestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.ZoomText(4);
            }
        }

        private void toolStripButton30_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (ActiveMdiChild is Browser)
                {
                    Browser br = (Browser)ActiveMdiChild;
                    br.webBrowser1.Navigate(toolStripButton30.Text);
                    e.Handled = true;
                }
            }
        }

        private void toolStripButton51_Click(object sender, EventArgs e)
        {
            navigateBackToolStripMenuItem.PerformClick();
        }

        private void toolStripButton52_Click(object sender, EventArgs e)
        {
            navigateForwardToolStripMenuItem.PerformClick();
        }

        private void sendPageByEmailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.Navigate("mailto:?Subject=" + br.webBrowser1.DocumentTitle + "&Body=" + br.webBrowser1.Url.ToString());
            }
        }

        private void createShortcutToDesktopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser br = (Browser)ActiveMdiChild;
                br.webBrowser1.ShowPropertiesDialog();
            }
        }

        private void browserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowBrowser(Properties.Settings.Default.HomePage);
        }

        private void toolStripButton30_Enter(object sender, EventArgs e)
        {
            toolStripButton30.SelectAll();
        }

        private void valiNetWebSiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowBrowser("http://www.valinet.ro");
        }

        private void addToFavoritesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild is Browser)
            {
                Browser br = (Browser)ActiveMdiChild;
                string name = Microsoft.VisualBasic.Interaction.InputBox(addToFavorites, AppName, br.webBrowser1.DocumentTitle);
                if (name == "")
                {
                    return;
                }
                foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                {
                    if (name.Contains(c))
                    {
                        CasetaDeMesaj(this, invalidName, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                foreach (char d in name.ToCharArray())
                {
                    if (d != '.') goto here;
                }
                CasetaDeMesaj(this, invalidName, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
                here:
                if (name == "CON" || name == "PRN" || name == "AUX" || name == "CLOCK$" || name == "NUL" || name == "COM0" || name == "COM1" || name == "COM2" || name == "COM3" || name == "COM4" || name == "COM5" || name == "COM6" || name == "COM7" || name == "COM8" || name == "COM9" || name == "LPT0" || name == "LPT1" || name == "LPT2" || name == "LPT3" || name == "LPT4" || name == "LPT5" || name == "LPT6" || name == "LPT7" || name == "LPT8" || name == "LPT9")
                {
                    CasetaDeMesaj(this, invalidName, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode\\" + name + ".url");
                sw.Write("[DEFAULT]\r\nBASEURL = " + br.webBrowser1.Url.ToString() + "\r\n[InternetShortcut]\r\nURL = " + br.webBrowser1.Url.ToString() + "r\nIDList =\r\n[{ 000214A0 - 0000 - 0000 - C000 - 000000000046}]\r\nProp3 = 19,2");
                sw.Close();
                ToolStripMenuItem mf = new ToolStripMenuItem();
                mf.Text = name; //.FullName;
                mf.Tag = Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode\\" + name + ".url";
                if (Properties.Settings.Default.UITheme == 1) mf.ForeColor = Color.Black;
                else mf.ForeColor = Color.White;
                fAVORITESToolStripMenuItem.DropDownItems.Add(mf);
                mf.Click += Mf_Click;
            }
        }

        private void organizeFavoritesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OrganizeFavorites og = new OrganizeFavorites(this, AppName);
            og.ShowDialog();
            try
            {
                for (int i = 4; i <= 10000; i++)
                {
                    fAVORITESToolStripMenuItem.DropDownItems.RemoveAt(i);
                }
            }
            catch { }
            AddFavorites();
        }

        private void toolStripButton30_Paint(object sender, PaintEventArgs e)
        {

        }

        private void runUsingLocalDebuggerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ToolStripItem a in toolStripButton32.DropDownItems)
            {
                ToolStripMenuItem b = a as ToolStripMenuItem;
                if (b != null) b.Checked = false;
            }
            runUsingBuiltinBrowserToolStripMenuItem.Checked = false;
            runUsingLocalDebuggerToolStripMenuItem.Checked = true;
            foreach (Form frmx in MdiChildren)
                if (frmx is Form1)
                {
                    Form1 frm = frmx as Form1;
                frm.compileMode = "1";
            }
            Properties.Settings.Default.DefaultDebugMode = 1;
            Properties.Settings.Default.Save();
            toolStripButton32.Text = runText + " (Local Debugger)";
        }

        private void runUsingBuiltinBrowserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ToolStripItem a in toolStripButton32.DropDownItems)
            {
                ToolStripMenuItem b = a as ToolStripMenuItem;
                if (b != null) b.Checked = false;
            }
            runUsingBuiltinBrowserToolStripMenuItem.Checked = true;
            runUsingLocalDebuggerToolStripMenuItem.Checked = false;
            foreach (Form frmx in MdiChildren)
                if (frmx is Form1)
                {
                    Form1 frm = frmx as Form1;
                frm.compileMode = "2";
            }
            Properties.Settings.Default.DefaultDebugMode = 2;
            Properties.Settings.Default.Save();
            toolStripButton32.Text = runText + " (Built-in Web Browser)";
        }
        private void Mf_Click1(object sender, EventArgs e)
        {
            foreach (ToolStripItem a in toolStripButton32.DropDownItems)
            {
                ToolStripMenuItem b = a as ToolStripMenuItem;
                if (b != null) b.Checked = false;
            }
            runUsingBuiltinBrowserToolStripMenuItem.Checked = false;
            runUsingLocalDebuggerToolStripMenuItem.Checked = false;
            ToolStripMenuItem ts = sender as ToolStripMenuItem;
            ts.Checked = true;
            foreach (Form frmx in MdiChildren)
                if (frmx is Form1)
                {
                    Form1 frm = frmx as Form1;
                    frm.compileMode = (Convert.ToInt32(ts.Tag) + 3).ToString();
                }
            Properties.Settings.Default.DefaultDebugMode = (Convert.ToInt32(ts.Tag) + 3);
            Properties.Settings.Default.Save();
            toolStripButton32.Text = runText + " (" + ts.Text +")";
        }
        private void manageCustomCompilersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomCompilers c = new CustomCompilers(this, AppName);
            c.ShowDialog();
            while (toolStripButton32.DropDownItems.Count >= 6) toolStripButton32.DropDownItems.RemoveAt(5);
            AddCompilers();
        }

        private void manageCustomCompilersToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            manageCustomCompilersToolStripMenuItem.PerformClick();
        }

        private void manageCustomCompilersToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            manageCustomCompilersToolStripMenuItem.PerformClick();
        }

        public System.Windows.Media.SolidColorBrush HSVtoRGB(int H, int S, int V)
        {
            //'# Scale the Saturation and Value components to be between 0 and 1
            decimal hue = H;
            decimal sat = S / 100m;
            decimal val = V / 100m;

            decimal r = default(decimal);
            decimal g = default(decimal);
            decimal b = default(decimal);

            if (sat == 0)
            {
                //'# If the saturation is 0, then all colors are the same.
                //'# (This is some flavor of gray.)
                r = val;
                g = val;
                b = val;
            }
            else
            {
                //'# Calculate the appropriate sector of a 6-part color wheel
                decimal sectorPos = hue / 60m;
                int sectorNumber = Convert.ToInt32(Math.Floor(sectorPos));

                //'# Get the fractional part of the sector
                //'# (that is, how many degrees into the sector you are)
                decimal fractionalSector = sectorPos - sectorNumber;

                //'# Calculate values for the three axes of the color
                decimal p = val * (1 - sat);
                decimal q = val * (1 - (sat * fractionalSector));
                decimal t = val * (1 - (sat * (1 - fractionalSector)));

                //'# Assign the fractional colors to red, green, and blue
                //'# components based on the sector the angle is in
                switch (sectorNumber)
                {
                    case 0:
                    case 6:
                        r = val;
                        g = t;
                        b = p;
                        break;
                    case 1:
                        r = q;
                        g = val;
                        b = p;
                        break;
                    case 2:
                        r = p;
                        g = val;
                        b = t;
                        break;
                    case 3:
                        r = p;
                        g = q;
                        b = val;
                        break;
                    case 4:
                        r = t;
                        g = p;
                        b = val;
                        break;
                    case 5:
                        r = val;
                        g = p;
                        b = q;
                        break;
                }
            }

            //'# Scale the red, green, and blue values to be between 0 and 255
            r *= 255;
            g *= 255;
            b *= 255;

            //'# Return a color in the new color space
            return new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, Convert.ToByte(Math.Round(r, MidpointRounding.AwayFromZero)), Convert.ToByte(Math.Round(g, MidpointRounding.AwayFromZero)), Convert.ToByte(Math.Round(b, MidpointRounding.AwayFromZero))));
        }























    }
    public class MyRenderer : ToolStripRenderer
    {

        protected override void OnRenderArrow(ToolStripArrowRenderEventArgs e)
        {
            e.ArrowColor = Color.White;
            base.OnRenderArrow(e);
        }

    }

}
