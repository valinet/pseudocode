﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class BorderBrowser : WebBrowser
    {
        public BorderBrowser()
        {
            InitializeComponent();
        }

        public BorderBrowser(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                var parms = base.CreateParams;
                parms.Style |= 0x800000;  // Turn on WS_BORDER
                return parms;
            }

        }
    }
}
