﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;

public class GDICounter : IDisposable
{
    private const int GR_GDIOBJECTS = 0, GR_USEROBJECTS = 1;
    [DllImport("User32", ExactSpelling = true, CharSet = CharSet.Auto)]
    public static extern int GetGuiResources(IntPtr hProcess, int uiFlags);
    private int intialHandleCount;

    public GDICounter()
    {
        intialHandleCount = 0;

        intialHandleCount = GetGDIObjects();
    }

    private int GetGDIObjects()
    {
        try
        {
            IntPtr processHandle = System.Diagnostics.Process.GetCurrentProcess().Handle;

            if (processHandle != IntPtr.Zero)
            {
                return (int)GetGuiResources(processHandle, GR_GDIOBJECTS);
            }
        }
        catch { }
        return 0;
    }

    public void Dispose()
    {
        int currentHandleCount = GetGDIObjects();
        if (intialHandleCount != currentHandleCount)
        {
            int change = currentHandleCount - intialHandleCount;
            if (change > 0)
            {
                Debug.WriteLine("Handle count changed by: " + change.ToString() + new StackTrace().ToString());
            }
        }
    }
}