﻿using System;
using System.Windows.Forms;


public class DoubleBufferedScintilla : ScintillaNET.Scintilla
{
    public DoubleBufferedScintilla() : base()
    {
        this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
    }
    protected override CreateParams CreateParams
    {
        get
        {
            CreateParams result = base.CreateParams;
            result.ExStyle |= 0x02000000; // WS_EX_COMPOSITED 
            return result;
        }
    }
}
