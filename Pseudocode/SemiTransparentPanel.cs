﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;


namespace PseudocodeSyntaxHighlightDemo
{
    /// <summary>
    /// Summary description for TransPanel.
    /// </summary>
    public class SemiTransparentPanel : Panel
    {

        Timer Wriggler = new Timer();

        public SemiTransparentPanel()
        {
            //
            // TODO: Add constructor logic here
            //

          //  Wriggler.Tick += new EventHandler(TickHandler);
            this.Wriggler.Interval = 1000;
            this.Wriggler.Enabled = true;

        }

        protected void TickHandler(object sender, EventArgs e)
        {
            this.InvalidateEx();
        }
        public void UpdateUI()
        {
            this.InvalidateEx();

        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x00000020; //WS_EX_TRANSPARENT
                return cp;
            }
        }

        protected void InvalidateEx()
        {
            if (Parent == null)
                return;

            Rectangle rc = new Rectangle(this.Location, this.Size);
            Parent.Invalidate(rc, true);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            //do not allow the background to be painted 
        }

        Random r = new Random();

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(100, 100, 100, 100)), e.ClipRectangle);
        }


    }
}