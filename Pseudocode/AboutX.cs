﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class AboutX : Form
    {
        static bool is64BitProcess = (IntPtr.Size == 8);
        static bool is64BitOperatingSystem = is64BitProcess || InternalCheckIsWow64();

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWow64Process(
            [In] IntPtr hProcess,
            [Out] out bool wow64Process
        );

        public static bool InternalCheckIsWow64()
        {
            if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                Environment.OSVersion.Version.Major >= 6)
            {
                using (Process p = Process.GetCurrentProcess())
                {
                    bool retVal;
                    if (!IsWow64Process(p.Handle, out retVal))
                    {
                        return false;
                    }
                    return retVal;
                }
            }
            else
            {
                return false;
            }
        }

        public AboutX()
        {
            InitializeComponent();
        }

        private void AboutX_Load(object sender, EventArgs e)
        {
            label1.Text += Application.ProductVersion.ToString();
            if (!is64BitOperatingSystem) label2.Text += "32-bit";
            if (is64BitOperatingSystem) label2.Text += "64-bit";
            /*var pos = this.PointToScreen(label3.Location);
            pos = pictureBox1.PointToClient(pos);
            label3.Parent = pictureBox1;
            label3.Location = pos;
            label3.BackColor = Color.Transparent;
            var pos2 = this.PointToScreen(label1.Location);
            pos2 = pictureBox1.PointToClient(pos2);
            label1.Parent = pictureBox1;
            label1.Location = pos2;
            label1.BackColor = Color.Transparent;
            var pos3 = this.PointToScreen(label2.Location);
            pos3 = pictureBox1.PointToClient(pos3);
            label2.Parent = pictureBox1;
            label2.Location = pos3;
            label2.BackColor = Color.Transparent;
            var pos4 = this.PointToScreen(label4.Location);
            pos4 = pictureBox1.PointToClient(pos4);
            label4.Parent = pictureBox1;
            label4.Location = pos4;
            label4.BackColor = Color.Transparent;*/
            var pos5 = this.PointToScreen(pictureBox2.Location);
            pos5 = pictureBox1.PointToClient(pos5);
            pictureBox2.Parent = pictureBox1;
            pictureBox2.Location = pos5;
            pictureBox2.BackColor = Color.Transparent;
            pictureBox4.Visible = true;

        }

        private void label4_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://valinet.ro/ro/?page_id=367");
        }

        private void label4_MouseEnter(object sender, EventArgs e)
        {
            label4.Font = new Font(label4.Font, FontStyle.Underline);
        }

        private void label4_MouseLeave(object sender, EventArgs e)
        {
            label4.Font = new Font(label4.Font, FontStyle.Regular);
        }

        private void AboutX_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
