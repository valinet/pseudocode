﻿namespace Pseudocode
{
    partial class Help
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Help));
            this.appearanceControl1 = new AppearanceControl();
            this.menuStrip1 = new CustomizableMenuStrip();
            this.bACKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fORWARDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pAGEOPTIONSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.knowledgeBaseHomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pageSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.findonThisPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.vizualizareSursăToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // appearanceControl1
            // 
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.Background = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intBackground = -2039584;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intBorderHighlight = -2039584;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientBegin = -2039584;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientEnd = -2039584;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientMiddle = -2039584;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intHighlight = -3878683;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intPressedBackground = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intSelectedBackground = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(128)))), ((int)(((byte)(62)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(128)))), ((int)(((byte)(62)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intBorder = -16777088;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intBorderHighlight = -13410648;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientBegin = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientEnd = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientMiddle = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intHighlight = -6771246;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorder = -4144960;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorderHighlight = -4144960;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientBegin = -2039584;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientEnd = -2039584;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientMiddle = -2039584;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intHighlight = -3878683;
            this.appearanceControl1.CustomAppearance.GripAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.GripAppearance.intDark = -16777216;
            this.appearanceControl1.CustomAppearance.GripAppearance.intLight = -16777216;
            this.appearanceControl1.CustomAppearance.GripAppearance.Light = System.Drawing.Color.Black;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.intGradientBegin = -1;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.intGradientEnd = -1;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.intGradientMiddle = -1;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientBegin = -3416586;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientEnd = -9266217;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientMiddle = -6175239;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intBorder = -1;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intPressedGradientBegin = -2039584;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intPressedGradientEnd = -2039584;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intPressedGradientMiddle = -2039584;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intSelected = -2039584;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intSelectedGradientBegin = -2039584;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intSelectedGradientEnd = -2039584;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.PressedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.PressedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.PressedGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.Selected = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.SelectedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.SelectedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.intBorder = -16777216;
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.intGradientBegin = -2236963;
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.intGradientEnd = -2236963;
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.intGradientBegin = -1;
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.intGradientEnd = -1;
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.intGradientMiddle = -1;
            this.appearanceControl1.CustomAppearance.RaftingContainerAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.RaftingContainerAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.RaftingContainerAppearance.intGradientBegin = -1;
            this.appearanceControl1.CustomAppearance.RaftingContainerAppearance.intGradientEnd = -1;
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.intDark = -16777216;
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.intLight = -16777216;
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.Light = System.Drawing.Color.Black;
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.intGradientBegin = -1;
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.intGradientEnd = -1;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.ContentPanelGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.ContentPanelGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.DropDownBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intBorder = -1;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intContentPanelGradientBegin = -1;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intContentPanelGradientEnd = -1;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intDropDownBackground = -1;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intGradientBegin = -1;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intGradientEnd = -1;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intGradientMiddle = -1;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intPanelGradientBegin = -1;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intPanelGradientEnd = -1;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.PanelGradientBegin = System.Drawing.Color.White;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.PanelGradientEnd = System.Drawing.Color.White;
            this.appearanceControl1.Preset = AppearanceControl.enumPresetStyles.Office2007;
            this.appearanceControl1.Renderer.RoundedEdges = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Appearance = this.appearanceControl1;
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bACKToolStripMenuItem,
            this.fORWARDToolStripMenuItem,
            this.pAGEOPTIONSToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // bACKToolStripMenuItem
            // 
            resources.ApplyResources(this.bACKToolStripMenuItem, "bACKToolStripMenuItem");
            this.bACKToolStripMenuItem.Name = "bACKToolStripMenuItem";
            this.bACKToolStripMenuItem.Click += new System.EventHandler(this.bACKToolStripMenuItem_Click);
            // 
            // fORWARDToolStripMenuItem
            // 
            resources.ApplyResources(this.fORWARDToolStripMenuItem, "fORWARDToolStripMenuItem");
            this.fORWARDToolStripMenuItem.Name = "fORWARDToolStripMenuItem";
            this.fORWARDToolStripMenuItem.Click += new System.EventHandler(this.fORWARDToolStripMenuItem_Click);
            // 
            // pAGEOPTIONSToolStripMenuItem
            // 
            this.pAGEOPTIONSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.knowledgeBaseHomeToolStripMenuItem,
            this.toolStripMenuItem1,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.pageSetupToolStripMenuItem,
            this.toolStripMenuItem2,
            this.findonThisPageToolStripMenuItem,
            this.toolStripMenuItem4,
            this.vizualizareSursăToolStripMenuItem});
            this.pAGEOPTIONSToolStripMenuItem.Name = "pAGEOPTIONSToolStripMenuItem";
            resources.ApplyResources(this.pAGEOPTIONSToolStripMenuItem, "pAGEOPTIONSToolStripMenuItem");
            // 
            // knowledgeBaseHomeToolStripMenuItem
            // 
            resources.ApplyResources(this.knowledgeBaseHomeToolStripMenuItem, "knowledgeBaseHomeToolStripMenuItem");
            this.knowledgeBaseHomeToolStripMenuItem.Name = "knowledgeBaseHomeToolStripMenuItem";
            this.knowledgeBaseHomeToolStripMenuItem.Click += new System.EventHandler(this.knowledgeBaseHomeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            // 
            // printToolStripMenuItem
            // 
            resources.ApplyResources(this.printToolStripMenuItem, "printToolStripMenuItem");
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // printPreviewToolStripMenuItem
            // 
            resources.ApplyResources(this.printPreviewToolStripMenuItem, "printPreviewToolStripMenuItem");
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Click += new System.EventHandler(this.printPreviewToolStripMenuItem_Click);
            // 
            // pageSetupToolStripMenuItem
            // 
            resources.ApplyResources(this.pageSetupToolStripMenuItem, "pageSetupToolStripMenuItem");
            this.pageSetupToolStripMenuItem.Name = "pageSetupToolStripMenuItem";
            this.pageSetupToolStripMenuItem.Click += new System.EventHandler(this.pageSetupToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            // 
            // findonThisPageToolStripMenuItem
            // 
            resources.ApplyResources(this.findonThisPageToolStripMenuItem, "findonThisPageToolStripMenuItem");
            this.findonThisPageToolStripMenuItem.Name = "findonThisPageToolStripMenuItem";
            this.findonThisPageToolStripMenuItem.Click += new System.EventHandler(this.findonThisPageToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
            // 
            // vizualizareSursăToolStripMenuItem
            // 
            this.vizualizareSursăToolStripMenuItem.Name = "vizualizareSursăToolStripMenuItem";
            resources.ApplyResources(this.vizualizareSursăToolStripMenuItem, "vizualizareSursăToolStripMenuItem");
            this.vizualizareSursăToolStripMenuItem.Click += new System.EventHandler(this.vizualizareSursăToolStripMenuItem_Click);
            // 
            // webBrowser1
            // 
            resources.ApplyResources(this.webBrowser1, "webBrowser1");
            this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.Url = new System.Uri("", System.UriKind.Relative);
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            this.webBrowser1.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.webBrowser1_Navigating);
            // 
            // Help
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Help";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Help_FormClosing);
            this.Load += new System.EventHandler(this.Help_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AppearanceControl appearanceControl1;
        private CustomizableMenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bACKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fORWARDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pAGEOPTIONSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pageSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem findonThisPageToolStripMenuItem;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem vizualizareSursăToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem knowledgeBaseHomeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    }
}