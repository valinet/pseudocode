﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;
using AppLimit.CloudComputing.SharpBox;
using AppLimit.CloudComputing.SharpBox.StorageProvider.DropBox;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic.Devices;
using System.Linq;
using Pseudocode;
//using CefSharp;
//using CefSharp.WinForms;
using ValiNet.PseudocodePluginsFramework;
using System.Collections.Specialized;
using System.Drawing.Printing;
using System.Drawing.Imaging;
//using System.Windows.Forms.Integration;
using System.Web;
using System.Threading;
using WeifenLuo.WinFormsUI.Docking;
using System.CodeDom.Compiler;
using System.CodeDom;
namespace Pseudocode
{
    public partial class Form1 : DockContent
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        private bool mouseIsDown = false;
        private Point firstPoint;
        public static List<ScintillaNET.Scintilla> pseudocode = new List<ScintillaNET.Scintilla>();
        public static List<ScintillaNET.Scintilla> converter = new List<ScintillaNET.Scintilla>();
        //public static ScintillaNET.Scintilla[] pseudocode = new ScintillaNET.Scintilla[1000];
        //public static ScintillaNET.Scintilla[] converter = new ScintillaNET.Scintilla[1000];
        private Splash splashScreen = new Splash();
        private bool done = false;
        string CloseText;
        string MaximizeText;
        string MinimizeText;
        string RestoreText;
        string NewTabTitle;
        public string AppName;
        string NoMatchFoundText;
        string SearchAtBeginning;
        string InvalidLineNumber;
        string NoBrowserInstalled;
        string AlreadySignedIn;
        string CompileMayGenerateErrors;
        string DoYouWantToSaveMod1;
        string DoYouWantToSaveMod2;
        string DownloadModule;
        string DownloadOK;
        string NoFileLocationSpecified;
        string CompileInfo;
        string FileNotSaved;
        string NotSignedIn;
        string CPPSource;
        string VB6Source;
        string CSSource;
        string PASSource;
        string WebPage;
        string UnableToPrint;
        string CantUndo;
        string CantRedo;
        string ProgramIsRunning;
        string EndText;
        string ErrorText1;
        string ErrorText2;
        string ExternalAddress;
        string DevTools;
        string WarningAdvancedUSersOnly;
        string PossibleError;
        string NoStackTrace;
        string UndeclaredVariable;
        string SaveLocationIsTooLong;
        string FileIsOpened;
        string ModOperatorError;
        string RootOperatorError;
        string SwitchToCode;
        string SwitchToImage;
        string SucessEndDebugProc;
        string FailEndDebugProc;
        string FailedToSendCom;
        string CreateEXEError;
        string CompileError;
        string InstallWindowsLiveMail;
        string InstallPowerPointViewer;
        string NewVariableName;
        string NotDefinible;
        string DifferentLanguage;
        string NewValue;
        public string NotInApp;
        string Executions;
        string UnsupportedFile;
        string codeExecution;
        string closeWindow;
        string executionTime;
        string seconds;
        string running;
        public string IntegerKeyword;
        public string DoubleKeyword;
        public string CharKeyword;
        public string StringKeyword;
        public string DivKeyword;
        public string ModKeyword;
        public string DifferentKeyword;
        public string AndKeyword;
        public string OrKeyword;
        public string ReadKeyword;
        public string WriteKeyword;
        public string IfKeyword;
        public string ThenKeyword;
        public string ElseKeyword;
        public string EndIfKeyword;
        public string WhileKeyword;
        public string EndWhileKeyword;
        public string ForKeyword;
        public string EndForKeyword;
        public string DoKeyword;
        public string LoopUntilKeyword;
        public string InputText;
        public string YES;
        public string NO;
        public int PseudocodeLang;
        public string FormTitle = "";
        public string[] str = new string[21];
        public List<string> strVar = new List<string>();
        public List<string> fileProperties = new List<string>();
        internal static class NativeWinAPI
        {
            internal static readonly int GWL_EXSTYLE = -20;
            internal static readonly int WS_EX_COMPOSITED = 0x02000000;

            [DllImport("user32")]
            internal static extern int GetWindowLong(IntPtr hWnd, int nIndex);

            [DllImport("user32")]
            internal static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        }
        private void ShowSplashScreen()
        {
            splashScreen.Show();
            while (!done)
            {
                Application.DoEvents();
            }
            splashScreen.Close();
            this.splashScreen.Dispose();
        }
        Dictionary<string, VPlugin> _Plugins;
        Color color1;
        Color color2;
        Color color3;
        public void ThemeCode()
        {
            if (Properties.Settings.Default.UITheme == 1) pseudocodeEd.Caret.Color = Color.Black;
            else pseudocodeEd.Caret.Color = Color.White;
            if (Properties.Settings.Default.UITheme == 1) converterEd.ForeColor = Color.Black;
            else converterEd.ForeColor = Color.White;
            if (Properties.Settings.Default.UITheme == 0)
            {
                this.BackColor = color1;
                statusBar1.BackColor = color1;
                buttonItem1.BackColor = color1;
                buttonItem2.BackColor = color1;
                toolStripStatusLabel1.BackColor = color1;
                buttonItem1.ForeColor = color2;
                buttonItem2.ForeColor = color2;
                toolStripStatusLabel1.ForeColor = color2;
                buttonItem5.BackColor = color1;
                buttonItem6.BackColor = color1;
                buttonItem4.BackColor = color1;
                progressBar1.BackColor = color1;
                toolStripTrackBar1.BackColor = color1;
                buttonItem5.ForeColor = color2;
                buttonItem6.ForeColor = color2;
                buttonItem4.ForeColor = color2;
                splitContainer3.BackColor = color1;
                splitContainer3.Panel2.BackColor = color1;
                BottomDown.BackColor = color1;
                splitContainer3.BackColor = color1;
                pseudocodeEd.BackColor = color1;
                converterEd.BackColor = color1;
                VisualScrollBar.BackColor = color1;
                pseudocodeEd.Caret.CurrentLineBackgroundColor = Color.DimGray;
                tabPage3.BackColor = color3;
                tabPage4.BackColor = color3;
                tabPage5.BackColor = color3;
                richTextBox1.BackColor = color1;
                richTextBox1.ForeColor = color2;
                textBox6.BackColor = color1;
                textBox6.ForeColor = color2;
                label37.ForeColor = color2;
                textBox7.BackColor = color1;
                textBox7.ForeColor = color2;
                label38.ForeColor = color2;
                listBox3.BackColor = color1;
                listBox4.BackColor = color1;
                listBox3.ForeColor = color2;
                listBox4.ForeColor = color2;
                splitContainer1.BackColor = color1;
                splitContainer1.Panel1.BackColor = color1;
                splitContainer1.Panel2.BackColor = color1;
                toolStripStatusLabel2.BackColor = color1;
                toolStripStatusLabel2.ForeColor = color2;
                toolStripStatusLabel3.BackColor = color1;
                toolStripStatusLabel3.ForeColor = color2;
                SchemaLogica.BackColor = color1;
                MainForm mf = this.MdiParent as MainForm;
                contextMenuStrip2.Appearance = mf.blackApparance1;
                foreach (var item in contextMenuStrip2.Items)
                {
                    ToolStripMenuItem it = item as ToolStripMenuItem;
                    if (it != null) it.ForeColor = Color.White;
                }
            }
            if (Properties.Settings.Default.UITheme == 1)
            {
                this.BackColor = Color.White;
                statusBar1.BackColor = Color.White;
                buttonItem1.BackColor = Color.White;
                buttonItem2.BackColor = Color.White;
                toolStripStatusLabel1.BackColor = Color.White;
                buttonItem1.ForeColor = Color.Black;
                buttonItem2.ForeColor = Color.Black;
                toolStripStatusLabel1.ForeColor = Color.Black;
                buttonItem5.BackColor = Color.White;
                buttonItem6.BackColor = Color.White;
                buttonItem4.BackColor = Color.White;
                progressBar1.BackColor = Color.White;
                toolStripTrackBar1.BackColor = Color.White;
                buttonItem5.ForeColor = Color.Black;
                buttonItem6.ForeColor = Color.Black;
                buttonItem4.ForeColor = Color.Black;
                splitContainer3.BackColor = Color.White;
                splitContainer3.Panel2.BackColor = Color.White;
                BottomDown.BackColor = Color.White;
                splitContainer3.BackColor = Color.White;
                pseudocodeEd.BackColor = Color.White;
                converterEd.BackColor = Color.White;
                VisualScrollBar.BackColor = Color.White;
                pseudocodeEd.Caret.CurrentLineBackgroundColor = Color.FromArgb(221, 221, 221);
                tabPage3.BackColor = Color.FromArgb(221, 221, 221);
                tabPage4.BackColor = Color.FromArgb(221, 221, 221);
                tabPage5.BackColor = Color.FromArgb(221, 221, 221);
                richTextBox1.BackColor = Color.White;
                richTextBox1.ForeColor = Color.Black;
                textBox6.BackColor = Color.White;
                textBox6.ForeColor = Color.Black;
                label37.ForeColor = Color.Black;
                textBox7.BackColor = Color.White;
                textBox7.ForeColor = Color.Black;
                label38.ForeColor = Color.Black;
                listBox3.BackColor = Color.White;
                listBox4.BackColor = Color.White;
                listBox3.ForeColor = Color.Black;
                listBox4.ForeColor = Color.Black;
                splitContainer1.BackColor = Color.White;
                splitContainer1.Panel1.BackColor = Color.White;
                splitContainer1.Panel2.BackColor = Color.White;
                toolStripStatusLabel2.BackColor = Color.White;
                toolStripStatusLabel2.ForeColor = Color.Black;
                toolStripStatusLabel3.BackColor = Color.White;
                toolStripStatusLabel3.ForeColor = Color.Black;
                SchemaLogica.BackColor = Color.White;
                MainForm mf = this.MdiParent as MainForm;
                contextMenuStrip2.Appearance = mf.whiteApparence1;
                foreach (var item in contextMenuStrip2.Items)
                {
                    ToolStripMenuItem it = item as ToolStripMenuItem;
                    if (it != null) it.ForeColor = Color.Black;
                }
            }
            pseudocodeEd.Lexing.Lexer = ScintillaNET.Lexer.Cpp;
            pseudocodeEd.Lexing.Keywords[0] = ReadKeyword + " " + WriteKeyword + " " + IfKeyword + " " + ThenKeyword + " " + ElseKeyword + " " + EndIfKeyword + " " + WhileKeyword + " " + EndWhileKeyword + " " + ForKeyword + " " + EndForKeyword + " " + DoKeyword + " " + LoopUntilKeyword;
            pseudocodeEd.Lexing.Keywords[1] = IntegerKeyword + " " + DoubleKeyword + " " + CharKeyword + " " + StringKeyword + " " + DivKeyword + " " + ModKeyword + " " + DifferentKeyword + " " + AndKeyword + " " + OrKeyword;
            //pseudocodeEd.Lexing.Keywords[0] = "citeste scrie daca atunci altfel sfarsit_daca cat_timp sfarsit_cat_timp pentru sfarsit_pentru executa pana_cand";
            //pseudocodeEd.Lexing.Keywords[1] = "intreg real caracter sir div mod diferit si sau";
            pseudocodeEd.Lexing.LineCommentPrefix = "//";
            if (Properties.Settings.Default.UITheme == 1)
            {
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].ForeColor = System.Drawing.Color.Black; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["NUMBER"]].ForeColor = System.Drawing.Color.FromArgb(43, 145, 175); //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD2"]].ForeColor = System.Drawing.Color.FromArgb(0, 0, 255); //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD"]].ForeColor = System.Drawing.Color.DarkOrange; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["STRING"]].ForeColor = System.Drawing.Color.FromArgb(163, 21, 21); //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["CHARACTER"]].ForeColor = System.Drawing.Color.FromArgb(163, 21, 21); //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["PREPROCESSOR"]].ForeColor = System.Drawing.Color.Black; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["OPERATOR"]].ForeColor = System.Drawing.Color.FromArgb(22, 196, 226); //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["IDENTIFIER"]].ForeColor = System.Drawing.Color.Black; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["COMMENT"]].ForeColor = System.Drawing.Color.FromArgb(0, 128, 0);
                pseudocodeEd.Styles.LineNumber.BackColor = Color.White;
                pseudocodeEd.Styles.LineNumber.ForeColor = Color.DimGray;
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].BackColor = System.Drawing.Color.White; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["NUMBER"]].BackColor = System.Drawing.Color.White; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD2"]].BackColor = System.Drawing.Color.White; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD"]].BackColor = System.Drawing.Color.White; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["STRING"]].BackColor = System.Drawing.Color.White; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["CHARACTER"]].BackColor = System.Drawing.Color.White; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["PREPROCESSOR"]].BackColor = System.Drawing.Color.White; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["OPERATOR"]].BackColor = System.Drawing.Color.White; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["IDENTIFIER"]].BackColor = System.Drawing.Color.White; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["COMMENT"]].BackColor = System.Drawing.Color.White;
            }
            else
            {
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].ForeColor = System.Drawing.Color.White;
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["NUMBER"]].ForeColor = System.Drawing.Color.FromArgb(174, 129, 221);
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD2"]].ForeColor = System.Drawing.Color.FromArgb(102, 217, 239);
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD"]].ForeColor = Color.Orange;//System.Drawing.Color.FromArgb(166, 246, 66);
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["STRING"]].ForeColor = System.Drawing.Color.FromArgb(230, 219, 116);
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["CHARACTER"]].ForeColor = System.Drawing.Color.FromArgb(230, 219, 116);
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["PREPROCESSOR"]].ForeColor = System.Drawing.Color.White;
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["OPERATOR"]].ForeColor = System.Drawing.Color.FromArgb(127, 226, 242);
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["IDENTIFIER"]].ForeColor = System.Drawing.Color.White;
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["COMMENT"]].ForeColor = System.Drawing.Color.FromArgb(117, 113, 75);
                pseudocodeEd.Styles.LineNumber.BackColor = Color.FromArgb(64, 64, 64);
                pseudocodeEd.Styles.LineNumber.ForeColor = Color.Silver;
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].BackColor = color1; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["NUMBER"]].BackColor = color1; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD2"]].BackColor = color1; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD"]].BackColor = color1; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["STRING"]].BackColor = color1; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["CHARACTER"]].BackColor = color1; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["PREPROCESSOR"]].BackColor = color1; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["OPERATOR"]].BackColor = color1; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["IDENTIFIER"]].BackColor = color1; //!
                pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["COMMENT"]].BackColor = color1;
            }
            splitContainer3.Dock = DockStyle.Fill;
            VisualScrollBar.Font = new Font("Courier New", 10);
            VisualScrollBar.ConfigurationManager.CustomLocation = Application.StartupPath + "\\Scintilla.XML";
            VisualScrollBar.ConfigurationManager.Language = "pascal";
            VisualScrollBar.ConfigurationManager.Configure();
            //        VisualScrollBar.Caret.CurrentLineBackgroundColor = Color.LemonChiffon;
            VisualScrollBar.Caret.HighlightCurrentLine = true;
            VisualScrollBar.Caret.Color = Color.White;
            VisualScrollBar.Lexing.Lexer = ScintillaNET.Lexer.Cpp;
            VisualScrollBar.Lexing.Keywords[0] = ReadKeyword + " " + WriteKeyword + " " + IfKeyword + " " + ThenKeyword + " " + ElseKeyword + " " + EndIfKeyword + " " + WhileKeyword + " " + EndWhileKeyword + " " + ForKeyword + " " + EndForKeyword + " " + DoKeyword + " " + LoopUntilKeyword;
            VisualScrollBar.Lexing.Keywords[1] = IntegerKeyword + " " + DoubleKeyword + " " + CharKeyword + " " + StringKeyword + " " + DivKeyword + " " + ModKeyword + " " + DifferentKeyword + " " + AndKeyword + " " + OrKeyword;
            //VisualScrollBar.Lexing.Keywords[0] = "citeste scrie daca atunci altfel sfarsit_daca cat_timp sfarsit_cat_timp pentru sfarsit_pentru executa pana_cand";
            //VisualScrollBar.Lexing.Keywords[1] = "intreg real caracter sir div mod diferit si sau";
            VisualScrollBar.Lexing.LineCommentPrefix = "//";
            if (Properties.Settings.Default.UITheme == 1)
            {
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].ForeColor = System.Drawing.Color.Black; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["NUMBER"]].ForeColor = System.Drawing.Color.FromArgb(43, 145, 175); //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["WORD2"]].ForeColor = System.Drawing.Color.FromArgb(0, 0, 255); //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["WORD"]].ForeColor = System.Drawing.Color.DarkOrange; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["STRING"]].ForeColor = System.Drawing.Color.FromArgb(163, 21, 21); //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["CHARACTER"]].ForeColor = System.Drawing.Color.FromArgb(163, 21, 21); //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["PREPROCESSOR"]].ForeColor = System.Drawing.Color.Black; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["OPERATOR"]].ForeColor = System.Drawing.Color.FromArgb(22, 196, 226); //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["IDENTIFIER"]].ForeColor = System.Drawing.Color.Black; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["COMMENT"]].ForeColor = System.Drawing.Color.FromArgb(0, 128, 0);
                VisualScrollBar.Styles.LineNumber.BackColor = Color.White;
                VisualScrollBar.Styles.LineNumber.ForeColor = Color.DimGray;
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].BackColor = System.Drawing.Color.White; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["NUMBER"]].BackColor = System.Drawing.Color.White; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["WORD2"]].BackColor = System.Drawing.Color.White; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["WORD"]].BackColor = System.Drawing.Color.White; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["STRING"]].BackColor = System.Drawing.Color.White; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["CHARACTER"]].BackColor = System.Drawing.Color.White; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["PREPROCESSOR"]].BackColor = System.Drawing.Color.White; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["OPERATOR"]].BackColor = System.Drawing.Color.White; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["IDENTIFIER"]].BackColor = System.Drawing.Color.White; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["COMMENT"]].BackColor = System.Drawing.Color.White;
            }
            else
            {
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].ForeColor = System.Drawing.Color.White;
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["NUMBER"]].ForeColor = System.Drawing.Color.FromArgb(174, 129, 221);
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["WORD2"]].ForeColor = System.Drawing.Color.FromArgb(102, 217, 239);
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["WORD"]].ForeColor = Color.Orange;//System.Drawing.Color.FromArgb(166, 246, 66);
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["STRING"]].ForeColor = System.Drawing.Color.FromArgb(230, 219, 116);
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["CHARACTER"]].ForeColor = System.Drawing.Color.FromArgb(230, 219, 116);
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["PREPROCESSOR"]].ForeColor = System.Drawing.Color.White;
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["OPERATOR"]].ForeColor = System.Drawing.Color.FromArgb(127, 226, 242);
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["IDENTIFIER"]].ForeColor = System.Drawing.Color.White;
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["COMMENT"]].ForeColor = System.Drawing.Color.FromArgb(117, 113, 75);
                VisualScrollBar.Styles.LineNumber.BackColor = Color.FromArgb(64, 64, 64);
                VisualScrollBar.Styles.LineNumber.ForeColor = Color.Silver;
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].BackColor = color1; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["NUMBER"]].BackColor = color1; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["WORD2"]].BackColor = color1; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["WORD"]].BackColor = color1; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["STRING"]].BackColor = color1; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["CHARACTER"]].BackColor = color1; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["PREPROCESSOR"]].BackColor = color1; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["OPERATOR"]].BackColor = color1; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["IDENTIFIER"]].BackColor = color1; //!
                VisualScrollBar.Styles[VisualScrollBar.Lexing.StyleNameMap["COMMENT"]].BackColor = color1;
            }
            if (Properties.Settings.Default.Language == 6) button27.PerformClick();
            //Sfarsit colorare cod tema inchisa
            //end code
        }
        public Form1()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            //theme code
            color1 = statusBar1.BackColor; //64
            color2 = buttonItem2.ForeColor; //alb
            color3 = tabPage3.BackColor; //negru


            toolStripTrackBar1.Height = 20;
            toolStripTrackBar1.Scroll -= toolStripTrackBar1_Scroll;
            if (Properties.Settings.Default.Zoom >= 0) toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
            toolStripTrackBar1.Scroll += toolStripTrackBar1_Scroll;
            UserPanel.Visible = false;
            mLastState = this.WindowState;
            pseudocodeEd.MouseMove += pseudocodeEd_MouseMove;
            if (Properties.Settings.Default.AddonToBeErased != "")
            {
                try
                {
                    Directory.Delete(Properties.Settings.Default.AddonToBeErased, true);
                    Properties.Settings.Default.AddonToBeErased = "";
                    Properties.Settings.Default.Save();
                }
                catch { }
            }
            //load plugins
            try
            {
                _Plugins = new Dictionary<string, VPlugin>();
                //ICollection<IPlugin> plugins = PluginLoader.LoadPlugins("Plugins");
                ICollection<VPlugin> plugins = GenericPluginLoader<VPlugin>.LoadPlugins(Application.StartupPath);
                try
                {
                    foreach (var item in plugins)
                    {
                        _Plugins.Add(item.Name, item);
                    }
                }
                catch { }
            }
            catch { }
            //end load plugins

            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            //System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(this.ShowSplashScreen));
            //  if (Properties.Settings.Default.UILang == 0) thread.CurrentUICulture = new System.Globalization.CultureInfo("");
            // if (Properties.Settings.Default.UILang == 1) thread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            //  thread.Start();

            //System.Configuration.ConfigurationFileMap fileMap = new System.Configuration.ConfigurationFileMap(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\ValiNet Romania\\Pseudocode\\user.config"); //Path to your config file
            //System.Configuration.Configuration configuration = System.Configuration.ConfigurationManager.OpenMappedMachineConfiguration(fileMap);
            if (Properties.Settings.Default.UILang == 0)
            {
                NewTabTitle = "Untitled";
                AppName = "Pseudocode";
                CloseText = "Close (Alt+F4)";
                MaximizeText = "Maximize";
                MinimizeText = "Minimize";
                RestoreText = "Restore down";
                NoMatchFoundText = "No match found";
                SearchAtBeginning = "Reached beginning of the pseudocode file";
                InvalidLineNumber = "Invalid line number";
                NoBrowserInstalled = "There is no Internet browser installed on this computer.\n\nIn order to use this feature, a browser has to be installed on your machine. Visit www.browserchoice.eu to download a web browser for your computer if you are based in the European Union or, if located in any other country, visit www.whatbrowser.org.";
                AlreadySignedIn = "You have already signed in into the application.\n\nIn order to sign out, click on your picture in the main window, then choose Sign out and switch to another account.";
                CompileMayGenerateErrors = "Do you want to continue the compile process?\n\nCompiling the pseudocode file may generate errors, because a necessary external module is not installed.\nYou can try looking for the missing module in the module repository.\n\nModule name: ";
                DoYouWantToSaveMod1 = "Save changes made to file \"";
                DoYouWantToSaveMod2 = "\"?\n\nA copy of the file will be kept temporary on the computer even if you choose to either save or dismiss the file.";
                DownloadModule = "Do you want to download a module?\n\nModule name: ";
                DownloadOK = "The module has been downloaded succesfully.\n\nDo you want to reference it in the current poseudocode file?";
                NoFileLocationSpecified = "The save file location was not specified.\n\nIn order to compile, that field must be filled with information. Only the remaining fields are optional.";
                CompileInfo = "An administrative account is neccessary in order to generate the EXE file.\n\nIf you are uysing Windows Vista/7/8, confirm the User Account Request. If you are using Windows XP or Windows Vista/7/8 with the User Account Control feature disabled, please log on (on Windows XP/Vista/7) or sign in (Windows 8) using an administrative account and try the operation again.";
                FileNotSaved = "This pseudocode file is not saved.\n\nSave the file firstly, then try uploading it again. In order to save, click the Save button on the toolbar or simply press the [Ctrl]+[S] key sequence.";
                NotSignedIn = "You are not signed into your Dropbox account.\n\nIn order to upload files to Dropbox, you have to sign in into your Dropbox account from within the Pseudocode application. In order to sign in, open the Tools menu and choose Sign In.";
                CPPSource = "CPP Source (*.cpp)|*.cpp";
                VB6Source = "Basic Classic script (*.vbs)|*.vbs";
                CSSource = "C# source (*.cs)|*.cs";
                PASSource = "Pascal source (*.pas)|*.pas";
                WebPage = "Web Page (*.htm)|*.htm";
                UnableToPrint = "Printing is not available right now.\n\nA printer may be unavailable or offline at the moment.\nAditional error information: ";
                CantUndo = "Can't undo (Ctrl + Z)";
                CantRedo = "Can't redo (Ctrl + Y)";
                ProgramIsRunning = "Your pseudocode is now running on your PC.\n\nThis overlay will close automatically when your pseudocode will complete running.\nYou can stop the execution at any time by clicking outside this message or on the End button below.";
                EndText = "End";
                ErrorText1 = "Error on line ";
                ErrorText2 = " at position ";
                ExternalAddress = "The address you are trying to visit is an external address.\n\nDo you want to open it in your PC default browser?\n\n";
                DevTools = "DEVELOPER TOOLS";
                WarningAdvancedUSersOnly = "Kepp in mind that the following settings are for advanced users only.\n\nShould the application warn you again in the future about this?";
                PossibleError = "Possible error on decimal division: the following variables are not of the same type: ";
                NoStackTrace = "No stack trace is available for this error.";
                UndeclaredVariable = "The following variable was not defined: ";
                SaveLocationIsTooLong = "Cannot save the file.\n\nThe specified path is too long to be handled by Windows. Try saving the file in some other location as a workaround.\nGeek's information: Windows' Win32 File API calls are limited to handling paths containing no more than 260 characters (MAX_PATH defined in windef.h), effectively no more than 259 characters, as the file name is ended by \\0 null terminator. Pseudocode uses managed .NET functions to save files to disk which rely on native Win32 API calls, thus it is unable to save files on paths longer than 259 human-readable characters or in folders with paths longer than 248 characters. It is a limitation of the legacy code put by Microsoft in Windows, not of the file system you may use (NTFS) which supports file names with up to 32,767 characters.";
                FileIsOpened = "Cannot save file.\n\nThe file you are trying to save is currently accesed by another program. Close any applications that may be useing the file and try again.\nGeek's information: You can try to free up the file from any processes which may be using it using the free utility UNlocker, available at http://www.emptyloop.com/unlocker/.";
                ModOperatorError = ": invalid operand of type double is assigned the result of a mod operation.";
                RootOperatorError = ": invalid operand of type double is assigned the result of a ^ operation.";
                SwitchToCode = "In order to print converted code, you have to switch to a programming language.\n\nTo convert the code to a programming language instead of displaying the Logic Schema, click a language's name in the Language toolbar.";
                SwitchToImage = "In order to save or print the Logic Schema, you have to firstly open it within the editor.\n\nTo open it within the editor, press the Logic Schema button in the Language toolbar.";
                SucessEndDebugProc = "Successfully ended debug process.";
                FailEndDebugProc = "Failed to end debug process, it may be already dead.";
                FailedToSendCom = "Failed to send command; no running instance of the debugger was found on this machine.";
                CreateEXEError = "An error has occured when creating the EXE file.\n\nReview the information provided in the fields and try again.";
                CompileError = "An error has occured when trying to compile your pseudocode.\n\nError description: ";
                InstallWindowsLiveMail = "Windows Live Mail is not installed.\n\nWhen running on the Microsoft(R) Windows XP operatying system, Pseudocode requires the free Microsoft Windows Live Mail to be installed in order to run.\n\nWould you like to go online and download the application now? This feature will use approximately 60 MB of disk space from your available storage.";
                InstallPowerPointViewer = "Microsoft PowerPoint Viewer is not installed.\n\nWhen running on the Microsoft(R) Windows XP operatying system, Pseudocode requires the free Microsoft PowerPoint Viewer to be installed in order to run.\n\nWould you like to go online and download the application now? This feature will use approximately 270 MB of disk space from your available storage.";
                NewVariableName = "Write the new name this variable should have.";
                NewValue = "Write the value you would like to assign the selected variable.";
                NotInApp = "DOES NOT EXIST IN COMPILED APP";
                Executions = "TIMES RUN";
                UnsupportedFile = "This file is not supported.\n\nIn order to instal this file, Pseudocode needs to have installed the custom dictionary file it references to. Install the respective file, then try reopening the file again.";
                codeExecution = "Code running";
                closeWindow = "Close this window";
                executionTime = "Execution time: ";
                seconds = "seconds";
                running = "Executing: ";
                //NotDefinible = "No match found.\n\nThis variable is not defined or the object you performed the operation on is a non-definable object.";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                NewTabTitle = "Neintitulat";
                AppName = "Pseudocode";
                CloseText = "Închidere (Alt+F4)";
                MaximizeText = "Maximizare";
                MinimizeText = "Minimizare";
                RestoreText = "Restabilire jos";
                NoMatchFoundText = "Nicio potrivire";
                SearchAtBeginning = "S-a ajuns la începutul pseudocodului";
                InvalidLineNumber = "Număr de linie invalid";
                NoBrowserInstalled = "Niciun browser Web nu este instalat pe acest computer.\n\nPentru a utiliza această caracteristică, un browser trebuie să fie instalat pe computer. Pentru a descărca un browser, vizitați www.browserchoice.eu dacă sunteți în Uniunea Europeană sau www.whatbrowser.org dacă sunteți din oricare altă țară.";
                AlreadySignedIn = "Ați făcut deja sign in în aplicație.\n\nPentru a face sign out, faceți clic pe imaginea dvs. în fereastra principală și apoi alegeți Sign out și comutare la alt cont.";
                CompileMayGenerateErrors = "Continuați compilarea?\n\nCompilarea ar putea genera erori, deoarece un modul necesar nu este instalat.\nPuteți căuta modulul lipsă în Depozitul de Module.\n\nNume modul: ";
                DoYouWantToSaveMod1 = "Salvați modificările efectuate asupra fișierului \"";
                DoYouWantToSaveMod2 = "\"?\n\nO copie a fișierului va fi păstrată temporar pe computer fie dacă alegeți sau nu să salvați fișierul.";
                DownloadModule = "Descărcați un modul?\n\nNume modul: ";
                DownloadOK = "Modulul s-a descărcat cu succes.\n\nAdăugați o referință către acesta în pseudocodul curent?";
                NoFileLocationSpecified = "Locația de salvare a fișierului nu a fost specificată.\n\nPentru a crea un fișier EXE, acel câmp trebuie completat. Numai restul câmpurilor sunt opționale.";
                CompileInfo = "Pentru a putea compila executabilul, este necesar un cont cu privilegii administrative.\n\nDacă utilizați Windows Vista/7/8, confirmați cererea CCU (Control cont utilizator).\nDacă utilizați Windows XP sau aveți CCU dezactivat în Windows Vista/7/8, faceți log on (în Windows XP/Vista/7) sau sign in (în Windows 8) pe computer folosind un cont cu privilegii administrative și încercați din nou.";
                FileNotSaved = "Acest fișier pseudocod nu este salvat.\n\nSalvați mai întâi fișierul, apoi încercați din nou să îl încărcați. Pentru a salva, apăsați pe butonul Salvare în bara de comenzi sau folosiți combinația de taste [Ctrl]+[S].";
                NotSignedIn = "Nu ați făcut sign in cu un ID Dropbox în Pseudocode.\n\nPentru a încărca fișiere la Dropbox, faceți sign in cu contul dvs. Dropbox în aplicație. Pentru a face sign in, deschideți meniul Unelte și alegeți Sign in.";
                CPPSource = "Sursă C++ (*.cpp)|*.cpp";
                VB6Source = "Script Basic Classic (*.vbs)|*.vbs";
                CSSource = "Sursă C# (*.cs)|*.cs";
                PASSource = "Sursă Pascal (*.pas)|*.pas";
                WebPage = "Pagină Web (*.htm)|*.htm";
                UnableToPrint = "Imprimarea este indisponibilă în acest moment.\n\nO imprimantă ar putea fi indisponibilă sau deconectată momentan.\nInformații adiționale eroare: ";
                CantUndo = "Nu se poate anula (Ctrl + Z)";
                CantRedo = "Nu se poate reface (Ctrl + Y)";
                ProgramIsRunning = "Pseudocodul dvs. se execută acum pe PC.\n\nAcest mesaj va dispărea automat când execuția pseudocodului dvs. se va finaliza.\nPuteți opri execuția în orice moment făcând clic pe butonul Terminare de mai jos sau în orice zonă din afara acestui mesaj.";
                EndText = "Terminare";
                ErrorText1 = "Eroare pe linia ";
                ErrorText2 = " în poziția ";
                ExternalAddress = "Adresa pe care încercați să o vizitați este externă.\n\nDoriți să o deschideți în browserul implicit?\n\n";
                DevTools = "UNELTE PENTRU DEZVOLTATORI";
                WarningAdvancedUSersOnly = "Rețineți că următoarele setări sunt destinate spre a fi editate numai de către utilizatori avansați.\n\nVi se amintește pe viitor acest lucru când accesați aceste setări?";
                PossibleError = "Eroare posibilă la împărțirea cu rezultat număr zecimal: variabilele următoare nu sunt de acelați tip: ";
                NoStackTrace = "Nu sunt disponibile activități suplimentare pentru această eroare.";
                UndeclaredVariable = "Următoarea variabilă nu a fost nedeclarată: ";
                SaveLocationIsTooLong = "Nu s-a putut salva fișierul.\n\nCalea specificată este prea lungă pentru a putea fi utilizată în Windows. Ca o rezolvare parțială, încercați salvarea într-o altă locație pe disc mai scurtă ca număr de caractere al căii.\nZona pasionaților: Apelurile API ale Win32 din Windows sunt limitate la lucrul cu nume de fișiere de până în 260 de caractere (MAX_PATH definit în windef.h); de fapt, numai 259 de caractere, căci al 260-lea este reprezentat mereu de terminatorul \\0. Pseudocode folosește cod manageriat care apelează funcțiile native Win32, de aceea nu poate salva fișiere cu nume mai lungi de 260 de caractere sau în căi mai lungi de 248 de caractere. Este mai mult o limitare a codului învechit păstrat de Microsoft în Windows, pentru compatibilitate, decât a sistemului de fișiere pe care probabil îl folosești (NTFS) care suportă nume de fișiere de până la 32.767 caractere.";
                FileIsOpened = "Nu s-a putut salva fișierul.\n\nFișierul pe care încercați să îl salvați este în mod curent în uz într-o altă aplicație. Închideți aplicațiile ce ar putea să îl utilizeze și încercați din nou.\nZona pasionaților: Puteți încerca să eliberați fișierul de orice procese care l-ar putea utiliza folosind utilitarul gratuit Unlocker, disponibil gratuit la http://www.emptyloop.com/unlocker/.";
                ModOperatorError = ": unui operator de tip double îi este atribuită valoarea unei operații mod.";
                RootOperatorError = ": unui operator de tip double îi este atribuită valoarea unei operații ^.";
                SwitchToCode = "Pentru a imprima cod convertit, comutați la un limbaj de programare.\n\nPentru a comuta la un limbaj de programare, faceți clic pe numele acestuia în cadrul meniului Cod.";
                SwitchToImage = "Pentru a salva sau imprima schema logică, trebuie mai întâi să o deschideți în editor.\n\nPentru a face acest lucru, faceți clic sau atingeți meniul Cod, iar apoi alegeți Convertire În Schemă Logică.";
                SucessEndDebugProc = "S-a terminat cu succes procesul de debug.";
                FailEndDebugProc = "Nu s-a putut termina procesul de debug, ar putea fi mort deja.";
                FailedToSendCom = "Nu s-a putut trimite comanda; nicio instanță a debuggerului nu a fost găsită să ruleze pe acest computer.";
                CreateEXEError = "A apărut o eroare la crearea fișierului EXE.\n\nRevizuiți câmpurile și încercați din nou.";
                CompileError = "A apărut o eroare la compilarea pseudocodului dvs.\n\nDescrierea erorii:";
                InstallWindowsLiveMail = "Windows Live Mail nu este instalat.\n\nCând se execută pe sistemul de operare Microsoft(R) Windows XP, Pseudocod necesită ca aplicația gratuită Microsoft Windows Live Mail să fie instalată pe sistem.\n\nDoriți să descărcați și să instalați Windows Live Mail acum? Acesta va ocupa aproximativ 60 MO spațiu-disc din spațiul dvs. de stocare.";
                InstallPowerPointViewer = "Microsoft PowerPoint Viewer nu este instalat.\n\nCând se execută pe sistemul de operare Microsoft(R) Windows XP, Pseudocod necesită ca aplicația gratuită Microsoft PowerPoint Viewer să fie instalată pe sistem.\n\nDoriți să descărcați și să instalați Microsoft PowerPoint Viewer acum? Acesta va ocupa aproximativ 270 MO spațiu-disc din spațiul dvs. de stocare.";
                NewVariableName = "Scrieți noul nume pe care doriți să îl aibă variabila selectată.";
                NewValue = "Scrieți valoarea pe care doriți să o atribuiți variabilei selectate.";
                NotInApp = "NU EXISTĂ ÎN APLICAȚIE";
                Executions = "EXECUTĂRI";
                UnsupportedFile = "Acest fișier nu este suportat.\n\nPentru a deschide acest fișier, Pseudocode are nevoie ca fișierul dicționar particularizat solicitat de către acesta să fie disponibil pe sistem. Instalați acest pachet, apoi încercați din nou deschiderea fișierului.";
                codeExecution = "Codul se execută";
                closeWindow = "Închideți această fereastră";
                executionTime = "Timp de execuție: ";
                seconds = "secunde";
                running = "Se execută: ";
                //NotDefinible = "Nicio potrivire.\n\nVariablia nu este definită sau obiectul asupra căruia ați executat operația este indefinibil.";
            }
            if (Properties.Settings.Default.PseudocodeLang == 0)
            {
                IntegerKeyword = "integer";
                DoubleKeyword = "real";
                CharKeyword = "charcater";
                StringKeyword = "string";
                DivKeyword = "div";
                ModKeyword = "mod";
                DifferentKeyword = "different";
                AndKeyword = "and";
                OrKeyword = "or";
                ReadKeyword = "read";
                WriteKeyword = "write";
                IfKeyword = "if";
                ThenKeyword = "then";
                ElseKeyword = "else";
                EndIfKeyword = "end_if";
                WhileKeyword = "while";
                EndWhileKeyword = "end_while";
                ForKeyword = "for";
                EndForKeyword = "end_for";
                DoKeyword = "do";
                LoopUntilKeyword = "loop_until";
                InputText = "Use the keyboard to type charcaters which will represent the value of the following variable: ";
                YES = "YES";
                NO = "NO";
                PseudocodeLang = 0;
            }
            if (Properties.Settings.Default.PseudocodeLang == 1)
            {
                IntegerKeyword = "intreg";
                DoubleKeyword = "real";
                CharKeyword = "caracter";
                StringKeyword = "sir";
                DivKeyword = "div";
                ModKeyword = "mod";
                DifferentKeyword = "diferit";
                AndKeyword = "si";
                OrKeyword = "sau";
                ReadKeyword = "citeste";
                WriteKeyword = "scrie";
                IfKeyword = "daca";
                ThenKeyword = "atunci";
                ElseKeyword = "altfel";
                EndIfKeyword = "sfarsit_daca";
                WhileKeyword = "cat_timp";
                EndWhileKeyword = "sfarsit_cat_timp";
                ForKeyword = "pentru";
                EndForKeyword = "sfarsit_pentru";
                DoKeyword = "executa";
                LoopUntilKeyword = "pana_cand";
                InputText = "Introduceti de la tastatura caractere. Acestea vor reprezenta continutul variabilei ";
                YES = "DA";
                NO = "NU";
                /*IntegerKeyword = "int";
                DoubleKeyword = "double";
                CharKeyword = "char";
                StringKeyword = "string";
                DivKeyword = "/";
                ModKeyword = "%";
                DifferentKeyword = "!=";
                AndKeyword = "&&";
                OrKeyword = "||";
                ReadKeyword = "cin>>";
                WriteKeyword = "cout<<";
                IfKeyword = "if (";
                ThenKeyword = ")\n{";
                ElseKeyword = "}\nelse\n{";
                EndIfKeyword = "}";
                WhileKeyword = "while (";
                EndWhileKeyword = "}";
                ForKeyword = "for (";
                EndForKeyword = "}";
                DoKeyword = ")\n{";
                LoopUntilKeyword = "}";
                InputText = "Introduceti de la tastatura caractere. Acestea vor reprezenta continutul variabilei ";
                YES = "DA";
                NO = "NU";*/

                PseudocodeLang = 1;
            }
            if (File.Exists(Application.StartupPath + "\\custom-keywords.txt"))
            {
                StreamReader rd = new StreamReader(Application.StartupPath + "\\custom-keywords.txt");
                string[] st = Regex.Split(rd.ReadToEnd(), "\r\n");
                rd.Close();
                IntegerKeyword = st[0];
                DoubleKeyword = st[1];
                CharKeyword = st[2];
                StringKeyword = st[3];
                DivKeyword = st[4];
                ModKeyword = st[5];
                DifferentKeyword = st[6];
                AndKeyword = st[7];
                OrKeyword = st[8];
                ReadKeyword = st[9];
                WriteKeyword = st[10];
                IfKeyword = st[11];
                ThenKeyword = st[12];
                ElseKeyword = st[13];
                EndIfKeyword = st[14];
                WhileKeyword = st[15];
                EndWhileKeyword = st[16];
                ForKeyword = st[17];
                EndForKeyword = st[18];
                DoKeyword = st[19];
                LoopUntilKeyword = st[20];
                InputText = st[21];
                YES = st[22];
                NO = st[23];
                PseudocodeLang = 2;

            }
            str[0] = IntegerKeyword;
            str[1] = DoubleKeyword;
            str[2] = CharKeyword;
            str[3] = StringKeyword;
            str[4] = DivKeyword;
            str[5] = ModKeyword;
            str[6] = DifferentKeyword;
            str[7] = AndKeyword;
            str[8] = OrKeyword;
            str[9] = ReadKeyword;
            str[10] = WriteKeyword;
            str[11] = IfKeyword;
            str[12] = ThenKeyword;
            str[13] = ElseKeyword;
            str[14] = EndIfKeyword;
            str[15] = WhileKeyword;
            str[16] = EndWhileKeyword;
            str[17] = ForKeyword;
            str[18] = EndForKeyword;
            str[19] = DoKeyword;
            str[20] = LoopUntilKeyword;   
            if (Properties.Settings.Default.AppName != "")
            {
                AppName = Properties.Settings.Default.AppName;
            }
            if (Properties.Settings.Default.UpdateInt != 0)
            {
                timer1.Interval = Properties.Settings.Default.UpdateInt;
            }
            /*int style = NativeWinAPI.GetWindowLong(this.Handle, NativeWinAPI.GWL_EXSTYLE);
            style |= NativeWinAPI.WS_EX_COMPOSITED;
            NativeWinAPI.SetWindowLong(this.Handle, NativeWinAPI.GWL_EXSTYLE, style);*/

            /*if (Properties.Settings.Default.UILang == 0)
            {
                HelpBrowser = new WebView(Application.StartupPath + "\\helpcontent\\en-UK\\index.htm", new BrowserSettings());
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                HelpBrowser = new WebView(Application.StartupPath + "\\helpcontent\\ro-RO\\index.htm", new BrowserSettings());
            }*/
            HelpPanel.Visible = true;
            HelpPanel.Tag = "hidden";
            //HelpPanel.Controls.Add(HelpBrowser);
            //HelpBrowser.Dock = DockStyle.Fill;
            //HelpBrowser.InitializeLifetimeService();
            //HelpBrowser.LoadCompleted += HelpBrowser_LoadCompleted;
            HelpPanel.Visible = false;
            //DisableClickSounds();
            /*GlobalMouseHandler gmh = new GlobalMouseHandler();
            gmh.TheMouseMoved += new MouseMovedEvent(gmh_TheMouseMoved);
            Application.AddMessageFilter(gmh);*/

        }

        void pseudocodeEd_MouseMove(object sender, MouseEventArgs e)
        {
            MainForm mf = this.MdiParent as MainForm;
            mf.time = 0;
            mf.Loop.Enabled = true;
        }


        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            //MessageBox.Show("da");
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {


                /*Point p = PointToScreen(e.Location);

                IntPtr callingTaskBarWindow = this.Handle;
                IntPtr wMenu = GetSystemMenu(this.Handle, false);
                // Display the menu
                uint command = TrackPopupMenuEx(wMenu,
                    (uint)(TPMLEFTBUTTON | TPMRETURNCMD), (int)p.X, (int)p.Y, callingTaskBarWindow, IntPtr.Zero);
                if (command == 0)
                    return;

                PostMessage(this.Handle, WM_SYSCOMMAND, new IntPtr(command), IntPtr.Zero);*/
            }
            else
            {
                firstPoint = e.Location;
                mouseIsDown = true;
            }
        }
        ToolTip tt1 = new ToolTip();
        ToolTip tt2 = new ToolTip();
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseIsDown)
            {
                // Get the difference between the two points
                int xDiff = firstPoint.X - e.Location.X;
                int yDiff = firstPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
            //MessageBox.Show(sender.ToString());
            if (e.Location.X > 19 && e.Location.X < 43 && (Panel)sender == panel1 && button56.Enabled == false && _currentToolTipControl == null && TooltipWasShown == false)
            {
                string toolTipString = toolTip1.GetToolTip(button56);
                //MessageBox.Show(toolTipString);
                // trigger the tooltip with no delay and some basic positioning just to give you an idea
                tt1.Show(toolTipString, this, e.Location.X, e.Location.Y + 30);
                _currentToolTipControl = button56;
                TooltipWasShown = true;
            }
            if (e.Location.X > 43 && e.Location.X < 67 && (Panel)sender == panel1 && button55.Enabled == false && _currentToolTipControl == null && TooltipWasShown == false)
            {
                string toolTipString = toolTip1.GetToolTip(button55);
                //MessageBox.Show(toolTipString);
                //MessageBox.Show("da");
                // trigger the tooltip with no delay and some basic positioning just to give you an idea
                tt2.Show(toolTipString, this, e.Location.X, e.Location.Y + 30);
                _currentToolTipControl = button55;
                TooltipWasShown = true;
            }

        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseIsDown = false;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {

        }
        private int GetLineCursorPosition(bool @fixed, ScintillaNET.Scintilla SyntaxHandle)
        {
            return @fixed ? SyntaxHandle.Selection.Start - SyntaxHandle.Lines.Current.IndentPosition + 1 : SyntaxHandle.Selection.Start - SyntaxHandle.Lines.Current.IndentPosition;
        }
        private int CountChars(string what, bool WithSpaces)
        {
            int count = 0;
            foreach (char c in what)
            {
                if (WithSpaces == true)
                {
                    count++;
                }
                else
                {
                    if (char.IsLetter(c))
                    {
                        count++;
                    }
                }
            }
            return count;
        }
        /*bool ShouldDrawBkg = false;
        protected override void OnPaintBackground( System.Windows.Forms.PaintEventArgs pevent) 
        { // Do nothing. 
            if (ShouldDrawBkg == false)
            {
                base.OnPaintBackground(pevent);
                ShouldDrawBkg = true;
            }
        }*/
        private Point VisualBarMouseDownLocation;
        bool TooltipWasShown = false;
        private Control _currentToolTipControl = null;
        public bool flagReident = false;
        public bool flagEnterWasPressed = false;
        bool wasAsked = false;
        public int lastLine = -1;
        string lastText = "";

        public bool updateScroll = false;
        public void Loop()
        {

//            VisualScrollBar.Text = pseudocodeEd.Text;

           // if (BegunScroll == 0)
           // {

           //     BegunScroll = 1;
          //  }
     //       if (BegunScroll == 1)
      //      {
       //         VisualScrollBar.SuspendLayout();
       //         VisualScrollBar.Text = pseudocodeEd.Text;
               // VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
               // VisualScrollBar.Scrolling.ScrollToLine(pseudocodeEd.Lines.Current.Number - (semiTransparentPanel1.Top / 6));
       //     }
       //     semiTransparentPanel1.Top = pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE) * VisualScrollBar.Height / pseudocodeEd.Lines.Count + 6;
       //     VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
      //      VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count - VisualScrollBar.Lines.Current.Number - 1);
            // VisualScrollBar.GoTo.Line(pseudocodeEd.Lines.Current.Number);
            //        VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
            //        VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Current.Number);f

   /*         VisualScrollBar.ResumeLayout();
            semiTransparentPanel1.UpdateUI();

            
            
            int factor = 6;
            int lastVisibleLine = pseudocodeEd.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN);
            semiTransparentPanel1.Height = factor * lastVisibleLine;
            semiTransparentPanel1.UpdateUI();
            VisualScrollBar.Font = pseudocodeEd.Font;
            BegunScroll = 1;*/

            if ((lastLine != pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE)) || lastLine == -1 || updateScroll == true)
            {
                lastLine = pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE);
                VisualScrollBar.Height = VisualScrollBar.Lines.Current.Height * pseudocodeEd.Lines.Count;
                VisualSBPic.Height = VisualScrollBar.Height;
                VisualScrollBar.Text = pseudocodeEd.Text;
                Bitmap bm = new Bitmap(VisualScrollBar.Width, VisualScrollBar.Height);
                VisualScrollBar.DrawToBitmap(bm, VisualScrollBar.ClientRectangle);
                VisualSBPic.Image = bm;
                if (VisualScrollBar.Height > VisualScrollBar.Parent.Height)
                {
                    VisualSBPic.Dock = DockStyle.Fill;
                    VisualSBPic.Height = VisualScrollBar.Parent.Height;
                    VisualSBPic.SizeMode = PictureBoxSizeMode.StretchImage;
                    double x = (double)VisualScrollBar.Parent.Height / pseudocodeEd.Lines.Count * pseudocodeEd.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN);
                    semiTransparentPanel1.Height = (int)Math.Round(x);
                }
                else
                {
                    VisualSBPic.Dock = DockStyle.None;
                    VisualSBPic.Top = 0;
                    VisualSBPic.Height = VisualScrollBar.Height;
                    VisualSBPic.SizeMode = PictureBoxSizeMode.StretchImage;
                    double x = (double)VisualSBPic.Height / pseudocodeEd.Lines.Count * pseudocodeEd.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN);
                    semiTransparentPanel1.Height = (int)Math.Round(x);
                }
                semiTransparentPanel1.BringToFront();
                semiTransparentPanel1.UpdateUI();
                if (VisualSBPic.Dock == DockStyle.Fill)
                {
                    semiTransparentPanel1.Top = (int)Math.Round(pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE) * (double)VisualSBPic.Parent.Height / pseudocodeEd.Lines.Count);
                }
                if (VisualSBPic.Dock == DockStyle.None)
                {
                    semiTransparentPanel1.Top = (int)Math.Round((double)pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE) * VisualScrollBar.Lines.Current.Height);
                }
            }
            if (statusBar1.Visible == true)
            {
                BottomDown.BringToFront();
                BottomDown.Height = 3;
            }
            if (wasAsked == false)
            {
                wasAsked = true;
                if (Environment.OSVersion.Version.Major <= 5)
                {

                    //Application.DoEvents();
                    if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\segoeui.ttf"))
                    {
                        splashScreen.Close();
                        this.splashScreen.Dispose();
                        DialogResult dr = CasetaDeMesaj(this, InstallWindowsLiveMail, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (dr == System.Windows.Forms.DialogResult.Yes)
                        {
                            Process.Start("http://www.microsoft.com/ro-ro/download/details.aspx?id=3945");
                        }

                        Process.GetCurrentProcess().Kill();

                    }
                    if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + "\\consola.ttf"))
                    {
                        splashScreen.Close();
                        this.splashScreen.Dispose();
                        DialogResult dr2 = CasetaDeMesaj(this, InstallPowerPointViewer, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (dr2 == System.Windows.Forms.DialogResult.Yes)
                        {
                            Process.Start("http://www.microsoft.com/ro-ro/download/details.aspx?id=13");
                        }
                        Process.GetCurrentProcess().Kill();

                    }
                    if ((File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\Windows Live\\Mail\\wlmail.exe") || File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\Microsoft Office\\Office14\\PPTVIEW.exe")) && Properties.Settings.Default.WasWLMPPVShown == false)
                    {
                        DependenciesInstalled dp = new DependenciesInstalled();
                        dp.ShowDialog();
                    }
                }

            }

            this.Invalidate();
            //  this.BackColor = Color.White;
            //OKtoActivate = true;
            oldDim = this.Size;
            ReturnInput = false;
            //if (pictureBox5.Visible == true) pictureBox5.BringToFront();
            /*if (HelpBrowser.Address.ToString() == "pseudocode://code-dep/")
            {
                HelpBrowser.Back();
                SendKeys.Send("{F1}");
                Open open = new Open(this);
                open.Tag = "rep";
                open.ShowDialog();

            }
            if (HelpBrowser.Address.ToString() == "pseudocode://mod-dep/")
            {
                HelpBrowser.Back();
                SendKeys.Send("{F1}");
                button21.PerformClick();
                tabControl1.SelectedIndex = 1;
            }

            if (HelpBrowser.IsLoading == false && HelpBrowser.Address.ToString().StartsWith("external://"))
            {
                string address = HelpBrowser.Address.ToString();
                HelpBrowser.Stop();
                HelpBrowser.Back();
                Application.DoEvents();
                DoOnUIThread(delegate()
                {
                    DialogResult dr = CasetaDeMesaj(this, ExternalAddress + address.Substring(11, address.Length - 11), AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {
                        Process.Start("http://" + address.Substring(11, address.Length - 11));
                        HelpBrowser.Back();
                    }
                });
            }*/
            //adaugBara();
            if (HelpPanel.Tag.ToString() == "visible")
            {
                statusBar1.Visible = false;
                //sliderItem1.Visible = false;
                button56.Visible = false;
                button55.Visible = false;
                //this.Text = HelpBrowser.Title.ToString() + " - " + AppName;
            }
            else
            {
                //sliderItem1.Visible = true;
                button56.Visible = true;
                button55.Visible = true;
                try
                {
                    //this.Text = superTabControl1.Tabs[superTabControl1.SelectedTabIndex].Text + " - " + AppName;
                }
                catch { }
            }
            if (Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\HelpHooker"))
            {
                Directory.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\HelpHooker");
                if (FallDownTimer.Enabled == false && RiseUpTimer.Enabled == false)
                {
                    if (HelpPanel.Visible == false)
                    {
                        if (Directory.Exists(Application.StartupPath + "\\com.valinet.pseudocode.help"))
                        {
                            MainForm mf = this.MdiParent as MainForm;
                            string lang2 = "ro";
                            if (Properties.Settings.Default.UILang == 0) lang2 = "en";
                            if (Properties.Settings.Default.UILang == 1) lang2 = "ro";
                            mf.ShowBrowser(Application.StartupPath + "\\com.valinet.pseudocode.help\\" + lang2 + "\\Default\\webframe.html");
                            /*if (mf.hp == null) mf.hp = new Help();
                            mf.hp.Show();
                            if (mf.WindowState == FormWindowState.Normal)
                            {
                                if (Properties.Settings.Default.DisableEnhancedUI == false)
                                {
                                    mf.hp.Location = mf.glow.Location;
                                    mf.hp.Size = new Size(mf.glow.Size.Width, mf.glow.Size.Height + Close.Height + 10);
                                }
                                else
                                {
                                    mf.hp.Location = mf.Location;
                                    mf.hp.Size = new Size(mf.Size.Width, mf.Size.Height);
                                }
                            }
                            if (mf.WindowState == FormWindowState.Maximized) mf.hp.WindowState = FormWindowState.Maximized;
                            else mf.hp.WindowState = FormWindowState.Normal;*/
                        }
                        else
                        {
                            HelpOverview ho = new HelpOverview();
                            ho.ShowDialog();
                        }
                        //Process.Start("http://www.valinet.ro/wiki");
                        //panel1.Height = 20;
                        //panel2.Height = 22;
                        /*if (Properties.Settings.Default.UILang == 1)
                        {
                            if (this.WindowState == FormWindowState.Normal)
                            {
                                HelpPanel.Visible = true;
                                HelpPanel.Tag = "visible";
                                HelpPanel.Left = 1;
                                //HelpPanel.Top = 21;
                                HelpPanel.Width = this.Width - 2;
                                HelpPanel.Height = this.Height - 42;
                                HelpPanel.Top = -HelpPanel.Height;
                                Application.DoEvents();
                                FallDownTimer.Enabled = true;
                            }
                            else
                            {
                                HelpPanel.Visible = true;
                                HelpPanel.Tag = "visible";
                                HelpPanel.Left = 1;
                                HelpPanel.Top = -HelpPanel.Height;
                                HelpPanel.Width = this.Width - 15;
                                HelpPanel.Height = this.Height - 52;
                                Title.BringToFront();
                                panel1.BringToFront();
                                panel2.BringToFront();
                                Application.DoEvents();
                                FallDownTimer.Enabled = true;
                            }
                            HelpPanel.BringToFront();
                            //HelpBrowser.Load(Application.StartupPath + "\\helpcontent\\ro-RO\\index.htm");
                            panel2.BringToFront();
                            HelpBrowser.Focus();
                        }*/
                        //if (Properties.Settings.Default.UILang == 0)
                        //{
                        /*if (this.WindowState == FormWindowState.Normal)
                        {
                            HelpPanel.Visible = true;
                            HelpPanel.Tag = "visible";
                            HelpPanel.Left = 1;
                            //HelpPanel.Top = 21;
                            HelpPanel.Width = this.Width - 2;
                            HelpPanel.Height = this.Height - 42;
                            HelpPanel.Top = -HelpPanel.Height;
                            Application.DoEvents();
                            FallDownTimer.Enabled = true;
                        }
                        else
                        {
                            HelpPanel.Visible = true;
                            HelpPanel.Tag = "visible";
                            HelpPanel.Left = 1;
                            HelpPanel.Top = -HelpPanel.Height;
                            HelpPanel.Width = this.Width - 15;
                            HelpPanel.Height = this.Height - 52;
                            Title.BringToFront();
                            panel1.BringToFront();
                            panel2.BringToFront();
                            Application.DoEvents();
                            FallDownTimer.Enabled = true;
                        }*/
                        HelpPanel.BringToFront();
                        //HelpBrowser.Load(Application.StartupPath + "\\helpcontent\\ro-RO\\index.htm");
                        panel2.BringToFront();
                        //HelpBrowser.Focus();
                        //}
                    }
                    else
                    {
                        //RiseUpTimer.Enabled = true;
                        //HelpPanel.Visible = false;
                        Process.Start("http://www.valinet.ro/wiki");

                    }
                }
            }
            //if (Properties.Settings.Default.UILang == 1) helpProvider1.HelpNamespace = "http://www.valinet.ro/ro-help-pseudocode";
            converterEd.Font = pseudocodeEd.Font;
            try
            {
                if (pseudocodeEd.ZoomFactor != 0)
                {
                   // pseudocodeEd.Margins[0].Width = pseudocodeEd.ZoomFactor + 30;
                   // converterEd.Margins[0].Width = converterEd.ZoomFactor + 30;
                }
                else
                {
                   // pseudocodeEd.Margins[0].Width = 20;
                   // converterEd.Margins[0].Width = 20;
                }
                int lines = pseudocodeEd.Lines.Count;
                if (lines != last_measure_lines)
                {
                    last_measure_lines = lines;
                    pseudocodeEd.Margins[0].Width = TextRenderer.MeasureText(lines.ToString(), pseudocodeEd.Font).Width;
                 //   converterEd.Margins[0].Width = TextRenderer.MeasureText(lines.ToString(), converterEd.Font).Width;
                }
                button13.Enabled = pseudocodeEd.Clipboard.CanCut;
                button14.Enabled = pseudocodeEd.Clipboard.CanCopy;
                button12.Enabled = pseudocodeEd.Clipboard.CanPaste;
                if (pseudocodeEd.Selection.Start != pseudocodeEd.Selection.End) button17.Enabled = true;
                else button17.Enabled = false;
                if (pseudocodeEd.Text == "") button18.Enabled = false;
                else button18.Enabled = true;
                if (pseudocodeEd != null) HighlightSelectedWord(pseudocodeEd);
                Control control = GetChildAtPoint(Cursor.Position);
                if (control != panel1 && TooltipWasShown == true)
                {
                    tt1.Hide(_currentToolTipControl);
                    tt2.Hide(_currentToolTipControl);
                    _currentToolTipControl = null;
                    TooltipWasShown = false;
                    toolTip1.Active = false;
                    toolTip1.Active = true;
                }
                if (pseudocodeEd.AutoComplete.IsActive == true)
                {
                    pseudocodeEd.AcceptsReturn = false;
                }
                else
                {
                    pseudocodeEd.AcceptsReturn = true;
                }
                if (pseudocodeEd.UndoRedo.CanUndo == true)
                {
                    button56.Enabled = true;
                    toolTip1.SetToolTip(button56, "");
                }
                else
                {
                    button56.Enabled = false;
                    toolTip1.SetToolTip(button56, CantUndo);
                }
                if (pseudocodeEd.UndoRedo.CanRedo == true)
                {
                    button55.Enabled = true;
                    toolTip1.SetToolTip(button55, "");
                }
                else
                {
                    button55.Enabled = false;
                    toolTip1.SetToolTip(button55, CantRedo);
                }
            }
            catch { }
            //shadowPanel1.BackColor = Color.White;
            //automaticUpdater1.BackColor = Color.FromArgb(106, 56, 137);
            //if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            //if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            if (UserPanel.Visible == true)
            {
                if (this.WindowState == FormWindowState.Normal)
                {
                    UserPanel.Width = label27.Width + 60;
                    UserPanel.Left = this.Width - UserPanel.Width;
                    pictureBox2.Left = label27.Width + 25;
                    //automaticUpdater1.Top = 3;

                }
                if (this.WindowState == FormWindowState.Maximized)
                {
                    UserPanel.Width = label27.Width + 80;
                    UserPanel.Left = this.Width - UserPanel.Width;
                    pictureBox2.Left = label27.Width + 30;
                }
            }
            try
            {
                ExTag exTag = (ExTag)pseudocodeEd.Tag;
                bool saved = (bool)exTag.Get("issaved");
                string savedtext = "";
                if (saved == true)
                {
                    if (Properties.Settings.Default.UILang == 0) savedtext = "FILE SAVED BY USER";
                    if (Properties.Settings.Default.UILang == 1) savedtext = "FIȘIER SALVAT DE UTILIZATOR";
                }
                else
                {
                    if (Properties.Settings.Default.UILang == 0) savedtext = "FILE NOT YET SAVED";
                    if (Properties.Settings.Default.UILang == 1) savedtext = "FIȘIER NESALVAT ÎNCĂ";
                }
                if (Properties.Settings.Default.UILang == 0) buttonItem1.Text = savedtext + " | " + " Ln: " + (pseudocodeEd.Lines.Current.Number + 1).ToString() + " | " + "Pos: " + GetLineCursorPosition(true, pseudocodeEd) + " | " + "No. of Ln: " + pseudocodeEd.Lines.Count + " | " + "No. of Words: " + WordCounting.CountWords1(pseudocodeEd.Text).ToString() + " | " + "No. of Chars (without spaces): " + CountChars(pseudocodeEd.Text, false) + " | " + "No. of Chars (with spaces): " + CountChars(pseudocodeEd.Text, true) + " | " + "No. of lines: " + pseudocodeEd.Lines.Count + " | " + "No. of commented lines: " + CountCommentLines(pseudocodeEd.Text); //" | " + "No. of Words (Loop): " + WordCounting.CountWords2(pseudocodeEd.Text).ToString() + 
                if (Properties.Settings.Default.UILang == 1) buttonItem1.Text = savedtext + " | " + " Ln: " + (pseudocodeEd.Lines.Current.Number + 1).ToString() + " | " + "Pos: " + GetLineCursorPosition(true, pseudocodeEd) + " | " + "Nr. de Ln: " + pseudocodeEd.Lines.Count + " | " + "Nr. de Cuvinte: " + WordCounting.CountWords1(pseudocodeEd.Text).ToString() + " | " + "Nr. de Caractere (fără spații): " + CountChars(pseudocodeEd.Text, false) + " | " + "Nr. de Caractere (cu spații): " + CountChars(pseudocodeEd.Text, true) + " | " + "Nr. de linii: " + pseudocodeEd.Lines.Count + " | " + "Din care nr. de linii comentate: " + CountCommentLines(pseudocodeEd.Text); //" | " + "Nr. de Cuvinte (Loop): " + WordCounting.CountWords2(pseudocodeEd.Text).ToString() + 
                if (HelpPanel.Tag.ToString() == "hidden")
                {
                    buttonItem2.Text = "LN: " + (pseudocodeEd.Lines.Current.Number + 1).ToString() + ", " + "POS: " + GetLineCursorPosition(true, pseudocodeEd);
                    if (lista_executii != null && lista_executii.Count != 0)
                    {
                        int a = corespA[pseudocodeEd.Lines.Current.Number];
                        //this.Text = (a + 6).ToString();
                        string b = "";
                        if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(IfKeyword) ||
                            pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(ElseKeyword) ||
                            pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(ForKeyword) ||
                            pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(WhileKeyword) ||
                            pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(DoKeyword)) b = lista_executii[a + (int)nr_linii + 6][0];
                        else b = lista_executii[a + (int)nr_linii + 6 + 1][0];
                        if (b == "#####") b = "0";
                        if (b == "-") b = NotInApp;
                        buttonItem2.Text += ", " + Executions + ": " + b; 
                    }
                }
                else
                {
                    buttonItem2.Text = DevTools;
                }
                // buttonItem1.Refresh();
                try
                {
                    //  metroStatusBar1.Refresh();
                }
                catch { }
            }
            catch { }
            if (UserPanel.Visible == true)
            {
                //superTabControl1.Width = this.Width - UserPanel.Width;
                //superTabControl1.Anchor = AnchorStyles.None;
                //superTabControl1.Width = this.Width - UserPanel.Width;
            }
            label27.ForeColor = Color.White;
            pictureBox2.BackColor = Color.FromArgb(106, 56, 137);
            Title.Text = this.Text;
            TitleMax.Text = Title.Text;
            UserPanel.SendToBack();
            UserPanel.BackColor = Color.FromArgb(106, 56, 137);
            //powerLabel1.ForeColor = Color.White;
            label4.Text = "Pseudocode Int'l Pre-Release 2.0.1000\nLocation: " + this.Location.ToString() + "\nSize: " + this.Size.ToString();
            Close.BackColor = Color.FromArgb(199, 80, 80);
            //Maximize.BackColor = Color.FromArgb(0, 0, 0, 0);
            panel1.BackColor = Color.White;
            pictureBox1.BackColor = Color.White;
            panel2.BackColor = Color.White;
            panel3.BackColor = Color.White;
            panel7.BackColor = Color.White;
            //   statusBar1.BringToFront();
            /*   E.BringToFront();
               V.BringToFront();
               VS.BringToFront();
               S.BringToFront();
               V2.BringToFront();
               E2.BringToFront();*/
            //this.BackColor = Color.FromArgb(211, 211, 211);
            try
            {
                //superTabControl1.Tabs[superTabControl1.SelectedTabIndex].Tag = superTabControl1.SelectedTabIndex;
            }
            catch
            {
                New();
            }
            //  if (panel3.HorizontalScroll.Visible == true)
            //  {
            //  VInt.Visible = false;
            //  EInt.Visible = false;
            //  }
            //  else
            //  {
            //      VInt.Visible = true;
            //       EInt.Visible = true;
            //   }
            if (this.WindowState == FormWindowState.Maximized) TitleMax.Visible = true;
            else TitleMax.Visible = false;
            /*pseudocodeEd.Width = this.Width / 2;
            pseudocodeEd.Height = this.Height - 181;
            converterEd.Location = new Point(this.Width / 2, converterEd.Location.Y);
            converterEd.Width = this.Width / 2;
            converterEd.Height = this.Height - 181;*/
            /* }
             catch{
                
             }*/
            //line1.ForeColor = Color.FromArgb(106, 56, 137);
            //line2.ForeColor = Color.FromArgb(106, 56, 137);
            if (this.WindowState == FormWindowState.Maximized)
            {
                //line1.Visible = false;
                //line2.Visible = false;
                Restore.Visible = true;
                Maximize.Visible = false;
                panel2.Top = 4;
                CloseLayer.Visible = true;
                panel3.Top = 5;
                /* NW.Visible = false;
                 N.Visible = false;
                 NE.Visible = false;
                 E.Visible = false;
                 E2.Visible = false;
                 EInt.Visible = false;
                 S.Visible = false;
                 VS.Visible = false;
                 V.Visible = false;
                 VInt.Visible = false;
                 V2.Visible = false;*/
            }
            else
            {
                panel1.Visible = true;
                Maximize.Visible = true;
                RestoreBlack.Visible = false;
                panel2.Top = -2;
                CloseLayer.Visible = false;
                Title.Top = 1;
                Title.Visible = true;
                pictureBox1.Visible = true;
                /* NW.Visible = true;
                 N.Visible = true;
                 NE.Visible = true;
                 E.Visible = true;
                 E2.Visible = true;
                 EInt.Visible = true;
                 S.Visible = true;
                 VS.Visible = true;
                 V.Visible = true;
                 VInt.Visible = true;
                 V2.Visible = true;*/
                /*    line1.Visible = true;
                    line2.Visible = true;
                    line1.BringToFront();
                    line2.BringToFront();*/
            }
            try
            {


                      //      converterEd.Scrolling.ScrollBars = ScrollBars.None ;
               // if (!(Properties.Settings.Default.ShowOldVBScript == false && lang == "VBSCRIPT"))
               // {
                    if (lastText != pseudocodeEd.Text)
                    {
                        if (Properties.Settings.Default.ShowOldVBScript == false && lang == "VBSCRIPT") converterEd.Text = Compile(pseudocodeEd.Text, "CPP");
                        else converterEd.Text = Compile(pseudocodeEd.Text, lang);
                        lastText = pseudocodeEd.Text;
                    }
             //   }
                if (lang == "VB6")
                {
                    int a = coresp[pseudocodeEd.Lines.Current.Number];
                    converterEd.GoTo.Line(a - 1);
                    converterEd.Lines[a - 1].Select();
                    //converterEd.GoTo.Line(pseudocodeEd.Caret.LineNumber);
                    //converterEd.Lines[pseudocodeEd.Caret.LineNumber].Select();
                }
                if (lang == "C#")
                {
                    int a = coresp[pseudocodeEd.Lines.Current.Number];
                    converterEd.GoTo.Line(a + 8);
                    converterEd.Lines[a + 8].Select();

             /*       int a = coresp[pseudocodeEd.Lines.Current.Number];
                    int dif = pseudocodeEd.Lines.Current.Number - pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE);
                    converterEd.GoTo.Line(converterEd.Lines.Count);
                    converterEd.GoTo.Line(converterEd.Lines.Current.Number - dif);
                    converterEd.Lines[converterEd.Lines.Current.Number - dif].Select();
                    converterEd.GoTo.Line(a + 8);
                    converterEd.Lines[a + 8].Select();
                    converterEd.Scrolling.ScrollToLine(converterEd.Lines.Current.Number - dif);
                    converterEd.Lines[a + 8].Select();*/
                }
                if (lang == "CPP")
                {
                    int a = coresp[pseudocodeEd.Lines.Current.Number];
                    converterEd.GoTo.Line(a + (int)nr_linii + 6);
                    converterEd.Lines[a + (int)nr_linii + 6].Select();
                 //   this.Text = (a + (int)nr_linii + 6).ToString();
                   /* int a = coresp[pseudocodeEd.Lines.Current.Number];
                    int dif = pseudocodeEd.Lines.Current.Number - pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE);
                    converterEd.GoTo.Line(converterEd.Lines.Count);
                    converterEd.GoTo.Line(converterEd.Lines.Current.Number - dif);
                    converterEd.Lines[converterEd.Lines.Current.Number - dif].Select();
                    converterEd.GoTo.Line(a + (int)nr_linii + 6);
                    converterEd.Lines[a + (int)nr_linii + 6].Select();
                    converterEd.Scrolling.ScrollToLine(converterEd.Lines.Current.Number - dif);
                    converterEd.Lines[a + (int)nr_linii + 6].Select();*/
                }
                if (lang == "VBSCRIPT")
                {
                    if (Properties.Settings.Default.ShowOldVBScript == false) CreateMap();
                    else
                    {
                        int a = coresp[pseudocodeEd.Lines.Current.Number];
                        converterEd.GoTo.Line(a - (int)nr_linii + 1);
                        converterEd.Lines[a - (int)nr_linii + 1].Select();
                    }
                }
                if (lang == "JAVASCRIPT")
                {
                    int a = coresp[pseudocodeEd.Lines.Current.Number];
                    if (a == 0)
                    {
                        converterEd.GoTo.Line(3);
                        converterEd.Lines[3].Select();
                    }
                    else
                    {
                        converterEd.GoTo.Line(a + 2);
                        converterEd.Lines[a + 2].Select();
                    }
                }
                if (lang == "PAS")
                {
                    int a = coresp[pseudocodeEd.Lines.Current.Number];
                    if (estevar[pseudocodeEd.Lines.Current.Number] == true)
                    {
                        if (isint && isreal)
                        {
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(IntegerKeyword))
                            {
                                converterEd.GoTo.Line(3);
                                converterEd.Lines[3].Select();
                            }
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(DoubleKeyword))
                            {
                                converterEd.GoTo.Line(4);
                                converterEd.Lines[4].Select();
                            }
                        }
                        else if (isint && isstr)
                        {
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(IntegerKeyword))
                            {
                                converterEd.GoTo.Line(3);
                                converterEd.Lines[3].Select();
                            }
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(StringKeyword))
                            {
                                converterEd.GoTo.Line(4);
                                converterEd.Lines[4].Select();
                            }
                        }
                        else if (isreal && isstr)
                        {
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(StringKeyword))
                            {
                                converterEd.GoTo.Line(3);
                                converterEd.Lines[3].Select();
                            }
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(DoubleKeyword))
                            {
                                converterEd.GoTo.Line(4);
                                converterEd.Lines[4].Select();
                            }
                        }
                        else if (isint && isreal && isstr)
                        {
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(IntegerKeyword))
                            {
                                converterEd.GoTo.Line(3);
                                converterEd.Lines[3].Select();
                            }
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(DoubleKeyword))
                            {
                                converterEd.GoTo.Line(4);
                                converterEd.Lines[4].Select();
                            }
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(StringKeyword))
                            {
                                converterEd.GoTo.Line(5);
                                converterEd.Lines[5].Select();
                            }
                        }
                        else if (isint && !isreal && !isstr)
                        {
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(IntegerKeyword))
                            {
                                converterEd.GoTo.Line(3);
                                converterEd.Lines[3].Select();
                            }
                        }
                        else if (isreal && !isint && !isstr)
                        {
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(IntegerKeyword))
                            {
                                converterEd.GoTo.Line(3);
                                converterEd.Lines[3].Select();
                            }
                        }
                        else if (isstr && !isreal && !isint)
                        {
                            if (pseudocodeEd.Lines.Current.Text.TrimStart().StartsWith(IntegerKeyword))
                            {
                                converterEd.GoTo.Line(3);
                                converterEd.Lines[3].Select();
                            }
                        }
                        /*if (isint | isreal | isstr)
                        {
                            converterEd.GoTo.Line(0);
                            converterEd.Lines[0].Select();
                        }
                        if (isint && isreal)
                        {
                            converterEd.GoTo.Line(a - 1);
                            converterEd.Lines[a - 1].Select();
                        }
                        if (isint && isstr)
                        {
                            if (a == 1)
                            {
                                converterEd.GoTo.Line(a - 1);
                                converterEd.Lines[a - 1].Select();
                            }
                            if (a == 3)
                            {
                                converterEd.GoTo.Line(a - 2);
                                converterEd.Lines[a - 2].Select();
                            }
                        }
                        if (isreal && isstr)
                        {
                            if (a == 2)
                            {
                                converterEd.GoTo.Line(a - 2);
                                converterEd.Lines[a - 2].Select();
                            }
                            if (a == 3)
                            {
                                converterEd.GoTo.Line(a - 1);
                                converterEd.Lines[a - 1].Select();
                            }
                        }
                        if (isint && isreal && isstr)
                        {
                            converterEd.GoTo.Line(a - 1);
                            converterEd.Lines[a - 1].Select();
                        }*/
                    }
                    else
                    {
                        converterEd.GoTo.Line(a + numarvariabilepas2 + 4);
                        converterEd.Lines[a + numarvariabilepas2 + 4].Select();
                    }
                        /*if (pseudocodeEd.Lines.Current.Text.StartsWith("intreg"))
                        {
                            converterEd.GoTo.Line(3);
                            converterEd.Lines[2].Select();
                        }
                        if (pseudocodeEd.Lines.Current.Text.StartsWith("real"))
                        {
                            converterEd.GoTo.Line(4);
                            converterEd.Lines[3].Select();
                        }
                        if (pseudocodeEd.Lines.Current.Text.StartsWith("sir"))
                        {
                            converterEd.GoTo.Line(5);
                            converterEd.Lines[4].Select();
                        }*/
                        //if (numarvariabilepas == 1)
                        //{
                        /*if (isint | isreal | isstr)
                        {
                            converterEd.GoTo.Line(0);
                            converterEd.Lines[0].Select();
                        }
                        if (isint && isreal)
                        {
                            converterEd.GoTo.Line(a + 1);
                            converterEd.Lines[a + 1].Select();
                        }
                        if (isint && isstr)
                        {
                            if (a == 1)
                            {
                                converterEd.GoTo.Line(a + 1);
                                converterEd.Lines[a + 1].Select();
                            }
                            if (a == 3)
                            {
                                converterEd.GoTo.Line(a );
                                converterEd.Lines[a ].Select();
                            }
                        }
                        if (isreal && isstr)
                        {
                            if (a == 2)
                            {
                                converterEd.GoTo.Line(a );
                                converterEd.Lines[a ].Select();
                            }
                            if (a == 3)
                            {
                                converterEd.GoTo.Line(a + 1);
                                converterEd.Lines[a + 1].Select();
                            }
                        }
                        if (isint && isreal && isstr)
                        {
                            converterEd.GoTo.Line(a + 1);
                            converterEd.Lines[a + 1].Select();
                        }
                        //Debug.Write(isint.ToString() + "\n" + isreal.ToString() + "\n" + isstr.ToString());
                        /*else
                        {
                            if (isint && isreal && isstr)
                            {
                                converterEd.GoTo.Line(a - 1);
                                converterEd.Lines[a - 1].Select();
                            }
                            else
                            {
                                converterEd.GoTo.Line(0);
                                converterEd.Lines[0].Select();
                            }
                        }*/
                        //    converterEd.GoTo.Line(a  - 2);
                        //    converterEd.Lines[a  - 2].Select();
                   /* }
                    else
                    {
                        converterEd.GoTo.Line(a + numarvariabilepas + 1);
                        converterEd.Lines[a + numarvariabilepas + 1].Select();
                    }*/
                    //System.Diagnostics.Debug.Write(nr_linii);
                }
                timer = timer + 1;
                if (timer == autosaveint)
                {
                    AutoSalvare();
                    timer = 0;
                }
            }
            catch { }

           // if (Environment.OSVersion.Version.Major <= 5) line2.Visible = false;
            GC.Collect();
            EmptyWorkingSet(Process.GetCurrentProcess().Handle);

        }
        public int CountCommentLines(string text)
        {
            int ret = 0;
            string[] split = text.Split('\n');
            foreach (string st in split)
            {
                if (st.TrimStart().StartsWith("//")) ret++;
            }
            return ret;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            //DEPRECATED, USE LOOP() INSTEAD
        }
        [DllImport("psapi.dll")]
        static extern int EmptyWorkingSet(IntPtr hwProc);
        private void Maximize_MouseEnter(object sender, EventArgs e)
        {
            Maximize.BackgroundImage = MaximizeWhite.BackgroundImage;
        }

        private void Maximize_MouseLeave(object sender, EventArgs e)
        {
            Maximize.BackgroundImage = MaximizeBlack.BackgroundImage;
        }

        private void Minimize_MouseEnter(object sender, EventArgs e)
        {
            Minimize.BackgroundImage = MinimizeWhite.BackgroundImage;
        }

        private void Minimize_MouseLeave(object sender, EventArgs e)
        {
            Minimize.BackgroundImage = MinimizeBlack.BackgroundImage;
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Maximize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        /*int originalExStyle = -1;
        bool enableFormLevelDoubleBuffering = true;

        protected override CreateParams CreateParams
        {
            get
            {
                if (originalExStyle == -1)
                    originalExStyle = base.CreateParams.ExStyle;

                CreateParams cp = base.CreateParams;
                if (enableFormLevelDoubleBuffering)
                    cp.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED
                else
                    cp.ExStyle = originalExStyle;

                return cp;
            }
        } */
        public static void SetDoubleBuffered(System.Windows.Forms.Control c)
        {
            //Taxes: Remote Desktop Connection and painting
            //http://blogs.msdn.com/oldnewthing/archive/2006/01/03/508694.aspx
            if (System.Windows.Forms.SystemInformation.TerminalServerSession)
                return;

            System.Reflection.PropertyInfo aProp =
                  typeof(System.Windows.Forms.Control).GetProperty(
                        "DoubleBuffered",
                        System.Reflection.BindingFlags.NonPublic |
                        System.Reflection.BindingFlags.Instance);

            aProp.SetValue(c, true, null);
        }
        Size oldDim;
        public void Form1_Resize(object sender, EventArgs e)
        {
            lastLine = -1;
            BottomDown.Width = this.Width;
            //OPTION ONE
            //  this.TopMost = true;
            //if (this.WindowState == FormWindowState.Maximized)
            //{

            //      wpfwindow.Visibility = System.Windows.Visibility.Hidden;
            //}
            //  if (oldDim.Height > this.Height || oldDim.Width > this.Width)
            //  wpfwindow.Hide();
            //  if (ResizeTimer.Enabled == false) DoResize = 0;
            //  Redim();
            //  ResizeTimer.Enabled = true;
            //OPTION TWO
            //Redim();
        }
        public void RedimLite()
        {
            /* if (this.WindowState != FormWindowState.Maximized)
             {
                 wpfwindow.Height = this.Height + 20;
                 wpfwindow.Width = this.Width + 20;
                 wpfwindow.Left = this.Left - 10;
                 wpfwindow.Top = this.Top - 10;
             }
             else
             {
                 wpfwindow.Visibility = System.Windows.Visibility.Hidden;
             }*/
        }
        public void Redim()
        {
            this.Invalidate();
            //Color culoare;
            //culoare = this.TransparencyKey;
            //this.TransparencyKey = Color.Black;

            // metroShell1.Visible = false;
            /*if (this.WindowState == FormWindowState.Maximized)
            {
                metroShell1.Dock = DockStyle.None;
                metroShell1.Top = TitleMax.Top;
                metroShell1.Width = this.Width;
                TitleMax.Height = 30;
            }
            else
            {
                metroShell1.Dock = DockStyle.Top;
            }*/

            try
            {
                if (HelpPanel.Tag.ToString() == "visible")
                {
                    if (this.WindowState == FormWindowState.Normal)
                    {
                        HelpPanel.Visible = true;
                        HelpPanel.Tag = "visible";
                        HelpPanel.Left = 1;
                        HelpPanel.Top = 21;
                        HelpPanel.Width = this.Width - 2;
                        HelpPanel.Height = this.Height - 42;
                        /*  NW.BringToFront();
                          NE.BringToFront();
                          N.BringToFront();*/
                    }
                    else
                    {
                        HelpPanel.Visible = true;
                        HelpPanel.Tag = "visible";
                        HelpPanel.Left = 1;
                        HelpPanel.Top = 26;
                        HelpPanel.Width = this.Width - 15;
                        HelpPanel.Height = this.Height - 52;
                        Title.BringToFront();
                        panel1.BringToFront();
                        panel2.BringToFront();
                        /*      NW.BringToFront();
                              NE.BringToFront();
                              N.BringToFront();*/
                    }
                }
                if (UserPanel.Visible == true)
                {
                    //superTabControl1.Width = this.Width - UserPanel.Width;
                }
                else
                {
                    //superTabControl1.Width = this.Width;
                }
                if (panel9.Focused == true)
                {
                    panel9.Visible = false;
                    button2.BackColor = Color.White;
                }
                if (panel10.Focused == true)
                {
                    panel10.Visible = false;
                    button6.BackColor = Color.White;
                }
                if (panel11.Focused == true)
                {
                    panel11.Visible = false;
                    button9.BackColor = Color.White;
                }
                if (panel12.Focused == true)
                {
                    panel12.Visible = false;
                    button48.BackColor = Color.White;
                    label31.BackColor = Color.White;
                }
                if (docPanel.Visible == false)
                {
                    if (this.WindowState == FormWindowState.Normal)
                    {
                        tabControl3.Location = new Point(1, this.Height - 173);
                        tabControl3.Size = new Size(this.Width - 2, 150);
                    }
                    else
                    {
                        tabControl3.Location = new Point(1, this.Height - 180);
                        tabControl3.Size = new Size(this.Width - 13 - 2, 150);
                    }
                    try
                    {
                        foreach (ScintillaNET.Scintilla sc in pseudocode)
                        {
                            sc.Width = this.Width / 2;
                            if (this.WindowState == FormWindowState.Normal) sc.Height = this.Height - 171 - 150;
                            if (this.WindowState == FormWindowState.Maximized) sc.Height = this.Height - 178 - 150;
                        }
                    }
                    catch { }
                    try
                    {
                        foreach (ScintillaNET.Scintilla sch in converter)
                        {
                            sch.Location = new Point(this.Width / 2, converterEd.Location.Y);
                            if (this.WindowState == FormWindowState.Normal) sch.Width = this.Width / 2 - 1;
                            else sch.Width = this.Width / 2 - 15;
                            if (this.WindowState == FormWindowState.Normal) sch.Height = this.Height - 171 - 150;
                            if (this.WindowState == FormWindowState.Maximized) sch.Height = this.Height - 178 - 150;
                        }
                    }
                    catch { }
                }
                else
                {
                    if (this.WindowState == FormWindowState.Normal)
                    {
                        tabControl3.Location = new Point(docPanel.Width, this.Height - 173);
                        tabControl3.Size = new Size(this.Width - docPanel.Width - 2, 150);
                    }
                    else
                    {
                        tabControl3.Location = new Point(docPanel.Width, this.Height - 180);
                        tabControl3.Size = new Size(this.Width - docPanel.Width - 13 - 2, 150);
                    }
                    try
                    {
                        foreach (ScintillaNET.Scintilla sc in pseudocode)
                        {
                            sc.Width = this.Width / 2 - docPanel.Width / 2;
                            sc.Location = new Point(docPanel.Width, sc.Location.Y);
                            if (this.WindowState == FormWindowState.Normal) sc.Height = this.Height - 171 - 150;
                            if (this.WindowState == FormWindowState.Maximized) sc.Height = this.Height - 178 - 150;
                        }
                    }
                    catch { }
                    try
                    {
                        foreach (ScintillaNET.Scintilla sch in converter)
                        {
                            sch.Location = new Point(this.Width / 2 + docPanel.Width / 2, sch.Location.Y);
                            if (this.WindowState == FormWindowState.Normal) sch.Width = this.Width / 2 - 1 - docPanel.Width / 2;
                            else sch.Width = this.Width / 2 - 15 - docPanel.Width / 2;
                            if (this.WindowState == FormWindowState.Normal) sch.Height = this.Height - 171 - 150;
                            if (this.WindowState == FormWindowState.Maximized) sch.Height = this.Height - 178 - 150;
                        }
                    }
                    catch { }
                }
                SchemaLogica.Size = converterEd.Size;
                SchemaLogica.Location = converterEd.Location;
                if (this.WindowState == FormWindowState.Maximized)
                {
                    Title.ForeColor = Color.White;
                    panel3.Anchor = AnchorStyles.None;
                    panel3.Width = 920;
                    panel3.Location = new Point(this.Width / 2 - panel3.Width / 2 - 50, 5);
                    if (panel3.Location.X <= 70) panel3.Left = this.Width / 2 - panel3.Width / 2 - 55;
                    //panel8.Visible = false;
                    panel1.Visible = true;
                    panel3.Height = 80;
                    panel1.Top = 5;
                    Title.Top = 5;
                    if (panel3.Location.X <= 70)
                    {
                        panel1.Location = new Point(2, 85);
                        //automaticUpdater1.Top = 85;
                        //automaticUpdater1.Left = this.Width + 2 - automaticUpdater1.Width;
                    }
                    else
                    {
                        panel1.Location = new Point(2, 6);
                        //automaticUpdater1.Top = 8;
                        //automaticUpdater1.Left = this.Width - automaticUpdater1.Width - 100;
                    }
                }
                else
                {
                    Title.ForeColor = Color.Black;
                    panel1.Location = new Point(2, 0);
                    //automaticUpdater1.Top = 3;
                    //automaticUpdater1.Left = this.Width - automaticUpdater1.Width - 80;

                    //automaticUpdater1.Left = this.Width - 148;
                    panel1.Top = 1;
                    panel3.Height = 92;
                    //if (panel3.Anchor == AnchorStyles.Top)
                    //{
                    if (this.Width <= 860)
                    {
                        //panel3.Anchor = AnchorStyles.Left & AnchorStyles.Top & AnchorStyles.Right;
                        panel3.Location = new Point(0, 30);
                        panel3.Width = this.Width;
                        panel3.Anchor = AnchorStyles.Top;
                    }
                    else
                    {
                        //panel3.Anchor = AnchorStyles.Top;

                        //panel3.Anchor = AnchorStyles.None;
                        panel3.Width = 920;
                        panel3.Top = 22;
                        panel3.Anchor = AnchorStyles.Top;
                        //panel3.Width = this.Width;
                        panel3.Location = new Point(this.Width / 2 - panel3.Width / 2, 30);
                        //panel8.Visible = true;
                    }
                    //}
                }
                if (UserPanel.Visible == true)
                {
                    //superTabControl1.Width = this.Width - UserPanel.Width;
                    //superTabControl1.Anchor = AnchorStyles.None;
                    //superTabControl1.Width = this.Width - UserPanel.Width;
                }
                label27.ForeColor = Color.White;
                pictureBox2.BackColor = Color.FromArgb(106, 56, 137);
                Title.Text = this.Text;
                TitleMax.Text = Title.Text;
                UserPanel.SendToBack();
                UserPanel.BackColor = Color.FromArgb(106, 56, 137);
                //powerLabel1.ForeColor = Color.White;
                label4.Text = "Pseudocode Int'l Pre-Release 2.0.1000\nLocation: " + this.Location.ToString() + "\nSize: " + this.Size.ToString();
                Close.BackColor = Color.FromArgb(199, 80, 80);
                //Maximize.BackColor = Color.FromArgb(0, 0, 0, 0);
                panel1.BackColor = Color.White;
                pictureBox1.BackColor = Color.White;
                panel2.BackColor = Color.White;
                panel3.BackColor = Color.White;
                panel7.BackColor = Color.White;
                //     statusBar1.BringToFront();

                /* E.BringToFront();
                 V.BringToFront();
                 VS.BringToFront();
                 S.BringToFront();
                 V2.BringToFront();
                 E2.BringToFront();*/
                //this.BackColor = Color.FromArgb(211, 211, 211);
                //if (panel3.HorizontalScroll.Visible == true)
                // {
                //  VInt.Visible = false;
                //  EInt.Visible = false;
                //  }
                //  else
                //  {
                //       VInt.Visible = true;
                //       EInt.Visible = true;
                //   }
                if (this.WindowState == FormWindowState.Maximized) TitleMax.Visible = true;
                else TitleMax.Visible = false;


                if (this.WindowState == FormWindowState.Maximized)
                {
                  //  line1.Visible = false;
                   // line2.Visible = false;
                    Restore.Visible = true;
                    Maximize.Visible = false;
                    panel2.Top = 4;
                    CloseLayer.Visible = true;
                    panel3.Top = 5;
                    /*   NW.Visible = false;
                       N.Visible = false;
                       NE.Visible = false;
                       E.Visible = false;
                       S.Visible = false;
                       VS.Visible = false;
                       V.Visible = false;*/
                }
                else
                {
                    panel1.Visible = true;
                    Maximize.Visible = true;
                    RestoreBlack.Visible = false;
                    panel2.Top = -2;
                    CloseLayer.Visible = false;
                    Title.Top = 1;
                    Title.Visible = true;
                    pictureBox1.Visible = true;
                    /*         NW.Visible = true;
                             N.Visible = true;
                             NE.Visible = true;
                             E.Visible = true;
                             S.Visible = true;
                             VS.Visible = true;
                             V.Visible = true;*/
                   /* line1.Visible = true;
                    line2.Visible = true;
                    line1.BringToFront();
                    line2.BringToFront();*/
                }
                pseudocodeEd.Focus();
            }
            catch { }
            /*if (this.WindowState != FormWindowState.Maximized)
            {
                wpfwindow.Height = this.Height + 20;
                wpfwindow.Width = this.Width + 20;
                wpfwindow.Left = this.Left - 10;
                wpfwindow.Top = this.Top - 10;
            }
            else
            {
                wpfwindow.Visibility = System.Windows.Visibility.Hidden;
            }*/

            //this.TransparencyKey = culoare;
            this.Invalidate();
        }
        private void Restore_MouseEnter(object sender, EventArgs e)
        {
            Restore.BackgroundImage = RestoreWhite.BackgroundImage;
        }

        private void Restore_MouseLeave(object sender, EventArgs e)
        {
            Restore.BackgroundImage = RestoreBlack.BackgroundImage;
        }
        bool wasMoved = false;
        public bool IsOnScreen(Form form)
        {
            Screen[] screens = Screen.AllScreens;
            foreach (Screen screen in screens)
            {
                Rectangle formRectangle = new Rectangle(form.Left, form.Top, form.Width, form.Height);

                if (screen.WorkingArea.Contains(formRectangle))
                {
                    return true;
                }
            }

            return false;
        }
        void AddFileToRecentFilesList(string fileName)
        {
            SHAddToRecentDocs((uint)ShellAddRecentDocs.SHARD_PATHW, fileName);
        }

        /// <summary>
        /// Native call to add the file to windows' recent file list
        /// </summary>
        /// <param name="uFlags">Always use (uint)ShellAddRecentDocs.SHARD_PATHW</param>
        /// <param name="pv">path to file</param>
        [DllImport("shell32.dll")]
        public static extern void SHAddToRecentDocs(UInt32 uFlags,
            [MarshalAs(UnmanagedType.LPWStr)] String pv);

        enum ShellAddRecentDocs
        {
            SHARD_PIDL = 0x00000001,
            SHARD_PATHA = 0x00000002,
            SHARD_PATHW = 0x00000003
        }
        public long autosaveint = 1;
        public long timer = 0;
        //public WebView HelpBrowser;
        // Glow wpfwindow = new Glow();
        float dx, dy;
        private void Form1_Load(object sender, EventArgs e)
        {
            MainForm m = this.MdiParent as MainForm;
            if (m.runUsingLocalDebuggerToolStripMenuItem.Checked == true) compileMode = "1";
            if (m.runUsingBuiltinBrowserToolStripMenuItem.Checked == true) compileMode = "2";

            Graphics g = this.CreateGraphics();
            try
            {
                dx = g.DpiX;
                dy = g.DpiY;
            }
            finally
            {
                g.Dispose();
            }
            splitContainer3.SplitterDistance = (int)(splitContainer3.SplitterDistance * (96 / (float)dx)) - 10;
            pseudocodeEd.BringToFront();
            if (FormTitle != "") this.Text = FormTitle;
            else this.Text = NewTabTitle;
            //UpdateChecker.RunWorkerAsync();
            helpProvider1.SetShowHelp(this, true);
            helpProvider1.HelpNamespace = Application.StartupPath + "\\HelpHooker.exe";
            //if (Properties.Settings.Default.UILang == 0) helpProvider1.HelpNamespace = "http://www.valinet.ro/en-help-pseudocode";
            //if (Properties.Settings.Default.UILang == 1) helpProvider1.HelpNamespace = "http://www.valinet.ro/ro-help-pseudocode";
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode")) Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\AutoSave")) Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\AutoSave");
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules")) Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules");
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Downloads")) Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Downloads");


            autosaveint = (long)Properties.Settings.Default.AutoSaveInterval;
            autosaveint = autosaveint * 60;
            //button52.Image = System.Drawing.SystemIcons.Information.ToBitmap();
            this.SetBevel(false);
            New();
            //if (this.WindowState == FormWindowState.Maximized) Restore.BringToFront();
            //else Maximize.BringToFront();
            /* toolTip1.SetToolTip(Close, CloseText);
             toolTip1.SetToolTip(Maximize, MaximizeText);
             toolTip1.SetToolTip(Minimize, MinimizeText);
             toolTip1.SetToolTip(Restore, RestoreText);*/
            //CONECTARE LA DROPOX - DE COPIAT
            /*if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
            {
                try
                {
                    UserPanel.Visible = true;

                    using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                    {
                        pictureBox2.Image = Image.FromStream(stream);
                    }
                    pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                    using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                    {
                        pictureBox3.Image = Image.FromStream(stream);
                    }
                    pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
                    label27.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                    label28.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                    label27.Left = 15;
                    UserPanel.Width = label27.Width + 25 + pictureBox2.Width;
                    UserPanel.Left = this.Width - UserPanel.Width;
                    wasMoved = true;
                }
                catch { }
            }
            */
            //backgroundWorker1.RunWorkerAsync();
            //!!!!

            //var config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoaming);
            /*if (Properties.Settings.Default.UILang == 0)
            {
                automaticUpdater1.Translation.Checking = "Checking for updates      ";
                automaticUpdater1.Translation.InstallOnNextStart = "Update will be installed at next start.      ";
                automaticUpdater1.Translation.AlreadyUpToDate = "Your Pseudocode application copy is up-to-date!      ";
                automaticUpdater1.Translation.FailedToCheck = "Unable to check for updates.      ";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                automaticUpdater1.Translation.CheckForUpdatesMenu = "&Verificare pentru actualizări...";
                automaticUpdater1.Translation.DownloadUpdateMenu = "&Descărcare şi actualizare acum";
                automaticUpdater1.Translation.InstallUpdateMenu = "&Instalare actualizare acum";
                automaticUpdater1.Translation.CancelUpdatingMenu = "&Anulare actualizare";
                automaticUpdater1.Translation.CancelCheckingMenu = "&Anulează verificarea de actualizări";
                automaticUpdater1.Translation.HideMenu = "Ascundere";
                automaticUpdater1.Translation.ViewChangesMenu = "Vizualizare îmbunătăţiri în versiunea %version%";
                automaticUpdater1.Translation.PrematureExitTitle = "Asistentul de actualizare s-a închis prematur.";
                automaticUpdater1.Translation.PrematureExitMessage = "Asistentul de actualizare s-a închis înainte ca pasul curent de actualizare să fie finalizat.";
                automaticUpdater1.Translation.StopChecking = "Opreşte căutarea de actualizări acum.";
                automaticUpdater1.Translation.StopDownloading = "Opreşte descărcarea actualizării acum.";
                automaticUpdater1.Translation.StopExtracting = "Opreşte extragerea actualizării acum.";
                automaticUpdater1.Translation.TryAgainLater = "Reîncearcă mai târziu.";
                automaticUpdater1.Translation.TryAgainNow = "Reîncearcă acum.";
                automaticUpdater1.Translation.ViewError = "Vezi detaliile erorii.";
                automaticUpdater1.Translation.CloseButton = "Închidere";
                automaticUpdater1.Translation.ErrorTitle = "Eroare";
                automaticUpdater1.Translation.UpdateNowButton = "Actualizare acum!";
                automaticUpdater1.Translation.ChangesInVersion = "Modificări în versiunea %version%";
                automaticUpdater1.Translation.FailedToCheck = "Nu am putut căuta actualizări.      ";
                automaticUpdater1.Translation.FailedToDownload = "Nu am putut descărca actualizarea.";
                automaticUpdater1.Translation.FailedToExtract = "Nu am putut extrage actualizarea.";
                automaticUpdater1.Translation.Checking = "Căutare actualizări      ";
                automaticUpdater1.Translation.Downloading = "Descărcare actualizare";
                automaticUpdater1.Translation.Extracting = "Extragere actualizare";
                automaticUpdater1.Translation.UpdateAvailable = "Actualizarea este gata de instalare.";
                automaticUpdater1.Translation.InstallOnNextStart = "Actualizarea va fi instalată la următoarea pornire a aplicaţiei.      ";
                automaticUpdater1.Translation.AlreadyUpToDate = "Copia dvs. a aplicației Pseudocod este actualizată!      ";
                automaticUpdater1.Translation.SuccessfullyUpdated = "Am actualizat cu succes la versiunea %version%";
                automaticUpdater1.Translation.UpdateFailed = "Actualizarea nu a putut fi instalată.";
            }*/
            {
                pseudocodeEd.ZoomFactor = Properties.Settings.Default.Zoom;
                converterEd.ZoomFactor = Properties.Settings.Default.Zoom;
                // sliderItem1.Value = Properties.Settings.Default.Zoom;
            }
            // MenuItem mnuRtime = new MenuItem("&Runtime Menu");
            //automaticUpdater1.MenuItem = mnuRtime;
            // mnuRtime.PerformClick();
            pseudocodeEd.Focus();

            this.Activate();
            // Redim();
            //Application.DoEvents();
            if (Properties.Settings.Default.HasCrashed == true)
            {
                //if (Properties.Settings.Default.RestoreByDefault == true) button8.PerformClick();
                Properties.Settings.Default.HasCrashed = false;
                Properties.Settings.Default.Save();
            }
            if (Properties.Settings.Default.ShowOldVBScript == false) if (lang == "VBSCRIPT") button27.PerformClick();
            if (Properties.Settings.Default.ShowOldVBScript == true) button27.Text = "VBSCRIPT";
            pseudocodeEd.Focus();
            ShowCodeboardTimer.Enabled = true;
            //MessageBox.Show(config.FilePath);
            cars = new List<Car>();
            dataGridView1.DataSource = cars;
            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
            dataGridView1.CellContentDoubleClick += dataGridView1_CellContentDoubleClick;
            pseudocodeEd.Parent = splitContainer4.Panel1;
            pseudocodeEd.Dock = DockStyle.Fill;
            converterEd.Parent = splitContainer2.Panel2;
            converterEd.Dock = DockStyle.Fill;
            SchemaLogica.Parent = splitContainer2.Panel2;
            SchemaLogica.Dock = DockStyle.Fill;
            BottomDown.BringToFront();
            BottomDown.Top = statusBar1.Top - 2;

            /*this.SetStyle(
    ControlStyles.AllPaintingInWmPaint |
    ControlStyles.UserPaint |
    ControlStyles.DoubleBuffer,
    true);*/
            /*       ElementHost.EnableModelessKeyboardInterop(wpfwindow);
                   wpfwindow.Show();
                   wpfwindow.Height = this.Height + 20;
                   wpfwindow.Width = this.Width + 20;
                   wpfwindow.Left = this.Left - 10;
                   wpfwindow.Top = this.Top - 10;
                   wpfwindow.Visibility = System.Windows.Visibility.Visible;*/
            //   this.Activated += Form1_Activated;
            //OPTION ONE
            //  this.TopMost = true;
            //if (this.WindowState == FormWindowState.Maximized)
            //{

            //  wpfwindow.Visibility = System.Windows.Visibility.Hidden;
            //}
            //   if (oldDim.Height > this.Height || oldDim.Width > this.Width)
            //       wpfwindow.Hide();
            //    if (ResizeTimer.Enabled == false) DoResize = 0;
            //   Redim();
            //  ResizeTimer.Enabled = true;
            //OPTION TWO
            //Redim();        }
            //Colorare cod tema inchisa
            /*pseudocodeEd.Lexing.Lexer = ScintillaNET.Lexer.Cpp;
            pseudocodeEd.Lexing.Keywords[0] = "citeste scrie daca atunci altfel sfarsit_daca cat_timp sfarsit_cat_timp pentru sfarsit_pentru executa pana_cand si sau intreg caracter sir real div mod diferit";
            pseudocodeEd.Lexing.LineCommentPrefix = "//";
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].ForeColor = System.Drawing.Color.White;
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["NUMBER"]].ForeColor = System.Drawing.Color.FromArgb(102, 177, 168);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD"]].ForeColor = System.Drawing.Color.FromArgb(86, 156, 214);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD2"]].ForeColor = System.Drawing.Color.FromArgb(86, 156, 214);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["STRING"]].ForeColor = System.Drawing.Color.FromArgb(214, 157, 133);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["CHARACTER"]].ForeColor = System.Drawing.Color.FromArgb(86, 156, 214);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["PREPROCESSOR"]].ForeColor = System.Drawing.Color.Brown;
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["OPERATOR"]].ForeColor = System.Drawing.Color.Gray;
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["IDENTIFIER"]].ForeColor = System.Drawing.Color.White;
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["COMMENT"]].ForeColor = System.Drawing.Color.Green;
            pseudocodeEd.Styles.LineNumber.BackColor = Color.FromArgb(64, 64, 64);
            pseudocodeEd.Styles.LineNumber.ForeColor = Color.Silver;*/
            /*pseudocodeEd.Lexing.Lexer = ScintillaNET.Lexer.Cpp;
            pseudocodeEd.Lexing.Keywords[0] = ReadKeyword + " " + WriteKeyword + " " + IfKeyword + " " + ThenKeyword + " " + ElseKeyword + " " + EndIfKeyword + " " + WhileKeyword + " " + EndWhileKeyword + " " + ForKeyword + " " + EndForKeyword + " " + DoKeyword + " " + LoopUntilKeyword;
            pseudocodeEd.Lexing.Keywords[1] = IntegerKeyword + " " + DoubleKeyword + " " + CharKeyword + " " + StringKeyword + " " + DivKeyword + " " + ModKeyword + " " + DifferentKeyword + " " + AndKeyword + " " + OrKeyword;
            //pseudocodeEd.Lexing.Keywords[0] = "citeste scrie daca atunci altfel sfarsit_daca cat_timp sfarsit_cat_timp pentru sfarsit_pentru executa pana_cand";
            //pseudocodeEd.Lexing.Keywords[1] = "intreg real caracter sir div mod diferit si sau";
            pseudocodeEd.Lexing.LineCommentPrefix = "//";*/
            ThemeCode();
            
            pseudocodeEd.NativeInterface.UpdateUI += NativeInterface_UpdateUI;
            //  try
            //   {
            /*converterEd.Styles[converterEd.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].ForeColor = System.Drawing.Color.White;
            converterEd.Styles[converterEd.Lexing.StyleNameMap["NUMBER"]].ForeColor = System.Drawing.Color.FromArgb(102, 177, 168);
            converterEd.Styles[converterEd.Lexing.StyleNameMap["WORD"]].ForeColor = System.Drawing.Color.FromArgb(86, 156, 214);
            converterEd.Styles[converterEd.Lexing.StyleNameMap["WORD2"]].ForeColor = System.Drawing.Color.FromArgb(86, 156, 214);
            converterEd.Styles[converterEd.Lexing.StyleNameMap["STRING"]].ForeColor = System.Drawing.Color.FromArgb(214, 157, 133);
            converterEd.Styles[converterEd.Lexing.StyleNameMap["CHARACTER"]].ForeColor = System.Drawing.Color.FromArgb(86, 156, 214);
            converterEd.Styles[converterEd.Lexing.StyleNameMap["PREPROCESSOR"]].ForeColor = System.Drawing.Color.Brown;
            converterEd.Styles[converterEd.Lexing.StyleNameMap["OPERATOR"]].ForeColor = System.Drawing.Color.Gray;
            converterEd.Styles[converterEd.Lexing.StyleNameMap["IDENTIFIER"]].ForeColor = System.Drawing.Color.White;
            converterEd.Styles[converterEd.Lexing.StyleNameMap["COMMENT"]].ForeColor = System.Drawing.Color.Green;
            converterEd.Styles.LineNumber.BackColor = Color.FromArgb(64, 64, 64);
            converterEd.Styles.LineNumber.ForeColor = Color.Silver;*/
            //   }
            //   catch { }
            if (Properties.Settings.Default.ShowVisualScrollBar == true) pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Horizontal;
            pseudocodeEd.TextChanged += pseudocodeEd_TextChanged;
            //pseudocodeEd.Text = "scrie \"Hello world!\"";
        }

        int lastShownLine = 0;
        void NativeInterface_UpdateUI(object sender, ScintillaNET.NativeScintillaEventArgs e)
        {
          /*  VisualScrollBar.SuspendLayout();
            if (lastShownLine != pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE))
            {
                semiTransparentPanel1.Top = VisualScrollBar.Height * pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE) / VisualScrollBar.Lines.Count;
                //VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
               // VisualScrollBar.Scrolling.ScrollToLine(pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE) - (semiTransparentPanel1.Top / 6));
                pseudocodeEd.GoTo.Line(pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE));
                semiTransparentPanel1.UpdateUI();
            }
            lastShownLine = pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE);
            VisualScrollBar.ResumeLayout();*/
        }


        class Car
        {
            public int Linie { get; set; }
            public string Mesaj { get; set; }
        }
        /*void HelpBrowser_LoadCompleted(object sender, LoadCompletedEventArgs url)
        {

        }*/

        private void Restore_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }
        private const int WM_SYSCOMMAND = 0x0112;
        private const int TPM_RIGHTBUTTON = 0x0002;
        private const int TPM_RETURNCMD = 0x0100;
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            // need to get handle of window


            //translate mouse cursor porition to screen coordinates
            Point p = PointToScreen(new Point(1, 20));

            IntPtr callingTaskBarWindow = this.Handle;
            IntPtr wMenu = GetSystemMenu(this.Handle, false);
            // Display the menu
            uint command = TrackPopupMenuEx(wMenu,
                (uint)(TPMLEFTBUTTON | TPMRETURNCMD), (int)p.X, (int)p.Y, callingTaskBarWindow, IntPtr.Zero);
            if (command == 0)
                return;

            PostMessage(this.Handle, WM_SYSCOMMAND, new IntPtr(command), IntPtr.Zero);
            /*if (this.WindowState == FormWindowState.Maximized)
            {
                restoreDownToolStripMenuItem.Enabled = true;
                maximizeToolStripMenuItem.Enabled = false;
            }
            else
            {
                restoreDownToolStripMenuItem.Enabled = false;
                maximizeToolStripMenuItem.Enabled = true;
            }
                contextMenuStrip1.Show(Cursor.Position);*/
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            button44.BackColor = Color.FromArgb(255, 192, 255);
            if (docPanel.Visible == false)
            {
                docPanel.Visible = true;
                docPanel.Location = new Point(-259, 150);
                docPanel.BringToFront();
                DocumentEditToolsTimer.Enabled = true;
                RecoverPanel.Visible = true;
                RecoverPanel.BringToFront();
            }
            RecoverPanel.Visible = true;
            RecoverPanel.BringToFront();
            DirectoryInfo dinfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\AutoSave\\");

            // What type of file do we want?...
            FileInfo[] Files = dinfo.GetFiles("*.psc");

            // Iterate through each file, displaying only the name inside the listbox...
            foreach (FileInfo file in Files)
            {
                listBox1.Items.Add(file.Name);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            panel11.Visible = true;
            panel11.BringToFront();
            panel11.Height = 0;
            //panel11.Top = panel3.Height - 27;
            if (this.WindowState == FormWindowState.Maximized) panel11.Top = panel3.Height - 29;
            else panel11.Top = panel3.Height - 17;
            panel11.Left = panel3.Left + 271;
            panel11.Focus();
            button9.BackColor = Color.FromArgb(229, 229, 229);
            //pictureBox2.BackColor = Color.FromArgb(229, 229, 229);
            graduateTimer.Enabled = true;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            ExTag extag = (ExTag)pseudocodeEd.Tag;
            MainForm mf = this.MdiParent as MainForm;
            if ((mf.UserPanel.Visible == true || mf.signInToolStripMenuItem.Text.StartsWith(mf.ConnectedAs)) && (bool)extag.Get("issaved") == true)
            {
                Upload up = new Upload((string)extag.Get("filename"));
                up.ShowDialog();
            }
            else
            {
                if ((mf.UserPanel.Visible == false || !mf.signInToolStripMenuItem.Text.StartsWith(mf.ConnectedAs))&& (bool)extag.Get("issaved") == true)
                {
                    CasetaDeMesaj(this, NotSignedIn, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if ((mf.UserPanel.Visible == true || mf.signInToolStripMenuItem.Text.StartsWith(mf.ConnectedAs)) && (bool)extag.Get("issaved") == false)
                {
                    CasetaDeMesaj(this, FileNotSaved, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if ((mf.UserPanel.Visible == false || !mf.signInToolStripMenuItem.Text.StartsWith(mf.ConnectedAs))&& (bool)extag.Get("issaved") == false)
                {
                    CasetaDeMesaj(this, FileNotSaved, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        private void ScintillaOnKeyPress(object sender, ScintillaNET.CharAddedEventArgs e)
        {
            MainForm mf = this.MdiParent as MainForm;
            mf.time = 0;
            mf.Loop.Enabled = true;
            if (e.Ch == 13)
            {
                u = 0;
                for (int i = 0; i < 1000; i++) a[i] = 0;
                for (int i = 0; i < pseudocodeEd.Lines.Count; i++)
                {
                    var list = pseudocodeEd.Markers.GetMarkers(i);

                    foreach (ScintillaNET.Marker mk in list)
                    {
                        if (mk.ToString() != "MarkerNumber0" && mk.ToString() == "MarkerNumber23")
                        {
                            a[u] = i;
                            u++;

                        }

                    }
                }
                markerTimer.Enabled = true;
            }
            ExTag extag = (ExTag)pseudocodeEd.Tag;
            ExTag ex = new ExTag();
            ex.Add("filename", extag.Get("filename"));
            ex.Add("issaved", false);
            if (!this.Text.EndsWith("*")) this.Text += " *";
            ex.Add("uniqueString", extag.Get("uniqueString"));
            pseudocodeEd.Tag = ex;
            var scintilla = sender as ScintillaNET.Scintilla;

            var pos = scintilla.NativeInterface.GetCurrentPos();

            var word = scintilla.GetWordFromPosition(pos);

            if (word == IfKeyword)
            {
                scintilla.Text = scintilla.Text.Substring(0, pos) + " " + ThenKeyword + "\n" + EndIfKeyword + scintilla.Text.Substring(pos, scintilla.Text.Length - pos);
                scintilla.GoTo.Position(pos);
            }
            if (word == WhileKeyword)
            {
                scintilla.Text = scintilla.Text.Substring(0, pos) + " " + DoKeyword + "\n" + EndWhileKeyword + scintilla.Text.Substring(pos, scintilla.Text.Length - pos);
                scintilla.GoTo.Position(pos);
            }
            if (word == ForKeyword)
            {
                scintilla.Text = scintilla.Text.Substring(0, pos) + " " + DoKeyword + "\n" + EndForKeyword + scintilla.Text.Substring(pos, scintilla.Text.Length - pos);
                scintilla.GoTo.Position(pos);
            }



            if (word == string.Empty) return;
            List<string> st = new List<string>();
            st.AddRange(str);
            st.AddRange(strVar);
            var list2 = st.FindAll(item => item.StartsWith(word));

            if (list2.Count > 0)
            {
                scintilla.AutoComplete.Show(list2);
                //scintilla.AcceptsReturn = false;
            }
            else
            {
                scintilla.AutoComplete.Cancel();
                //scintilla.AcceptsReturn = true;
            }

        }
        private void ScintillaOnSelectionChanged(object sender, EventArgs e)
        {
            var _scintilla = sender as ScintillaNET.Scintilla;
            _scintilla.Indicators[1].Style = ScintillaNET.IndicatorStyle.RoundBox;

            _scintilla.Indicators[1].Color = System.Drawing.Color.Black;

            var range = _scintilla.GetRange(0, _scintilla.Text.Length - 1);

            range.ClearIndicator(1);

            if (_scintilla.Selection.Start != _scintilla.Selection.End)
            {

                string selectedWord = _scintilla.Selection.Text;

                _scintilla.FindReplace.Flags = ScintillaNET.SearchFlags.WholeWord;

                IList<ScintillaNET.Range> ranges = _scintilla.FindReplace.FindAll(selectedWord);

                foreach (var r in ranges) { r.SetIndicator(1); }

            }

        }
        private void HighlightSelectedWord(ScintillaNET.Scintilla _scintilla)
        {
            //var _scintilla = sender as ScintillaNET.Scintilla;
            _scintilla.Indicators[1].Style = ScintillaNET.IndicatorStyle.RoundBox;

            _scintilla.Indicators[1].Color = System.Drawing.Color.Yellow;

            var range = _scintilla.GetRange(0, _scintilla.Text.Length - 1);

            range.ClearIndicator(1);

            if (_scintilla.Selection.Start == _scintilla.Selection.End)
            {
                var pos = _scintilla.NativeInterface.GetCurrentPos();

                var word = _scintilla.GetWordFromPosition(pos);
                //Debug.WriteLine(word);
                string selectedWord = word;

                _scintilla.FindReplace.Flags = ScintillaNET.SearchFlags.WholeWord;
                try
                {
                    IList<ScintillaNET.Range> ranges;
                    if (word != "")
                    {
                        ranges = _scintilla.FindReplace.FindAll(selectedWord);

                        foreach (var r in ranges) { r.SetIndicator(1); }
                    }
                }
                catch { }

            }

        }
        private void _initScintilla_MouseDown(object sender, MouseEventArgs e)
        {
            var scintilla = sender as ScintillaNET.Scintilla;
            if (e.Button == MouseButtons.Right)
            {

                undoToolStripMenuItem.Enabled = scintilla.UndoRedo.CanUndo;
                redoToolStripMenuItem.Enabled = scintilla.UndoRedo.CanRedo;
                cutToolStripMenuItem.Enabled = scintilla.Clipboard.CanCut;
                copyToolStripMenuItem.Enabled = scintilla.Clipboard.CanCopy;
                pasteToolStripMenuItem.Enabled = scintilla.Clipboard.CanPaste;
                if (scintilla.Selection.Start != scintilla.Selection.End) deleteToolStripMenuItem.Enabled = true;
                else deleteToolStripMenuItem.Enabled = false;
                if (scintilla.Text == "") selectAllToolStripMenuItem.Enabled = false;
                else selectAllToolStripMenuItem.Enabled = true;

            }

        }

        public void New()
        {
            //this.SuspendLayout();
            lastLine = -1;
            //DevComponents.DotNetBar.SuperTabItem it = new DevComponents.DotNetBar.SuperTabItem();
            //superTabControl1.Tabs.Add(it);
            //this.Text = NewTabTitle;
            //superTabControl1.SelectedTabIndex = superTabControl1.Tabs.Count - 1;
            //pseudocode.Add(new ScintillaNET.Scintilla());
            //this.Controls.Add(pseudocodeEd);
            // pseudocodeEd.Location = new Point(1, 1);
            pseudocodeEd.LineWrapping.Mode = ScintillaNET.LineWrappingMode.None;
            //     pseudocodeEd.Size = new Size(this.Width / 2, this.Height - 160);
            pseudocodeEd.Margins[0].Width = 20;
            pseudocodeEd.Margins[0].Width = 20;
            //pseudocodeEd.LineWrapping.Mode = ScintillaNET.LineWrappingMode.Word;
            pseudocodeEd.Font = new Font("Courier New", 10);
            pseudocodeEd.ConfigurationManager.CustomLocation = Application.StartupPath + "\\Scintilla.XML";
            pseudocodeEd.ConfigurationManager.Language = "pascal";
            //converterEd.ConfigurationManager.Language = "";
            //converterEd.ConfigurationManager.Configure();
            pseudocodeEd.ConfigurationManager.Configure();
            //        pseudocodeEd.Caret.CurrentLineBackgroundColor = Color.LemonChiffon;
            pseudocodeEd.Caret.HighlightCurrentLine = true;
            if (Properties.Settings.Default.UITheme == 1) pseudocodeEd.Caret.Color = Color.Black;
            else pseudocodeEd.Caret.Color = Color.White;
            //pseudocodeEd.Show();
            //pseudocodeEd.BringToFront();
            //converter.Add(new ScintillaNET.Scintilla());
            //this.Controls.Add(converterEd);
            //  converterEd.Location = new Point(this.Width / 2, 1);
            //    converterEd.Size = new Size(this.Width / 2, this.Height - 160);
            converterEd.Margins[0].Width = 0;
            converterEd.Margins[0].Width = 0;
            converterEd.Margins[1].Width = 0;
            converterEd.Margins[1].Width = 0;
            //converterEd.Scrolling.ScrollBars = ScrollBars.None ;
            converterEd.LineWrapping.Mode = ScintillaNET.LineWrappingMode.None;
            converterEd.Font = new Font("Courier New", 10);
            if (Properties.Settings.Default.UITheme == 1) converterEd.ForeColor = Color.Black;
            else converterEd.ForeColor = Color.White;
            /*converterEd.ConfigurationManager.CustomLocation = System.IO.Path.GetFullPath("Scintilla.XML");
            converterEd.ConfigurationManager.Language = "pascal";
            converterEd.ConfigurationManager.Configure();*/
            //converterEd.Show();
            //if (lang != "VBSCRIPT") converterEd.BringToFront();
            pseudocodeEd.CharAdded += ScintillaOnKeyPress;
            pseudocodeEd.SelectionChanged += ScintillaOnSelectionChanged;
            pseudocodeEd.ContextMenuStrip = contextMenuStrip2;
            pseudocodeEd.MouseDown += _initScintilla_MouseDown;
            converterEd.NativeInterface.UsePopUp(false);
            converterEd.Selection.BackColorUnfocused = Color.FromArgb(105, 105, 105); // Color..Gray;
            converterEd.Indentation.ShowGuides = true;
            converterEd.Indentation.SmartIndentType = ScintillaNET.SmartIndent.CPP;
            pseudocodeEd.Indentation.IndentWidth = 4;
            pseudocodeEd.Indentation.TabIndents = true;
            pseudocodeEd.Indentation.UseTabs = false;
            pseudocodeEd.Indentation.ShowGuides = true;
            pseudocodeEd.KeyDown += ScintillaOnKeyPress;
            pseudocodeEd.KeyUp += ScintillaOnKeyUp;
            converterEd.Scrolling.ScrollBars = ScrollBars.Both;
            pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Both;
            converterEd.Folding.IsEnabled = false;

            //pseudocodeEd.Zoom = sliderItem1.Value;
            //converterEd.Zoom = sliderItem1.Value;
            pseudocodeEd.Paint += scintilla_Paint;
            //converterEd.Tag = 0;
            //if (newfiletext != "") pseudocodeEd.Text = newfiletext;
            //pseudocodeEd. += HighlightSelectedWord;
            pseudocodeEd.MarkerChanged += pseudocode_MarkerChanged;
            //converterEd.Caret.CurrentLineBackgroundColor = Color.LemonChiffon;
            converterEd.Caret.HighlightCurrentLine = true;

            /*try
            {
                foreach (ScintillaNET.Scintilla sc in pseudocode)
                {
                    sc.Width = this.Width / 2;
                    if (this.WindowState == FormWindowState.Normal) sc.Height = this.Height - 171;
                    if (this.WindowState == FormWindowState.Maximized) sc.Height = this.Height - 178;
                }
            }
            catch { }
            try
            {
                foreach (ScintillaNET.Scintilla sch in converter)
                {
                    sch.Location = new Point(this.Width / 2, converterEd.Location.Y);
                    sch.Width = this.Width / 2;
                    if (this.WindowState == FormWindowState.Normal) sch.Height = this.Height - 171;
                    if (this.WindowState == FormWindowState.Maximized) sch.Height = this.Height - 178;
                }
            }
            catch { }*/
            ExTag exTag = new ExTag();
            exTag.Add("filename", "");
            exTag.Add("issaved", false);
            exTag.Add("uniqueString", RandomString(12, true));
            pseudocodeEd.Tag = exTag;
            if (docPanel.Visible == false)
            {
                if (this.WindowState == FormWindowState.Normal)
                {
                    tabControl3.Location = new Point(1, this.Height - 173);
                    tabControl3.Size = new Size(this.Width - 2, 150);
                }
                else
                {
                    tabControl3.Location = new Point(1, this.Height - 180);
                    tabControl3.Size = new Size(this.Width - 13 - 2, 150);
                }
                try
                {
                    foreach (ScintillaNET.Scintilla sc in pseudocode)
                    {
                        sc.Width = this.Width / 2;
                        if (this.WindowState == FormWindowState.Normal) sc.Height = this.Height - 171 - 150;
                        if (this.WindowState == FormWindowState.Maximized) sc.Height = this.Height - 178 - 150;
                    }
                }
                catch { }
                try
                {
                    foreach (ScintillaNET.Scintilla sch in converter)
                    {
                        sch.Location = new Point(this.Width / 2, converterEd.Location.Y);
                        if (this.WindowState == FormWindowState.Normal) sch.Width = this.Width / 2 - 1;
                        else sch.Width = this.Width / 2 - 15;
                        if (this.WindowState == FormWindowState.Normal) sch.Height = this.Height - 171 - 150;
                        if (this.WindowState == FormWindowState.Maximized) sch.Height = this.Height - 178 - 150;
                    }
                }
                catch { }
            }
            if (docPanel.Visible == true)
            {
                if (this.WindowState == FormWindowState.Normal)
                {
                    tabControl3.Location = new Point(docPanel.Width, this.Height - 173);
                    tabControl3.Size = new Size(this.Width - docPanel.Width - 2, 150);
                }
                else
                {
                    tabControl3.Location = new Point(docPanel.Width, this.Height - 180);
                    tabControl3.Size = new Size(this.Width - docPanel.Width - 13 - 2, 150);
                }
                try
                {
                    foreach (ScintillaNET.Scintilla sc in pseudocode)
                    {
                        sc.Width = this.Width / 2 - docPanel.Width / 2;
                        sc.Location = new Point(docPanel.Width, sc.Location.Y);
                        if (this.WindowState == FormWindowState.Normal) sc.Height = this.Height - 171 - 150;
                        if (this.WindowState == FormWindowState.Maximized) sc.Height = this.Height - 178 - 150;
                    }
                }
                catch { }
                try
                {
                    foreach (ScintillaNET.Scintilla sch in converter)
                    {
                        sch.Location = new Point(this.Width / 2 + docPanel.Width / 2, sch.Location.Y);
                        if (this.WindowState == FormWindowState.Normal) sch.Width = this.Width / 2 - docPanel.Width / 2;
                        else sch.Width = this.Width / 2 - docPanel.Width / 2 - 15;
                        if (this.WindowState == FormWindowState.Normal) sch.Height = this.Height - 171 - 150;
                        if (this.WindowState == FormWindowState.Maximized) sch.Height = this.Height - 178 - 150;
                    }
                }
                catch { }
            }

            //if (button23.BackColor == Color.White && button22.BackColor == Color.White && button25.BackColor == Color.White && button24.BackColor == Color.White && button26.BackColor == Color.White && button27.BackColor == Color.White)
            // {
            if (Properties.Settings.Default.Language == 1)
            {
                lang = "CPP";
                button23.PerformClick();
            }
            if (Properties.Settings.Default.Language == 2)
            {
                lang = "VB6";
                button22.PerformClick();
            }
            if (Properties.Settings.Default.Language == 3)
            {
                lang = "PAS";
                button25.PerformClick();
            }
            if (Properties.Settings.Default.Language == 4)
            {
                lang = "C#";
                button24.PerformClick();
            }
            if (Properties.Settings.Default.Language == 5)
            {
                lang = "JAVASCRIPT";
                button26.PerformClick();
            }
            if (Properties.Settings.Default.Language == 6)
            {
                lang = "VBSCRIPT";
                button27.PerformClick();
            }
            //}

            pseudocodeEd.Focus();
            //this.ResumeLayout();
        }

        private void pseudocode_MarkerChanged(object sender, ScintillaNET.MarkerChangedEventArgs e)
        {

        }
        public const int SCI_GETFIRSTVISIBLELINE = 2152;
        public const int SCI_LINEFROMPOSITION = 2166;
        public const int SCI_LINEONSCREEN = 2370;

        private void scintilla_Paint(object sender, PaintEventArgs e)
        {
            SolidBrush br = new SolidBrush(Color.Orange);
            Pen pen = new Pen(br);
            Color cl = Color.FromArgb(64, 64, 64);
            if (Properties.Settings.Default.UITheme == 1) cl = Color.White;
            //SendMessage(pseudocodeEd.Handle, 11, 0, 0);
            //SetDoubleBuffered(pseudocodeEd);
            if (pseudocodeEd.Selection.Text == "" && Properties.Settings.Default.UseBar == true && pseudocodeEd.Scrolling.HorizontalScrollOffset == 0)
            {
                //Debug.WriteLine(pseudocodeEd.Margins[0].Width);
                int[] folositi = new int[10000];
                string[] text = Regex.Split(pseudocodeEd.Text, "\n");
                for (int i = 0; i <= text.Length - 1; i++)
                {
                    if (text[i].TrimStart().TrimEnd() == EndIfKeyword)
                    {
                        int x = 0;
                        for (int j = i + x; j >= 0; j--)
                        {
                            if (text[j].Contains(IfKeyword ) && !text[j].Contains(EndIfKeyword))
                            {
                                if (folositi[j] != 1)
                                {
                                    using (Graphics g = Graphics.FromHwnd(pseudocodeEd.Handle))
                                    {
                                        //MessageBox.Show(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])).ToString());
                                        int m = pseudocodeEd.Margins[0].Width;
                                        int desc = pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE);
                                        int linesize = pseudocodeEd.NativeInterface.TextHeight(i);//pseudocodeEd.Height / pseudocodeEd.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN);//(int)g.MeasureString("test", pseudocodeEd.Font, pseudocodeEd.Width).Height;
                                        //if (linesize > 16) linesize = 16;
                                        desc = desc * linesize;
                                        if (i != pseudocodeEd.Lines.Current.Number) g.FillRectangle(new SolidBrush(cl), pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 15, (int)(i - (double)desc / linesize + 1) * linesize - linesize, 1000, linesize);
                                        g.DrawLine(pen, new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (int)(j - (double)desc / linesize + 1) * linesize), new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (int)(i - (double)desc / linesize + 1) * linesize - 2));
                                        if (i != pseudocodeEd.Lines.Current.Number) g.DrawLine(pen, new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (i - desc / linesize + 1) * linesize - 2), new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 25, (int)(i - (double)desc / linesize + 1) * linesize - 2));
                                        if (i != pseudocodeEd.Lines.Current.Number) g.FillRectangle(br, pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 25, (int)(i - (double)desc / linesize + 1) * linesize - 7, 10, 10);
                                        folositi[j] = 1;
                                    }
                                    break;
                                }
                            }
                        }

                    }
                    if (text[i].TrimStart().TrimEnd() == EndForKeyword )
                    {
                        int x = 0;
                        for (int j = i + x; j >= 0; j--)
                        {
                            if (text[j].Contains(ForKeyword ) && !text[j].Contains(EndForKeyword))
                            {
                                if (folositi[j] != 1)
                                {
                                    using (Graphics g = Graphics.FromHwnd(pseudocodeEd.Handle))
                                    {
                                        //MessageBox.Show(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])).ToString());
                                        int m = pseudocodeEd.Margins[0].Width;
                                        int desc = pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE);
                                        int linesize = pseudocodeEd.NativeInterface.TextHeight(i);//pseudocodeEd.Height / pseudocodeEd.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN);//(int)g.MeasureString("test", pseudocodeEd.Font, pseudocodeEd.Width).Height;
                                        //if (linesize > 16) linesize = 16;
                                        desc = desc * linesize;
                                        if (i != pseudocodeEd.Lines.Current.Number) g.FillRectangle(new SolidBrush(cl), pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 15, (int)(i - (double)desc / linesize + 1) * linesize - linesize, 1000, linesize);
                                        g.DrawLine(pen, new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (int)(j - (double)desc / linesize + 1) * linesize), new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (int)(i - (double)desc / linesize + 1) * linesize - 2));
                                        if (i != pseudocodeEd.Lines.Current.Number) g.DrawLine(pen, new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (i - desc / linesize + 1) * linesize - 2), new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 25, (int)(i - (double)desc / linesize + 1) * linesize - 2));
                                        if (i != pseudocodeEd.Lines.Current.Number) g.FillRectangle(br, pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 25, (int)(i - (double)desc / linesize + 1) * linesize - 7, 10, 10);
                                        folositi[j] = 1;
                                    }
                                    break;
                                }
                            }
                        }

                    }
                    if (text[i].TrimStart().TrimEnd() == EndWhileKeyword )
                    {
                        int x = 0;
                        for (int j = i + x; j >= 0; j--)
                        {
                            if (text[j].Contains(WhileKeyword ) && !text[j].Contains(EndWhileKeyword ))
                            {
                                if (folositi[j] != 1)
                                {
                                    using (Graphics g = Graphics.FromHwnd(pseudocodeEd.Handle))
                                    {
                                        //MessageBox.Show(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])).ToString());
                                        int m = pseudocodeEd.Margins[0].Width;
                                        int desc = pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE);
                                        int linesize = pseudocodeEd.NativeInterface.TextHeight(i);//pseudocodeEd.Height / pseudocodeEd.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN);//(int)g.MeasureString("test", pseudocodeEd.Font, pseudocodeEd.Width).Height;
                                        //if (linesize > 16) linesize = 16;
                                        desc = desc * linesize;
                                        if (i != pseudocodeEd.Lines.Current.Number) g.FillRectangle(new SolidBrush(cl), pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 15, (int)(i - (double)desc / linesize + 1) * linesize - linesize, 1000, linesize);
                                        g.DrawLine(pen, new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (int)(j - (double)desc / linesize + 1) * linesize), new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (int)(i - (double)desc / linesize + 1) * linesize - 2));
                                        if (i != pseudocodeEd.Lines.Current.Number) g.DrawLine(pen, new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (i - desc / linesize + 1) * linesize - 2), new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 25, (int)(i - (double)desc / linesize + 1) * linesize - 2));
                                        if (i != pseudocodeEd.Lines.Current.Number) g.FillRectangle(br, pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 25, (int)(i - (double)desc / linesize + 1) * linesize - 7, 10, 10);
                                        folositi[j] = 1;
                                    }
                                    break;
                                }
                            }
                        }

                    }
                    if (text[i].TrimStart().TrimEnd().Contains(LoopUntilKeyword ) && !text[i].TrimStart().TrimEnd().StartsWith("//"))
                    {
                        string rest = Regex.Replace(text[i].TrimStart().TrimEnd(), "pana_cand ", "PÂNĂ CÂND ");
                        int x = 0;
                        for (int j = i + x; j >= 0; j--)
                        {
                            if (text[j].Contains(DoKeyword ) && !text[j].Contains(LoopUntilKeyword ))
                            {
                                if (folositi[j] != 1)
                                {
                                    using (Graphics g = Graphics.FromHwnd(pseudocodeEd.Handle))
                                    {
                                        //MessageBox.Show(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])).ToString());
                                        int m = pseudocodeEd.Margins[0].Width;
                                        int desc = pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE);
                                        int linesize = pseudocodeEd.NativeInterface.TextHeight(i);//pseudocodeEd.Height / pseudocodeEd.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN);//(int)g.MeasureString("test", pseudocodeEd.Font, pseudocodeEd.Width).Height;
                                        //if (linesize > 16) linesize = 16;
                                        desc = desc * linesize;
                                        if (i != pseudocodeEd.Lines.Current.Number) g.FillRectangle(new SolidBrush(cl), pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 15, (int)(i - (double)desc / linesize + 1) * linesize - linesize, 1000, linesize);
                                        g.DrawLine(pen, new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (int)(j - (double)desc / linesize + 1) * linesize), new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (int)(i - (double)desc / linesize + 1) * linesize - 2));
                                        if (i != pseudocodeEd.Lines.Current.Number) g.DrawLine(pen, new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 18, (i - desc / linesize + 1) * linesize - 2), new Point(pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 25, (int)(i - (double)desc / linesize + 1) * linesize - 2));
                                        if (i != pseudocodeEd.Lines.Current.Number) g.FillRectangle(br, pseudocodeEd.NativeInterface.TextWidth(0, GetLeadingWhitespace(text[i])) + m + 25, (int)(i - (double)desc / linesize + 1) * linesize - 7, 10, 10);
                                        folositi[j] = 1;
                                    }
                                    break;
                                }
                            }
                        }

                    }

                }

            }
            pen.Dispose();
            br.Dispose();
            //SendMessage(pseudocodeEd.Handle, 11, 1, 0);
            //pseudocodeEd.NativeInterface.SetBufferedDraw(true);
            //pseudocodeEd.NativeInterface.SetTwoPhaseDraw(true);
            /*using (Graphics g = Graphics.FromHwnd(pseudocodeEd.Handle))
            {
                g.DrawLine(Pens.Red, Point.Empty,
                           new Point(40, 50));
            }*/
        }
        public int GetLeadingWhitespaceAsInt(string line, bool trimToLowerTab = true)
        {
            int whitespace = 0;
            foreach (char ch in line)
            {
                if (ch != ' ') break;
                ++whitespace;
            }

            if (trimToLowerTab)
                whitespace -= whitespace % 4;

            return whitespace;
        }
        private void ScintillaOnKeyUp(object sender, KeyEventArgs e)
        {

        }
        public int IdentationIndex = 0;
        private void ScintillaOnKeyPress(object sender, KeyEventArgs e)
        {


        }
        public static string Indent(int count)
        {
            return "".PadLeft(count);
        }
        private void button3_Click(object sender, EventArgs e)
        {
            New();

        }

        private void metroShell1_Click(object sender, EventArgs e)
        {

        }
        List<ScintillaNET.Marker> listM;
        int[] a = new int[1000];
        int u = 0;
      /*  private void superTabControl1_SelectedTabChanged(object sender, DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs e)
        {

            try
            {
                u = 0;
                for (int i = 0; i < 1000; i++) a[i] = 0;
                for (int i = 0; i < pseudocodeEd.Lines.Count; i++)
                {
                    var list = pseudocodeEd.Markers.GetMarkers(i);

                    foreach (ScintillaNET.Marker mk in list)
                    {
                        if (mk.ToString() != "MarkerNumber0" && mk.ToString() == "MarkerNumber23")
                        {
                            a[u] = i;
                            u++;

                        }

                    }
                }
                pseudocodeEd.BringToFront();
                converterEd.BringToFront();
                //Title.Text = superTabControl1.Tabs[superTabControl1.SelectedTabIndex].Text + " - " + AppName;
                //this.Text = Title.Text;
                flagReident = true;
                pseudocodeEd.Lines.Current.Number = (int)converterEd.Tag;
                pseudocodeEd.Focus();
                if (Properties.Settings.Default.ShowOldVBScript == false) if (lang == "VBSCRIPT") button27.PerformClick();
                //listM = pseudocodeEd.Markers.GetMarkers(pseudocodeEd.Lines.Current);
                markerTimer.Enabled = true;

            }
            catch { }

        }*/

      /*  private void superTabControl1_TabRemoved(object sender, DevComponents.DotNetBar.SuperTabStripTabRemovedEventArgs e)
        {
            // for (int j = 0; j < superTabControl1.Tabs.Count; j++)
            {
                // superTabControl1.Tabs[j].Tag = j;
            }
            u = 0;
            try
            {
                for (int i = 0; i < 1000; i++) a[i] = 0;
                for (int i = 0; i < pseudocodeEd.Lines.Count; i++)
                {
                    var list = pseudocodeEd.Markers.GetMarkers(i);

                    foreach (ScintillaNET.Marker mk in list)
                    {
                        if (mk.ToString() != "MarkerNumber0" && mk.ToString() == "MarkerNumber23")
                        {
                            a[u] = i;
                            u++;

                        }

                    }
                }
                markerTimer.Enabled = true;
            }
            catch { }
        }*/

     /*   private void superTabControl1_TabItemClose(object sender, DevComponents.DotNetBar.SuperTabStripTabItemCloseEventArgs e)
        {
            //MessageBox.Show(pseudocode[(int)e.Tab.Tag].Text.ToString());
            ExTag exTag;
            try
            {
                exTag = (ExTag)pseudocode[(int)e.Tab.Tag].Tag;
            }
            catch
            {
                e.Cancel = true;
                return;
            }
            /*this.Controls.Remove(pseudocode[(int)e.Tab.Tag]);
            this.Controls.Remove(converter[(int)e.Tab.Tag]);
            pseudocode[(int)e.Tab.Tag].Dispose();
            converter[(int)e.Tab.Tag].Dispose();
            pseudocode = pseudocode.RemoveAt((int)e.Tab.Tag);
            converter = converter.RemoveAt((int)e.Tab.Tag);*/
            /*if ((bool)exTag.Get("issaved") == false)
            {
                if (pseudocode[(int)e.Tab.Tag].Text != "")
                {
                    DialogResult dr = CasetaDeMesaj(this, DoYouWantToSaveMod1 + this.Text + DoYouWantToSaveMod2, "Pseudocode", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {

                        AutoSalvare();
                        Salvare();
                        this.Controls.Remove(pseudocode[(int)e.Tab.Tag]);
                        this.Controls.Remove(converter[(int)e.Tab.Tag]);
                        pseudocode[(int)e.Tab.Tag].Dispose();
                        converter[(int)e.Tab.Tag].Dispose();
                        pseudocode.RemoveAt((int)e.Tab.Tag);
                        converter.RemoveAt((int)e.Tab.Tag);
                    }
                    else if (dr == System.Windows.Forms.DialogResult.No)
                    {
                        AutoSalvare();
                        this.Controls.Remove(pseudocode[(int)e.Tab.Tag]);
                        this.Controls.Remove(converter[(int)e.Tab.Tag]);
                        pseudocode[(int)e.Tab.Tag].Dispose();
                        converter[(int)e.Tab.Tag].Dispose();
                        pseudocode.RemoveAt((int)e.Tab.Tag);
                        converter.RemoveAt((int)e.Tab.Tag);
                    }
                    else if (dr == System.Windows.Forms.DialogResult.Cancel)
                    {
                        e.Cancel = true;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    this.Controls.Remove(pseudocode[(int)e.Tab.Tag]);
                    this.Controls.Remove(converter[(int)e.Tab.Tag]);
                    pseudocode[(int)e.Tab.Tag].Dispose();
                    converter[(int)e.Tab.Tag].Dispose();
                    pseudocode.RemoveAt((int)e.Tab.Tag);
                    converter.RemoveAt((int)e.Tab.Tag);
                }
            }
            else
            {

                this.Controls.Remove(pseudocode[(int)e.Tab.Tag]);
                this.Controls.Remove(converter[(int)e.Tab.Tag]);
                pseudocode[(int)e.Tab.Tag].Dispose();
                converter[(int)e.Tab.Tag].Dispose();
                pseudocode.RemoveAt((int)e.Tab.Tag);
                converter.RemoveAt((int)e.Tab.Tag);

            }
            flagReident = true;*/
     //   }

        private void Title_TextChanged(object sender, EventArgs e)
        {
            Title.Refresh();
        }
        /*private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }*/

        #region Borders
        bool Active = false;
        private void borderE_MouseDown(object sender, MouseEventArgs e)
        {
            Active = true;
        }
        private void borderE_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                this.Size = new Size(this.Width + e.Location.X, this.Height);
                this.Refresh();

            }
        }
        private void borderE_MouseUp(object sender, MouseEventArgs e)
        {
            Active = false;
        }
        private void borderS_MouseDown(object sender, MouseEventArgs e)
        {
            Active = true;
        }
        private void borderS_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                this.Size = new Size(this.Width, this.Height + e.Y);
                this.Refresh();
            }
        }
        private void borderS_MouseUp(object sender, MouseEventArgs e)
        {
            Active = false;
        }

        private void borderSE_MouseDown(object sender, MouseEventArgs e)
        {
            Active = true;
        }
        private void borderSE_MouseUp(object sender, MouseEventArgs e)
        {
            Active = false;
        }

        private void borderW_MouseDown(object sender, MouseEventArgs e)
        {
            Active = true;
        }
        private void borderW_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                this.Refresh();
                if (e.X < 0)
                {
                    this.Location = new Point(this.Left + e.X, this.Top);
                    this.Size = new Size(this.Width - e.X, this.Height);
                }
                else
                {
                    this.Size = new Size(this.Width - e.X, this.Height);
                    this.Location = new Point(this.Left + e.X, this.Top);
                }
                this.Refresh();
            }
        }
        private void borderW_MouseUp(object sender, MouseEventArgs e)
        {
            Active = false;
        }

        private void borderSW_MouseDown(object sender, MouseEventArgs e)
        {
            Active = true;
        }
        private void borderSW_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                if (e.X < 0)
                {
                    this.Location = new Point(this.Left + e.Location.X, this.Top);
                    this.Size = new Size(this.Width - e.Location.X, this.Height + e.Location.Y);
                }
                else
                {
                    this.Size = new Size(this.Width - e.Location.X, this.Height + e.Location.Y);
                    this.Location = new Point(this.Left + e.Location.X, this.Top);
                }
                this.Refresh();
            }
        }
        private void borderSW_MouseUp(object sender, MouseEventArgs e)
        {
            Active = false;
        }

        private void borderN_MouseDown(object sender, MouseEventArgs e)
        {
            Active = true;
        }
        private void borderN_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                if (e.Y > 0)
                {
                    this.Size = new Size(this.Width, this.Height - e.Y);
                    this.Location = new Point(this.Left, this.Top + e.Y);
                }
                else
                {
                    this.Location = new Point(this.Left, this.Top + e.Y);
                    this.Size = new Size(this.Width, this.Height - e.Y);
                }
                this.Refresh();
            }
        }
        private void borderN_MouseUp(object sender, MouseEventArgs e)
        {
            Active = false;
        }

        private void borderNE_MouseDown(object sender, MouseEventArgs e)
        {
            Active = true;
        }
        private void borderNE_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                if (e.Y < 0)
                {
                    this.Location = new Point(this.Left, this.Top + e.Y);
                    this.Size = new Size(this.Width + e.X, this.Height - e.Y);
                }
                else
                {
                    this.Size = new Size(this.Width + e.X, this.Height - e.Y);
                    this.Location = new Point(this.Left, this.Top + e.Y);
                }
                this.Refresh();
            }
        }
        private void borderNE_MouseUp(object sender, MouseEventArgs e)
        {
            Active = false;
        }

        private void borderNW_MouseDown(object sender, MouseEventArgs e)
        {
            Active = true;
        }
        private void borderNW_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                if (e.Y < 0 || e.X < 0)
                {
                    this.Location = new Point(this.Left + e.X, this.Top + e.Y);
                    this.Size = new Size(this.Width - e.X, this.Height - e.Y);
                }
                else
                {
                    this.Size = new Size(this.Width - e.X, this.Height - e.Y);
                    this.Location = new Point(this.Left + e.X, this.Top + e.Y);
                }
                this.Refresh();
            }
        }
        private void borderNW_MouseUp(object sender, MouseEventArgs e)
        {
            Active = false;
        }

        #endregion

        private void Form1_Paint(object sender, PaintEventArgs e)
        {

        }
        #region titlebar
        bool Active2 = false;
        int X = 0, Y = 0;

        private void CaptionBar_MouseDown(object sender, MouseEventArgs e)
        {
            Active2 = true;
            X = e.X;
            Y = e.Y;
        }
        private void CaptionBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active2)
            {
                this.Location = new Point(this.Left + e.X - X, this.Top + e.Y - Y);
                Refresh();
            }

        }
        private void CaptionBar_MouseUp(object sender, MouseEventArgs e)
        {
            Active2 = false;
        }
        #endregion

        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("user32.dll")]
        public static extern int TrackPopupMenu(int hMenu, int wFlags, int x, int y, int nReserved, int hwnd, ref RECT lprc);
        [DllImport("user32.dll")]
        static extern uint TrackPopupMenuEx(IntPtr hmenu, uint fuFlags, int x, int y,
           IntPtr hwnd, IntPtr lptpm);
        const long TPMLEFTBUTTON = 0x0000L;
        const long TPMRETURNCMD = 0x0100L;
        private void TitleMax_MouseDown(object sender, MouseEventArgs e)
        {



        }

        private void restoreDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }

        private void minimizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void maximizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel9_MouseHover(object sender, EventArgs e)
        {

        }

        private void panel9_MouseMove(object sender, MouseEventArgs e)
        {
            Cursor.Position = new Point(Cursor.Position.X + 10 - e.Location.X, Cursor.Position.Y + 10 - e.Location.Y);
        }

        private void transparentPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void saveTimer_Tick(object sender, EventArgs e)
        {
            panel9.Visible = true;
            panel9.Height += 60;
            if (panel9.Height % 10 == 0)
            {
                panel9.Top += 1;
            }
            if (panel9.Height == 240)
            {
                panel9.Height = 239;
                saveTimer.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            panel9.Visible = true;
            panel9.BringToFront();
            panel9.Height = 0;
            if (this.WindowState == FormWindowState.Maximized) panel9.Top = panel3.Height - 7;
            else panel9.Top = panel3.Height + 7;
            panel9.Left = panel3.Left + 2;
            panel9.Focus();
            button2.BackColor = Color.FromArgb(229, 229, 229);
            saveTimer.Enabled = true;

        }

        private void panel9_Leave(object sender, EventArgs e)
        {
            panel9.Visible = false;
            button2.BackColor = Color.White;
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            label6.BackColor = Color.FromArgb(229, 229, 229);


        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            label6.BackColor = Color.White;


        }

        private void button28_MouseEnter(object sender, EventArgs e)
        {
            label7.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button28_MouseLeave(object sender, EventArgs e)
        {
            label7.BackColor = Color.White;
        }

        private void button28_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("#neimplementat");
            /*    int temp = superTabControl1.SelectedTabIndex;
                for (int i = 0; i < superTabControl1.Tabs.Count; i++)
                {
                    superTabControl1.SelectedTabIndex = i;

                    Salvare();
                }
                superTabControl1.SelectedTabIndex = temp;*/
            pseudocodeEd.Focus();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label6_MouseEnter(object sender, EventArgs e)
        {
            button4.BackColor = Color.FromArgb(229, 229, 229);
            label6.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label6_MouseLeave(object sender, EventArgs e)
        {
            button4.BackColor = Color.White;
            label6.BackColor = Color.White;

        }

        private void label7_MouseEnter(object sender, EventArgs e)
        {
            button28.BackColor = Color.FromArgb(229, 229, 229);
            label7.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label7_MouseLeave(object sender, EventArgs e)
        {
            button28.BackColor = Color.White;
            label7.BackColor = Color.White;

        }

        private void button6_Click(object sender, EventArgs e)
        {

            panel10.Visible = true;
            panel10.BringToFront();
            panel10.Height = 0;
            if (this.WindowState == FormWindowState.Maximized) panel10.Top = panel3.Height - 7;
            else panel10.Top = panel3.Height + 7;
            panel10.Left = panel3.Left + 164;
            panel10.Focus();
            button6.BackColor = Color.FromArgb(229, 229, 229);
            printTimer.Enabled = true;
        }

        private void printTimer_Tick(object sender, EventArgs e)
        {
            panel10.Visible = true;
            panel10.Height += 40;
            if (panel10.Height % 10 == 0)
            {
                panel10.Top += 1;
            }
            if (panel10.Height == 160)
            {
                panel10.Height = 158;
                printTimer.Enabled = false;

            }
        }

        private void panel10_Leave(object sender, EventArgs e)
        {
            panel10.Visible = false;
            button6.BackColor = Color.White;
        }

        private void button30_MouseEnter(object sender, EventArgs e)
        {
            label9.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button30_MouseLeave(object sender, EventArgs e)
        {
            label9.BackColor = Color.White;
        }

        private void label9_MouseEnter(object sender, EventArgs e)
        {
            label9.BackColor = Color.FromArgb(229, 229, 229);
            button30.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label9_MouseLeave(object sender, EventArgs e)
        {
            label9.BackColor = Color.White;
            button30.BackColor = Color.White;
        }

        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
        }

        private void button9_MouseEnter(object sender, EventArgs e)
        {
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
        }

        private void button9_MouseLeave(object sender, EventArgs e)
        {
        }

        private void selectablePanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void graduateTimer_Tick(object sender, EventArgs e)
        {
            panel11.Visible = true;
            panel11.Height += 78;
            if (panel11.Height % 10 == 0)
            {
                panel11.Top += 1;
            }
            if (panel11.Height == 390)
            {
                graduateTimer.Enabled = false;
            }
        }

        private void panel11_Leave(object sender, EventArgs e)
        {
            panel11.Visible = false;
            button9.BackColor = Color.White;
        }

        private void label8_MouseEnter(object sender, EventArgs e)
        {
            label8.BackColor = Color.FromArgb(229, 229, 229);
            button29.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button31_MouseEnter(object sender, EventArgs e)
        {
            label10.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label11_MouseEnter(object sender, EventArgs e)
        {
            label11.BackColor = Color.FromArgb(229, 229, 229);
            button32.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label12_MouseEnter(object sender, EventArgs e)
        {
            label12.BackColor = Color.FromArgb(229, 229, 229);
            button33.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label13_MouseEnter(object sender, EventArgs e)
        {
            label13.BackColor = Color.FromArgb(229, 229, 229);
            button34.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label14_MouseEnter(object sender, EventArgs e)
        {
            label14.BackColor = Color.FromArgb(229, 229, 229);
            button35.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button29_MouseEnter(object sender, EventArgs e)
        {
            label8.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label10_MouseEnter(object sender, EventArgs e)
        {
            label10.BackColor = Color.FromArgb(229, 229, 229);
            button31.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button32_MouseEnter(object sender, EventArgs e)
        {
            label11.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button33_MouseEnter(object sender, EventArgs e)
        {
            label12.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button34_MouseEnter(object sender, EventArgs e)
        {
            label13.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button35_MouseEnter(object sender, EventArgs e)
        {
            label14.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button29_MouseLeave(object sender, EventArgs e)
        {
            label8.BackColor = Color.White;
        }

        private void button31_MouseLeave(object sender, EventArgs e)
        {
            label10.BackColor = Color.White;
        }

        private void button32_MouseLeave(object sender, EventArgs e)
        {
            label11.BackColor = Color.White;
        }

        private void button33_MouseLeave(object sender, EventArgs e)
        {
            label12.BackColor = Color.White;
        }

        private void button34_MouseLeave(object sender, EventArgs e)
        {
            label13.BackColor = Color.White;
        }

        private void button35_MouseLeave(object sender, EventArgs e)
        {
            label14.BackColor = Color.White;
        }

        private void label8_MouseLeave(object sender, EventArgs e)
        {
            label8.BackColor = Color.White;
            button29.BackColor = Color.White;
        }

        private void label10_MouseLeave(object sender, EventArgs e)
        {
            label10.BackColor = Color.White;
            button31.BackColor = Color.White;
        }

        private void label11_MouseLeave(object sender, EventArgs e)
        {
            label11.BackColor = Color.White;
            button32.BackColor = Color.White;
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label12_MouseLeave(object sender, EventArgs e)
        {
            label12.BackColor = Color.White;
            button33.BackColor = Color.White;
        }

        private void label13_MouseLeave(object sender, EventArgs e)
        {
            label13.BackColor = Color.White;
            button34.BackColor = Color.White;
        }

        private void label14_MouseLeave(object sender, EventArgs e)
        {
            label14.BackColor = Color.White;
            button35.BackColor = Color.White;
        }

        private void Form1_LocationChanged(object sender, EventArgs e)
        {
            if (panel9.Focused == true)
            {
                panel9.Visible = false;
                button2.BackColor = Color.White;
            }
            if (panel10.Focused == true)
            {
                panel10.Visible = false;
                button6.BackColor = Color.White;
            }
            if (panel11.Focused == true)
            {
                panel11.Visible = false;
                button9.BackColor = Color.White;
            }
        }

        private void Form1_ResizeBegin(object sender, EventArgs e)
        {

        }

        private void DocumentEditToolsTimer_Tick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                tabControl3.Location = new Point(docPanel.Width, this.Height - 173);
                tabControl3.Size = new Size(this.Width - docPanel.Width - 2, 150);
            }
            else
            {
                tabControl3.Location = new Point(docPanel.Width, this.Height - 180);
                tabControl3.Size = new Size(this.Width - docPanel.Width - 13 - 2, 150);
            }
            docPanel.Location = new Point(docPanel.Location.X + 52, 150);
            if (docPanel.Location.X == 1)
            {
                DocumentEditToolsTimer.Enabled = false;
                SchemaLogica.Size = converterEd.Size;
                SchemaLogica.Location = converterEd.Location;
                //highlighter1.UpdateHighlights();
            }
            try
            {
                foreach (ScintillaNET.Scintilla sc in pseudocode)
                {
                    sc.Width = this.Width / 2 - docPanel.Width / 2;
                    sc.Location = new Point(docPanel.Width, sc.Location.Y);
                }
            }
            catch { }
            try
            {
                foreach (ScintillaNET.Scintilla sch in converter)
                {
                    sch.Location = new Point(this.Width / 2 + docPanel.Width / 2, sch.Location.Y);
                    if (this.WindowState == FormWindowState.Normal) sch.Width = this.Width / 2 - docPanel.Width / 2;
                    else sch.Width = this.Width / 2 - docPanel.Width / 2 - 15;

                }
            }
            catch { }

            // 1; 154
            // 259; 310
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (docPanel.Visible == false)
            {
                docPanel.Visible = true;
                docPanel.Location = new Point(-259, 150);
                docPanel.BringToFront();
                DocumentEditToolsTimer.Enabled = true;
                FindPanel.Visible = true;
                FindPanel.BringToFront();
            }
            FindPanel.Visible = true;
            FindPanel.BringToFront();
            textBox1.Focus();


            for (int i = 1; i <= pseudocodeEd.Lines.Count; i++)
            {
                comboBox1.Items.Add(i.ToString());
            }
            lblStatus.Text = "";
            lblLine.Text = "";
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pseudocodeEd.FindReplace.ShowFind();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pseudocodeEd.FindReplace.ShowReplace();
        }
        private ScintillaNET.Range FindNextF(bool searchUp)
        {
            ScintillaNET.Range foundRange;
            foundRange = pseudocodeEd.FindReplace.FindNext(textBox1.Text, true, ScintillaNET.SearchFlags.Posix);
            return foundRange;
        }
        ScintillaNET.Range rg;
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                e.Handled = true;
                if (textBox1.Text == string.Empty)
                    return;

                lblStatus.Text = string.Empty;

                ScintillaNET.Range foundRange = null;

                try
                {
                    foundRange = FindNextF(false);
                }
                catch (ArgumentException ex)
                {
                    lblStatus.Text = "Error in Regular Expression: " + ex.Message;
                    return;
                }

                if (foundRange == null)
                {
                    lblStatus.Text = NoMatchFoundText;
                    pseudocodeEd.Selection.SelectNone();
                }
                else
                {
                    if (foundRange.Start < pseudocodeEd.Caret.Anchor)
                    {
                        lblStatus.Text = SearchAtBeginning;
                    }
                    rg = foundRange;
                    rangeSelector.Enabled = true;
                    //MessageBox.Show(foundRange.Text);
                    //pseudocodeEd.Selection.Range = foundRange;
                    //foundRange.Select();
                    pseudocodeEd.Selection.Range = foundRange;
                    //MoveFormAwayFromSelection();
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            pseudocodeEd.GoTo.Line(comboBox1.SelectedIndex);
            pseudocodeEd.Focus();
            lblLine.Text = "";
        }

        private void comboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {

                e.Handled = true;
                if (int.Parse(comboBox1.Text) > 0 & int.Parse(comboBox1.Text) < pseudocodeEd.Lines.Count + 1)
                {
                    pseudocodeEd.GoTo.Line(int.Parse(comboBox1.Text) - 1);
                    pseudocodeEd.Focus();
                }
                else
                {
                    lblLine.Text = InvalidLineNumber;
                }
            }
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            for (int i = 1; i <= pseudocodeEd.Lines.Count; i++)
            {

                comboBox1.Items.Add(i.ToString());
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            JumpToLine jl = new JumpToLine(this);
            for (int i = 1; i <= pseudocodeEd.Lines.Count; i++)
            {
                jl.comboBox1.Items.Add(i.ToString());
            }
            jl.ShowDialog();
            return;
            if (docPanel.Visible == false)
            {
                docPanel.Visible = true;
                docPanel.Location = new Point(-259, 150);
                docPanel.BringToFront();
                DocumentEditToolsTimer.Enabled = true;
                FindPanel.Visible = true;
                FindPanel.BringToFront();
            }
            FindPanel.Visible = true;
            FindPanel.BringToFront();
            comboBox1.Focus();
            for (int i = 1; i <= pseudocodeEd.Lines.Count; i++)
            {
                comboBox1.Items.Add(i.ToString());
            }
            lblStatus.Text = "";
            lblLine.Text = "";
        }
        public void ClosePanel()
        {
            DocumentEditToolsTimerClose.Enabled = true;
            if (this.WindowState == FormWindowState.Normal)
            {
                tabControl3.Location = new Point(1, this.Height - 173);
                tabControl3.Size = new Size(this.Width - 2, 150);
            }
            else
            {
                tabControl3.Location = new Point(1, this.Height - 180);
                tabControl3.Size = new Size(this.Width - 13 - 2, 150);
            }
            try
            {
                foreach (ScintillaNET.Scintilla sc in pseudocode)
                {
                    sc.Location = new Point(1, pseudocodeEd.Location.Y);
                    sc.Width = this.Width / 2;
                    if (this.WindowState == FormWindowState.Normal) sc.Height = this.Height - 171 - 150;
                    if (this.WindowState == FormWindowState.Maximized) sc.Height = this.Height - 178 - 150;
                }
            }
            catch { }
            try
            {
                foreach (ScintillaNET.Scintilla sch in converter)
                {
                    sch.Location = new Point(this.Width / 2, converterEd.Location.Y);
                    if (this.WindowState == FormWindowState.Normal) sch.Width = this.Width / 2 - 1;
                    else sch.Width = this.Width / 2 - 15;
                    if (this.WindowState == FormWindowState.Normal) sch.Height = this.Height - 171 - 150;
                    if (this.WindowState == FormWindowState.Maximized) sch.Height = this.Height - 178 - 150;
                }
            }
            catch { }
            //    highlighter1.UpdateHighlights();
        }
        private void button36_Click(object sender, EventArgs e)
        {
            DocumentEditToolsTimerClose.Enabled = true;
            ClosePanel();
        }

        private void DocumentEditToolsTimerClose_Tick(object sender, EventArgs e)
        {
            SchemaLogica.Size = converterEd.Size;
            SchemaLogica.Location = converterEd.Location;
            docPanel.Location = new Point(docPanel.Location.X - 26, 150);
            if (docPanel.Location.X == -259)
            {
                docPanel.Visible = false;
                DocumentEditToolsTimerClose.Enabled = false;

                pseudocodeEd.Focus();
            }

        }

        private void button18_Click(object sender, EventArgs e)
        {
            pseudocodeEd.Selection.SelectAll();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            pseudocodeEd.Selection.Clear();
        }

        private void button37_Click(object sender, EventArgs e)
        {
            pseudocodeEd.FindReplace.ShowFind();

        }

        private void button38_Click(object sender, EventArgs e)
        {
            pseudocodeEd.FindReplace.ShowReplace();

        }

        private void button41_Click(object sender, EventArgs e)
        {
            ClosePanel();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Compile(false);
            CreateEXEWizard cr = new CreateEXEWizard(this.MdiParent as MainForm);
            cr.ShowDialog();
            return;
            button42.BackColor = Color.FromArgb(255, 192, 255);
            if (docPanel.Visible == false)
            {
                docPanel.Visible = true;
                docPanel.Location = new Point(-259, 150);
                docPanel.BringToFront();
                DocumentEditToolsTimer.Enabled = true;
                EXEPanel.Visible = true;
                EXEPanel.BringToFront();
            }
            EXEPanel.Visible = true;
            EXEPanel.BringToFront();
            EXELocation.Focus();
            //highlighter1.UpdateHighlights();
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc")) File.Copy(Application.StartupPath + "\\test.rc", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc");
            StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc");
            string text = sr.ReadToEnd();
            sr.Close();
            string[] textRC = text.Split('\n');
            foreach (string st in textRC)
            {
                if (st.StartsWith("id ICON ")) EXEIcon.Text = Regex.Replace(st, "id ICON ", "").Replace("\"", "");
                if (st.StartsWith("      VALUE \"CompanyName\", ")) EXECompanyName.Text = Regex.Replace(st, "      VALUE \"CompanyName\", ", "").Replace("\"", "");
                if (st.StartsWith("      VALUE \"FileDescription\", ")) EXEName.Text = Regex.Replace(st, "      VALUE \"FileDescription\", ", "").Replace("\"", "");
                if (st.StartsWith("      VALUE \"InternalName\", ")) EXEInternalName.Text = Regex.Replace(st, "      VALUE \"InternalName\", ", "").Replace("\"", "");
                if (st.StartsWith("      VALUE \"LegalCopyright\", ")) EXECopyright.Text = Regex.Replace(st, "      VALUE \"LegalCopyright\", ", "").Replace("\"", "");
                if (st.StartsWith("      VALUE \"ProductName\", ")) EXEProductName.Text = Regex.Replace(st, "      VALUE \"ProductName\", ", "").Replace("\"", "");
                if (st.StartsWith("FILEVERSION     ")) EXEVersion.Text = Regex.Replace(st, "FILEVERSION     ", "");
            }
            EXELocation.Text = Properties.Settings.Default.EXELocation;
        }

        private void button21_Click(object sender, EventArgs e)
        {
            string Code = pseudocodeEd.Text.Replace("+", "#ENCODED_PLUS_SIGN#");
            ShareCode sh = new ShareCode(this.MdiParent, HttpUtility.UrlEncode(Code));
            sh.ShowDialog();
            return;
            button43.BackColor = Color.FromArgb(255, 192, 255);
            if (docPanel.Visible == false)
            {
                docPanel.Visible = true;
                docPanel.Location = new Point(-259, 150);
                docPanel.BringToFront();
                DocumentEditToolsTimer.Enabled = true;
                ModulePanel.Visible = true;
                ModulePanel.BringToFront();
            }
            ModulePanel.Visible = true;
            ModulePanel.BringToFront();
            DirectoryInfo dinfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules\\");

            // What type of file do we want?...
            FileInfo[] Files = dinfo.GetFiles("*.psf");
            listBox2.Items.Clear();
            // Iterate through each file, displaying only the name inside the listbox...
            foreach (FileInfo file in Files)
            {
                listBox2.Items.Add(file.Name);
            }
            if (Properties.Settings.Default.UILang == 0) webBrowser1.Navigate("http://www.valinet.ro/functii/en-uk/index.htm");
            if (Properties.Settings.Default.UILang == 1) webBrowser1.Navigate("http://www.valinet.ro/functii/ro-ro/index.htm");
        }

        private void button46_Click(object sender, EventArgs e)
        {
            ClosePanel();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            pseudocodeEd.Selection.Text = Clipboard.GetText();
            flagReident = true;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(pseudocodeEd.Selection.Text);
                pseudocodeEd.Selection.Text = "";
            }
            catch { }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(pseudocodeEd.Selection.Text);
            }
            catch { }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
                if (lang == "VBSCRIPT") Clipboard.SetImage(SchemaLogicaCont.Image);
                else Clipboard.SetText(converterEd.Text);
            }
            catch { }
        }

        private void button45_Click(object sender, EventArgs e)
        {
            ClosePanel();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                pseudocodeEd.Printing.PrintPreview();
            }
            catch (Exception ex)
            {
                CasetaDeMesaj(this, UnableToPrint + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.ShowOldVBScript == false && lang == "VBSCRIPT")
            {
                CasetaDeMesaj(this, SwitchToCode, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                pseudocodeEd.Focus();
            }
            else
            {
                converterEd.Printing.PrintPreview();
                pseudocodeEd.Focus();
            }
        }

        private void button30_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.ShowOldVBScript == false && lang == "VBSCRIPT")
            {
                CasetaDeMesaj(this, SwitchToCode, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                pseudocodeEd.Focus();
            }
            else
            {
                try
                {
                    converterEd.Printing.PrintPreview();
                    pseudocodeEd.Focus();
                }
                catch (Exception ex)
                {
                    CasetaDeMesaj(this, UnableToPrint + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public string SalvareCa()
        {
            //MessageBox.Show("da");
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    //string[] lines = Regex.Split(scintilla1.Text, "\n");
                    string proprietati = "\n<properties>\n";
                    for (int i = 0; i <= fileProperties.Count - 1; i++)
                    {
                        if (i == 0) proprietati += fileProperties[i];
                        else proprietati += "\n" + fileProperties[i];
                    }
                    File.WriteAllText(saveFileDialog1.FileName, pseudocodeEd.Text + proprietati);

                    /*System.IO.TextWriter tw = new System.IO.StreamWriter(saveFileDialog1.FileName);

                    // write a line of text to the file
                    tw.WriteLine(pseudocodeEd.Text);

                    // close the stream
                    tw.Close();*/
                    FileInfo fi = new FileInfo(saveFileDialog1.FileName);
                    this.Text = Path.GetFileNameWithoutExtension(fi.Name);
                    //   superTabControl1.SelectedTab.Text = Path.GetFileNameWithoutExtension(fi.Name);
                    ExTag exTag = new ExTag();
                    exTag.Add("filename", saveFileDialog1.FileName);
                    exTag.Add("issaved", true);
                    exTag.Add("uniqueString", RandomString(12, true));
                    pseudocodeEd.Tag = exTag;
                    StringCollection col = Properties.Settings.Default.RecentFiles;
                    bool isAdded = false;
                    foreach (string s in col)
                    {
                        if (s == saveFileDialog1.FileName)
                        {
                            isAdded = true;
                        }
                    }
                    if (isAdded == false) col.Add(saveFileDialog1.FileName);
                    Properties.Settings.Default.RecentFiles = col;
                    Properties.Settings.Default.Save();
                    AddFileToRecentFilesList(saveFileDialog1.FileName);
                    timer2.Enabled = true;
                    if (this.Text.EndsWith("*")) this.Text = this.Text.Substring(0, this.Text.Length - 2);
                    // MainForm mf = this.MdiParent as MainForm;
                    // if (mf.ActiveMdiChild == this) mf.Text = this.Text + " - " + AppName;
                    return Path.GetFileNameWithoutExtension(fi.Name);
                }
                catch (System.Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    if (ex.Message == "The specified path, file name, or both are too long. The fully qualified file name must be less than 260 characters, and the directory name must be less than 248 characters.")
                    {
                        saveFileDialog1.FileName = "";
                        CasetaDeMesaj(this, SaveLocationIsTooLong, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return "";
                    }
                    if (ex.Message.StartsWith("The process cannot access the file"))
                    {
                        saveFileDialog1.FileName = "";
                        CasetaDeMesaj(this, FileIsOpened, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return "";
                    }
                    return "";
                }
            }
            else
            {
                return "";
            }
        }
        public string Salvare()
        {
            try
            {

                //string[] lines = Regex.Split(scintilla1.Text, "\n");
                ExTag exTag = (ExTag)(pseudocodeEd.Tag);
                if (Path.GetDirectoryName((string)(exTag.Get("filename"))) == Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\AutoSave") return SalvareCa();
                if (Path.GetDirectoryName((string)(exTag.Get("filename"))) == Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Downloads") return SalvareCa();
                string proprietati = "\n<properties>\n";
                for (int i = 0; i <= fileProperties.Count - 1; i++)
                {
                    if (i == 0) proprietati += fileProperties[i];
                    else proprietati += "\n" + fileProperties[i];
                }
                File.WriteAllText((string)exTag.Get("filename"), pseudocodeEd.Text + proprietati);
                /*System.IO.TextWriter tw = new System.IO.StreamWriter((string)(exTag.Get("filename")));

                // write a line of text to the file
                tw.Write(pseudocodeEd.Text);
                
                // close the stream
                tw.Close();*/
                ExTag tg = new ExTag();
                tg.Add("filename", (string)(exTag.Get("filename")));
                tg.Add("issaved", true);
                tg.Add("uniqueString", (string)(exTag.Get("uniqueString")));
                pseudocodeEd.Tag = tg;
                timer2.Enabled = true;
                if (this.Text.EndsWith("*")) this.Text = this.Text.Substring(0, this.Text.Length - 2);
                // MainForm mf = this.MdiParent as MainForm;
                //   if (mf.ActiveMdiChild == this) mf.Text = this.Text + " - " + AppName;
                return Path.GetFileNameWithoutExtension(pseudocodeEd.Tag.ToString());
            }
            catch
            {
                return SalvareCa();
            }
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Salvare();
            pseudocodeEd.Focus();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SalvareCa();
            //panel9.Visible = false;
            pseudocodeEd.Focus();
        }

        private void startupTimer_Tick(object sender, EventArgs e)
        {
            startupTimer.Enabled = false;
            int a = splitContainer3.Height - Properties.Settings.Default.SplitterDistance;
            splitContainer3.SplitterDistance = a - 4;
            splitContainer3.SplitterMoved += splitContainer3_SplitterMoved;
            /*   if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
               {
                   if (Properties.Settings.Default.AutoSignIn == true && Properties.Settings.Default.WasWelcomeShown == true)
                   {
                       Sign sg = new Sign(this, false);
                       sg.ShowDialog();
                   }
                   //backgroundWorker1.RunWorkerAsync();
               }
               if (Properties.Settings.Default.ErrorMessage != "")
               {
                   CasetaDeMesaj(this, Properties.Settings.Default.ErrorMessage, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                   Properties.Settings.Default.ErrorMessage = "";
                   Properties.Settings.Default.Save();
               }
                   pseudocodeEd.Focus();*/

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            /*       if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat") & Properties.Settings.Default.AutoSignIn == true)
                   {
                       DoOnUIThread(delegate()
                       {
                           UserPanel.Visible = true;
                    //       superTabControl1.Dock = DockStyle.None;
                     //      superTabControl1.Width = this.Width - UserPanel.Width;

                       });
                       var dropBoxStorage = new CloudStorage();
                       var dropBoxConfig = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox);
                       ICloudStorageAccessToken accessToken;
                       using (var fs = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat", FileMode.Open, FileAccess.Read, FileShare.None))
                       {
                           accessToken = dropBoxStorage.DeserializeSecurityToken(fs);
                       }
                       try
                       {

                           var storageToken = dropBoxStorage.Open(dropBoxConfig, accessToken);
                       }
                       catch { }
                       try
                       {
               
                           dropBoxStorage.DownloadFile("/user.pic", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
                           DoOnUIThread(delegate()
                           {
                               try
                               {
                                   using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                                   {
                                       pictureBox2.Image = Image.FromStream(stream);
                                   }
                                   //pictureBox2.Image = Image.FromFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic");
                                   pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                                   using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                                   {
                                       pictureBox3.Image = Image.FromStream(stream);
                                   }
                                   //pictureBox3.Image = Image.FromFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic");
                                   pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
                               }
                               catch { }
                           });
                       }
                       catch { }
                       try
                       {
                           dropBoxStorage.DownloadFile("/user.dat", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
                           DoOnUIThread(delegate()
                           {
                               label27.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                               label28.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                               if (wasMoved == false)
                               {
                                   label27.Left = 15;
                                   UserPanel.Width = label27.Width + 25 + pictureBox2.Width;
                                   UserPanel.Left = this.Width - UserPanel.Width;
                               }
                               linkLabel5.Visible = true;
                           });
                       }
                       catch { }
                       try
                       {
                           if (wasDownloaded == false)
                           {
                               dropBoxStorage.DownloadFile("/user.config", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
                               string text = System.IO.File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\user.config");
                               string[] rez = text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                               Properties.Settings.Default.Position = Convert.ToBoolean(rez[0]);
                               Properties.Settings.Default.Language = Convert.ToInt32(rez[1]);
                               Properties.Settings.Default.AutoSaveInterval = Convert.ToInt32(rez[2]);
                               Properties.Settings.Default.Zoom = Convert.ToInt32(rez[3]);

                               Properties.Settings.Default.Save();
                               if (Properties.Settings.Default.Language == 1)
                               {
                                   //lang = "CPP";
                                   DoOnUIThread(delegate()
       {
           button23.PerformClick();
       });
                               }
                               if (Properties.Settings.Default.Language == 2)
                               {
                                   //lang = "VB6";
                                   DoOnUIThread(delegate()
       {
           button22.PerformClick();
       });
                               }
                               if (Properties.Settings.Default.Language == 3)
                               {
                                   //lang = "PAS";
                                   DoOnUIThread(delegate()
       {
           button25.PerformClick();
       });
                               }
                               if (Properties.Settings.Default.Language == 4)
                               {
                                   //lang = "C#";
                                   DoOnUIThread(delegate()
       {
           button24.PerformClick();
       });
                               }
                               if (Properties.Settings.Default.Language == 5)
                               {
                                   //lang = "JAVASCRIPT";
                                   DoOnUIThread(delegate()
       {
           button26.PerformClick();
       });
                               }
                               if (Properties.Settings.Default.Language == 6)
                               {
                                   //lang = "VBSCRIPT";
                                   DoOnUIThread(delegate()
       {
           button27.PerformClick();
       });
                               }
                                                           DoOnUIThread(delegate()
       {
                     //          for (int i = 0; i < superTabControl1.Tabs.Count; i++)
                               {
                                   pseudocodeEd.Zoom = Properties.Settings.Default.Zoom;
                                   converterEd.Zoom = Properties.Settings.Default.Zoom;
                                 //  sliderItem1.Value = Properties.Settings.Default.Zoom;
                               }
       });
                               autosaveint = (long)Properties.Settings.Default.AutoSaveInterval * 60;
                               Properties.Settings.Default.WasWelcomeShown = Convert.ToBoolean(rez[4]);
                               Properties.Settings.Default.DownloadImages = Convert.ToBoolean(rez[5]);
                               Properties.Settings.Default.UseBar = Convert.ToBoolean(rez[6]);
                               Properties.Settings.Default.Save();
                           }
                       }
                       catch { }
                   }
                   else
                   {
                                           DoOnUIThread(delegate()
                           {
                       UserPanel.Visible = false;
                        
                       Redim();
                           });
                   }*/
        }

        private void userTimer_Tick(object sender, EventArgs e)
        {
            UserDropPanel.Visible = true;
            UserDropPanel.Height += 22;
            if (UserDropPanel.Height % 11 == 0)
            {
                UserDropPanel.Top += 1;
            }
            if (UserDropPanel.Height == 154)
            {
                userTimer.Enabled = false;
            }
        }

        private void UserPanel_Click(object sender, EventArgs e)
        {
            try
            {
                UserDropPanel.Top = 144;
                UserDropPanel.Visible = true;
                UserDropPanel.BringToFront();
                UserDropPanel.Height = 0;
                UserDropPanel.Focus();

                userTimer.Enabled = true;
            }
            catch { }
        }

        private void UserDropPanel_Leave(object sender, EventArgs e)
        {
            UserDropPanel.Visible = false;
        }

        private void button47_Click(object sender, EventArgs e)
        {
            UserDropPanel.Visible = false;
            WaitSignOut ws = new WaitSignOut(this.MdiParent as MainForm, false);
            if (ws.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {


                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                UserPanel.Visible = false;

                //Sign sg = new Sign(this, false);
                //sg.ShowDialog();
                try
                {

                    backgroundWorker1.RunWorkerAsync();
                }
                catch { }
            }
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process p = Process.Start("https://www.dropbox.com/account#settings");
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                {
                    CasetaDeMesaj(this, NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            UserDropPanel.Visible = false;
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            UserDropPanel.Visible = false;


            Sign sg = new Sign(this.MdiParent as MainForm, true);
            sg.ShowDialog();
            //label27.Left = pictureBox2.Left;
            try
            {
                using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                {
                    pictureBox2.Image = Image.FromStream(stream);
                    pictureBox3.Image = Image.FromStream(stream);
                }
            }
            catch { }
            label27.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
            label28.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
            label27.Left = 15;
            UserPanel.Width = label27.Width + 25 + pictureBox2.Width;
            if (this.WindowState == FormWindowState.Normal) UserPanel.Left = this.Width - UserPanel.Width;
            else UserPanel.Left = this.Width - UserPanel.Width - 13;
            linkLabel5.Visible = true;
        }

        private void button48_MouseEnter(object sender, EventArgs e)
        {
            if (panel12.Visible == false)
            {
                label31.BackColor = Color.FromArgb(229, 229, 229);
            }
            else
            {
                label31.BackColor = Color.FromArgb(206, 206, 206);
            }
        }

        private void button48_MouseLeave(object sender, EventArgs e)
        {
            if (panel12.Visible == false)
            {
                label31.BackColor = Color.White;
            }
            else
            {
                label31.BackColor = Color.FromArgb(229, 229, 229);
            }
        }

        private void label31_MouseEnter(object sender, EventArgs e)
        {
            if (panel12.Visible == false)
            {
                label31.BackColor = Color.FromArgb(229, 229, 229);
                button48.BackColor = Color.FromArgb(229, 229, 229);
            }
        }

        private void label31_MouseLeave(object sender, EventArgs e)
        {
            if (panel12.Visible == false)
            {
                label31.BackColor = Color.White;
                button48.BackColor = Color.White;
            }
        }

        private void button48_Click(object sender, EventArgs e)
        {
            panel12.Visible = true;
            panel12.BringToFront();
            panel12.Height = 0;
            if (this.WindowState == FormWindowState.Maximized) panel12.Top = panel3.Height - 5;
            else panel12.Top = panel3.Height;
            if (this.Width < 1200) panel12.Left = panel3.Left + panel3.Width - 165;
            else panel12.Left = panel3.Left + panel3.Width - 57;
            panel12.Focus();
            button48.BackColor = Color.FromArgb(229, 229, 229);
            label31.BackColor = Color.FromArgb(206, 206, 206);
            optionsTimer.Enabled = true;
        }

        private void optionsTimer_Tick(object sender, EventArgs e)
        {
            panel12.Visible = true;
            panel12.Height += 44;
            if (panel12.Height % 44 == 0)
            {
                panel12.Top += 1;
            }
            if (panel12.Height == 176)
            {
                optionsTimer.Enabled = false;
            }
        }

        private void panel12_Leave(object sender, EventArgs e)
        {
            panel12.Visible = false;
            button48.BackColor = Color.White;
            label31.BackColor = Color.White;
        }

        private void label31_Click(object sender, EventArgs e)
        {
            panel12.Visible = true;
            panel12.BringToFront();
            panel12.Height = 0;
            if (this.WindowState == FormWindowState.Maximized) panel12.Top = panel3.Height - 5;
            else panel12.Top = panel3.Height + 2;
            if (this.Width < 1200) panel12.Left = panel3.Left + panel3.Width - 165;
            else panel12.Left = panel3.Left + panel3.Width - 57;
            panel12.Focus();
            button48.BackColor = Color.FromArgb(229, 229, 229);
            label31.BackColor = Color.FromArgb(229, 229, 229);
            optionsTimer.Enabled = true;
        }

        private void button53_Click(object sender, EventArgs e)
        {
            if (UserPanel.Visible == true)
            {
                panel12.Visible = false;
                button48.BackColor = Color.White;
                label31.BackColor = Color.White;
                CasetaDeMesaj(this, AlreadySignedIn, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                //plugin executor
                /*foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
                {
                    if (_Plugins.ContainsKey(pair.Key))
                    {
                        VPlugin plugin = _Plugins[pair.Key];
                        bool rez = plugin.UserIsAboutToSignIntoDropbox();
                        if (rez == true)
                        {
                            return;
                        }
                    }
                }*/
                //plugin executor finish
                panel12.Visible = false;
                button48.BackColor = Color.White;
                label31.BackColor = Color.White;
                Properties.Settings.Default.AutoSignIn = true;
                Properties.Settings.Default.Save();
                Sign sg = new Sign(this.MdiParent as MainForm, false);
                sg.ShowDialog();
                //backgroundWorker1.RunWorkerAsync();

            }
        }

        private void button52_Click(object sender, EventArgs e)
        {
            panel12.Visible = false;
            button48.BackColor = Color.White;
            label31.BackColor = Color.White;
            About about = new About(this);
            about.ShowDialog();

        }
        public bool ReadyToClose = false;
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ReadyToClose == false)
            {
                e.Cancel = true;
                return;
            }
            if (HelpPanel.Visible == true)
            {
                SendKeys.Send("{F1}");
                e.Cancel = true;
            }
            else
            {
                // for (int i = 0; i < superTabControl1.Tabs.Count; i++)
                {
                    //  superTabControl1.SelectedTabIndex = i;
                    ExTag exTag = (ExTag)pseudocodeEd.Tag;
                    if (pseudocodeEd.Text != "" && (bool)exTag.Get("issaved") == false)
                    {
                        DialogResult dr = CasetaDeMesaj(this, DoYouWantToSaveMod1 + this.Text + DoYouWantToSaveMod2, "Pseudocode", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                        if (dr == System.Windows.Forms.DialogResult.Yes)
                        {
                            AutoSalvare();
                            Salvare();

                        }
                        else if (dr == System.Windows.Forms.DialogResult.No)
                        {
                            AutoSalvare();
                        }
                        else if (dr == System.Windows.Forms.DialogResult.Cancel)
                        {
                            e.Cancel = true;
                            return;
                        }
                        else
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }



            }
        }

        private void PositioningTimer_Tick(object sender, EventArgs e)
        {

        }
        public DialogResult Overlay(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            MessageBoxI msg = new MessageBoxI((Form)form.MdiParent, mesaj, titlu, butoane, icon);
            msg.OK.Text = EndText;
            msg.ShowDialog();

            return DialogResult.OK;
        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI((Form)form.MdiParent, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }

        private void button49_Click(object sender, EventArgs e)
        {
            //plugin executor
            /*foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
            {
                if (_Plugins.ContainsKey(pair.Key))
                {
                    VPlugin plugin = _Plugins[pair.Key];
                    bool rez = plugin.Options();
                    if (rez == true)
                    {
                        return;
                    }
                }
            }*/
            //plugin executor finish
          //  OptionsX op = new OptionsX(false, this);
         //   op.ShowDialog();

           // op.Dispose();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        public string compileMode = "1";
        public string lang;
        public void Compile(bool exec)
        {
            if (exec == true)
            {
                //plugin executor
                /*foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
                {
                    if (_Plugins.ContainsKey(pair.Key))
                    {
                        VPlugin plugin = _Plugins[pair.Key];
                        bool rez = plugin.RunningPseudocodeFile(converterEd.Text, lang);
                        if (rez == true) return;
                    }
                }*/
                //plugin executor finish
            }
            bool cont = true;
            string nume = "";
            try
            {
                string[] func = Regex.Split(functii, "\r");
                foreach (string f in func)
                {
                    string file = "";
                    nume = f;
                    if (f != "") file = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules\\" + f + ".psf");
                }
            }
            catch (Exception ex)
            {
                DialogResult dr = CasetaDeMesaj(this, CompileMayGenerateErrors + nume, "Pseudocod", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                if (dr == System.Windows.Forms.DialogResult.No) cont = false;
            }
            try
            {
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe");
            }
            catch (Exception ex)
            {
                if (!(ex.Message.StartsWith("The process cannot access the file") && ex.Message.EndsWith("it is being used by another process."))) CasetaDeMesaj(this, CompileError + " " + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cont == true)
            {
                if (lang == "CPP")
                {
                    converterEd.Text = Compile(pseudocodeEd.Text, lang);
                    string final = "";
                    /*string[] text;
                    text = Regex.Split(converterEd.Text, "\n");
                    string final = "";
                    for (int i = 0; i < text.Length; i++)
                    {
                        //if (!text[i].EndsWith("as Integer") & !text[i].EndsWith("as Long"))
                        final += text[i] + "\r\n";
                    }
                    final = Regex.Replace(final, @"^\s+$[\n]*", "", RegexOptions.Multiline);*/
                    final = Regex.Replace(converterEd.Text, "\r\n", "\n");
                    System.IO.TextWriter tw = new System.IO.StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp");

                    // write a line of text to the file
                    tw.Write(final);

                    // close the stream
                    tw.Close();
                    if (exec == true)
                    {
                        if (compileMode == "1")
                        {
                            try
                            {
                                backgroundWorker2.RunWorkerAsync();
                            }
                            catch { }
                            DialogResult drBW2 = Overlay(this, ProgramIsRunning, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                            if (drBW2 == System.Windows.Forms.DialogResult.OK)
                            {
                                try
                                {
                                    //processPSC.Kill();
                                    processPSC2.Kill();
                                    var allProcceses = Process.GetProcesses();
                                    foreach (Process pr in allProcceses)
                                    {
                                        if (pr.ProcessName == "temp")
                                        {
                                            Process process = pr;  // Process.GetProcesses(); if you dont have.
                                            string fullPath = pr.Modules[0].FileName;
                                            if (fullPath == Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe") pr.Kill();
                                        }
                                    }
                                }
                                catch { }
                            }
                        }
                        else if (compileMode == "2")
                        {
                            string newtext2;
                            customJavaScriptHeader = true;
                            newtext2 = Compile(pseudocodeEd.Text, "JAVASCRIPT");
                            customJavaScriptHeader = false;
                            string final2;
                            /*string[] text;
                            text = Regex.Split(converterEd.Text, "\n");
                            string final = "";
                            for (int i = 0; i < text.Length; i++)
                            {
                                //if (!text[i].EndsWith("as Integer") & !text[i].EndsWith("as Long"))

                                final += text[i] + "\r\n";
                            }*/
                            final2 = Regex.Replace(newtext2, "\r\n", "\n");
                            //final = Regex.Replace(final, @"^\s+$[\n]*", "", RegexOptions.Multiline);
                            System.IO.TextWriter tw2 = new System.IO.StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.html");

                            //2 write a line of text to the file
                            tw2.WriteLine(final2);

                            // close the stream
                            tw2.Close();
                            MainForm mf = this.MdiParent as MainForm;
                            mf.ShowBrowser(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.html");
                            progressBarItem1.Visible = false;

                        }
                        else
                        {
                            string s = Properties.Settings.Default.Compilers[Convert.ToInt32(compileMode) - 3];
                            string[] t = s.Split(';');
                            string extension = "";
                            switch (t[1])
                            {
                                case "CPP": extension = ".cpp"; break;
                                case "PASCAL": extension = ".pas"; break;
                                case "C#": extension = ".cs"; break;
                                case "JAVASCRIPT": extension = ".html"; break;
                                case "VB6": extension = ".vb"; break;
                            }
                            string newtext2;
                            customJavaScriptHeader = true;
                            newtext2 = Compile(pseudocodeEd.Text, t[1]);
                            customJavaScriptHeader = false;
                            string final2;
                            final2 = Regex.Replace(newtext2, "\r\n", "\n");
                            System.IO.TextWriter tw2 = new System.IO.StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp" + extension);
                            tw2.WriteLine(final2);
                            tw2.Close();
                            MainForm mf = this.MdiParent as MainForm;
                            Process.Start(t[2], t[3].Replace("%1", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp" + extension));
                            progressBarItem1.Visible = false;
                        }
                    }
                    else
                    {
                        if (fabkth3 == true)
                        {

                            fabkth3 = false;
                        }
                        else
                        {
                            if (backgroundWorker3.IsBusy == false)
                            {
                                try
                                {
                                    backgroundWorker3.RunWorkerAsync();
                                }
                                catch { }
                            }
                        }


                    }
                }
                else
                {
                    //string era = lang;
                    //lang = "CPP";
                    string newtext;
                    newtext = Compile(pseudocodeEd.Text, "CPP");
                    string final;
                    /*string[] text;
                    text = Regex.Split(converterEd.Text, "\n");
                    string final = "";
                    for (int i = 0; i < text.Length; i++)
                    {
                        //if (!text[i].EndsWith("as Integer") & !text[i].EndsWith("as Long"))

                        final += text[i] + "\r\n";
                    }*/
                    final = Regex.Replace(newtext, "\r\n", "\n");
                    //final = Regex.Replace(final, @"^\s+$[\n]*", "", RegexOptions.Multiline);
                    System.IO.TextWriter tw = new System.IO.StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp");

                    // write a line of text to the file
                    tw.WriteLine(final);

                    // close the stream
                    tw.Close();
                    if (exec == true)
                    {
                        if (compileMode == "1")
                        {
                            try
                            {
                                backgroundWorker2.RunWorkerAsync();
                            }
                            catch { }
                            DialogResult drBW2 = Overlay(this, ProgramIsRunning, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            if (drBW2 == System.Windows.Forms.DialogResult.OK)
                            {
                                try
                                {
                                    //processPSC.Kill();
                                    processPSC2.Kill();
                                    var allProcceses = Process.GetProcesses();
                                    foreach (Process pr in allProcceses)
                                    {
                                        if (pr.ProcessName == "temp")
                                        {
                                            Process process = pr;  // Process.GetProcesses(); if you dont have.
                                            string fullPath = pr.Modules[0].FileName;
                                            if (fullPath == Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe") pr.Kill();
                                        }
                                    }
                                }
                                catch { }
                            }
                        }
                        else if (compileMode == "2")
                        {
                            string newtext2;
                            customJavaScriptHeader = true;
                            newtext2 = Compile(pseudocodeEd.Text, "JAVASCRIPT");
                            customJavaScriptHeader = false;
                            string final2;
                            /*string[] text;
                            text = Regex.Split(converterEd.Text, "\n");
                            string final = "";
                            for (int i = 0; i < text.Length; i++)
                            {
                                //if (!text[i].EndsWith("as Integer") & !text[i].EndsWith("as Long"))

                                final += text[i] + "\r\n";
                            }*/
                            final2 = Regex.Replace(newtext2, "\r\n", "\n");
                            //final = Regex.Replace(final, @"^\s+$[\n]*", "", RegexOptions.Multiline);
                            System.IO.TextWriter tw2 = new System.IO.StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.html");

                            // write a line of text to the file
                            tw2.WriteLine(final2);

                            // close the stream
                            tw2.Close();
                            MainForm mf = this.MdiParent as MainForm;
                            mf.ShowBrowser(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.html");
                            progressBarItem1.Visible = false;
                        }
                        else
                        {
                            string s = Properties.Settings.Default.Compilers[Convert.ToInt32(compileMode) - 3];
                            string[] t = s.Split(';');
                            string extension = t[4];
                            /*switch (t[1])
                            {
                                case "CPP": extension = ".cpp"; break;
                                case "PASCAL": extension = ".pas"; break;
                                case "C#": extension = ".cs"; break;
                                case "JAVASCRIPT": extension = ".html"; break;
                                case "VB6": extension = ".vb"; break;
                            }*/
                            string newtext2;
                            customJavaScriptHeader = true;
                            newtext2 = Compile(pseudocodeEd.Text, t[1]);
                            customJavaScriptHeader = false;
                            string final2;
                            final2 = Regex.Replace(newtext2, "\r\n", "\n");
                            System.IO.TextWriter tw2 = new System.IO.StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp." + extension);
                            tw2.WriteLine(final2);
                            tw2.Close();
                            MainForm mf = this.MdiParent as MainForm;
                            Process.Start(t[2], t[3].Replace("%1", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp." + extension));
                            progressBarItem1.Visible = false;
                        }
                        //System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs");
                    }
                    else
                    {
                        if (fabkth3 == true)
                        {

                            fabkth3 = false;
                        }
                        else
                        {
                            if (backgroundWorker3.IsBusy == false)
                            {
                                try
                                {
                                    backgroundWorker3.RunWorkerAsync();
                                }
                                catch { }
                            }
                        }
                    }
                    //lang = era;
                }
            }
            //this.Close();
        }
        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
        /// <summary>
        /// Return the whitespace at the start of a line.
        /// </summary>
        /// <param name="trimToLowerTab">Round the number of spaces down to the nearest multiple of 4.</param>
        public string GetLeadingWhitespace(string line, bool trimToLowerTab = true)
        {
            int whitespace = 0;
            foreach (char ch in line)
            {
                if (ch != ' ') break;
                ++whitespace;
            }

            if (trimToLowerTab)
                whitespace -= whitespace % 4;

            return "".PadLeft(whitespace);
        }
        private static string ToLiteral(string input, string lang)
        {
            return input;
            using (var writer = new StringWriter())
            {
                using (var provider = CodeDomProvider.CreateProvider("CSharp"))
                {
                    bool ghilInc = false;
                    bool ghilFin = false;
                    if (input.StartsWith("\"")) { input = input.Substring(1, input.Length - 1); }
                    else ghilInc = true;
                    if (input.EndsWith("\"")) { input = input.Substring(0, input.Length - 1);  }
                    else ghilFin = true;
                    input = input.Replace("\\\"", "\"");
                    provider.GenerateCodeFromExpression(new CodePrimitiveExpression(input), writer, null);
                    if (ghilInc && ghilFin) return writer.ToString().Substring(1, writer.ToString().Length - 2);
                    else if (ghilInc) return writer.ToString().Substring(1, writer.ToString().Length - 1);
                    else if (ghilFin) return writer.ToString().Substring(0, writer.ToString().Length - 1); 
                    else return writer.ToString();
                }
            }
        }
        public struct Caciula
        {
            public int Stangastanga;
            public int Stangadreapta;
            public int Dreaptastanga;
            public int Dreaptadreapta;
            public int adancime;
            public int pozitie;
        }
        public Caciula[] caciula = new Caciula[1000];
        public int lumina = 0;
        public string TransformariCaciula(string textEfectiv, string lang)
        {
            string spatii = GetLeadingWhitespace(textEfectiv);
            textEfectiv = textEfectiv.TrimStart();
            try
            {
            inceput:
                textEfectiv += " ";
                char[] chr = textEfectiv.ToCharArray();
                for (int i = 0; i < textEfectiv.Length; i++)
                {
                    if (chr[i] == '^')
                    {
                        int adancime = 1;
                        int stanga = WriteKeyword.Length;
                        int dreapta = textEfectiv.Length - 1;
                        bool areP = false;
                        for (int j = i - 1; j >= 0; j--)
                        {
                            if (adancime == 1)
                            {
                                if (chr[j] == ',' || chr[j] == '%' || chr[j] == '+' || chr[j] == '-' || chr[j] == '*' || chr[j] == '/' || chr[j] == '=' || chr[j] == '^' || chr[j] == '!' || chr[j] == '&' || chr[j] == '|' || chr[j] == '>' || chr[j] == '<')
                                {
                                    if (chr[j - 1] == '&' || chr[j - 1] == '=') stanga = j + 2;
                                    //if (dreapta == textEfectiv.Length) dreapta--;
                                    else if (chr[j] == '|') stanga = j + 2;
                                    else stanga = j;
                                    break;
                                }
                            }
                            if (adancime == 0)
                            {
                                stanga = j + 2;
                                break;
                            }
                            if (chr[j] == '(')
                            {
                                areP = true;
                                adancime--;
                            }
                            if (chr[j] == ')')
                            {
                                areP = true;
                                adancime++;
                            }
                        }
                        adancime = 1;
                        for (int j = i + 1; j <= textEfectiv.Length - 1; j++)
                        {
                            if (adancime == 1)
                            {
                                if (chr[j] == ',' || chr[j] == '%' || chr[j] == '+' || chr[j] == '-' || chr[j] == '*' || chr[j] == '/' || chr[j] == '=' || chr[j] == '^' || chr[j] == '!' || chr[j] == '&' || chr[j] == '|' || chr[j] == '>' || chr[j] == '<')
                                {
                                    //if (dreapta == textEfectiv.Length) dreapta--;
                                    dreapta = j;
                                    break;
                                }
                            }
                            if (adancime == 0)
                            {
                                dreapta = j;
                                break;
                            }
                            if (chr[j] == '(')
                            {
                                areP = true;
                                adancime++;
                            }
                            if (chr[j] == ')')
                            {
                                areP = true;
                                adancime--;
                            }
                        }
                        if (dreapta == textEfectiv.Length - 1 && adancime == 1 && areP == false)
                        {
                            dreapta++;
                        }
                        string stangaS;
                        if (textEfectiv.Contains(IfKeyword)) stangaS = textEfectiv.Substring(0, stanga);
                        else if (textEfectiv.Contains(WhileKeyword)) stangaS = textEfectiv.Substring(0, stanga + 4);
                        else stangaS = textEfectiv.Substring(0, stanga + 2);
                        string argum1 = "";
                        if (lang == "CPP") //argum1 = "pow(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        {
                            if (textEfectiv.Contains(IfKeyword)) argum1 = "pow(" + textEfectiv.Substring(stanga, i - stanga - 1);
                            else if (textEfectiv.Contains(WhileKeyword)) argum1 = "pow(" + textEfectiv.Substring(stanga + 4, i - stanga - 5);
                            else argum1 = "pow(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        }
                        if (lang == "C#") //argum1 = "Math.Pow(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        {
                            if (textEfectiv.Contains(IfKeyword)) argum1 = "Math.Pow(" + textEfectiv.Substring(stanga, i - stanga - 1);
                            else if (textEfectiv.Contains(WhileKeyword)) argum1 = "Math.Pow(" + textEfectiv.Substring(stanga + 4, i - stanga - 5);
                            else argum1 = "Math.Pow(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        }
                        if (lang == "JAVASCRIPT") //argum1 = "Math.pow(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        {
                            if (textEfectiv.Contains(IfKeyword)) argum1 = "Math.pow(" + textEfectiv.Substring(stanga, i - stanga - 1);
                            else if (textEfectiv.Contains(WhileKeyword)) argum1 = "Math.pow(" + textEfectiv.Substring(stanga + 4, i - stanga - 5);
                            else argum1 = "Math.pow(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        }
                        string interim = "@_@@";
                        string argum2 = textEfectiv.Substring(i + 1, dreapta - i - 2) + ")";
                        string dreaptaS = textEfectiv.Substring(dreapta - 1, textEfectiv.Length - dreapta + 1);
                        textEfectiv = stangaS + argum1 + interim + argum2 + dreaptaS;
                        textEfectiv = textEfectiv + " ";
                        goto inceput;
                    }
                }
                return spatii + textEfectiv.Trim();
                /* try
                 {
                     string[] rupere = Regex.Split(textEfectiv, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                     for (int i = 0; i < rupere.Length; i++)
                     {
                         if (rupere[i].TrimStart().StartsWith("\"") && rupere[i].TrimEnd().EndsWith("\"")) rupere[i] = rupere[i].Replace("^", "#_##");
                         if (i == 0) textEfectiv = rupere[i];
                         else textEfectiv += "," + rupere[i];
                     }
                     char[] text = textEfectiv.ToCharArray(); //scrie 2 ^ 2 ^ (2 ^ (2 ^ (2 ^ 2)) ^ 2 ^ 2), 2 ^ 4
                 inceput:
                     string textS = "";
                     for (int i = 0; i < text.Length; i++)
                         textS += text[i];
                     lumina = 0;
                     for (int i = 0; i < text.Length; i++)
                     {
                         if (text[i] == '^')
                         {
                             caciula[lumina] = new Caciula();
                             caciula[lumina].pozitie = i;
                             caciula[lumina].Stangastanga = 0;
                             caciula[lumina].Stangadreapta = 0;
                             caciula[lumina].Dreaptastanga = 0;
                             caciula[lumina].Dreaptadreapta = 0;
                             for (int j = 0; j < i; j++)
                             {
                                 if (text[j] == '(') caciula[lumina].Stangastanga += 1;
                                 if (text[j] == ')') caciula[lumina].Stangadreapta += 1;
                             }
                             for (int j = i; j < text.Length; j++)
                             {
                                 if (text[j] == '(') caciula[lumina].Dreaptastanga += 1;
                                 if (text[j] == ')') caciula[lumina].Dreaptadreapta += 1;
                             }
                             lumina++;
                         }
                     }
                     for (int i = 0; i < lumina; i++)
                     {
                         caciula[i].adancime = caciula[i].Stangastanga - caciula[i].Stangadreapta;
                     }
                     for (int i = 0; i < lumina - 1; i++)
                         for (int j = i + 1; j < lumina; j++)
                         {
                             if (caciula[i].adancime < caciula[j].adancime)
                             {
                                 Caciula t = caciula[i];
                                 caciula[i] = caciula[j];
                                 caciula[j] = t;
                             }
                         }
                     if (caciula[0].adancime == 0)
                     {
                         for (int i = 0; i < lumina - 1; i++)
                             for (int j = i + 1; j < lumina; j++)
                             {
                                 if (caciula[i].pozitie < caciula[j].pozitie)
                                 {
                                     Caciula t = caciula[i];
                                     caciula[i] = caciula[j];
                                     caciula[j] = t;
                                 }
                             }
                     }
                     for (int i = 0; i < lumina; i++)
                     {
                         int pozitiegasitStanga = -1;
                         int pozitiegasitDreapta = -1;
                         bool eraStangaParanteza = false;
                         bool eraDreaptaParanteza = false;
                         for (int j = caciula[i].pozitie - 1; j >= 0; j--)
                         {
                             try
                             {
                                 string p = new string(text);
                                 if (p.Substring(0, j) == GetLeadingWhitespace(textEfectiv) + IfKeyword)
                                 {
                                  /*   while (text[j + 1] == '(')
                                     {
                                         j++;
                                     }*/
                /*        pozitiegasitStanga = j;
                        break;
                    }
                    if (p.Substring(0, j) == GetLeadingWhitespace(textEfectiv) + ForKeyword)
                    {
                        pozitiegasitStanga = j;
                        break;
                    }
                    if (p.Substring(0, j) == GetLeadingWhitespace(textEfectiv) + WhileKeyword)
                    {
                        pozitiegasitStanga = j;
                        break;
                    }
                    if (p.Substring(0, j) == GetLeadingWhitespace(textEfectiv) + LoopUntilKeyword)
                    {
                        pozitiegasitStanga = j;
                        break;
                    }
                    if (p.Substring(0, j) == GetLeadingWhitespace(textEfectiv) + WriteKeyword)
                    {
                        pozitiegasitStanga = j;
                        break;
                    }*/
                /*if (PseudocodeLang == 1)
                {
                    if (text[j] == ' ' && text[j - 1] == 'e' && text[j - 2] == 'i' && text[j - 3] == 'r' && text[j - 4] == 'c' && text[j - 5] == 's')
                    {
                        pozitiegasitStanga = j;
                        break;
                    }
                }
                if (PseudocodeLang == 0)
                {
                    if (text[j] == ' ' && text[j - 1] == 'e' && text[j - 2] == 't' && text[j - 3] == 'i' && text[j - 4] == 'r' && text[j - 5] == 'w')
                    {
                        pozitiegasitStanga = j;
                        break;
                    }
                }
                if (PseudocodeLang == 2)
                {
                    string p = new string(text);
                    if (p.Substring(0, j) == WriteKeyword)
                    {
                        pozitiegasitStanga = j;
                        break;
                    }
                }*/
                /*      }
                      catch { }
                      if (j == 0)
                      {
                          pozitiegasitStanga = 0;
                          break;
                      }
                      if (text[j] == ',' || text[j] == '(' || text[j] == '^' || text[j] == '+' || text[j] == '-' || text[j] == '*' || text[j] == '/' || text[j] == '=' || (text[j] == ' ' && text[j - 1] == ',') || (text[j] == ' ' && text[j - 1] == '(') || (text[j] == ' ' && text[j - 1] == '^') || (text[j] == ' ' && text[j - 1] == '+') || (text[j] == ' ' && text[j - 1] == '-') || (text[j] == ' ' && text[j - 1] == '*') || (text[j] == ' ' && text[j - 1] == '/') || (text[j] == ' ' && text[j - 1] == '=') || (text[j] == ' ' && text[j - 1] == '&' && text[j - 2] == '&') ||
                                         (text[j] == ' ' && text[j - 1] == '|') ||
                                         (text[j] == ' ' && text[j - 1] == '=' && text[j - 2] == '!'))
                      {
                          if (text[j] == '(' || (text[j] == ' ' && text[j - 1] == '(') || text[j] == '^' || (text[j] == ' ' && text[j - 1] == '^'))
                          {
                              pozitiegasitStanga = j - 1;
                              eraStangaParanteza = true;
                          }
                          else pozitiegasitStanga = j;
                          break;
                      }
                  }
                  for (int j = caciula[i].pozitie + 1; j < text.Length; j++)
                  {
                      if (j == text.Length - 1)
                      {
                          pozitiegasitDreapta = text.Length;
                          break;
                      }
                      if (text[j] == ',' || text[j] == ')' || text[j] == '^' || text[j] == '+' || text[j] == '-' || text[j] == '*' || text[j] == '/' || text[j] == '=' || (text[j] == ' ' && text[j + 1] == ',') || (text[j] == ' ' && text[j + 1] == ')') || (text[j] == ' ' && text[j + 1] == '^') || (text[j] == ' ' && text[j + 1] == '+') || (text[j] == ' ' && text[j + 1] == '-') || (text[j] == ' ' && text[j + 1] == '*') || (text[j] == ' ' && text[j + 1] == '/') || (text[j] == ' ' && text[j + 1] == '=') || (text[j] == ' ' && text[j + 1] == '&' && text[j + 2] == '&') ||
                                         (text[j] == ' ' && text[j + 1] == '|') ||
                                         (text[j] == ' ' && text[j + 1] == '!' && text[j + 2] == '='))
                      {
                          if (text[j] == ')' || (text[j] == ' ' && text[j + 1] == ')') || text[j] == '^' || (text[j] == ' ' && text[j + 1] == '^'))
                          {
                              pozitiegasitDreapta = j + 1;
                              eraDreaptaParanteza = true;
                          }
                          else pozitiegasitDreapta = j;
                          break;
                      }

                  }
                  string parteaStanga = "";
                  string parteaDreapta = "";
                  string baza = "";
                  string exponent = "";
                  for (int j = 0; j <= pozitiegasitStanga; j++) parteaStanga += text[j];
                  if (eraStangaParanteza && eraDreaptaParanteza) for (int j = pozitiegasitStanga + 2; j < caciula[i].pozitie; j++) baza += text[j];
                  else for (int j = pozitiegasitStanga + 1; j < caciula[i].pozitie; j++) baza += text[j];
                  if (eraDreaptaParanteza && eraStangaParanteza) for (int j = caciula[i].pozitie + 1; j < pozitiegasitDreapta - 1; j++) exponent += text[j];
                  else for (int j = caciula[i].pozitie + 1; j < pozitiegasitDreapta; j++) exponent += text[j];
                  for (int j = pozitiegasitDreapta; j < text.Length; j++) parteaDreapta += text[j];
                  string final = "";
                  if (lang == "CPP") final = parteaStanga + "pow$_$$" + baza.Trim() + "@_@@ " + exponent.Trim() + "!_!!" + parteaDreapta;
                  if (lang == "C#") final = parteaStanga + "Math.Pow$_$$" + baza.Trim() + "@_@@ " + exponent.Trim() + "!_!!" + parteaDreapta;
                  if (lang == "JAVASCRIPT") final = parteaStanga + "Math.pow$_$$" + baza.Trim() + "@_@@ " + exponent.Trim() + "!_!!" + parteaDreapta;
                  text = final.ToCharArray();
                  //for (int j = 0; j < text.Length; j++)
                  //    Console.Write(text[j]);
                  //Console.Write("\n");
                  goto inceput;
                  //Console.WriteLine(caciula[i].pozitie + "\t" + caciula[i].adancime);
              }
              string finalizare = "";
              for (int i = 0; i < text.Length; i++)
                  finalizare += text[i];
              finalizare = finalizare.Replace("$_$$", "(");
              //finalizare = finalizare.Replace("@_@@", ",");
              finalizare = finalizare.Replace("!_!!", ")");
              finalizare = finalizare.Replace("#_##", "^");
              finalizare = finalizare.Replace("pow((", "(pow(");
              return finalizare;*/
            }
            catch
            {
                return spatii + textEfectiv;
            }
        }
        public string TransformariModulo(string textEfectiv, string lang)
        {
            string spatii = GetLeadingWhitespace(textEfectiv);
            textEfectiv = textEfectiv.TrimStart();
            try
            {
                if (textEfectiv.StartsWith(WriteKeyword) && !textEfectiv.StartsWith(WriteKeyword + "  ")) textEfectiv = Regex.Replace(textEfectiv, "^" + WriteKeyword, WriteKeyword + " ");
            inceput:
                textEfectiv += " ";
                char[] chr = textEfectiv.ToCharArray();
                for (int i = 0; i < textEfectiv.Length; i++)
                {
                    if (chr[i] == '%')
                    {
                        int adancime = 1;
                        int stanga = WriteKeyword.Length;
                        int dreapta = textEfectiv.Length - 1;
                        bool areP = false;
                        for (int j = i - 1; j >= 0; j--)
                        {
                            if (adancime == 1)
                            {
                                if (chr[j] == ',' || chr[j] == '%' || chr[j] == '+' || chr[j] == '-' || chr[j] == '*' || chr[j] == '/' || chr[j] == '=' || chr[j] == '^' || chr[j] == '!' || chr[j] == '&' || chr[j] == '|' || chr[j] == '>' || chr[j] == '<')
                                {
                                    if (chr[j - 1] == '&' || chr[j - 1] == '=') stanga = j + 2;
                                    //if (dreapta == textEfectiv.Length) dreapta--;
                                    else if (chr[j] == '|') stanga = j + 2;
                                    else stanga = j;
                                    break;
                                }
                            }
                            if (adancime == 0)
                            {
                                stanga = j + 2;
                                break;
                            }
                            if (chr[j] == '(')
                            {
                                areP = true;
                                adancime--;
                            }
                            if (chr[j] == ')')
                            {
                                areP = true;
                                adancime++;
                            }
                        }
                        adancime = 1;
                        for (int j = i + 1; j <= textEfectiv.Length - 1; j++)
                        {
                            if (adancime == 1)
                            {
                                if (chr[j] == ',' || chr[j] == '%' || chr[j] == '+' || chr[j] == '-' || chr[j] == '*' || chr[j] == '/' || chr[j] == '=' || chr[j] == '^' || chr[j] == '!' || chr[j] == '&' || chr[j] == '|' || chr[j] == '>' || chr[j] == '<')
                                {
                                   // if (chr[j + 1] == '|' || chr[j + 1] == '&' || chr[j + 1] == '=') dreapta = j + 1;

                                    //if (dreapta == textEfectiv.Length) dreapta--;
                                     dreapta = j;
                                    break;
                                }
                            }
                            if (adancime == 0)
                            {
                                dreapta = j;
                                break;
                            }
                            if (chr[j] == '(')
                            {
                                areP = true;
                                adancime++;
                            }
                            if (chr[j] == ')')
                            {
                                areP = true;
                                adancime--;
                            }
                        }
                        if (dreapta == textEfectiv.Length - 1 && adancime == 1 && areP == false)
                        {
                            dreapta++;
                        }
                        string stangaS;
                        if (textEfectiv.Contains(IfKeyword)) stangaS = textEfectiv.Substring(0, stanga);
                        else if (textEfectiv.Contains(WhileKeyword)) stangaS = textEfectiv.Substring(0, stanga + 4);
                        else stangaS = textEfectiv.Substring(0, stanga + 2);
                        string argum1 = "";
                        if (lang == "CPP") //argum1 = "pow(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        {
                            if (textEfectiv.Contains(IfKeyword)) argum1 = "fmod(" + textEfectiv.Substring(stanga, i - stanga - 1);
                            else if (textEfectiv.Contains(WhileKeyword)) argum1 = "fmod(" + textEfectiv.Substring(stanga + 4, i - stanga - 5);
                            else argum1 = "fmod(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        }
                        if (lang == "C#") //argum1 = "Math.Pow(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        {
                            if (textEfectiv.Contains(IfKeyword)) argum1 = "Math.IEEERemainder(" + textEfectiv.Substring(stanga, i - stanga - 1);
                            else if (textEfectiv.Contains(WhileKeyword)) argum1 = "Math.IEEERemainder(" + textEfectiv.Substring(stanga + 4, i - stanga - 5);
                            else argum1 = "Math.IEEERemainder(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        }
                        if (lang == "JAVASCRIPT") //argum1 = "Math.pow(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        {
                            if (textEfectiv.Contains(IfKeyword)) argum1 = "Math.fmod(" + textEfectiv.Substring(stanga, i - stanga - 1);
                            else if (textEfectiv.Contains(WhileKeyword)) argum1 = "Math.fmod(" + textEfectiv.Substring(stanga + 4, i - stanga - 5);
                            else argum1 = "Math.fmod(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);
                        }

                      /*  if (textEfectiv.Contains(IfKeyword)) argum1 = "fmod(" + textEfectiv.Substring(stanga, i - stanga - 1);
                        else if (textEfectiv.Contains(WhileKeyword)) argum1 = "fmod(" + textEfectiv.Substring(stanga + 4, i - stanga - 5);
                        else argum1 = "fmod(" + textEfectiv.Substring(stanga + 2, i - stanga - 3);*/
                        string interim = "@_@@";
                        string argum2 = textEfectiv.Substring(i + 1, dreapta - i - 2) + ")";
                        string dreaptaS = textEfectiv.Substring(dreapta - 1, textEfectiv.Length - dreapta + 1);
                        textEfectiv = stangaS + argum1 + interim + argum2 + dreaptaS;
                        textEfectiv = textEfectiv + " ";
                        goto inceput;
                    }
                }
                return spatii + textEfectiv.Trim();
                /*string[] rupere = Regex.Split(textEfectiv, ",(?=(?:[%\"]*\"[%\"]*\")*[%\"]*$)");
                for (int i = 0; i < rupere.Length; i++)
                {
                    if (rupere[i].TrimStart().StartsWith("\"") && rupere[i].TrimEnd().EndsWith("\"")) rupere[i] = rupere[i].Replace("%", "#_##");
                    if (i == 0) textEfectiv = rupere[i];
                    else textEfectiv += "," + rupere[i];
                }
                char[] text = textEfectiv.ToCharArray(); //scrie 2 ^ 2 ^ (2 ^ (2 ^ (2 ^ 2)) ^ 2 ^ 2), 2 ^ 4
            inceput:
                string textS = "";
                for (int i = 0; i < text.Length; i++)
                    textS += text[i];
                lumina = 0;
                for (int i = 0; i < text.Length; i++)
                {
                    if (text[i] == '%')
                    {
                        caciula[lumina] = new Caciula();
                        caciula[lumina].pozitie = i;
                        caciula[lumina].Stangastanga = 0;
                        caciula[lumina].Stangadreapta = 0;
                        caciula[lumina].Dreaptastanga = 0;
                        caciula[lumina].Dreaptadreapta = 0;
                        for (int j = 0; j < i; j++)
                        {
                            if (text[j] == '(') caciula[lumina].Stangastanga += 1;
                            if (text[j] == ')') caciula[lumina].Stangadreapta += 1;
                        }
                        for (int j = i; j < text.Length; j++)
                        {
                            if (text[j] == '(') caciula[lumina].Dreaptastanga += 1;
                            if (text[j] == ')') caciula[lumina].Dreaptadreapta += 1;
                        }
                        lumina++;
                    }
                }
                for (int i = 0; i < lumina; i++)
                {
                    caciula[i].adancime = caciula[i].Stangastanga - caciula[i].Stangadreapta;
                }
                for (int i = 0; i < lumina - 1; i++)
                    for (int j = i + 1; j < lumina; j++)
                    {
                        if (caciula[i].adancime < caciula[j].adancime)
                        {
                            Caciula t = caciula[i];
                            caciula[i] = caciula[j];
                            caciula[j] = t;
                        }
                    }
                if (caciula[0].adancime == 0)
                {
                    for (int i = 0; i < lumina - 1; i++)
                        for (int j = i + 1; j < lumina; j++)
                        {
                            if (caciula[i].pozitie < caciula[j].pozitie)
                            {
                                Caciula t = caciula[i];
                                caciula[i] = caciula[j];
                                caciula[j] = t;
                            }
                        }
                }
                for (int i = 0; i < lumina; i++)
                {
                    int pozitiegasitStanga = -1;
                    int pozitiegasitDreapta = -1;
                    bool eraStangaParanteza = false;
                    bool eraDreaptaParanteza = false;
                    for (int j = caciula[i].pozitie - 1; j >= 0; j--)
                    {
                        try
                        {
                            string p = new string(text);
                            if (p.Substring(0, j) == GetLeadingWhitespace(textEfectiv) + IfKeyword)
                            {
                                pozitiegasitStanga = j;
                                break;
                            }
                            if (p.Substring(0, j) == GetLeadingWhitespace(textEfectiv) + ForKeyword)
                            {
                                pozitiegasitStanga = j;
                                break;
                            }
                            if (p.Substring(0, j) == GetLeadingWhitespace(textEfectiv) + WhileKeyword)
                            {
                                pozitiegasitStanga = j;
                                break;
                            }
                            if (p.Substring(0, j) == GetLeadingWhitespace(textEfectiv) + LoopUntilKeyword)
                            {
                                pozitiegasitStanga = j;
                                break;
                            }
                            if (p.Substring(0, j) == GetLeadingWhitespace(textEfectiv) + WriteKeyword)
                            {
                                pozitiegasitStanga = j;
                                break;
                            }
/*
                            if (PseudocodeLang == 1)
                            {
                                if (text[j] == ' ' && text[j - 1] == 'e' && text[j - 2] == 'i' && text[j - 3] == 'r' && text[j - 4] == 'c' && text[j - 5] == 's')
                                {
                                    pozitiegasitStanga = j;
                                    break;
                                }
                            }
                            if (PseudocodeLang == 0)
                            {
                                if (text[j] == ' ' && text[j - 1] == 'e' && text[j - 2] == 't' && text[j - 3] == 'i' && text[j - 4] == 'r' && text[j - 5] == 'w')
                                {
                                    pozitiegasitStanga = j;
                                    break;
                                }
                            }
                            if (PseudocodeLang == 2)
                            {
                                string p = new string(text);
                                if (p.Substring(0, j) == WriteKeyword)
                                {
                                    pozitiegasitStanga = j;
                                    break;
                                }
                            }*/
                /*   }
                   catch { }
                   if (j == 0)
                   {
                       pozitiegasitStanga = 0;
                       break;
                   }
                   if (text[j] == ',' || text[j] == '(' || text[j] == '%' || text[j] == '+' || text[j] == '-' || text[j] == '*' || text[j] == '/' || text[j] == '=' || (text[j] == ' ' && text[j - 1] == ',') || (text[j] == ' ' && text[j - 1] == '(') || (text[j] == ' ' && text[j - 1] == '%') || (text[j] == ' ' && text[j - 1] == '+') || (text[j] == ' ' && text[j - 1] == '-') || (text[j] == ' ' && text[j - 1] == '*') || (text[j] == ' ' && text[j - 1] == '/') || (text[j] == ' ' && text[j - 1] == '=') || (text[j] == ' ' && text[j - 1] == '&' && text[j - 2] == '&') ||
                                      (text[j] == ' ' && text[j - 1] == '|') ||
                                      (text[j] == ' ' && text[j - 1] == '=' && text[j - 2] == '!'))
                   {
                       if (text[j] == '(' || (text[j] == ' ' && text[j - 1] == '(') || text[j] == '%' || (text[j] == ' ' && text[j - 1] == '%'))
                       {
                           pozitiegasitStanga = j - 1;
                           eraStangaParanteza = true;
                       }
                       else pozitiegasitStanga = j;
                       break;
                   }
               }
               for (int j = caciula[i].pozitie + 1; j < text.Length; j++)
               {
                   if (j == text.Length - 1)
                   {
                       pozitiegasitDreapta = text.Length;
                       break;
                   }
                   if (text[j] == ',' || text[j] == ')' || text[j] == '%' || text[j] == '+' || text[j] == '-' || text[j] == '*' || text[j] == '/' || text[j] == '=' || (text[j] == ' ' && text[j + 1] == ',') || (text[j] == ' ' && text[j + 1] == ')') || (text[j] == ' ' && text[j + 1] == '%') || (text[j] == ' ' && text[j + 1] == '+') || (text[j] == ' ' && text[j + 1] == '-') || (text[j] == ' ' && text[j + 1] == '*') || (text[j] == ' ' && text[j + 1] == '/') || (text[j] == ' ' && text[j + 1] == '=') || (text[j] == ' ' && text[j + 1] == '&' && text[j + 2] == '&') ||
                                      (text[j] == ' ' && text[j + 1] == '|') ||
                                      (text[j] == ' ' && text[j + 1] == '!' && text[j + 2] == '='))
                   {
                       if (text[j] == ')' || (text[j] == ' ' && text[j + 1] == ')') || text[j] == '%' || (text[j] == ' ' && text[j + 1] == '%'))
                       {
                           pozitiegasitDreapta = j + 1;
                           eraDreaptaParanteza = true;
                       }
                       else pozitiegasitDreapta = j;
                       break;
                   }

               }
               string parteaStanga = "";
               string parteaDreapta = "";
               string baza = "";
               string exponent = "";
               for (int j = 0; j <= pozitiegasitStanga; j++) parteaStanga += text[j];
               if (eraStangaParanteza && eraDreaptaParanteza) for (int j = pozitiegasitStanga + 2; j < caciula[i].pozitie; j++) baza += text[j];
               else for (int j = pozitiegasitStanga + 1; j < caciula[i].pozitie; j++) baza += text[j];
               if (eraDreaptaParanteza && eraStangaParanteza) for (int j = caciula[i].pozitie + 1; j < pozitiegasitDreapta - 1; j++) exponent += text[j];
               else for (int j = caciula[i].pozitie + 1; j < pozitiegasitDreapta; j++) exponent += text[j];
               for (int j = pozitiegasitDreapta; j < text.Length; j++) parteaDreapta += text[j];
               string final = "";
               if (lang == "CPP") final = parteaStanga + "fmod$_$$" + baza.Trim() + "@_@@ " + exponent.Trim() + "!_!!" + parteaDreapta;
               //if (lang == "C#") final = parteaStanga + "Math.Pow$_$$" + baza.Trim() + "@_@@ " + exponent.Trim() + "!_!!" + parteaDreapta;
              // if (lang == "JAVASCRIPT") final = parteaStanga + "Math.pow$_$$" + baza.Trim() + "@_@@ " + exponent.Trim() + "!_!!" + parteaDreapta;
               text = final.ToCharArray();
               //for (int j = 0; j < text.Length; j++)
               //    Console.Write(text[j]);
               //Console.Write("\n");
               goto inceput;
               //Console.WriteLine(caciula[i].pozitie + "\t" + caciula[i].adancime);
           }
           string finalizare = "";
           for (int i = 0; i < text.Length; i++)
               finalizare += text[i];
           finalizare = finalizare.Replace("$_$$", "(");
           //finalizare = finalizare.Replace("@_@@", ",");
           finalizare = finalizare.Replace("!_!!", ")");
           finalizare = finalizare.Replace("#_##", "%");
           finalizare = finalizare.Replace("fmod((", "(fmod(");
           return finalizare;*/
            }
            catch
            {
                return spatii + textEfectiv;
            }
        }

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
        int[] coresp = new int[10000];
        public int[] corespA = new int[10000];
        bool[] estevar = new bool[10000];
        int nrvar = 0;
        string functii = "";
        bool handluit = true;
        string queryVar = "";
        int numarvariabilepas2 = 0;
        bool customJavaScriptHeader = false;
        public string Compile(string source, string lang, bool infoarena = false)
        {
            //warningBox1.Text = "Nicio eroare și niciun avertisment de afișat";
            bool xxx = false;
            source = source + "\r\n";
            string[] prelucrare = Regex.Split(source, "\n");
            string returnare = "";
            string variabile = "";
            coresp = null;
            coresp = new int[10000];
            //estevar = null;
            //estevar = new bool[10000];
            string[,] mat = new string[10000, 2];
            int nrcurent = 0;
            nrvar = 0;
            functii = "";
            for (int i = 0; i < prelucrare.Length; i++)
            {
                xxx = false;
                // URMĂTORUL MATERIAL ESTE PARTE INTEGRATĂ A ACESTEI LUCRĂRI, ÎNSĂ CODUL
                // SURSĂ ESTE PROPRIETAREA EXCLUSIVĂ A LUI VALENTIN RADU ȘI ESTE FOLOSITĂ
                // SUB LICENȚĂ DE CĂTRE VALINET ROMANIA. COPIEREA FĂRĂ ACORDUL PROPRIETARULUI,
                // PRIN ORICE MIJLOC, ESTE STRICT INTERZISĂ ȘI VA FI PEDEPSITĂ ÎN CEL MAI
                // DRASTIC MOD CONFORM LEGILOR ÎN VIGOARE.
                // 
                // DE FAPT, NU AR TREBUI SĂ VEZI ACEST TEXT.
                //
                // THE FOLLOWING CODE IS AN INTEGRATED PART OF THIS WORK, BUT ITS SOURCE CODE
                // IS THE EXCLUSIVE PROPERTY OF VALENTIN RADU AND IS USED UNDER LICENSE BY
                // VALINET. UNAUTHORIZED COPYING OF THE FOLLOWING CODE WITHOUT THE WRITTEN
                // PERMISSION OF THE OWNER IS STRICTLY FORBIDDEN AND WILL BE PROSECUTED TO
                // THE MAXIMUM EXTEND POSSIBLE WITH THE CURRENT LAWS.
                //
                // ACTUALLY, YOU SHOULDN'T SEE THIS AT ALL.
                if (prelucrare[i].TrimStart().StartsWith("functie:"))
                {
                    functii += Regex.Replace(prelucrare[i], "functie:(.*)", "$1");
                    prelucrare[i] = "";
                }
                if (prelucrare[i].Contains(DivKeyword) & !prelucrare[i].Contains("\"") & lang != "PAS") prelucrare[i] = Regex.Replace(prelucrare[i], DivKeyword, "/");
                if (prelucrare[i].Contains(ModKeyword ) & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], ModKeyword , "%");
                if (prelucrare[i].Contains(DifferentKeyword ) & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], DifferentKeyword , "!=");
                #region Basic Classic/VBScript
                if (lang == "VB6" | lang == "VBSCRIPT")
                {
                    //Partea intreaga
                    if (prelucrare[i].Contains("{") && (prelucrare[i].Contains("}"))) prelucrare[i] = Regex.Replace(Regex.Replace(prelucrare[i], "\\{", "Int("), "\\}", ")");
                    //**Sintaxa Visual Basic***
                    //apelare functie
                    //string[] func = Regex.Split(functii, "\r");
                    /*foreach (string f in func)
                    {
                        try
                        {
                            if (prelucrare[i].StartsWith(f) & f != "")
                            {
                                System.Diagnostics.Debug.Write(f);
                                string nume_var;
                                nume_var = Regex.Replace(prelucrare[i], f, "");
                                nume_var = nume_var.Substring(1, nume_var.Length - 2);
                                nume_var = nume_var.Substring(0, nume_var.Length - 1);
                                prelucrare[i] = Regex.Replace(f, prelucrare[i], nume_var + " = $1(" + nume_var + ")");
                                prelucrare[i] = nume_var + " = " + prelucrare[i] + "(" + nume_var + ")";
                            }
                        }
                        catch { }
                    }*/
                    //Linie noua
                    if (prelucrare[i].Contains("\\n"))
                    {
                        if (prelucrare[i].Contains("\\n\"")) prelucrare[i] = prelucrare[i].Replace("\\n\"", "\" & vbNewLine");
                        else if (prelucrare[i].Contains("\"\\n")) prelucrare[i] = prelucrare[i].Replace("\"\\n", "\"\" & vbNewLine & \"");
                        else if (prelucrare[i].Contains("\\n")) prelucrare[i] = prelucrare[i].Replace("\\n", "\" & vbNewLine & \"");
                    }
                    if (prelucrare[i].Contains(OrKeyword )) prelucrare[i] = Regex.Replace(prelucrare[i], " "+ OrKeyword +" ", " Or ");
                    if (prelucrare[i].Contains(AndKeyword )) prelucrare[i] = Regex.Replace(prelucrare[i], " " + AndKeyword + " ", " And ");
                    //Opersatorul !=
                    if (prelucrare[i].Contains("!=") & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], "!=", "<>");
                    //Opersatorul mod
                    if (prelucrare[i].Contains("%") & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], "%", "mod");
                    //Operatorul div
                    if (prelucrare[i].Contains("/") & !prelucrare[i].Contains("\"") & !prelucrare[i].TrimStart().StartsWith("//"))
                    {
                        string initialul = prelucrare[i].Replace(" ", String.Empty);
                        string cat = initialul.Split('=')[0];
                        //string deimpartit = initialul.Split('=')[1].Split('/')[0];
                        //string impartitor = initialul.Split('=')[1].Split('/')[1];
                        string tipcat = "";
                        //string tipdeimpartit = "";
                        //string tipimpartitor = "";
                        for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++)
                        {
                            if (mat[nt2, 0] != null)
                            {
                                if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == cat | mat[nt2, 0].TrimStart().TrimEnd() == cat)
                                {
                                    string tip2 = mat[nt2, 1];
                                    if (tip2 == "int") tipcat = "int";
                                    if (tip2 == "double") tipcat = "double";
                                }
                                /*if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == deimpartit | mat[nt2, 0].TrimStart().TrimEnd() == deimpartit)
                                {
                                    string tip2 = mat[nt2, 1];
                                    if (tip2 == "int") tipdeimpartit = "int";
                                    if (tip2 == "double") tipdeimpartit = "double";
                                }
                                if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == impartitor | mat[nt2, 0].TrimStart().TrimEnd() == impartitor)
                                {

                                    string tip2 = mat[nt2, 1];
                                    if (tip2 == "int") tipimpartitor = "int";
                                    if (tip2 == "double") tipimpartitor = "double";
                                }*/
                            }
                        }
                        //Debug.WriteLine(tipcat + " " + tipdeimpartit + " " + tipimpartitor);
                        if (tipcat == "int") prelucrare[i] = Regex.Replace(prelucrare[i], "/", "\\");
                        /*string initialul = prelucrare[i];
                        string trn = Regex.Replace(prelucrare[i], @"\=(.*)", "");
                        trn = trn.Substring(0, trn.Length - 1);
                        for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++)
                        {
                            if (mat[nt2, 0] == trn + "\r")
                            {
                                string tip2 = mat[nt2, 1];
                                if (tip2 == "int")
                                {
                                    prelucrare[i] = Regex.Replace(prelucrare[i], "/", "\\");
                                }
                            }
                        }*/
                    }
                    //Comentariu
                    if (prelucrare[i].TrimStart().StartsWith("//")) prelucrare[i] = Regex.Replace(prelucrare[i], "//", "'");
                    //Eliminarea finalizarii cu punct si virgula
                    if (prelucrare[i].EndsWith(";")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                    //Operatia de citire de la tastatura
                    if (prelucrare[i].TrimStart().StartsWith(ReadKeyword))
                    {
                        if (!prelucrare[i].Contains(','))
                        {
                            string initial = prelucrare[i];
                            string ct = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1");
                            if (ct.Contains('[')) ct = ct.Split('[')[0];
                            for (int nt = 0; nt < (mat.Length / 2); nt++)
                            {
                                //System.Diagnostics.Debug.WriteLine(mat[nt, 0]);
                                if (mat[nt, 0] == ct + "\r" | mat[nt, 0] == ct)
                                {
                                    string tip = mat[nt, 1];
                                    if (tip == "int")
                                    {
                                        prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1 = Int(InputBox(\"" + InputText + "$1.\"))");
                                    }
                                    if (tip == "double")
                                    {
                                        prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1 = CDbl(InputBox(\"" + InputText + "$1.\"))");
                                    }
                                    if (tip == "string")
                                    {
                                        prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1 = InputBox(\"" + InputText + "$1.\")");
                                    }
                                    if (tip == "char")
                                    {
                                        prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1 = InputBox(\"" + InputText + "$1.\")");
                                    }
                                    prelucrare[i] = prelucrare[i].Replace("][", ",");
                                    prelucrare[i] = prelucrare[i].Replace("[", "(");
                                    prelucrare[i] = prelucrare[i].Replace("]", ")");
                                }
                                coresp[i] += 1;
                                /*if (prelucrare[i] == initial)
                                {

                                    buttonItem3.Visible = true;
                                    buttonItem3.Text = superTabControl1.SelectedTab.Text + ": " + UndeclaredVariable + ct + ".";
                                    buttonItem3.Tag = (i + 1).ToString() + "." + "1" + "." + "1";
                                    handluit = false;

                                    //warningBox1.Text = "<b>Eroare</b>: Variabilă nedeclarată: <i>" + ct + "</i>";
                                }
                                else
                                {
                                    if (handluit == false)
                                    {
                                        buttonItem3.Visible = false;
                                        handluit = true;
                                    }
                                }*/
                            }
                        }
                        else
                        {

                            string initial = prelucrare[i];
                            string[] varlist;
                            if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), ",");
                            else varlist = Regex.Split(prelucrare[i], ",");
                            varlist[0] = Regex.Replace(varlist[0], ReadKeyword + " ", "");
                            prelucrare[i] = GetLeadingWhitespace(initial);
                            foreach (string ct in varlist)
                            {
                                if (ct.Contains('['))
                                {
                                    string ct2;
                                    ct2 = ct.Split('[')[0];
                                    for (int nt = 0; nt < (mat.Length / 2); nt++)
                                    {
                                        //System.Diagnostics.Debug.WriteLine(mat[nt, 0]);
                                        if (mat[nt, 0] == ct2.TrimStart().TrimEnd() + "\r" | mat[nt, 0] == ct2.TrimStart().TrimEnd())
                                        {
                                            string tip = mat[nt, 1];
                                            if (tip == "int")
                                            {
                                                prelucrare[i] += ct.TrimStart().TrimEnd().Replace("][", ",").Replace("[", "(").Replace("]", ")") + " = Int(InputBox(\"" + InputText + "" + ct2.TrimStart().TrimEnd() + ".\"))";
                                            }
                                            if (tip == "double")
                                            {
                                                prelucrare[i] += ct.TrimStart().TrimEnd().Replace("][", ",").Replace("[", "(").Replace("]", ")") + " = CDbl(InputBox(\"" + InputText + "" + ct2.TrimStart().TrimEnd() + ".\"))";
                                            }
                                            if (tip == "string")
                                            {
                                                prelucrare[i] += ct.TrimStart().TrimEnd().Replace("][", ",").Replace("[", "(").Replace("]", ")") + " = InputBox(\"" + InputText + "" + ct.TrimStart().TrimEnd() + ".\")";
                                            }
                                            if (tip == "char")
                                            {
                                                prelucrare[i] += ct.TrimStart().TrimEnd().Replace("][", ",").Replace("[", "(").Replace("]", ")") + " = InputBox(\"" + InputText + "" + ct.TrimStart().TrimEnd() + ".\")";
                                            }
                                            if (ct2 != varlist[varlist.Length - 1]) prelucrare[i] += "\n" + GetLeadingWhitespace(initial);
                                        }
                                    }
                                }
                                else
                                {

                                    for (int nt = 0; nt < (mat.Length / 2); nt++)
                                    {
                                        //System.Diagnostics.Debug.WriteLine(mat[nt, 0]);
                                        if (mat[nt, 0] == ct.TrimStart().TrimEnd() + "\r" | mat[nt, 0] == ct.TrimStart().TrimEnd())
                                        {
                                            string tip = mat[nt, 1];
                                            if (tip == "int")
                                            {
                                                prelucrare[i] += ct.TrimStart().TrimEnd() + " = Int(InputBox(\"" + InputText + "" + ct.TrimStart().TrimEnd() + ".\"))";
                                            }
                                            if (tip == "double")
                                            {
                                                prelucrare[i] += ct.TrimStart().TrimEnd() + " = CDbl(InputBox(\"" + InputText + "" + ct.TrimStart().TrimEnd() + ".\"))";
                                            }
                                            if (tip == "string")
                                            {
                                                prelucrare[i] += ct.TrimStart().TrimEnd() + " = InputBox(\"" + InputText + "" + ct.TrimStart().TrimEnd() + ".\")";
                                            }
                                            if (tip == "char")
                                            {
                                                prelucrare[i] += ct.TrimStart().TrimEnd() + " = InputBox(\"" + InputText + "" + ct.TrimStart().TrimEnd() + ".\")";
                                            }
                                            if (ct != varlist[varlist.Length - 1]) prelucrare[i] += "\n" + GetLeadingWhitespace(initial);
                                        }
                                    }
                                }
                            }
                        }
                        //ORIGINAL prelucrare[i] = Regex.Replace(prelucrare[i], @"citeste \s*(.*?)(?:\s|$)", "$1 = InputBox(\"" + InputText + "$1.\")");
                    }
                    //Operatia de scriere pe ecran
                    if (prelucrare[i].TrimStart().StartsWith(WriteKeyword))
                    {
                        if (!prelucrare[i].Contains(','))
                        {
                            prelucrare[i] = Regex.Replace(prelucrare[i], WriteKeyword + @" (.*)$", "MsgBox $1");
                            prelucrare[i] = Regex.Replace(prelucrare[i], "\\]\\[(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", ",");
                            prelucrare[i] = Regex.Replace(prelucrare[i], "\\[(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", "(");
                            prelucrare[i] = Regex.Replace(prelucrare[i], "\\](?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", ")");
                            //if (prelucrare[i].Contains("+")) prelucrare[i] = Regex.Replace(prelucrare[i], "\\+", "&");
                        }
                        else
                        {
                            if (prelucrare[i].TrimStart().Replace("^" + WriteKeyword + " ", "").StartsWith("\"") == false && (prelucrare[i].TrimStart().Replace(WriteKeyword + " ", "").EndsWith("\"") == false || prelucrare[i].TrimStart().Substring(0, prelucrare[i].TrimStart().Length - 1).Replace(WriteKeyword + " ", "").EndsWith("\"") == false))
                            {
                                string initial = prelucrare[i];
                                string[] varlist;
                                //Debug.WriteLine(prelucrare[i]);
                                prelucrare[i] = Regex.Replace(prelucrare[i], ", ", ",");
                                string virgula = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
                                if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), virgula);
                                else varlist = Regex.Split(prelucrare[i], virgula);
                                varlist[0] = Regex.Replace(varlist[0].TrimStart(), "^" + WriteKeyword + " ", "");
                                for (int k = 0; k < varlist.Length - 1; k++)
                                {

                                    if (varlist[k].Contains("\"") && (!(varlist[k].StartsWith("\"") && varlist[k].EndsWith("\""))))
                                    {
                                    aici:
                                        try
                                        {
                                            varlist[k] += "," + varlist[k + 1];
                                            var list = new List<string>(varlist);
                                            list.RemoveAt(k + 1);
                                            varlist = list.ToArray();
                                            int count = Regex.Matches(varlist[k], "\"").Count;
                                            if (count % 2 != 0) goto aici;
                                        }
                                        catch { }
                                    }
                                }
                                prelucrare[i] = "";// +"MsgBox ";
                                int ka = 0;
                                foreach (string ct in varlist)
                                {
                                    if (ka == 0) prelucrare[i] += GetLeadingWhitespace(initial) + "MsgBox " + ct.TrimStart().TrimEnd();
                                    else
                                    prelucrare[i] +=  "\n" + GetLeadingWhitespace(initial) + "MsgBox " + ct.TrimStart().TrimEnd();
                                    ka++;
                                    prelucrare[i] = Regex.Replace(prelucrare[i], "\\]\\[(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", ",");
                                    prelucrare[i] = Regex.Replace(prelucrare[i], "\\[(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", "(");
                                    prelucrare[i] = Regex.Replace(prelucrare[i], "\\](?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", ")");
                                }
                                prelucrare[i] += "";
                            }
                            //if (prelucrare[i].Contains("+")) prelucrare[i] = Regex.Replace(prelucrare[i], "\\+", "&");
                            //if (prelucrare[i].TrimStart().Replace("^" + WriteKeyword + " ", "").StartsWith("\"") == false && prelucrare[i].TrimStart().Replace(WriteKeyword + " ", "").EndsWith("\"") == false)
                            /*{
                                string initial = prelucrare[i];
                                string[] varlist;
                                if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), ",");
                                else varlist = Regex.Split(prelucrare[i], ",");
                                varlist[0] = Regex.Replace(varlist[0].TrimStart(), "^" + WriteKeyword + " ", "");
                                for (int k = 0; k < varlist.Length - 1; k++)
                                {

                                    if (varlist[k].Contains("\"") && (!(varlist[k].StartsWith("\"") && varlist[k].EndsWith("\""))))
                                    {
                                    aici:
                                        try
                                        {
                                            varlist[k] += "," + varlist[k + 1];
                                            var list = new List<string>(varlist);
                                            list.RemoveAt(k + 1);
                                            varlist = list.ToArray();
                                            int count = Regex.Matches(varlist[k], "\"").Count;
                                            if (count % 2 != 0) goto aici;
                                        }
                                        catch { }
                                    }
                                }
                                prelucrare[i] = GetLeadingWhitespace(initial);
                                foreach (string ct in varlist)
                                {
                                    prelucrare[i] += "MsgBox " + ct.TrimStart().TrimEnd();
                                    if (ct != varlist[varlist.Length - 1]) prelucrare[i] += "\n" + GetLeadingWhitespace(initial);
                                }
                            }*/
                          /*  else
                            {
                                prelucrare[i] = GetLeadingWhitespace(prelucrare[i]) + "MsgBox " + Regex.Replace(prelucrare[i].TrimStart(), "^scrie ", "");
                            }*/
                        }
                    }
                    //Instructiunea daca
                    if (prelucrare[i].TrimStart().StartsWith(IfKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], IfKeyword + " (.*)", "If $1");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + " Then";
                        if (prelucrare[i].TrimStart().Contains(ThenKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + ThenKeyword, "");
                    }
                    //Instructiunea altfel
                    if (prelucrare[i].TrimStart().StartsWith(ElseKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], ElseKeyword, "Else");
                    //Instructiunea sfarsit_daca
                    if (prelucrare[i].TrimStart().StartsWith(EndIfKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], EndIfKeyword, "End If");
                    //Instructiunea cat timp
                    if (prelucrare[i].TrimStart().StartsWith(WhileKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], WhileKeyword + " (.*)", "Do While $1");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        if (prelucrare[i].TrimStart().Contains(DoKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + DoKeyword, "");
                    }
                    //Instructiunea sfarsit cat timp
                    if (prelucrare[i].TrimStart().StartsWith(EndWhileKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], EndWhileKeyword, "Loop");
                    //Definirea unui intreg
                    if (prelucrare[i].TrimStart().StartsWith(IntegerKeyword) & prelucrare[i].TrimStart().StartsWith("intreg.citeste") == false)
                    {
                        if (prelucrare[i].TrimStart().Contains(","))
                        {
                            string tt = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1");
                            string[] vars = Regex.Split(tt, ",");
                            foreach (string vars1 in vars)
                            {
                                string vars2 = vars1;
                                if (vars1.Contains("["))
                                {
                                    vars2 = vars1.Split('[')[0];
                                }
                                mat[nrcurent, 0] = vars2.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "int";
                                nrcurent += 1;
                                prelucrare[i] = prelucrare[i].Replace("][", ",");
                                prelucrare[i] = prelucrare[i].Replace("[", "(");
                                prelucrare[i] = prelucrare[i].Replace("]", ")");
                            }
                            prelucrare[i] = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "Dim $1");
                        }
                        else
                        {

                            string tt = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1");
                            //if (tt.Contains("a")) Debug.WriteLine(tt);
                            if (tt.Contains("["))
                            {
                                tt = tt.Split('[')[0];
                            }
                            mat[nrcurent, 0] = tt;
                            mat[nrcurent, 1] = "int";
                            nrcurent += 1;
                            prelucrare[i] = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "Dim $1");
                            prelucrare[i] = prelucrare[i].Replace("][", ",");
                            prelucrare[i] = prelucrare[i].Replace("[", "(");
                            prelucrare[i] = prelucrare[i].Replace("]", ")");
                        }
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //if (lang == "VB6") prelucrare[i] = prelucrare[i] + " as Long";
                    }
                    if (prelucrare[i].TrimStart().StartsWith(DoubleKeyword) & prelucrare[i].TrimStart().StartsWith("intreg.citeste") == false)
                    {
                        if (prelucrare[i].TrimStart().Contains(","))
                        {
                            string tt = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1");
                            string[] vars = Regex.Split(tt, ",");
                            foreach (string vars1 in vars)
                            {
                                string vars2 = vars1;
                                if (vars1.Contains("["))
                                {
                                    vars2 = vars1.Split('[')[0];
                                }
                                mat[nrcurent, 0] = vars2.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "double";
                                nrcurent += 1;
                                prelucrare[i] = prelucrare[i].Replace("][", ",");
                                prelucrare[i] = prelucrare[i].Replace("[", "(");
                                prelucrare[i] = prelucrare[i].Replace("]", ")");
                            }
                            prelucrare[i] = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "Dim $1");
                        }
                        else
                        {
                            string tt = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1");
                            if (tt.Contains("["))
                            {
                                tt = tt.Split('[')[0];
                            }
                            mat[nrcurent, 0] = tt;
                            mat[nrcurent, 1] = "double";
                            nrcurent += 1;
                            prelucrare[i] = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "Dim $1");
                            prelucrare[i] = prelucrare[i].Replace("][", ",");
                            prelucrare[i] = prelucrare[i].Replace("[", "(");
                            prelucrare[i] = prelucrare[i].Replace("]", ")");
                        }
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //if (lang == "VB6") prelucrare[i] = prelucrare[i] + " as Double";
                    }
                    if (prelucrare[i].TrimStart().StartsWith(StringKeyword) & prelucrare[i].TrimStart().StartsWith("sir.citeste") == false)
                    {
                        if (prelucrare[i].TrimStart().Contains(","))
                        {
                            string tt = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1");
                            string[] vars = Regex.Split(tt, ",");
                            foreach (string vars1 in vars)
                            {
                                string vars2 = vars1;
                                if (vars1.Contains("["))
                                {
                                    vars2 = vars1.Split('[')[0];
                                }
                                mat[nrcurent, 0] = vars2.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "string";
                                nrcurent += 1;
                                prelucrare[i] = prelucrare[i].Replace("][", ",");
                                prelucrare[i] = prelucrare[i].Replace("[", "(");
                                prelucrare[i] = prelucrare[i].Replace("]", ")");
                            }
                            prelucrare[i] = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "Dim $1");
                        }
                        else
                        {
                            string tt = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1");
                            if (tt.Contains("["))
                            {
                                tt = tt.Split('[')[0];
                            }
                            mat[nrcurent, 0] = tt;
                            mat[nrcurent, 1] = "string";
                            nrcurent += 1;
                            prelucrare[i] = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "Dim $1");
                            prelucrare[i] = prelucrare[i].Replace("][", ",");
                            prelucrare[i] = prelucrare[i].Replace("[", "(");
                            prelucrare[i] = prelucrare[i].Replace("]", ")");
                        }
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //if (lang == "VB6") prelucrare[i] = prelucrare[i] + " as String";
                    }
                    if (prelucrare[i].TrimStart().StartsWith(CharKeyword) & prelucrare[i].TrimStart().StartsWith("caracter.citeste") == false)
                    {
                        string tt = Regex.Replace(prelucrare[i], CharKeyword + " (.*)", "$1");
                        mat[nrcurent, 0] = tt;
                        mat[nrcurent, 1] = "char";
                        nrcurent += 1;
                        prelucrare[i] = Regex.Replace(prelucrare[i], CharKeyword + " (.*)", "Dim $1");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //if (lang == "VB6") prelucrare[i] = prelucrare[i] + " as Character";
                    }
                    //Operatia de citire de la tastatura a unui intreg
                    // (prelucrare[i].TrimStart().StartsWith("intreg.citeste")) prelucrare[i] = Regex.Replace(prelucrare[i], @"intreg.citeste \s*(.*?)(?:\s|$)", "$1 = CInt(InputBox(\"Introduceti de la tastatură un intreg. Acesta va reprezenta variabila $1.\"))");
                    //Operatia de citire de la tastatura a unui numar real
                    //if (prelucrare[i].TrimStart().StartsWith("real.citeste")) prelucrare[i] = Regex.Replace(prelucrare[i], @"real.citeste \s*(.*?)(?:\s|$)", "$1 = CLong(InputBox(\"Introduceti de la tastatură un numar real. Acesta va reprezenta variabila $1.\"))");
                    //Instructiunea pentru
                    if (prelucrare[i].TrimStart().StartsWith(ForKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], ForKeyword + " (.*)", "For $1");
                        prelucrare[i] = Regex.Replace(prelucrare[i], ",", " To ");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        if (prelucrare[i].TrimStart().Contains(DoKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + DoKeyword, "");
                    }
                    //Instructiunea sfarsit_pentru
                    if (prelucrare[i].TrimStart().StartsWith(EndForKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], EndForKeyword, "Next");
                    //Instructiunea executa
                    if (prelucrare[i].TrimStart().StartsWith(DoKeyword) && !prelucrare[i].TrimStart().StartsWith("double")) prelucrare[i] = Regex.Replace(prelucrare[i], DoKeyword, "Do");
                    //Instructiunea executa pana cand (loop until)
                    if (prelucrare[i].TrimStart().StartsWith(LoopUntilKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], LoopUntilKeyword + " (.*)", "Loop Until $1");
                        //prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                    }
                    //Prelucrari de final pentru cod Visual Basic
                    if (i == 0)
                        returnare = prelucrare[i];
                    else
                        returnare = returnare + "\n" + prelucrare[i];

                }
                #endregion
                #region C#
                if (lang == "C#")
                {
                    //Partea intreaga
                    if (prelucrare[i].Contains("{") && (prelucrare[i].Contains("}"))) prelucrare[i] = Regex.Replace(Regex.Replace(prelucrare[i], @"\{", "(int)Math.Floor((decimal)("), @"\}", "))");

                    if ((prelucrare[i].TrimStart().StartsWith(IfKeyword) | prelucrare[i].TrimStart().StartsWith(WhileKeyword) | prelucrare[i].TrimStart().StartsWith(LoopUntilKeyword))) prelucrare[i] = Regex.Replace(prelucrare[i], "[^!/</>]=", " ==");
                    //Sintaxa C#
                    if (prelucrare[i].Contains(OrKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + OrKeyword + " ", " | ");
                    if (prelucrare[i].Contains(AndKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + AndKeyword + " ", " && ");
                    //Verifica daca s-a gasit vreo instructiune
                    bool x2 = false;
                    /*if (prelucrare[i].Contains("adevarat") & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], "adevarat", "true");
                    if (prelucrare[i].Contains("fals") & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], "fals", "false");*/
                    //Ridicare la putere
                   /* if (prelucrare[i].TrimStart().Contains("^") && !prelucrare[i].StartsWith("//"))
                    {
                        char[] myString = prelucrare[i].ToString().ToCharArray();
                        int loc = 0;
                        for (int cnt = 0; cnt < myString.Length; cnt++)
                        {
                            if (myString[cnt] == '^')
                            {
                                loc = cnt;
                                break;
                            }
                        }
                        int inceput = 0;
                        int fin = 0;
                        int factor = 1;
                        string nr = "";
                        string exponent = "";
                        int oktoexit = 0;
                        for (int cnt = loc - 1; cnt > 0; cnt--)
                        {
                            if (myString[cnt].ToString() != " " && myString[cnt].ToString() != "(")
                            {
                                oktoexit = 1;
                                nr += myString[cnt].ToString();
                                inceput = cnt;
                            }
                            else if (oktoexit == 1) break;
                        }
                        factor = 1;
                        oktoexit = 0;
                        for (int cnt = loc + 1; cnt < myString.Length; cnt++)
                        {
                            if (myString[cnt].ToString() != " " && myString[cnt].ToString() != ")")
                            {
                                oktoexit = 1;
                                exponent += myString[cnt].ToString();
                                fin = cnt;
                            }
                            else if (oktoexit == 1) break;
                        }
                        prelucrare[i] = "";
                        for (int cnt = 0; cnt < inceput; cnt++)
                        {
                            prelucrare[i] += myString[cnt];
                        }
                        nr = Reverse(nr);
                        prelucrare[i] = prelucrare[i] + "Math.Pow(" + nr + ", " + exponent + ")";// +prelucrare[i].Substring(fin, prelucrare[i].Length - 1);
                        for (int cnt = fin + 1; cnt < myString.Length; cnt++)
                        {
                            prelucrare[i] += myString[cnt];
                        }
                    }*/
                    //Comentariu
                    if (prelucrare[i].TrimStart().StartsWith("//")) x2 = true;
                    //RidicareLaPutere
                    if (prelucrare[i].TrimStart().Contains("^") && !prelucrare[i].StartsWith("//"))
                    {
                        prelucrare[i] = TransformariCaciula(prelucrare[i], lang);
                    }
                    //if (prelucrare[i].TrimStart().StartsWith("daca") & prelucrare[i].Contains("=")) prelucrare[i] = Regex.Replace(prelucrare[i], "=", "==");
                    //Operatia de impartire
                    if (prelucrare[i].Contains("/") & !prelucrare[i].Contains("\"") & !prelucrare[i].TrimStart().StartsWith("//"))
                    {
                        string initialul = prelucrare[i].Replace(" ", String.Empty);
                        string cat = initialul.Split('=')[0];
                        //string deimpartit = initialul.Split('=')[1].Split('/')[0];
                        //string impartitor = initialul.Split('=')[1].Split('/')[1];
                        string tipcat = "";
                        //string tipdeimpartit = "";
                        //string tipimpartitor = "";
                        for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++)
                        {
                            if (mat[nt2, 0] != null)
                            {
                                if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == cat | mat[nt2, 0].TrimStart().TrimEnd() == cat)
                                {
                                    string tip2 = mat[nt2, 1];
                                    if (tip2 == "int") tipcat = "int";
                                    if (tip2 == "double") tipcat = "double";
                                }
                                /*if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == deimpartit | mat[nt2, 0].TrimStart().TrimEnd() == deimpartit)
                                {
                                    string tip2 = mat[nt2, 1];
                                    if (tip2 == "int") tipdeimpartit = "int";
                                    if (tip2 == "double") tipdeimpartit = "double";
                                }
                                if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == impartitor | mat[nt2, 0].TrimStart().TrimEnd() == impartitor)
                                {

                                    string tip2 = mat[nt2, 1];
                                    if (tip2 == "int") tipimpartitor = "int";
                                    if (tip2 == "double") tipimpartitor = "double";
                                }*/
                            }
                        }
                        if (tipcat == "double")
                        {
                            string[] finish;
                            finish = prelucrare[i].Split('=');
                            prelucrare[i] = finish[0] + "= (float)" + finish[1].Substring(1, finish[1].Length - 1);
                            //prelucrare[i] = Regex.Replace(prelucrare[i], GetLeadingWhitespace(prelucrare[i], false) + cat + " = (float)" + deimpartit + " / " + impartitor;
                        }
                    }
                    //Definirea unui intreg
                    if (prelucrare[i].TrimStart().StartsWith(IntegerKeyword) & prelucrare[i].TrimStart().StartsWith("intreg.citeste") == false)
                    {
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        if (prelucrare[i].TrimStart().Contains(","))
                        {
                            string tt = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1");
                            string[] vars = Regex.Split(tt, ",");
                            foreach (string vars1 in vars)
                            {
                                string vars2 = vars1;
                                if (vars1.Contains("["))
                                {
                                    vars2 = vars1.Split('[')[0];
                                }
                                mat[nrcurent, 0] = vars2.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "int";
                                nrcurent += 1;
                            }
                            if (!prelucrare[i].Contains("["))
                                prelucrare[i] = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "int $1");
                            else
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1");
                                string[] st = prelucrare[i].Split(',');
                                prelucrare[i] = "";
                                foreach (string s in st)
                                {
                                    if (s.Contains("["))
                                    {
                                        prelucrare[i] += s.Split('[')[0];
                                        string v = Regex.Replace(s, s.Split('[')[0], "");
                                        prelucrare[i] = "int" + Regex.Replace(v, "[^\\[-\\]]", "") + " " + prelucrare[i] + " = new int" + Regex.Replace(s, s.Split('[')[0], "");
                                    }
                                    else prelucrare[i] += "int " + s;
                                    prelucrare[i] += "; ";
                                    prelucrare[i] = prelucrare[i].Replace("][", ",");
                                }
                                if (prelucrare[i].EndsWith("; ")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 2);
                            }
                            //prelucrare[i] = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "int $1");
                        }
                        else
                        {
                            string tt = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1");
                            if (tt.Contains("["))
                            {
                                tt = tt.Split('[')[0];
                            }
                            mat[nrcurent, 0] = tt;
                            mat[nrcurent, 1] = "int";
                            nrcurent += 1;
                            if (!prelucrare[i].Contains("["))
                                prelucrare[i] = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "int $1");
                            else
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1");
                                string s = prelucrare[i];
                                prelucrare[i] = "";
                                prelucrare[i] += s.Split('[')[0];
                                string v = Regex.Replace(s, s.Split('[')[0], "");
                                prelucrare[i] = "int" + Regex.Replace(v, "[^\\[-\\]]", "") + " " + prelucrare[i] + " = new int" + Regex.Replace(s, s.Split('[')[0], "");
                                prelucrare[i] = prelucrare[i].Replace("][", ",");
                            }
                            //prelucrare[i] = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "int $1");
                        }

                        /*string tt = Regex.Replace(prelucrare[i], "intreg (.*)", "$1");
                        mat[nrcurent, 0] = tt;
                        mat[nrcurent, 1] = "int";
                        nrcurent += 1;
                        prelucrare[i] = Regex.Replace(prelucrare[i], "intreg (.*)", "int $1");*/
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ";";
                        x2 = true;
                    }
                    //Definirea unui nr real
                    if (prelucrare[i].TrimStart().StartsWith(DoubleKeyword) & prelucrare[i].TrimStart().StartsWith("real.citeste") == false)
                    {
                        //string tt = Regex.Replace(prelucrare[i], "real (.*)", "$1");
                        if (prelucrare[i].TrimStart().Contains(","))
                        {
                            string tt = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1");
                            string[] vars = Regex.Split(tt, ",");
                            foreach (string vars1 in vars)
                            {
                                string vars2 = vars1;
                                if (vars1.Contains("["))
                                {
                                    vars2 = vars1.Split('[')[0];
                                }
                                mat[nrcurent, 0] = vars2.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "double";
                                nrcurent += 1;
                            }
                            if (!prelucrare[i].Contains("["))
                                prelucrare[i] = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "double $1");
                            else
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1");
                                string[] st = prelucrare[i].Split(',');
                                prelucrare[i] = "";
                                foreach (string s in st)
                                {
                                    if (s.Contains("["))
                                    {
                                        prelucrare[i] += s.Split('[')[0];
                                        string v = Regex.Replace(s, s.Split('[')[0], "");
                                        prelucrare[i] = "double" + Regex.Replace(v, "[^\\[-\\]]", "") + " " + prelucrare[i] + " = new double" + Regex.Replace(s, s.Split('[')[0], "");
                                    }
                                    else prelucrare[i] += "double " + s;
                                    prelucrare[i] += "; ";
                                    prelucrare[i] = prelucrare[i].Replace("][", ",");

                                }
                                if (prelucrare[i].EndsWith("; ")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 2);
                            }
                            //prelucrare[i] = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "double $1");
                        }
                        else
                        {
                            string tt = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1");
                            if (tt.Contains("["))
                            {
                                tt = tt.Split('[')[0];
                            }
                            mat[nrcurent, 0] = tt;
                            mat[nrcurent, 1] = "double";
                            nrcurent += 1;
                            if (!prelucrare[i].Contains("["))
                                prelucrare[i] = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "double $1");
                            else
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1");
                                string s = prelucrare[i];
                                prelucrare[i] = "";
                                prelucrare[i] += s.Split('[')[0];
                                string v = Regex.Replace(s, s.Split('[')[0], "");
                                prelucrare[i] = "double" + Regex.Replace(v, "[^\\[-\\]]", "") + " " + prelucrare[i] + " = new double" + Regex.Replace(s, s.Split('[')[0], "");
                                prelucrare[i] = prelucrare[i].Replace("][", ",");

                            }
                           // prelucrare[i] = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "double $1");
                        }
                        /*mat[nrcurent, 0] = tt;
                        mat[nrcurent, 1] = "double";
                        nrcurent += 1;
                        prelucrare[i] = Regex.Replace(prelucrare[i], "real (.*)", "double $1");*/
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ";";
                        x2 = true;
                    }
                    //Definirea unui sir de caractere
                    if (prelucrare[i].TrimStart().StartsWith(StringKeyword) & prelucrare[i].TrimStart().StartsWith("sir.citeste") == false)
                    {
                        //string tt = Regex.Replace(prelucrare[i], "sir (.*)", "$1");
                        if (prelucrare[i].TrimStart().Contains(","))
                        {
                            string tt = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1");
                            string[] vars = Regex.Split(tt, ",");
                            foreach (string vars1 in vars)
                            {
                                string vars2 = vars1;
                                if (vars1.Contains("["))
                                {
                                    vars2 = vars1.Split('[')[0];
                                }
                                mat[nrcurent, 0] = vars2.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "string";
                                nrcurent += 1;
                            }
                            if (!prelucrare[i].Contains("["))
                                prelucrare[i] = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "string $1");
                            else
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1");
                                string[] st = prelucrare[i].Split(',');
                                prelucrare[i] = "";
                                foreach (string s in st)
                                {
                                    if (s.Contains("["))
                                    {
                                        prelucrare[i] += s.Split('[')[0];
                                        string v = Regex.Replace(s, s.Split('[')[0], "");
                                        prelucrare[i] = "string" + Regex.Replace(v, "[^\\[-\\]]", "") + " " + prelucrare[i] + " = new string" + Regex.Replace(s, s.Split('[')[0], "");
                                    }
                                    else prelucrare[i] += "string " + s;
                                    prelucrare[i] += "; ";
                                    prelucrare[i] = prelucrare[i].Replace("][", ",");

                                }
                                if (prelucrare[i].EndsWith("; ")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 2);
                            }
                            //prelucrare[i] = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "string $1");
                        }
                        else
                        {
                            string tt = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1");
                            if (tt.Contains("["))
                            {
                                tt = tt.Split('[')[0];
                            }
                            mat[nrcurent, 0] = tt;
                            mat[nrcurent, 1] = "string";
                            nrcurent += 1;
                            if (!prelucrare[i].Contains("["))
                                prelucrare[i] = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "string $1");
                            else
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1");
                                string s = prelucrare[i];
                                prelucrare[i] = "";
                                prelucrare[i] += s.Split('[')[0];
                                string v = Regex.Replace(s, s.Split('[')[0], "");
                                prelucrare[i] = "string" + Regex.Replace(v, "[^\\[-\\]]", "") + " " + prelucrare[i] + " = new string" + Regex.Replace(s, s.Split('[')[0], "");
                                prelucrare[i] = prelucrare[i].Replace("][", ",");

                            }
                            //prelucrare[i] = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "string $1");
                        }
                        /*mat[nrcurent, 0] = tt;
                        mat[nrcurent, 1] = "string";
                        nrcurent += 1;
                        prelucrare[i] = Regex.Replace(prelucrare[i], "sir (.*)", "string $1");*/
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ";";
                        x2 = true;
                    }
                    //Definirea unui caracter
                    if (prelucrare[i].TrimStart().StartsWith(CharKeyword) & prelucrare[i].TrimStart().StartsWith("caracter.citeste") == false)
                    {
                        string tt = Regex.Replace(prelucrare[i], CharKeyword + " (.*)", "$1");
                        mat[nrcurent, 0] = tt;
                        mat[nrcurent, 1] = "char";
                        nrcurent += 1;
                        prelucrare[i] = Regex.Replace(prelucrare[i], CharKeyword + " (.*)", "char $1");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ";";
                        x2 = true;
                    }
                    if (queryVar != "")
                    {
                            for (int nt = 0; nt < (mat.Length / 2); nt++)
                            {
                                //System.Diagnostics.Debug.WriteLine(mat[nt, 0]);
                                if (mat[nt, 0] == queryVar + "\r" | mat[nt, 0] == queryVar)
                                {
                                    string tip = mat[nt, 1];
                                    if (tip == "int")
                                    {
                                        queryVar = "int";
                                    }
                                    if (tip == "double")
                                    {
                                        queryVar = "double";
                                    }
                                    if (tip == "string")
                                    {
                                        queryVar = "string";
                                    }
                                    if (tip == "char")
                                    {
                                        queryVar = "char";
                                    }
                                }
                            }
                    }
                    //Operatia de citire de la tastatura
                    if (prelucrare[i].TrimStart().StartsWith(ReadKeyword))
                    {
                        if (!prelucrare[i].Contains(','))
                        {
                            string initial = prelucrare[i].TrimStart();
                            string ct = Regex.Replace(prelucrare[i].TrimStart(), ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1");
                            if (ct.Contains("[")) ct = ct.Split('[')[0];
                            for (int nt = 0; nt < (mat.Length / 2); nt++)
                            {
                                //System.Diagnostics.Debug.WriteLine(mat[nt, 0]);
                                if (mat[nt, 0] == ct + "\r" | mat[nt, 0] == ct)
                                {
                                    string tip = mat[nt, 1];
                                    if (tip == "int")
                                    {
                                        prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1 = (int)Math.Floor(decimal.Parse(Console.ReadLine()));");
                                    }
                                    if (tip == "double")
                                    {
                                        prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1 = double.Parse(Console.ReadLine());");
                                    }
                                    if (tip == "string")
                                    {
                                        prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1 = Console.ReadLine();");
                                    }
                                    if (tip == "char")
                                    {
                                        prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1 = char.Parse(Console.ReadLine());");
                                    }
                                }
                                /*if (prelucrare[i] == initial)
                                {

                                    buttonItem3.Visible = true;
                                    buttonItem3.Text = superTabControl1.SelectedTab.Text + ": " + UndeclaredVariable + ct + ".";
                                    buttonItem3.Tag = (i + 1).ToString() + "." + "1" + "." + "1";
                                    handluit = false;

                                    //warningBox1.Text = "<b>Eroare</b>: Variabilă nedeclarată: <i>" + ct + "</i>";
                                }
                                else
                                {
                                    if (handluit == false)
                                    {
                                        buttonItem3.Visible = false;
                                        handluit = true;
                                    }
                                }*/
                            }
                        }
                        else
                        {

                            string initial = prelucrare[i];
                            string[] varlist;
                            if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), ",");
                            else varlist = Regex.Split(prelucrare[i], ",");
                            varlist[0] = Regex.Replace(varlist[0], ReadKeyword + " ", "");
                            prelucrare[i] = GetLeadingWhitespace(initial);
                            foreach (string ct in varlist)
                            {

                                for (int nt = 0; nt < (mat.Length / 2); nt++)
                                {
                                    string ct2 = ct;
                                    if (ct.Contains("[")) ct2 = ct.Split('[')[0];

                                    //System.Diagnostics.Debug.WriteLine(mat[nt, 0]);
                                    if (mat[nt, 0] == ct2.TrimStart().TrimEnd() + "\r" | mat[nt, 0] == ct2.TrimStart().TrimEnd())
                                    {
                                        string tip = mat[nt, 1];
                                        if (tip == "int")
                                        {
                                            prelucrare[i] += ct.TrimStart() + " = (int)Math.Floor(decimal.Parse(Console.ReadLine()));";
                                        }
                                        if (tip == "double")
                                        {
                                            prelucrare[i] += ct.TrimStart() + " = double.Parse(Console.ReadLine());";
                                        }
                                        if (tip == "string")
                                        {
                                            prelucrare[i] += ct.TrimStart() + " = Console.ReadLine();";
                                        }
                                        if (tip == "char")
                                        {
                                            prelucrare[i] += ct.TrimStart() + " = char.Parse(Console.ReadLine());";
                                        }
                                    }
                                }
                            }
                        }
                        prelucrare[i] = prelucrare[i].Replace("][", ",");
                        x2 = true;
                    }
                    //Operatia de scriere pe ecran
                    if (prelucrare[i].TrimStart().StartsWith(WriteKeyword))
                    {
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        if (!prelucrare[i].Contains(','))
                        {
                            string ad = GetLeadingWhitespace(prelucrare[i]);
                            prelucrare[i] = Regex.Replace(prelucrare[i], WriteKeyword + @" (.*)$", "$1");
                            prelucrare[i] = ad + "Console.Write(" + ToLiteral(prelucrare[i].Trim(), "CSharp");
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            //if (prelucrare[i].TrimStart().StartsWith("Console.WriteLine(\"") && !prelucrare[i].EndsWith("\"")) prelucrare[i] += "\"";
                            prelucrare[i] = prelucrare[i] + ");";
                        }
                        else
                        {
                            if (prelucrare[i].TrimStart().Replace("^" + WriteKeyword + " ", "").StartsWith("\"") == false && (prelucrare[i].TrimStart().Replace(WriteKeyword + " ", "").EndsWith("\"") == false || prelucrare[i].TrimStart().Substring(0, prelucrare[i].TrimStart().Length - 1).Replace(WriteKeyword + " ", "").EndsWith("\"") == false))
                            {
                                string initial = prelucrare[i];
                                string[] varlist;
                                //Debug.WriteLine(prelucrare[i]);
                                prelucrare[i] = Regex.Replace(prelucrare[i], ", ", ",");
                                string virgula = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
                                if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), virgula);
                                else varlist = Regex.Split(prelucrare[i], virgula);
                                /* if (prelucrare[i].TrimStart().Replace("^scrie ", "").StartsWith("\"") == false && prelucrare[i].TrimStart().Replace("scrie ", "").EndsWith("\"") == false)
                                 {
                                     string initial = prelucrare[i];
                                     string[] varlist;
                                     if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), ",");
                                     else varlist = Regex.Split(prelucrare[i], ",");*/
                                varlist[0] = Regex.Replace(varlist[0].TrimStart(), "^" + WriteKeyword + " ", "");
                                for (int k = 0; k < varlist.Length - 1; k++)
                                {

                                    if (varlist[k].Contains("\"") && (!(varlist[k].StartsWith("\"") && varlist[k].EndsWith("\""))))
                                    {
                                    aici:
                                        try
                                        {
                                            varlist[k] += "," + varlist[k + 1];
                                            var list = new List<string>(varlist);
                                            list.RemoveAt(k + 1);
                                            varlist = list.ToArray();
                                            int count = Regex.Matches(varlist[k], "\"").Count;
                                            if (count % 2 != 0) goto aici;
                                        }
                                        catch { }
                                    }
                                }
                                prelucrare[i] = GetLeadingWhitespace(initial);
                                foreach (string ct in varlist)
                                {
                                    prelucrare[i] += "Console.Write(" + ToLiteral(ct.TrimStart().TrimEnd(), "CSharp") + "); ";
                                }
                                //prelucrare[i] += "Console.Write(\"\\n\");";
                            }
                            else
                            {
                                prelucrare[i] = GetLeadingWhitespace(prelucrare[i]) + "Console.Write(" + ToLiteral(Regex.Replace(prelucrare[i].TrimStart(), "^" + WriteKeyword + " ", ""), "CSharp") + "); ";
                            }
                        }
                        prelucrare[i] = Regex.Replace(prelucrare[i], "\\]\\[(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", ",");

                        x2 = true;
                    }
                    //Ridicare la putere
                    prelucrare[i] = prelucrare[i].Replace("@_@@", ",");

                    /*if (prelucrare[i].TrimStart().Contains("^") && !prelucrare[i].StartsWith("//"))
                    {
                        if (lang == "C#") prelucrare[i] = prelucrare[i].Replace(",", "$_$$$$");
                          if (lang == "C#") prelucrare[i] = prelucrare[i].Replace("Console.WriteLine(", "@_@@@@");
                          if (lang == "C#") prelucrare[i] = prelucrare[i].Replace(");", "!_!!!!");
                        if (prelucrare[i].EndsWith("\r"))
                        {
                            prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        }
                        //string[] text = prelucrare[i].Split('^');
                        var arr = Regex.Matches(prelucrare[i], @"(|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z\(\(|\d|\^|\(|\)| |""|\.)(a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|\d|\^|\(|\)| |""|\.)+").Cast<Match>().Select(m => m.Value).ToArray();
                        var arr2 = Regex.Matches(prelucrare[i], @"(|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z\(\(|\d|\^|\(|\)| |""|\.)(a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|\d|\^|\(|\)| |""|\.)+").Cast<Match>().Select(m => m.Value).ToArray();
                        string ceramane = Regex.Replace(prelucrare[i], @"(|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z\(\(|\d|\^|\(|\)| |""|\.)(a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|\d|\^|\(|\)| |""|\.)+", "#_####");


                        for (int il = 0; il < arr.Length; il++)
                        {
                            if (!arr[il].TrimStart().StartsWith("\""))
                            {

                                if (arr[il].Contains('^'))
                                {
                                    bool count = false;
                                    string strInceput = "";
                                    string strText = "";
                                    string strFinal = "";
                                    int a = 0;
                                    string[] text = arr[il].Split('^');
                                    for (int ix = 0; ix < text.Length; ix++)
                                    {
                                        if (ix == text.Length - 1)
                                        {
                                            //   strText += text[ix].Trim();
                                            //  count = false;
                                            text[ix] = text[ix].Trim();
                                            //   if (text[ix].Contains("("))
                                            //   {
                                            //  text[ix] = text[ix].Replace("(", "pow(");
                                            //  text[ix] = text[ix].Trim() + ", ";
                                            //  a++;
                                            //    }
                                            //   else
                                            //  {
                                            //  if (!text[ix].Contains(")"))
                                            //   {
                                            //       text[ix] = "pow(" + text[ix].Trim() + ", ";
                                            //       a++;
                                            //    }
                                            //    else
                                            //    {
                                            //       int b = text[ix].Split(')').Length - 1;
                                            //if (text[ix - b].Contains("pow"))
                                            //  {
                                            //          text[ix - b] = "pow(" + text[ix - b];
                                            //     if (!text[ix].Contains(")"))
                                            //     {
                                            //       text[ix] = text[ix].Trim() + "), ";
                                            //     }
                                            //    else
                                            //    {
                                            //        text[ix] = text[ix].Trim() + ", ";
                                            //        a = a - b;
                                            //    }
                                            // }
                                            //   else text[ix] = text[ix].Trim() + ", ";
                                            //  }
                                            //  }
                                        }
                                        else
                                        {
                                            if (text[ix].Contains("("))
                                            {
                                                text[ix] = text[ix].Replace("(", "Math.Pow(");
                                                text[ix] = text[ix].Trim() + ", ";
                                                a++;
                                            }
                                            else
                                            {
                                                if (!text[ix].Contains(")"))
                                                {
                                                    text[ix] = "Math.Pow(" + text[ix].Trim() + ", ";
                                                    a++;
                                                }
                                                else
                                                {
                                                    int b = text[ix].Split(')').Length - 1;
                                                    if (text[ix - b].Contains("Math.Pow"))
                                                    {
                                                        text[ix - b] = "Math.Pow(" + text[ix - b];
                                                        if (!text[ix].Contains(")"))
                                                        {
                                                            text[ix] = text[ix].Trim() + "), ";
                                                        }
                                                        else
                                                        {
                                                            text[ix] = text[ix].Trim() + ", ";
                                                            //  a = a - b;
                                                        }
                                                    }
                                                    else text[ix] = text[ix].Trim() + ", ";
                                                }
                                            }
                                        }

                                    }
                                    a = 0;
                                    for (int ix = 0; ix < text.Length; ix++)
                                    {
                                        a += text[ix].Split('(').Length - 1;
                                        a -= text[ix].Split(')').Length - 1;
                                    }
                                    for (int ix = 1; ix <= a; ix++)
                                        text[text.Length - 1] += ")";
                                    strText = "";
                                    for (int ix = 0; ix < text.Length; ix++)
                                    {
                                        strText += text[ix];
                                    }
                                    arr[il] = strText;
                                }
                            }
                        }
                        prelucrare[i] = "";
                        string[] finalizare = Regex.Split(ceramane, "#_####");
                        for (int il = 0; il < finalizare.Length; il++)
                        {
                            // if (!arr[il].TrimStart().StartsWith("\""))
                            // {
                            if (!(il == finalizare.Length - 1))
                            {
                                if (arr2[il].StartsWith("(") && arr2[il].EndsWith(")"))
                                {
                                    prelucrare[i] += finalizare[il] + "(" + arr[il] + ")";
                                }
                                else
                                {
                                    prelucrare[i] += finalizare[il] + arr[il];
                                }
                            }
                            else prelucrare[i] += finalizare[il];
                            // if (arr2[il].StartsWith("(") && arr2[il].EndsWith(")"))
                            // {
                            //      prelucrare[i] = Regex.Replace(prelucrare[i], arr2[il], "(" + arr[il] + ")", RegexOptions.;
                            // }
                            // else prelucrare[i] = prelucrare[i].Replace(arr2[il], arr[il]);
                            // }
                        }
                        /* if (prelucrare[i].StartsWith("alertpow"))
                         {
                             prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1).Replace("alert", "alert(") + ");";
                         }*/
                       // if (!prelucrare[i].EndsWith(";")) prelucrare[i] += ";";
                   //     if (lang == "C#") prelucrare[i] = prelucrare[i].Replace("$_$$$$", ",");
                   //     if (lang == "C#") prelucrare[i] = prelucrare[i].Replace("@_@@@@", "Console.WriteLine(");
                    //      if (lang == "C#") prelucrare[i] = prelucrare[i].Replace("!_!!!!", ");");
                        /*char[] myString = prelucrare[i].ToString().ToCharArray();
                        int loc = 0;
                        for (int cnt = 0; cnt < myString.Length; cnt++)
                        {
                            if (myString[cnt] == '^')
                            {
                                loc = cnt;
                                break;
                            }
                        }
                        int inceput = 0;
                        int fin = 0;
                        int factor = 1;
                        string nr = "";
                        string exponent = "";
                        int oktoexit = 0;
                        for (int cnt = loc - 1; cnt > 0; cnt--)
                        {
                            if (myString[cnt].ToString() != " " && myString[cnt].ToString() != "(")
                            {
                                oktoexit = 1;
                                nr += myString[cnt].ToString();
                                inceput = cnt;
                            }
                            else if (oktoexit == 1) break;
                        }
                        factor = 1;
                        oktoexit = 0;
                        for (int cnt = loc + 1; cnt < myString.Length; cnt++)
                        {
                            if (myString[cnt].ToString() != " " && myString[cnt].ToString() != ")")
                            {
                                oktoexit = 1;
                                exponent += myString[cnt].ToString();
                                fin = cnt;
                            }
                            else if (oktoexit == 1) break;
                        }
                        prelucrare[i] = "";
                        for (int cnt = 0; cnt < inceput; cnt++)
                        {
                            prelucrare[i] += myString[cnt];
                        }
                        if (nr.EndsWith("\r")) nr = nr.Substring(0, nr.Length - 1);
                        nr = Reverse(nr);
                        if (nr.EndsWith("\r")) nr = nr.Substring(0, nr.Length - 1);
                        if (exponent.EndsWith("\r")) exponent = exponent.Substring(0, exponent.Length - 1);
                        prelucrare[i] = prelucrare[i] + "pow(" + nr + ", " + exponent + ")";// +prelucrare[i].Substring(fin, prelucrare[i].Length - 1);
                        for (int cnt = fin + 1; cnt < myString.Length; cnt++)
                        {
                            prelucrare[i] += myString[cnt];
                        }*/
                //    }
                    //Instructiunea daca
                    if (prelucrare[i].TrimStart().StartsWith(IfKeyword))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], IfKeyword + " (.*)", "if ($1");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ")\n            " + spaces + "{";
                        if (prelucrare[i].TrimStart().Contains(ThenKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + ThenKeyword, "");
                        x2 = true;
                    }
                    //Instructiunea altfel
                    if (prelucrare[i].TrimStart().StartsWith(ElseKeyword))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], ElseKeyword, "}\n            " + spaces + "else\n            " + spaces + "{");
                        x2 = true;
                    }
                    //Instructiunea sfarsit_daca
                    if (prelucrare[i].TrimStart().StartsWith(EndIfKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], EndIfKeyword, "}");
                        x2 = true;
                    }
                    //Instructiunea cat timp
                    if (prelucrare[i].TrimStart().StartsWith(WhileKeyword))
                    {
                        if (prelucrare[i].TrimStart().Contains(DoKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + DoKeyword, "");
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], WhileKeyword + " (.*)", "while ($1");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ")\n            " + spaces + "{";
                        x2 = true;
                    }
                    //Instructiunea sfarsit cat timp
                    if (prelucrare[i].TrimStart().StartsWith(EndWhileKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], EndWhileKeyword, "}");
                        x2 = true;
                    }
                    //Operatia de citire de la tastatura a unui intreg
                    if (prelucrare[i].TrimStart().StartsWith("intreg.citeste"))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], @"intreg.citeste \s*(.*?)(?:\s|$)", "cin>>$1;");
                        x2 = true;
                    }
                    //Operatia de citire de la tastatura a unui numar real
                    if (prelucrare[i].TrimStart().StartsWith("real.citeste"))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], @"real.citeste \s*(.*?)(?:\s|$)", "cin>>$1;");
                        x2 = true;
                    }
                    //Instructiunea pentru
                    if (prelucrare[i].TrimStart().StartsWith(ForKeyword))
                    {
                        if (prelucrare[i].TrimStart().Contains(DoKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + DoKeyword, "");
                        string a1 = Regex.Replace(prelucrare[i], ForKeyword + " (.*)", "$1");
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        //prelucrare[i] = Regex.Replace(prelucrare[i], "pentru (.*)", "for ($1");
                        string[] a2 = Regex.Split(a1.TrimStart(), ",");
                        try
                        {
                            string fin = "";
                            for (int sks = 1; sks < a2.Length; sks++)
                            {
                                if (sks == a2.Length - 1) fin += a2[sks];
                                else fin += a2[sks] + ",";
                            }
                            a2[1] = fin;
                        }
                        catch { }
                        string nume_var = sep(a2[0], "=");
                        bool edec = false;
                        for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++) if (mat[nt2, 0] != null && mat[nt2, 0].TrimStart().TrimEnd() + "\r" == nume_var | mat[nt2, 0].TrimStart().TrimEnd() == nume_var | mat[nt2, 0].TrimStart().TrimEnd() + "\n" == nume_var | mat[nt2, 0].TrimStart().TrimEnd() + " " == nume_var) edec = true;
                        try
                        {
                            if (edec == true)
                            {
                                prelucrare[i] = spaces + "for (" + a2[0] + "; " + nume_var + " <= " + a2[1]; // +"; " + nume_var + "++)";
                            }
                            else
                            {
                                prelucrare[i] = spaces + "for (int " + a2[0] + "; " + nume_var + " <= " + a2[1]; // +"; " + nume_var + "++)";
                            }
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            prelucrare[i] = prelucrare[i] + "; " + nume_var + "++)\n            " + spaces + "{ ";
                            x2 = true;
                        }
                        catch
                        {
                            prelucrare[i] = "";
                        }
                        x2 = true;
                    }
                    //Instructiunea sfarsit_pentru
                    if (prelucrare[i].TrimStart().StartsWith(EndForKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], EndForKeyword, "}");
                        x2 = true;
                    }
                    //Instructiunea executa
                    if (prelucrare[i].TrimStart().StartsWith(DoKeyword) && !prelucrare[i].TrimStart().StartsWith("double"))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], DoKeyword, "do\n            " + spaces + "{");
                        x2 = true;
                    }
                    //Instructiunea executa pana cand (loop until)
                    if (prelucrare[i].TrimStart().StartsWith(LoopUntilKeyword))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], LoopUntilKeyword + " (.*)", "}\n            " + spaces + "while ($1");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ");";
                        if (!prelucrare[i].Contains("="))
                        {
                            if (prelucrare[i].Contains("<"))
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], "<", ">=");
                            }
                            else
                            {
                                if (prelucrare[i].Contains(">")) prelucrare[i] = Regex.Replace(prelucrare[i], ">", "<=");
                            }
                        }
                        else
                        {
                            prelucrare[i] = Regex.Replace(prelucrare[i], ">=", "<");
                            prelucrare[i] = Regex.Replace(prelucrare[i], "<=", ">");
                        }
                        x2 = true;
                        //prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                    }
                    //Prelucrari de final pentru cod C++
                    if (x2 == true)
                    {
                        if (i == 0)
                            returnare = "            " + prelucrare[i] + "\n";
                        else
                            returnare = returnare + "            " + prelucrare[i] + "\n";
                    }
                    else
                    {
                        if (prelucrare[i] != Environment.NewLine & prelucrare[i] != "")
                        {
                            if (i == 0)
                            {
                                returnare = "            " + prelucrare[i];
                                if (returnare.EndsWith("\r")) returnare = returnare.Substring(0, returnare.Length - 1);
                                returnare += ";\n";
                            }
                            else
                            {
                                returnare = returnare + "            " + prelucrare[i];
                                if (returnare.EndsWith("\r")) returnare = returnare.Substring(0, returnare.Length - 1);
                                returnare = returnare + ";\n";
                            }
                        }
                    }
                }
                #endregion
                #region C++/JavaScript
                if (lang == "CPP" | lang == "JAVASCRIPT")
                {
                    //Partea intreaga
                    if (lang == "JAVASCRIPT")
                    {
                        if (prelucrare[i].Contains("{") && (prelucrare[i].Contains("}"))) prelucrare[i] = Regex.Replace(Regex.Replace(prelucrare[i], "\\{", "Math.floor("), "\\}", ")");
                    }
                    else
                    {
                        if (prelucrare[i].Contains("{") && (prelucrare[i].Contains("}"))) prelucrare[i] = Regex.Replace(Regex.Replace(prelucrare[i], "\\{", "(int)floor("), "\\}", ")");
                    }


                    //Sintaxa CPP
                    if (prelucrare[i].Contains(OrKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + OrKeyword + " ", " | ");
                    if (prelucrare[i].Contains(AndKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + AndKeyword + " ", " && ");
                    //Verifica daca s-a gasit vreo instructiune
                    bool x = false;
                    /*if (prelucrare[i].Contains("adevarat") & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], "adevarat", "true");
                    if (prelucrare[i].Contains("fals") & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], "fals", "false");*/
                    //Comentariu
                    if (prelucrare[i].TrimStart().StartsWith("//")) x = true;
                    //Egal in if
                    if ((prelucrare[i].TrimStart().StartsWith(IfKeyword) | prelucrare[i].TrimStart().StartsWith(EndWhileKeyword) | prelucrare[i].TrimStart().StartsWith(LoopUntilKeyword))) prelucrare[i] = Regex.Replace(prelucrare[i], "[^!/</>]=", " ==");
                    //Verificare daca mod este posibil
                    /*if (prelucrare[i].Contains("%"))
                    if (lang == "CPP")
                    {
                        //Operatia de impartire
                        if (prelucrare[i].Contains("%") & !prelucrare[i].Contains("\"") & !prelucrare[i].TrimStart().StartsWith("//"))
                        {
                            string initialul = prelucrare[i].Replace(" ", String.Empty);
                            string cat = initialul.Split('=')[0];
                            string tipcat = "";
                            for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++)
                            {
                                if (mat[nt2, 0] != null)
                                {
                                    if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == cat | mat[nt2, 0].TrimStart().TrimEnd() == cat)
                                    {
                                        string tip2 = mat[nt2, 1];
                                        if (tip2 == "int") tipcat = "int";
                                        if (tip2 == "double") tipcat = "double";
                                    }
                                }
                            }
                            if (tipcat == "double")
                            {
                                
                                //prelucrare[i] = finish[0] + "= fmod(" + finish[1].Substring(1, finish[1].Length - 2) + ")";
                            }
                        }
                    }*/
                    //Linie noua
                    if (prelucrare[i].Contains("\\n") && lang == "JAVASCRIPT")
                    {
                        prelucrare[i] = prelucrare[i].Replace("\\n", "<br>");
                    }
                    //Operator %
                    if (prelucrare[i].TrimStart().Contains("%") && !prelucrare[i].StartsWith("//") && lang == "CPP")
                    {
                        prelucrare[i] = TransformariModulo(prelucrare[i], lang);
                    }
                    //RidicareLaPutere
                    if (prelucrare[i].TrimStart().Contains("^") && !prelucrare[i].StartsWith("//"))
                    {
                        prelucrare[i] = TransformariCaciula(prelucrare[i], lang);
                    }
                    //Operatorul div
                    if (lang == "JAVASCRIPT")
                    {
                        //Operatia de impartire
                        if (prelucrare[i].Contains("/") & !prelucrare[i].Contains("\"") & !prelucrare[i].TrimStart().StartsWith("//"))
                        {
                            string initialul = prelucrare[i].Replace(" ", String.Empty);
                            string cat = initialul.Split('=')[0];
                            string tipcat = "";
                            for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++)
                            {
                                if (mat[nt2, 0] != null)
                                {
                                    if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == cat | mat[nt2, 0].TrimStart().TrimEnd() == cat)
                                    {
                                        string tip2 = mat[nt2, 1];
                                        if (tip2 == "int") tipcat = "int";
                                        if (tip2 == "double") tipcat = "double";
                                    }
                                }
                            }
                            if (tipcat == "int")
                            {
                                string[] finish;
                                finish = prelucrare[i].Split('=');
                                prelucrare[i] = finish[0] + "= Math.floor(" + finish[1].Substring(1, finish[1].Length - 1) + ")";
                            }
                        }
                    }
                    else
                    {
                        //Operatia de impartire
                        if (prelucrare[i].Contains("/") & !prelucrare[i].Contains("\"") & !prelucrare[i].TrimStart().StartsWith("//"))
                        {
                            string initialul = prelucrare[i].Replace(" ", String.Empty);
                            string cat = initialul.Split('=')[0];
                            string tipcat = "";
                            for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++)
                            {
                                if (mat[nt2, 0] != null)
                                {
                                    if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == cat | mat[nt2, 0].TrimStart().TrimEnd() == cat)
                                    {
                                        string tip2 = mat[nt2, 1];
                                        if (tip2 == "int") tipcat = "int";
                                        if (tip2 == "double") tipcat = "double";
                                    }
                                }
                            }
                            if (tipcat == "double")
                            {
                                string[] finish;
                                finish = prelucrare[i].Split('=');
                                prelucrare[i] = finish[0] + "= (float)" + finish[1].Substring(1, finish[1].Length - 1);
                            }
                        }
                    }
                    //Definirea unui intreg
                    if (prelucrare[i].TrimStart().StartsWith(IntegerKeyword) & prelucrare[i].TrimStart().StartsWith("intreg.citeste") == false)
                    {
                        string tt = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1");
                        if (prelucrare[i].TrimStart().Contains(","))
                        {
                            string[] vars = Regex.Split(tt, ",");
                            foreach (string vars1 in vars)
                            {
                                string vars2 = vars1;
                                if (vars1.Contains("["))
                                {
                                    vars2 = vars1.Split('[')[0];
                                }
                                mat[nrcurent, 0] = vars2.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "int";
                                nrcurent += 1;
                            }
                        }
                        else
                        {
                            if (tt.Contains("["))
                            {
                                tt = tt.Split('[')[0];
                            }
                            mat[nrcurent, 0] = tt;
                            mat[nrcurent, 1] = "int";
                            nrcurent += 1;
                        }
                        /*mat[nrcurent, 0] = tt;
                        mat[nrcurent, 1] = "int";
                        nrcurent += 1;*/
                        if (lang == "CPP")
                        {
                            prelucrare[i] = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "int $1");
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            prelucrare[i] = prelucrare[i] + ";";
                            x = true;
                        }
                        else
                        {
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            if (prelucrare[i].Contains("["))
                            {
                                string t = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1").TrimStart();
                                if (t.Contains(","))
                                {
                                    string[] str = t.Split(',');
                                    foreach (string s in str)
                                    {
                                        if (!s.Contains('[')) variabile += s + ", ";
                                        else
                                        {
                                            variabile += s.Split('[')[0] + " = " + Regex.Replace(s, "[^\\[-\\]]", "").Replace("][", ",") + ", ";
                                        }
                                    }
                                }
                                else
                                {
                                    variabile += t.Split('[')[0] + " = " + Regex.Replace(t, "[^\\[-\\]]", "") + ", ";
                                }
                            }
                            else variabile = variabile + Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1, ").TrimStart();
                            prelucrare[i] = "";
                            x = true;
                            coresp[i] = 0;
                            xxx = true;
                            nrvar += 1;
                        }
                    }
                    //Definirea unui nr real
                    if (prelucrare[i].TrimStart().StartsWith(DoubleKeyword) & prelucrare[i].TrimStart().StartsWith("real.citeste") == false)
                    {
                        string tt = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1");
                        if (prelucrare[i].TrimStart().Contains(","))
                        {
                            string[] vars = Regex.Split(tt, ",");
                            foreach (string vars1 in vars)
                            {
                                string vars2 = vars1;
                                if (vars1.Contains("["))
                                {
                                    vars2 = vars1.Split('[')[0];
                                }
                                mat[nrcurent, 0] = vars2.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "double";
                                nrcurent += 1;
                            }
                        }
                        else
                        {
                            if (tt.Contains("["))
                            {
                                tt = tt.Split('[')[0];
                            }
                            mat[nrcurent, 0] = tt;
                            mat[nrcurent, 1] = "double";
                            nrcurent += 1;
                        }
                        /*mat[nrcurent, 0] = tt;
                        mat[nrcurent, 1] = "double";
                        nrcurent += 1;*/
                        if (lang == "CPP")
                        {
                            prelucrare[i] = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "double $1");
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            prelucrare[i] = prelucrare[i] + ";";
                            x = true;
                        }
                        else
                        {
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            if (prelucrare[i].Contains("["))
                            {
                                string t = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1").TrimStart();
                                if (t.Contains(","))
                                {
                                    string[] str = t.Split(',');
                                    foreach (string s in str)
                                    {
                                        if (!s.Contains('[')) variabile += s + ", ";
                                        else
                                        {
                                            variabile += s.Split('[')[0] + " = " + Regex.Replace(s, "[^\\[-\\]]", "").Replace("][", ",") + ", ";
                                        }
                                    }
                                }
                                else
                                {
                                    variabile += t.Split('[')[0] + " = " + Regex.Replace(t, "[^\\[-\\]]", "") + ", ";
                                }
                            }
                            else 
                            variabile = variabile + Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1, ").TrimStart();
                            prelucrare[i] = "";
                            x = true;
                            coresp[i] = 0;
                            xxx = true;
                            nrvar += 1;
                        }
                    }
                    //Definirea unui sir de caractere
                    if (prelucrare[i].TrimStart().StartsWith(StringKeyword) & prelucrare[i].TrimStart().StartsWith("sir.citeste") == false)
                    {
                        string tt = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1");
                        if (prelucrare[i].TrimStart().Contains(","))
                        {
                            string[] vars = Regex.Split(tt, ",");
                            foreach (string vars1 in vars)
                            {
                                string vars2 = vars1;
                                if (vars1.Contains("["))
                                {
                                    vars2 = vars1.Split('[')[0];
                                }
                                mat[nrcurent, 0] = vars2.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "string";
                                nrcurent += 1;
                            }
                        }
                        else
                        {
                            if (tt.Contains("["))
                            {
                                tt = tt.Split('[')[0];
                            }
                            mat[nrcurent, 0] = tt;
                            mat[nrcurent, 1] = "string";
                            nrcurent += 1;
                        }
                        /*mat[nrcurent, 0] = tt;
                        mat[nrcurent, 1] = "string";
                        nrcurent += 1;*/
                        if (lang == "CPP")
                        {
                            prelucrare[i] = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "string $1");
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            prelucrare[i] = prelucrare[i] + ";";
                            x = true;
                        }
                        else
                        {
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            if (prelucrare[i].Contains("["))
                            {
                                string t = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1").TrimStart();
                                if (t.Contains(","))
                                {
                                    string[] str = t.Split(',');
                                    foreach (string s in str)
                                    {
                                        if (!s.Contains('[')) variabile += s + ", ";
                                        else
                                        {
                                            variabile += s.Split('[')[0] + " = " + Regex.Replace(s, "[^\\[-\\]]", "").Replace("][", ",") + ", ";
                                        }
                                    }
                                }
                                else
                                {
                                    variabile += t.Split('[')[0] + " = " + Regex.Replace(t, "[^\\[-\\]]", "") + ", ";
                                }
                            }
                            else 
                            variabile = variabile + Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1, ").TrimStart();
                            prelucrare[i] = "";
                            x = true;
                            coresp[i] = 0;
                            xxx = true;
                            nrvar += 1;
                        }
                    }
                    //Definirea unui caracter
                    if (prelucrare[i].TrimStart().StartsWith(CharKeyword) & prelucrare[i].TrimStart().StartsWith("caracter.citeste") == false)
                    {
                        string tt = Regex.Replace(prelucrare[i], CharKeyword + " (.*)", "$1");
                        mat[nrcurent, 0] = tt;
                        mat[nrcurent, 1] = "character";
                        nrcurent += 1;
                        if (lang == "CPP")
                        {
                            prelucrare[i] = Regex.Replace(prelucrare[i], CharKeyword + " (.*)", "char $1");
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            prelucrare[i] = prelucrare[i] + ";";
                            x = true;
                        }
                        else
                        {
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            variabile = variabile + Regex.Replace(prelucrare[i], CharKeyword + " (.*)", "$1, ").TrimStart();
                            prelucrare[i] = "";
                            x = true;
                            coresp[i] = 0;
                            xxx = true;
                            nrvar += 1;
                        }
                    }

                    //Verificarea impartirii
                    /*if (prelucrare[i].Contains('=') && prelucrare[i].Contains('/') && !prelucrare[i].StartsWith("//"))
                    {
                        string cat = prelucrare[i].Split('=').First().Remove(prelucrare[i].Split('=').First().Length - 1);
                        string deimaprtit = prelucrare[i].Split('=')[1].Split('/')[0].TrimStart().TrimEnd();
                        string impartitor = prelucrare[i].Split('=')[1].Split('/')[1].TrimStart().TrimEnd();
                        string tipcat = "";
                        string tipdeimpartit = "";
                        string tipimpartitor = "";
                        //Debug.WriteLine(cat);
                        //int result = mat.Count(s => s != null);
                        for (int loopint = 0; loopint < mat.GetLength(0); loopint++)
                        {
                            //Debug.WriteLine(mat[loopint, 0]);
                            if (mat[loopint, 0] == cat | mat[loopint, 0] == cat + "\r")
                            {
                                tipcat = mat[loopint, 1];
                                //Debug.WriteLine(mat[loopint, 0]);
                            }
                            if (mat[loopint, 0] == deimaprtit | mat[loopint, 0] == deimaprtit + "\r")
                            {
                                tipdeimpartit = mat[loopint, 1];
                            }
                            if (mat[loopint, 0] == impartitor | mat[loopint, 0] == impartitor + "\r")
                            {
                                tipimpartitor = mat[loopint, 1];
                            }
                        }
                        //Debug.WriteLine(tipcat);
                        if (tipcat == tipdeimpartit && tipdeimpartit == tipimpartitor)
                        {

                            if (handluit == false)
                            {
                                buttonItem3.Visible = false;
                                handluit = true;
                            }
                        }
                        else
                        {
                            //Debug.WriteLine("da");
                            if (tipcat == "double")
                            {
                                buttonItem3.Visible = true;
                                buttonItem3.Text = superTabControl1.SelectedTab.Text + ": " + PossibleError + cat + " - " + tipcat + "; " + deimaprtit + " - " + tipdeimpartit + "; " + impartitor + " - " + tipimpartitor + ".";
                                buttonItem3.Tag = (i + 1).ToString() + "." + "1" + "." + "1";
                                handluit = false;
                            }
                            else
                            {
                                if (handluit == false)
                                {
                                    buttonItem3.Visible = false;
                                    handluit = true;
                                }
                            }
                        }
                    }
                    else
                    {

                    }*/
                    //Operatia de citire de la tastatura
                    if (prelucrare[i].TrimStart().StartsWith(ReadKeyword))
                    {
                        if (lang == "CPP")
                        {
                            if (!prelucrare[i].Contains(','))
                            {
                                if (infoarena == true) prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "f>>$1;");
                                else prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "cin>>$1;");
                                x = true;
                            }
                            else
                            {
                                string initial = prelucrare[i];
                                string[] varlist;
                                if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), ",");
                                else varlist = Regex.Split(prelucrare[i], ",");
                                varlist[0] = Regex.Replace(varlist[0], ReadKeyword + " ", "");
                                if (infoarena == true) prelucrare[i] = GetLeadingWhitespace(initial) + "f";
                                else prelucrare[i] = GetLeadingWhitespace(initial) + "cin";
                                foreach (string ct in varlist)
                                {
                                    prelucrare[i] += ">>" + ct.TrimStart().TrimEnd();
                                }
                            }
                        }
                        /*string initial = prelucrare[i];
                        string ct = Regex.Replace(prelucrare[i], @"cin>>\s*(.*?)(?:\s|$)", "$1");
                        ct = ct.Remove(ct.Length - 1);
                        for (int nt = 0; nt < (mat.Length / 2); nt++)
                        {
                            //System.Diagnostics.Debug.WriteLine(mat[nt, 0]);
                            if (mat[nt, 0] == ct + "\r" | mat[nt, 0] == ct)
                            {
                                string tip = mat[nt, 1];
                                if (tip == "int")
                                {
                                    prelucrare[i] = Regex.Replace(prelucrare[i], @"cin>>\s*(.*?)(?:\s|$)", "$1=parseInt(window.prompt(\"" + InputText + "$1.\"));");
                                }
                                if (tip == "double")
                                {
                                    prelucrare[i] = Regex.Replace(prelucrare[i], @"cin>>\s*(.*?)(?:\s|$)", "$1=parseFloat(window.prompt(\"" + InputText + "$1.\"));");
                                }
                                if (tip == "string")
                                {
                                    prelucrare[i] = Regex.Replace(prelucrare[i], @"cin>>\s*(.*?)(?:\s|$)", "$1=window.prompt(\"" + InputText + "$1.\");");
                                }
                                if (tip == "char")
                                {
                                    prelucrare[i] = Regex.Replace(prelucrare[i], @"cin>>\s*(.*?)(?:\s|$)", "$1=window.prompt(\"" + InputText + "$1.\");");
                                }
                            }
                            if (prelucrare[i] == initial)
                            {

                                buttonItem3.Visible = true;
                                buttonItem3.Text = superTabControl1.SelectedTab.Text + ": " + UndeclaredVariable + ct + ".";
                                buttonItem3.Tag = (i + 1).ToString() + "." + "1" + "." + "1";
                                handluit = false;

                                //warningBox1.Text = "<b>Eroare</b>: Variabilă nedeclarată: <i>" + ct + "</i>";
                            }
                            else
                            {
                                if (handluit == false)
                                {
                                    buttonItem3.Visible = false;
                                    handluit = true;
                                }
                            }
                        }*/
                        //prelucrare[i] = initial;

                        else
                        {
                            if (!prelucrare[i].Contains(','))
                            {
                                string initial = prelucrare[i];
                                string ct = Regex.Replace(prelucrare[i].TrimStart(), ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1");
                                if (ct.Contains("[")) ct = ct.Split('[')[0];
                                for (int nt = 0; nt < (mat.Length / 2); nt++)
                                {
                                    //System.Diagnostics.Debug.WriteLine(mat[nt, 0]);
                                    if (mat[nt, 0] == ct + "\r" | mat[nt, 0] == ct)
                                    {
                                        string tip = mat[nt, 1];
                                        if (tip == "int")
                                        {
                                            prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword +  @" \s*(.*?)(?:\s|$)", "$1=parseInt(window.prompt(\"" + InputText + "$1.\"));");
                                        }
                                        if (tip == "double")
                                        {
                                            prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1=parseFloat(window.prompt(\"" + InputText + "$1.\"));");
                                        }
                                        if (tip == "string")
                                        {
                                            prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1=window.prompt(\"" + InputText + "$1.\");");
                                        }
                                        if (tip == "char")
                                        {
                                            prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + @" \s*(.*?)(?:\s|$)", "$1=window.prompt(\"" + InputText + "$1.\");");
                                        }
                                    }
                                    /*if (prelucrare[i] == initial)
                                    {
                                        buttonItem3.Visible = true;
                                        buttonItem3.Text = superTabControl1.SelectedTab.Text + ": " + UndeclaredVariable + ct + ".";
                                        buttonItem3.Tag = (i + 1).ToString() + "." + "1" + "." + "1";
                                        handluit = false;

                                        //warningBox1.Text = "<b>Eroare</b>: Variabilă nedeclarată: <i>" + ct + "</i>";
                                    }
                                    else
                                    {
                                        if (handluit == false)
                                        {
                                            buttonItem3.Visible = false;
                                            handluit = true;
                                        }
                                    }*/
                                }
                            }
                            else
                            {

                                string initial = prelucrare[i];
                                string[] varlist;
                                if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), ",");
                                else varlist = Regex.Split(prelucrare[i], ",");
                                varlist[0] = Regex.Replace(varlist[0], ReadKeyword + " ", "");
                                prelucrare[i] = GetLeadingWhitespace(initial);
                                foreach (string ct in varlist)
                                {
                                    string ct2 = ct;
                                    if (ct.Contains("[")) ct2 = ct.Split('[')[0];

                                    for (int nt = 0; nt < (mat.Length / 2); nt++)
                                    {
                                        //System.Diagnostics.Debug.WriteLine(mat[nt, 0]);
                                        if (mat[nt, 0] == ct2.TrimStart().TrimEnd() + "\r" | mat[nt, 0] == ct2.TrimStart().TrimEnd())
                                        {
                                            string tip = mat[nt, 1];
                                            if (tip == "int")
                                            {
                                                prelucrare[i] += ct + "=parseInt(window.prompt(\"" + InputText + "$1.\"));";
                                            }
                                            if (tip == "double")
                                            {
                                                prelucrare[i] += ct + "=parseFloat(window.prompt(\"" + InputText + "$1.\"));";
                                            }
                                            if (tip == "string")
                                            {
                                                prelucrare[i] += ct + "=window.prompt(\"" + InputText + "$1.\");";
                                            }
                                            if (tip == "char")
                                            {
                                                prelucrare[i] += ct + "=window.prompt(\"" + InputText + "$1.\");";
                                            }
                                        }
                                    }
                                }
                            }
                            prelucrare[i] = prelucrare[i].Replace("][", ",");
                            x = true;
                        }
                    }
                    //Operatia de scriere pe ecran
                    if (prelucrare[i].TrimStart().StartsWith(WriteKeyword))
                    {
                        if (lang == "CPP")
                        {
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            if (prelucrare[i].Contains("&")) prelucrare[i] = Regex.Replace(prelucrare[i], "\\+", "<<");
                            if (!prelucrare[i].Contains(','))
                            {
                                string ad = GetLeadingWhitespace(prelucrare[i]);
                                prelucrare[i] = ToLiteral(Regex.Replace(prelucrare[i].Trim(), WriteKeyword + @" (.*)$", "$1"), "C++");
                                if (infoarena == true) prelucrare[i] = ad + "g<<" + prelucrare[i];
                                else prelucrare[i] = ad + "cout<<" + prelucrare[i];
                                if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                                //if (prelucrare[i].TrimStart().StartsWith("cout<<\"") && !prelucrare[i].EndsWith("\"")) prelucrare[i] += "\"";
                                prelucrare[i] = prelucrare[i] + ";";
                            }
                            else
                            {
                                if (prelucrare[i].TrimStart().Replace("^" + WriteKeyword + " ", "").StartsWith("\"") == false && (prelucrare[i].TrimStart().Replace(WriteKeyword + " ", "").EndsWith("\"") == false || prelucrare[i].TrimStart().Substring(0, prelucrare[i].TrimStart().Length - 1).Replace(WriteKeyword + " ", "").EndsWith("\"") == false))
                                {
                                    string initial = prelucrare[i];
                                    string[] varlist;
                                    //Debug.WriteLine(prelucrare[i]);
                                    prelucrare[i] = Regex.Replace(prelucrare[i], ", ", ",");
                                    string virgula = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
                                    if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), virgula);
                                    else varlist = Regex.Split(prelucrare[i], virgula);
                                    varlist[0] = Regex.Replace(varlist[0].TrimStart(), "^" + WriteKeyword + " ", "");
                                    for (int k = 0; k < varlist.Length - 1; k++)
                                    {

                                        if (varlist[k].Contains("\"") && (!(varlist[k].StartsWith("\"") && varlist[k].EndsWith("\""))))
                                        {
                                        aici:
                                            try
                                            {
                                                varlist[k] += "," + varlist[k + 1];
                                                var list = new List<string>(varlist);
                                                list.RemoveAt(k + 1);
                                                varlist = list.ToArray();
                                                int count = Regex.Matches(varlist[k], "\"").Count;
                                                if (count % 2 != 0) goto aici;
                                            }
                                            catch { }
                                        }
                                    }
                                    if (infoarena == true) prelucrare[i] = GetLeadingWhitespace(initial) + "g";
                                    else prelucrare[i] = GetLeadingWhitespace(initial) + "cout";
                                    foreach (string ct in varlist)
                                    {
                                        //MessageBox.Show(ct.TrimStart().TrimEnd());
                                        prelucrare[i] += "<<" + ToLiteral(ct.TrimStart().TrimEnd(), "C++") + "";
                                    }
                                    prelucrare[i] += ";";
                                }
                                else
                                {
                                    if (infoarena == true) prelucrare[i] = GetLeadingWhitespace(prelucrare[i]) + "g<<" + ToLiteral(Regex.Replace(prelucrare[i].TrimStart(), "^" + WriteKeyword + " ", ""), "C++") + "; ";
                                    else prelucrare[i] = GetLeadingWhitespace(prelucrare[i]) + "cout<<" + ToLiteral(Regex.Replace(prelucrare[i].TrimStart(), "^" + WriteKeyword + " ", ""), "C++") + "; ";
                                }

                            }
                            x = true;
                        }
                        else
                        {
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            if (!prelucrare[i].Contains(','))
                            {
                                string ad = GetLeadingWhitespace(prelucrare[i]);
                                prelucrare[i] = ToLiteral(Regex.Replace(prelucrare[i].Trim(), WriteKeyword + @" (.*)$", "$1"), "JavaScript");
                                prelucrare[i] = prelucrare[i].Replace(" ", "&nbsp;");
                                prelucrare[i] = ad + "document.write(" + prelucrare[i];
                                if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                                //if (prelucrare[i].TrimStart().StartsWith("alert(\"") && !prelucrare[i].EndsWith("\"")) prelucrare[i] += "\"";
                                prelucrare[i] = prelucrare[i] + ");";

                            }
                            else
                            {
                                 if (prelucrare[i].TrimStart().Replace("^" + WriteKeyword + " ", "").StartsWith("\"") == false && (prelucrare[i].TrimStart().Replace(WriteKeyword + " ", "").EndsWith("\"") == false || prelucrare[i].TrimStart().Substring(0, prelucrare[i].TrimStart().Length - 1).Replace(WriteKeyword + " ", "").EndsWith("\"") == false))
                                    {
                                string initial = prelucrare[i];
                                    string[] varlist;
                                    //Debug.WriteLine(prelucrare[i]);
                                    prelucrare[i] = Regex.Replace(prelucrare[i], ", ", ",");
                                    string virgula = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
                                    if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), virgula);
                                    else varlist = Regex.Split(prelucrare[i], virgula);
                                    varlist[0] = Regex.Replace(varlist[0].TrimStart(), "^" + WriteKeyword + " ", "");
                                    for (int k = 0; k < varlist.Length - 1; k++)
                                    {

                                        if (varlist[k].Contains("\"") && (!(varlist[k].StartsWith("\"") && varlist[k].EndsWith("\""))))
                                        {
                                        aici:
                                            try
                                            {
                                                varlist[k] += "," + varlist[k + 1];
                                                var list = new List<string>(varlist);
                                                list.RemoveAt(k + 1);
                                                varlist = list.ToArray();
                                                int count = Regex.Matches(varlist[k], "\"").Count;
                                                if (count % 2 != 0) goto aici;
                                            }
                                            catch { }
                                        }
                                    }
                                    prelucrare[i] = GetLeadingWhitespace(initial);// +"alert(";
                                    foreach (string ct in varlist)
                                    {
                                        prelucrare[i] += "document.write(" + ToLiteral(ct.TrimStart().TrimEnd().Replace(" ", "&nbsp;"), "JavaScript") + "); ";
                                    }
                                    //prelucrare[i] += ";";
                                }
                                else
                                {
                                    prelucrare[i] = GetLeadingWhitespace(prelucrare[i]) + "document.write(" + ToLiteral(Regex.Replace(prelucrare[i].TrimStart(), "^" + WriteKeyword + " ", "").Replace(" ", "&nbsp;"), "JavaScript") + "); ";
                                }
                            }
                            prelucrare[i] = Regex.Replace(prelucrare[i], "\\]\\[(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", ",");

                            x = true;
                               /* if (prelucrare[i].TrimStart().Replace("^" + WriteKeyword + " ", "").StartsWith("\"") == false && prelucrare[i].TrimStart().Replace(WriteKeyword + " ", "").EndsWith("\"") == false)
                                {
                                    string initial = prelucrare[i];
                                    string[] varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), ",");
                                    varlist[0] = Regex.Replace(varlist[0], "^" + WriteKeyword + " ", "");
                                    for (int k = 0; k < varlist.Length - 1; k++)
                                    {

                                        if (varlist[k].Contains("\"") && (!(varlist[k].StartsWith("\"") && varlist[k].EndsWith("\""))))
                                        {
                                        aici:
                                            try
                                            {
                                                varlist[k] += "," + varlist[k + 1];
                                                var list = new List<string>(varlist);
                                                list.RemoveAt(k + 1);
                                                varlist = list.ToArray();
                                                int count = Regex.Matches(varlist[k], "\"").Count;
                                                if (count % 2 != 0) goto aici;
                                            }
                                            catch { }
                                        }
                                    }
                                    prelucrare[i] = GetLeadingWhitespace(initial);
                                    foreach (string ct in varlist)
                                    {
                                        prelucrare[i] += "alert(" + ct.TrimStart().TrimEnd() + ");";
                                    }
                                }
                                else
                                {
                                    prelucrare[i] = "alert(" + Regex.Replace(prelucrare[i].TrimStart(), "^" + WriteKeyword + " ", "") + ");";
                                }*/
                         //   }
                         //   x = true;
                        }
                    }
                    //Ridicare la putere
                    prelucrare[i] = prelucrare[i].Replace("@_@@", ",");
                  /*  if (prelucrare[i].TrimStart().Contains("^") && !prelucrare[i].StartsWith("//"))
                    {
                        if (lang == "JAVASCRIPT") prelucrare[i] = prelucrare[i].Replace(",", "$_$$$$");
                        if (lang == "JAVASCRIPT") prelucrare[i] = prelucrare[i].Replace("alert(", "@_@@@@");
                        if (lang == "JAVASCRIPT") prelucrare[i] = prelucrare[i].Replace(");", "!_!!!!");
                        if (prelucrare[i].EndsWith("\r"))
                        {
                            prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        }
                        //string[] text = prelucrare[i].Split('^');
                        var arr = Regex.Matches(prelucrare[i], @"(|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z\(\(|\d|\^|\(|\)| |""|\.)(a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|\d|\^|\(|\)| |""|\.)+").Cast<Match>().Select(m => m.Value).ToArray();
                        var arr2 = Regex.Matches(prelucrare[i], @"(|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z\(\(|\d|\^|\(|\)| |""|\.)(a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|\d|\^|\(|\)| |""|\.)+").Cast<Match>().Select(m => m.Value).ToArray();
                        string ceramane = Regex.Replace(prelucrare[i], @"(|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z\(\(|\d|\^|\(|\)| |""|\.)(a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|\d|\^|\(|\)| |""|\.)+", "#_####");

                        
                        for (int il = 0; il < arr.Length; il++)
                        {
                            if (!arr[il].TrimStart().StartsWith("\""))
                            {

                                if (arr[il].Contains('^'))
                                {
                                    bool count = false;
                                    string strInceput = "";
                                    string strText = "";
                                    string strFinal = "";
                                    int a = 0;
                                    string[] text = arr[il].Split('^');
                                    for (int ix = 0; ix < text.Length; ix++)
                                    {
                                        if (ix == text.Length - 1)
                                        {
                                            //   strText += text[ix].Trim();
                                            //  count = false;
                                            text[ix] = text[ix].Trim();
                                            //   if (text[ix].Contains("("))
                                            //   {
                                            //  text[ix] = text[ix].Replace("(", "pow(");
                                            //  text[ix] = text[ix].Trim() + ", ";
                                            //  a++;
                                            //    }
                                            //   else
                                            //  {
                                            //  if (!text[ix].Contains(")"))
                                            //   {
                                            //       text[ix] = "pow(" + text[ix].Trim() + ", ";
                                            //       a++;
                                            //    }
                                            //    else
                                            //    {
                                            //       int b = text[ix].Split(')').Length - 1;
                                            //if (text[ix - b].Contains("pow"))
                                            //  {
                                            //          text[ix - b] = "pow(" + text[ix - b];
                                            //     if (!text[ix].Contains(")"))
                                            //     {
                                            //       text[ix] = text[ix].Trim() + "), ";
                                            //     }
                                            //    else
                                            //    {
                                            //        text[ix] = text[ix].Trim() + ", ";
                                            //        a = a - b;
                                            //    }
                                            // }
                                            //   else text[ix] = text[ix].Trim() + ", ";
                                            //  }
                                            //  }
                                        }
                                        else
                                        {
                                            if (text[ix].Contains("("))
                                            {
                                                if (lang == "JAVASCRIPT") text[ix] = text[ix].Replace("(", "Math.pow(");
                                                else text[ix] = text[ix].Replace("(", "pow(");
                                                text[ix] = text[ix].Trim() + ", ";
                                                a++;
                                            }
                                            else
                                            {
                                                if (!text[ix].Contains(")"))
                                                {
                                                    if (lang == "JAVASCRIPT") text[ix] = "Math.pow(" + text[ix].Trim() + ", ";
                                                    else text[ix] = "pow(" + text[ix].Trim() + ", ";
                                                    a++;
                                                }
                                                else
                                                {
                                                    int b = text[ix].Split(')').Length - 1;
                                                    if (text[ix - b].Contains("pow"))
                                                    {
                                                        if (lang == "JAVASCRIPT") text[ix - b] = "Math.pow(" + text[ix - b];
                                                        else text[ix - b] = "pow(" + text[ix - b];
                                                        if (!text[ix].Contains(")"))
                                                        {
                                                            text[ix] = text[ix].Trim() + "), ";
                                                        }
                                                        else
                                                        {
                                                            text[ix] = text[ix].Trim() + ", ";
                                                            //  a = a - b;
                                                        }
                                                    }
                                                    else text[ix] = text[ix].Trim() + ", ";
                                                }
                                            }
                                        }

                                    }
                                    a = 0;
                                    for (int ix = 0; ix < text.Length; ix++)
                                    {
                                        a += text[ix].Split('(').Length - 1;
                                        a -= text[ix].Split(')').Length - 1;
                                    }
                                    for (int ix = 1; ix <= a; ix++)
                                        text[text.Length - 1] += ")";
                                    strText = "";
                                    for (int ix = 0; ix < text.Length; ix++)
                                    {
                                        strText += text[ix];
                                    }
                                    arr[il] = strText;
                                }
                            }
                        }
                        prelucrare[i] = "";
                        string[] finalizare = Regex.Split(ceramane, "#_####");
                        for (int il = 0; il < finalizare.Length; il++)
                        {
                           // if (!arr[il].TrimStart().StartsWith("\""))
                           // {
                            if (!(il == finalizare.Length - 1))
                            {
                                if (arr2[il].StartsWith("(") && arr2[il].EndsWith(")"))
                                {
                                    prelucrare[i] += finalizare[il] + "(" + arr[il] + ")";
                                }
                                else
                                {
                                    prelucrare[i] += finalizare[il] + arr[il];
                                }
                            }
                            else prelucrare[i] += finalizare[il];
                               // if (arr2[il].StartsWith("(") && arr2[il].EndsWith(")"))
                               // {
                              //      prelucrare[i] = Regex.Replace(prelucrare[i], arr2[il], "(" + arr[il] + ")", RegexOptions.;
                               // }
                               // else prelucrare[i] = prelucrare[i].Replace(arr2[il], arr[il]);
                           // }
                        }
                       /* if (prelucrare[i].StartsWith("alertpow"))
                        {
                            prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1).Replace("alert", "alert(") + ");";
                        }*/
                        //if (!prelucrare[i].EndsWith(";")) prelucrare[i] += ";";
                     /*   if (lang == "JAVASCRIPT") prelucrare[i] = prelucrare[i].Replace("$_$$$$", ",");
                        if (lang == "JAVASCRIPT") prelucrare[i] = prelucrare[i].Replace("@_@@@@", "alert(");
                        if (lang == "JAVASCRIPT") prelucrare[i] = prelucrare[i].Replace("!_!!!!", ");");
                        /*char[] myString = prelucrare[i].ToString().ToCharArray();
                        int loc = 0;
                        for (int cnt = 0; cnt < myString.Length; cnt++)
                        {
                            if (myString[cnt] == '^')
                            {
                                loc = cnt;
                                break;
                            }
                        }
                        int inceput = 0;
                        int fin = 0;
                        int factor = 1;
                        string nr = "";
                        string exponent = "";
                        int oktoexit = 0;
                        for (int cnt = loc - 1; cnt > 0; cnt--)
                        {
                            if (myString[cnt].ToString() != " " && myString[cnt].ToString() != "(")
                            {
                                oktoexit = 1;
                                nr += myString[cnt].ToString();
                                inceput = cnt;
                            }
                            else if (oktoexit == 1) break;
                        }
                        factor = 1;
                        oktoexit = 0;
                        for (int cnt = loc + 1; cnt < myString.Length; cnt++)
                        {
                            if (myString[cnt].ToString() != " " && myString[cnt].ToString() != ")")
                            {
                                oktoexit = 1;
                                exponent += myString[cnt].ToString();
                                fin = cnt;
                            }
                            else if (oktoexit == 1) break;
                        }
                        prelucrare[i] = "";
                        for (int cnt = 0; cnt < inceput; cnt++)
                        {
                            prelucrare[i] += myString[cnt];
                        }
                        if (nr.EndsWith("\r")) nr = nr.Substring(0, nr.Length - 1);
                        nr = Reverse(nr);
                        if (nr.EndsWith("\r")) nr = nr.Substring(0, nr.Length - 1);
                        if (exponent.EndsWith("\r")) exponent = exponent.Substring(0, exponent.Length - 1);
                        prelucrare[i] = prelucrare[i] + "pow(" + nr + ", " + exponent + ")";// +prelucrare[i].Substring(fin, prelucrare[i].Length - 1);
                        for (int cnt = fin + 1; cnt < myString.Length; cnt++)
                        {
                            prelucrare[i] += myString[cnt];
                        }*/
                   // }

                    //Instructiunea daca
                    if (prelucrare[i].TrimStart().StartsWith(IfKeyword))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], IfKeyword + " (.*)", "if ($1");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ")\n" + spaces + "    {";
                        if (prelucrare[i].TrimStart().Contains(ThenKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + ThenKeyword, "");
                        x = true;
                    }
                    //Instructiunea altfel
                    if (prelucrare[i].TrimStart().StartsWith(ElseKeyword))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], ElseKeyword, "}\n" + spaces + "    else\n" + spaces + "    {");
                        x = true;
                    }
                    //Instructiunea sfarsit_daca
                    if (prelucrare[i].TrimStart().StartsWith(EndIfKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], EndIfKeyword, "}");
                        x = true;
                    }
                    //Instructiunea cat timp
                    if (prelucrare[i].TrimStart().StartsWith(WhileKeyword))
                    {
                        if (prelucrare[i].TrimStart().Contains(DoKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + DoKeyword, "");
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], WhileKeyword + " (.*)", "while ($1");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ")\n" + spaces + "    {";
                        x = true;
                    }
                    //Instructiunea sfarsit cat timp
                    if (prelucrare[i].TrimStart().StartsWith(EndWhileKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], EndWhileKeyword, "}");
                        x = true;
                    }
                    //Operatia de citire de la tastatura a unui intreg
                    if (prelucrare[i].TrimStart().StartsWith("intreg.citeste"))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], @"intreg.citeste \s*(.*?)(?:\s|$)", "cin>>$1;");
                        x = true;
                    }
                    //Operatia de citire de la tastatura a unui numar real
                    if (prelucrare[i].TrimStart().StartsWith("real.citeste"))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], @"real.citeste \s*(.*?)(?:\s|$)", "cin>>$1;");
                        x = true;
                    }
                    //Instructiunea pentru
                    if (prelucrare[i].TrimStart().StartsWith(ForKeyword))
                    {
                        if (prelucrare[i].TrimStart().Contains(DoKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + DoKeyword, "");
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        string a1 = Regex.Replace(prelucrare[i], ForKeyword + " (.*)", "$1");
                        // Debug.WriteLine(a1);
                        //prelucrare[i] = Regex.Replace(prelucrare[i], "pentru (.*)", "for ($1");
                        string[] a2 = Regex.Split(a1.TrimStart(), ",");
                        try
                        {
                            string fin = "";
                            for (int sks = 1; sks < a2.Length; sks++)
                            {
                                if (sks == a2.Length - 1) fin += a2[sks];
                                else fin += a2[sks] + ",";
                            }
                            a2[1] = fin;
                        }
                        catch { }
                        string nume_var = sep(a2[0], "=");
                        bool edec = false;
                        for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++) if (mat[nt2, 0] != null) if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == nume_var | mat[nt2, 0].TrimStart().TrimEnd() == nume_var) edec = true;
                        try
                        {
                            if (edec == true)
                            {
                                prelucrare[i] = spaces + "for (" + a2[0] + "; " + nume_var + " <= " + a2[1]; // +"; " + nume_var + "++)";
                            }
                            else
                            {
                                if (lang == "JAVASCRIPT") prelucrare[i] = spaces + "for (var " + a2[0] + "; " + nume_var + " <= " + a2[1]; // +"; " + nume_var + "++)";
                                else prelucrare[i] = spaces + "for (int " + a2[0] + "; " + nume_var + " <= " + a2[1];
                            }
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            prelucrare[i] = prelucrare[i] + "; " + nume_var + "++)\n" + spaces + "    { ";
                            x = true;
                        }
                        catch
                        {
                            prelucrare[i] = "";
                        }
                        x = true;
                    }
                    //Instructiunea sfarsit_pentru
                    if (prelucrare[i].TrimStart().StartsWith(EndForKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], EndForKeyword, "}");
                        x = true;
                    }
                    //Instructiunea executa
                    if (prelucrare[i].TrimStart().StartsWith(DoKeyword) && !prelucrare[i].TrimStart().StartsWith("double") && !prelucrare[i].TrimStart().StartsWith("document.write"))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], DoKeyword, "do\n" + spaces + "    {");
                        x = true;
                    }
                    //Instructiunea executa pana cand (loop until)
                    if (prelucrare[i].TrimStart().StartsWith(LoopUntilKeyword))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], LoopUntilKeyword + " (.*)", "}\n" + spaces + "    while ($1");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ");";
                        if (!prelucrare[i].Contains("="))
                        {
                            if (prelucrare[i].Contains("<"))
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], "<", ">=");
                            }
                            else
                            {
                                if (prelucrare[i].Contains(">")) prelucrare[i] = Regex.Replace(prelucrare[i], ">", "<=");
                            }
                        }
                        else
                        {
                            prelucrare[i] = Regex.Replace(prelucrare[i], ">=", "<");
                            prelucrare[i] = Regex.Replace(prelucrare[i], "<=", ">");
                        }
                        x = true;
                        //prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                    }
                    //Prelucrari de final pentru cod C++
                    if (lang == "JAVASCRIPT") returnare = Regex.Replace(returnare, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                    if (x == true)
                    {
                        if (i == 0)
                            returnare = "    " + prelucrare[i] + "\n";
                        else
                            returnare = returnare + "" + "    " + prelucrare[i] + "\n";
                    }
                    else
                    {
                        if (prelucrare[i] != Environment.NewLine & prelucrare[i] != "")
                        {
                            if (i == 0)
                            {
                                returnare = "    " + prelucrare[i];
                                if (returnare.EndsWith("\r")) returnare = returnare.Substring(0, returnare.Length - 1);
                                returnare += ";\n";
                            }
                            else
                            {
                                returnare = returnare + "" + "    " + prelucrare[i];
                                if (returnare.EndsWith("\r")) returnare = returnare.Substring(0, returnare.Length - 1);
                                returnare = returnare + ";\n";
                            }
                        }
                    }
                }
                #endregion
                #region Pascal
                if (lang == "PAS")
                {
                    numarvariabilepas2 = 0;
                    prelucrare[i] = Regex.Replace(prelucrare[i], "'", "''");
                    prelucrare[i] = Regex.Replace(prelucrare[i], "\"", "'");
                    //Partea intreaga
                    if (prelucrare[i].Contains("{") && (prelucrare[i].Contains("}"))) prelucrare[i] = Regex.Replace(Regex.Replace(prelucrare[i], "\\{", "trunc("), "\\}", ")");

                    if (prelucrare[i].Contains(OrKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + OrKeyword + " ", " OR ");
                    if (prelucrare[i].Contains(AndKeyword)) prelucrare[i] = 
                        Regex.Replace(prelucrare[i], " " + AndKeyword + " ", " AND ");

                    //Sintaxa Pascal
                    //Verifica daca s-a gasit vreo instructiune
                    bool x = false;
                    //Opersatorul !=
                    if (prelucrare[i].Contains("^") & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], "\\^", "**");
                    //Opersatorul !=
                    if (prelucrare[i].Contains("!=") & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], "!=", "<>");
                    //Opersatorul mod
                    if (prelucrare[i].Contains("%") & !prelucrare[i].Contains("\"")) prelucrare[i] = Regex.Replace(prelucrare[i], "%", "mod");
                    //Opersatorul div
                    if (prelucrare[i].Contains("/") & !prelucrare[i].Contains("\'"))
                    {
                        string initialul = prelucrare[i].Replace(" ", String.Empty);
                        string cat = initialul.Split('=')[0];
                        string tipcat = "";
                        for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++)
                        {
                            if (mat[nt2, 0] != null)
                            {
                                if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == cat | mat[nt2, 0].TrimStart().TrimEnd() == cat)
                                {
                                    string tip2 = mat[nt2, 1];
                                    if (tip2 == "int") tipcat = "int";
                                    if (tip2 == "double") tipcat = "double";
                                }
                            }
                        }
                        if (tipcat == "int")
                        {
                            prelucrare[i] = Regex.Replace(prelucrare[i], "/", "div");
                        }
                    }
                    //Linie noua
                    if (prelucrare[i].Contains("\\n"))
                    {
                        if (prelucrare[i].Contains("\\n\"")) prelucrare[i] = prelucrare[i].Replace("\\n\"", "' + #13#10");
                        else if (prelucrare[i].Contains("\"\\n")) prelucrare[i] = prelucrare[i].Replace("\"\\n", "#13#10 + '");
                        else if (prelucrare[i].Contains("\\n")) prelucrare[i] = prelucrare[i].Replace("\\n", "' + #13#10 + '");
                    }
                    //Comentariu
                    if (prelucrare[i].TrimStart().StartsWith("//"))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], "//", "");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = "{ " + prelucrare[i] + " }";
                        x = true;
                    }
                    //Definirea unui intreg
                    if (prelucrare[i].TrimStart().StartsWith(IntegerKeyword) & prelucrare[i].TrimStart().StartsWith("intreg.citeste") == false)
                    {
                        if (i < pseudocodeEd.Lines.Current.Number) numarvariabilepas2++;
                        string denumire;
                        if (!prelucrare[i].Contains(','))
                        {
                            denumire = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1").TrimStart();
                            mat[nrcurent, 0] = denumire.TrimStart().TrimEnd();
                            mat[nrcurent, 1] = "int";
                            nrcurent += 1;
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            if (prelucrare[i].Contains("["))
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1").TrimStart();
                                if (prelucrare[i].Contains("]["))
                                    variabile += Regex.Replace(prelucrare[i], "\\[.*", "") + " : array[1.." + Regex.Replace(prelucrare[i].Replace("][", ","), "[^0-9,]", "").Replace(",", ", 1..") + "]:integer;\n";
                                else
                                    variabile += Regex.Replace(prelucrare[i], "\\[.*", "") + " : array[1.." + Regex.Replace(prelucrare[i], "[^0-9]", "") + "]:integer;\n";
                            }
                            else
                            {
                                variabile = variabile + Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1:integer;\n").TrimStart();
                            }
                        }
                        else
                        {
                            denumire = Regex.Replace(prelucrare[i], IntegerKeyword + " (.*)", "$1").TrimStart();
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            string[] rupt = Regex.Split(denumire, ",");
                            foreach (string st in rupt)
                            {
                                if (st.Contains("["))
                                {
                                    if (st.Contains("]["))
                                        variabile += Regex.Replace(st, "\\[.*", "") + " : array[1.." + Regex.Replace(st.Replace("][", ","), "[^0-9,]", "").Replace(",", ", 1..") + "]:integer;\n";
                                    else
                                        variabile += Regex.Replace(st, "\\[.*", "") + " : array[1.." + Regex.Replace(st, "[^0-9]", "") + "]:integer;\n";
                                }
                                else
                                {
                                    variabile = variabile + st + ":integer;\n";
                                }
                                mat[nrcurent, 0] = st.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "int";
                                nrcurent += 1;
                            }
                        }


                        prelucrare[i] = "";
                        //prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //prelucrare[i] = prelucrare[i] + ";";
                        x = true;
                        //coresp[i] = 1;// variabile.Split('\n').Length;
                        estevar[i] = true;
                        xxx = true;
                        nrvar += 1;

                    }
                    //Definirea unui nr real
                    if (prelucrare[i].TrimStart().StartsWith(DoubleKeyword) & prelucrare[i].TrimStart().StartsWith("real.citeste") == false)
                    {
                        if (i < pseudocodeEd.Lines.Current.Number) numarvariabilepas2++;

                        string denumire;
                        if (!prelucrare[i].Contains(','))
                        {
                            denumire = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1").TrimStart();
                            mat[nrcurent, 0] = denumire.TrimStart().TrimEnd();
                            mat[nrcurent, 1] = "double";
                            nrcurent += 1;
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            if (prelucrare[i].Contains("["))
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1").TrimStart();
                                if (prelucrare[i].Contains("]["))
                                    variabile += Regex.Replace(prelucrare[i], "\\[.*", "") + " : array[1.." + Regex.Replace(prelucrare[i].Replace("][", ","), "[^0-9,]", "").Replace(",", ", 1..") + "]:real;\n";
                                else
                                    variabile += Regex.Replace(prelucrare[i], "\\[.*", "") + " : array[1.." + Regex.Replace(prelucrare[i], "[^0-9]", "") + "]:real;\n";
                            }
                            else
                            {
                                variabile = variabile + Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1:real;\n").TrimStart();
                            }

                        }
                        else
                        {
                            denumire = Regex.Replace(prelucrare[i], DoubleKeyword + " (.*)", "$1").TrimStart();
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            string[] rupt = Regex.Split(denumire, ",");
                            foreach (string st in rupt)
                            {
                                if (st.Contains("["))
                                {
                                    if (st.Contains("]["))
                                        variabile += Regex.Replace(st, "\\[.*", "") + " : array[1.." + Regex.Replace(st.Replace("][", ","), "[^0-9,]", "").Replace(",", ", 1..") + "]:real;\n";
                                    else
                                        variabile += Regex.Replace(st, "\\[.*", "") + " : array[1.." + Regex.Replace(st, "[^0-9]", "") + "]:real;\n";
                                }
                                else
                                {
                                    variabile = variabile + st + ":real;\n";
                                }
                                mat[nrcurent, 0] = st.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "double";
                                nrcurent += 1;
                            }
                        }
                        prelucrare[i] = "";
                        //prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //prelucrare[i] = prelucrare[i] + ";";
                        x = true;
                        //coresp[i] = 2;// variabile.Split('\n').Length;
                        estevar[i] = true;
                        xxx = true;
                        nrvar += 1;
                    }
                    //Definirea unui sir de caractere
                    if (prelucrare[i].TrimStart().StartsWith(StringKeyword) & prelucrare[i].TrimStart().StartsWith("sir.citeste") == false)
                    {
                        if (i < pseudocodeEd.Lines.Current.Number) numarvariabilepas2++;

                        string denumire;
                        if (!prelucrare[i].Contains(','))
                        {
                            denumire = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1").TrimStart();
                            mat[nrcurent, 0] = denumire.TrimStart().TrimEnd();
                            mat[nrcurent, 1] = "string";
                            nrcurent += 1;
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            if (prelucrare[i].Contains("["))
                            {
                                prelucrare[i] = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1").TrimStart();
                                if (prelucrare[i].Contains("]["))
                                    variabile += Regex.Replace(prelucrare[i], "\\[.*", "") + " : array[1.." + Regex.Replace(prelucrare[i].Replace("][", ","), "[^0-9,]", "").Replace(",", ", 1..") + "]:string;\n";
                                else
                                    variabile += Regex.Replace(prelucrare[i], "\\[.*", "") + " : array[1.." + Regex.Replace(prelucrare[i], "[^0-9]", "") + "]:string;\n";
                            }
                            else
                            {
                                variabile = variabile + Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1:string;\n").TrimStart();
                            }

                        }
                        else
                        {
                            denumire = Regex.Replace(prelucrare[i], StringKeyword + " (.*)", "$1").TrimStart();
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            string[] rupt = Regex.Split(denumire, ",");
                            foreach (string st in rupt)
                            {
                                if (st.Contains("["))
                                {
                                    if (st.Contains("]["))
                                        variabile += Regex.Replace(st, "\\[.*", "") + " : array[1.." + Regex.Replace(st.Replace("][", ","), "[^0-9,]", "").Replace(",", ", 1..") + "]:string;\n";
                                    else
                                        variabile += Regex.Replace(st, "\\[.*", "") + " : array[1.." + Regex.Replace(st, "[^0-9]", "") + "]:string;\n";
                                }
                                else
                                {
                                    variabile = variabile + st + ":string;\n";
                                }
                                mat[nrcurent, 0] = st.TrimStart().TrimEnd();
                                mat[nrcurent, 1] = "string";
                                nrcurent += 1;
                            }
                        }
                        prelucrare[i] = "";
                        //prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //prelucrare[i] = prelucrare[i] + ";";
                        x = true;
                        //coresp[i] = 3;// variabile.Split('\n').Length;
                        estevar[i] = true;
                        xxx = true;
                        nrvar += 1;
                    }
                    /*//Definirea unui caracter
                    if (prelucrare[i].TrimStart().StartsWith("caracter") & prelucrare[i].TrimStart().StartsWith("caracter.citeste") == false)
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], "caracter (.*)", "char $1");
                        prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ";";
                        x = true;
                    }*/
                    /*Operatia de citire de la tastatura
                    if (prelucrare[i].TrimStart().StartsWith(ReadKeyword))
                    {
                        string initial = Regex.Replace(prelucrare[i], ReadKeyword, "$1");
                        //initial = initial.Remove(initial.Length - 2);
                        //button2.Text = initial;
                        /*if (!variabile.Contains(initial + ":"))
                        {
                            //MessageBox.Show(initial);
                            buttonItem3.Visible = true;
                            buttonItem3.Text = superTabControl1.SelectedTab.Text + ": " + UndeclaredVariable + initial + ".";
                            buttonItem3.Tag = (i + 1).ToString() + "." + "1" + "." + "1";
                            handluit = false;

                            //warningBox1.Text = "<b>Eroare</b>: Variabilă nedeclarată: <i>" + ct + "</i>";
                        }
                        else
                        {
                            if (handluit == false)
                            {
                                buttonItem3.Visible = false;
                                handluit = true;
                            }
                        }*/

                   /**     prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + " ", "Readln(");

                         if (i == prelucrare.Length - 2 || prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] += ");";
                        x = true;*/

                  //  }
                    //Operatia de citire de la tastatura
                    if (prelucrare[i].TrimStart().StartsWith(ReadKeyword))
                    {
                        if (!prelucrare[i].Contains(','))
                        {
                            string initial = Regex.Replace(prelucrare[i], ReadKeyword, "$1");
                            //initial = initial.Remove(initial.Length - 2);
                            //button2.Text = initial;
                            /*if (!variabile.Contains(initial + ":"))
                            {
                                //MessageBox.Show(initial);
                                buttonItem3.Visible = true;
                                buttonItem3.Text = superTabControl1.SelectedTab.Text + ": " + UndeclaredVariable + initial + ".";
                                buttonItem3.Tag = (i + 1).ToString() + "." + "1" + "." + "1";
                                handluit = false;

                                //warningBox1.Text = "<b>Eroare</b>: Variabilă nedeclarată: <i>" + ct + "</i>";
                            }
                            else
                            {
                                if (handluit == false)
                                {
                                    buttonItem3.Visible = false;
                                    handluit = true;
                                }
                            }*/

                            prelucrare[i] = Regex.Replace(prelucrare[i], ReadKeyword + " ", "Readln(");

                            if (i == prelucrare.Length - 2 || prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            prelucrare[i] += ");";
                            x = true;
                        }
                        else
                        {

                            string initial = prelucrare[i];
                            string[] varlist;
                            if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), ",");
                            else varlist = Regex.Split(prelucrare[i], ",");
                            varlist[0] = Regex.Replace(varlist[0], ReadKeyword + " ", "");
                            prelucrare[i] = GetLeadingWhitespace(initial);
                            foreach (string ct in varlist)
                            {

                                prelucrare[i] += "Readln(" + ct.TrimStart() + "); ";
                            }
                        }
                        x = true;
                    }
                    //Operatia de scriere pe ecran
                    if (prelucrare[i].TrimStart().StartsWith(WriteKeyword))
                    {
                      //  if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //if (prelucrare[i].TrimStart().StartsWith("scrie \"") && !prelucrare[i].EndsWith("\"")) prelucrare[i] += "\"";
                      //  prelucrare[i] = Regex.Replace(prelucrare[i], WriteKeyword + @" (.*)$", "Writeln($1);");
                      //  prelucrare[i] = Regex.Replace(prelucrare[i], "\"", "'");
                        //prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //prelucrare[i] = prelucrare[i] + ";";
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //if (prelucrare[i].Contains("&")) prelucrare[i] = Regex.Replace(prelucrare[i], "\\+", "<<");
                        if (!prelucrare[i].Contains(','))
                        {
                            string ad = GetLeadingWhitespace(prelucrare[i]);
                            prelucrare[i] = Regex.Replace(prelucrare[i].Trim(), WriteKeyword + @" (.*)$", "$1");
                            prelucrare[i] = ad + "Write(" + prelucrare[i];
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            //if (prelucrare[i].TrimStart().StartsWith("cout<<\"") && !prelucrare[i].EndsWith("\"")) prelucrare[i] += "\"";
                            prelucrare[i] = prelucrare[i] + ");";
                        }
                        else
                        {
                            if (prelucrare[i].TrimStart().Replace("^" + WriteKeyword + " ", "").StartsWith("\"") == false && (prelucrare[i].TrimStart().Replace(WriteKeyword + " ", "").EndsWith("\"") == false || prelucrare[i].TrimStart().Substring(0, prelucrare[i].TrimStart().Length - 1).Replace(WriteKeyword + " ", "").EndsWith("\"") == false))
                            {
                                string initial = prelucrare[i];
                                string[] varlist;
                                //Debug.WriteLine(prelucrare[i]);
                                prelucrare[i] = Regex.Replace(prelucrare[i], ", ", ",");
                                string virgula = ",(?=(?:[^\']*\'[^\']*\')*[^\']*$)";
                                if (prelucrare[i].EndsWith("\r")) varlist = Regex.Split(prelucrare[i].Substring(0, prelucrare[i].Length - 1), virgula);
                                else varlist = Regex.Split(prelucrare[i], virgula);
                                varlist[0] = Regex.Replace(varlist[0].TrimStart(), "^" + WriteKeyword + " ", "");
                                for (int k = 0; k < varlist.Length - 1; k++)
                                {

                                    if (varlist[k].Contains("\"") && (!(varlist[k].StartsWith("\"") && varlist[k].EndsWith("\""))))
                                    {
                                    aici:
                                        try
                                        {
                                            varlist[k] += "," + varlist[k + 1];
                                            var list = new List<string>(varlist);
                                            list.RemoveAt(k + 1);
                                            varlist = list.ToArray();
                                            int count = Regex.Matches(varlist[k], "\"").Count;
                                            if (count % 2 != 0) goto aici;
                                        }
                                        catch { }
                                    }
                                }
                                prelucrare[i] = GetLeadingWhitespace(initial);
                                foreach (string ct in varlist)
                                {
                                    //MessageBox.Show(ct.TrimStart().TrimEnd());
                                    prelucrare[i] += "Write(" + ct.TrimStart().TrimEnd() + "); ";
                                }
                                prelucrare[i] += "";
                            }
                            else
                            {
                                prelucrare[i] = GetLeadingWhitespace(prelucrare[i]) + "Write(" + Regex.Replace(prelucrare[i].TrimStart(), "^" + WriteKeyword + " ", "") + "); ";
                            }
                        }
                        x = true;
                        x = true;
                    }
                    //Operatorul %
                    if (prelucrare[i].Contains("%"))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], @"%", "mod");
                        //prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //prelucrare[i] = prelucrare[i] + ";";
                        x = true;
                    }
                    //Operatia de atribuire
                    if (prelucrare[i].Contains("="))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], @"=", ":=");
                        //prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        //prelucrare[i] = prelucrare[i] + ";";
                    }
                    //Instructiunea daca
                    if (prelucrare[i].TrimStart().StartsWith(IfKeyword))
                    {
                        if (prelucrare[i].Contains("OR") || prelucrare[i].Contains("AND"))
                        {
                            string spaces = GetLeadingWhitespace(prelucrare[i]);
                            if (prelucrare[i].TrimStart().Contains(ThenKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + ThenKeyword, "");
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            prelucrare[i] = Regex.Replace(prelucrare[i], IfKeyword + " (.*)", "if ($1) then");
                            prelucrare[i] = Regex.Replace(prelucrare[i], " OR ", ") OR (");
                            prelucrare[i] = Regex.Replace(prelucrare[i], " AND ", ") AND (");
                            prelucrare[i] = prelucrare[i] + "\n" + spaces + "    begin";
                            if (prelucrare[i].Contains(":=")) prelucrare[i] = Regex.Replace(prelucrare[i], ":=", "=");
                            x = true;
                        }
                        else
                        {
                            string spaces = GetLeadingWhitespace(prelucrare[i]);
                            if (prelucrare[i].TrimStart().Contains(ThenKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + ThenKeyword, "");
                            if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            prelucrare[i] = Regex.Replace(prelucrare[i], IfKeyword + " (.*)", "if $1 then");

                            prelucrare[i] = prelucrare[i] + "\n" + spaces + "    begin";
                            if (prelucrare[i].Contains(":=")) prelucrare[i] = Regex.Replace(prelucrare[i], ":=", "=");
                            x = true;
                        }
                    }
                    //Instructiunea altfel
                    if (prelucrare[i].TrimStart().StartsWith(ElseKeyword))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], ElseKeyword, "end\n" + spaces + "    else\n" + spaces + "    begin");
                        x = true;
                    }
                    //Instructiunea sfarsit_daca
                    if (prelucrare[i].TrimStart().StartsWith(EndIfKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], EndIfKeyword, "end;");
                        x = true;
                    }
                    //Instructiunea cat timp
                    if (prelucrare[i].TrimStart().StartsWith(WhileKeyword))
                    {
                        if (prelucrare[i].TrimStart().Contains(DoKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + DoKeyword, "");

                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = Regex.Replace(prelucrare[i], WhileKeyword + " (.*)", "while ($1) do\n");

                        prelucrare[i] = prelucrare[i] + spaces + "    begin";
                        if (prelucrare[i].Contains(":=")) prelucrare[i] = Regex.Replace(prelucrare[i], ":=", "=");
                        x = true;
                    }
                    //Instructiunea sfarsit cat timp
                    if (prelucrare[i].TrimStart().StartsWith(EndWhileKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], EndWhileKeyword, "end;");
                        x = true;
                    }
                    //Operatia de citire de la tastatura a unui intreg
                    if (prelucrare[i].TrimStart().StartsWith("intreg.citeste"))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], @"intreg.citeste \s*(.*?)(?:\s|$)", "cin>>$1;");
                        x = true;
                    }
                    //Operatia de citire de la tastatura a unui numar real
                    if (prelucrare[i].TrimStart().StartsWith("real.citeste"))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], @"real.citeste \s*(.*?)(?:\s|$)", "cin>>$1;");
                        x = true;
                    }
                    //Instructiunea pentru
                    if (prelucrare[i].TrimStart().StartsWith(ForKeyword))
                    {
                        if (prelucrare[i].TrimStart().Contains(DoKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + DoKeyword, "");
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        string a1 = Regex.Replace(prelucrare[i], ForKeyword + " (.*)", "$1");
                        // Debug.WriteLine(a1);
                        //prelucrare[i] = Regex.Replace(prelucrare[i], "pentru (.*)", "for ($1");
                        string[] a2 = Regex.Split(a1.TrimStart(), ",");
                        try
                        {
                            string fin = "";
                            for (int sks = 1; sks < a2.Length; sks++)
                            {
                                if (sks == a2.Length - 1) fin += a2[sks];
                                else fin += a2[sks] + ",";
                            }
                            a2[1] = fin;
                        }
                        catch { }
                        string nume_var = sep(a2[0], ":=").TrimEnd();
                        bool edec = false;
                        for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++) if (mat[nt2, 0] != null) if (mat[nt2, 0].TrimStart().TrimEnd() + "\r" == nume_var | mat[nt2, 0].TrimStart().TrimEnd() == nume_var) edec = true;
                        try
                        {
                            if (edec == true)
                            {
                                if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                                prelucrare[i] = Regex.Replace(prelucrare[i], ForKeyword + " (.*)", "for $1 do\n" + spaces + "    begin");
                                prelucrare[i] = Regex.Replace(prelucrare[i], ",", " to ");
                            }
                            else
                            {
                                string denumire;
                                    denumire = nume_var;
                                    mat[nrcurent, 0] = denumire.TrimStart().TrimEnd();
                                    mat[nrcurent, 1] = "int";
                                    nrcurent += 1;
                                    variabile = variabile + nume_var + ":integer;\n";
                                //prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                                //prelucrare[i] = prelucrare[i] + ";";
                                x = true;
                                estevar[i] = true;
                                xxx = true;
                                nrvar += 1;
                                if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                                prelucrare[i] = Regex.Replace(prelucrare[i], ForKeyword + " (.*)", "for $1 do\n" + spaces + "    begin");
                                prelucrare[i] = Regex.Replace(prelucrare[i], ",", " to ");
                            }
                            //if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                            //prelucrare[i] = prelucrare[i] + "; " + nume_var + "++)\n" + spaces + "    { ";
                            x = true;
                        }
                        catch
                        {
                            prelucrare[i] = "";
                        }
                        x = true;
                        //if (prelucrare[i].TrimStart().Contains(DoKeyword)) prelucrare[i] = Regex.Replace(prelucrare[i], " " + DoKeyword, "");
                        //string spaces = GetLeadingWhitespace(prelucrare[i]);

                        x = true;
                    }
                    //Instructiunea sfarsit_pentru
                    if (prelucrare[i].TrimStart().StartsWith(EndForKeyword))
                    {
                        prelucrare[i] = Regex.Replace(prelucrare[i], EndForKeyword, "end;");
                        x = true;
                    }
                    //Instructiunea executa
                    if (prelucrare[i].TrimStart().StartsWith(DoKeyword) && !prelucrare[i].TrimStart().StartsWith("double"))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], DoKeyword, "repeat");
                        x = true;
                    }
                    //Instructiunea executa pana cand (loop until)
                    if (prelucrare[i].TrimStart().StartsWith(LoopUntilKeyword))
                    {
                        string spaces = GetLeadingWhitespace(prelucrare[i]);
                        prelucrare[i] = Regex.Replace(prelucrare[i], LoopUntilKeyword + " (.*)", "until ($1");
                        if (prelucrare[i].Contains(":=")) prelucrare[i] = Regex.Replace(prelucrare[i], ":=", "=");
                        if (prelucrare[i].EndsWith("\r")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
                        prelucrare[i] = prelucrare[i] + ");";
                        x = true;
                    }
                    //System.Diagnostics.Debug.WriteLine(prelucrare[i]);
                    //Prelucrari de final pentru cod Pascal
                    returnare = Regex.Replace(returnare, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
                    if (x == true)
                    {
                        if (i == 0)
                            returnare = "    " + prelucrare[i] + "\n";
                        else
                            returnare = returnare + "" + "    " + prelucrare[i] + "\n";
                    }
                    else
                    {
                        if (prelucrare[i] != Environment.NewLine & prelucrare[i] != "")
                        {

                            if (i == 0)
                            {
                                returnare = "    " + prelucrare[i];
                                //returnare = returnare.Substring(0, returnare.Length - 1);
                                returnare += "\n";
                            }
                            else
                            {
                                returnare = returnare + "" + "    " + prelucrare[i];
                                if (returnare.EndsWith("\r")) returnare = returnare.Substring(0, returnare.Length - 1);
                                returnare = returnare + ";\n";
                            }
                        }
                    }
                }
                #endregion

                if (xxx == false) coresp[i] = returnare.Split('\n').Length;


            }
            strVar = new List<string>();
            int kontor = 0;
            for (int nt2 = 0; nt2 < (mat.Length / 2); nt2++)
            {
                if (mat[nt2, 0] != null)
                {
                    strVar.Add(mat[nt2, 0]);
                    kontor++;
                }
            }
            if (lang == "VB6")
            {
                /*   try
                   {
                       string[] func = Regex.Split(functii, "\r");
                       foreach (string f in func)
                       {
                           string file = "";
                           if (f != "") file = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules\\" + f + ".psf");
                           string[] rez = Regex.Split(file, "//<>");
                           returnare = returnare + "\n" + rez[0];
                       }
                   }
                   catch
                   {
                   }*/
                //returnare = Regex.Replace(returnare, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
            }
            if (lang == "VBSCRIPT")
            {
                //returnare = "<HTML>\n<SCRIPT LANGUAGE=VBSCRIPT>\n" + returnare;// +"</SCRIPT>\n</HTML>";
                int cat = 0;
                /*      try
                      {

                          nr_linii = 0;
                          string[] func = Regex.Split(functii, "\r");
                          foreach (string f in func)
                          {
                              if (nr_linii == 0) nr_linii = -1;
                              cat += 1;
                              string file = "";
                              if (f != "") file = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules\\" + f + ".psf");
                              string[] rez = Regex.Split(file, "//<>");
                              returnare = returnare + "\n" + rez[5] + "\n";
                              nr_linii += 1;

                          }
                          //Debug.WriteLine(nr_linii);
                      }
                      catch
                      {
                          nr_linii = 0;
                      }*/
                if (cat == 2) returnare = "<HTML>\n<SCRIPT LANGUAGE=VBSCRIPT>" + returnare + "</SCRIPT>\n</HTML>";
                else returnare = "<HTML>\n<SCRIPT LANGUAGE=VBSCRIPT>\n" + returnare + "</SCRIPT>\n</HTML>";
                returnare = Regex.Replace(returnare, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
            }
            if (lang == "CPP")
            {
                string dddd = returnare;
                if (infoarena == true) returnare = "#include <iostream>\n#include <math.h>\n#include <fstream>\n\nusing namespace std;\n\nifstream f(\"TEXT_TO_BE_REPLACED\");\nofstream g(\"TEXT_TO_BE_REPLACED\");"; 
                else returnare = "#include <iostream>\n#include <math.h>\n#include <conio.h>\n\nusing namespace std;"; //void main()\n{\n" + returnare + "getch();\n}";
                /*  try
                  {
                      nr_linii = 3;
                      string[] func = Regex.Split(functii, "\r");
                      foreach (string f in func)
                      {
                          string file = "";
                          if (f != "") file = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules\\" + f + ".psf");
                          string[] rez = Regex.Split(file, "//<>");
                          returnare = returnare + "\n" + rez[2];
                          nr_linii += CountLinesInString(rez[2]);
                      }
                  }
                  catch
                  {
                  }*/
                if (infoarena == true) returnare = returnare + "\n\nint main()\n{\n" + dddd + "    return 0;\n}";
                else returnare = returnare + "\n\nint main()\n{\n" + dddd + "    getch();\n    return 0;\n}";
            }
            if (lang == "JAVASCRIPT")
            {
                string aaa2 = Regex.Replace(this.Text, " ", "");
                try
                {
                    variabile = variabile.Substring(0, variabile.Length - 2);
                }
                catch { }
                if (customJavaScriptHeader == false)
                {
                    if (variabile != "") returnare = "<html>\n<script language=javascript>\nfunction " + "Pseudocode" + "()\n{   document.write(\"<font face=\\\"Consolas\\\">\"); var " + variabile + ";\n" + returnare + "}\n"; //</script>\n<body>\n<input type=button name=n1 value=\"" + aaa2 + "\" onclick=\"" + aaa2 + "()\">\n</body></html>";
                    else returnare = "<html>\n<script language=javascript>\nfunction " + "Pseudocode" + "()\n{   document.write(\"<font face=\\\"Consolas\\\">\");\n" + returnare + "}\n"; //</script>\n<body>\n<input type=button name=n1 value=\"" + aaa2 + "\" onclick=\"" + aaa2 + "()\">\n</body></html>";
                    returnare += "</script>\n<body>\n<input type=button name=n1 value=\"" + aaa2 + "\" onclick=\"Pseudocode" + "()\">\n</body></html>"; //+ aaa2

                }
                else
                {
                    if (Properties.Settings.Default.UITheme == 0)
                    {
                        if (variabile != "") returnare = "<html>\n<script language=javascript>\nfunction " + "Pseudocode" + "()\n{   var startX = new Date().getTime();\n     document.write(\"<body bgcolor=#404040>\");\n    document.title = \"" + running + aaa2 + "\";\n   document.write(\"<font color=\\\"white\\\" face=\\\"Consolas\\\">\"); var " + variabile + ";\n" + returnare + "\n"; //</script>\n<body>\n<input type=button name=n1 value=\"" + aaa2 + "\" onclick=\"" + aaa2 + "()\">\n</body></html>";
                        else returnare = "<html>\n<script language=javascript>\nfunction " + "Pseudocode" + "()\n{   var startX = new Date().getTime();\n    document.write(\"<body bgcolor=#404040>\");\n    document.title = \"" + running + aaa2 + "\";\n    document.write(\"<font color=\\\"white\\\" face=\\\"Consolas\\\">\");\n" + returnare + "\n"; //</script>\n<body>\n<input type=button name=n1 value=\"" + aaa2 + "\" onclick=\"" + aaa2 + "()\">\n</body></html>";
                    }
                    else
                    {
                        if (variabile != "") returnare = "<html>\n<script language=javascript>\nfunction " + "Pseudocode" + "()\n{   var startX = new Date().getTime();\n     document.write(\"<body bgcolor=#ffffff>\");\n    document.title = \"" + running + aaa2 + "\";\n    document.write(\"<font face=\\\"Consolas\\\">\"); var " + variabile + ";\n" + returnare + "}\n"; //</script>\n<body>\n<input type=button name=n1 value=\"" + aaa2 + "\" onclick=\"" + aaa2 + "()\">\n</body></html>";
                        else returnare = "<html>\n<script language=javascript>\nfunction " + "Pseudocode" + "()\n{   var startX = new Date().getTime();\n     document.write(\"<body bgcolor=#ffffff>\");\n    document.title = \"" + running + aaa2 + "\";\n   document.write(\"<font face=\\\"Consolas\\\">\");\n" + returnare + "\n"; //</script>\n<body>\n<input type=button name=n1 value=\"" + aaa2 + "\" onclick=\"" + aaa2 + "()\">\n</body></html>";
                    }
                    returnare += "    var endX = new Date().getTime();\n    var timeX = endX - startX;\n    document.write(\"<br><br>" + executionTime + "\" + timeX / 1000 + \" " + seconds + ".<br><br><input type=button name=n1 value=\\\"" + closeWindow + "\\\" onclick=\\\"window.close" + "()\\\">\");\n}\n</script></head>>\n<body onload=\"Pseudocode" + "()\">\n</body></html>"; //+ aaa2
                    returnare = returnare.Replace("<html>", "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/><title>" + running + aaa2 + "</title>");
                }
                                                                                                                                                                      /*    try
                                                                                                                                                                                           {
                                                                                                                                                                                               string[] func = Regex.Split(functii, "\r");
                                                                                                                                                                                               foreach (string f in func)
                                                                                                                                                                                               {
                                                                                                                                                                                                   string file = "";
                                                                                                                                                                                                   if (f != "") file = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules\\" + f + ".psf");
                                                                                                                                                                                                   string[] rez = Regex.Split(file, "//<>");
                                                                                                                                                                                                   returnare = returnare + "\n" + rez[4] + "\n";
                                                                                                                                                                                               }
                                                                                                                                                                                           }
                                                                                                                                                                                           catch
                                                                                                                                                                                           {
                                                                                                                                                                                           }*/

                returnare = Regex.Replace(returnare, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
            }
            if (lang == "C#")
            {
                /*string nou = "";
                string[] final = Regex.Split(returnare, "\r\n");
                for (int kkk = 0; kkk < final.Length; kkk++)
                {
                    nou = "            " + final[kkk] + "\n";
                }*/

                string aaa = Regex.Replace(this.Text, " ", "");
                returnare = "using System;\nusing System.Collections.Generic;\nusing System.Text;\n\nnamespace " + "Pseudocode" + "\n{\n    class Program\n    {\n        static void Main(string[] args)\n        {\n" + returnare + "            Console.ReadLine();\n        }"; //\n    }\n}";
              /*  try
                {
                    string[] func = Regex.Split(functii, "\r");
                    foreach (string f in func)
                    {
                        string file = "";
                        if (f != "") file = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules\\" + f + ".psf");
                        string[] rez = Regex.Split(file, "//<>");
                        returnare = returnare + "\n" + rez[1];
                    }
                }
                catch
                {
                }*/
                returnare = returnare + "\n    }\n}";


                //"#include <iostream.h>\n#include <stdio.h>\n#include <conio.h>\nvoid main()\n{\n" + returnare + "}";
            }
            if (lang == "PAS")
            {
                string dddd = returnare;
                returnare = "";
                while (returnare.TrimStart().StartsWith("\n"))
                {
                    returnare = returnare.Substring(1, returnare.Length - 1);
                }
                while (returnare.EndsWith("\n"))
                {
                    returnare = returnare.Substring(0, returnare.Length - 1);
                }

                /*    try
                    {
                        nr_linii = 1;
                        string[] func = Regex.Split(functii, "\r");
                        //System.Diagnostics.Debug.Write(func.Length);
                        nr_linii = nr_linii - func.Length + 1;
                        foreach (string f in func)
                        {
                            string file = "";
                            if (f != "") file = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules\\" + f + ".psf");
                            string[] rez = Regex.Split(file, "//<>");
                            returnare = returnare + "\n" + rez[3] + "\n";
                            nr_linii += CountLinesInString(rez[3]);

                        }

                    }
                    catch
                    {
                    }*/
                if (variabile != "")
                {
                    string[,] matvaloripas = new string[2, 10000];
                    string[] valoripas;
                    //Debug.WriteLine(variabile);
                    valoripas = Regex.Split(variabile, "\n");
                    string stringuripas = "";
                    string realuripas = "";
                    string integeripas = "";
                    for (int integerrapidpas = 0; integerrapidpas < valoripas.Length - 1; integerrapidpas++)
                    {
                        if (valoripas[integerrapidpas].EndsWith(":integer;"))
                        {
                            if (valoripas[integerrapidpas].Contains("array"))
                            {
                                integeripas += Regex.Replace(valoripas[integerrapidpas], ":integer;", " of integer; ").TrimStart();
                            }
                            else integeripas += Regex.Replace(valoripas[integerrapidpas].TrimStart(), ":integer", " : integer") + " ";
                        }
                        if (valoripas[integerrapidpas].EndsWith(":string;"))
                        {
                            if (valoripas[integerrapidpas].Contains("array"))
                            {
                                realuripas += Regex.Replace(valoripas[integerrapidpas], ":string;", " of string; ").TrimStart();
                            }
                            else realuripas += Regex.Replace(valoripas[integerrapidpas].TrimStart(), ":string", " : string") + " ";
                        }
                        if (valoripas[integerrapidpas].EndsWith(":real;"))
                        {
                            if (valoripas[integerrapidpas].Contains("array"))
                            {
                                stringuripas += Regex.Replace(valoripas[integerrapidpas], ":real;", " of real; ").TrimStart();
                            }
                            else stringuripas += Regex.Replace(valoripas[integerrapidpas].TrimStart(), ":real", " : real") + " ";
                        }
                     //   if (valoripas[integerrapidpas].EndsWith(":real;")) realuripas += Regex.Replace(valoripas[integerrapidpas], ":real;", "") + ", ";
                      //  if (valoripas[integerrapidpas].EndsWith(":string;")) stringuripas += Regex.Replace(valoripas[integerrapidpas], ":string;", "") + ", ";
                    }
                    if (integeripas != "") integeripas = integeripas.Substring(0, integeripas.Length - 2);
                    if (stringuripas != "") stringuripas = stringuripas.Substring(0, stringuripas.Length - 2);
                    if (realuripas != "") realuripas = realuripas.Substring(0, realuripas.Length - 2);
                    returnare = returnare + "    " + integeripas + ";\n    " + realuripas + ";\n    " + stringuripas + ";\n    " + "Begin\n" + dddd + "Readln;\nEnd.";
                    string[] terminarepas;
                    terminarepas = returnare.Split('\n');
                    //numarvariabilepas = 0;

                    if (terminarepas[0] == "     : integer;")
                    {
                        terminarepas[0] = "";
                    }
                    else
                    {
                        isint = true;
                    }
                    if (terminarepas[1] == "     : real;")
                    {
                        terminarepas[1] = "";
                    }
                    else
                    {
                        isreal = true;
                    }
                    if (terminarepas[2] == "     : string;")
                    {
                        terminarepas[2] = "";
                    }
                    else
                    {
                        isstr = true;
                    }
                    returnare = "Program Pseudoco (INPUT, OUTPUT);\nUses Math;\nVar\n";
                    for (int finalizaredecacatpas = 0; finalizaredecacatpas <= terminarepas.Length - 1; finalizaredecacatpas++)
                    {
                        if (terminarepas[finalizaredecacatpas] != "")
                        {
                            returnare = returnare + terminarepas[finalizaredecacatpas] + "\n";
                        }
                    }
                    //if (returnare == "var ")
                    //  returnare = "";
                }
                else returnare = returnare + "PROGRAM Pseudoco (INPUT, OUTPUT);\nUses Math;\n" + "Begin\n" + dddd + "    Readln;\nEnd.";
                returnare = Regex.Replace(returnare, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);

            }



            /*    MessageBox.Show(converterEd.Tag.ToString());
                if (text[text.Length - 1].TrimStart().StartsWith("daca") | text[text.Length - 1].TrimStart().StartsWith("pentru") | text[text.Length - 1].TrimStart().StartsWith("cat_timp") | text[text.Length - 1].TrimStart().StartsWith("executa"))
                {
                    converterEd.Tag = Convert.ToInt32(converterEd.Tag) + 4;
                }
                if (text[text.Length - 1].TrimStart().StartsWith("sfarsit_daca") | text[text.Length - 1].TrimStart().StartsWith("sfarsit_pentru") | text[text.Length - 1].TrimStart().StartsWith("sfarsit_cat_timp") | text[text.Length - 1].TrimStart().StartsWith("pana_cand"))
                {
                    converterEd.Tag = Convert.ToInt32(converterEd.Tag) + 4;
                }
                if (Convert.ToInt32(converterEd.Tag) != 0)
                {
                    int x = pseudocodeEd.Caret.Position;
                    for (int i = 0; i < Convert.ToInt32(converterEd.Tag); i++)
                    {
                        pseudocodeEd.Text += " ";
                    }
                    //pseudocodeEd.Caret.Position = x + Convert.ToInt32(converterEd.Tag);
                }*/



            //plugin executor
            /*foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
            {
                if (_Plugins.ContainsKey(pair.Key))
                {
                    VPlugin plugin = _Plugins[pair.Key];
                    string rez = plugin.PseudocodeFileIsConverted(pseudocodeEd.Text, returnare, lang);
                    if (rez != "")
                    {
                        returnare = rez;
                    }
                }
            }*/
            //plugin executor finish
            /*string[] intermediar = Regex.Split(returnare, "\n");
            returnare = "";
            for (int v = 0; v < intermediar.Length; v++)
            {
                //if (intermediar[v].EndsWith(";;")) intermediar[v] = intermediar[v].Substring(0, intermediar[v].Length - 1);
                if (v == 0)
                    returnare = intermediar[v];
                else
                    returnare = returnare + "\n" + intermediar[v];
            }*/
            //NUMAI VISUAL BASIC
            string[] final = returnare.Split('\n');
            for (int gogo = 0; gogo < final.Length; gogo++)
            {
                if (final[gogo].TrimStart().StartsWith(";")) final[gogo] = final[gogo].Replace(";", "");
                final[gogo] += "\n";
            }
            returnare = "";
            foreach (string tampta in final)
            {
                returnare += tampta;
            }
            return returnare;
            //NUMAI PT C++
            /*string[] prelucrare2 = Regex.Split(returnare, "\n");
            returnare ="";
            for (int i = 0; i < prelucrare2.Length; i++)
            {
                if (!prelucrare2[i].EndsWith(")") && !prelucrare2[i].EndsWith("{") && !prelucrare2[i].EndsWith("}"))
                {
                    prelucrare2[i] = prelucrare2[i]+ ";\n";
                returnare = returnare + prelucrare2[i];
                }
            }
            return returnare;*/
        }
        public long nr_linii = 0;
        int numarvariabilepas = 0;
        bool isint;
        bool isreal;
        bool isstr;
        static long CountLinesInString(string s)
        {
            long count = 1;
            int start = 0;
            while ((start = s.IndexOf('\n', start)) != -1)
            {
                count++;
                start++;
            }
            return count;
        }
        public static string sep(string s, string separator)
        {
            int l = s.IndexOf(separator);
            if (l > 0)
            {
                return s.Substring(0, l);
            }
            return "";

        }

        private void button22_Click(object sender, EventArgs e)
        {
            lang = "VB6";
            //for (int i = 0; i < superTabControl1.Tabs.Count; i++)
            {
                //converterEd.ConfigurationManager.Language = "vbscript";
                //converterEd.Margins[2].Width = 0;
                converterEd.Text = Compile(pseudocodeEd.Text, lang);
                Properties.Settings.Default.Language = 2;
                Properties.Settings.Default.Save();
            }
            button22.BackColor = Color.FromArgb(206, 206, 206);
            button23.BackColor = Color.White;
            button24.BackColor = Color.White;
            button25.BackColor = Color.White;
            button26.BackColor = Color.White;
            button27.BackColor = Color.White;
            pseudocodeEd.Focus();
            SchemaLogica.Visible = false;
            SchemaLogica.SendToBack();
            MainForm mf = this.MdiParent as MainForm;
            mf.toCToolStripMenuItem.Checked = false;
            mf.toBasicClassicToolStripMenuItem.Checked = true;
            mf.toPascalToolStripMenuItem.Checked = false;
            mf.toCToolStripMenuItem1.Checked = false;
            mf.toJavaScriptToolStripMenuItem.Checked = false;
            mf.toLogicSchemeToolStripMenuItem.Checked = false;
        }

        private void button23_Click(object sender, EventArgs e)
        {
            lang = "CPP";
            //  for (int i = 0; i < superTabControl1.Tabs.Count; i++)
            {
                //converterEd.ConfigurationManager.Language = "cpp";
                //converterEd.Margins[2].Width = 0;
                converterEd.Text = Compile(pseudocodeEd.Text, lang);
                Properties.Settings.Default.Language = 1;
                Properties.Settings.Default.Save();
            }
            button23.BackColor = Color.FromArgb(206, 206, 206);
            button22.BackColor = Color.White;
            button24.BackColor = Color.White;
            button25.BackColor = Color.White;
            button26.BackColor = Color.White;
            button27.BackColor = Color.White;
            pseudocodeEd.Focus();
            SchemaLogica.Visible = false;
            SchemaLogica.SendToBack();
            MainForm mf = this.MdiParent as MainForm;
            mf.toCToolStripMenuItem.Checked = true;
            mf.toBasicClassicToolStripMenuItem.Checked = false;
            mf.toPascalToolStripMenuItem.Checked = false;
            mf.toCToolStripMenuItem1.Checked = false;
            mf.toJavaScriptToolStripMenuItem.Checked = false;
            mf.toLogicSchemeToolStripMenuItem.Checked = false;
        }

        private void button24_Click(object sender, EventArgs e)
        {

            lang = "C#";
            //   for (int i = 0; i < superTabControl1.Tabs.Count; i++)
            {
                //converterEd.ConfigurationManager.Language = "cs";
                //converterEd.Margins[2].Width = 0;
                converterEd.Text = Compile(pseudocodeEd.Text, lang);
                Properties.Settings.Default.Language = 4;
                Properties.Settings.Default.Save();
            }
            button24.BackColor = Color.FromArgb(206, 206, 206);
            button23.BackColor = Color.White;
            button22.BackColor = Color.White;
            button25.BackColor = Color.White;
            button26.BackColor = Color.White;
            button27.BackColor = Color.White;
            pseudocodeEd.Focus();
            SchemaLogica.Visible = false;
            SchemaLogica.SendToBack();
            MainForm mf = this.MdiParent as MainForm;
            mf.toCToolStripMenuItem.Checked = false;
            mf.toBasicClassicToolStripMenuItem.Checked = false;
            mf.toPascalToolStripMenuItem.Checked = false;
            mf.toCToolStripMenuItem1.Checked = true;
            mf.toJavaScriptToolStripMenuItem.Checked = false;
            mf.toLogicSchemeToolStripMenuItem.Checked = false;
        }

        private void button25_Click(object sender, EventArgs e)
        {

            lang = "PAS";
            //for (int i = 0; i < superTabControl1.Tabs.Count; i++)
            {
                //converterEd.ConfigurationManager.Language = "cpp";
                //converterEd.Margins[2].Width = 0;
                converterEd.Text = Compile(pseudocodeEd.Text, lang);
                Properties.Settings.Default.Language = 3;
                Properties.Settings.Default.Save();
            }
            button25.BackColor = Color.FromArgb(206, 206, 206);
            button23.BackColor = Color.White;
            button24.BackColor = Color.White;
            button22.BackColor = Color.White;
            button26.BackColor = Color.White;
            button27.BackColor = Color.White;
            pseudocodeEd.Focus();
            SchemaLogica.Visible = false;
            SchemaLogica.SendToBack();
            MainForm mf = this.MdiParent as MainForm;
            mf.toCToolStripMenuItem.Checked = false;
            mf.toBasicClassicToolStripMenuItem.Checked = false;
            mf.toPascalToolStripMenuItem.Checked = true;
            mf.toCToolStripMenuItem1.Checked = false;
            mf.toJavaScriptToolStripMenuItem.Checked = false;
            mf.toLogicSchemeToolStripMenuItem.Checked = false;
        }

        private void button26_Click(object sender, EventArgs e)
        {
            lang = "JAVASCRIPT";
            // for (int i = 0; i < superTabControl1.Tabs.Count; i++)
            {
                //converterEd.ConfigurationManager.Language = "js";
                //converterEd.Margins[2].Width = 0;
                converterEd.Text = Compile(pseudocodeEd.Text, lang);
                Properties.Settings.Default.Language = 5;
                Properties.Settings.Default.Save();
            }
            button26.BackColor = Color.FromArgb(206, 206, 206);
            button23.BackColor = Color.White;
            button24.BackColor = Color.White;
            button25.BackColor = Color.White;
            button22.BackColor = Color.White;
            button27.BackColor = Color.White;
            pseudocodeEd.Focus();
            SchemaLogica.Visible = false;
            SchemaLogica.SendToBack();
            MainForm mf = this.MdiParent as MainForm;
            mf.toCToolStripMenuItem.Checked = false;
            mf.toBasicClassicToolStripMenuItem.Checked = false;
            mf.toPascalToolStripMenuItem.Checked = false;
            mf.toCToolStripMenuItem1.Checked = false;
            mf.toJavaScriptToolStripMenuItem.Checked = true;
            mf.toLogicSchemeToolStripMenuItem.Checked = false;
        }

        private void button27_Click(object sender, EventArgs e)
        {
            lang = "VBSCRIPT";
            // for (int i = 0; i < superTabControl1.Tabs.Count; i++)
            {
                //converterEd.ConfigurationManager.Language = "vbscript";
                //converterEd.Margins[2].Width = 0;
                converterEd.Text = Compile(pseudocodeEd.Text, lang);
                Properties.Settings.Default.Language = 6;
                Properties.Settings.Default.Save();
            }
            button27.BackColor = Color.FromArgb(206, 206, 206);
            button23.BackColor = Color.White;
            button24.BackColor = Color.White;
            button25.BackColor = Color.White;
            button26.BackColor = Color.White;
            button22.BackColor = Color.White;
            pseudocodeEd.Focus();
            if (Properties.Settings.Default.ShowOldVBScript == false)
            {
                SchemaLogica.Visible = true;
                SchemaLogica.BringToFront();
                //SchemaLogica.Paint += SchemaLogica_Paint;
                SchemaLogica.Width = converterEd.Width;
                SchemaLogica.Height = converterEd.Height - 2;
                SchemaLogica.Left = converterEd.Left;
                SchemaLogica.Top = converterEd.Top;
                //SchemaLogica.BackColor = Color.White;
            }
            MainForm mf = this.MdiParent as MainForm;
            mf.toCToolStripMenuItem.Checked = false;
            mf.toBasicClassicToolStripMenuItem.Checked = false;
            mf.toPascalToolStripMenuItem.Checked = false;
            mf.toCToolStripMenuItem1.Checked = false;
            mf.toJavaScriptToolStripMenuItem.Checked = false;
            mf.toLogicSchemeToolStripMenuItem.Checked = true;
        }
        public string AutoSalvare()
        {
            ExTag exTag = (ExTag)(pseudocodeEd.Tag);
            string unique;
            unique = (string)exTag.Get("uniqueString");
            string a = DateTime.Now.ToShortDateString().Replace('/', '.');
            // for (int i = 0; i < superTabControl1.Tabs.Count; i++)
            {
                if (pseudocodeEd.Text != "")
                {
                    if (this.Text.EndsWith("*"))
                    {
                        string l = this.Text.Substring(0, this.Text.Length - 2);
                        File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\AutoSave\\Unsaved-" + a + "-" + l + "-" + unique + ".psc", pseudocodeEd.Text);
                    }
                    else File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\AutoSave\\Unsaved-" + a + "-" + this.Text + "-" + unique + ".psc", pseudocodeEd.Text);
                }
                //return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\AutoSave\\Unsaved-" + DateTime.Now.ToShortDateString() + "-" + this.Text + "-" + unique + ".psc";
            }
            return "";
        }
        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (pseudocodeEd.Text == "scrie \"eternitate\"")
            {
                Drawing dr = new Drawing();
                dr.ShowDialog();
                return;
            }
            progressBarItem1.Visible = true;
            statusBar1.Refresh();
            tabControl3.SelectedIndex = 0;
            buttonItem5.Visible = false;
            Compile(true);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Open open = new Open(this);
            if (open.ShowDialog() != System.Windows.Forms.DialogResult.Ignore)
            {
                ReadyToClose = true;
                this.Close();
            }
            else
            {
                LanguageIdentification();
                
            }
        }
        public bool LanguageIdentification(bool startup = false)
        {
            bool aGasitLimba = false;
            MainForm mf = (MainForm)this.MdiParent;
            for (int i = 0; i <= fileProperties.Count - 1; i++)
            {
                if (fileProperties[i].StartsWith("language"))
                {
                    string limba = fileProperties[i].Split(':')[1];
                    if (limba == "0") mf.fromEnglishCodeToolStripMenuItem.PerformClick();
                    if (limba == "1") mf.fromRomanianPseudoccodeToolStripMenuItem.PerformClick();
                    if (limba == "2" && File.Exists(Application.StartupPath + "\\custom-keywords.txt")) mf.userdefinedToolStripMenuItem.PerformClick();
                    if (!(limba == "2" && !File.Exists(Application.StartupPath + "\\custom-keywords.txt"))) aGasitLimba = true;
                    if (limba == "2" && !File.Exists(Application.StartupPath + "\\custom-keywords.txt"))
                    {
                        if (startup == false) CasetaDeMesaj(this, UnsupportedFile, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            mf.spl.Close();
                            MessageBox.Show(new Form() { TopMost = true }, UnsupportedFile, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        ReadyToClose = true;
                        this.Close();
                        return true;
                    }
                }
            }
            if (aGasitLimba == false)
            {
                UnidentifiedLanguage ul = new UnidentifiedLanguage();
                ul.pseudocodeEd.Text = pseudocodeEd.Text;
                ul.ShowDialog();
                if (ul.PseudocodeLang != -1)
                {
                    if (ul.PseudocodeLang == 0) mf.fromEnglishCodeToolStripMenuItem.PerformClick();
                    if (ul.PseudocodeLang == 1) mf.fromRomanianPseudoccodeToolStripMenuItem.PerformClick();
                    if (ul.PseudocodeLang == 2) mf.userdefinedToolStripMenuItem.PerformClick();
                    fileProperties.Add("language:" + ul.PseudocodeLang.ToString());
                    Salvare();
                }
                else
                {
                    ReadyToClose = true;
                    this.Close();
                    return true;
                }

            }
            if (Properties.Settings.Default.ShowOldVBScript == false) CreateMap();
            return false;
        }
        private void button54_Click(object sender, EventArgs e)
        {
            pseudocodeEd.UndoRedo.Undo();
            pseudocodeEd.Focus();
        }

        private void button55_Click(object sender, EventArgs e)
        {
            pseudocodeEd.UndoRedo.Redo();
            pseudocodeEd.Focus();
        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {
            string text = buttonItem1.Text;
            text = text.Replace(" | ", "\r\n");
            text = text.Replace("FILE NOT YET SAVED", "");
            text = text.Replace("FILE SAVED BY USER", "");
            text = text.Replace("FIȘIER SALVAT DE UTILIZATOR", "");
            text = text.Replace("FIȘIER NESALVAT ÎNCĂ", "");
            text = text.Substring(3, text.Length - 3);
            Stats st = new Stats(text);
            st.ShowDialog();
        }

        private void metroStatusBar1_ItemClick(object sender, EventArgs e)
        {

        }
        public string filesize(string URL)
        {

            string filetype = URL.Substring(URL.LastIndexOf(".") + 1,
                    (URL.Length - URL.LastIndexOf(".") - 1));
            string filename = URL.Substring(URL.LastIndexOf("/") + 1,
                    (URL.Length - URL.LastIndexOf("/") - 1));
            System.Net.WebRequest req = System.Net.HttpWebRequest.Create(URL);
            req.Method = "HEAD";
            System.Net.WebResponse resp = req.GetResponse();
            long ContentLength = 0;
            long result;
            if (long.TryParse(resp.Headers.Get("Content-Length"), out ContentLength))
            {

                if (ContentLength >= 1073741824)
                {
                    return (result = ContentLength / 1073741824).ToString("0.00") + " GO";
                }
                else if (ContentLength >= 1048576)
                {
                    return (result = ContentLength / 1048576).ToString("0.00") + " MO";
                }
                else
                {
                    return (result = ContentLength / 1024).ToString("0.00") + " KO";
                }
            }
            return "";
        }
        static string filename;
        private void webBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (webBrowser1.Url.ToString().EndsWith(".psf"))
            {
                Uri uri = new Uri(webBrowser1.Url.ToString());
                filename = System.IO.Path.GetFileName(uri.LocalPath);
                webBrowser1.GoBack();
                DialogResult dr = CasetaDeMesaj(this, DownloadModule + filename, "Pseudocod", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == System.Windows.Forms.DialogResult.Yes)
                {

                    Computer c = new Computer();
                    try
                    {
                        c.Network.DownloadFile(webBrowser1.Url.ToString(), Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules\\" + filename);
                    }
                    catch { }
                    DialogResult dm = CasetaDeMesaj(this, DownloadOK, "Pseudocod", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dm == System.Windows.Forms.DialogResult.Yes)
                    {
                        pseudocodeEd.Text = "functie:" + System.IO.Path.GetFileNameWithoutExtension(filename) + "\n" + pseudocodeEd.Text;
                    }
                    else
                    {

                    }
                }
                else
                {

                }
            }
        }

        private void tabControl1_TabIndexChanged(object sender, EventArgs e)
        {

        }

        private void button43_Click(object sender, EventArgs e)
        {
            pseudocodeEd.Text = "functie:" + Path.GetDirectoryName(listBox2.SelectedItem.ToString()) + Path.GetFileNameWithoutExtension(listBox2.SelectedItem.ToString()) + "\n" + pseudocodeEd.Text;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DirectoryInfo dinfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\Modules\\");

            // What type of file do we want?...
            FileInfo[] Files = dinfo.GetFiles("*.psf");
            listBox2.Items.Clear();
            // Iterate through each file, displaying only the name inside the listbox...
            foreach (FileInfo file in Files)
            {
                listBox2.Items.Add(file.Name);
            }
        }

        private void button44_Click(object sender, EventArgs e)
        {
            New();
            System.IO.TextReader tw = new System.IO.StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\AutoSave\\" + listBox1.SelectedItem.ToString());

            // write a line of text to the file
            pseudocodeEd.Text = tw.ReadToEnd();

            // close the stream
            tw.Close();

        }

        private void button42_Click(object sender, EventArgs e)
        {
            if (EXEName.Text.Contains("\r")) EXEName.Text = EXEName.Text.Replace("\r", "");
            if (EXECopyright.Text.Contains("\r")) EXECopyright.Text = EXECopyright.Text.Replace("\r", "");
            if (EXEInternalName.Text.Contains("\r")) EXEInternalName.Text = EXEInternalName.Text.Replace("\r", "");
            if (EXEVersion.Text.Contains("\r")) EXEVersion.Text = EXEVersion.Text.Replace("\r", "");
            if (EXEProductName.Text.Contains("\r")) EXEProductName.Text = EXEProductName.Text.Replace("\r", "");
            if (EXECompanyName.Text.Contains("\r")) EXECompanyName.Text = EXECompanyName.Text.Replace("\r", "");
            if (EXEIcon.Text.Contains("\r")) EXEIcon.Text = EXEIcon.Text.Replace("\r", "");
            string text = "";
            if (EXEIcon.Text == "")
            {
                text += "id ICON " + "\"" + (Application.StartupPath + "\\test.ico").Replace('\\', '/') + "\"\r\n";
            }
            else
            {
                text += "id ICON " + "\"" + EXEIcon.Text.Replace('\\', '/') + "\"\r\n";
            }
            if (EXEVersion.Text == "") EXEVersion.Text = "1,0,0,0";
            text += "1 VERSIONINFO\r\nFILEVERSION     " + EXEVersion.Text + ",0\r\n";
            text += "PRODUCTVERSION  " + EXEVersion.Text + ",0\r\n";
            text += "BEGIN\r\n  BLOCK \"StringFileInfo\"\r\n  BEGIN\r\n    BLOCK \"080904E4\"\r\n    BEGIN\r\n      VALUE \"CompanyName\", \"" + EXECompanyName.Text + "\"" + "\r\n";
            text += "      VALUE \"FileDescription\", \"" + EXEName.Text + "\"\r\n";
            text += "      VALUE \"FileVersion\", \"" + EXEVersion.Text + "\"\r\n";
            text += "      VALUE \"InternalName\", \"" + EXEInternalName.Text + "\"\r\n";
            text += "      VALUE \"LegalCopyright\", \"" + EXECopyright.Text + "\"\r\n";
            text += "      VALUE \"OriginalFilename\", \"Pseudocode.exe\"\r\n";
            text += "      VALUE \"ProductName\", \"" + EXEProductName.Text + "\"\r\n";
            text += "      VALUE \"ProductVersion\", \"" + Application.ProductVersion.ToString() + "\"\r\n";
            text += "    END\r\n  END\r\n\r\n  BLOCK \"VarFileInfo\"\r\n  BEGIN\r\n    VALUE \"Translation\", 0x809, 1252\r\n  END\r\nEND";
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc");
            StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc");
            sw.Write(text);
            sw.Close();
            panel14.Visible = true;
            backgroundWorker5.RunWorkerAsync();
            /*string icon = "null";
            if (EXEIcon.Text != "") icon = EXEIcon.Text;
            string[] args = { "\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs" + "\"", "\"" + EXELocation.Text + "\"", "\"" + icon + "\"" };
            if (EXELocation.Text == "")
            {
                CasetaDeMesaj(this, NoFileLocationSpecified, "Pseudocod", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    Compile(false);
                    Process p = Process.Start(Application.StartupPath + "\\Compiler.exe", String.Join(" ", args));
                }
                catch
                {
                    CasetaDeMesaj(this, CompileInfo, "Elevare necesară", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }*/
        }

        private void button39_Click(object sender, EventArgs e)
        {
            if (openFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                EXEIcon.Text = openFileDialog2.FileName;
            }
            else
            {

            }
        }

        private void button40_Click(object sender, EventArgs e)
        {
            if (saveFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                EXELocation.Text = saveFileDialog2.FileName;
            }
            else
            {

            }
        }

     /*   private void superTabControl1_SelectedTabChanging(object sender, DevComponents.DotNetBar.SuperTabStripSelectedTabChangingEventArgs e)
        {

        }*/

        public void ExportLa(string lang2, bool deschidere)
        {

            var scintilla1 = pseudocodeEd;
            var scintilla2 = converterEd;
            if (lang2 == "CPP")
            {
                saveFileDialog3.Filter = CPPSource;
                if (saveFileDialog3.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fost;
                    fost = lang;
                    lang = "CPP";
                    scintilla2.Text = Compile(scintilla1.Text, lang);
                    string[] text;
                    text = Regex.Split(scintilla2.Text, "\n");
                    string final = "";
                    for (int i = 0; i < text.Length; i++)
                    {
                        if (!text[i].EndsWith("as Integer") & !text[i].EndsWith("as Long"))
                            final += text[i] + "\r\n";
                    }
                    System.IO.TextWriter tw = new System.IO.StreamWriter(saveFileDialog3.FileName);
                    // write a line of text to the file
                    tw.WriteLine(final);
                    // close the stream
                    tw.Close();
                    lang = fost;

                }
            }
            if (lang2 == "VB6")
            {
                saveFileDialog3.Filter = VB6Source;
                if (saveFileDialog3.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fost;
                    fost = lang;
                    lang = "VB6";
                    scintilla2.Text = Compile(scintilla1.Text, lang);
                    string[] text;
                    text = Regex.Split(scintilla2.Text, "\n");
                    string final = "";
                    for (int i = 0; i < text.Length; i++)
                    {
                        if (!text[i].EndsWith("as Integer") & !text[i].EndsWith("as Long"))
                            final += text[i] + "\r\n";
                    }
                    System.IO.TextWriter tw = new System.IO.StreamWriter(saveFileDialog3.FileName);
                    // write a line of text to the file
                    tw.WriteLine(final);
                    // close the stream
                    tw.Close();
                    lang = fost;
                }
            }
            if (lang2 == "C#")
            {
                saveFileDialog3.Filter = CSSource;
                if (saveFileDialog3.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fost;
                    fost = lang;
                    lang = "C#";
                    scintilla2.Text = Compile(scintilla1.Text, lang);
                    string[] text;
                    text = Regex.Split(scintilla2.Text, "\n");
                    string final = "";
                    for (int i = 0; i < text.Length; i++)
                    {
                        if (!text[i].EndsWith("as Integer") & !text[i].EndsWith("as Long"))
                            final += text[i] + "\r\n";
                    }
                    System.IO.TextWriter tw = new System.IO.StreamWriter(saveFileDialog3.FileName);
                    // write a line of text to the file
                    tw.WriteLine(final);
                    // close the stream
                    tw.Close();
                    lang = fost;
                }
            }
            if (lang2 == "PAS")
            {
                saveFileDialog3.Filter = PASSource;
                if (saveFileDialog3.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fost;
                    fost = lang;
                    lang = "PAS";
                    scintilla2.Text = Compile(scintilla1.Text, lang);
                    string[] text;
                    text = Regex.Split(scintilla2.Text, "\n");
                    string final = "";
                    for (int i = 0; i < text.Length; i++)
                    {
                        if (!text[i].EndsWith("as Integer") & !text[i].EndsWith("as Long"))
                            final += text[i] + "\r\n";
                    }
                    System.IO.TextWriter tw = new System.IO.StreamWriter(saveFileDialog3.FileName);
                    // write a line of text to the file
                    tw.WriteLine(final);
                    // close the stream
                    tw.Close();
                    lang = fost;

                }
            }
            if (lang2 == "JAVASCRIPT")
            {
                saveFileDialog3.Filter = WebPage;
                if (saveFileDialog3.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fost;
                    fost = lang;
                    lang = "JAVASCRIPT";
                    scintilla2.Text = Compile(scintilla1.Text, lang);
                    string[] text;
                    text = Regex.Split(scintilla2.Text, "\n");
                    string final = "";
                    for (int i = 0; i < text.Length; i++)
                    {
                        if (!text[i].EndsWith("as Integer") & !text[i].EndsWith("as Long"))
                            final += text[i] + "\r\n";
                    }
                    System.IO.TextWriter tw = new System.IO.StreamWriter(saveFileDialog3.FileName);
                    // write a line of text to the file
                    tw.WriteLine(final);
                    // close the stream
                    tw.Close();
                    lang = fost;
                    try
                    {
                        System.Diagnostics.Process.Start(saveFileDialog3.FileName);
                    }
                    catch
                    {
                        CasetaDeMesaj(this.MdiParent, NoBrowserInstalled, "Pseudocod", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            if (lang2 == "VBSCRIPT")
            {
                saveFileDialog3.Filter = WebPage;
                if (saveFileDialog3.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fost;
                    fost = lang;
                    lang = "VBSCRIPT";
                    scintilla2.Text = Compile(scintilla1.Text, lang);
                    string[] text;
                    text = Regex.Split(scintilla2.Text, "\n");
                    string final = "";
                    for (int i = 0; i < text.Length; i++)
                    {
                        if (!text[i].EndsWith("as Integer") & !text[i].EndsWith("as Long"))
                            final += text[i] + "\r\n";
                    }
                    System.IO.TextWriter tw = new System.IO.StreamWriter(saveFileDialog3.FileName);
                    // write a line of text to the file
                    tw.WriteLine(final);
                    // close the stream
                    tw.Close();
                    lang = fost;
                    try
                    {
                        System.Diagnostics.Process.Start("IEXPLORE.EXE", saveFileDialog3.FileName);
                    }
                    catch
                    {
                        CasetaDeMesaj(this.MdiParent, NoBrowserInstalled, "Pseudocod", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            //panel11.Visible = false;
        }

        private void button29_Click(object sender, EventArgs e)
        {
            ExportLa("VB6", false);
        }

        private void button31_Click(object sender, EventArgs e)
        {
            ExportLa("CPP", false);
        }

        private void button32_Click(object sender, EventArgs e)
        {
            ExportLa("C#", false);
        }

        private void button33_Click(object sender, EventArgs e)
        {
            ExportLa("PAS", false);
        }

        private void button34_Click(object sender, EventArgs e)
        {
            ExportLa("JAVASCRIPT", true);
        }

        private void button35_Click(object sender, EventArgs e)
        {
            ExportLa("VBSCRIPT", true);
        }

        private void button51_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{F1}");
        }

        private void button50_Click(object sender, EventArgs e)
        {
            panel12.Visible = false;
            ScreenCapture sc = new ScreenCapture();
            // capture entire screen, and save it to a file
            Image img = sc.CaptureScreen();
            // display image in a Picture control named imageDisplay
            this.imageDisplay.Image = img;
            // capture this window, and save it
            sc.CaptureWindowToFile(this.Handle, Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.png", System.Drawing.Imaging.ImageFormat.Png);
            Feedback mail = new Feedback(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.png");
            mail.ShowDialog();

        }

        private void splashWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void splashWorker_DoWork(object sender, DoWorkEventArgs e)
        {

        }
        int currentLineBeforeEnter = 0;
        int numberOfLinesBeforeEnter = 0;
        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hWnd);
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Alt | Keys.Home))
            {
                MainForm mf = this.MdiParent as MainForm;
                mf.codeboardToolStripMenuItem.PerformClick();
                /*MessageBoxI msg2 = new MessageBoxI(this, "", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                msg2.Show();
                msg2.panel1.Visible = false;
                Welcome wel = new Welcome(this, true, true);
                wel.ShowDialog();
                msg2.Close();
                SetForegroundWindow(this.Handle);*/
                return true;
            }
            if (keyData == (Keys.Control | Keys.I))
            {
                MainForm frm = this.MdiParent as MainForm;
                frm.uploadProblemToInfoarenaToolStripMenuItem.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.E))
            {
                MainForm frm = this.MdiParent as MainForm;
                frm.createApplicationToolStripMenuItem.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.Q))
            {
                MainForm frm = this.MdiParent as MainForm;
                frm.SearchBox.Focus();
                return true;
            }
            if (keyData == (Keys.Control | Keys.O))
            {
                MainForm frm = this.MdiParent as MainForm;
                frm.openPseudocodeToolStripMenuItem.PerformClick();
                //button5.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.S))
            {
                button1.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.Z))
            {
                button56.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.Y))
            {
                button55.PerformClick();
                return true;
            }
            if (keyData == (Keys.F5))
            {
                button11.PerformClick();
                return true;
            }
            if (keyData == (Keys.F6))
            {
                tabControl3.SelectedIndex = 0;
                progressBarItem1.Visible = true;
                statusBar1.Refresh();
                Compile(false);
                return true;
            }
            /*if (keyData == (Keys.F9))
            {
                button11.PerformClick();
                return true;
            }*/
            if (keyData == (Keys.Control | Keys.P))
            {
                button7.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.N))
            {
                button3.PerformClick();
                return true;
            }
            if (keyData == (Keys.Enter))
            {
                try
                {
                    u = 0;
                    for (int i = 0; i < 1000; i++) a[i] = 0;
                    for (int i = 0; i < pseudocodeEd.Lines.Count; i++)
                    {
                        var list = pseudocodeEd.Markers.GetMarkers(i);

                        foreach (ScintillaNET.Marker mk in list)
                        {
                            if (mk.ToString() != "MarkerNumber0" && mk.ToString() == "MarkerNumber23")
                            {
                                a[u] = i;
                                u++;

                            }

                        }
                    }
                    markerTimer.Enabled = true;

                    if (pseudocodeEd.AutoComplete.IsActive == true)
                    {
                        //int a = pseudocodeEd.Lines.Current.Number;
                        //pseudocodeEd.Lines.Current.Text  += "\r\n";
                        //pseudocodeEd.GoTo.Line(a + 1);
                        pseudocodeEd.AutoComplete.Cancel();
                        SendKeys.Send("{ESC}");
                    }
                    currentLineBeforeEnter = pseudocodeEd.Lines.Current.Number;
                    numberOfLinesBeforeEnter = pseudocodeEd.Lines.Count;

                    var pos = pseudocodeEd.NativeInterface.GetCurrentPos();
                    pozitieNoua = pseudocodeEd.Lines.Current.Number + 1;
                    pseudocodeEd.Text = pseudocodeEd.Text.Substring(0, pos) + "\n" + pseudocodeEd.Text.Substring(pos, pseudocodeEd.Text.Length - pos);

                    EnterTimer.Enabled = true;
                   // pseudocodeEd.GoTo.Line(pozitieNoua + 2);
                    //pseudocodeEd.GoTo.Position(pos);
                    flagReident = true;
                    flagEnterWasPressed = true;
                }
                catch { }
                // else
                // {
                /* if ((pseudocodeEd.Lines.Current.Number + 2) == pseudocodeEd.Lines.Count)
                 {
                     int a = pseudocodeEd.Lines.Current.Number;
                     pseudocodeEd.Lines.Current.Text += "\r\n";
                     pseudocodeEd.GoTo.Line(a + 1);
                     pseudocodeEd.AutoComplete.Cancel();
                 }
                 else
                 {
                     int a = pseudocodeEd.Lines.Current.Number;
                     //pseudocodeEd.Lines.Current.Text += "\n";
                     //pseudocodeEd.Lines.Current.Text = pseudocodeEd.Lines.Current.Text.Substring(0, pseudocodeEd.Lines.Current.Length - 3);
                     pseudocodeEd.GoTo.Line(a + 1);
                     pseudocodeEd.Lines.Current.Text = "\n" + pseudocodeEd.Lines.Current.Text;

                 }*/
                //pseudocodeEd.Lines.Current.Text = pseudocodeEd.Lines.Current.Text.Substring(0, pseudocodeEd.Lines.Current.Length - 1);

                //   }
                //MessageBox.Show(pseudocodeEd.Lines.Current.Number.ToString() + "\n" + pseudocodeEd.Lines.Count.ToString());

                /*if ((pseudocodeEd.Lines.Current.Number + 2) == pseudocodeEd.Lines.Count)
                {
                    string[] tt = pseudocodeEd.Text.Split('\n');
                    //button2.Text = Counter.ToString();
                    foreach (string sta in tt)
                    {
                        if (sta.TrimStart().StartsWith("daca") | sta.TrimStart().StartsWith("cat_timp") | sta.TrimStart().StartsWith("pentru") | sta.TrimStart().StartsWith("executa")) Counter += 1;
                        if (sta.TrimStart().StartsWith("sfarsit_daca")) Counter -= 1;
                    }
                    //MessageBox.Show(Counter.ToString());
                    if (Counter != 0)
                    {
                        for (int i = 0; i < Counter; i++)
                        {
                            pseudocodeEd.Text += "    ";
                        }

                        pseudocodeEd.GoTo.Position(pseudocodeEd.Text.Length);
                        //if (pseudocodeEd.AutoComplete.IsActive == true) pseudocodeEd.Text = pseudocodeEd.Text.Substring(0, pseudocodeEd.Text.Length - 1);
                    }
                    else pseudocodeEd.GoTo.Position(pseudocodeEd.Text.Length);

                }
                else
                {
                    //MessageBox.Show(pseudocodeEd.Lines.Current.ToString());
                    string[] tt = pseudocodeEd.Text.Split('\n');
                    if (pseudocodeEd.Lines.Current.Text.ToString().TrimStart().StartsWith("daca"))
                    {
                        for (int i = pseudocodeEd.Lines.Current.Number + 1; i < pseudocodeEd.Lines.Count; i++)
                        {
                            tt[i] = "    " + tt[i];
                        }
                        pseudocodeEd.Text = "";
                        foreach (string sted in tt)
                        {
                            pseudocodeEd.Text += sted + "\n";
                        }
                        pseudocodeEd.Text.Replace("\r\n\r\n", "");
                    }
                }*/
                return true;
                //MessageBox.Show(Counter.ToString());

            }
            if (keyData == (Keys.Control | Keys.Alt | Keys.Back))
            {
                if (Properties.Settings.Default.AdvancedUpdatesWarningShown == false)
                {
                    //plugin executor
                    foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
                    {
                        if (_Plugins.ContainsKey(pair.Key))
                        {
                            VPlugin plugin = _Plugins[pair.Key];
                            string rez = plugin.Perform("com.valinet.pseudocode.tools.config", "");
                            if (rez == "true")
                            {
                                return true;
                            }
                        }
                    }
                    //plugin executor finish
                    DialogResult dr = CasetaDeMesaj(this, WarningAdvancedUSersOnly, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dr == System.Windows.Forms.DialogResult.No)
                    {
                        Properties.Settings.Default.AdvancedUpdatesWarningShown = true;
                        Properties.Settings.Default.Save();
                    }
                    Config c = new Config(this);
                    c.ShowDialog();
                    return true;
                }
                else
                {
                    //plugin executor
                    /*foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
                    {
                        if (_Plugins.ContainsKey(pair.Key))
                        {
                            VPlugin plugin = _Plugins[pair.Key];
                            bool rez = plugin.Options();
                            if (rez == true)
                            {
                                return true;
                            }
                        }
                    }*/
                    //plugin executor finish
                    Config c = new Config(this);
                    c.ShowDialog();
                    return true;
                }
            }
            if (keyData == (Keys.Space))
            {
                if (pseudocodeEd.AutoComplete.IsActive == true)
                {
                    pseudocodeEd.AutoComplete.Cancel();
                }
            }
            if (keyData == (Keys.F1))
            {
                MainForm mf = this.MdiParent as MainForm;
                mf.time = 0;
                mf.Loop.Enabled = true;
                /*try
                {
                    Browser code = new Browser("http://valinet-help.somee.com/");
                    code.Text = "Asistență";
                    code.MdiParent = this;
                    code.Show();
                    //Skybound.Gecko.GeckoPreferences.User["general.useragent.override"] = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.26) Gecko";
                }
                catch
                {
                }
                return true;*/
            }
            if (keyData == (Keys.Control | Keys.F))
            {
                MainForm mf = this.MdiParent as MainForm;
                mf.findToolStripMenuItem.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.J))
            {
                button19.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.H))
            {
                MainForm mf = this.MdiParent as MainForm;
                mf.replaceToolStripMenuItem.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.U))
            {
                button10.PerformClick();
                return true;
            }
            /*if (keyData == (Keys.Home))
            {
                if (splitContainer1.Visible) office2007StartButton1.RaiseClick();
                return true;
            }*/
            /*if (keyData == (Keys.Escape))
            {
                if (splitContainer1.Visible) office2007StartButton1.RaiseClick();
                return true;
            }*/
            if (keyData == (Keys.F10))
            {
                button20.PerformClick();
                return true;
            }
            if (keyData == (Keys.F7))
            {
                button66.PerformClick();
                return true;
            }
            /*if (keyData == (Keys.F9))
            {
                button21.PerformClick();
                return true;
            }*/
            if (keyData == (Keys.F11))
            {
                button8.PerformClick();
                return true;
            }
            if (keyData == (Keys.F8))
            {
                CompileDebugRun();

                return true;
            }
            if (keyData == (Keys.Control | Keys.F9))
            {
                addremoveBreakpointOnThisLineToolStripMenuItem.PerformClick();
                return true;
            }
            if (keyData == (Keys.F9))
            {
                addremoveWatchOnThisVariableToolStripMenuItem.PerformClick();
                return true;
            }
            if (keyData == (Keys.F12))
            {
                goToDefinitionToolStripMenuItem.PerformClick();
                return true;
            }
            if (keyData == (Keys.F4))
            {
                renameVariableToolStripMenuItem.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.D1))
            {
                button23.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.D2))
            {
                button22.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.D3))
            {
                button25.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.D4))
            {
                button24.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.D5))
            {
                button26.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.D6))
            {
                button27.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.V))
            {
                button12.PerformClick();
                return true;
            }
            if (keyData == (Keys.Shift | Keys.F5))
            {
                button65.PerformClick();
                return true;
            }
            if (keyData == (Keys.Control | Keys.W))
            {
                this.ReadyToClose = true;
                this.Close();
                return true;
            }
            if (keyData == (Keys.Control | Keys.Shift | Keys.F5))
            {
                button65.PerformClick();
                button61.PerformClick();
                return true;
            }
            /*if (keyData == (Keys.Menu))
            {
                Code child = ActiveMdiChild as Code;
                if (child != null)
                {
                    child.menuStrip1.Visible = !child.menuStrip1.Visible;
                    Properties.Settings.Default.ShowMenu = child.menuStrip1.Visible;
                    Properties.Settings.Default.Save();
                }
                return true;
            }*/
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void linkLabel2_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                SendKeys.Send("{F1}");
                Application.DoEvents();
                if (Properties.Settings.Default.UILang == 1)
                {
                    while (FallDownTimer.Enabled == true)
                    {
                        Application.DoEvents();
                    }
                    //HelpBrowser.Load(Application.StartupPath + "\\helpcontent\\ro-RO\\template-aplicatia-pseudocod-module.htm");
                }
            }
            catch
            {
                CasetaDeMesaj(this.MdiParent, NoBrowserInstalled, "Pseudocod", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                SendKeys.Send("{F1}");
                Application.DoEvents();
                if (Properties.Settings.Default.UILang == 1)
                {
                    while (FallDownTimer.Enabled == true)
                    {
                        Application.DoEvents();
                    }
                    //HelpBrowser.Load(Application.StartupPath + "\\helpcontent\\ro-RO\\template-aplicatia-pseudocod-autosalvare.htm");
                }
            }
            catch
            {
                CasetaDeMesaj(this.MdiParent, NoBrowserInstalled, "Pseudocod", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {


        }

        const int FEATURE_DISABLE_NAVIGATION_SOUNDS = 21;
        const int SET_FEATURE_ON_PROCESS = 0x00000002;

        [DllImport("urlmon.dll")]
        [PreserveSig]
        [return: MarshalAs(UnmanagedType.Error)]
        static extern int CoInternetSetFeatureEnabled(
            int FeatureEntry,
            [MarshalAs(UnmanagedType.U4)] int dwFlags,
            bool fEnable);

        static void DisableClickSounds()
        {
            CoInternetSetFeatureEnabled(
                FEATURE_DISABLE_NAVIGATION_SOUNDS,
                SET_FEATURE_ON_PROCESS,
                true);
        }

        private void button1_MouseEnter_1(object sender, EventArgs e)
        {
            /*webBrowser2.Left = button1.Left + panel3.Left;
            webBrowser2.Visible = true;
            webBrowser2.BringToFront();
            webBrowser2.ScrollBarsEnabled = false;
            webBrowser2.IsWebBrowserContextMenuEnabled = false;
            webBrowser2.ScriptErrorsSuppressed = true;
            webBrowser2.DocumentText = "<font face=\"Segoe UI\"><p style=\"font-size:12px\"><b>Salvare (Ctrl + S)</b><br>Salvează fișierul pseudocod deschis în acest moment.<br><b>Apăsați [F1] pentru asistență suplimentară.</b>";*/

        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            //webBrowser2.Visible = false;
        }

        private void button56_MouseMove(object sender, MouseEventArgs e)
        {
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            /*Control control = GetChildAtPoint(e.Location);
            Debug.WriteLine(GetChildAtPoint(e.Location).ToString());
            MessageBox.Show(GetChildAtPoint(e.Location).ToString());
            if (control == button56)
            {
                MessageBox.Show("da");
                if (control.Enabled == false)
                {
                    string toolTipString = toolTip1.GetToolTip(control);
                    // trigger the tooltip with no delay and some basic positioning just to give you an idea
                    toolTip1.Show(toolTipString, control, control.Width / 2, control.Height / 2);
                }
            }*/
        }

        private void sliderItem1_ValueChanged(object sender, EventArgs e)
        {
            // for (int i = 0; i < superTabControl1.Tabs.Count; i++)
            //  {
            // pseudocodeEd.Zoom = sliderItem1.Value;
            //  converterEd.Zoom = sliderItem1.Value;
            //   }
            //  Properties.Settings.Default.Zoom = sliderItem1.Value;
            //  Properties.Settings.Default.Save();
            // if (sliderItem1.Value == 0) sliderItem1.Text = "100 %";
            // else
            /*  {
                  if (sliderItem1.Value > 0)
                  {
                      sliderItem1.Text = (sliderItem1.Value * 100 + 100).ToString() + " %";
                  }
                  else
                  {
                      if (sliderItem1.Value == -1) sliderItem1.Text = "90 %";
                      if (sliderItem1.Value == -2) sliderItem1.Text = "80 %";
                      if (sliderItem1.Value == -3) sliderItem1.Text = "70 %";
                      if (sliderItem1.Value == -4) sliderItem1.Text = "60 %";
                      if (sliderItem1.Value == -5) sliderItem1.Text = "50 %";
                      if (sliderItem1.Value == -6) sliderItem1.Text = "40 %";
                      if (sliderItem1.Value == -7) sliderItem1.Text = "30 %";
                      if (sliderItem1.Value == -8) sliderItem1.Text = "20 %";
              */
            //sliderItem1.Text = (sliderItem1.Value * 100).ToString() + " %";
            //     }
            //  }
            //converterEd.Font = new Font(converterEd.Font.FontFamily, sliderItem1.Value);
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button56.PerformClick();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button55.PerformClick();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button13.PerformClick();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button14.PerformClick();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button12.PerformClick();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button17.PerformClick();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button18.PerformClick();
        }

        private void buttonItem2_Click(object sender, EventArgs e)
        {
            if (HelpPanel.Tag.ToString() == "visible")
            {
                //HelpBrowser.ShowDevTools();
            }
            else
            {
                string text = buttonItem1.Text;
                text = text.Replace(" | ", "\r\n");
                text = text.Replace("FILE NOT YET SAVED", "");
                text = text.Replace("FILE SAVED BY USER", "");
                text = text.Replace("FIȘIER SALVAT DE UTILIZATOR", "");
                text = text.Replace("FIȘIER NESALVAT ÎNCĂ", "");
                text = text.Substring(3, text.Length - 3);
                Stats st = new Stats(text);
                st.ShowDialog();
            }

        }

        private void startUpFocusTimer_Tick(object sender, EventArgs e)
        {
            pseudocodeEd.Focus();

        }
        Process processPSC;
        Process processPSC2;
        List<Car> cars;
        DialogResult drBW2;
        public List<List<string>> lista_executii = new List<List<string>>();
        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            //new code which uses the MinGW c++ compiler
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
            cmdStartInfo.Arguments = "-g -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe" + "\" \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp\"";
            cmdStartInfo.RedirectStandardOutput = true;
            cmdStartInfo.RedirectStandardError = true;
            cmdStartInfo.RedirectStandardInput = true;
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            processPSC = new Process();
            processPSC.StartInfo = cmdStartInfo;
            processPSC.ErrorDataReceived += cmd_Error;
            processPSC.OutputDataReceived += cmd_DataReceived;
            cars = new List<Car>();
            processPSC.Start(); //"cscript.exe", "//Nologo " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs");
            processPSC.BeginOutputReadLine();
            processPSC.BeginErrorReadLine();
            processPSC.WaitForExit();
            DoOnUIThread(delegate()
            {
                buttonItem5.Visible = false;
                buttonItem6.Visible = false;
                if (cars.Count == 0) buttonItem6.Visible = true;
                else buttonItem5.Visible = true;
                HideCompileStatus.Enabled = true;
                dataGridView1.DataSource = cars;
                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView1.MultiSelect = false;
                dataGridView1.CellContentDoubleClick += dataGridView1_CellContentDoubleClick;
                progressBarItem1.Visible = false;
            });
            if (cars.Count == 0)
            {
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.o")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.o");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp.gcov")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp.gcov");
                ProcessStartInfo cmdStartInfox = new ProcessStartInfo();
                cmdStartInfox.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
                cmdStartInfox.Arguments = "-g --coverage -c \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp\"" + " -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.o" + "\"";
                cmdStartInfox.UseShellExecute = false;
                cmdStartInfox.CreateNoWindow = true;
                Process processx = new Process();
                processx.StartInfo = cmdStartInfox;
                processx.Start();
                processx.WaitForExit();
                ProcessStartInfo cmdStartInfo2 = new ProcessStartInfo();
                cmdStartInfo2.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
                cmdStartInfo2.Arguments = "--coverage -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe\"" + " \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.o" + "\"";
                cmdStartInfo2.UseShellExecute = false;
                cmdStartInfo2.CreateNoWindow = true;
                Process process2 = new Process();
                process2.StartInfo = cmdStartInfo2;
                process2.Start();
                process2.WaitForExit();
                ProcessStartInfo cmdStartInfo3 = new ProcessStartInfo();
                cmdStartInfo3.FileName = Application.StartupPath + @"\cb_console_runner.exe";
                cmdStartInfo3.Arguments = "\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe\"";
                Process process3 = new Process();
                process3.StartInfo = cmdStartInfo3;
                process3.Start();
                process3.WaitForExit();
                if (Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Platform == PlatformID.Win32NT)
                {
                    // The following code has been tested and it is confirmed to work
                    // on the following Microsoft Windows operating system releases:
                    // Windows 7 Service Pack 1, Windows 8.1 Pro (with Update)
                    ProcessStartInfo cmdStartInfo4 = new ProcessStartInfo();
                    cmdStartInfo4.FileName = Properties.Settings.Default.MinGWPath + "\\bin\\gcov.exe";
                    cmdStartInfo4.Arguments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp";
                    cmdStartInfo4.WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode";
                    //cmdStartInfo4.RedirectStandardInput = true;
                    //cmdStartInfo4.RedirectStandardOutput = true;
                    cmdStartInfo4.UseShellExecute = false;
                    cmdStartInfo4.CreateNoWindow = true;
                    Process process4 = new Process();
                    process4.StartInfo = cmdStartInfo4;
                    process4.Start();
                    //MessageBox.Show(process4.HasExited.ToString());
                    //process4.StandardInput.WriteLine("\"" + Properties.Settings.Default.MinGWPath + "\\bin\\gcov.exe\" " + "\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp" + "\"");
                    //MessageBox.Show(process4.StandardOutput.ReadToEnd());
                    //process4.StandardInput.WriteLine("exit");
                    process4.WaitForExit();
                }
                else
                {
                    // The following code has been tested and it is confirmed to work
                    // on the following Microsoft Windows operating system releases:
                    // Windows XP Professional Service Pack 3
                    if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.lnk")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.lnk");
                    using (ShellLink shortcut = new ShellLink())
                    {
                        shortcut.Target = "\"" + Properties.Settings.Default.MinGWPath + "\\bin\\gcov.exe" + "\"";
                        shortcut.WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode";
                        shortcut.Description = "Gcov";
                        shortcut.DisplayMode = ShellLink.LinkDisplayMode.edmMinimized;
                        shortcut.Arguments = "\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp" + "\"";
                        shortcut.Save(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.lnk");
                        Process process4 = new Process();
                        process4.StartInfo.FileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.lnk";
                        process4.Start();
                        while (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp.gcov"))
                        {};
                        try
                        {
                            File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.lnk");
                            process4.Kill();
                        }
                        catch { };
                        while (Process.GetProcessesByName("gcov").Length != 0)
                        { };
                    }
                }
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\atomicity.h.gcov")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\atomicity.h.gcov");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\basic_string.h.gcov")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\basic_string.h.gcov");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\char_traits.h.gcov")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\char_traits.h.gcov");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\gthr-default.h.gcov")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\gthr-default.h.gcov");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\ios_base.h.gcov")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\ios_base.h.gcov");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\iostream.gcov")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\iostream.gcov");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\new_allocator.h.gcov")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\new_allocator.h.gcov");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.gcda")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.gcda");
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.gcno")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.gcno");
                try
                {
                    StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp.gcov");
                    corespA = coresp;
                    string[] txt = sr.ReadToEnd().Split(':');
                    sr.Close();
                    lista_executii = new List<List<string>>();
                    List<string> lsx = new List<string>();
                    lsx.Add("0");
                    lsx.Add("0");
                    lista_executii.Add(lsx);
                    for (int i = 0; i < txt.Length; i++)
                    {
                        if (txt[i].EndsWith("\r\n")) break;
                        try
                        {
                            if (txt[i + 1].Trim() != "0")
                            {
                                List<string> ls = new List<string>();
                                if (txt[i].Contains('\n')) ls.Add(txt[i].Split('\n')[1].Trim());
                                else ls.Add(txt[i].Trim());
                                ls.Add(txt[i + 1].Trim());
                                lista_executii.Add(ls);
                            }
                        }
                        catch
                        {
                            List<string> ls = new List<string>();
                            if (txt[i].Contains('\n')) ls.Add(txt[i].Split('\n')[1].Trim());
                            else ls.Add(txt[i].Trim());
                            ls.Add(txt[i + 1].Trim());
                            lista_executii.Add(ls);
                        }
                        do
                        {
                            i = i + 1;
                            // MessageBox.Show(txt[i]);
                        }
                        while (!txt[i].Contains("\n"));
                        i = i - 1;
                        //MessageBox.Show(ls[0] + " " + ls[1]);

                    }
                    DoOnUIThread(delegate()
                    {
                        if (Properties.Settings.Default.ShowOldVBScript == false) CreateMap();
                    });
                    // code marked like this is old working code - replace if needed
                    //               ProcessStartInfo cmdStartInfo2 = new ProcessStartInfo();
                    //               cmdStartInfo2.FileName = Application.StartupPath + @"\cb_console_runner.exe";
                    //               cmdStartInfo2.Arguments = "\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe\"";
                    //cmdStartInfo2.RedirectStandardOutput = true;
                    //cmdStartInfo2.RedirectStandardError = true;
                    //cmdStartInfo2.RedirectStandardInput = true;
                    //cmdStartInfo2.UseShellExecute = false;
                    //cmdStartInfo2.CreateNoWindow = false;
                    //               processPSC2 = new Process();
                    //               processPSC2.StartInfo = cmdStartInfo2;
                    //processPSC2.ErrorDataReceived += cmd_Error;
                    //processPSC2.OutputDataReceived += cmd_DataReceived;
                    //               processPSC2.Start(); //"cscript.exe", "//Nologo " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs");
                    /*DoOnUIThread(delegate()
                    {
                        drBW2 = Overlay(this, ProgramIsRunning, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        if (drBW2 == System.Windows.Forms.DialogResult.OK)
                        {
                            try
                            {
                                //processPSC.Kill();
                                processPSC2.Kill();
                            }
                            catch { }
                        }
                    });*/
                    //processPSC2.BeginOutputReadLine();
                    //processPSC2.BeginErrorReadLine();
                    //processPSC2.StandardInput.WriteLine("run");
                    //cmdStartInfo2.RedirectStandardInput = false;
                    //                processPSC2.WaitForExit();
                }
                catch { }
            }
            try
            {
                DoOnUIThread(delegate()
                {
                    SendKeys.Send("{ESC}");
                });
            }
            catch { }
            /*{
               new Car() { File = "Subaru", Message = "Impreza", Line = 2005 },
               new Car() { File = "Ford", Message = "Mustang", Line = 1984 }
            };*/


            //old code when Visual BAsic Script Compiler was used
            /*ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = @"cscript.exe";
            cmdStartInfo.Arguments = "//Nologo " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs";
            cmdStartInfo.RedirectStandardOutput = true;
            cmdStartInfo.RedirectStandardError = true;
            cmdStartInfo.RedirectStandardInput = true;
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            processPSC = new Process();
            processPSC.StartInfo = cmdStartInfo;
            processPSC.ErrorDataReceived += cmd_Error;
            processPSC.OutputDataReceived += cmd_DataReceived;
            processPSC.Start(); //"cscript.exe", "//Nologo " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs");
            processPSC.BeginOutputReadLine();
            processPSC.BeginErrorReadLine();
            processPSC.WaitForExit();
            try
            {
                DoOnUIThread(delegate()
    {
        SendKeys.Send("{ESC}");
    });
            }
            catch { }*/
        }

        void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            pseudocodeEd.GoTo.Line(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value) - 1);
            pseudocodeEd.Focus();
        }
        static void cmd_DataReceived(object sender, DataReceivedEventArgs e)
        {
            try
            {
                MessageBox.Show(e.Data.ToString(), sender.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch { }
        }

        private void cmd_Error(object sender, DataReceivedEventArgs e)
        {
            //Debug.WriteLine("Error from other process");
            if (e.Data != "" & e.Data != null)
            {
                string message;
                string line;
                try
                {
                    line = e.Data.Split(':')[2];
                }
                catch
                {
                    line = "6";
                }
                if (line.StartsWith(" "))
                {
                    message = line;
                    line = "6";
                }
                else
                {
                    try
                    {
                        message = e.Data.Split(':')[5];
                    }
                    catch
                    {
                        message = e.Data;
                    }
                }
                int num = 0;
                try
                {
                    DoOnUIThread(delegate()
                    {
                        //string era = lang;
                        //lang = "CPP";
                        string tt;
                        tt = Compile(pseudocodeEd.Text, "CPP");
                        string[] text = tt.Split('\n');
                        int linei = 0;
                        try
                        {
                            linei = Convert.ToInt32(line);
                            for (int i = 0; i < Convert.ToInt32(line); i++)
                            {
                                //try
                               // {
                                    if (text[i].Contains("{") || text[i].Contains("else")) linei--;
                               // }
                                //catch { }
                            }
                        }
                        catch
                        {
                            
                        }
                        num = linei - 7;
                        //lang = era;
                    });
                    //int a = coresp[num];
                    //num = a - (int)nr_linii - 3;
                }
                catch
                {
                    num = 6;
                }
                if (num == -1) num = 0;
                //Debug.WriteLine(message);
                if (!message.StartsWith(" In function 'int main()'") && !message.StartsWith(" expected ';' before") && !message.StartsWith(" expected ')' before ';' token"))
                {
                    if (Properties.Settings.Default.UILang == 1)
                    {
                        message = message.Replace(" was not declared in this scope", " nu este recunoscut ca un nume al unei variabile declarate anterior.");
                        message = message.Replace("expected primary-expression before ')' token", "Sintaxa acestei instrucțiuni este incorectă. Se așteaptă o operație logică, dar nu s-a primit nimic.");
                        message = message.Replace("stray '\\' in program", "Caracterul '\\' este nerecunoscut.");
                    }
                    cars.Add(new Car() { Linie = num, Mesaj = message });
                }

                //try
                //{
                //old Visual Basic Script code, deprecated
                /*if (e.Data.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp"))
                {
                    string error;
                    error = e.Data;
                    string xstr = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs";
                    error = error.Replace(xstr, "");
                    error = Regex.Replace(error, " Microsoft VBScript ", "");
                    string[] parametrii;
                    parametrii = error.Split(')');
                    string[] parametrii2;
                    parametrii2 = Regex.Split(parametrii[0], ",");
                    parametrii2[0] = parametrii2[0].Substring(1, parametrii2[0].Length - 1);
                    parametrii2[1] = parametrii2[1].Substring(1, parametrii2[1].Length - 1);
                    string total = "";
                    for (int i = 1; i < parametrii.Length; i++)
                    {
                        total = total + parametrii[i];
                    }
                                    DoOnUIThread(delegate()
{
    SendKeys.Send("{ESC}");
    buttonItem5.Visible = true;
    buttonItem5.Text = superTabControl1.SelectedTab.Text + ": " + ErrorText1 + parametrii2[0] + ErrorText2 + parametrii2[1] + ": " + total;
    System.Media.SystemSounds.Hand.Play();
    buttonItem5.Tag = parametrii2[0] + "." + parametrii2[1] + "." + total;
    handluit = true;
    reidentTimer.Tag = "error";
    //CasetaDeMesaj(this, "test", AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
});
                }*/
                //}
                //catch
                //{ }
            }


        }

        private void buttonItem3_Click(object sender, EventArgs e)
        {
            contextMenuStrip3.Show(Cursor.Position);
        }

        private void goToErrorLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string[] st = statusBar1.Tag.ToString().Split('.');
                pseudocodeEd.GoTo.Line(Convert.ToInt32(st[0]) - 1);
                pseudocodeEd.Focus();
                statusBar1.Visible = false;
                handluit = true;
                reidentTimer.Tag = "";
            }
            catch
            {
                CasetaDeMesaj(this, NoStackTrace, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lookForErrorOnlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string[] st = buttonItem3.Tag.ToString().Split('.');
            System.Diagnostics.Process.Start("http://google.ro/search?q=" + statusBar1.Text.ToString());
        }

        private void borderSE_MouseDown_1(object sender, MouseEventArgs e)
        {
            Active = true;
        }

        private void borderSE_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                this.Size = new Size(this.Width + e.X, this.Height + e.Y);
                this.Refresh();
            }
        }

        private void borderSE_MouseUp_1(object sender, MouseEventArgs e)
        {
            Active = false;
        }
        int saveAnimation = 0;
        private void timer2_Tick(object sender, EventArgs e)
        {
            if (saveAnimation % 2 == 0)
            {
                buttonItem4.Visible = true;
            }
            else
            {
                buttonItem4.Visible = false;
            }
            saveAnimation += 1;
            if (saveAnimation == 4)
            {
                saveAnimation = 0;
                timer2.Enabled = false;
            }
            statusBar1.Refresh();
        }

        private void button57_Click(object sender, EventArgs e)
        {
            HelpPanel.Visible = false;
            HelpPanel.Tag = "hidden";

        }

        private void FallDownTimer_Tick(object sender, EventArgs e)
        {
            if (FallDownTimer.Tag == "up")
            {
                HelpPanel.Top -= 3;
                Application.DoEvents();
                this.Refresh();
                if (this.WindowState == FormWindowState.Normal)
                {
                    if (HelpPanel.Top < 21)
                    {
                        HelpPanel.Top = 21;
                        FallDownTimer.Tag = "";
                        FallDownTimer.Enabled = false;
                    }
                    /*if (HelpPanel.Top == 21)
                    {
                        FallDownTimer.Tag = "";
                        FallDownTimer.Enabled = false;
                    }*/
                }
                else
                {
                    if (HelpPanel.Top < 26)
                    {
                        HelpPanel.Top = 26;
                        FallDownTimer.Tag = "";
                        FallDownTimer.Enabled = false;
                    }
                    /*if (HelpPanel.Top == 26)
                    {
                        FallDownTimer.Tag = "";
                        FallDownTimer.Enabled = false;
                    }*/
                }
            }
            else
            {

                HelpPanel.Top += 30;
                Application.DoEvents();
                this.Refresh();
                if (HelpPanel.Top + HelpPanel.Height > this.Height)
                {
                    if (this.WindowState == FormWindowState.Normal)
                    {
                        FallDownTimer.Tag = "up";
                    }
                    else
                    {
                        FallDownTimer.Tag = "up";
                    }
                    // FallDownTimer.Enabled = false;
                }
            }
            /*if (this.WindowState == FormWindowState.Normal)
            {
                if (HelpPanel.Top > -11 && HelpPanel.Top < 21)
                {
                    HelpPanel.Top = 21;
                    FallDownTimer.Enabled = false;
                }
            }
            else
            {
                if (HelpPanel.Top > -6 && HelpPanel.Top < 26)
                {
                    HelpPanel.Top = 26;
                    FallDownTimer.Enabled = false;
                }
            }*/
        }

        private void RiseUpTimer_Tick(object sender, EventArgs e)
        {
            HelpPanel.Top -= 40;
            Application.DoEvents();
            this.Refresh();
            //if (this.WindowState == FormWindowState.Normal)
            //{
            if (HelpPanel.Top < -HelpPanel.Height)
            {
                HelpPanel.Visible = false;
                HelpPanel.Tag = "hidden";
                panel3.BringToFront();
                panel1.Height = 24;
                panel2.Height = 34;
                panel7.BringToFront();
                RiseUpTimer.Enabled = false;
            }
            /*}
            else
            {
                if (HelpPanel.Top > -5 && HelpPanel.Top < 26)
                {
                    HelpPanel.Top = 26;
                    FallDownTimer.Enabled = false;
                }
            }*/
        }
        public void ThirdPartyInfo()
        {
            try
            {
                SendKeys.Send("{F1}");
                Application.DoEvents();
                if (Properties.Settings.Default.UILang == 1)
                {
                    while (FallDownTimer.Enabled == true)
                    {
                        Application.DoEvents();
                    }
                    //HelpBrowser.Load(Application.StartupPath + "\\helpcontent\\ro-RO\\template-aplicatia-pseudocod-despre.htm");
                }
            }
            catch
            {
                CasetaDeMesaj(this.MdiParent, NoBrowserInstalled, "Pseudocod", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void button57_Click_1(object sender, EventArgs e)
        {
        }
        public bool Update(string a1, string a2, string a3, string a4)
        {
            Update up = new Update(a1, a2, a3, a4);
            up.Show();
            return true;
        }
        private void backgroundWorker3_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            try
            {
                wc.DownloadFile(Properties.Settings.Default.ServerPath + Properties.Settings.Default.UILang.ToString() + "/update.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\update.txt");

                System.IO.TextReader tw = new System.IO.StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\update.txt", Encoding.UTF8);
                string text = tw.ReadToEnd();
                tw.Close();
                string[] info = System.Text.RegularExpressions.Regex.Split(text, "\n");
                string text2 = "";
                for (int i = 3; i < info.Length; i++)
                {
                    text2 = text2 + info[i] + "\r\n";
                }
                if (Application.ProductVersion != info[0])
                {
                    DoOnUIThread(delegate()
                    {
                        Update(info[0], text2, info[2], info[1]);
                    });

                    //Update(info[0], text2, info[2], info[1]);
                }
            }
            catch { }
        }

        private void reidentTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (flagReident == true)
                {
                    int KeyWords = 0;
                    int line = pseudocodeEd.Lines.Current.Number;
                    int pos = pseudocodeEd.CurrentPos;
                    int add = 0;
                    bool en;
                    if (currentLineBeforeEnter + 1 == numberOfLinesBeforeEnter)
                        en = true;
                    else
                        en = false;
                    string[] spl = pseudocodeEd.Text.Split('\n');
                    for (int j = 0; j < spl.Length; j++)
                    {
                        spl[j] = spl[j].TrimStart();
                        for (int i = 0; i < KeyWords; i++)
                        {
                            spl[j] = "    " + spl[j];
                            if (j < line + 2) add += 4;


                        }
                        if (spl[j].TrimStart().StartsWith(IfKeyword) | spl[j].TrimStart().StartsWith(ForKeyword) | spl[j].TrimStart().StartsWith(DoKeyword) | spl[j].TrimStart().StartsWith(WhileKeyword)) KeyWords += 1;
                        if (spl[j].TrimStart().StartsWith(EndIfKeyword) | spl[j].TrimStart().StartsWith(EndForKeyword) | spl[j].TrimStart().StartsWith(LoopUntilKeyword) | spl[j].TrimStart().StartsWith(EndWhileKeyword))
                        {
                            KeyWords -= 1;
                            if (spl[j].StartsWith("    ")) spl[j] = spl[j].Substring(4, spl[j].Length - 4);
                        }
                        if (spl[j].TrimStart().StartsWith(ElseKeyword))
                        {
                            if (spl[j].StartsWith("    ")) spl[j] = spl[j].Substring(4, spl[j].Length - 4);
                        }

                    }
                    // Debug.WriteLine(currentLineBeforeEnter.ToString() + " " + numberOfLinesBeforeEnter.ToString());
                    string final = "";
                    foreach (string x in spl)
                    {
                        final += x + "\n";
                    }
                    final = final.Substring(0, final.Length - 1);
                    pseudocodeEd.Text = final;
                    if (flagEnterWasPressed == true)
                    {
                        if (en == true)
                        {
                            pseudocodeEd.GoTo.Line(line);
                            var poz = pseudocodeEd.NativeInterface.GetCurrentPos();
                            pseudocodeEd.GoTo.Position(poz + pseudocodeEd.Lines[line - 1].Length - 1);
                        }
                        else
                        {
                            pseudocodeEd.GoTo.Line(line - 1);
                            var poz = pseudocodeEd.NativeInterface.GetCurrentPos();
                            pseudocodeEd.GoTo.Position(poz + pseudocodeEd.Lines[line - 1].Length - 1);
                        }
                    }
                    else
                    {
                        try
                        {

                            pseudocodeEd.GoTo.Position((int)converterEd.Tag);
                        }
                        catch { }
                    }
                    flagReident = false;
                    flagEnterWasPressed = false;
                }
                converterEd.Tag = pseudocodeEd.CurrentPos;
            }
            catch { }

        }

        private void buttonItem5_Click(object sender, EventArgs e)
        {
            //contextMenuStrip4.Show(Cursor.Position);
        }

        private void goToErrorLineToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                string[] st = buttonItem5.Tag.ToString().Split('.');
                pseudocodeEd.GoTo.Line(Convert.ToInt32(st[0]) - 1);
                pseudocodeEd.Focus();
                buttonItem5.Visible = false;
                handluit = true;
                reidentTimer.Tag = "";
            }
            catch
            {
                CasetaDeMesaj(this, NoStackTrace, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lookForErrorOnlineToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string[] st = buttonItem5.Tag.ToString().Split('.');
            System.Diagnostics.Process.Start("http://google.ro/search?q=" + st[2] + "%20vbscript");
        }

        private void button54_Click_1(object sender, EventArgs e)
        {

        }

        private void rangeSelector_Tick(object sender, EventArgs e)
        {
            rg.Select();
            rangeSelector.Enabled = false;
        }
        public void ShowCodeboard()
        {
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
            {
                MessageBoxI msg = new MessageBoxI(this.MdiParent, "", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                msg.Show();
                msg.panel1.Visible = false;
                Welcome wel = new Welcome(this, true, false);
                wel.ShowDialog();
                msg.Close();
                SetForegroundWindow(wel.Handle);
            }
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
            {
                MessageBoxI msg = new MessageBoxI(this.MdiParent, "", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                msg.Show();
                msg.panel1.Visible = false;
                Welcome wel = new Welcome(this, false, false);
                wel.ShowDialog();
                msg.Close();
                SetForegroundWindow(wel.Handle);
            }
        }
        private void ShowCodeboardTimer_Tick(object sender, EventArgs e)
        {
            ShowCodeboardTimer.Enabled = false;

        }

        private void button57_Click_2(object sender, EventArgs e)
        {
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat"))
            {
                panel12.Visible = false;
                button48.BackColor = Color.White;
                label31.BackColor = Color.White;
                MessageBoxI msg = new MessageBoxI(this, "", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                msg.Show();
                msg.panel1.Visible = false;
                Welcome wl = new Welcome(this, true, false);
                wl.ShowDialog();
                msg.Close();
                SetForegroundWindow(this.Handle);
            }
            else
            {
                panel12.Visible = false;
                button48.BackColor = Color.White;
                label31.BackColor = Color.White;
                MessageBoxI msg = new MessageBoxI(this, "", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                msg.Show();
                msg.panel1.Visible = false;
                Welcome wl = new Welcome(this, false, false);
                wl.ShowDialog();
                msg.Close();
                SetForegroundWindow(this.Handle);
            }
        }

        private void button58_Click(object sender, EventArgs e)
        {
            int a = 0;
            MessageBox.Show((1 / a).ToString());

            /*SchemaLogica.Visible = true;
            SchemaLogica.BringToFront();
            //SchemaLogica.Paint += SchemaLogica_Paint;
            SchemaLogica.Width = converterEd.Width;
            SchemaLogica.Height = converterEd.Height;
            SchemaLogica.Left = converterEd.Left;
            SchemaLogica.Top = converterEd.Top;
            SchemaLogica.BackColor = Color.White;*/
            //CreateMap();

        }

        void SchemaLogica_Paint(object sender, PaintEventArgs e)
        {

        }

        /*int s, M = 10, D = 60,i;

        void altfelDist(int j)
        {
            if (j < textPseudocod.Length)
            {
                if (textPseudocod[j].TrimStart().StartsWith("daca")) tataDist(j+1);
                else
                    if (textPseudocod[j].TrimStart().StartsWith("altfel")) s += M + D;

                i++; altfelDist(j + 1);
            }

        }

        void tataDist(int j)   //By fll0pY cu Va|0ArE
        {
            if (j<textPseudocod.Length)
            {
                if (!textPseudocod[j].TrimStart().StartsWith("altfel")) altfelDist(j);
                else            
                if (textPseudocod[j].TrimStart().StartsWith("daca")) s += M + D;
                 i++; tataDist(j + 1);
            }
        }*/
        string[] textPseudocod;
        int Nr1;
        int Nr2;
        /*void HartaDaca(int j)
        {
            Nr1 = 1;
            Nr2 = 1;
            bool prima = false;
            int spatii = GetLeadingWhitespaceAsInt(textPseudocod[j]);
            /*if (spatii != 0)
            {
                Nr1 = 1;
                Nr2 = 1;
            }
            int lastspacing = 0;
            for (int i = j + 1; i < textPseudocod.Length; i++)
            {
                if (textPseudocod[i].TrimStart().StartsWith("daca"))
                {
                    if (lastspacing < GetLeadingWhitespaceAsInt(textPseudocod[i]))
                    {
                        if (prima == false) Nr1++;
                        else Nr2++;
                        lastspacing = GetLeadingWhitespaceAsInt(textPseudocod[i]);
                    }
                }
                if (textPseudocod[i].TrimStart().StartsWith("altfel") && GetLeadingWhitespaceAsInt(textPseudocod[i]) == spatii)
                {
                    lastspacing = GetLeadingWhitespaceAsInt(textPseudocod[i]);
                    prima = true;
                }
                if (textPseudocod[i].TrimStart().StartsWith("sfarsit_daca") && GetLeadingWhitespaceAsInt(textPseudocod[i]) == spatii)
                {
                    return;
                }
            }
        }*/

        /* short[,] Map = new short[10, 10];
         int xMap = 1, yMap = 1, xTeava = 1, yTeava = 1;
         public struct altfel{
             public int x,y;
         };
         altfel[] Altfel = new altfel[10000];
         int NumarAltfel = 0;
         void TranspunereMatrice(int Inceput)
         {
             for (int i = xMap; i >= Inceput; i--)
                 for (int j = yMap; j >= 1; j--)
                 {
                     Map[i + 1, j] = Map[i, j];
                 }
             xMap++;
             for (int i = 1; i <= yMap; i++) Map[Inceput, i] = 0;
         }
         */
        int CPPLineNumberToPseudocodeLineNumber(int cpp_ln)
        {
            cpp_ln = cpp_ln - 6 - (int)nr_linii;
            int i = 0;
            for (i = 1; i <= pseudocodeEd.Lines.Count; i++)
            {
                if (coresp[i] == cpp_ln) break;
            }
            return i;
        }
        public void CreateMap()
        {
            Bitmap bm = new Bitmap(SchemaLogicaCont.Width, SchemaLogicaCont.Height);
            using (Graphics g = Graphics.FromImage(bm))
            {
                g.Clear(Color.White);
                Point Curent = new Point();
                string[] ForVar = new string[1000];
                bool[] wasElse = new bool[1000];
                Point[] LastIf = new Point[1000];
                Point[] LastElse = new Point[1000];
                Point[] LastFor = new Point[1000];
                Point[] LastWhile = new Point[1000];
                Point[] LastDo = new Point[1000];
                int lastifnum = 0;
                int lastelsenum = 0;
                int lastfornum = 0;
                int lastwhilenum = 0;
                int lastdonum = 0;
                string[] textPseudocod = Regex.Split(pseudocodeEd.Text, "\n");
                int RombDim = 110;
                int LineDim = 10;
                int Elongatie = 0;
                g.DrawEllipse(Pens.Black, 40, 10, 70, 30);
                g.DrawString("START", new Font("Consolas", 10 * (96 / (float)dx)), Brushes.Black, 55, 18);
                g.DrawLine(Pens.Black, 75, 40, 75, 50);
                Elongatie = 100;
                Curent = new Point(75, 50);
                SolidBrush LineHighlightColour = new SolidBrush(Color.FromArgb(254, 128, 62));
                for (int i = 0; i < textPseudocod.Length; i++)
                {
                    //if (textPseudocod[i].ToString().TrimStart().StartsWith("scrie"))
                    //{
                       /* Point[] points = { new Point(Curent.X - RombDim / 2 + 10, Curent.Y), new Point(Curent.X + RombDim / 2, Curent.Y), new Point(Curent.X + RombDim / 2 - 10, Curent.Y + LineDim * 2 + 5), new Point(Curent.X - RombDim / 2, Curent.Y + LineDim * 2 + 5) };
                        if (i == pseudocodeEd.Lines.Current.Number) g.FillPolygon(LineHighlightColour, points);
                        else g.DrawPolygon(Pens.Black, points);
                        g.DrawLine(Pens.Black, new Point(Curent.X, Curent.Y + LineDim * 2 + 5), new Point(Curent.X, Curent.Y + LineDim * 2 + 5 + LineDim));
                        if (textPseudocod[i].ToString().TrimStart().Replace("scrie ", "").Length >= 10)
                        {
                            g.DrawString(textPseudocod[i].ToString().TrimStart().Replace("scrie ", "").Substring(0, 8), new Font("Consolas", 8), Brushes.Black, Curent.X - CountChars(textPseudocod[i].ToString().TrimStart().Replace("scrie ", "").Substring(0, 8), true) * 3 - 3, Curent.Y + LineDim - 3);
                            g.DrawString("...", new Font("Arial Narrow", 10), Brushes.Black, Curent.X + 20, Curent.Y + LineDim - 6);
                        }
                        else g.DrawString(textPseudocod[i].ToString().TrimStart().Replace("scrie ", ""), new Font("Consolas", 8), Brushes.Black, Curent.X - CountChars(textPseudocod[i].ToString().TrimStart().Replace("scrie ", ""), true) * 3 + 2, Curent.Y + LineDim - 3);
                        g.DrawLine(Pens.Black, new Point(Curent.X + RombDim / 2 - 5, Curent.Y + LineDim + 2), new Point(Curent.X + RombDim / 2 - 5 + LineDim, Curent.Y + LineDim + 2));
                        Point[] triangle = { new Point(Curent.X + RombDim / 2 - 5 + LineDim, Curent.Y + LineDim + 2 - 5), new Point(Curent.X + RombDim / 2 + LineDim, Curent.Y + LineDim + 2), new Point(Curent.X + RombDim / 2 - 5 + LineDim, Curent.Y + LineDim + 2 + 5), new Point(Curent.X + RombDim / 2 - 5 + LineDim, Curent.Y + LineDim + 2 - 5) };
                        g.FillPolygon(Brushes.Black, triangle);
                        Curent = new Point(Curent.X, Curent.Y + LineDim * 2 + 5 + LineDim);
                        if (Elongatie < Curent.X + RombDim / 2 + LineDim) Elongatie = Curent.X + RombDim / 2 + LineDim;*/
                   // }
                   // else if (textPseudocod[i].ToString().TrimStart().StartsWith("citeste"))
                   // {
                       /* Point[] points = { new Point(Curent.X - RombDim / 2 + 10, Curent.Y), new Point(Curent.X + RombDim / 2, Curent.Y), new Point(Curent.X + RombDim / 2 - 10, Curent.Y + LineDim * 2 + 5), new Point(Curent.X - RombDim / 2, Curent.Y + LineDim * 2 + 5) };
                        if (i == pseudocodeEd.Lines.Current.Number) g.FillPolygon(LineHighlightColour, points);
                        else g.DrawPolygon(Pens.Black, points);
                        g.DrawLine(Pens.Black, new Point(Curent.X, Curent.Y + LineDim * 2 + 5), new Point(Curent.X, Curent.Y + LineDim * 2 + 5 + LineDim));
                        if (textPseudocod[i].ToString().TrimStart().Replace("citeste ", "").Length >= 10)
                        {
                            g.DrawString(textPseudocod[i].ToString().TrimStart().Replace("citeste ", "").Substring(0, 8), new Font("Consolas", 8), Brushes.Black, Curent.X - CountChars(textPseudocod[i].ToString().TrimStart().Replace("citeste ", "").Substring(0, 8), true) * 3 - 3, Curent.Y + LineDim - 3);
                            g.DrawString("...", new Font("Arial Narrow", 10), Brushes.Black, Curent.X + 20, Curent.Y + LineDim - 6);
                        }
                        else g.DrawString(textPseudocod[i].ToString().TrimStart().Replace("citeste ", ""), new Font("Consolas", 8), Brushes.Black, Curent.X - CountChars(textPseudocod[i].ToString().TrimStart().Replace("citeste ", ""), true) * 3 + 2, Curent.Y + LineDim - 3);
                        g.DrawLine(Pens.Black, new Point(Curent.X + RombDim / 2 - 5, Curent.Y + LineDim + 2), new Point(Curent.X + RombDim / 2 - 5 + LineDim + 3, Curent.Y + LineDim + 2));
                        Point[] triangle = { new Point(Curent.X + RombDim / 2 - 5, Curent.Y + LineDim + 2), new Point(Curent.X + RombDim / 2, Curent.Y + LineDim + 2 - 5), new Point(Curent.X + RombDim / 2, Curent.Y + LineDim + 2 + 5), new Point(Curent.X + RombDim / 2 - 5, Curent.Y + LineDim + 2) };
                        g.FillPolygon(Brushes.Black, triangle);
                        Curent = new Point(Curent.X, Curent.Y + LineDim * 2 + 5 + LineDim);
                        if (Elongatie < Curent.X + RombDim / 2 - 5 + LineDim + 3) Elongatie = Curent.X + RombDim / 2 - 5 + LineDim + 3;*/
                  //  } else
                     if (textPseudocod[i].ToString().TrimStart().StartsWith(IfKeyword))
                    {
                        int a = 0;
                        string b = "";
                        string c = "";
                        Pen pn = Pens.Black;
                        Pen pn2 = Pens.Black;
                        Pen pn3 = Pens.Black;
                        Brush bn = Brushes.Black;
                        Brush bn2 = Brushes.Black;
                        Brush bn3 = Brushes.Black;
                        if (lista_executii != null && lista_executii.Count != 0)
                        {
                            try
                            {
                                a = corespA[i] - 1;
                                int count1 = 1;
                                int count2 = 3;
                            inceput:
                                b = lista_executii[a + (int)nr_linii + 6 + count1][0];
                                c = lista_executii[a + (int)nr_linii + 6 + count2][0];
                                if (b == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                    pn2 = Pens.LightGray;
                                    bn2 = Brushes.LightGray;
                                    pn3 = Pens.LightGray;
                                    bn3 = Brushes.LightGray;
                                }
                                else if (b == "-")
                                {
                                    count1++;
                                    goto inceput;
                                }
                                if (c == "#####")
                                {
                                    pn2 = Pens.LightGray;
                                    bn2 = Brushes.LightGray;
                                }
                                else if (c == "-")
                                {
                                    count2++;
                                    goto inceput;
                                }
                            }
                            catch { }
                        }
                         if (lista_executii == null)
                         {
                             pn = Pens.Black;
                             pn2 = Pens.Black;
                             bn = Brushes.Black;
                             bn2 = Brushes.Black;
                         }
                         int nrord = 0;
                         bool ok = false;
                         if (lista_executii != null && lista_executii.Count != 0)
                         {
                             try
                             {
                                 for (int j = i + 1; j < textPseudocod.Length; j++)
                                 {
                                     if (textPseudocod[j].TrimStart().StartsWith(EndIfKeyword))
                                     {
                                         if (nrord == 0) break;
                                         nrord--;
                                     }
                                     if (textPseudocod[j].TrimStart().StartsWith(IfKeyword))
                                     {
                                         nrord++;
                                     }
                                     if (textPseudocod[j].TrimStart().StartsWith(ElseKeyword) && nrord == 0)
                                     {
                                         ok = true;
                                         int count1 = 2;
                                     inceput1:
                                         string d = lista_executii[corespA[j] + (int)nr_linii + 6 + count1][0];
                                         if (d == "#####")
                                         {
                                             pn3 = Pens.LightGray;
                                             bn3 = Brushes.LightGray;
                                             ok = true;
                                             break;
                                         }
                                         else if (d == "-")
                                         {
                                             count1++;
                                             goto inceput1;
                                         }
                                     }
                                 }
                             }
                             catch { }
                         }
                         try
                         {
                             if (pn3 == Pens.Black && bn3 == Brushes.Black && pn2 == Pens.Black && bn2 == Brushes.Black && lista_executii != null && ok == false)
                             {
                                 pn3 = Pens.LightGray;
                                 bn3 = Brushes.LightGray;
                             }
                         }
                         catch { }
                             lastifnum++;
                        LastIf[lastifnum] = Curent;
                        wasElse[lastifnum] = true;
                        textPseudocod[i] = textPseudocod[i].Replace(" " + ThenKeyword, "");
                        Point[] points = { Curent, new Point(Curent.X + RombDim / 2, Curent.Y + RombDim / 2), new Point(Curent.X, Curent.Y + RombDim), new Point(Curent.X - RombDim / 2, Curent.Y + RombDim / 2), Curent };
                        if (i == pseudocodeEd.Lines.Current.Number) g.FillPolygon(LineHighlightColour, points);
                        else g.DrawPolygon(pn, points);
                        g.DrawLine(pn2, new Point(Curent.X, Curent.Y + RombDim), new Point(Curent.X, Curent.Y + RombDim + LineDim));
                        g.DrawLine(pn3, new Point(Curent.X + RombDim / 2, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2));
                        /*else
                        {
                            if (pn == Pens.LightGray) g.DrawLine(lista_executii == null ? Pens.Black : Pens.LightGray, new Point(Curent.X + RombDim / 2, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2));
                            else g.DrawLine(Pens.Black, new Point(Curent.X + RombDim / 2, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2));
                        }*/
                        if (textPseudocod[i].ToString().TrimStart().Replace(IfKeyword + " ", "").Length >= 17)
                        {
                            g.DrawString(textPseudocod[i].ToString().TrimStart().Replace(IfKeyword + " ", "").Substring(0, 15), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textPseudocod[i].ToString().TrimStart().Replace(IfKeyword + " ", "").Substring(0, 15), true) * 3 - 3, Curent.Y + RombDim / 2 - 7);
                            g.DrawString("...", new Font("Arial Narrow", 10 * (96 / (float)dx)), bn, Curent.X + 40, Curent.Y + RombDim / 2 - 10);
                        }
                        else g.DrawString(textPseudocod[i].ToString().TrimStart().Replace(IfKeyword + " ", ""), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textPseudocod[i].ToString().TrimStart().Replace(IfKeyword + " ", ""), true) * 3 + 2, Curent.Y + RombDim / 2 - 7);
                        g.DrawString(YES, new Font("Arial Narrow", 8 * (96 / (float)dx)), bn2, Curent.X - LineDim * 2 + 3, Curent.Y + RombDim - 3);
                        g.DrawString(NO, new Font("Arial Narrow", 8 * (96 / (float)dx)), bn3, Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2 - LineDim - 3);
                       /* else
                        {
                            if (bn == Brushes.LightGray) g.DrawString(NO, new Font("Arial Narrow", 8 * (96 / (float)dx)), lista_executii == null || ok == true? Brushes.Black : Brushes.LightGray, Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2 - LineDim - 3);
                            else g.DrawString(NO, new Font("Arial Narrow", 8 * (96 / (float)dx)), Brushes.Black, Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2 - LineDim - 3);
                        }*/
                      //  g.DrawString(NO, new Font("Arial Narrow", 8 * (96 / (float)dx)), Brushes.Black, Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2 - LineDim - 3);
                        Curent = new Point(Curent.X, Curent.Y + RombDim + LineDim);
                        if (Elongatie < Curent.X + RombDim / 2 + LineDim) Elongatie = Curent.X + RombDim / 2 + LineDim;
                    }
                    else if (textPseudocod[i].ToString().TrimStart().StartsWith(ElseKeyword))
                    {
                        int a = 0;
                        string b = "";
                        string c = "";
                        Pen pn = Pens.Black;
                        Pen pn2 = Pens.Black;
                        int j = 0;
                        int nrord = 1;
                        if (lista_executii != null && lista_executii.Count != 0)
                        {
                            for (j = i - 1; j >= 0; j--)
                            {
                                if (nrord == 0) break;
                                if (textPseudocod[j].TrimStart().StartsWith(EndIfKeyword)) nrord++;
                                if (textPseudocod[j].TrimStart().StartsWith(IfKeyword))
                                {
                                    nrord--;
                                }
                            }
                            j++;
                            try
                            {
                                a = corespA[i];
                                //   b = lista_executii[a + (int)nr_linii + 6 + 2][0];
                                //   c = lista_executii[corespA[j] + (int)nr_linii + 6][0];
                                //   if (b == "#####") pn = Pens.LightGray;
                                //   if (c == "#####") pn2 = Pens.LightGray;

                                int count1 = 2;
                                int count2 = 6;
                            inceput:
                                b = lista_executii[a + (int)nr_linii + 6 + count1][0];
                                c = lista_executii[corespA[j] + (int)nr_linii + count2][0];
                                if (b == "#####")
                                {
                                    pn = Pens.LightGray;
                                }
                                else if (b == "-")
                                {
                                    count1++;
                                    goto inceput;
                                }
                                if (c == "#####")
                                {
                                    pn2 = Pens.LightGray;
                                }
                                else if (c == "-")
                                {
                                    count2++;
                                    goto inceput;
                                }

                            }
                            catch { }
                        }
                        if (lista_executii == null)
                        {
                            pn = Pens.Black;
                            pn2 = Pens.Black;
                        }
                        lastelsenum++;
                        LastElse[lastelsenum] = Curent;
                        Curent = LastIf[lastifnum];
                        wasElse[lastifnum] = false;
                        if (pn2 == Pens.LightGray)
                        {
                            g.DrawLine(pn2, new Point(Curent.X + RombDim / 2 + 10, Curent.Y + RombDim / 2), new Point(Elongatie + RombDim / 2, Curent.Y + RombDim / 2));
                            g.DrawLine(pn2, new Point(Elongatie + RombDim / 2, Curent.Y + RombDim / 2), new Point(Elongatie + RombDim / 2, Curent.Y + RombDim + 10));
                        }
                        else
                        {
                            g.DrawLine(pn, new Point(Curent.X + RombDim / 2 + 10, Curent.Y + RombDim / 2), new Point(Elongatie + RombDim / 2, Curent.Y + RombDim / 2));
                            g.DrawLine(pn, new Point(Elongatie + RombDim / 2, Curent.Y + RombDim / 2), new Point(Elongatie + RombDim / 2, Curent.Y + RombDim + 10));
                        }
                        Curent = new Point(Elongatie + RombDim / 2, Curent.Y + RombDim + 10);
                    }
                    else if (textPseudocod[i].ToString().TrimStart().StartsWith(EndIfKeyword))
                    {
                        if (wasElse[lastifnum] == false)
                        {
                            int a = 0;
                            string b = "";
                            int c = 0;
                            Pen pn = Pens.Black;
                            Pen pn2 = Pens.Black;
                            Pen pn3 = Pens.Black;
                            if (lista_executii != null && lista_executii.Count != 0)
                            {
                                try
                                {
                                    a = corespA[i] - 1;
                                    //b = lista_executii[a + (int)nr_linii + 6 + 1][0];
                                    c = Convert.ToInt32(lista_executii[a + (int)nr_linii + 6 + 1][1]);
                                    //if (b == "#####") pn = Pens.LightGray;

                                    int count1 = 1;
                                inceput:
                                    b = lista_executii[a + (int)nr_linii + 6 + count1][0];
                                    if (b == "#####")
                                    {
                                        pn = Pens.LightGray;
                                    }
                                    else if (b == "-")
                                    {
                                        count1++;
                                        goto inceput;
                                    }
                                }
                                catch { }
                                int j = 0;
                                int nrord = 1;
                                for (j = i - 1; j >= 0; j--)
                                {
                                    if (nrord == 0) break;
                                    if (textPseudocod[j].TrimStart().StartsWith(EndIfKeyword)) nrord++;
                                    if (textPseudocod[j].TrimStart().StartsWith(IfKeyword))
                                    {
                                        nrord--;
                                    }
                                }
                                j++;
                                try
                                {
                                    int count1 = 6;
                                    int count2 = 8;
                                inceput1:
                                    string cx = lista_executii[corespA[j] + (int)nr_linii + count1][0];
                                    if (cx == "#####") pn = pn2 = Pens.LightGray;
                                    else if (cx == "-")
                                    {
                                        count1++;
                                        goto inceput1;
                                    }
                                inceput2:
                                    string cx2 = lista_executii[corespA[j] + (int)nr_linii + count2][0];
                                    if (cx2 == "#####") pn3 = Pens.LightGray;
                                    else if (cx == "-")
                                    {
                                        count2++;
                                        goto inceput2;
                                    }

                                }
                                catch { }
                            }
                            if (lista_executii == null)
                            {
                                pn = Pens.Black;
                                pn2 = Pens.Black;
                            }
                            if (Curent.X > LastElse[lastelsenum].X && Curent.Y > LastElse[lastelsenum].Y)
                            {
                                g.DrawLine(pn, Curent, new Point(LastElse[lastelsenum].X, Curent.Y));
                                g.DrawLine(pn3, new Point(LastElse[lastelsenum].X, Curent.Y), LastElse[lastelsenum]);
                                g.DrawLine(pn2, new Point(LastElse[lastelsenum].X, Curent.Y), new Point(LastElse[lastelsenum].X, Curent.Y + LineDim));
                                Curent = new Point(LastElse[lastelsenum].X, Curent.Y + LineDim);
                            }
                            else
                            {
                                g.DrawLine(pn, Curent, new Point(Curent.X, LastElse[lastelsenum].Y));
                                g.DrawLine(pn, LastElse[lastelsenum], new Point(Curent.X, LastElse[lastelsenum].Y));
                                g.DrawLine(pn2, LastElse[lastelsenum], new Point(LastElse[lastelsenum].X, LastElse[lastelsenum].Y + LineDim));
                                Curent = new Point(LastElse[lastelsenum].X, LastElse[lastelsenum].Y + LineDim);
                            }
                            lastelsenum--;
                            lastifnum--;
                        }
                        else
                        {
                            Point oldCurent = Curent;
                            Curent = LastIf[lastifnum];
                            wasElse[lastifnum] = false;
                            int a = 0;
                            string b = "";
                            string c = "";
                            Pen pn = Pens.Black;
                            Pen pn2 = Pens.Black;
                            int j = 0;
                            int nrord = 1;
                            if (lista_executii != null && lista_executii.Count != 0)
                            {
                                for (j = i - 1; j >= 0; j--)
                                {
                                    if (nrord == 0) break;
                                    if (textPseudocod[j].TrimStart().StartsWith(EndIfKeyword)) nrord++;
                                    if (textPseudocod[j].TrimStart().StartsWith(IfKeyword))
                                    {
                                        nrord--;
                                    }
                                }
                                j++;
                                try
                                {
                                    j = corespA[j];
                                    int count1 = 6;
                                    int count2 = 2;
                                inceput:
                                    b = lista_executii[j + (int)nr_linii + count1][0];
                                    c = lista_executii[j + (int)nr_linii + 6 + count2][0];
                                    if (b == "#####") pn = Pens.LightGray;
                                    else if (b == "-") { count1++; goto inceput; }
                                    if (c == "#####") pn2 = Pens.LightGray;
                                    else if (c == "-") { count2++; goto inceput; }
                                }
                                catch { }
                            }
                            if (lista_executii == null)
                            {
                                pn = Pens.Black;
                                pn2 = Pens.Black;
                            }
                            if (pn == Pens.LightGray)
                            {
                                g.DrawLine(pn, new Point(Curent.X + RombDim / 2 + 10, Curent.Y + RombDim / 2), new Point(Elongatie + RombDim / 2, Curent.Y + RombDim / 2));
                                g.DrawLine(pn, new Point(Elongatie + RombDim / 2, Curent.Y + RombDim / 2), new Point(Elongatie + RombDim / 2, oldCurent.Y));
                                g.DrawLine(pn, oldCurent, new Point(Elongatie + RombDim / 2, oldCurent.Y));
                                g.DrawLine(pn, oldCurent, new Point(oldCurent.X, oldCurent.Y + 10));
                            }
                            else
                            {
                                if (pn2 == Pens.LightGray)
                                {
                                    g.DrawLine(Pens.Black, new Point(Curent.X + RombDim / 2 + 10, Curent.Y + RombDim / 2), new Point(Elongatie + RombDim / 2, Curent.Y + RombDim / 2));
                                    g.DrawLine(Pens.Black, new Point(Elongatie + RombDim / 2, Curent.Y + RombDim / 2), new Point(Elongatie + RombDim / 2, oldCurent.Y));
                                    g.DrawLine(Pens.Black , oldCurent, new Point(Elongatie + RombDim / 2, oldCurent.Y));
                                   // g.DrawLine(Pens.Black, oldCurent, new Point(oldCurent.X, oldCurent.Y + 10));
                                }
                                else
                                {
                                    g.DrawLine(lista_executii == null ? Pens.Black : Pens.LightGray, new Point(Curent.X + RombDim / 2 + 10, Curent.Y + RombDim / 2), new Point(Elongatie + RombDim / 2, Curent.Y + RombDim / 2));
                                    g.DrawLine(lista_executii == null ? Pens.Black : Pens.LightGray, new Point(Elongatie + RombDim / 2, Curent.Y + RombDim / 2), new Point(Elongatie + RombDim / 2, oldCurent.Y));
                                    g.DrawLine(lista_executii == null ? Pens.Black : Pens.LightGray, oldCurent, new Point(Elongatie + RombDim / 2, oldCurent.Y));
                                   // g.DrawLine(Pens.LightGray, oldCurent, new Point(oldCurent.X, oldCurent.Y + 10));
                                }
                                g.DrawLine(Pens.Black, oldCurent, new Point(oldCurent.X, oldCurent.Y + 10));

                            }
                            Curent = new Point(oldCurent.X, oldCurent.Y + 10);
                            if (Elongatie < Elongatie + RombDim / 2) Elongatie = Elongatie + RombDim / 2 + LineDim;
                            lastifnum--;
                        }
                        if (lastifnum == 0 && GetLeadingWhitespaceAsInt(textPseudocod[i]) == 0) Elongatie = Curent.X;
                    }
                    else if (textPseudocod[i].ToString().TrimStart().StartsWith(ForKeyword))
                    {
                        int a = 0;
                        string b = "";
                        string c = "";
                        Pen pn = Pens.Black;
                        Brush bn = Brushes.Black;
                        Pen pn2 = Pens.Black;
                        Brush bn2 = Brushes.Black;
                        if (lista_executii != null && lista_executii.Count != 0)
                        {
                            try
                            {
                                a = corespA[i] - 1;
                                int count1 = 1;
                                int count2 = 3;
                            inceput:
                                b = lista_executii[a + (int)nr_linii + 6 + count1][0];
                                c = lista_executii[a + (int)nr_linii + 6 + count2][0];
                                if (b == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                }
                                else if (b == "-")
                                {
                                    count1++;
                                    goto inceput;
                                }
                                if (c == "#####")
                                {
                                    pn2 = Pens.LightGray;
                                    bn2 = Brushes.LightGray;
                                }
                                else if (c == "-")
                                {
                                    count2++;
                                    goto inceput;
                                }
                            }
                            catch { }
                        }
                        //COD NOU
                      /*  int j = 0;
                        int nrord = 1;
                        for (j = i - 1; j >= 0; j--)
                        {
                            if (nrord == 0) break;
                            if (textPseudocod[j].TrimStart().StartsWith(EndIfKeyword)) nrord++;
                            if (textPseudocod[j].TrimStart().StartsWith(IfKeyword))
                            {
                                nrord--;
                            }
                        }
                        j++;
                        try
                        {
                            int count1 = 6;
                            inceput1:
                            string cx = lista_executii[corespA[j] + (int)nr_linii + count1][0];
                            if (cx == "#####")
                            {
                                pn3 = Pens.LightGray;
                                bn3 = Brushes.LightGray;
                            }
                            else if (cx == "-") { count1++; goto inceput1; }

                        }
                        catch { }
                        if */
                        //SF COD NOU
                        if (lista_executii == null)
                        {
                            pn = Pens.Black;
                            pn2 = Pens.Black;
                            bn = Brushes.Black;
                            bn2 = Brushes.Black;
                        }
                        lastfornum++;
                        LastFor[lastfornum] = Curent;
                        textPseudocod[i] = textPseudocod[i].Replace(" " + DoKeyword, "");
                        string vali = "";
                        string valf = "";
                        try
                        {
                            if (PseudocodeLang == 0)
                            {
                                ForVar[lastfornum] = textPseudocod[i].TrimStart().Substring(4, textPseudocod[i].TrimStart().Length - 4).Split('=')[0].Split(' ')[0];
                                vali = textPseudocod[i].TrimStart().Substring(4, textPseudocod[i].TrimStart().Length - 4).Split('=')[1].Split(',')[0];
                                valf = textPseudocod[i].TrimStart().Substring(4, textPseudocod[i].TrimStart().Length - 4).Split('=')[1].Split(',')[1].Split(' ')[1];
                            }
                            if (PseudocodeLang == 1)
                            {
                                ForVar[lastfornum] = textPseudocod[i].TrimStart().Substring(7, textPseudocod[i].TrimStart().Length - 7).Split('=')[0].Split(' ')[0];
                                vali = textPseudocod[i].TrimStart().Substring(7, textPseudocod[i].TrimStart().Length - 7).Split('=')[1].Split(',')[0];
                                valf = textPseudocod[i].TrimStart().Substring(7, textPseudocod[i].TrimStart().Length - 7).Split('=')[1].Split(',')[1].Split(' ')[1];
                            }
                            if (PseudocodeLang == 2)
                            {
                                ForVar[lastfornum] = textPseudocod[i].TrimStart().Substring(ForKeyword.Length, textPseudocod[i].TrimStart().Length - ForKeyword.Length).Split('=')[0].Split(' ')[0];
                                vali = textPseudocod[i].TrimStart().Substring(ForKeyword.Length, textPseudocod[i].TrimStart().Length - ForKeyword.Length).Split('=')[1].Split(',')[0];
                                valf = textPseudocod[i].TrimStart().Substring(ForKeyword.Length, textPseudocod[i].TrimStart().Length - ForKeyword.Length).Split('=')[1].Split(',')[1].Split(' ')[1];
                            }
                        }
                        catch
                        {
                            ForVar[lastfornum] = "...";
                            vali = "...";
                            valf = "...";
                        }
                        if (i == pseudocodeEd.Lines.Current.Number) g.FillRectangle(LineHighlightColour, Curent.X - RombDim / 2, Curent.Y, RombDim, LineDim * 2);
                        else g.DrawRectangle(pn, Curent.X - RombDim / 2, Curent.Y, RombDim, LineDim * 2);
                        string textrect = ForVar[lastfornum] + " =" + vali;
                        if (textrect.Length >= 17)
                        {
                            g.DrawString(textrect.Substring(0, 15), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textrect.Substring(0, 15), true) * 3 - 3, Curent.Y + 5);
                            g.DrawString("...", new Font("Arial Narrow", 10 * (96 / (float)dx)), bn, Curent.X + 40, Curent.Y + RombDim / 2 - 10);
                        }
                        else g.DrawString(textrect, new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textrect, true) * 3 + 2, Curent.Y + 4);
                        g.DrawLine(pn, new Point(Curent.X, Curent.Y + LineDim * 2), new Point(Curent.X, Curent.Y + LineDim * 3));
                        Curent = new Point(Curent.X, Curent.Y + LineDim * 3);
                        Point[] points = { Curent, new Point(Curent.X + RombDim / 2, Curent.Y + RombDim / 2), new Point(Curent.X, Curent.Y + RombDim), new Point(Curent.X - RombDim / 2, Curent.Y + RombDim / 2), Curent };
                        if (i == pseudocodeEd.Lines.Current.Number) g.FillPolygon(LineHighlightColour, points);
                        else g.DrawPolygon(pn, points);
                        g.DrawLine(pn, new Point(Curent.X, Curent.Y + RombDim), new Point(Curent.X, Curent.Y + RombDim + LineDim));
                        g.DrawLine(pn2, new Point(Curent.X + RombDim / 2, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2));
                        string textromb = ForVar[lastfornum] + " <= " + valf;
                        if (textromb.Length >= 17)
                        {
                            g.DrawString(textromb.Substring(0, 15), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textromb.Substring(0, 15), true) * 3 - 3, Curent.Y + RombDim / 2 - 7);
                            g.DrawString("...", new Font("Arial Narrow", 10 * (96 / (float)dx)), bn, Curent.X + 40, Curent.Y + RombDim / 2 - 10);
                        }
                        else g.DrawString(textromb, new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textromb, true) * 3 + 2, Curent.Y + RombDim / 2 - 7);
                        g.DrawLine(pn2, new Point(Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim + RombDim, Curent.Y + RombDim / 2));
                        g.DrawLine(pn2, new Point(Curent.X + RombDim / 2 + LineDim + RombDim, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim + RombDim, Curent.Y + RombDim + LineDim));
                        g.DrawString(NO, new Font("Arial Narrow", 8 * (96 / (float)dx)), bn, Curent.X - LineDim * 2 + 3, Curent.Y + RombDim - 3);
                        g.DrawString(YES, new Font("Arial Narrow", 8 * (96 / (float)dx)), bn, Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2 - LineDim - 3);
                        Curent = new Point(Curent.X + RombDim / 2 + LineDim + RombDim, Curent.Y + RombDim + LineDim);
                        if (Elongatie < Curent.X) Elongatie = Curent.X;
                    }
                    else if (textPseudocod[i].ToString().TrimStart().StartsWith(EndForKeyword))
                    {
                        int a = 0;
                        string b = "";
                        int c = 0;
                        Pen pn = Pens.Black;
                        Brush bn = Brushes.Black;
                        Pen pn2 = Pens.Black;
                        Brush bn2 = Brushes.Black;
                        if (lista_executii != null && lista_executii.Count != 0)
                        {
                           /* try
                            {
                                a = corespA[i];
                                int count1 = 0;
                            inceput:
                                b = lista_executii[a + (int)nr_linii + 6 - count1][0];
                                c = Convert.ToInt32(lista_executii[a + (int)nr_linii + 6 + 1][1]);
                                if (b == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                }
                                else if (b == "-")
                                {
                                    count1++;
                                    goto inceput;
                                }
                                //  if (pn == Pens.Black) pn2 = Pens.LightGray;
                            }
                            catch { }*/

                            //COD NOU
                            int j = 0;
                            int nrord = 1;
                            for (j = i - 1; j >= 0; j--)
                            {
                                if (nrord == 0) break;
                                if (textPseudocod[j].TrimStart().StartsWith(EndForKeyword)) nrord++;
                                if (textPseudocod[j].TrimStart().StartsWith(ForKeyword))
                                {
                                    nrord--;
                                }
                            }
                            j++;

                            try
                            {
                                int count1 = 6;
                                int count2 = 8;
                            inceput1:
                                string cx = lista_executii[corespA[j] + (int)nr_linii + count1][0];
                                if (cx == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                    pn2 = Pens.LightGray;
                                    bn2 = Brushes.LightGray;
                                }
                                else if (cx == "-") { count1++; goto inceput1; }
                                inceput2:
                                string cx2 = lista_executii[corespA[j] + (int)nr_linii + count2][0];
                                if (cx2 == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                  //  pn2 = Pens.LightGray;
                                  //  bn2 = Brushes.LightGray;
                                }
                                else if (cx == "-") { count2++; goto inceput2; }

                            }
                            catch { }
                        }
                        //SF COD NOU
                        if (lista_executii == null)
                        {
                            pn = Pens.Black;
                         //   pn2 = Pens.Black;
                            bn = Brushes.Black;
                         //   bn2 = Brushes.Black;
                        }
                        Point oldCurent = Curent;
                        g.DrawLine(pn, Curent, new Point(Elongatie + LineDim + RombDim / 2, Curent.Y));
                        g.DrawLine(pn, new Point(Elongatie + LineDim + RombDim / 2, Curent.Y), new Point(Elongatie + LineDim + RombDim / 2, Curent.Y - LineDim));
                        if (i == pseudocodeEd.Lines.Current.Number) g.FillRectangle(LineHighlightColour, Elongatie + LineDim, Curent.Y - LineDim * 3, RombDim, LineDim * 2);
                        else g.DrawRectangle(pn, Elongatie + LineDim, Curent.Y - LineDim * 3, RombDim, LineDim * 2);
                        Curent = new Point(Elongatie + LineDim + RombDim / 2, Curent.Y - LineDim * 3);
                        string textrect = ForVar[lastfornum] + " = " + ForVar[lastfornum] + " + 1";
                        if (textrect.Length >= 16)
                        {
                            g.DrawString(textrect.Substring(0, 15), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textrect.Substring(0, 15), true) * 3 - 3, Curent.Y + 5);
                            g.DrawString("...", new Font("Arial Narrow", 10 * (96 / (float)dx)), bn, Curent.X + 40, Curent.Y + 2);
                        }
                        else g.DrawString(textrect, new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textrect, true) * 3 + 2, Curent.Y + 4);
                        g.DrawLine(pn, Curent, new Point(Curent.X, LastFor[lastfornum].Y + LineDim * 2 + LineDim / 2));
                        g.DrawLine(pn, new Point(Curent.X, LastFor[lastfornum].Y + LineDim * 2 + LineDim / 2), new Point(LastFor[lastfornum].X, LastFor[lastfornum].Y + LineDim * 2 + LineDim / 2));
                        g.DrawLine(pn2, new Point(LastFor[lastfornum].X, LastFor[lastfornum].Y + LineDim * 4 + RombDim), new Point(LastFor[lastfornum].X, oldCurent.Y + 10));
                        Curent = new Point(LastFor[lastfornum].X, oldCurent.Y + 10);
                        lastfornum--;
                        if (Elongatie < Elongatie + LineDim + RombDim) Elongatie = Elongatie + LineDim + RombDim;
                        if (lastfornum == 0 && GetLeadingWhitespaceAsInt(textPseudocod[i]) == 0) Elongatie = Curent.X;
                    }
                    else if (textPseudocod[i].ToString().TrimStart().StartsWith(WhileKeyword))
                    {
                        int a = 0;
                        string b = "";
                        string c = "";
                        Pen pn = Pens.Black;
                        Brush bn = Brushes.Black;
                        Pen pn2 = Pens.Black;
                        Brush bn2 = Brushes.Black;
                        if (lista_executii != null && lista_executii.Count != 0)
                        {
                            try
                            {
                                a = corespA[i] - 1;
                                int count1 = 1;
                                int count2 = 3;
                            inceput:
                                b = lista_executii[a + (int)nr_linii + 6 + count1][0];
                                c = lista_executii[a + (int)nr_linii + 6 + count2][0];
                                if (b == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                }
                                else if (b == "-")
                                {
                                    count1++;
                                    goto inceput;
                                }
                                if (c == "#####")
                                {
                                    pn2 = Pens.LightGray;
                                    bn2 = Brushes.LightGray;
                                }
                                else if (c == "-")
                                {
                                    count2++;
                                    goto inceput;
                                }
                            }
                            catch { }
                            //COD NOU
                            int j = 0;
                            int nrord = 1;
                            for (j = i - 1; j >= 0; j--)
                            {
                                if (nrord == 0) break;
                                if (textPseudocod[j].TrimStart().StartsWith(EndIfKeyword)) nrord++;
                                if (textPseudocod[j].TrimStart().StartsWith(IfKeyword))
                                {
                                    nrord--;
                                }
                            }
                            j++;
                            try
                            {
                                int count1 = 6;
                            inceput1:
                                string cx = lista_executii[corespA[j] + (int)nr_linii + count1][0];
                                if (cx == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                    pn2 = Pens.LightGray;
                                    bn2 = Brushes.LightGray;
                                }
                                else if (cx == "-") { count1++; goto inceput1; }

                            }
                            catch { }
                        }
                        //SF COD NOU
                        if (lista_executii == null)
                        {
                            pn = Pens.Black;
                            pn2 = Pens.Black;
                            bn = Brushes.Black;
                            bn2 = Brushes.Black;
                        }
                        lastwhilenum++;
                        LastWhile[lastwhilenum] = Curent;
                        textPseudocod[i] = textPseudocod[i].Replace(" " + DoKeyword, "");
                        Point[] points = { Curent, new Point(Curent.X + RombDim / 2, Curent.Y + RombDim / 2), new Point(Curent.X, Curent.Y + RombDim), new Point(Curent.X - RombDim / 2, Curent.Y + RombDim / 2), Curent };
                        if (i == pseudocodeEd.Lines.Current.Number) g.FillPolygon(LineHighlightColour, points);
                        else g.DrawPolygon(pn, points);
                        g.DrawLine(pn, new Point(Curent.X, Curent.Y + RombDim), new Point(Curent.X, Curent.Y + RombDim + LineDim));
                        g.DrawLine(pn2, new Point(Curent.X + RombDim / 2, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2));
                        string textrect;
                        try
                        {
                            textrect = textPseudocod[i].Replace(WhileKeyword + " ", "");
                        }
                        catch
                        {
                            textrect = "...";
                        }
                        if (textrect.TrimStart().Length >= 17)
                        {
                            g.DrawString(textrect.TrimStart().Substring(0, 15), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textrect.TrimStart().Substring(0, 15 ), true) * 3 - 3, Curent.Y + RombDim / 2 - 7);
                            g.DrawString("...", new Font("Arial Narrow", 10 * (96 / (float)dx)), bn, Curent.X + 40, Curent.Y + RombDim / 2 - 10);
                        }
                        else g.DrawString(textrect.TrimStart(), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textrect.TrimStart(), true) * 3 + 2, Curent.Y + RombDim / 2 - 7);
                        g.DrawLine(pn2, new Point(Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim + RombDim, Curent.Y + RombDim / 2));
                        g.DrawLine(pn2, new Point(Curent.X + RombDim / 2 + LineDim + RombDim, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim + RombDim, Curent.Y + RombDim / 2 + LineDim * 4));
                        g.DrawString(NO, new Font("Arial Narrow", 8 * (96 / (float)dx)), bn, Curent.X - LineDim * 2 + 3, Curent.Y + RombDim - 3);
                        g.DrawString(YES, new Font("Arial Narrow", 8 * (96 / (float)dx)), bn2, Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2 - LineDim - 3);
                        Curent = new Point(Curent.X + RombDim / 2 + LineDim + RombDim, Curent.Y + RombDim / 2 + LineDim * 4);
                        if (Elongatie < Curent.X) Elongatie = Curent.X;
                    }
                    else if (textPseudocod[i].ToString().TrimStart().StartsWith(EndWhileKeyword))
                    {
                        int a = 0;
                        string b = "";
                        int c = 0;
                        Pen pn = Pens.Black;
                        Pen pn2 = Pens.Black;
                        //Brush bn = Brushes.Black;
                        if (lista_executii != null && lista_executii.Count != 0)
                        {
                            try
                            {
                                a = corespA[i];
                                int count1 = 0;
                            inceput:
                                b = lista_executii[a + (int)nr_linii + 6 - count1][0];
                                c = Convert.ToInt32(lista_executii[a + (int)nr_linii + 6 + 1][1]);
                                if (b == "#####")
                                {
                                    pn = Pens.LightGray;
                                    pn2 = Pens.Black;
                                    //bn = Brushes.LightGray;
                                }
                                else if (b == "-")
                                {
                                    count1++;
                                    goto inceput;
                                }
                            }
                            catch { }
                            //COD NOU
                            int j = 0;
                            int nrord = 1;
                            for (j = i - 1; j >= 0; j--)
                            {
                                if (nrord == 0) break;
                                if (textPseudocod[j].TrimStart().StartsWith(EndWhileKeyword)) nrord++;
                                if (textPseudocod[j].TrimStart().StartsWith(WhileKeyword))
                                {
                                    nrord--;
                                }
                            }
                            j++;
                            try
                            {
                                int count1 = 6;
                            inceput1:
                                string cx = lista_executii[corespA[j] + (int)nr_linii + count1][0];
                                if (cx == "#####")
                                {
                                    pn = Pens.LightGray;
                                    pn2 = Pens.LightGray;
                                }
                                else if (cx == "-") { count1++; goto inceput1; }

                            }
                            catch { }
                        }
                        //SF COD NOU
                        if (lista_executii == null)
                        {
                            pn = Pens.Black;
                            pn2 = Pens.Black;
                      //      bn = Brushes.Black;
                      //      bn2 = Brushes.Black;
                        }
                        Point oldCurent = Curent;
                        g.DrawLine(pn, Curent, new Point(Elongatie + LineDim + RombDim / 2, Curent.Y));
                        g.DrawLine(pn, new Point(Elongatie + LineDim + RombDim / 2, Curent.Y), new Point(Elongatie + LineDim + RombDim / 2, LastWhile[lastwhilenum].Y - LineDim / 2));
                        g.DrawLine(pn, new Point(Elongatie + LineDim + RombDim / 2, LastWhile[lastwhilenum].Y - LineDim / 2), new Point(LastWhile[lastwhilenum].X, LastWhile[lastwhilenum].Y - LineDim / 2));
                        g.DrawLine(pn2, new Point(LastWhile[lastwhilenum].X, LastWhile[lastwhilenum].Y + RombDim + LineDim), new Point(LastWhile[lastwhilenum].X, Curent.Y + LineDim));
                        Curent = new Point(LastWhile[lastwhilenum].X, Curent.Y + LineDim);
                        lastwhilenum--;
                        if (Elongatie < Elongatie + LineDim + RombDim / 2) Elongatie = Elongatie + LineDim + RombDim / 2;
                        if (lastwhilenum == 0 && GetLeadingWhitespaceAsInt(textPseudocod[i]) == 0) Elongatie = Curent.X;
                    }
                    else if (textPseudocod[i].ToString().TrimStart().StartsWith(DoKeyword))
                    {
                        int a = 0;
                        string b = "";
                        int c = 0;
                        Pen pn = Pens.Black;
                        if (lista_executii != null && lista_executii.Count != 0)
                        {
                            try
                            {
                                a = corespA[i] - 1;
                                int count1 = 1;
                            inceput:
                                b = lista_executii[a + (int)nr_linii + 6 + count1][0];
                                c = Convert.ToInt32(lista_executii[a + (int)nr_linii + 6 + 1][1]);
                                if (b == "#####") pn = Pens.LightGray;
                                else if (b == "-") { count1++; goto inceput; }
                            }
                            catch { }
                            //COD NOU
                            int j = 0;
                            int nrord = 1;
                            for (j = i - 1; j >= 0; j--)
                            {
                                if (nrord == 0) break;
                                if (textPseudocod[j].TrimStart().StartsWith(EndIfKeyword)) nrord++;
                                if (textPseudocod[j].TrimStart().StartsWith(IfKeyword))
                                {
                                    nrord--;
                                }
                            }
                            j++;
                            try
                            {
                                int count1 = 6;
                            inceput1:
                                string cx = lista_executii[corespA[j] + (int)nr_linii + count1][0];
                                if (cx == "#####")
                                {
                                    pn = Pens.LightGray;
                                    //  bn = Brushes.LightGray;
                                }
                                else if (cx == "-") { count1++; goto inceput1; }

                            }
                            catch { }
                        }
                        //SF COD NOU
                        if (lista_executii == null)
                        {
                            pn = Pens.Black;
                       //     pn2 = Pens.Black;
                       //     bn = Brushes.Black;
                        //    bn2 = Brushes.Black;
                        }
                        lastdonum++;
                        LastDo[lastdonum] = Curent;
                        g.DrawLine(pn, Curent, new Point(Curent.X, Curent.Y + LineDim));
                        Curent = new Point(Curent.X, Curent.Y + LineDim);
                    }
                    else if (textPseudocod[i].ToString().TrimStart().StartsWith(LoopUntilKeyword ))
                    {
                        int a = 0;
                        string b = "";
                        int c = 0;
                        Pen pn = Pens.Black;
                        Brush bn = Brushes.Black;
                        if (lista_executii != null && lista_executii.Count != 0)
                        {
                            try
                            {
                                a = corespA[i];
                                int count1 = 1;
                            inceput:
                                b = lista_executii[a + (int)nr_linii + 6 + count1][0];
                                c = Convert.ToInt32(lista_executii[a + (int)nr_linii + 6 + 1][1]);
                                if (b == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                }
                                else if (b == "-")
                                {
                                    count1++;
                                    goto inceput;
                                }
                            }
                            catch { }
                            //COD NOU
                            int j = 0;
                            int nrord = 1;
                            for (j = i - 1; j >= 0; j--)
                            {
                                if (nrord == 0) break;
                                if (textPseudocod[j].TrimStart().StartsWith(LoopUntilKeyword)) nrord++;
                                if (textPseudocod[j].TrimStart().StartsWith(DoKeyword))
                                {
                                    nrord--;
                                }
                            }
                            j++;
                            try
                            {
                                int count1 = 6;
                            inceput1:
                                string cx = lista_executii[corespA[j] + (int)nr_linii + count1][0];
                                if (cx == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                }
                                else if (cx == "-") { count1++; goto inceput1; }

                            }
                            catch { }
                        }
                        //SF COD NOU
                        if (lista_executii == null)
                        {
                            pn = Pens.Black;
                         //   pn2 = Pens.Black;
                            bn = Brushes.Black;
                         //   bn2 = Brushes.Black;
                        }
                        Point[] points = { Curent, new Point(Curent.X + RombDim / 2, Curent.Y + RombDim / 2), new Point(Curent.X, Curent.Y + RombDim), new Point(Curent.X - RombDim / 2, Curent.Y + RombDim / 2), Curent };
                        if (i == pseudocodeEd.Lines.Current.Number) g.FillPolygon(LineHighlightColour, points);
                        else g.DrawPolygon(pn, points);
                        g.DrawLine(pn, new Point(Curent.X, Curent.Y + RombDim), new Point(Curent.X, Curent.Y + RombDim + LineDim));
                        g.DrawLine(pn, new Point(Curent.X + RombDim / 2, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2));
                        string textrect;
                        try
                        {
                            textrect = textPseudocod[i].Replace(LoopUntilKeyword+" ", "");
                        }
                        catch
                        {
                            textrect = "...";
                        }
                        if (textPseudocod[i].ToString().TrimStart().Replace(LoopUntilKeyword + " ", "").Length >= 17)
                        {
                            g.DrawString(textPseudocod[i].ToString().TrimStart().Replace(LoopUntilKeyword + " ", "").Substring(0, 15), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textPseudocod[i].ToString().TrimStart().Replace(LoopUntilKeyword + " ", "").Substring(0, 15), true) * 3 - 3, Curent.Y + RombDim / 2 - 7);
                            g.DrawString("...", new Font("Arial Narrow", 10 * (96 / (float)dx)), bn, Curent.X + 40, Curent.Y + RombDim / 2 - 10);
                        }
                        else g.DrawString(textPseudocod[i].ToString().TrimStart().Replace(LoopUntilKeyword + " ", ""), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textPseudocod[i].ToString().TrimStart().Replace(IfKeyword + " ", ""), true) * 3 + 2, Curent.Y + RombDim / 2 - 7);

                        /*if (textrect.Length >= 17)
                        {
                            g.DrawString(textrect.Trim().Substring(0, 15), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textrect.Substring(0, 15), true) * 3 - 3, Curent.Y + RombDim / 2 - 7);
                            g.DrawString("...", new Font("Arial Narrow", 10 * (96 / (float)dx)), bn, Curent.X + 40, Curent.Y + RombDim / 2 - 10);
                        }
                        else g.DrawString(textrect.Trim(), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textrect, true) * 3 + 2, Curent.Y + RombDim / 2 - 7);*/
                        g.DrawLine(pn, new Point(Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim + RombDim, Curent.Y + RombDim / 2));
                        g.DrawLine(pn, new Point(Curent.X + RombDim / 2 + LineDim + RombDim, Curent.Y + RombDim / 2), new Point(Curent.X + RombDim / 2 + LineDim + RombDim, LastDo[lastdonum].Y));
                        g.DrawLine(pn, new Point(Curent.X + RombDim / 2 + LineDim + RombDim, LastDo[lastdonum].Y), LastDo[lastdonum]);
                        g.DrawString(NO, new Font("Arial Narrow", 8 * (96 / (float)dx)), bn, Curent.X - LineDim * 2 + 3, Curent.Y + RombDim - 3);
                        g.DrawString(YES, new Font("Arial Narrow", 8), bn, Curent.X + RombDim / 2 + LineDim, Curent.Y + RombDim / 2 - LineDim - 3);
                        Curent = new Point(Curent.X, Curent.Y + RombDim + LineDim);
                        if (Elongatie < Elongatie + LineDim + RombDim) Elongatie = Elongatie + LineDim + RombDim;
                    }
                    else
                    {
                        int a = 0;
                        string b = "";
                        int c = 0;
                        Pen pn = Pens.Black;
                        Brush bn = Brushes.Black;
                        if (lista_executii != null && lista_executii.Count != 0)
                        {
                            try
                            {
                                a = coresp[i];
                                int count1 = 1;
                            inceput:
                                b = lista_executii[a + (int)nr_linii + 6 + count1][0];
                                c = Convert.ToInt32(lista_executii[a + (int)nr_linii + 6 + 1][1]);
                                if (b == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                }
                                else if (b == "-")
                                {
                                    count1++;
                                    goto inceput;
                                }
                            }
                            catch { }
                            int j = 0;
                            int nrord = 1;
                            for (j = i - 1; j >= 0; j--)
                            {
                                if (nrord == 0) break;
                                if (textPseudocod[j].TrimStart().StartsWith(EndIfKeyword)) nrord++;
                                if (textPseudocod[j].TrimStart().StartsWith(IfKeyword))
                                {
                                    nrord--;
                                }
                            }
                            j++;
                            try
                            {
                                int count1 = 6;
                            inceput1:
                                string cx = lista_executii[corespA[j] + (int)nr_linii + count1][0];
                                if (cx == "#####")
                                {
                                    pn = Pens.LightGray;
                                    bn = Brushes.LightGray;
                                }
                                else if (cx == "-") { count1++; goto inceput1; }

                            }
                            catch { }
                        }
                        if (lista_executii == null)
                        {
                            pn = Pens.Black;
                     //       pn2 = Pens.Black;
                            bn = Brushes.Black;
                      //      bn2 = Brushes.Black;
                        }
                        if (!textPseudocod[i].TrimStart().StartsWith("//") && textPseudocod[i] != "")
                        {
                            if (i == pseudocodeEd.Lines.Current.Number) g.FillRectangle(LineHighlightColour, Curent.X - RombDim / 2, Curent.Y, RombDim, LineDim * 2);
                            else g.DrawRectangle(pn, Curent.X - RombDim / 2, Curent.Y, RombDim, LineDim * 2);
                            string textrect = textPseudocod[i].TrimStart();
                            if (textrect.Length >= 19)
                            {
                                g.DrawString(textrect.Substring(0, 17), new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textrect.Substring(0, 17), true) * 3 - 3, Curent.Y + 5);
                                g.DrawString("     ...", new Font("Arial Narrow", 10 * (96 / (float)dx)), bn, Curent.X + 40, Curent.Y + 2);
                            }
                            else g.DrawString(textrect, new Font("Consolas", 8 * (96 / (float)dx)), bn, Curent.X - CountChars(textrect, true) * 3 - 1, Curent.Y + 4);
                            g.DrawLine(pn, Curent.X, Curent.Y + LineDim * 2, Curent.X, Curent.Y + LineDim * 3);
                            Curent = new Point(Curent.X, Curent.Y + LineDim * 3);
                            if (Elongatie < Curent.X + RombDim / 2 + LineDim) Elongatie = Curent.X + RombDim / 2 + LineDim;
                        }
                    }
                    if (Elongatie + 130 > SchemaLogicaCont.Width) SchemaLogicaCont.Width = Elongatie + 130;
                }
                g.DrawEllipse(Pens.Black, Curent.X - RombDim / 2 + 20, Curent.Y, 70, 30);
                g.DrawString("STOP", new Font("Consolas", 10 * (96 / (float)dx)), Brushes.Black, Curent.X - RombDim / 2 + 37, Curent.Y + 7);
                SchemaLogicaCont.Height = Curent.Y + 50;
                //bp = new Bitmap(SchemaLogicaCont.Width, SchemaLogicaCont.Height, g);
                //PENTRU DEBUG
                //g.DrawLine(Pens.Red, Elongatie, 0, Elongatie, 2000); //DESENEAZA O DREAPTA PRIN PUNCTUL ELONGATIEI MAXIME
                //g.DrawLine(Pens.Blue, Curent, new Point(Elongatie, 0)); //UNESTE PUNCTUL CU COORDONATA (ELONGATIE, 0) DE SFARSITUL CURENT AL TEVII
                //SchemaLogicaCont.Refresh();
                SchemaLogicaCont.Image = bm;
            }

            //bm.Dispose();


        }
        Point mdLoc;
        private void SchemaLogicaCont_Paint(object sender, PaintEventArgs e)
        {
            //CreateMap();
        }

        private void SchemaLogicaCont_MouseDown(object sender, MouseEventArgs e)
        {
            mdLoc = e.Location;
        }

        private void SchemaLogicaCont_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                SchemaLogicaCont.Left += e.X - mdLoc.X;
                SchemaLogicaCont.Top += e.Y - mdLoc.Y;
                if (SchemaLogicaCont.Left < SchemaLogicaCont.Parent.Left)
                {
                    SchemaLogicaCont.Left = 5;
                }
                if (SchemaLogicaCont.Top < SchemaLogicaCont.Parent.Top)
                {
                    SchemaLogicaCont.Top = 5;
                }
               /* if (SchemaLogicaCont.Left + SchemaLogicaCont.Width > SchemaLogicaCont.Parent.Width)
                {
                    SchemaLogicaCont.Left = SchemaLogicaCont.Parent.Width - SchemaLogicaCont.Width;
                }
                if (SchemaLogicaCont.Top + SchemaLogicaCont.Height > SchemaLogicaCont.Parent.Height)
                {
                    SchemaLogicaCont.Top = SchemaLogicaCont.Parent.Height - SchemaLogicaCont.Height;
                }*/
            }
        }

        private void button59_MouseEnter(object sender, EventArgs e)
        {
            label33.BackColor = Color.FromArgb(229, 229, 229);

        }

        private void button59_MouseLeave(object sender, EventArgs e)
        {
            label33.BackColor = Color.White;

        }

        private void label33_MouseEnter(object sender, EventArgs e)
        {
            button59.BackColor = Color.FromArgb(229, 229, 229);
            label33.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label33_MouseLeave(object sender, EventArgs e)
        {
            button59.BackColor = Color.White;
            label33.BackColor = Color.White;
        }

        private void label33_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.ShowOldVBScript == true | lang != "VBSCRIPT")
            {
                CasetaDeMesaj(this, SwitchToImage, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (saveFileDialog4.ShowDialog() == DialogResult.OK)
                {
                    SchemaLogicaCont.Image.Save(saveFileDialog4.FileName, ImageFormat.Png);
                }
                pseudocodeEd.Focus();
            }
        }

        private void button60_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.ShowOldVBScript == true | lang != "VBSCRIPT")
            {
                CasetaDeMesaj(this, SwitchToImage, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                try
                { 
                    PrintDocument printDocument1 = new PrintDocument();
                    printDocument1.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
                    //printDocument1.Print();
                    PrintPreviewDialog pr = new PrintPreviewDialog();
                    pr.Document = printDocument1;
                    ((Form)pr).WindowState = FormWindowState.Maximized;
                    ((Form)pr).ShowInTaskbar = true;
                    pr.ShowDialog();
                    pseudocodeEd.Focus();
                }
                catch (Exception ex)
                {
                    CasetaDeMesaj(this, UnableToPrint + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(SchemaLogicaCont.Image, 0, 0);
        }
        private void button60_MouseEnter(object sender, EventArgs e)
        {
            label34.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button60_MouseLeave(object sender, EventArgs e)
        {
            label34.BackColor = Color.White;
        }

        private void label34_MouseEnter(object sender, EventArgs e)
        {
            button60.BackColor = Color.FromArgb(229, 229, 229);
            label34.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label34_MouseLeave(object sender, EventArgs e)
        {
            button60.BackColor = Color.White;
            label34.BackColor = Color.White;
        }

        private void backgroundWorker3_DoWork_1(object sender, DoWorkEventArgs e)
        {
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
            cmdStartInfo.Arguments = "-g -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe" + "\" \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp\"";
            cmdStartInfo.RedirectStandardOutput = true;
            cmdStartInfo.RedirectStandardError = true;
            cmdStartInfo.RedirectStandardInput = true;
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            processPSC = new Process();
            processPSC.StartInfo = cmdStartInfo;
            processPSC.ErrorDataReceived += cmd_Error;
            processPSC.OutputDataReceived += cmd_DataReceived;
            cars = new List<Car>();
            processPSC.Start(); //"cscript.exe", "//Nologo " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs");
            processPSC.BeginOutputReadLine();
            processPSC.BeginErrorReadLine();
            processPSC.WaitForExit();
            DoOnUIThread(delegate()
            {
                try
                {
                    buttonItem5.Visible = false;
                    buttonItem6.Visible = false;
                    if (cars.Count == 0) buttonItem6.Visible = true;
                    else buttonItem5.Visible = true;
                    HideCompileStatus.Enabled = true;
                    dataGridView1.DataSource = cars;
                    dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                    dataGridView1.MultiSelect = false;
                    dataGridView1.CellContentDoubleClick += dataGridView1_CellContentDoubleClick;
                    progressBarItem1.Visible = false;
                }
                catch { }
            });
        }

        private void HideCompileStatus_Tick(object sender, EventArgs e)
        {
            buttonItem5.Visible = false;
            buttonItem6.Visible = false;
            HideCompileStatus.Enabled = false;
        }
        int[,] breakpoints = new int[2, 1000];
        private void addremoveBreakpointOnThisLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var list = pseudocodeEd.Markers.GetMarkers(pseudocodeEd.Lines.Current);
            bool ok = false;
            foreach (ScintillaNET.Marker mk in list)
            {
                if (mk.ToString() != "MarkerNumber0") ok = true;
            }
            if (ok == true) pseudocodeEd.Markers.DeleteInstance(pseudocodeEd.Lines.Current.Number, -1);
            else pseudocodeEd.Markers.AddInstanceSet(pseudocodeEd.Lines.Current, 23);

        }

        private void removeBreakpointOnThisLineToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void runTimer_Tick(object sender, EventArgs e)
        {
            runPanel.Visible = true;
            runPanel.Height += 40;
            if (runPanel.Height % 10 == 0)
            {
                runPanel.Top += 1;
            }
            if (runPanel.Height == 160)
            {
                runPanel.Height = 158;
                runTimer.Enabled = false;

            }
        }

        private void button63_Click(object sender, EventArgs e)
        {
            runPanel.Visible = true;
            runPanel.BringToFront();
            runPanel.Height = 0;
            if (this.WindowState == FormWindowState.Maximized) runPanel.Top = panel3.Height - 7;
            else runPanel.Top = panel3.Height + 7;
            runPanel.Left = panel3.Left + 218;
            runPanel.Focus();
            button63.BackColor = Color.FromArgb(229, 229, 229);
            runTimer.Enabled = true;
        }

        private void runPanel_Leave(object sender, EventArgs e)
        {
            runPanel.Visible = false;
            button63.BackColor = Color.White;
        }

        private void button62_Click(object sender, EventArgs e)
        {
            tabControl3.SelectedIndex = 0;
            progressBarItem1.Visible = true;
            statusBar1.Refresh();
            Compile(false);
            pseudocodeEd.Focus();
        }
        Process processPSC3;
        bool fabkth3 = false;
        void CompileDebugRun()
        {
            richTextBox1.Text = "";
            pseudocodeEd.Focus();
            //tabControl3.SelectedTabIndex = 1;
            fabkth3 = true;
            Compile(false);
            backgroundWorker4.RunWorkerAsync();


        }
        private void button61_Click(object sender, EventArgs e)
        {
            CompileDebugRun();
        }

        private void button62_MouseEnter(object sender, EventArgs e)
        {
            label36.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button62_MouseLeave(object sender, EventArgs e)
        {
            label36.BackColor = Color.White;
        }

        private void label36_MouseEnter(object sender, EventArgs e)
        {
            button62.BackColor = Color.FromArgb(229, 229, 229);
            label36.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label36_MouseLeave(object sender, EventArgs e)
        {
            button62.BackColor = Color.White;
            label36.BackColor = Color.White;
        }

        private void button61_MouseEnter(object sender, EventArgs e)
        {
            label35.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void button61_MouseLeave(object sender, EventArgs e)
        {
            label35.BackColor = Color.White;
        }

        private void label35_MouseEnter(object sender, EventArgs e)
        {
            button61.BackColor = Color.FromArgb(229, 229, 229);
            label35.BackColor = Color.FromArgb(229, 229, 229);
        }

        private void label35_MouseLeave(object sender, EventArgs e)
        {
            button61.BackColor = Color.White;
            label35.BackColor = Color.White;
        }
        IntPtr processHWND = IntPtr.Zero;
        private void backgroundWorker4_DoWork(object sender, DoWorkEventArgs e)
        {

            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
            cmdStartInfo.Arguments = "-g -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe" + "\" \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp\"";
            cmdStartInfo.RedirectStandardOutput = true;
            cmdStartInfo.RedirectStandardError = true;
            cmdStartInfo.RedirectStandardInput = true;
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            processPSC = new Process();
            processPSC.StartInfo = cmdStartInfo;
            processPSC.ErrorDataReceived += cmd_Error;
            processPSC.OutputDataReceived += cmd_DataReceived;
            cars = new List<Car>();
            processPSC.Start(); //"cscript.exe", "//Nologo " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs");
            processPSC.BeginOutputReadLine();
            processPSC.BeginErrorReadLine();
            processPSC.WaitForExit();
            DoOnUIThread(delegate()
            {
                buttonItem5.Visible = false;
                buttonItem6.Visible = false;
                if (cars.Count == 0) buttonItem6.Visible = true;
                else buttonItem5.Visible = true;
                HideCompileStatus.Enabled = true;
                dataGridView1.DataSource = cars;
                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView1.MultiSelect = false;
                dataGridView1.CellContentDoubleClick += dataGridView1_CellContentDoubleClick;
                progressBarItem1.Visible = false;
            });
            if (cars.Count == 0)
            {



                DoOnUIThread(delegate()
                {

                });

                ProcessStartInfo cmdStartInfo2 = new ProcessStartInfo();
                cmdStartInfo2.FileName = Properties.Settings.Default.MinGWPath + @"\bin\gdb.exe";
                cmdStartInfo2.Arguments = "-quiet \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe\"";
                cmdStartInfo2.RedirectStandardOutput = true;
                cmdStartInfo2.RedirectStandardError = true;
                cmdStartInfo2.RedirectStandardInput = true;
                cmdStartInfo2.UseShellExecute = false;
                cmdStartInfo2.CreateNoWindow = true;
                processPSC3 = new Process();
                processPSC3.StartInfo = cmdStartInfo2;
                processPSC3.ErrorDataReceived += ErrorDebug;
                processPSC3.OutputDataReceived += DataReceivedDebug;
                //"cscript.exe", "//Nologo " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs");
                /*DoOnUIThread(delegate()
                {
                    drBW2 = Overlay(this, ProgramIsRunning, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (drBW2 == System.Windows.Forms.DialogResult.OK)
                    {
                        try
                        {
                            //processPSC.Kill();
                            processPSC2.Kill();
                        }
                        catch { }
                    }
                });*/
                processPSC3.Start();
                processPSC3.BeginOutputReadLine();
                processPSC3.BeginErrorReadLine();
                processPSC3.StandardInput.WriteLine("set breakpoint pending on", Color.Yellow, true);
                processPSC3.StandardInput.WriteLine("delete", Color.Yellow, true);
                processPSC3.StandardInput.WriteLine("set new-console on", Color.Yellow, true);
                DoOnUIThread(delegate()
                {
                    string oldlang = lang;
                    lang = "CPP";
                    for (int i = 0; i < pseudocodeEd.Lines.Count; i++)
                    {
                        var list = pseudocodeEd.Markers.GetMarkers(i);

                        foreach (ScintillaNET.Marker mk in list)
                        {
                            if (mk.ToString() != "MarkerNumber0")
                            {


                                string[] text = converterEd.Text.Split('\n');
                                int linei = 0;
                                for (int j = 0; j < coresp[i] + 6; j++)
                                {
                                    try
                                    {
                                        if (text[j].Contains("{") || text[j].Contains("else")) linei++;
                                    }
                                    catch { }

                                }


                                //MessageBox.Show((i + linei + 6).ToString());
                                converterEd.Text = Compile(pseudocodeEd.Text, lang);
                                int a = coresp[i];
                                processPSC3.StandardInput.WriteLine("break \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp\"" + ":" + (i + linei + 8) + "");
                                break;
                            }


                        }
                    }
                    lang = oldlang;
                    processPSC3.StandardInput.WriteLine("run", Color.Yellow, true);

                    /*IntPtr hWnd = IntPtr.Zero;
                    while (hWnd == IntPtr.Zero)
                    {
                        foreach (Process pList in Process.GetProcesses())
                        {
                            if (pList.MainWindowTitle.Contains(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe"))
                            {
                                hWnd = pList.MainWindowHandle;
                            }
                        }
                    }
                    SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
                    processHWND = hWnd;*/
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.ScrollToCaret();
                });

                //processPSC2.StandardInput.WriteLine("run");
                //processPSC2.WaitForExit();

            }
            else
            {
                DoOnUIThread(delegate()
                {
                    tabControl3.SelectedIndex = 0;
                });
            }
            //new code which uses the MinGW c++ compiler
            /*ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
            cmdStartInfo.Arguments = "-g -o" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe" + " " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp";
            cmdStartInfo.RedirectStandardOutput = true;
            cmdStartInfo.RedirectStandardError = true;
            cmdStartInfo.RedirectStandardInput = true;
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            processPSC = new Process();
            processPSC.StartInfo = cmdStartInfo;
            processPSC.ErrorDataReceived += cmd_Error;
            processPSC.OutputDataReceived += cmd_DataReceived;
            cars = new List<Car>();
            processPSC.Start(); //"cscript.exe", "//Nologo " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs");
            processPSC.BeginOutputReadLine();
            processPSC.BeginErrorReadLine();
            processPSC.WaitForExit();
            DoOnUIThread(delegate()
            {
                buttonItem5.Visible = false;
                buttonItem6.Visible = false;
                if (cars.Count == 0) buttonItem6.Visible = true;
                else buttonItem5.Visible = true;
                HideCompileStatus.Enabled = true;
                dataGridView1.DataSource = cars;
                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView1.MultiSelect = false;
                dataGridView1.CellContentDoubleClick += dataGridView1_CellContentDoubleClick;
            });*/
            /*try
            {
                DoOnUIThread(delegate()
                {
                    SendKeys.Send("{ESC}");
                });
            }
            catch { }*/

        }
        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 SWP_SHOWWINDOW = 0x0040;
        // Call this way:
        string val = "";
        int aajuns = 0;
        bool ReturnInput = false;
        private void DataReceivedDebug(object sender, DataReceivedEventArgs e)
        {
            DoOnUIThread(delegate()
            {
                try
                {
                    if (e.Data.Contains("Breakpoint"))
                    {
                        aajuns = 0;
                        listBox4.Items.Clear();
                        for (int i = 0; i < listBox3.Items.Count; i++)
                        {
                            processPSC3.StandardInput.WriteLine("print " + listBox3.Items[i].ToString(), Color.Yellow, true);
                            ReturnInput = true;
                        }
                    }
                    richTextBox1.Text += e.Data.ToString() + "\r\n";
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.ScrollToCaret();
                    if (ReturnInput == true)
                    {
                        if (aajuns > 1)
                        {
                            try
                            {
                                listBox4.Items.Add(e.Data.ToString().Split('=')[1]);
                                //CasetaDeMesaj(this, e.Data.ToString().Split('=')[1], AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            catch
                            {
                                listBox4.Items.Add(e.Data.ToString());
                                //CasetaDeMesaj(this, e.Data.ToString(), AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        aajuns++;
                        if (aajuns == listBox3.Items.Count + 2) ReturnInput = false;
                    }
                }
                catch { }
            });
        }

        private void ErrorDebug(object sender, DataReceivedEventArgs e)
        {
            //CasetaDeMesaj(this, e.Data.ToString(), AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button64_Click(object sender, EventArgs e)
        {
            try
            {

                processPSC3.StandardInput.WriteLine(textBox6.Text, Color.Yellow, true);
                textBox6.Text = "";
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
                u = 0;
                for (int i = 0; i < 1000; i++) a[i] = 0;
                for (int i = 0; i < pseudocodeEd.Lines.Count; i++)
                {
                    var list = pseudocodeEd.Markers.GetMarkers(i);

                    foreach (ScintillaNET.Marker mk in list)
                    {
                        if (mk.ToString() != "MarkerNumber0" && mk.ToString() == "MarkerNumber23")
                        {
                            a[u] = i;
                            u++;

                        }

                    }
                }
                markerTimer.Enabled = true;
            }
            catch
            {
                richTextBox1.Text = FailedToSendCom;
            }
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                button64.PerformClick();
            }
        }

        private void button65_Click(object sender, EventArgs e)
        {
            try
            {
                processPSC3.Kill();
                richTextBox1.Text = SucessEndDebugProc;
            }
            catch
            {
                richTextBox1.Text = FailEndDebugProc;
            }
        }

        private void button66_Click(object sender, EventArgs e)
        {
            try
            {
                processPSC3.StandardInput.WriteLine("continue", Color.Yellow, true);
            }
            catch
            {
                richTextBox1.Text = FailedToSendCom;
            }
        }

        private void markerTimer_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < 1000; i++)
                if (a[i] != 0) pseudocodeEd.Markers.AddInstanceSet(a[i], 23);

            markerTimer.Enabled = false;
        }

        private void button67_Click(object sender, EventArgs e)
        {
            listBox3.Items.Add(textBox7.Text);
            textBox7.Text = "";
        }

        private void button68_Click(object sender, EventArgs e)
        {
            listBox3.Items.Remove(listBox3.SelectedItem);
            listBox4.Items.Clear();
        }

        private void listBox3_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void listBox3_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                processPSC3.StandardInput.WriteLine("print " + listBox3.SelectedItem, Color.Yellow, true);
                ReturnInput = true;
            }
            catch
            {
                ReturnInput = false;

            }
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                button67.PerformClick();
            }
        }

        private void backgroundWorker5_DoWork(object sender, DoWorkEventArgs e)
        {
            DoOnUIThread(delegate()
            {
                fabkth3 = true;
                Compile(false);
            });
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
            cmdStartInfo.Arguments = "-g -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe" + "\" \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp\"";
            cmdStartInfo.RedirectStandardOutput = true;
            cmdStartInfo.RedirectStandardError = true;
            cmdStartInfo.RedirectStandardInput = true;
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            processPSC = new Process();
            processPSC.StartInfo = cmdStartInfo;
            processPSC.ErrorDataReceived += cmd_Error;
            processPSC.OutputDataReceived += cmd_DataReceived;
            cars = new List<Car>();
            processPSC.Start(); //"cscript.exe", "//Nologo " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs");
            processPSC.BeginOutputReadLine();
            processPSC.BeginErrorReadLine();
            processPSC.WaitForExit();
            DoOnUIThread(delegate()
            {
                buttonItem5.Visible = false;
                buttonItem6.Visible = false;
                if (cars.Count == 0) buttonItem6.Visible = true;
                else buttonItem5.Visible = true;
                HideCompileStatus.Enabled = true;
                dataGridView1.DataSource = cars;
                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView1.MultiSelect = false;
                dataGridView1.CellContentDoubleClick += dataGridView1_CellContentDoubleClick;
                progressBarItem1.Visible = false;
            });
            if (cars.Count == 0)
            {
                //new code which uses the MinGW c++ compiler
                ProcessStartInfo cmdStartInfo3 = new ProcessStartInfo();
                cmdStartInfo3.FileName = "cmd"; // Properties.Settings.Default.MinGWPath + @"\bin\windres.exe";
                cmdStartInfo3.Arguments = " " + System.IO.Path.GetPathRoot(Application.StartupPath).Substring(0, 2); //"\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc\"" + " -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res\"";
                cmdStartInfo3.RedirectStandardInput = true;
                cmdStartInfo3.RedirectStandardError = true;
                cmdStartInfo3.RedirectStandardOutput = true;
                cmdStartInfo3.UseShellExecute = false;
                //cmdStartInfo3.CreateNoWindow = true;
                Process processPSC5 = new Process();
                processPSC5.StartInfo = cmdStartInfo3;
                processPSC5.ErrorDataReceived += processPSC5_ErrorDataReceived;
                //processPSC5.OutputDataReceived += processPSC5_ErrorDataReceived;
                processPSC5.Start();
                processPSC5.BeginErrorReadLine();
                processPSC5.StandardInput.WriteLine("cd \"" + Application.StartupPath + "\\MinGW\\bin\"");
                processPSC5.StandardInput.WriteLine("windres.exe " + "\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc\"" + " -O coff -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res\"");
                processPSC5.StandardInput.WriteLine("exit");
                processPSC5.WaitForExit();
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res"))
                {
                    ProcessStartInfo cmdStartInfo2 = new ProcessStartInfo();
                    cmdStartInfo2.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
                    cmdStartInfo2.Arguments = "-o \"" + EXELocation.Text + "\" \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp" + "\" \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res\"";
                    cmdStartInfo2.CreateNoWindow = true;
                    processPSC2 = new Process();
                    processPSC2.StartInfo = cmdStartInfo2;
                    processPSC2.Start();
                    processPSC2.WaitForExit();

                }
                else
                {
                    /*                   DoOnUIThread(delegate()
                   {
                   CasetaDeMesaj(this, CreateEXEError, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                   });*/
                }
                try
                {
                    DoOnUIThread(delegate()
                    {
                        panel14.Visible = false;
                    });
                }
                catch { }
            }
            else
            {
                try
                {
                    DoOnUIThread(delegate()
                    {
                        panel14.Visible = false;
                    });
                }
                catch { }
            }
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res");
            Properties.Settings.Default.EXELocation = EXELocation.Text;
            Properties.Settings.Default.Save();
        }

        void processPSC5_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {

            DoOnUIThread(delegate()
            {
                try
                {
                    CasetaDeMesaj(this, CreateEXEError + "\n" + e.Data.ToString(), AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Focus();
                }
                catch { }
            });

        }

        private void panel13_Scroll(object sender, ScrollEventArgs e)
        {
            //highlighter1.UpdateHighlights();
        }

        private void line1_Click(object sender, EventArgs e)
        {
            //this.Close();
        }
        private FormWindowState mLastState;
        /*   protected override void OnClientSizeChanged(EventArgs e)
           {
               if (this.WindowState == FormWindowState.Normal && mLastState == FormWindowState.Minimized)
               {
                   //this.Activated -= Form1_Activated;
                   MinimizeWaitABit.Enabled = true;
                   mLastState = this.WindowState;
                   base.OnClientSizeChanged(e);
                   return;
               }
               if (this.WindowState != mLastState)
               {
                   mLastState = this.WindowState;
                   OnWindowStateChanged(e);
               }
               base.OnClientSizeChanged(e);
           }
           protected void OnWindowStateChanged(EventArgs e)
           {
               if (this.WindowState == FormWindowState.Minimized) wpfwindow.Hide();
               if (this.WindowState == FormWindowState.Maximized) wpfwindow.Hide();
               if (this.WindowState == FormWindowState.Normal) 
               {
                   wpfwindow.Visibility = System.Windows.Visibility.Visible;
                   wpfwindow.Activate();
                   UnminimizedWindow.Enabled = true;
               }
            
           }*/

        private void Form1_Move(object sender, EventArgs e)
        {
            /* wpfwindow.Height = this.Height + 20;
             wpfwindow.Width = this.Width + 20;
             wpfwindow.Left = this.Left - 10;
             wpfwindow.Top = this.Top - 10;*/
        }

        private void Form1_Activated(object sender, EventArgs e)
        {

            /*    try
                {
                    if (this.WindowState == FormWindowState.Normal)
                    {
                        this.TopMost = true;
                        wpfwindow.Visibility = System.Windows.Visibility.Visible;
                        wpfwindow.Activate();
                    
                        this.TopMost = false;
                        this.Focus();
                    }
                }
                catch { }*/
            /* try
             {
                 if (processHWND != IntPtr.Zero)
                 {

                         SetWindowPos(processHWND, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
                 }
             }
             catch { }*/
        }
        bool UnminimizedOK = false;
        private void UnminimizedWindow_Tick(object sender, EventArgs e)
        {

            /*   this.TopMost = true;
               this.TopMost = false;
               this.Focus();
               UnminimizedWindow.Enabled = false;

               return;
               if (UnminimizedOK == true)
               {
                   this.TopMost = true;
                   this.TopMost = false;
                   this.Focus();
                   pseudocodeEd.Focus();
                   UnminimizedOK = false;
                   UnminimizedWindow.Enabled = false;
               }
               else
               {*/
            /*    wpfwindow.Visibility = System.Windows.Visibility.Visible;
                //wpfwindow.Activate();
                wpfwindow.Height = this.Height + 20;
                wpfwindow.Width = this.Width + 20;
                wpfwindow.Left = this.Left - 10;
                wpfwindow.Top = this.Top - 10;*/

            /*      UnminimizedOK = true;
                  Debug.WriteLine("da");
              }*/
            UnminimizedWindow.Enabled = false;
        }
        bool hidespl = false;
        int DoResize = 0;
        private void ResizeTimer_Tick(object sender, EventArgs e)
        {
            /*  done = true;
              DoResize++;
              if (DoResize == 10)
              {
                  //RedimLite();
                  DoResize = 0;

                  if (this.WindowState != FormWindowState.Maximized)
                  {
                      wpfwindow.Height = this.Height + 20;
                      wpfwindow.Width = this.Width + 20;
                      wpfwindow.Left = this.Left - 10;
                      wpfwindow.Top = this.Top - 10;
                      if (wpfwindow.Visibility == System.Windows.Visibility.Hidden)
                      {
                          wpfwindow.Visibility = System.Windows.Visibility.Visible;
                          UnminimizedWindow.Enabled = true;

                      }
                  }
                  else
                  {
                      wpfwindow.Visibility = System.Windows.Visibility.Hidden;
                      this.TopMost = false;
                  }*/


            //UnminimizedOK = false;
            //UnminimizedWindow.Enabled = true;
            ResizeTimer.Enabled = false;
            // }
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {

            GC.Collect();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            /*enableFormLevelDoubleBuffering = false;
            this.MaximizeBox = true;*/
        }

        private void MinimizeWaitABit_Tick(object sender, EventArgs e)
        {
            /*      wpfwindow.Visibility = System.Windows.Visibility.Visible;
          wpfwindow.Activate();
          UnminimizedWindow.Enabled = true;*/
            MinimizeWaitABit.Enabled = false;
            //this.Activated += Form1_Activated;
        }

        private void Title_Click(object sender, EventArgs e)
        {

        }

        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (e.Clicks == 2)
                {
                    if (this.WindowState == FormWindowState.Maximized) this.WindowState = FormWindowState.Normal;
                    else if (this.WindowState == FormWindowState.Normal) this.WindowState = FormWindowState.Maximized;
                    return;
                }
                bool wasMaximized = false;
                if (this.WindowState == FormWindowState.Maximized)
                {
                    this.Resize -= Form1_Resize;
                    wasMaximized = true;
                }

                ReleaseCapture();

                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);

                if (wasMaximized == true) this.Resize += Form1_Resize;

                if (wasMaximized == true) Redim();
            }
            if (e.Button == MouseButtons.Right)
            {
                // need to get handle of window


                //translate mouse cursor porition to screen coordinates
                Point p = PointToScreen(e.Location);

                IntPtr callingTaskBarWindow = this.Handle;
                IntPtr wMenu = GetSystemMenu(this.Handle, false);
                // Display the menu
                uint command = TrackPopupMenuEx(wMenu,
                    (uint)(TPMLEFTBUTTON | TPMRETURNCMD), (int)p.X, (int)p.Y, callingTaskBarWindow, IntPtr.Zero);
                if (command == 0)
                    return;

                PostMessage(this.Handle, WM_SYSCOMMAND, new IntPtr(command), IntPtr.Zero);
                /*if (this.WindowState == FormWindowState.Maximized)
                {
                    restoreDownToolStripMenuItem.Enabled = true;
                    maximizeToolStripMenuItem.Enabled = false;
                }
                else
                {
                    restoreDownToolStripMenuItem.Enabled = false;
                    maximizeToolStripMenuItem.Enabled = true;
                }
                contextMenuStrip1.Show(Cursor.Position);*/
            }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
           /* if (e.Button == MouseButtons.Left)
            {
                if (e.Clicks == 2)
                {
                    if (this.WindowState == FormWindowState.Maximized)
                    {
                        //this.Resize -= Form1_Resize;
                        this.WindowState = FormWindowState.Normal;
                        //RedrawTimer.Enabled = true;
                    }
                    else if (this.WindowState == FormWindowState.Normal)
                    {
                        //this.Resize -= Form1_Resize;
                        this.WindowState = FormWindowState.Maximized;
                        //RedrawTimer.Enabled = true;
                    }
                    return;
                }
                bool wasMaximized = false;
                if (this.WindowState == FormWindowState.Maximized)
                {
                    RedrawTimer.Enabled = true;
                    this.Resize -= Form1_Resize;
                    wasMaximized = true;
                }

                ReleaseCapture();

                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);

                if (wasMaximized == true) this.Resize += Form1_Resize;

                if (wasMaximized == true) Redim();
            }
            if (e.Button == MouseButtons.Right)
            {
                // need to get handle of window


                //translate mouse cursor porition to screen coordinates
                Point p = PointToScreen(e.Location);

                IntPtr callingTaskBarWindow = this.Handle;
                IntPtr wMenu = GetSystemMenu(this.Handle, false);
                // Display the menu
                uint command = TrackPopupMenuEx(wMenu,
                    (uint)(TPMLEFTBUTTON | TPMRETURNCMD), (int)p.X, (int)p.Y, callingTaskBarWindow, IntPtr.Zero);
                if (command == 0)
                    return;

                PostMessage(this.Handle, WM_SYSCOMMAND, new IntPtr(command), IntPtr.Zero);
                /*if (this.WindowState == FormWindowState.Maximized)
                {
                    restoreDownToolStripMenuItem.Enabled = true;
                    maximizeToolStripMenuItem.Enabled = false;
                }
                else
                {
                    restoreDownToolStripMenuItem.Enabled = false;
                    maximizeToolStripMenuItem.Enabled = true;
                }
                contextMenuStrip1.Show(Cursor.Position);*/
           // }
        }

        private void Form1_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void RedrawTimer_Tick(object sender, EventArgs e)
        {
            this.Resize += Form1_Resize;
            Redim();
            RedrawTimer.Enabled = false;
        }
        public void RedenumireVariabila(string oldname, string numenou)
        {
            string[] text = pseudocodeEd.Text.Split('\n');
            //var pos = pseudocodeEd.NativeInterface.GetCurrentPos();
            if (pseudocodeEd.Selection.Start == pseudocodeEd.Selection.End && numenou != "")
            {
                pseudocodeEd.FindReplace.Flags = ScintillaNET.SearchFlags.WholeWord;
                try
                {
                    IList<ScintillaNET.Range> ranges;
                    if (oldname != "")
                    {
                        List<ScintillaNET.Range> ret = new List<ScintillaNET.Range>();

                        pseudocodeEd.UndoRedo.BeginUndoAction();
                        int startPos = 0;
                        int diff = numenou.Length - oldname.Length;
                        bool first = true;
                        int num = -1;
                        int startPosi = 0;
                        while (true)
                        {

                            ScintillaNET.Range r = pseudocodeEd.FindReplace.Find(startPosi, pseudocodeEd.NativeInterface.GetTextLength(), oldname, ScintillaNET.SearchFlags.WholeWord);
                            if (r == null || r.StartingLine.Number == num)
                            {
                                break;
                            }
                            else
                            {

                                string inc = pseudocodeEd.Text.Substring(0, r.Start);
                                string fin = pseudocodeEd.Text.Substring(r.End, pseudocodeEd.Text.Length - r.Start - r.Length);
                                string[] incx = inc.Split('\n');
                                string[] finx = fin.Split('\n');
                                char[] incc = incx[incx.Length - 1].ToCharArray();
                                char[] finc = finx[0].ToCharArray();
                                int nrinc = 0, nrfin = 0;
                                for (int k = incc.Length - 1; k >= 0; k--)
                                {
                                    if (incc[k] == '\"') nrinc++;
                                    if (incc[k] == ',' && (k == incc.Length - 2 || k == incc.Length - 3)) break;
                                }
                                for (int k = 0; k < finc.Length; k++)
                                {
                                    if (finc[k] == '\"')
                                    {
                                        nrfin++;
                                        break;
                                    }
                                }
                                //Debug.WriteLine(nrinc.ToString() + " " + nrfin.ToString());
                                if (!text[r.StartingLine.Number].StartsWith("//") && (nrinc != nrfin || (nrinc == 0 && nrfin == 0)))
                                {
                                    r.Text = numenou;
                                    r.End = startPos = r.Start + numenou.Length;
                                    ret.Add(r);
                                }
                                else
                                {
                                    r.Text = oldname;
                                    r.End = startPos = r.Start + numenou.Length;
                                    ret.Add(r);
                                }
                            }
                            if (first == true)
                            {
                                first = false;
                                num = r.StartingLine.Number;
                            }
                            startPosi = r.End;
                        }

                        pseudocodeEd.UndoRedo.EndUndoAction();
                        ExTag exTag = (ExTag)(pseudocodeEd.Tag);
                        ExTag tg = new ExTag();
                        tg.Add("filename", (string)(exTag.Get("filename")));
                        tg.Add("issaved", false);
                        tg.Add("uniqueString", (string)(exTag.Get("uniqueString")));
                        pseudocodeEd.Tag = tg;
                        if (!this.Text.EndsWith("*")) this.Text += " *";
                    }
                }
                catch { }
            }

        }
        private void renameVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string[] text = pseudocodeEd.Text.Split('\n');
            var pos = pseudocodeEd.NativeInterface.GetCurrentPos();
            string oldname = pseudocodeEd.GetWordFromPosition(pos);
            //MessageBox.Show(oldname);
            string numenou = Microsoft.VisualBasic.Interaction.InputBox(NewVariableName, AppName);
            RedenumireVariabila(oldname, numenou);
            /*if (pseudocodeEd.Selection.Start == pseudocodeEd.Selection.End && numenou != "")
            {
                pseudocodeEd.FindReplace.Flags = ScintillaNET.SearchFlags.WholeWord;
                try
                {
                    IList<ScintillaNET.Range> ranges;
                    if (oldname != "")
                    {
                        List<ScintillaNET.Range> ret = new List<ScintillaNET.Range>();

                        pseudocodeEd.UndoRedo.BeginUndoAction();
                        int startPos = 0;
                        int diff = numenou.Length - oldname.Length;
                        bool first = true;
                        int num = -1;
                        int startPosi = 0;
                        while (true)
                        {

                            ScintillaNET.Range r = pseudocodeEd.FindReplace.Find(startPosi, pseudocodeEd.NativeInterface.GetTextLength(), oldname, ScintillaNET.SearchFlags.WholeWord);
                            if (r == null || r.StartingLine.Number == num)
                            {
                                break;
                            }
                            else
                            {

                                string inc = pseudocodeEd.Text.Substring(0, r.Start);
                                string fin = pseudocodeEd.Text.Substring(r.End, pseudocodeEd.Text.Length - r.Start - r.Length);
                                string[] incx = inc.Split('\n');
                                string[] finx = fin.Split('\n');
                                char[] incc = incx[incx.Length - 1].ToCharArray();
                                char[] finc = finx[0].ToCharArray();
                                int nrinc = 0, nrfin = 0;
                                for (int k = incc.Length - 1; k >= 0; k--)
                                {
                                    if (incc[k] == '\"') nrinc++;
                                    if (incc[k] == ',' && (k == incc.Length - 2 || k == incc.Length - 3)) break;
                                }
                                for (int k = 0; k < finc.Length; k++)
                                {
                                    if (finc[k] == '\"')
                                    {
                                        nrfin++;
                                        break;
                                    }
                                }
                                //Debug.WriteLine(nrinc.ToString() + " " + nrfin.ToString());
                                if (!text[r.StartingLine.Number].StartsWith("//") && (nrinc != nrfin || (nrinc == 0 && nrfin == 0)))
                                {
                                    r.Text = numenou;
                                    r.End = startPos = r.Start + numenou.Length;
                                    ret.Add(r);
                                }
                                else
                                {
                                    r.Text = oldname;
                                    r.End = startPos = r.Start + numenou.Length;
                                    ret.Add(r);
                                }
                            }
                            if (first == true)
                            {
                                first = false;
                                num = r.StartingLine.Number;
                            }
                            startPosi = r.End;
                        }

                        pseudocodeEd.UndoRedo.EndUndoAction();
                        ExTag exTag = (ExTag)(pseudocodeEd.Tag);
                        ExTag tg = new ExTag();
                        tg.Add("filename", (string)(exTag.Get("filename")));
                        tg.Add("issaved", false);
                        tg.Add("uniqueString", (string)(exTag.Get("uniqueString")));
                        pseudocodeEd.Tag = tg;
                        if (!this.Text.EndsWith("*")) this.Text += " *";
                    }
                }
                catch { }
            }*/
        }
        
        private void goToDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (pseudocodeEd.Text != "")
            {
                string[] text = pseudocodeEd.Text.Split('\n');
                var pos = pseudocodeEd.NativeInterface.GetCurrentPos();
                string oldname = pseudocodeEd.GetWordFromPosition(pos);
                if (pseudocodeEd.Selection.Start == pseudocodeEd.Selection.End && oldname != "")
                {
                    pseudocodeEd.FindReplace.Flags = ScintillaNET.SearchFlags.WholeWord;
                    try
                    {
                        IList<ScintillaNET.Range> r = pseudocodeEd.FindReplace.FindAll(oldname);
                        foreach (var ran in r)
                        {
                            if (!text[ran.StartingLine.Number].StartsWith("//") && (text[ran.StartingLine.Number].Contains("intreg") || text[ran.StartingLine.Number].Contains("sir") || text[ran.StartingLine.Number].Contains("real")))
                            {
                                pseudocodeEd.GoTo.Line(ran.StartingLine.Number);

                            }
                            //else CasetaDeMesaj(this, NotDefinible, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    catch { }
                }
            }
        }

        private void addremoveWatchOnThisVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pos = pseudocodeEd.NativeInterface.GetCurrentPos();
            string oldname = pseudocodeEd.GetWordFromPosition(pos);
            if (listBox3.Items.Contains(oldname)) listBox3.Items.Remove(oldname);
            else listBox3.Items.Add(oldname);

        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void listBox3_OnVerticalScroll_1(object sender, ScrollEventArgs e)
        {
            listBox4.TopIndex = listBox3.TopIndex;
        }

        private void listBox4_OnVerticalScroll(object sender, ScrollEventArgs e)
        {
            listBox3.TopIndex = listBox4.TopIndex;
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                listBox4.SelectedIndex = listBox3.SelectedIndex;
            }
            catch { }
        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                listBox3.SelectedIndex = listBox4.SelectedIndex;
            }
            catch { }
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void copyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(listBox4.SelectedItem.ToString());
            }
            catch { }
        }
        Thread newThread = null;
        private void button71_Click(object sender, EventArgs e)
        {

            ThreadStart newThreadStart = new ThreadStart(newThread_Execute);
            newThread = new Thread(newThreadStart);
            newThread.Start();
        }

        // The thread we start up to demonstrate non-UI exception handling.  
        void newThread_Execute()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private void splitContainer2_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        public void splitContainer3_SplitterMoved(object sender, SplitterEventArgs e)
        {
           // MessageBox.Show(splitContainer3.Panel2.Height.ToString());
                MainForm mf = this.MdiParent as MainForm;
                mf.ResizeErrorList(splitContainer3.Panel2.Height, this.Handle);
        }
        private void tabControl3_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {

        }

        private void buttonItem2_Click_1(object sender, EventArgs e)
        {
            string text = buttonItem1.Text;
            text = text.Replace(" | ", "\r\n");
            text = text.Replace("FILE NOT YET SAVED", "");
            text = text.Replace("FILE SAVED BY USER", "");
            text = text.Replace("FIȘIER SALVAT DE UTILIZATOR", "");
            text = text.Replace("FIȘIER NESALVAT ÎNCĂ", "");
            text = text.Substring(3, text.Length - 3);
            Stats st = new Stats(text);
            st.ShowDialog();
        }
        int last_measure_lines = -1;
        private void pseudocodeEd_TextChanged(object sender, EventArgs e)
        {
            int lines = pseudocodeEd.Lines.Count;
            if (lines != last_measure_lines)
            {
                last_measure_lines = lines;
                pseudocodeEd.Margins[0].Width = TextRenderer.MeasureText(lines.ToString(), pseudocodeEd.Font).Width;
            }
            CompileTime = 0;
            AutoCompile.Enabled = true;
            lastLine = -1;
            lista_executii = null;
        }
        int CompileTime = 0;
        private void AutoCompile_Tick(object sender, EventArgs e)
        {
            // if (pseudocodeEd.Focused == true)
            // {
            CompileTime++;
            if (CompileTime == 10)
            {
                //System.Media.SystemSounds.Asterisk.Play();
                SendKeys.Send("{F6}");
                CompileTime = 0;
                AutoCompile.Enabled = false;
            }
            // }
        }

        private void Form1_TextChanged(object sender, EventArgs e)
        {
            this.MdiParent.Text = this.Text + " - " + AppName;
        }

        private void addBreakpointsOnAllLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var list = pseudocodeEd.Markers.GetMarkers(pseudocodeEd.Lines.Current);
            bool ok = false;
            foreach (ScintillaNET.Marker mk in list)
            {
                if (mk.ToString() != "MarkerNumber0") ok = true;
            }
            if (ok == true)
            {
                for (int i = 0; i <= pseudocodeEd.Lines.Count; i++)
                    pseudocodeEd.Markers.DeleteInstance(pseudocodeEd.Lines[i].Number, -1);
            }
            else
            {
                for (int i = 0; i <= pseudocodeEd.Lines.Count; i++)
                    pseudocodeEd.Markers.AddInstanceSet(pseudocodeEd.Lines[i].Number, 23);
            }
        }

        private void pseudocodeEd_AutoCompleteAccepted(object sender, ScintillaNET.AutoCompleteAcceptedEventArgs e)
        {
            var pos = pseudocodeEd.NativeInterface.GetCurrentPos();

            var word = e.Text;

            if (word == IfKeyword)
            {
                pseudocodeEd.Text = pseudocodeEd.Text.Substring(0, pos) + " " + ThenKeyword + "\n" + EndIfKeyword + pseudocodeEd.Text.Substring(pos, pseudocodeEd.Text.Length - pos);
                pseudocodeEd.GoTo.Position(pos);
            }
            if (word == WhileKeyword)
            {
                pseudocodeEd.Text = pseudocodeEd.Text.Substring(0, pos) + " " + DoKeyword + "\n" + EndWhileKeyword + pseudocodeEd.Text.Substring(pos, pseudocodeEd.Text.Length - pos);
                pseudocodeEd.GoTo.Position(pos);
            }
            if (word == ForKeyword)
            {
                pseudocodeEd.Text = pseudocodeEd.Text.Substring(0, pos) + " " + DoKeyword + "\n" + EndForKeyword + pseudocodeEd.Text.Substring(pos, pseudocodeEd.Text.Length - pos);
                pseudocodeEd.GoTo.Position(pos);
            }
        }

        private void semiTransparentPanel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                VisualBarMouseDownLocation = e.Location;
            }
        }
        int BegunScroll = 1;
        private void semiTransparentPanel1_MouseMove(object sender, MouseEventArgs e)
        {
            MainForm mf = this.MdiParent as MainForm;
            mf.time = 0;
            mf.Loop.Enabled = true;
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                semiTransparentPanel1.UpdateUI();
                int was = semiTransparentPanel1.Top;
                semiTransparentPanel1.Top = e.Y + semiTransparentPanel1.Top - VisualBarMouseDownLocation.Y;
                if (semiTransparentPanel1.Top < VisualScrollBar.Top) semiTransparentPanel1.Top = VisualScrollBar.Top;
                if (semiTransparentPanel1.Top + semiTransparentPanel1.Height > VisualScrollBar.Height) semiTransparentPanel1.Top = VisualScrollBar.Height -semiTransparentPanel1.Height;
                if (semiTransparentPanel1.Top + semiTransparentPanel1.Height > semiTransparentPanel1.Parent.Height) semiTransparentPanel1.Top = semiTransparentPanel1.Parent.Height - semiTransparentPanel1.Height;
                if (VisualSBPic.Dock == DockStyle.Fill)
                {
                    pseudocodeEd.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
                    pseudocodeEd.Scrolling.ScrollToLine((int)Math.Round((double)semiTransparentPanel1.Top / ((double)VisualSBPic.Parent.Height / pseudocodeEd.Lines.Count)));
                }
                if (VisualSBPic.Dock == DockStyle.None)
                {
                    pseudocodeEd.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
                    pseudocodeEd.Scrolling.ScrollToLine((int)Math.Round((double)semiTransparentPanel1.Top / VisualScrollBar.Lines.Current.Height));
                }
                if (semiTransparentPanel1.Top < 0) semiTransparentPanel1.Top = was;
                semiTransparentPanel1.UpdateUI();
            }
         /*   if (e.Button == System.Windows.Forms.MouseButtons.Left && pseudocodeEd.Lines.Count > pseudocodeEd.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN))
            {
                BegunScroll = 0;
                semiTransparentPanel1.UpdateUI();
                semiTransparentPanel1.Top = e.Y + semiTransparentPanel1.Top - VisualBarMouseDownLocation.Y;
                int linieCurenta = (int)(semiTransparentPanel1.Top / 6);
                pseudocodeEd.Scrolling.ScrollToLine(linieCurenta + semiTransparentPanel1.Height * 6);
                if (VisualScrollBar.Lines.Count > VisualScrollBar.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN))
                {
                    pseudocodeEd.Scrolling.ScrollToLine(semiTransparentPanel1.Top * pseudocodeEd.Lines.Count / VisualScrollBar.Height);
                    VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
                    VisualScrollBar.Scrolling.ScrollToLine(pseudocodeEd.Lines.Current.Number - (semiTransparentPanel1.Top / 6));
                    semiTransparentPanel1.UpdateUI();
                    if (semiTransparentPanel1.Top + semiTransparentPanel1.Height > VisualScrollBar.Height)
                    {
                        semiTransparentPanel1.Top = VisualScrollBar.Height - semiTransparentPanel1.Height;
                        semiTransparentPanel1.UpdateUI();
                    }
                    if (semiTransparentPanel1.Top < 0)
                    {
                        semiTransparentPanel1.Top = 0;
                        semiTransparentPanel1.UpdateUI();
                    }
                    VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
                    VisualScrollBar.Scrolling.ScrollToLine(pseudocodeEd.Lines.Current.Number - (semiTransparentPanel1.Top / 6));

                }
                else
                {
                    int x = (VisualScrollBar.Lines.Count * VisualScrollBar.Height);

                    int y = VisualScrollBar.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN);
                    x = x / y;
                    Debug.WriteLine(x);
                    pseudocodeEd.Scrolling.ScrollToLine(semiTransparentPanel1.Top * pseudocodeEd.Lines.Count / x);
                    VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
                    VisualScrollBar.Scrolling.ScrollToLine(pseudocodeEd.Lines.Current.Number - (semiTransparentPanel1.Top / 6));
                    semiTransparentPanel1.UpdateUI();
                    if (semiTransparentPanel1.Top + semiTransparentPanel1.Height > x)
                    {
                        semiTransparentPanel1.Top = x - semiTransparentPanel1.Height;
                        semiTransparentPanel1.UpdateUI();
                    }
                    if (semiTransparentPanel1.Top < 0)
                    {
                        semiTransparentPanel1.Top = 0;
                        semiTransparentPanel1.UpdateUI();
                    }
                    VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
                    VisualScrollBar.Scrolling.ScrollToLine(pseudocodeEd.Lines.Current.Number - (semiTransparentPanel1.Top / 6));

                }
            }*/
        }

        private void pseudocodeEd_Scroll(object sender, ScrollEventArgs e)
        {

           /* MessageBox.Show("scrolling");
            if (BegunScroll == 0 && pseudocodeEd.Lines.Count > pseudocodeEd.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN))
            {
                //BegunScroll = 0;
                semiTransparentPanel1.UpdateUI();
                semiTransparentPanel1.Top = (VisualScrollBar.Height / pseudocodeEd.Lines.Count) * pseudocodeEd.NativeInterface.SendMessageDirect(SCI_GETFIRSTVISIBLELINE);
                //VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
                //VisualScrollBar.Scrolling.ScrollToLine(pseudocodeEd.Lines.Current.Number - (semiTransparentPanel1.Top / 6));
                //int linieCurenta = (int)(semiTransparentPanel1.Top / 6);
                //pseudocodeEd.Scrolling.ScrollToLine(linieCurenta + semiTransparentPanel1.Height * 6);
                if (VisualScrollBar.Lines.Count > VisualScrollBar.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN))
                {
                    pseudocodeEd.Scrolling.ScrollToLine(semiTransparentPanel1.Top * pseudocodeEd.Lines.Count / VisualScrollBar.Height);
                    VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
                    VisualScrollBar.Scrolling.ScrollToLine(pseudocodeEd.Lines.Current.Number - (semiTransparentPanel1.Top / 6));
                    semiTransparentPanel1.UpdateUI();
                    if (semiTransparentPanel1.Top + semiTransparentPanel1.Height > VisualScrollBar.Height)
                    {
                        semiTransparentPanel1.Top = VisualScrollBar.Height - semiTransparentPanel1.Height;
                        semiTransparentPanel1.UpdateUI();
                    }
                    if (semiTransparentPanel1.Top < 0)
                    {
                        semiTransparentPanel1.Top = 0;
                        semiTransparentPanel1.UpdateUI();
                    }
                }
                else
                {
                    int x = (VisualScrollBar.Lines.Count * VisualScrollBar.Height);

                    int y = VisualScrollBar.NativeInterface.SendMessageDirect(SCI_LINEONSCREEN);
                    x = x / y;

                    pseudocodeEd.Scrolling.ScrollToLine(semiTransparentPanel1.Top * pseudocodeEd.Lines.Count / x);
                    VisualScrollBar.Scrolling.ScrollToLine(VisualScrollBar.Lines.Count);
                    VisualScrollBar.Scrolling.ScrollToLine(pseudocodeEd.Lines.Current.Number - (semiTransparentPanel1.Top / 6));
                    semiTransparentPanel1.UpdateUI();
                    if (semiTransparentPanel1.Top + semiTransparentPanel1.Height > x)
                    {
                        semiTransparentPanel1.Top = x - semiTransparentPanel1.Height;
                        semiTransparentPanel1.UpdateUI();
                    }
                    if (semiTransparentPanel1.Top < 0)
                    {
                        semiTransparentPanel1.Top = 0;
                        semiTransparentPanel1.UpdateUI();
                    }
                }
            }*/

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public void WhatsThisF(string key)
        {
            string htmlHeader = "<html><style>html, body { overflow: auto; } a:link {color:#FFFFFF;} a:visited {color:#FFFFFF;} a:hover {color:#B3B3BC;} a:active {color:#0000FF;} div {text-align:justify; text-justify:inter-word;}</style><body bgcolor=Black><div><font face=\"Segoe UI\" color=White>";
            string htmlFooter = "<br><br><font size = 2>Apăsați F1 acum pentru mai mult ajutor.</font></font></div></body></html>";
            string body = "";
            if (key == IntegerKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>intreg</i></b> definește o nouă variabilă de tip întreg în cadrul pseudocodului curent. <br>Prototipul este <i>intreg n</i>, unde n este numele sub care variabila va fi recunoscută în pseudocod. Prototipul acceptă și declararea mai multor variabile pe aceeași linie, de exemplu <i>intreg a, b, c</i>. <br>În C++, tipul de dată întreg (int) poate stoca numere cuprinse între -2,147,483,648 și 2,147,483,648.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == DoubleKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>real</i></b> definește o nouă variabilă de tip număr real în cadrul pseudocodului curent. <br>Prototipul este <i>real n</i>, unde n este numele sub care variabila va fi recunoscută în pseudocod. Prototipul acceptă și declararea mai multor variabile pe aceeași linie, de exemplu <i>real a, b, c</i>. <br>În C++, tipul de dată real (double) poate stoca numere cuprinse între 2.2E-308 și 1.8E+308.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == StringKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>sir</i></b> definește o nouă variabilă de tip șir de caractere în cadrul pseudocodului curent. <br>Prototipul este <i>sir n</i>, unde n este numele sub care variabila va fi recunoscută în pseudocod. Prototipul acceptă și declararea mai multor variabile pe aceeași linie, de exemplu <i>sir a, b, c</i>. <br>În C++, tipul de dată sir (string) poate stoca până la 4294967291 caractere.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == ReadKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>citeste</i></b> efectuează citirea de la tastatură a conținutului variabilelor. <br>Prototipul este <i>citeste n</i>, unde n este variabila care va prelua valoarea (în care se va stoca) informația citită. Prototipul acceptă și citirea mai multor variabile una după alta, prin înșiruirea lor pe aceeași linie, de exemplu <i>citeste a, b, c</i>. <br>Citirea nu produce întodeauna erori dacă tipul de date aștepat nu coincide cu cel primit (de exemplu, se așteaptă ca utilizatorul să introducă un număr, citindu-se valoarea într-o variabilă de tip întreg, însă utilizatorul introduce litere), însă poate produce oricum funcționare necorespunzătoare a pseudocodului.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == WriteKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>scrie</i></b> efectuează afișarea pe ecran a conținutului variabilelor. <br>Prototipul este <i>scrie n</i>, unde n este variabila al cărei conținut va fi afișat pe ecran. Prototipul acceptă și afișarea conținutului mai multor variabile una după alta, prin înșiruirea lor pe aceeași linie, de exemplu <i>scrie a, b, c</i>. <br>În C++, atunci când se încearcă afișarea valorii unei variabile care nu a fost inițializată anterior cu o valoare (de exemplu, o variabilă a fost declarată, iar apoi se afișează valoarea acesteia), pe ecran va fi afișată o valoare aleatoare din cadrul memoriei RAM (tehnic, valoarea nu e aleatoare, ci este valoarea rămasă în memorie de la ultima variabilă, de care s-a dispus, care a fost declarată în locul unde este definită acum variabila nosatră).</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == IfKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>daca</i></b> este parte a instrucțiunii condiționale, definind începutul instrucțiunii. Sintaxa este <b>daca <i>conditie</i></b> sau <b>daca <i>conditie</i> atunci</b> unde <i>conditie</i> reprezintă o variabilă a cărei valoare este fie Adevărat, fie Fals, în funcție de validitatea logică a expresiei. Spre exemplu, dacă conditie este 2 > 3, atunci valoarea conditiei va deveni Fals în urma executării codului. Dacă valoarea de adevăr este Adevărat, se execută codul care se află imediat sub linia acestui cuvânt-cheie, până se ajunge la unul din următoarele două cuvinte cheie, aflate pe același nivel față de instrucțiunea daca: altfel sau sfarsit_daca; dacă se întâlnește cuvântul-cheie altfel, se caută cuvântul sfarsit_daca și se continuă executarea codului de dedesuptul acestuia; dacă se întâlnește sfarsit_daca, se continuă executarea codului de dedesuptul acestuia. Dacă valoarea de adevăr este Fals, se face un salt către primul cuvânt-cheie altfel aflat pe același nivel cu daca, aflat înaintea primului cuvânt-cheie sfarsit_daca aflat pe același nivel cu daca. Dacă această situație există, se execută codul de dedesuptul cuvantului-cheie altfel. Dacă situația nu există, se execută codul de dedesuptul cuvântului sfarsit_daca.<br><br>Vizual, instrucțiunile care sunt pe același nivel au același număr de spații (caracterul Space) înaintea lor, în urma identării textului făcută automat de editor.<br>Pentru o mai bună reprezentare a modului în care se comportă instrucțiunea condițională, urmăriți Schema logică. Pentru a comuta la Schema logică, faceți clic sau atingeți opțiunea ”În Schemă Logică” din meniul ”Convertire”.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == ThenKeyword)
            {
                body = "<font size=\"2\">Cuvântul-cheie <b>atunci</b> este un cuvânt opțional ce poate preceda instrucțiunea condițională. Pentru mai multe detalii despre instrucțiunea condițională, folosiți funcția ”Ce este aceasta?” asupra cuvântului-cheie daca.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == ElseKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>altfel</i></b> este parte a instrucțiunii condiționale. Pentru a afișa mai multe detalii despre modul în care funcționează instrucțiunea condițională, executați funcția ”Ce este aceasta?” asupra cuvântului-cheie daca.<br><br>Pentru o mai bună reprezentare a modului în care se comportă instrucțiunea condițională, urmăriți Schema logică. Pentru a comuta la Schema logică, faceți clic sau atingeți opțiunea ”În Schemă Logică” din meniul ”Convertire”.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == EndIfKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>sfarsit_daca</i></b> este parte a instrucțiunii condiționale. Pentru a afișa mai multe detalii despre modul în care funcționează instrucțiunea condițională, executați funcția ”Ce este aceasta?” asupra cuvântului-cheie daca.<br><br>Pentru o mai bună reprezentare a modului în care se comportă instrucțiunea condițională, urmăriți Schema logică. Pentru a comuta la Schema logică, faceți clic sau atingeți opțiunea ”În Schemă Logică” din meniul ”Convertire”.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == ForKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>pentru</i></b> este parte a instrucțiunii repetitive cu număr cunoscut de iterații. Sintaxa este <b>pentru n=<i>valoare-initiala,valoare-finala</i></b> sau <b>pentru n=<i>valoare-initiala,valoare-finala</i> executa</b>, unde n este numele contorului care va crește cu o unitate la fiecare executare a instrucțiunii, valoare-initiala este valoarea pe care n o are la începutul executării instrucțiunii, iar valoare-finala reprezintă valoarea la care trebuie să ajungă n pentru a se finaliza executarea instrucțiunii. Contorul poate să fie sau nu declarat anterior în pseudocod. Codul aflat între acest cuvânt-cheie și primul cuvânt-cheie sfarsit_pentru aflat pe același nivel cu acest cuvânt-cheie va fi executat până când variabila contor va fi egală cu valoare-finala, valoarea acestuia crescând cu o unitate în urma fiecărei executări a codului.<br><br>Vizual, instrucțiunile care sunt pe același nivel au același număr de spații (caracterul Space) înaintea lor, în urma identării textului făcută automat de editor.<br>Pentru o mai bună reprezentare a modului în care se comportă instrucțiunea condițională, urmăriți Schema logică. Pentru a comuta la Schema logică, faceți clic sau atingeți opțiunea ”În Schemă Logică” din meniul ”Convertire”.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == EndForKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>sfarsit_pentru</i></b> este parte a instrucțiunii repetitive cu număr cunoscut de iterații. Pentru a afișa mai multe detalii despre modul în care funcționează instrucțiunea repetitivă cu număr cunoscut de iterații, executați funcția ”Ce este aceasta?” asupra cuvântului-cheie pentru.<br><br>Pentru o mai bună reprezentare a modului în care se comportă instrucțiunea repetitivă cu număr cunoscut de iterații, urmăriți Schema logică. Pentru a comuta la Schema logică, faceți clic sau atingeți opțiunea ”În Schemă Logică” din meniul ”Convertire”.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == WhileKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>cat_timp</i></b> este parte a instrucțiunii repetitive cu număr necunoscut de iterații pozitivă. Sintaxa este <b>cat_timp <i>conditie</i></b> sau <b>cat_timp <i>conditie</i> executa</b>, unde conditie reprezintă o variabilă a cărei valoare este fie Adevărat, fie Fals, în funcție de validitatea logică a expresiei. Spre exemplu, dacă conditie este 2 > 3, atunci valoarea conditiei va deveni Fals în urma executării codului. Dacă valoarea de adevăr este Adevărat, se execută codul care se află imediat sub linia acestui cuvânt-cheie până la întâlnirea primului cuvânt-cheie sfarsit_cat_timp aflat pe același nivel cu cuvântul-cheie cat_timp, după care se face executorul face un salt la linia cuvantului-cheie cat_timp si reia procedura descrisă mai sus. Dacă valoarea de adevăr este Fals, se continuă cu executarea codului aflat dedesuptul primului cuvânt-cheie sfarsit_cat_timp de pe același nivel cu cuvântul-cheie cat_timp întâlnit.<br><br>Vizual, instrucțiunile care sunt pe același nivel au același număr de spații (caracterul Space) înaintea lor, în urma identării textului făcută automat de editor.<br>Pentru o mai bună reprezentare a modului în care se comportă instrucțiunea repetitivă cu număr necunoscut de iterații pozitivă, urmăriți Schema logică. Pentru a comuta la Schema logică, faceți clic sau atingeți opțiunea ”În Schemă Logică” din meniul ”Convertire”.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == EndWhileKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>sfarsit_cat_timp</i></b> este parte a instrucțiunii repetitive cu număr necunoscut de iterații pozitivă. Pentru a afișa mai multe detalii despre modul în care funcționează instrucțiunea repetitivă cu număr necunoscut de iterații pozitivă, executați funcția ”Ce este aceasta?” asupra cuvântului-cheie cat_timp.<br><br>Pentru o mai bună reprezentare a modului în care se comportă instrucțiunea repetitivă cu număr cunoscut de iterații pozitivă, urmăriți Schema logică. Pentru a comuta la Schema logică, faceți clic sau atingeți opțiunea ”În Schemă Logică” din meniul ”Convertire”.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == DoKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>executa</i></b> are două roluri: <br><br>1) este parte a instrucțiunii repetitive cu număr necunoscut de iterații negativă. Sintaxa este <b>executa</b>. După întâlnirea acestui cuvânt-cheie, se execută cosul până ce se întâlnește prima instrucțiune pana_cand (Sintaxa este <b>pana_cand <i>conditie</i></b>) aflată pe același nivel cu instrucțiunea executa. Apoi, se evaluează condiția instrucțiunii pana_cand: daca este falsă, executorul revine la instrucțiunea executa și se reia procedura descrisă mai sus; dacă este adevărată, executorul continuă executarea cosului aflat imediat sub linia instrucțiunii pana_cand identificate.<br><br>Vizual, instrucțiunile care sunt pe același nivel au același număr de spații (caracterul Space) înaintea lor, în urma identării textului făcută automat de editor.<br>Pentru o mai bună reprezentare a modului în care se comportă instrucțiunea repetitivă cu număr necunoscut de iterații negativă, urmăriți Schema logică. Pentru a comuta la Schema logică, faceți clic sau atingeți opțiunea ”În Schemă Logică” din meniul ”Convertire”.<br><br>2) Este parte a instrucțiunilor pentru sau cat_timp. Pentru mai multe informații despre aceste funcții, executați ”Ce este aceasta?” asupra funcțiilor respective.</font";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == LoopUntilKeyword)
            {
                body = "<font size=\"2\"><b>Cuvântul-cheie <i>pana_cand</i></b> este parte a instrucțiunii repetitive cu număr necunoscut de iterații negativă. Pentru a afișa mai multe detalii despre modul în care funcționează instrucțiunea repetitivă cu număr necunoscut de iterații negativă, executați funcția ”Ce este aceasta?” asupra cuvântului-cheie executa.<br><br>Pentru o mai bună reprezentare a modului în care se comportă instrucțiunea repetitivă cu număr cunoscut de iterații negativă, urmăriți Schema logică. Pentru a comuta la Schema logică, faceți clic sau atingeți opțiunea ”În Schemă Logică” din meniul ”Convertire”.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == "=" && pseudocodeEd.Lines.Current.Text.ToCharArray()[GetLineCursorPosition(true, pseudocodeEd) - 2].ToString() != "<" && pseudocodeEd.Lines.Current.Text.ToCharArray()[GetLineCursorPosition(true, pseudocodeEd) - 1].ToString() != ">" && pseudocodeEd.Lines.Current.Text.ToCharArray()[GetLineCursorPosition(true, pseudocodeEd) - 2].ToString() != "!")
            {
                body = "<font size=\"2\"><b>Operatorul =</b> poate fi folosit în 2 scopuri<br><br>1) Atribuire (de exemplu, a = b) - îi atribuie lui a valoarea lui b. Este necesar ca toate variabilele utilizate în cadrul atribuirii să fie de același tip.<br>2) Verificarea unei condiții (de exemplu, 2 > 3 = Adevarat) - atribuie expresiei valoarea Adevarat daca toate propozitiile din cadrul expresiei au ca valoare de adevar Adevarat sau valoarea Fals în orcie alt caz.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == "<" && pseudocodeEd.Lines.Current.Text.ToCharArray()[GetLineCursorPosition(true, pseudocodeEd)].ToString() != "=")
            {
                body = "<font size=\"2\"><b>Operatorul <</b> compară valorile din dreapta respectiv stânga lui și acordă expresiei valoarea Adevărat dacă valoarea din stânga este mai mică decât cea din dreapta; în caz contrar, atribuie expresiei valoarea Fals.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == ">" && pseudocodeEd.Lines.Current.Text.ToCharArray()[GetLineCursorPosition(true, pseudocodeEd)].ToString() != "=")
            {
                body = "<font size=\"2\"><b>Operatorul ></b> compară valorile din dreapta respectiv stânga lui și acordă expresiei valoarea Adevărat dacă valoarea din stânga este mai mare decât cea din dreapta; în caz contrar, atribuie expresiei valoarea Fals.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == "<" && pseudocodeEd.Lines.Current.Text.ToCharArray()[GetLineCursorPosition(true, pseudocodeEd)].ToString() == "=")
            {
                body = "<font size=\"2\"><b>Operatorul <=</b> compară valorile din dreapta respectiv stânga lui și acordă expresiei valoarea Adevărat dacă valoarea din stânga este mai mică sau egală față de cea din dreapta; în caz contrar, atribuie expresiei valoarea Fals.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == ">" && pseudocodeEd.Lines.Current.Text.ToCharArray()[GetLineCursorPosition(true, pseudocodeEd)].ToString() == "=")
            {
                body = "<font size=\"2\"><b>Operatorul >=</b> compară valorile din dreapta respectiv stânga lui și acordă expresiei valoarea Adevărat dacă valoarea din stânga este mai mare sau egală față de cea din dreapta; în caz contrar, atribuie expresiei valoarea Fals.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == "!" && pseudocodeEd.Lines.Current.Text.ToCharArray()[GetLineCursorPosition(true, pseudocodeEd)].ToString() != "=" || key == "diferit")
            {
                body = "<font size=\"2\"><b>Operatorul != (diferit)</b> verifică o condiție (de exemplu, 2 != 3) - atribuie expresiei valoarea Adevarat daca valorile din dreapta și stânga operatorului sunt diferite sau Fals dacă valorile sunt egale.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == "%" || key == "mod")
            {
                body = "<font size=\"2\"><b>Operatorul % (mod)</b> se folosește pentru a afla restul unei împărțiri, De exemplu, valoarea expresiei 3 % 2 este 1, deoarece restul împărțirii numărului 3 la 2 este 1.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == "Adevarat" || key == "adevarat")
            {
                body = "<font size=\"2\"><b>Operatorul logic Adevarat</b></font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == "Fals" || key == "fals")
            {
                body = "<font size=\"2\"><b>Operatorul logic Fals</b></font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == "[" || key == "]")
            {
                body = "<font size=\"2\"><b>Operatorii [ și ]</b> se folosesc pentru a extrage partea întreagă dintr-un număr. De exemplu, [2,3] este egală cu 2.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == "\"")
            {
                body = "<font size=\"2\"><b>Ghilimelele</b> se folosesc pentru a delimita un șir de caractere. De exmplu, pentru a scrie pe ecran textul ”Hello world”, folosiți ghilimelele în instrucțiunea scrie astfel: scrie \"Hello world\"</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96)));
            }
            if (key == "command-bar")
            {
                body = "<font size=\"2\">Aceasta este <b>bara de comenzi</b>. Aceasta conține comenzile din meniuri cel mai des utilizate, separate în grupuri în funcție de specificul acțiunii fiecărei opțiuni. De exemplu, toate comenzile care lucrează cu fișierul pseudocod sunt separate într-un grup față de comenzile care lucrează cu textul pseudocodului.<br><br>S-ar putea ca dvs. să folosiți ordinea implicită a funcțiilor în bara de comenzi, așa cum a fost gândită de inginerii ValiNet. Totuși, dacă dvs. preferați alte comenzi sau doriți să eliminați comenzi din bara de comenzi, puteți face acest lucru prin folosirea opțiunii ”Particularizare bară de comenzi” din meniul ”Unelte”.<br><br>Dacă nu aveți nevoie deloc de bara de comenzi, puteți face ca aceasta să nu mai ocupe spațiu inutil pe ecran: mergeți în meniul ”Vizualizare” și faceți clic sau atingeți ”Bara de comenzi” pentru a o ascunde.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == "menu-bar")
            {
                body = "<font size=\"2\">Aceasta este <b>bara de meniuri</b>. Aceasta conține toate funcțiile disponibile în aplicație, grupate în categorii, în funcție de rolul acestora în aplicație.<br><br>Puteți folosi tasta Alt pentru a accesa rapid meniurile astfel: apăsați tasta Alt, iar apoi apăsați litera subliniată în numele fiecărui meniu pentru a deschide respectivul meniu. Apoi, folosiți tastele săgeți pentru a naviga prin meniuri.<br><br>Puteți căuta în meniuri o funcție, dacă nu vă mai amintiți numele acesteia, folosind Bara Căutare rapidă în meniuri. Apăsați Ctrl+Q pentru a o evidenția acum. Pentru a afla mai multe detalii despre aceasta, executați funcția ”Ce este aceasta?” asupra bării de căutare rapidă.<br><br>Pentru a obține mai multe opțiuni de asistență, precum și informații suplimentare despre această versiune a aplicației, consultați meniul Asistență.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == "search-bar")
            {
                body = "<font size=\"2\">Aceasta este <b>bara de Căutare rapidă în meniuri</b>. Folosiți această caracteristică pentru a căuta rapid o comandă în cadrl meniurilor, astfel: începeți să tastați o parte din numele comenzii. În lista afișată, vor apărea toate comenzile din meniuri ce conțin caracterele tastate, în ordinea tastată. Faceți clic sau atingeți comanda dorită pentru a o accesa. De asemenea, puteți memora ușor locul comenzii în meniuri pentru utilizarea ulterioară, deoarece în rezultatele căutării numele comenzii este însoțit și de calea către acesta în cadrul meniurilor.<br><br>Puteți apăsa Ctr+Q pentru a comuta introducerea de caractere la bara de Căutare rapidă.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == "tab-bar")
            {
                body = "<font size=\"2\">Aceasta este <b>bara filelor</b>. Folosiți această bară pentru a comuta rapid între fișierele pseudocod deschise în aplicație.<br><br>Pentru a comuta la un fișier pseudocod deschis în aplicație, faceți clic sau atingeți numele acestuia.<br><br>Pentru a închide un fișier pseudocod din aplicație (și a-i elimina fila din bara de file), faceți clic sau atingeți meniul ”Fișier” și alegeți ”Închidere fișier” sau apăsați Ctrl+W.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == "status-bar")
            {
                body = "<font size=\"2\">Aceasta este <b>bara de stare</b>. Faceți clic sau atingeți pe numărul liniei și al poziției curente pentru a vedea statistici avansate legate de semantica fișierului pseudocod.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == "converted-code")
            {
                body = "<font size=\"2\">Acesta este <b>codul convertit</b>. Acesta reprezintă transcrierea pseudocodului dvs. într-unul din limbajele de programare disponibile pentru convertire: C++, C#, Basic Classic, Pascal sau JavaScript.<br><br>Puteți alege în ce limbaj să se convertească codul făcând clic sau atingere pe maniul ”Convertire” și alegerea unuia dintre limbajele de programare afișate în listă.<br><br>Codul convertit se derulează automat odată ce dvs. derulați pseudocodul în cadrul editorului.<br><br>Pentru a exporta codul convertit într-un fișier specific unui limbaj de programare, faceți clic sau atingeți meniul ”Fișier”, alegeți ”Exportare cod” și mai departe, alegeți un format de fișier spre care doriți să exportați codul convertit.<br><br>Pentru a imprima codul convertit, faceți clic sau atingere pe ”Fișier” și alegeți ”Imprimare cod convertit”.<br><br>Puteți alege să convertiți în Schemă logică pseudocodul, în locul unui limbaj de programare. Pentru a converti la Schemă logică, faceți clic sau atingeți meniul ”Convertire” și alegeți ”În Schemă Logică”.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == "logic-schema")
            {
                body = "<font size=\"2\">Aceasta este <b>schema logică</b>. Această reprezentare grafică evidențiază concret modul de operare al pseudocodului dvs. arătându-vă traseele posibile ale executorului în cadrul rulării în funcție de valorile variabilelor.<br><br>Linia curentă din pseudocod este evidențiată distinctiv, cu culoarea portocaliu, în cadrul schemei logice.<br><br>Pentru a salva schema logică într-o imagine pe disc, faceți clic pe ”Fișier” și alegeți ”Salvare Schemă Logică”.<br><br>Pentru a imprima schema logică, faceți clic sau atingeți ”Fișier” și alegeți ”Imprimare Schemă Logică”.<br><br>Puteți muta diagrama schemei logice în cadrul zonei rezervate acesteia. Pentru aceasta, faceți clic sau atingeți și glisați diagrama schemei logice.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96)));
            }
            if (key == "error-list")
            {
                body = "<font size=\"2\">Aceasta este <b>lista erorilor</b>. Această listă afișează eventualele erori pe care le-ați făcut atunci când ați scris pseudocod. Dacă greșiți în cadrul editorului, la scurt timp după producerea greșelii, una sau mai multe erori (traduse sau nu în limba română), vor apărea în lista de erori. Faceți dublu clic sau atingeți de două ori rapid pentru a face un salt la linia erorii și a încerca să corectați eroarea.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96))); 
            }
            if (key == "debug-console")
            {
                body = "<font size=\"2\">Aceasta este <b>consola de debugging</b>. În timp ce faceți debugging, mesajele primite de la procesul compilatorului sunt afișate în această fereastră.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96))); 
            }
            if (key == "debug-line")
            {
                body = "<font size=\"2\">Aceasta este <b>linia de comandă în timpul debuggingului</b>. Atunci când a fost lovit un breakpoint, puteți introduce text în această casetă și apoi apăsați tasta Enter sau butonul Trimitere comandă pentru a trimite comanda tastată la procesul compilatorului. Urmăriți efectele comenzii dvs. în consola de debugging.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96))); 
            }
            if (key == "watch-list")
            {
                body = "<font size=\"2\">Aceasta este <b>lista de watchuri</b>. În această listă sunt afișate variabilele asupra cărora ați setat watchuri, iar când un breakpoint este atins, în timpul debuggingului, lista este populată automat cu valorile variabilelor.</font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(330 * ((float)dx / 96))); 
            }
            queryVar = key;
            Compile(pseudocodeEd.Text, "C#");
            if (queryVar == "int")
            {
                body = "<font size=\"2\"><b>Aceasta este o variabilă de tip întreg.</b> Pentru mai multe informații despre tipul de date întreg, executați funcția ”Ce este aceasta?” asupra cuvântului-cheie intreg.<br><br>Puteți face un salt la linia unde această variabilă este definită prin folosirea opțiunii ”Salt la definiție” din meniul contextual al editorului, sau prin apăsarea tastei F12.<br>Puteți redenumi această variabilă (schimbă numele acesteia în tot pseudocodul) prin folosirea opțiunii ”Redenumire variabilă” din meniul contextual al editorului, sau prin apăsarea tastei F4.<br>Puteți urmări valoarea acestei variabile în timpul debuggingului, pe măsură ce breakpointurile sunt lovite, prin punerea unui watch asupra acesteia. Pentru a pune un watch pe această variabilă, folosiți opțiunea ”Adăugare watch asupra acestei variabile” din meniul contextual al editorului, sau prin apăsarea tastei F9. </font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96))); 
            }
            if (queryVar == "double")
            {
                body = "<font size=\"2\"><b>Aceasta este o variabilă de tip număr real.</b> Pentru mai multe informații despre tipul de date număr real, executați funcția ”Ce este aceasta?” asupra cuvântului-cheie real.<br><br>Puteți face un salt la linia unde această variabilă este definită prin folosirea opțiunii ”Salt la definiție” din meniul contextual al editorului, sau prin apăsarea tastei F12.<br>Puteți redenumi această variabilă (schimbă numele acesteia în tot pseudocodul) prin folosirea opțiunii ”Redenumire variabilă” din meniul contextual al editorului, sau prin apăsarea tastei F4.<br>Puteți urmări valoarea acestei variabile în timpul debuggingului, pe măsură ce breakpointurile sunt lovite, prin punerea unui watch asupra acesteia. Pentru a pune un watch pe această variabilă, folosiți opțiunea ”Adăugare watch asupra acestei variabile” din meniul contextual al editorului, sau prin apăsarea tastei F9. </font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96))); 
            }
            if (queryVar == "string")
            {
                body = "<font size=\"2\"><b>Aceasta este o variabilă de tip șir de caractere.</b> Pentru mai multe informații despre tipul de date șir de caractere, executați funcția ”Ce este aceasta?” asupra cuvântului-cheie sir.<br><br>Puteți face un salt la linia unde această variabilă este definită prin folosirea opțiunii ”Salt la definiție” din meniul contextual al editorului, sau prin apăsarea tastei F12.<br>Puteți redenumi această variabilă (schimbă numele acesteia în tot pseudocodul) prin folosirea opțiunii ”Redenumire variabilă” din meniul contextual al editorului, sau prin apăsarea tastei F4.<br>Puteți urmări valoarea acestei variabile în timpul debuggingului, pe măsură ce breakpointurile sunt lovite, prin punerea unui watch asupra acesteia. Pentru a pune un watch pe această variabilă, folosiți opțiunea ”Adăugare watch asupra acestei variabile” din meniul contextual al editorului, sau prin apăsarea tastei F9. </font>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96))); 
            }

            queryVar = "";
            if (body != "")
            {
                webBrowser2.DocumentText = htmlHeader + body + htmlFooter;
                WhatsThis.Visible = true;
                WhatsThis.BringToFront();
                var relativePoint = this.PointToClient(Cursor.Position);
                if (relativePoint.Y < 0) relativePoint.Y = 0;
                if (relativePoint.Y + WhatsThis.Height > this.Height) relativePoint.Y = this.Height - WhatsThis.Height - 100;
                if (relativePoint.X + WhatsThis.Width > this.Width) relativePoint.X = this.Width - WhatsThis.Width - 100;
                WhatsThis.Location = relativePoint;
                webBrowser2.Focus();
            }
            else
            {
                webBrowser2.DocumentText = htmlHeader + "<font size=\"2\"><b>Nu am găsit ceea ce căutați.</b><br><br>Funcția ”Ce este aceasta?” nu a găsi nimic special în legătură cu obiectul ales de dvs. <br><br>Puteți încerca să găsiți mai mult ajutor folosind metode alternative de asistență. Pentru a vedea o listă a acestor metode, apăsați tasta F1 acum.</font></font></div></body></html>";
                WhatsThis.Size = new Size((int)(300 * ((float)dx / 96)), (int)(280 * ((float)dx / 96))); 
                WhatsThis.Visible = true;
                WhatsThis.BringToFront();
                var relativePoint = this.PointToClient(Cursor.Position);
                if (relativePoint.Y < 0) relativePoint.Y = 0;
                if (relativePoint.Y + WhatsThis.Height > this.Height) relativePoint.Y = this.Height - WhatsThis.Height - 100;
                if (relativePoint.X + WhatsThis.Width > this.Width) relativePoint.X = this.Width - WhatsThis.Width - 100;
                WhatsThis.Location = relativePoint;
                webBrowser2.Focus();
            }

        }
        private void ceEsteAceastaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pos = pseudocodeEd.NativeInterface.GetCurrentPos();
            string key = pseudocodeEd.GetWordFromPosition(pos);
            int l = GetLineCursorPosition(true, pseudocodeEd) - 1;
            try
            {
                if (key == "") key = pseudocodeEd.Lines.Current.Text.ToCharArray()[l].ToString();
            }
            catch {
                try
                {
                    if (key == "") key = pseudocodeEd.Lines.Current.Text.ToCharArray()[GetLineCursorPosition(true, pseudocodeEd) - 2].ToString();
                }
                catch { }
            }
            WhatsThisF(key);
        }

        private void webBrowser2_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            //MessageBox.Show(e.Url.ToString());
            if (e.Url.ToString() == "about:close();")
            {
                WhatsThis.Visible = false;
            }
        }

        private void WhatsThis_Leave(object sender, EventArgs e)
        {
            WhatsThis.Visible = false;
        }

        private void whatsThisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Try to cast the sender to a ToolStripItem
            ToolStripItem menuItem = sender as ToolStripItem;
            if (menuItem != null)
            {
                // Retrieve the ContextMenuStrip that owns this ToolStripItem
                ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
                if (owner != null)
                {
                    // Get the control that is displaying this context menu
                    Control sourceControl = owner.SourceControl;
                    if (sourceControl.Name == "statusBar1")
                    {
                        this.WhatsThisF("status-bar");
                    }
                    if (sourceControl.Name == "converterEd")
                    {
                        this.WhatsThisF("converted-code");
                    }
                    if (sourceControl.Name == "SchemaLogicaCont" || sourceControl.Name == "SchemaLogica")
                    {
                        this.WhatsThisF("logic-schema");
                    }
                    if (sourceControl.Name == "dataGridView1")
                    {
                        this.WhatsThisF("error-list");
                    }
                    if (sourceControl.Name == "richTextBox1")
                    {
                        this.WhatsThisF("debug-console");
                    }
                    if (sourceControl.Name == "textBox6")
                    {
                        this.WhatsThisF("debug-line");
                    }
                    if (sourceControl.Name == "listBox3")
                    {
                        this.WhatsThisF("watch-list");
                    }
                }
            }
        }

        private void whatsThisToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form1 frm = ActiveMdiChild as Form1;
            frm.WhatsThisF("watch-list");
        }

        private void button72_Click(object sender, EventArgs e)
        {
            WhatsThis.Visible = false;
        }
        private Point MouseDownLocationWhats;

        private void label20_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocationWhats = e.Location;
            }
        }

        private void label20_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                WhatsThis.Left = e.X + WhatsThis.Left - MouseDownLocationWhats.X;
                WhatsThis.Top = e.Y + WhatsThis.Top - MouseDownLocationWhats.Y;
            }
        }

        private void button73_Click(object sender, EventArgs e)
        {
            try
            {
                string a = Microsoft.VisualBasic.Interaction.InputBox(NewValue, AppName, listBox4.SelectedItem.ToString().Trim());
                processPSC3.StandardInput.WriteLine("set variable " + listBox3.SelectedItem.ToString() + " = " + a, Color.Yellow, true);
                listBox4.Items[listBox4.SelectedIndex] = GetLeadingWhitespace(listBox4.SelectedItem.ToString()) + a;
                ReturnInput = true;

            }
            catch
            {
                ReturnInput = false;

            }
        }

        private void pseudocodeEd_SelectionChanged(object sender, EventArgs e)
        {
            MainForm mf = this.MdiParent as MainForm;
            mf.time = 0;
            mf.Loop.Enabled = true;
        }

        public void Coverage()
        {
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
            cmdStartInfo.Arguments = "-g --coverage -c \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp\"" + " -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.o" + "\"";
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            Process process = new Process();
            process.StartInfo = cmdStartInfo;
            process.Start();
            process.WaitForExit();
            ProcessStartInfo cmdStartInfo2 = new ProcessStartInfo();
            cmdStartInfo2.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
            cmdStartInfo2.Arguments = "--coverage -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe\"" + " \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.o" + "\"";
            cmdStartInfo2.UseShellExecute = false;
            cmdStartInfo2.CreateNoWindow = true;
            Process process2 = new Process();
            process2.StartInfo = cmdStartInfo2;
            process2.Start();
            process2.WaitForExit();
            ProcessStartInfo cmdStartInfo3 = new ProcessStartInfo();
            cmdStartInfo3.FileName = Application.StartupPath + @"\cb_console_runner.exe";
            cmdStartInfo3.Arguments = "\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe\"";
            Process process3 = new Process();
            process3.StartInfo = cmdStartInfo3;
            process3.Start();
            process3.WaitForExit();
            ProcessStartInfo cmdStartInfo4 = new ProcessStartInfo();
            cmdStartInfo4.FileName = "cmd.exe";
            cmdStartInfo4.WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode";
            cmdStartInfo4.RedirectStandardInput = true;
            cmdStartInfo4.UseShellExecute = false;
            cmdStartInfo4.CreateNoWindow = true;
            Process process4 = new Process();
            process4.StartInfo = cmdStartInfo4;
            process4.Start();
            process4.StandardInput.WriteLine("\"" + Properties.Settings.Default.MinGWPath + "\\bin\\gcov.exe\" " + "\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp" + "\"");
            process4.StandardInput.WriteLine("exit");
            process4.WaitForExit();
        }

        public void pseudocodeEd_ZoomFactorChanged(object sender, EventArgs e)
        {
            ZoomFactorChanged(pseudocodeEd.ZoomFactor);
        }
        public void trackBarScroll()
        {
            if (toolStripTrackBar1.Value >= 0) ZoomFactorChanged(toolStripTrackBar1.Value);
            else
            {
                if (toolStripTrackBar1.Value % 2 == 0) ZoomFactorChanged(toolStripTrackBar1.Value / 2);
                else
                {
                    ZoomFactorChanged((toolStripTrackBar1.Value - 1) / 2);
                    toolStripTrackBar1.Scroll -= toolStripTrackBar1_Scroll;
                    toolStripTrackBar1.Value = toolStripTrackBar1.Value - 1;
                    toolStripTrackBar1.Scroll += toolStripTrackBar1_Scroll;
                }
            }
        }
        public void toolStripTrackBar1_Scroll(object sender, EventArgs e)
        {
            trackBarScroll();
        }

        public void ZoomFactorChanged(int x)
        {
            MainForm mf = this.MdiParent as MainForm;

            try
            {
                foreach (Form frmt in mf.MdiChildren)
                {
                    if (frmt is Form1)
                    {
                        Form1 frmx = (Form1)frmt;
                        frmx.pseudocodeEd.ZoomFactorChanged -= frmx.pseudocodeEd_ZoomFactorChanged;
                    }
                }
                //int x = pseudocodeEd.ZoomFactor;
                Properties.Settings.Default.Zoom = x;
                foreach (Form frmt in mf.MdiChildren)
                {
                    if (frmt is Form1)
                    {
                        Form1 frmx = (Form1)frmt;
                        frmx.pseudocodeEd.ZoomFactor = x;
                        frmx.converterEd.ZoomFactor = x;
                    }
                }
                mf.zoom25.Checked = false;
                mf.zoom50.Checked = false;
                mf.zoom75.Checked = false;
                mf.zoom100.Checked = false;
                mf.zoom200.Checked = false;
                mf.zoom300.Checked = false;
                mf.zoom400.Checked = false;
                mf.customizeToolStripMenuItem.Checked = true;
                if (x == -3)
                {
                    mf.zoom25.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == -2)
                {
                    mf.zoom50.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == -1)
                {
                    mf.zoom75.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == 0)
                {
                    mf.zoom100.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == 1)
                {
                    mf.zoom200.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == 2)
                {
                    mf.zoom300.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == 3)
                {
                    mf.zoom400.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
            }
            catch { }
            foreach (Form frmt in mf.MdiChildren)
                if (frmt is Form1)
                {

                    Form1 frmx = (Form1)frmt;
                    frmx.pseudocodeEd.ZoomFactorChanged += frmx.pseudocodeEd_ZoomFactorChanged;
                }
               
        }

        private void toolStripStatusLabel3_Click(object sender, EventArgs e)
        {
            try
            {
                toolStripTrackBar1.Value -= 1;
                trackBarScroll();
            }
            catch { }
        }

        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {
            try
            {
                toolStripTrackBar1.Value += 1;
                trackBarScroll();
            }
            catch { }
        }
        int pozitieNoua = 0;
        private void EnterTimer_Tick(object sender, EventArgs e)
        {
            pseudocodeEd.GoTo.Line(pozitieNoua);
            pseudocodeEd.Caret.Goto(pseudocodeEd.Caret.Position + GetLeadingWhitespaceAsInt(pseudocodeEd.Lines.Current.Text));
            EnterTimer.Enabled = false;
        }
        /*   protected override void OnPaintBackground(PaintEventArgs e)
           {
               base.OnPaintBackground(e);
               Rectangle lasttabrect = tabControl3.GetTabRect(tabControl3.TabPages.Count - 1);
               RectangleF emptyspacerect = new RectangleF(
                       lasttabrect.X + lasttabrect.Width + tabControl3.Left,
                       tabControl3.Top + lasttabrect.Y,
                       tabControl3.Width - (lasttabrect.X + lasttabrect.Width),
                       lasttabrect.Height);

               Brush b = new SolidBrush(Color.FromArgb(64,64,64)); // the color you want
               e.Graphics.FillRectangle(b, emptyspacerect);
           }*/
        /*for (int i = 1; i <= xMap; i++)
            for (int j = 1; j <= yMap; j++)
            {
                Map[i, j] = 0;
            }
         xMap = 1;
         yMap = 1;
         xTeava = 1;
         yTeava = 1;
         Map[1, 1] = 1;
         string[] textPseudocod = Regex.Split(pseudocodeEd.Text, "\n");
         for (int i = 0; i < textPseudocod.Length; i++)
         {
             if (textPseudocod[i].ToString().TrimStart().StartsWith("daca"))
             {
                 if (xTeava < 2)
                 {
                     TranspunereMatrice(1);
                 }
                 yMap++;
                 yTeava++;
                 Map[xTeava + 1, yTeava] = 9;
                 NumarAltfel++;
                 Altfel[NumarAltfel].x = xTeava + 2;
                 Altfel[NumarAltfel].y = yMap;
                 goto Finish;
             }
             if (textPseudocod[i].ToString().TrimStart().StartsWith("altfel"))
             {
                 xTeava = Altfel[NumarAltfel].x;
                 if(xMap==Altfel[NumarAltfel].x-1) xMap++;
                 yTeava = Altfel[NumarAltfel].y + 1;
                 NumarAltfel--;
                 goto Finish;
             }

             #region Oricefel
             Map[xTeava, ++yTeava] = 3;
             if (yTeava - 1 == yMap) yMap++;
             #endregion
         Finish:
             string v;
         }
     }*/

        //SchemaLogica.Invalidate();
        /*SchemaLogica.Visible = true;
        SchemaLogica.BringToFront();
        string source = pseudocodeEd.Text;
        bool xxx = false;
        source = source + "\r\n";
        textPseudocod = Regex.Split(source, "\n");
        string returnare = "";
        string variabile = "";
        coresp = null;
        coresp = new int[10000];
        estevar = null;
        estevar = new bool[10000];
        string[,] mat = new string[10000, 2];
        int nrcurent = 0;
        nrvar = 0;
        functii = "";
        //EXCLUSIV SCHEMA
        //int top = 10;
        //int left = 80;
        int right = 0;
        Point[] lastif = new Point[1000];
        Point[] lastfor = new Point[1000];
        Point[] lastfor2 = new Point[1000];
        Point[] lastwhile = new Point[1000];
        Point[] lastdo = new Point[1000];
        int lastifnum = 0;
        int lastfornum = 0;
        int lastwhilenum = 0;
        int lastdonum = 0;
        Point[] lastYESif = new Point[1000];
        int lastYESifnum = 0;
        bool wasElse = false;
        //SF EXCLUSIV SCHEMA
        //using (Graphics g = e.Graphics.gr))
        //{

        // Mai jos avem o functie liniara (contrar aparentelor) care cauta cea mai mare intindere a codului
        // pe o ramura afirmativa a lui daca si o atribuie intregului maxim. Vom folosi aceasta valoare pt a deplasa 
        // graficul pe care il desenam.
        int maxim = 0;
        int lastspacing = 0;
        for (int i = 0; i < textPseudocod.Length; i++)
        {
            if (textPseudocod[i].StartsWith("daca"))
            {
                int lat = 1;
                lastspacing = 0;
                for (int j = i; j < textPseudocod.Length; j++)
                {
                    if (textPseudocod[j].TrimStart().StartsWith("daca") || textPseudocod[j].TrimStart().StartsWith("pentru") || textPseudocod[j].TrimStart().StartsWith("cat_timp") || textPseudocod[j].TrimStart().StartsWith("executa"))
                    {
                        if (lastspacing < GetLeadingWhitespaceAsInt(textPseudocod[j]))
                        {
                            lat++;
                            lastspacing = GetLeadingWhitespaceAsInt(textPseudocod[j]);
                        }
                        if (maxim < lat) maxim = lat;
                    }
                    if (textPseudocod[j].StartsWith("sfarsit_daca") || textPseudocod[j].StartsWith("altfel") || textPseudocod[j].StartsWith("sfarsit_pentru") || textPseudocod[j].StartsWith("sfarsit_cat_timp") || textPseudocod[j].StartsWith("pana_cand"))
                    {
                        if (textPseudocod[j].StartsWith("altfel"))
                        {
                            for (int l = j; l < textPseudocod.Length; l++)
                            {
                                if (textPseudocod[l].StartsWith("sfarsit_daca"))
                                {
                                    l = j;
                                    break;
                                }
                            }
                        }
                        i = j;
                        break;
                    }
                }
            }
        }
        // Intregul maxim contine acum ce ne trebuia.
        // Desenam acum cuvantul START, la mijlocul graficului
        // Pt a determina mijlocul, vom folosi formula: Mijloc = Spatiator + Latime-Bloc * maxim + Spatiator * maxim
        // Codificam Mijloc ca un intreg M, latime-bloc ca LM, spatiator ca S
        int M;
        int LM = 200;
        int S = 10;
        int H = 10; // Inaltimea liniutei de unire
        M = S + LM * maxim + S * maxim;
        Debug.WriteLine(maxim);
        int RombDim = 35;
        //Definim 2 puncte
        Point LastPoint1, LastPoint2;
        // Acum desenam cuvantul START pe grafic
        e.Graphics.DrawEllipse(Pens.Black, M, 10, 70, 30);
        e.Graphics.DrawString("START", new Font("Consolas", 10), Brushes.Black, M +15, 18);
        e.Graphics.DrawLine(Pens.Black, M + 35, 40, M + 35, 40 + H); // Liniuta de unire
        LastPoint1 = new Point(M + 35, 40 + H);
        LastPoint2 = new Point(M + 35, 40 + H);
        for (int i = 0; i < textPseudocod.Length; i++)
        {
            if (textPseudocod[i].TrimStart().StartsWith("daca"))
            {
                HartaDaca(i);
                /*if (Nr1 != 0) Nr1 = 2;
                else Nr1 = 1;
                Nr2++;*/
        /*Debug.WriteLine(Nr1 + " " + Nr2);
        Point[] points = { LastPoint1, new Point(LastPoint1.X - RombDim, LastPoint1.Y + RombDim), new Point(LastPoint1.X, LastPoint1.Y + RombDim * 2), new Point(LastPoint1.X + RombDim, LastPoint1.Y + RombDim) };
        e.Graphics.DrawPolygon(Pens.Black, points);
        e.Graphics.DrawString(textPseudocod[i].TrimStart().Replace("daca ", ""), new Font("Consolas", 10), Brushes.Black, new Point(LastPoint1.X - RombDim + 10, LastPoint1.Y + RombDim - 10));
        e.Graphics.DrawLine(Pens.Black, new Point(LastPoint1.X - RombDim, LastPoint1.Y + RombDim), new Point(LastPoint1.X - RombDim - H, LastPoint1.Y + RombDim));
        e.Graphics.DrawLine(Pens.Black, new Point(LastPoint1.X + RombDim, LastPoint1.Y + RombDim), new Point(LastPoint1.X + RombDim + H, LastPoint1.Y + RombDim));
        e.Graphics.DrawLine(Pens.Black, new Point(LastPoint1.X - RombDim, LastPoint1.Y + RombDim), new Point(LastPoint1.X - RombDim - (Nr1 * LM + Nr1 * S) / 2, LastPoint1.Y + RombDim));
        e.Graphics.DrawLine(Pens.Black, new Point(LastPoint1.X + RombDim, LastPoint1.Y + RombDim), new Point(LastPoint1.X + RombDim + (Nr2 * LM + Nr2 * S) / 2, LastPoint1.Y + RombDim));
        e.Graphics.DrawLine(Pens.Black, new Point(LastPoint1.X - RombDim - (Nr1 * LM + Nr1 * S) / 2, LastPoint1.Y + RombDim), new Point(LastPoint1.X - RombDim - (Nr1 * LM + Nr1 * S) / 2, LastPoint1.Y + RombDim + H));
        e.Graphics.DrawLine(Pens.Black, new Point(LastPoint1.X + RombDim + (Nr2 * LM + Nr2 * S) / 2, LastPoint1.Y + RombDim), new Point(LastPoint1.X + RombDim + (Nr2 * LM + Nr2 * S) / 2, LastPoint1.Y + RombDim + H));
        LastPoint1 = new Point(LastPoint1.X - RombDim - (Nr1 * LM + Nr1 * S) / 2, LastPoint1.Y + RombDim + H);*/
        //   }
        //if (i == 3) break;
        //  }
        //#region Schema_Logica_Veche_Si_Comentata
        /*if (lang == "VB6" | lang == "VBSCRIPT")
        {
            //Comentariu
            if (prelucrare[i].TrimStart().StartsWith("//")) prelucrare[i] = Regex.Replace(prelucrare[i], "//", "'");
            //Eliminarea finalizarii cu punct si virgula
            if (prelucrare[i].EndsWith(";")) prelucrare[i] = prelucrare[i].Substring(0, prelucrare[i].Length - 1);
            //Operatia de citire de la tastatura
            if (prelucrare[i].TrimStart().StartsWith("citeste"))
            {
                e.Graphics.DrawLine(Pens.Black, left + 50, top - 15, left + 50, top);
                int size = (int)e.Graphics.MeasureString(prelucrare[i].TrimStart(), pseudocodeEd.Font).Width + 27;
                Point[] points = { new Point(left, top + 40), new Point(left + 10, top), new Point(left + size, top), new Point(left + size - 10, top + 40) };
                e.Graphics.DrawPolygon(Pens.Black, points);
                e.Graphics.DrawString(prelucrare[i].TrimStart(), new Font("Consolas", 12), Brushes.Black, left + 10, top + 10);
                e.Graphics.DrawLine(Pens.Black, left + size - 5, top + 20, left + size + 20, top + 20);
                Point[] triangle = { new Point(left + size - 5, top + 20), new Point(left + size, top + 15), new Point(left + size, top + 25) };
                e.Graphics.FillPolygon(Brushes.Black, triangle);
                //e.Graphics.DrawLine(Pens.Black, left + 50, top + 40, left + 50, top + 55);
                top = top + 55;
                if (left + size + 20 > right) right = left + size + 20;
            }
            //Operatia de scriere pe ecran
            if (prelucrare[i].TrimStart().StartsWith("scrie"))
            {
                e.Graphics.DrawLine(Pens.Black, left + 50, top - 15, left + 50, top);
                int size = (int)e.Graphics.MeasureString(prelucrare[i].TrimStart(), pseudocodeEd.Font).Width + 27;
                Point[] points = { new Point(left, top + 40), new Point(left + 10, top), new Point(left + size + 10, top), new Point(left + size, top + 40) };
                e.Graphics.DrawPolygon(Pens.Black, points);
                e.Graphics.DrawString(prelucrare[i].TrimStart(), new Font("Consolas", 12), Brushes.Black, left + 10, top + 10);
                e.Graphics.DrawLine(Pens.Black, left + size + 5, top + 20, left + size + 30, top + 20);
                Point[] triangle = { new Point(left + size + 25, top + 15), new Point(left + size + 25, top + 25), new Point(left + size + 30, top + 20) };
                e.Graphics.FillPolygon(Brushes.Black, triangle);
                //e.Graphics.DrawLine(Pens.Black, left + 50, top + 40, left + 50, top + 55);
                top = top + 55;
                if (left + size + 20 > right) right = left + size + 20;
            }
            //Instructiunea daca
            if (prelucrare[i].TrimStart().StartsWith("daca"))
            {
                e.Graphics.DrawLine(Pens.Black, left + 50, top - 15, left + 50, top);
                wasElse = false;
                lastYESifnum++;
                lastYESif[lastYESifnum] = new Point(left, top + 40);
                prelucrare[i] = Regex.Replace(prelucrare[i], "daca ", "");
                int size = (int)e.Graphics.MeasureString(prelucrare[i], pseudocodeEd.Font).Width + 27;
                Point[] points = { new Point(left + 50, top), new Point(left + 40 - size, top + 20), new Point(left + 50, top + 40), new Point(left + 60 + size, top + 20) };
                e.Graphics.DrawPolygon(Pens.Black, points);
                e.Graphics.DrawString(prelucrare[i].TrimStart(), new Font("Consolas", 12), Brushes.Black, left + 20, top + 10);
                e.Graphics.DrawLine(Pens.Black, left + 60 + size, top + 20, left + 60 + size + 45, top + 20);
                e.Graphics.DrawString("DA", new Font("Consolas", 12), Brushes.Black, left + 60 + size + 10, top + 4);
                Point[] triangle = { new Point(left + size + 60, top + 15), new Point(left + size + 60, top + 25), new Point(left + size + 70, top + 20) };
                e.Graphics.FillPolygon(Brushes.Black, triangle);
                e.Graphics.DrawLine(Pens.Black, left + 60 + size + 45, top + 20, left + 60 + size + 45, top + 45);
                lastifnum++;
                lastif[lastifnum] = new Point(left, top + 40);
                top = top + 45;
                left = left + size + 55;
                if (left + size + 55 > right) right = left + size + 55;
            }
            //Instructiunea altfel
            if (prelucrare[i].TrimStart().StartsWith("altfel"))
            {
                lastifnum = 1;
                wasElse = true;
                lastYESif[lastYESifnum] = new Point(left, top);
                int oldtop = top;
                left = lastif[lastifnum].X;
                top = lastif[lastifnum].Y;
                e.Graphics.DrawLine(Pens.Black, left + 50, top, left + 50, oldtop + 15);
                e.Graphics.DrawString("NU", new Font("Consolas", 12), Brushes.Black, left + 26, top);
                Point[] triangle = { new Point(left + 45, top), new Point(left + 50, top + 10), new Point(left + 55, top) };
                e.Graphics.FillPolygon(Brushes.Black, triangle);
                top = oldtop + 15;
                lastifnum--;
                if (left > right) right = left;
            }
            //Instructiunea sfarsit_daca
            if (prelucrare[i].TrimStart().StartsWith("sfarsit_daca"))
            {
                if (wasElse == true)
                {
                    int oldleft = left;
                    int oldtop = top;
                    left = lastYESif[lastYESifnum].X;
                    top = lastYESif[lastYESifnum].Y;
                    e.Graphics.DrawLine(Pens.Black, left + 50, oldtop, left + 50, top - 15);
                    e.Graphics.DrawLine(Pens.Black, oldleft + 50, oldtop, left + 50, oldtop);
                    e.Graphics.DrawLine(Pens.Black, oldleft + 50, oldtop - 15, oldleft + 50, oldtop);
                    e.Graphics.FillRectangle(Brushes.Black, oldleft + 45, oldtop - 5, 10, 10);
                    left = oldleft;
                    top = oldtop;
                }
                else
                {
                    lastYESif[lastYESifnum] = new Point(left, top);
                    int oldtop = top;
                    left = lastif[lastifnum].X;
                    top = lastif[lastifnum].Y;
                    e.Graphics.DrawLine(Pens.Black, left + 50, top, left + 50, oldtop + 15);
                    e.Graphics.DrawString("NU", new Font("Consolas", 12), Brushes.Black, left + 26, top);
                    top = oldtop + 15;
                    lastifnum--;
                    int oldleft = left;
                    left = lastYESif[lastYESifnum].X;
                    top = lastYESif[lastYESifnum].Y;
                    e.Graphics.DrawLine(Pens.Black, left + 50, oldtop, left + 50, top - 15);
                    e.Graphics.DrawLine(Pens.Black, oldleft + 50, oldtop, left + 50, oldtop);
                    e.Graphics.FillRectangle(Brushes.Black, oldleft + 45, oldtop - 5, 10, 10);
                    left = oldleft;
                    top = oldtop;
                }
                //e.Graphics.DrawLine(Pens.Black, left + 50, top, left + 50, top + 15);
                top += 15;
                wasElse = false;
                if (left > right) right = left;
            }
            //Instructiunea cat timp
            if (prelucrare[i].TrimStart().StartsWith("cat_timp"))
            {
                //TODO
            }
            //Instructiunea sfarsit cat timp
            if (prelucrare[i].TrimStart().StartsWith("sfarsit_cat_timp"))
            {
                //TODO
            }
            //Instructiunea pentru
            if (prelucrare[i].TrimStart().StartsWith("pentru"))
            {
                Debug.WriteLine(lastfornum);
                string numecontor = Regex.Replace(prelucrare[i].TrimStart(), "pentru ", "").Split(' ')[0];
                prelucrare[i] = Regex.Replace(prelucrare[i], "pentru ", "");
                int size = (int)e.Graphics.MeasureString(prelucrare[i], pseudocodeEd.Font).Width + 27;
                e.Graphics.DrawLine(Pens.Black, left + 50, top, left + 50, top - 15);
                e.Graphics.DrawRectangle(Pens.Black, left, top, size, 40);
                e.Graphics.DrawString(prelucrare[i].TrimStart().Split(',')[0], new Font("Consolas", 12), Brushes.Black, left + 20, top + 10);
                e.Graphics.DrawLine(Pens.Black, left + 50, top + 40 + 15, left + 50, top + 40);
                top = top + 55;
                Point[] points = { new Point(left + 50, top), new Point(left + 40 - size, top + 20), new Point(left + 50, top + 40), new Point(left + 60 + size, top + 20) };
                e.Graphics.DrawPolygon(Pens.Black, points);
                try
                {
                    e.Graphics.DrawString(numecontor + " <=" + prelucrare[i].Split(',')[1], new Font("Consolas", 12), Brushes.Black, left + 20, top + 10);
                }
                catch { }
                e.Graphics.DrawLine(Pens.Black, left + 60 + size, top + 20, left + 60 + size + 45, top + 20);
                e.Graphics.DrawString("DA", new Font("Consolas", 12), Brushes.Black, left + 60 + size + 10, top + 4);
                Point[] triangle = { new Point(left + size + 60, top + 15), new Point(left + size + 60, top + 25), new Point(left + size + 70, top + 20) };
                e.Graphics.FillPolygon(Brushes.Black, triangle);
                e.Graphics.DrawLine(Pens.Black, left + 60 + size + 45, top + 20, left + 60 + size + 45, top + 45);
                lastfornum++;
                lastfor[lastfornum] = new Point(left + 40 - size, top + 40);
                lastfor2[lastfornum] = new Point(left, top + 40);
                top = top + 45;
                left = left + size + 55;
                if (left + size + 55 > right) right = left + size + 55;
            }
            //Instructiunea sfarsit_pentru
            if (prelucrare[i].TrimStart().StartsWith("sfarsit_pentru"))
            {
                int oldleft = left;
                int oldtop = top;
                left = lastfor[lastfornum].X;
                top = lastfor[lastfornum].Y;
                e.Graphics.DrawLine(Pens.Black, oldleft + 50, oldtop, oldleft + 50, oldtop - 15);
                e.Graphics.DrawRectangle(Pens.Black, oldleft, oldtop, 320, 40);
                if (oldleft + 320 > right) right = oldleft + 320;
                e.Graphics.DrawString("Se crește contorul cu o unitate.", new Font("Consolas", 12), Brushes.Black, oldleft + 10, oldtop + 10);
                e.Graphics.DrawLine(Pens.Black, oldleft + 50, oldtop + 55, oldleft + 50, oldtop + 40);
                //IMPLEMENTAREA 1
                //e.Graphics.DrawLine(Pens.Black, oldleft + 50, oldtop + 55, left - 10, oldtop + 55);
                //e.Graphics.DrawLine(Pens.Black, left - 10, oldtop + 55, left - 10, top - 45);
                //e.Graphics.DrawLine(Pens.Black, left - 10, top - 45, lastfor2[lastfornum].X + 50, top - 45);
                //Pen blackPen = new Pen(Color.Black, 2);
                //blackPen.DashStyle = System.Drawine.Graphics.Drawing2D.DashStyle.DashDot;
                //e.Graphics.DrawLine(blackPen, lastfor2[lastfornum].X + 50, oldtop + 70, lastfor2[lastfornum].X + 50, top);
                //e.Graphics.DrawLine(Pens.Black, lastfor2[lastfornum].X + 50, oldtop + 55, lastfor2[lastfornum].X + 50, oldtop + 70);
                //SFARSIT IMPLEMENTAREA 1
                //IMPLEMENTAREA 2
                e.Graphics.DrawLine(Pens.Black, oldleft + 50, oldtop + 55, right + 10, oldtop + 55);
                e.Graphics.DrawLine(Pens.Black, right + 10, oldtop + 55, right + 10, top - 45);
                e.Graphics.DrawLine(Pens.Black, lastfor2[lastfornum].X + 50, top - 45, right + 10, top - 45);
                e.Graphics.DrawLine(Pens.Black, lastfor2[lastfornum].X + 50, oldtop + 70, lastfor2[lastfornum].X + 50, top);
                right += 10;
                left = lastfor2[lastfornum].X;
                top = oldtop + 70;
                lastfornum--;
            }
            //Instructiunea executa
            if (prelucrare[i].TrimStart().StartsWith("executa"))
            {
                //TODO
            }
            //Instructiunea executa pana cand (loop until)
            if (prelucrare[i].TrimStart().StartsWith("pana_cand"))
            {
                //TODO
            }
        }*/
        // #endregion
        //}

        /*e.Graphics.DrawEllipse(Pens.Black, left, top, 100, 40);
        e.Graphics.DrawString("STOP", new Font("Consolas", 12), Brushes.Black, left + 28, top + 10);
        e.Graphics.DrawLine(Pens.Black, left + 50, top, left + 50, top - 15);
        SchemaLogicaCont.Width = right + 30;
        SchemaLogicaCont.Height = top + 50;*/


        //}


        /* private void TestTim_Tick(object sender, EventArgs e)
         {
             string source = pseudocodeEd.Text;
             textPseudocod = Regex.Split(source, "\n");
             for (i = 0; i < textPseudocod.Length; i++)
             {
                 if (textPseudocod[i].TrimStart().StartsWith("daca"))
                 {
                     s = M + D;
                     tataDist(i + 1);
                     Debug.WriteLine(s);

                 }
             }
         }*/
        /*void gmh_TheMouseMoved()
        {
            Point cur_pos = System.Windows.Forms.Cursor.Position;
            Control control = GetChildAtPoint(System.Windows.Forms.Cursor.Position);
            Debug.WriteLine(GetChildAtPoint(System.Windows.Forms.Cursor.Position));
            if (control == button56)
            {
                MessageBox.Show("da");
                if (control.Enabled == false)
                {
                    string toolTipString = toolTip1.GetToolTip(control);
                    // trigger the tooltip with no delay and some basic positioning just to give you an idea
                    toolTip1.Show(toolTipString, control, control.Width / 2, control.Height / 2);
                }
            }
        }*/
    }
    /*public delegate void MouseMovedEvent();

    public class GlobalMouseHandler : IMessageFilter
    {
        private const int WM_MOUSEMOVE = 0x0200;

        public event MouseMovedEvent TheMouseMoved;

        #region IMessageFilter Members

        public bool PreFilterMessage(ref Message m)
        {
            if (m.Msg == WM_MOUSEMOVE)
            {
                if (TheMouseMoved != null)
                {
                    TheMouseMoved();
                }
            }
            // Always allow message to continue to the next filter control
            return false;
        }

        #endregion
    }*/
}