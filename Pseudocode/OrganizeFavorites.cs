﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class OrganizeFavorites : Form
    {
        string rename;
        string delete;
        MainForm mf;
        string AppName;
        string invalidName;
        public void LoadFiles()
        {
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode")) Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode");
            foreach (string file in Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode"))
            {
                if (Path.GetExtension(file) == ".url") listBox1.Items.Add(Path.GetFileNameWithoutExtension(file));
            }
        }
        public OrganizeFavorites(MainForm m, string ap)
        {
            InitializeComponent();
            mf = m;
            AppName = ap;
            LoadFiles();
            if (Properties.Settings.Default.UILang == 0)
            {
                rename = "Type the new name for the selected favorite.";
                delete = "Are you sure you would like to delete the selected favorite?";
                invalidName = "A file name cannot contain any of the following:\n\nForbidden characters: \\ / :  * ? \" < > | \nForbidden patterns: any filename conatining only the dot ('.') character.\nForbidden filenames (excluding extension): CON, PRN, AUX, CLOCK$, NUL, COM0, COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8, COM9, LPT0, LPT1, LPT2, LPT3, LPT4, LPT5, LPT6, LPT7, LPT8, and LPT9.";
            }
            else if (Properties.Settings.Default.UILang == 1)
            {
                rename = "Tastați noul nume pentru site-ul selectat.";
                delete = "Sigur eliminați site-ul selectat din lista de favorite?";
                invalidName = "Un nume de fișier nu poate conține niciunul din următoarele caractere:\n\nCaractere interzise: \\ / :  * ? \" < > | \nPatternuri interzise: orice nume de fi;ier ce con'ine numai caracterul punct ('.').\nNume de fișier interzise (excluzând extensia): CON, PRN, AUX, CLOCK$, NUL, COM0, COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8, COM9, LPT0, LPT1, LPT2, LPT3, LPT4, LPT5, LPT6, LPT7, LPT8, and LPT9.";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string name = Microsoft.VisualBasic.Interaction.InputBox(rename, AppName, listBox1.Items[listBox1.SelectedIndex].ToString());
            if (name == "")
            {
                return;
            }
            else
            {
                foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                {
                    if (name.Contains(c))
                    {
                        CasetaDeMesaj(this, invalidName, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                foreach (char d in name.ToCharArray())
                {
                    if (d != '.') goto here;
                }
                CasetaDeMesaj(this, invalidName, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
                here:
                if (name == "CON" || name == "PRN" || name == "AUX" || name == "CLOCK$" || name == "NUL" || name == "COM0" || name == "COM1" || name == "COM2" || name == "COM3" || name == "COM4" || name == "COM5" || name == "COM6" || name == "COM7" || name == "COM8" || name == "COM9" || name == "LPT0" || name == "LPT1" || name == "LPT2" || name == "LPT3" || name == "LPT4" || name == "LPT5" || name == "LPT6" || name == "LPT7" || name == "LPT8" || name == "LPT9")
                {
                    CasetaDeMesaj(this, invalidName, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                File.Move(Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode\\" + listBox1.Items[listBox1.SelectedIndex].ToString() + ".url", Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode\\" + name + ".url");
                listBox1.Items.Clear();
                LoadFiles();
            }
        }
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dr = CasetaDeMesaj(mf, delete + "\n\n\"" + listBox1.Items[listBox1.SelectedIndex].ToString() + "\"", AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Favorites) + "\\Pseudocode\\" + listBox1.Items[listBox1.SelectedIndex].ToString() + ".url");
                listBox1.Items.Clear();
                LoadFiles();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
