﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections.Specialized;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Net;

namespace Pseudocode
{
    public partial class Welcome : Form
    {
                public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        /// <summary>
        /// Native call to add the file to windows' recent file list
        /// </summary>
        /// <param name="uFlags">Always use (uint)ShellAddRecentDocs.SHARD_PATHW</param>
        /// <param name="pv">path to file</param>
        [DllImport("shell32.dll")]
        public static extern void SHAddToRecentDocs(UInt32 uFlags,
            [MarshalAs(UnmanagedType.LPWStr)] String pv);

        enum ShellAddRecentDocs
        {
            SHARD_PIDL = 0x00000001,
            SHARD_PATHA = 0x00000002,
            SHARD_PATHW = 0x00000003
        }
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        Button[] button = new Button[100];
        Button[] tile = new Button[100];
        StringCollection col = new StringCollection();
        Form1 Form1;
        string AppName;
        string SureToEmptyList;
        string NoSuchFile;
        string Before;
        bool atstart;
        string LocationText;
        public Welcome(Form1 form, bool IsSignedIn, bool AtStartup)
        {

            Form1 = form;
            if (Properties.Settings.Default.UILang == 0)
            {
                Before = "Download images from Bing daily, like today's ";
                AppName = "Pseudocode";
                SureToEmptyList = "Erase the contents of the recent files list?\n\nJust the recent files list will be cleared off data, the actual files will stay at their respective places on the disk.";
                NoSuchFile = "The specified file was not found on the disk.\n\nThe file may have been deleted or moved.";
                LocationText = "Location: ";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                Before = "Descărcare imagini de la Bing zilnic, precum cea de astăzi: ";
                AppName = "Pseudocod";
                SureToEmptyList = "Curățați conținutul listei de fișiere recente?\n\nDoar lista va fi ștearsă, fișierele conținute într-aceasta vor rămâne intacte pe disc.";
                NoSuchFile = "Nu s-a găsit fișierul specificat pe disc.\n\nAcesta ar fi putut să fi fost mutat sau șters.";
                LocationText = "Locație: ";
            }
            InitializeComponent();
            if (IsSignedIn == true)
            {
                button15.Visible = false;
                button16.Visible = false;
                button5.Visible = true;
            }
            else
            {
                button15.Visible = true;
                button16.Visible = true;
                button5.Visible = false;
            }
            if (Properties.Settings.Default.AutoSignIn == false || AtStartup == false)
            {
                button15.Visible = false;
                button16.Visible = false;
            }
            atstart = AtStartup;
            string CloseText = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                CloseText = "Close (Alt+F4)";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                CloseText = "Închidere (Alt+F4)";
            }
            toolTip1.SetToolTip(Close, CloseText);
           // Close.BackColor = Color.FromArgb(199, 80, 80);
        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }
        private void Welcome_Load(object sender, EventArgs e)
        {
          //  Close.BackColor = Color.FromArgb(199, 80, 80);
            panel1.BackColor = Color.White;
            panel2.BackColor = Color.White;
            button1.BackColor = Color.FromArgb(106, 56, 137);
            button2.BackColor = Color.FromArgb(106, 56, 137);
            button3.BackColor = Color.FromArgb(106, 56, 137);
            button4.BackColor = Color.FromArgb(106, 56, 137);
            button6.BackColor = Color.FromArgb(106, 56, 137);
            button5.BackColor = Color.FromArgb(0, 126, 229);
            button16.BackColor = Color.FromArgb(0, 126, 229);
            button1.ForeColor = Color.White;
            button2.ForeColor = Color.White;
            button3.ForeColor = Color.White;
            button4.ForeColor = Color.White;
            button5.ForeColor = Color.White;
            button16.ForeColor = Color.White;
            button6.ForeColor = Color.White;
            button15.BackColor = Color.DarkSlateBlue;
            button15.ForeColor = Color.White;
            button14.BackColor = Color.DodgerBlue;
            button7.BackColor = Color.DarkSlateBlue;
            button9.BackColor = Color.FromArgb(106, 56, 137);
            button12.BackColor = Color.FromArgb(106, 56, 137);
            button7.ForeColor = Color.White;
            button9.ForeColor = Color.White;
            button12.ForeColor = Color.White;
            button14.ForeColor = Color.White;
            button1.FlatAppearance.MouseOverBackColor = Color.FromArgb(69, 37, 90);
            button1.FlatAppearance.MouseDownBackColor = Color.FromArgb(49, 26, 64);
            button2.FlatAppearance.MouseOverBackColor = Color.FromArgb(69, 37, 90);
            button2.FlatAppearance.MouseDownBackColor = Color.FromArgb(49, 26, 64);
            button3.FlatAppearance.MouseOverBackColor = Color.FromArgb(69, 37, 90);
            button3.FlatAppearance.MouseDownBackColor = Color.FromArgb(49, 26, 64);
            button4.FlatAppearance.MouseOverBackColor = Color.FromArgb(69, 37, 90);
            button4.FlatAppearance.MouseDownBackColor = Color.FromArgb(49, 26, 64);
            button5.FlatAppearance.MouseOverBackColor = Color.FromArgb(69, 37, 90);
            button5.FlatAppearance.MouseDownBackColor = Color.FromArgb(49, 26, 64);
            button6.FlatAppearance.MouseOverBackColor = Color.FromArgb(69, 37, 90);
            button6.FlatAppearance.MouseDownBackColor = Color.FromArgb(49, 26, 64);
            button7.FlatAppearance.MouseOverBackColor = Color.FromArgb(69, 37, 90);
            button7.FlatAppearance.MouseDownBackColor = Color.FromArgb(49, 26, 64);
            button9.FlatAppearance.MouseOverBackColor = Color.FromArgb(69, 37, 90);
            button9.FlatAppearance.MouseDownBackColor = Color.FromArgb(49, 26, 64);
            button12.FlatAppearance.MouseOverBackColor = Color.FromArgb(69, 37, 90);
            button12.FlatAppearance.MouseDownBackColor = Color.FromArgb(49, 26, 64);
            button5.FlatAppearance.MouseOverBackColor = Color.DeepSkyBlue;
            button5.FlatAppearance.MouseDownBackColor = Color.DarkBlue;
            button16.FlatAppearance.MouseOverBackColor = Color.DeepSkyBlue;
            button16.FlatAppearance.MouseDownBackColor = Color.DarkBlue;
            button14.FlatAppearance.MouseOverBackColor = Color.DeepSkyBlue;
            button14.FlatAppearance.MouseDownBackColor = Color.DarkBlue;
            button15.FlatAppearance.MouseOverBackColor = Color.BlueViolet;
            button15.FlatAppearance.MouseDownBackColor = Color.DarkViolet;
            col = Properties.Settings.Default.RecentFiles;
            int v;
            v = 1;
            for (int i = col.Count - 1; i >= 0; i--)
            {
                System.Diagnostics.Debug.WriteLine(col.Count);
                button[i] = new Button();
                button[i].Image = buttonEx.Image;
                using (Graphics g = this.CreateGraphics())
                {
                    if (g.MeasureString(Path.GetFileNameWithoutExtension(col[i]), button2.Font).Width + 2 > button2.Width * 2 + 40)
                    {
                        string a = Path.GetFileNameWithoutExtension(col[i]);
                        while (g.MeasureString(a, button2.Font).Width + 2 > button2.Width * 2 + 40) a = a.Substring(0, a.Length - 1);
                        button[i].Text = a + "...";
                    }
                    else button[i].Text = Path.GetFileNameWithoutExtension(col[i]);
                }
                //button[i].Text = System.IO.Path.GetFileNameWithoutExtension(col[i]);
                toolTip1.SetToolTip(button[i], LocationText + col[i]);
                button[i].BackColor = Color.FromArgb(106, 56, 137);
                button[i].ImageAlign = ContentAlignment.MiddleCenter;
                button[i].Size = buttonEx.Size;
                try
                {
                    
                        button[i].Location = new Point(button[i + 3].Location.X + button[i + 3].Width + 6, button[i + 3].Location.Y);
                    
                }
                catch
                {
                    if (button15.Visible == true)
                    {
                        if (v == 1) button[i].Location = new Point(button15.Location.X + button15.Width + 31, button15.Location.Y);
                        if (v == 2) button[i].Location = new Point(button15.Location.X + button15.Width + 31, button[i + 1].Location.Y + button[i + 1].Height + 6);
                        if (v == 3) button[i].Location = new Point(button15.Location.X + button15.Width + 31, button[i + 1].Location.Y + button[i + 1].Height + 6);
                    }
                    else
                    {
                        if (v == 1) button[i].Location = new Point(button2.Location.X + button2.Width + 31, button2.Location.Y);
                        if (v == 2) button[i].Location = new Point(button2.Location.X + button2.Width + 31, button[i + 1].Location.Y + button[i + 1].Height + 6);
                        if (v == 3) button[i].Location = new Point(button2.Location.X + button2.Width + 31, button[i + 1].Location.Y + button[i + 1].Height + 6);
                    }

                }
                button[i].FlatStyle = FlatStyle.Flat;
                button[i].FlatAppearance.BorderSize = 0;
                button[i].FlatAppearance.MouseOverBackColor = Color.FromArgb(69, 37, 90);
                button[i].FlatAppearance.MouseDownBackColor = Color.FromArgb(49, 26, 64);
                button[i].Font = buttonEx.Font;
                button[i].TextAlign = ContentAlignment.BottomLeft;
                button[i].Tag = col[i];
                button[i].Anchor = buttonEx.Anchor;
                button[i].Click += new EventHandler(button_Click);
                button[i].AutoEllipsis = true;
                button[i].Show();
                button[i].BringToFront();
                button[i].ForeColor = Color.White;
                panel1.Controls.Add(button[i]);
                v++;
            }
            try
            {
                panel3.Left = button[0].Location.X + button[0].Width + 31;
            }
            catch
            {
                if (button15.Visible == true)
                {
                    panel3.Left = button15.Left + button15.Width + 31;
                    if (col.Count == 0)
                    {
                        label3.Left = label2.Left;
                        label2.Visible = false;
                    }
                }
                else
                {
                    label3.Left = label2.Left;
                    label2.Visible = false;
                    panel3.Left = button15.Left;
                }
            }
            pictureBox1.SendToBack();
            if (Properties.Settings.Default.DownloadImages == true)
            {
                backgroundWorker1.RunWorkerAsync();
                checkBoxItem2.Checked = true;
            }
            else checkBoxItem2.Checked = false;
            if (Properties.Settings.Default.WasWelcomeShown == true)
            {
                checkBoxItem1.Checked = false;
            }
            else checkBoxItem1.Checked = true;

            backgroundWorker2.RunWorkerAsync();

            panel3.BackColor = Color.Transparent;
            panel1.PerformLayout();
            panel3.PerformLayout();
            panel1.Refresh();
            panel3.Refresh();
            panel1.Update();
            panel3.Update();
        }

        private void button_Click(object sender, EventArgs e)
        {
            var Button = (Button)sender;
            openFileDialog1.FileName = (string)Button.Tag;
            OpenFile(true);
        }
        public void OpenFile(bool isRecentAlready)
        {
            try
            {
                System.IO.TextReader tw = new System.IO.StreamReader(openFileDialog1.FileName);
                //main.New();
                this.DialogResult = System.Windows.Forms.DialogResult.Ignore;
                string citit = tw.ReadToEnd();
                string[] parcurgere = citit.Split('\n');
                bool incepProp = false;
                Form1.fileProperties.Clear();
                for (int i = 0; i < parcurgere.Length; i++)
                {
                    if (parcurgere[i] == "<properties>") incepProp = true;
                    if (incepProp == true && parcurgere[i] != "<properties>") Form1.fileProperties.Add(parcurgere[i]);
                    else if (parcurgere[i] != "<properties>")
                        if (i == 0) Form1.pseudocodeEd.Text += parcurgere[i];
                        else Form1.pseudocodeEd.Text += "\n" + parcurgere[i];
                }
                //Form1.pseudocodeEd.Text = tw.ReadToEnd();
                Form1.pseudocodeEd.UndoRedo.EmptyUndoBuffer();
                ExTag oldEX = (ExTag)Form1.pseudocodeEd.Tag;
                ExTag exTag = new ExTag();
                exTag.Add("issaved", true);
                exTag.Add("filename", openFileDialog1.FileName);
                exTag.Add("uniqueString", oldEX.Get("uniqueString"));
                Form1.pseudocodeEd.Tag = exTag;
                tw.Close();
                FileInfo fi = new FileInfo(openFileDialog1.FileName);
                Form1.Text = Path.GetFileNameWithoutExtension(fi.Name);
                // Form1.superTabControl1.Tabs[Form1.superTabControl1.Tabs.Count - 1].Text = Path.GetFileNameWithoutExtension(fi.Name);
                if (isRecentAlready == false)
                {
                    StringCollection col = Properties.Settings.Default.RecentFiles;
                    bool isAdded = false;
                    foreach (string s in col)
                    {
                        if (s == openFileDialog1.FileName)
                        {
                            isAdded = true;
                        }
                    }
                    if (isAdded == false) col.Add(openFileDialog1.FileName);
                    Properties.Settings.Default.RecentFiles = col;
                    Properties.Settings.Default.Save();
                    AddFileToRecentFilesList(openFileDialog1.FileName);
                }
                else
                {
                    StringCollection col = Properties.Settings.Default.RecentFiles;
                    col.Remove(openFileDialog1.FileName);
                    col.Add(openFileDialog1.FileName);
                    Properties.Settings.Default.RecentFiles = col;
                    Properties.Settings.Default.Save();
                    AddFileToRecentFilesList(openFileDialog1.FileName);
                }
                Form1.flagReident = true;
                this.Close();
            }
            catch
            {
                CasetaDeMesaj(Form1, NoSuchFile, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                col.Remove(openFileDialog1.FileName);
                this.Close();
            }
        }
        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void AddFileToRecentFilesList(string fileName)
        {
            SHAddToRecentDocs((uint)ShellAddRecentDocs.SHARD_PATHW, fileName);
        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            panel1.HorizontalScroll.Value = 0;
            panel1.PerformLayout();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            panel1.HorizontalScroll.Value = 0;
            panel1.PerformLayout();
            if (button15.Visible == true) panel1.HorizontalScroll.Value = button15.Left + button15.Width;
            else panel1.HorizontalScroll.Value = button2.Left + button2.Width;
            panel1.PerformLayout();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            panel1.HorizontalScroll.Value = 0;
            panel1.PerformLayout();
            panel1.HorizontalScroll.Value = panel3.Left;
                panel1.PerformLayout();
        }

        private void panel1_MouseEnter(object sender, EventArgs e)
        {
            panel1.Focus();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Open open = new Open(Form1);
            if (open.ShowDialog() != System.Windows.Forms.DialogResult.Ignore)
            {
                //Form1.ReadyToClose = true;
                //Form1.Close();
            }
            else
            {
                this.Close();
                Form1.LanguageIdentification();

            }
            /*if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    OpenFile(false);
                }
                catch
                {
                    //uperTabItem1.RaiseClick();
                }
            }
            else
            {
                //superTabControl1.SelectedTabIndex = 0;
            }*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (atstart == true) this.Close();
            else
            {
                Form1.button3.PerformClick();
                this.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Open open = new Open(Form1);
            open.doAfterLoad = "use-code";
            if (open.ShowDialog() != System.Windows.Forms.DialogResult.Ignore)
            {
                //Form1.ReadyToClose = true;
                //Form1.Close();
            }
            else
            {
                this.Close();

                Form1.LanguageIdentification();

            }

            /*Open open = new Open(Form1);
            Application.DoEvents();
            open.Show();
            open.treeView1.SelectedNode = open.treeView1.Nodes[1];
            open.Visible = false;
            if (open.ShowDialog() != System.Windows.Forms.DialogResult.Ignore)
            {
                //Form1.ReadyToClose = true;
                //Form1.Close();
            }
            else
            {
                this.Close();

                Form1.LanguageIdentification();

            }*/
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Open open = new Open(Form1);
            open.doAfterLoad = "pseudocode-folder";
            if (open.ShowDialog() != System.Windows.Forms.DialogResult.Ignore)
            {
                //Form1.ReadyToClose = true;
                //Form1.Close();
            }
            else
            {
                this.Close();

                Form1.LanguageIdentification();

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Open open = new Open(Form1);
            open.doAfterLoad = "code-rep";
            if (open.ShowDialog() != System.Windows.Forms.DialogResult.Ignore)
            {
                //Form1.ReadyToClose = true;
                //Form1.Close();
            }
            else
            {
                this.Close();
                Form1.LanguageIdentification();

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //this.Hide();
            //SendKeys.Send("{ESC}");
            Open open = new Open(Form1);
            open.Tag = "drp";
            open.PerformLayout();
            open.ShowDialog();
            this.Close();
        }

        private void checkBoxItem1_Click(object sender, EventArgs e)
        {

        }

        private void checkBoxItem1_CheckStateChanged(object sender, EventArgs e)
        {

        }

        private void Welcome_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Focus();
            if (Properties.Settings.Default.Language == 1)
            {
                Form1.button23.PerformClick();
            }
            if (Properties.Settings.Default.Language == 2)
            {
                Form1.button22.PerformClick();
            }
            if (Properties.Settings.Default.Language == 3)
            {
                Form1.button25.PerformClick();
            }
            if (Properties.Settings.Default.Language == 4)
            {
                Form1.button24.PerformClick();
            }
            if (Properties.Settings.Default.Language == 5)
            {
                Form1.button26.PerformClick();
            }
            if (Properties.Settings.Default.Language == 6)
            {
                Form1.button27.PerformClick();
            }
           // SendKeys.Send("{ESC}");
            //this.DialogResult = System.Windows.Forms.DialogResult.OK;
            //Form1.TopMost = true;
            //Form1.TopMost = false;
            //SetForegroundWindow(Form1.Handle);
            //Form1.Size = Form1.Size;
        }
        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hWnd);
        private void button15_Click(object sender, EventArgs e)
        {
            Sign sg = new Sign(Form1.MdiParent as MainForm, false);
            sg.ShowDialog();
            this.Close();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                XDocument doc = XDocument.Load("http://www.bing.com/HPImageArchive.aspx?format=xml&idx=0&n=1&mkt=en-US");
                var request = WebRequest.Create("http://www.bing.com" + doc.Element("images").Element("image").Element("url").Value);
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    pictureBox1.BackgroundImage = Bitmap.FromStream(stream);
                }
                pictureBox1.BackgroundImageLayout = ImageLayout.Tile;
                checkBoxItem2.Refresh();
                DoOnUIThread(delegate()
{
    label4.Text = doc.Element("images").Element("image").Element("copyright").Value.ToString(); //Before + ...
    panel1.PerformLayout();
    panel3.PerformLayout();
    panel1.Refresh();
    panel3.Refresh();
    panel1.Update();
    panel3.Update();
    label4.PerformLayout();
    label4.Refresh();
    label4.Update();
});
            }
            catch {
                checkBoxItem2.Visible = false;
            }
        }
        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }
        private void panel1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void checkBoxItem2_Click(object sender, EventArgs e)
        {

        }

      /*  private void checkBoxItem2_CheckedChanged(object sender, DevComponents.DotNetBar.CheckBoxChangeEventArgs e)
        {
            
            Properties.Settings.Default.DownloadImages = checkBoxItem2.Checked;
            Properties.Settings.Default.Save();
            try
            {
                if (checkBoxItem2.Checked == true) backgroundWorker1.RunWorkerAsync();
            }
            catch { }
        }*/

      /*  private void checkBoxItem1_CheckedChanged(object sender, DevComponents.DotNetBar.CheckBoxChangeEventArgs e)
        {
            Properties.Settings.Default.WasWelcomeShown = !checkBoxItem1.Checked;
            Properties.Settings.Default.Save();
        }*/
        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            SetForegroundWindow(this.Handle);
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int x = 0;
                bool perecheZERO = false;
                Point perecheZEROLoc = new Point(0, 0);
                List<Point> freeSpace = new List<Point>();
                List<int> flags = new List<int>();
                try
                {
                    System.Net.WebClient wc = new System.Net.WebClient();
                    if (Properties.Settings.Default.UILang == 0) wc.DownloadFile(Properties.Settings.Default.ServerPath + "/com.valinet.pseudocode.codeboard/en/description.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\codeboard.txt");
                    if (Properties.Settings.Default.UILang == 1) wc.DownloadFile(Properties.Settings.Default.ServerPath + "/com.valinet.pseudocode.codeboard/ro/description.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\codeboard.txt");
                }
                catch { }
                string[] text = System.Text.RegularExpressions.Regex.Split(File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\codeboard.txt"), "\n");
                if (text[0].StartsWith("<")) throw new Exception();
                for (int i = 0; i < text.Length; i = i + 6)
                {
                    if (text[i] == "0")
                    {
                        if (i == 0)
                        {
                            tile[x] = new Button();
                            tile[x].Text = text[i + 1];
                            tile[x].Tag = text[i + 2];
                            pictureBox2.Load(text[i + 3]); tile[x].BackgroundImage = pictureBox2.Image;
                            string[] cul = System.Text.RegularExpressions.Regex.Split(text[i + 4], ",");
                            tile[x].ForeColor = Color.FromArgb(Convert.ToInt32(cul[0]), Convert.ToInt32(cul[1]), Convert.ToInt32(cul[2]));
                            string[] cul2 = System.Text.RegularExpressions.Regex.Split(text[i + 5], ","); tile[x].BackColor = Color.FromArgb(Convert.ToInt32(cul2[0]), Convert.ToInt32(cul2[1]), Convert.ToInt32(cul2[2]));
                            tile[x].Click += new EventHandler(tile_Click);
                            tile[x].FlatStyle = FlatStyle.Flat;
                            tile[x].FlatAppearance.BorderSize = 0;
                            tile[x].Font = buttonEx.Font;
                            tile[x].TextAlign = ContentAlignment.BottomLeft;
                            tile[x].Size = new Size(128, 128);
                            tile[x].Location = new Point(3, 3);
                            DoOnUIThread(delegate()
                            {
                                panel3.Controls.Add(tile[x]);
                            });
                            perecheZERO = true;
                            perecheZEROLoc = tile[x].Location;
                            x++;
                        }
                        else
                        {
                            if (freeSpace.Count == 0 || perecheZERO == true)
                            {
                                if (perecheZERO == false)
                                {
                                    tile[x] = new Button();
                                    tile[x].Text = text[i + 1];
                                    tile[x].Tag = text[i + 2];
                                    pictureBox2.Load(text[i + 3]); tile[x].BackgroundImage = pictureBox2.Image;
                                    string[] cul = System.Text.RegularExpressions.Regex.Split(text[i + 4], ",");
                                    tile[x].ForeColor = Color.FromArgb(Convert.ToInt32(cul[0]), Convert.ToInt32(cul[1]), Convert.ToInt32(cul[2]));
                                    string[] cul2 = System.Text.RegularExpressions.Regex.Split(text[i + 5], ","); tile[x].BackColor = Color.FromArgb(Convert.ToInt32(cul2[0]), Convert.ToInt32(cul2[1]), Convert.ToInt32(cul2[2]));
                                    tile[x].Click += new EventHandler(tile_Click);
                                    tile[x].FlatStyle = FlatStyle.Flat;
                                    tile[x].FlatAppearance.BorderSize = 0;
                                    tile[x].Font = buttonEx.Font;
                                    tile[x].TextAlign = ContentAlignment.BottomLeft;
                                    tile[x].Size = new Size(128, 128);
                                    int l = x - 1;
                                    while (flags.Contains(l)) l--;
                                    if (tile[l].Height == 128)
                                    {
                                        if (tile[l].Location.Y == 271)
                                        {
                                            tile[x].Location = new Point(tile[l].Location.X + 262 + 6, 3);
                                        }
                                        else
                                        {
                                            tile[x].Location = new Point(tile[l].Location.X, tile[l].Location.Y + 128 + 6);
                                        }
                                    }
                                    else if (tile[l].Height == 262)
                                    {
                                        if (tile[l].Location.Y == 3)
                                        {
                                            tile[x].Location = new Point(tile[l].Location.X, tile[l].Location.Y + 262 + 6);
                                        }
                                        else
                                        {
                                            tile[x].Location = new Point(tile[l].Location.X + 262 + 6, 3);
                                        }
                                    }
                                    DoOnUIThread(delegate()
                                    {
                                        panel3.Controls.Add(tile[x]);
                                    });
                                    perecheZERO = true;
                                    perecheZEROLoc = tile[x].Location;
                                    x++;
                                }
                                else
                                {
                                    tile[x] = new Button();
                                    tile[x].Text = text[i + 1];
                                    tile[x].Tag = text[i + 2];
                                    pictureBox2.Load(text[i + 3]); tile[x].BackgroundImage = pictureBox2.Image;
                                    string[] cul = System.Text.RegularExpressions.Regex.Split(text[i + 4], ",");
                                    tile[x].ForeColor = Color.FromArgb(Convert.ToInt32(cul[0]), Convert.ToInt32(cul[1]), Convert.ToInt32(cul[2]));
                                    string[] cul2 = System.Text.RegularExpressions.Regex.Split(text[i + 5], ","); tile[x].BackColor = Color.FromArgb(Convert.ToInt32(cul2[0]), Convert.ToInt32(cul2[1]), Convert.ToInt32(cul2[2]));
                                    tile[x].Click += new EventHandler(tile_Click);
                                    tile[x].FlatStyle = FlatStyle.Flat;
                                    tile[x].FlatAppearance.BorderSize = 0;
                                    tile[x].Font = buttonEx.Font;
                                    tile[x].TextAlign = ContentAlignment.BottomLeft;
                                    tile[x].Size = new Size(128, 128);
                                    tile[x].Location = new Point(perecheZEROLoc.X + 128 + 6, perecheZEROLoc.Y);
                                    flags.Add(x);
                                    DoOnUIThread(delegate()
                                    {
                                        panel3.Controls.Add(tile[x]);
                                    });
                                    perecheZERO = false;
                                    x++;
                                }
                            }
                            else
                            {
                                if (perecheZERO == false)
                                {
                                    tile[x] = new Button();
                                    tile[x].Text = text[i + 1];
                                    tile[x].Tag = text[i + 2];
                                    pictureBox2.Load(text[i + 3]); tile[x].BackgroundImage = pictureBox2.Image;
                                    string[] cul = System.Text.RegularExpressions.Regex.Split(text[i + 4], ",");
                                    tile[x].ForeColor = Color.FromArgb(Convert.ToInt32(cul[0]), Convert.ToInt32(cul[1]), Convert.ToInt32(cul[2]));
                                    string[] cul2 = System.Text.RegularExpressions.Regex.Split(text[i + 5], ","); tile[x].BackColor = Color.FromArgb(Convert.ToInt32(cul2[0]), Convert.ToInt32(cul2[1]), Convert.ToInt32(cul2[2]));
                                    tile[x].Click += new EventHandler(tile_Click);
                                    tile[x].FlatStyle = FlatStyle.Flat;
                                    tile[x].FlatAppearance.BorderSize = 0;
                                    tile[x].Font = buttonEx.Font;
                                    tile[x].TextAlign = ContentAlignment.BottomLeft;
                                    tile[x].Size = new Size(128, 128);
                                    tile[x].Location = freeSpace[0];
                                    freeSpace.RemoveAt(0);
                                    DoOnUIThread(delegate()
                                    {
                                        panel3.Controls.Add(tile[x]);
                                    });
                                    perecheZERO = true;
                                    perecheZEROLoc = tile[x].Location;
                                    x++;
                                }
                            }
                        }
                    }
                    else if (text[i] == "1")
                    {
                        if (i == 0)
                        {
                            tile[x] = new Button();
                            tile[x].Text = text[i + 1];
                            tile[x].Tag = text[i + 2];
                            pictureBox2.Load(text[i + 3]); tile[x].BackgroundImage = pictureBox2.Image;
                            string[] cul = System.Text.RegularExpressions.Regex.Split(text[i + 4], ",");
                            tile[x].ForeColor = Color.FromArgb(Convert.ToInt32(cul[0]), Convert.ToInt32(cul[1]), Convert.ToInt32(cul[2]));
                            string[] cul2 = System.Text.RegularExpressions.Regex.Split(text[i + 5], ","); tile[x].BackColor = Color.FromArgb(Convert.ToInt32(cul2[0]), Convert.ToInt32(cul2[1]), Convert.ToInt32(cul2[2]));
                            tile[x].Click += new EventHandler(tile_Click);
                            tile[x].FlatStyle = FlatStyle.Flat;
                            tile[x].FlatAppearance.BorderSize = 0;
                            tile[x].Font = buttonEx.Font;
                            tile[x].TextAlign = ContentAlignment.BottomLeft;
                            tile[x].Size = new Size(262, 128);
                            tile[x].Location = new Point(3, 3);
                            DoOnUIThread(delegate()
                            {
                                panel3.Controls.Add(tile[x]);
                            });
                            x++;
                        }
                        else
                        {
                            if (freeSpace.Count == 0)
                            {
                                tile[x] = new Button();
                                tile[x].Text = text[i + 1];
                                tile[x].Tag = text[i + 2];
                                pictureBox2.Load(text[i + 3]); tile[x].BackgroundImage = pictureBox2.Image;
                                string[] cul = System.Text.RegularExpressions.Regex.Split(text[i + 4], ",");
                                tile[x].ForeColor = Color.FromArgb(Convert.ToInt32(cul[0]), Convert.ToInt32(cul[1]), Convert.ToInt32(cul[2]));
                                string[] cul2 = System.Text.RegularExpressions.Regex.Split(text[i + 5], ","); tile[x].BackColor = Color.FromArgb(Convert.ToInt32(cul2[0]), Convert.ToInt32(cul2[1]), Convert.ToInt32(cul2[2]));
                                tile[x].Click += new EventHandler(tile_Click);
                                tile[x].FlatStyle = FlatStyle.Flat;
                                tile[x].FlatAppearance.BorderSize = 0;
                                tile[x].Font = buttonEx.Font;
                                tile[x].TextAlign = ContentAlignment.BottomLeft;
                                tile[x].Size = new Size(262, 128);
                                int l = x - 1;
                            start:
                                while (flags.Contains(l)) l--;
                                if (tile[l].Location.Y == 128 * 2 + 6 * 2 + 3 && tile[l].Height == 128)
                                {
                                    if (perecheZERO == true)
                                    {
                                        l--;
                                        goto start;
                                    }
                                    tile[x].Location = new Point(tile[l].Location.X + 262 + 6, 3);
                                }
                                else if (tile[l].Location.Y == 128 + 6 + 3 && tile[l].Height == 262)
                                {
                                    tile[x].Location = new Point(tile[l].Location.X + 262 + 6, 3);
                                }
                                else if (tile[l].Location.Y == 3 && tile[l].Height == 262)
                                {
                                    tile[x].Location = new Point(tile[l].Location.X, tile[l].Location.Y + 262 + 6);
                                }
                                else
                                {
                                    tile[x].Location = new Point(tile[l].Location.X, tile[l].Location.Y + 128 + 6);
                                }
                                DoOnUIThread(delegate()
                                {
                                    panel3.Controls.Add(tile[x]);
                                });
                                x++;
                            }
                            else
                            {
                                tile[x] = new Button();
                                tile[x].Text = text[i + 1];
                                tile[x].Tag = text[i + 2];
                                pictureBox2.Load(text[i + 3]); tile[x].BackgroundImage = pictureBox2.Image;
                                string[] cul = System.Text.RegularExpressions.Regex.Split(text[i + 4], ",");
                                tile[x].ForeColor = Color.FromArgb(Convert.ToInt32(cul[0]), Convert.ToInt32(cul[1]), Convert.ToInt32(cul[2]));
                                string[] cul2 = System.Text.RegularExpressions.Regex.Split(text[i + 5], ","); tile[x].BackColor = Color.FromArgb(Convert.ToInt32(cul2[0]), Convert.ToInt32(cul2[1]), Convert.ToInt32(cul2[2]));
                                tile[x].Click += new EventHandler(tile_Click);
                                tile[x].FlatStyle = FlatStyle.Flat;
                                tile[x].FlatAppearance.BorderSize = 0;
                                tile[x].Font = buttonEx.Font;
                                tile[x].TextAlign = ContentAlignment.BottomLeft;
                                tile[x].Size = new Size(262, 128);
                                tile[x].Location = freeSpace[0];
                                flags.Add(x);
                                freeSpace.RemoveAt(0);
                                DoOnUIThread(delegate()
                                {
                                    panel3.Controls.Add(tile[x]);
                                });
                                x++;
                            }
                        }
                    }
                    else if (text[i] == "2")
                    {
                        if (i == 0)
                        {
                            tile[x] = new Button();
                            tile[x].Text = text[i + 1];
                            tile[x].Tag = text[i + 2];
                            pictureBox2.Load(text[i + 3]); tile[x].BackgroundImage = pictureBox2.Image;
                            string[] cul = System.Text.RegularExpressions.Regex.Split(text[i + 4], ",");
                            tile[x].ForeColor = Color.FromArgb(Convert.ToInt32(cul[0]), Convert.ToInt32(cul[1]), Convert.ToInt32(cul[2]));
                            string[] cul2 = System.Text.RegularExpressions.Regex.Split(text[i + 5], ","); tile[x].BackColor = Color.FromArgb(Convert.ToInt32(cul2[0]), Convert.ToInt32(cul2[1]), Convert.ToInt32(cul2[2]));
                            tile[x].Click += new EventHandler(tile_Click);
                            tile[x].FlatStyle = FlatStyle.Flat;
                            tile[x].FlatAppearance.BorderSize = 0;
                            tile[x].Font = buttonEx.Font;
                            tile[x].TextAlign = ContentAlignment.BottomLeft;
                            tile[x].Size = new Size(262, 262);
                            tile[x].Location = new Point(3, 3);
                            DoOnUIThread(delegate()
                            {
                                panel3.Controls.Add(tile[x]);
                            });
                            x++;
                        }
                        else
                        {
                            tile[x] = new Button();
                            tile[x].Text = text[i + 1];
                            tile[x].Tag = text[i + 2];
                            pictureBox2.Load(text[i + 3]); tile[x].BackgroundImage = pictureBox2.Image;
                            string[] cul = System.Text.RegularExpressions.Regex.Split(text[i + 4], ",");
                            tile[x].ForeColor = Color.FromArgb(Convert.ToInt32(cul[0]), Convert.ToInt32(cul[1]), Convert.ToInt32(cul[2]));
                            string[] cul2 = System.Text.RegularExpressions.Regex.Split(text[i + 5], ","); tile[x].BackColor = Color.FromArgb(Convert.ToInt32(cul2[0]), Convert.ToInt32(cul2[1]), Convert.ToInt32(cul2[2]));
                            tile[x].Click += new EventHandler(tile_Click);
                            tile[x].FlatStyle = FlatStyle.Flat;
                            tile[x].FlatAppearance.BorderSize = 0;
                            tile[x].Font = buttonEx.Font;
                            tile[x].TextAlign = ContentAlignment.BottomLeft;
                            tile[x].Size = new Size(262, 262);
                            int l = x - 1;
                            while (flags.Contains(l)) l--;
                            if (tile[l].Height == 128)
                            {
                                if (tile[l].Location.Y == 3)
                                {
                                    tile[x].Location = new Point(tile[l].Location.X, tile[l].Location.Y + 128 + 6);
                                }
                                else if (tile[l].Location.Y == 3 + 128 + 6)
                                {
                                    tile[x].Location = new Point(tile[l].Location.X + 262 + 6, 3);
                                    freeSpace.Add(new Point(tile[l].Location.X, tile[l].Location.Y + 128 + 6));
                                }
                                else if (tile[l].Location.Y == 3 + 128 * 2 + 6 * 2)
                                {
                                    tile[x].Location = new Point(tile[l].Location.X + 262 + 6, 3);
                                }
                            }
                            else if (tile[l].Height == 262)
                            {
                                tile[x].Location = new Point(tile[l].Location.X + 262 + 6, 3);
                                freeSpace.Add(new Point(tile[l].Location.X, tile[l].Location.Y + 262 + 6));
                            }
                            DoOnUIThread(delegate()
                            {
                                panel3.Controls.Add(tile[x]);
                            });
                            x++;
                        }
                    }
                }
                int v = x - 1;
                while (flags.Contains(v)) v--;
                DoOnUIThread(delegate() { try { panel3.Width = tile[v].Location.X + 262 + 6; } catch { } });
            }
            catch
            {
                DoOnUIThread(delegate() {
                button7.Visible = true;
                button7.BringToFront();
                });
            }
        }
        private void tile_Click(object sender, EventArgs e)
        {
            var Button = (Button)sender;
            try
            {
                System.Diagnostics.Process.Start(Button.Tag.ToString());
            }
            catch { }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                System.Media.SystemSounds.Asterisk.Play();
            }
            catch { }
        }

        private void Close_MouseDown(object sender, MouseEventArgs e)
        {
            Close.BackgroundImage = CloseLightPressed.BackgroundImage;

        }

        private void Close_MouseLeave(object sender, EventArgs e)
        {
            Close.BackgroundImage = CloseLight.BackgroundImage;

        }

        private void Close_MouseEnter(object sender, EventArgs e)
        {
            Close.BackgroundImage = CloseLightHover.BackgroundImage;
        }
    }
}