﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class CreateEXEWizard : Form
    {
        string AppName;
        string CreateEXEError;
        string Finish;
        string CompileError;
        MainForm par;
        public CreateEXEWizard(MainForm mf)
        {
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            InitializeComponent();
            if (Properties.Settings.Default.UILang == 0)
            {
                CreateEXEError = "An error has occured when creating the EXE file.\n\nReview the information provided in the fields and try again.";
                CompileError = "The application cannot be created becuase the pseudocode file contains errors.\n\nCancel this wizard and switch back to the editor, correct the errors in the pseudocode file and try running this wizard again.";
                AppName = "Pseudocode";
                Finish = "Finish";
            }
            else
            {
                CreateEXEError = "A apărut o eroare la crearea fișierului EXE.\n\nRevizuiți câmpurile și încercați din nou.";
                AppName = "Pseudocode";
                Finish = "Finalizare";
                CompileError = "Aplicația nu a putut fi creată deoarece pseudocodul conține erori.\n\nÎnchideți acest expert, comutați înapoi la editor și corectați erorile din pseudocod, apoi încercați executarea acestui expert din nou.";
            }
            if (Properties.Settings.Default.AppName != "")
            {
                AppName = Properties.Settings.Default.AppName;
            }
            par = mf;
            button3.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Step1.Visible == true)
            {
                button3.Enabled = true;
                Step1.Visible = false;
                Step2.Visible = true;
                Step3.Visible = false;
                Step4.Visible = false;
                button2.Enabled = true;
                checkBox1.Checked = Properties.Settings.Default.AlwaysLoadProperties;
                if (checkBox1.Checked) button4.PerformClick();
                return;
            }
            if (Step2.Visible == true)
            {
                Properties.Settings.Default.AlwaysLoadProperties = checkBox1.Checked;

                button2.Enabled = false;
                Step1.Visible = false;
                Step2.Visible = false;
                Step4.Visible = false;
                Step3.Visible = true;
                progressBar1.Style = ProgressBarStyle.Marquee;
                if (EXEName.Text.Contains("\r")) EXEName.Text = EXEName.Text.Replace("\r", "");
                if (EXECopyright.Text.Contains("\r")) EXECopyright.Text = EXECopyright.Text.Replace("\r", "");
                if (EXEInternalName.Text.Contains("\r")) EXEInternalName.Text = EXEInternalName.Text.Replace("\r", "");
                if (EXEVersion.Text.Contains("\r")) EXEVersion.Text = EXEVersion.Text.Replace("\r", "");
                if (EXEProductName.Text.Contains("\r")) EXEProductName.Text = EXEProductName.Text.Replace("\r", "");
                if (EXECompanyName.Text.Contains("\r")) EXECompanyName.Text = EXECompanyName.Text.Replace("\r", "");
                if (EXEIcon.Text.Contains("\r")) EXEIcon.Text = EXEIcon.Text.Replace("\r", "");
                string text = "";
                if (EXEIcon.Text == "")
                {
                    text += "id ICON " + "\"" + (Application.StartupPath + "\\test.ico").Replace('\\', '/') + "\"\r\n";
                }
                else
                {
                    text += "id ICON " + "\"" + EXEIcon.Text.Replace('\\', '/') + "\"\r\n";
                }
                if (EXEVersion.Text == "") EXEVersion.Text = "1,0,0,0";
                text += "1 VERSIONINFO\r\nFILEVERSION     " + EXEVersion.Text + "\r\n";
                text += "PRODUCTVERSION  " + EXEVersion.Text + "\r\n";
                text += "BEGIN\r\n  BLOCK \"StringFileInfo\"\r\n  BEGIN\r\n    BLOCK \"080904E4\"\r\n    BEGIN\r\n      VALUE \"CompanyName\", \"" + EXECompanyName.Text + "\"" + "\r\n";
                text += "      VALUE \"FileDescription\", \"" + EXEName.Text + "\"\r\n";
                text += "      VALUE \"FileVersion\", \"" + EXEVersion.Text + "\"\r\n";
                text += "      VALUE \"InternalName\", \"" + EXEInternalName.Text + "\"\r\n";
                text += "      VALUE \"LegalCopyright\", \"" + EXECopyright.Text + "\"\r\n";
                text += "      VALUE \"OriginalFilename\", \"Pseudocode.exe\"\r\n";
                text += "      VALUE \"ProductName\", \"" + EXEProductName.Text + "\"\r\n";
                text += "      VALUE \"ProductVersion\", \"" + Application.ProductVersion.ToString() + "\"\r\n";
                text += "    END\r\n  END\r\n\r\n  BLOCK \"VarFileInfo\"\r\n  BEGIN\r\n    VALUE \"Translation\", 0x809, 1252\r\n  END\r\nEND";
                try
                {
                    if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc");

                StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc");
                sw.Write(text);
                sw.Close();
                backgroundWorker1.RunWorkerAsync();
                }
                catch (Exception ex)
                {
                    CasetaDeMesaj(par, CreateEXEError + "\n" + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Step1.Visible = false;
                    Step2.Visible = true;
                    Step4.Visible = false;
                    Step3.Visible = false;
                    button1.Enabled = true;
                    button2.Enabled = true;
                    button3.Enabled = true;
                }

                return;
            }
            this.Close();
        }
        static void cmd_DataReceived(object sender, DataReceivedEventArgs e)
        {
            try
            {
                MessageBox.Show(e.Data.ToString(), sender.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch { }
        }
        bool wasError = false;
                    Process processPSC;
                Process processPSC2;
                class Car
        {
            public int Linie { get; set; }
            public string Mesaj { get; set; }
        }
                List<Car> cars;
                void processPSC5_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {

            DoOnUIThread(delegate()
            {
                try
                {
                    CasetaDeMesaj(par, CreateEXEError + "\n" + e.Data.ToString(), AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    wasError = true;
                    this.Focus();
                }
                catch { }
            });
                }
                            public DialogResult CasetaDeMesaj(MainForm form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
            msg.ShowDialog();
            return msg.DialogResult;
        }
        
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            wasError = false;
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
            cmdStartInfo.Arguments = "-g -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.exe" + "\" \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp\"";
            cmdStartInfo.RedirectStandardOutput = true;
            cmdStartInfo.RedirectStandardError = true;
            cmdStartInfo.RedirectStandardInput = true;
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            processPSC = new Process();
            processPSC.StartInfo = cmdStartInfo;
            processPSC.ErrorDataReceived += cmd_Error;
            processPSC.OutputDataReceived += cmd_DataReceived;
            cars = new List<Car>();
            processPSC.Start(); //"cscript.exe", "//Nologo " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.vbs");
            processPSC.BeginOutputReadLine();
            processPSC.BeginErrorReadLine();
            processPSC.WaitForExit();
            if (cars.Count == 0)
            {
                //new code which uses the MinGW c++ compiler
                ProcessStartInfo cmdStartInfo3 = new ProcessStartInfo();
                cmdStartInfo3.FileName = "cmd"; // Properties.Settings.Default.MinGWPath + @"\bin\windres.exe";
                cmdStartInfo3.Arguments = " " + System.IO.Path.GetPathRoot(Properties.Settings.Default.MinGWPath).Substring(0, 2); //"\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc\"" + " -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res\"";
                cmdStartInfo3.RedirectStandardInput = true;
                cmdStartInfo3.RedirectStandardError = true;
                cmdStartInfo3.RedirectStandardOutput = true;
                cmdStartInfo3.UseShellExecute = false;
                //cmdStartInfo3.CreateNoWindow = true;
                Process processPSC5 = new Process();
                processPSC5.StartInfo = cmdStartInfo3;
                processPSC5.ErrorDataReceived += processPSC5_ErrorDataReceived;
                //processPSC5.OutputDataReceived += processPSC5_ErrorDataReceived;
                processPSC5.Start();
                processPSC5.BeginErrorReadLine();
                processPSC5.StandardInput.WriteLine("cd \"" + Properties.Settings.Default.MinGWPath + "\\bin\"");
                processPSC5.StandardInput.WriteLine("windres.exe " + "\"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc\"" + " -O coff -o \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res\"");
                processPSC5.StandardInput.WriteLine("exit");
                processPSC5.WaitForExit();
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res"))
                {
                    ProcessStartInfo cmdStartInfo2 = new ProcessStartInfo();
                    cmdStartInfo2.FileName = Properties.Settings.Default.MinGWPath + @"\bin\g++.exe";
                    cmdStartInfo2.Arguments = "-o \"" + EXELocation.Text + "\" \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\temp.cpp" + "\" \"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res\"";
                    cmdStartInfo2.CreateNoWindow = true;
                    processPSC2 = new Process();
                    processPSC2.StartInfo = cmdStartInfo2;
                    processPSC2.Start();
                    processPSC2.WaitForExit();

                }
                else
                {
                    /*                   DoOnUIThread(delegate()
                   {
                   CasetaDeMesaj(this, CreateEXEError, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                   });*/
                }
                DoOnUIThread(delegate()
                   {
                       if (wasError == false)
                       {
                           Step1.Visible = false;
                           Step2.Visible = false;
                           Step3.Visible = false;
                           Step4.Visible = true;
                           button2.Enabled = true;
                           button2.Text = "Finalizare";
                           button1.Enabled = false;
                           button3.Enabled = false;
                       }
                       else
                       {
                           Step1.Visible = false;
                           Step2.Visible = true;
                           Step3.Visible = false;
                           Step4.Visible = false;
                           button3.Enabled = true;
                           button2.Enabled = true;
                           button1.Enabled = true;
                       }
                   });
            }
            else
            {
                DoOnUIThread(delegate()
                {
                    button2.Enabled = true;
                    button3.Enabled = true;
                CasetaDeMesaj(par, CompileError, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Step1.Visible = false;
                Step2.Visible = true;
                Step3.Visible = false;
                Step4.Visible = false;
                });
            }
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.res");
            Properties.Settings.Default.EXELocation = EXELocation.Text;
            Properties.Settings.Default.Save();
        }
        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }
        private void cmd_Error(object sender, DataReceivedEventArgs e)
        {

            if (e.Data != "" & e.Data != null)
            {

                cars.Add(new Car() { Linie = 0, Mesaj = "error" });
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Step2.Visible == true)
            {
                Step1.Visible = true;
                Step2.Visible = false;
                Step3.Visible = false;
                Step4.Visible = false;
                button3.Enabled = false;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(EXELocation.Text);
            }
            catch
            {
                CasetaDeMesaj(par, CreateEXEError, AppName,MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(Path.GetDirectoryName(EXELocation.Text));
            }
            catch
            {
                CasetaDeMesaj(par, CreateEXEError, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void Step4_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                EXELocation.Focus();
                //highlighter1.UpdateHighlights();
                if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc")) File.Copy(Application.StartupPath + "\\test.rc", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc");
                StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\test.rc");
                string text = sr.ReadToEnd();
                sr.Close();
                string[] textRC = text.Split('\n');
                foreach (string st in textRC)
                {
                    if (st.StartsWith("id ICON ")) EXEIcon.Text = Regex.Replace(st, "id ICON ", "").Replace("\"", "");
                    if (st.StartsWith("      VALUE \"CompanyName\", ")) EXECompanyName.Text = Regex.Replace(st, "      VALUE \"CompanyName\", ", "").Replace("\"", "");
                    if (st.StartsWith("      VALUE \"FileDescription\", ")) EXEName.Text = Regex.Replace(st, "      VALUE \"FileDescription\", ", "").Replace("\"", "");
                    if (st.StartsWith("      VALUE \"InternalName\", ")) EXEInternalName.Text = Regex.Replace(st, "      VALUE \"InternalName\", ", "").Replace("\"", "");
                    if (st.StartsWith("      VALUE \"LegalCopyright\", ")) EXECopyright.Text = Regex.Replace(st, "      VALUE \"LegalCopyright\", ", "").Replace("\"", "");
                    if (st.StartsWith("      VALUE \"ProductName\", ")) EXEProductName.Text = Regex.Replace(st, "      VALUE \"ProductName\", ", "").Replace("\"", "");
                    if (st.StartsWith("FILEVERSION     ")) EXEVersion.Text = Regex.Replace(st, "FILEVERSION     ", "");
                }
                EXELocation.Text = Properties.Settings.Default.EXELocation;
            }
            catch (Exception ex)
            {
                CasetaDeMesaj(par, CreateEXEError + "\n" + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void button40_Click(object sender, EventArgs e)
        {
            if (saveFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                EXELocation.Text = saveFileDialog2.FileName;
            }
            else
            {

            }
        }

        private void button39_Click(object sender, EventArgs e)
        {
            if (openFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                EXEIcon.Text = openFileDialog2.FileName;
            }
            else
            {

            }
        }

        private void CreateEXEWizard_Load(object sender, EventArgs e)
        {
            this.AcceptButton = button2;
        }

    }
}
