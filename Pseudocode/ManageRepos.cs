﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Controls.Development;
using System.Text.RegularExpressions;
using System.Diagnostics;
using ValiNet.PseudocodePluginsFramework;

namespace Pseudocode
{
    public partial class ManageRepos : Form
    {
        MainForm frm;
        string SureToUninstall;
        string AppName;
        string DoneUninstall;
        string Error;
        string DefaultSearchBoxText;
        string UnidentifiedPackage;
        string NoBrowserInstalled;
        string SuccessfullyInstalled;
        string ErrorInstalled;
        string OverwriteInstall;
        string RestartNeeded;
        List<string> ls;
        List<string> adr;
        List<bool> restart;
        Dictionary<string, VPlugin> _Plugins;
        public ManageRepos(MainForm mf)
        {
            InitializeComponent();
            frm = mf;
            if (Properties.Settings.Default.UILang == 0)
            {
                SureToUninstall = "Are you sure you want to uninstall the following package?\n\n";
                DoneUninstall = "The selected package has been successfully uninstalled.";
                Error = "An error has occured while uninstalling the package:\n\n";
                DefaultSearchBoxText = "Search packages (Ctrl+Q)";
                UnidentifiedPackage = "Do you want to uninstall the selected package?\n\nThe selected package is no longer installed or its installation is damaged.";
                NoBrowserInstalled = "There is no Internet browser installed on this computer.\n\nIn order to use this feature, a browser has to be installed on your machine. Visit www.browserchoice.eu to download a web browser for your computer if you are based in the European Union or, if located in any other country, visit www.whatbrowser.org.";
                //SuccessfullyInstalled = "Do you want to restart the application?\n\nThe package has been successfully installed, but the application has to be restarted. Save your work before proceeding.";
                SuccessfullyInstalled = "Installation of the opened packages has finished. Check Tools - Extensions and Updates for information about the available packages.";
                ErrorInstalled = "An error has occured while attempting to install the package.\n\n";
                OverwriteInstall = "A package with the same name is already installed.\n\nUninstall that first before attempting to install this replacement.";
                RestartNeeded = "The application will restart. Save your work before continuing.";
            }
            if (Properties.Settings.Default.UILang ==1)
            {
                SureToUninstall = "Sigur dezinstalați următorul pachet?\n\n";
                DoneUninstall = "Pachetul selectat a fost dezinstalat cu succes.";
                Error = "A apărut o eroare la dezinstalarea pachetului:\n\n";
                DefaultSearchBoxText = "Căutare în pachete (Ctrl+Q)";
                UnidentifiedPackage = "Dezinstalați pachetul selectat?\n\nPachetul selectat nu mai este instalat sau instalarea sa este deteriorată.";
                NoBrowserInstalled = "Niciun browser Web nu este instalat pe acest computer.\n\nPentru a utiliza această caracteristică, un browser trebuie să fie instalat pe computer. Pentru a descărca un browser, vizitați www.browserchoice.eu dacă sunteți în Uniunea Europeană sau www.whatbrowser.org dacă sunteți din oricare altă țară.";
                //SuccessfullyInstalled = "Reporniți aplicația?\n\nPachetul s-a instalat cu succes, însă aplicația trebuie repornită pentru a încărca noul pachet. Salvați-vă munca înainte de a continua.";
                SuccessfullyInstalled = "Instalarea pachetelor deschise s-a finalizat. Mergeți la Unelte - Extensii  și Actualizări pentru a verifica starea pachetelor instalate.";
                ErrorInstalled = "S-a produs o eroare la instalarea pachetului.\n\n";
                OverwriteInstall = "Un pachet cu un nume similar este deja instalat.\n\nDezinstalați acel pachet înainte de a încerca instalarea acestui înlocuitor.";
                RestartNeeded = "Aplicația va reporni. Salvați-vă munca înainte de a continua.";
            }
            if (Properties.Settings.Default.UILang == 0)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
                if (Properties.Settings.Default.AppName != "") AppName = Properties.Settings.Default.AppName;
                else AppName = "Pseudocode";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
                if (Properties.Settings.Default.AppName != "") AppName = Properties.Settings.Default.AppName;
                else AppName = "Pseudocode";
            }
            _Plugins = new Dictionary<string, VPlugin>();
            //ICollection<IPlugin> plugins = PluginLoader.LoadPlugins("Plugins");
            ICollection<VPlugin> plugins = GenericPluginLoader<VPlugin>.LoadPlugins(Application.StartupPath);
            try
            {
                foreach (var item in plugins)
                {
                    _Plugins.Add(item.Name, item);
                }
            }
            catch { }
        }
        public void LoadS(string what = "*")
        {
            StreamReader sr = new StreamReader(Application.StartupPath + "\\package-list.txt");
            string[] pachete = Regex.Split(sr.ReadToEnd(), "\r\n");
            sr.Close();
            foreach (string st in pachete)
            {
                if (st != "")
                {
                    if (what == "*") listBox1.Items.Add(new ImageListBoxItem(st));
                    else if (st.Contains(what)) listBox1.Items.Add(new ImageListBoxItem(st));
                }
            }
        }
        public void CheckUpdates()
        {
            try
            {
                checkUpd.RunWorkerAsync();
            }
            catch { }
        }
        private void ManageRepos_Load(object sender, EventArgs e)
        {
            //SearchBox.Text = DefaultSearchBoxText;
            listBox1.ImageList = im;
            LoadS();
            CheckUpdates();
            listBox1.SelectedIndex = 0;
            listBox1.Focus();
            this.ActiveControl = listBox1;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string lang = "";
                if (Properties.Settings.Default.UILang == 0) lang = "en";
                if (Properties.Settings.Default.UILang == 1) lang = "ro";
                StreamReader sr = new StreamReader(Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text + "\\version.txt");
                string[] infos = Regex.Split(sr.ReadToEnd(), "\r\n");
                three.Text = infos[0];
                four.Text = infos[1];
                sr.Close();
                StreamReader sr2 = new StreamReader(Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text + "\\" + lang + "\\description.txt");
                string[] infos2 = Regex.Split(sr2.ReadToEnd(), "\r\n");
                one.Text = infos2[0];
                textBox1.Text = infos2[1];
                sr2.Close();
                two.Text = listBox1.Items[listBox1.SelectedIndex].Text;
                button1.Enabled = Convert.ToBoolean(infos[3]);
                button4.Tag = infos[2];
                if (listBox1.Items[listBox1.SelectedIndex].ImageIndex == 1) button4.Visible = true;
                else button4.Visible = false;
                if (File.Exists(Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text + "\\" + listBox1.Items[listBox1.SelectedIndex].Text + ".dll")) button5.Visible = true;
                else button5.Visible = false;
            }
            catch
            {
                if (listBox1.SelectedIndex != -1)
                {
                    if (File.Exists(Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text + "\\" + listBox1.Items[listBox1.SelectedIndex].Text + ".dll"))
                    {
                        if (CasetaDeMesaj(frm, UnidentifiedPackage + "\n" + RestartNeeded, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            StreamReader sr = new StreamReader(Application.StartupPath + "\\" + "package-list.txt");
                            string[] text = Regex.Split(sr.ReadToEnd(), "\r\n");
                            sr.Close();
                            StreamWriter sw = new StreamWriter(Application.StartupPath + "\\" + "package-list.txt");
                            foreach (string st in text)
                            {
                                if (st != listBox1.Items[listBox1.SelectedIndex].Text) sw.WriteLine(st);
                            }
                            sw.Close();
                            Properties.Settings.Default.AddonToBeErased = Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text;
                            Properties.Settings.Default.Save();
                            System.Diagnostics.Process.Start(Application.ExecutablePath);
                            Process.GetCurrentProcess().Kill();
                        }
                        return;
                    }
                    DialogResult dr = CasetaDeMesaj(frm, UnidentifiedPackage, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            if (Directory.Exists(Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text)) Directory.Delete(Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text, true);
                            StreamReader sr = new StreamReader(Application.StartupPath + "\\" + "package-list.txt");
                            string[] text = Regex.Split(sr.ReadToEnd(), "\r\n");
                            sr.Close();
                            StreamWriter sw = new StreamWriter(Application.StartupPath + "\\" + "package-list.txt");
                            foreach (string st in text)
                            {
                                if (st != listBox1.Items[listBox1.SelectedIndex].Text) sw.WriteLine(st);
                            }
                            sw.Close();
                            CasetaDeMesaj(frm, DoneUninstall, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch (Exception ex)
                        {
                            CasetaDeMesaj(frm, Error + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    listBox1.Items.Clear();
                    LoadS();
                    CheckUpdates();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (File.Exists(Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text + "\\" + listBox1.Items[listBox1.SelectedIndex].Text + ".dll"))
            {
                if (CasetaDeMesaj(frm, SureToUninstall + listBox1.Items[listBox1.SelectedIndex].Text + "\n" + RestartNeeded, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    StreamReader sr = new StreamReader(Application.StartupPath + "\\" + "package-list.txt");
                    string[] text = Regex.Split(sr.ReadToEnd(), "\r\n");
                    sr.Close();
                    StreamWriter sw = new StreamWriter(Application.StartupPath + "\\" + "package-list.txt");
                    foreach (string st in text)
                    {
                        if (st != listBox1.Items[listBox1.SelectedIndex].Text) sw.WriteLine(st);
                    }
                    sw.Close();
                    Properties.Settings.Default.AddonToBeErased = Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text;
                    Properties.Settings.Default.Save();
                    System.Diagnostics.Process.Start(Application.ExecutablePath);
                    Process.GetCurrentProcess().Kill();
                }
                return;
            }
            if (CasetaDeMesaj(frm, SureToUninstall + listBox1.Items[listBox1.SelectedIndex].Text, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    if (Directory.Exists(Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text)) Directory.Delete(Application.StartupPath + "\\" + listBox1.Items[listBox1.SelectedIndex].Text, true);
                    StreamReader sr = new StreamReader(Application.StartupPath + "\\" + "package-list.txt");
                    string[] text = Regex.Split(sr.ReadToEnd(), "\r\n");
                    sr.Close();
                    StreamWriter sw = new StreamWriter(Application.StartupPath + "\\" + "package-list.txt");
                    foreach (string st in text)
                    {
                        if (st != listBox1.Items[listBox1.SelectedIndex].Text) sw.WriteLine(st);
                    }
                    sw.Close();
                    CasetaDeMesaj(frm, DoneUninstall, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    listBox1.Items.Clear();
                    LoadS();
                    listBox1.SelectedIndex = 0;
                    listBox1.Focus();
                }
                catch (Exception ex)
                {
                    CasetaDeMesaj(frm, Error + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }
        [System.Runtime.InteropServices.DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }

        private void SearchBox_Enter(object sender, EventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText) SearchBox.Text = "";

        }
        private void DisplayHScroll()
        {
            // Make no partial items are displayed vertically.
            listBox1.IntegralHeight = true;



            // Display a horizontal scroll bar.
            listBox1.HorizontalScrollbar = true;

            // Create a Graphics object to use when determining the size of the largest item in the ListBox.
            Graphics g = listBox1.CreateGraphics();
            int maxim = 0;
            foreach (Controls.Development.ImageListBoxItem it in listBox1.Items)
            {
                if (maxim < (int)g.MeasureString(it.ToString(), listBox1.Font).Width) maxim = (int)g.MeasureString(it.ToString(), listBox1.Font).Width;
            }
            // Determine the size for HorizontalExtent using the MeasureString method using the last item in the list. 
            // Set the HorizontalExtent property.
            listBox1.HorizontalExtent = maxim;
            g.Dispose();
        }
        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText)
            {
                SearchBox.Text = "";
                DisplayHScroll();
            }
            if (e.KeyCode == Keys.Enter) e.SuppressKeyPress = true;
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                SearchBox.Text = DefaultSearchBoxText;
                DisplayHScroll();
            }
        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                try
                {
                    listBox1.SelectedIndex = 0;
                }
                catch { }
                listBox1.Focus();
                e.SuppressKeyPress = true;
                return;
            }
            listBox1.Tag = SearchBox.Text;
            if (SearchBox.Text != "" && SearchBox.Text != DefaultSearchBoxText)
            {
                listBox1.HorizontalScrollbar = true;
                listBox1.Items.Clear();
                LoadS(SearchBox.Text);
                DisplayHScroll();
            }
            else
            {
                
                listBox1.Items.Clear();
                LoadS();
                DisplayHScroll();
            }
        }

        private void SearchBox_Leave(object sender, EventArgs e)
        {
            if (listBox1.Focused == false)
            {
                SearchBox.Text = DefaultSearchBoxText;
                listBox1.Items.Clear();
                LoadS();
                listBox1.SelectedIndex = 0;
                DisplayHScroll();
            }
            if (SearchBox.Text == "")
            {
                SearchBox.Text = DefaultSearchBoxText;
                CheckUpdates();
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.Q))
            {
                SearchBox.Text = "";
                SearchBox.Focus();
                return true;
            }
            return false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkUpd_DoWork(object sender, DoWorkEventArgs e)
        {
            ls = new List<string>();
            adr = new List<string>();
            restart = new List<bool>();
            System.Net.WebClient wc = new System.Net.WebClient();
            try
            {
                foreach (ImageListBoxItem it in listBox1.Items)
                {
                    try
                    {
                        StreamReader sr2 = new StreamReader(Application.StartupPath + "\\" + it.Text + "\\version.txt");
                        string[] infos = Regex.Split(sr2.ReadToEnd(), "\r\n");
                        sr2.Close();
                        try
                        {
                            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                        }
                        catch { }
                        if (it.Text == "com.valinet.pseudocode") wc.DownloadFile(Properties.Settings.Default.ServerPath + "/" + it.Text + "/version.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                        else wc.DownloadFile(infos[2] + "/" + it.Text + "/version.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                        StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                        string[] citit = Regex.Split(sr.ReadToEnd(), "\n");
                        sr.Close();
                        DoOnUIThread(delegate()
                        {
                            try
                            {
                                if (citit[0] == infos[0]) it.ImageIndex = 0;
                                else it.ImageIndex = 1;
                                ls.Add(citit[0]);
                                adr.Add(citit[1]);
                                restart.Add(Convert.ToBoolean(citit[2]));
                                if (it.Index == listBox1.SelectedIndex)
                                {
                                    if (listBox1.Items[listBox1.SelectedIndex].ImageIndex == 1) button4.Visible = true;
                                    else button4.Visible = false;
                                }
                            }
                            catch
                            {
                                DoOnUIThread(delegate()
                                {
                                    it.ImageIndex = 2;
                                });
                            }
                        });
                    }
                    catch
                    {
                        DoOnUIThread(delegate()
                        {
                            it.ImageIndex = 2;
                        });
                    }
                }
            }
            catch { }
            DoOnUIThread(delegate()
            {
                listBox1.Refresh();
                
            });
        }
        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CheckUpdates();
        }
        public bool IsUserAdministrator()
        {
            if (Application.StartupPath.Contains("Program Files"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            string lang = "";
            if (Properties.Settings.Default.UILang == 0) lang = "en";
            if (Properties.Settings.Default.UILang == 1) lang = "ro";
            if (listBox1.Items[listBox1.SelectedIndex].Text == "com.valinet.pseudocode")
            {
                System.Net.WebClient wc = new System.Net.WebClient();
                wc.DownloadFile(Properties.Settings.Default.ServerPath + "/" + listBox1.Items[listBox1.SelectedIndex].Text + "/" + lang + "/" + "description" + ".txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt", Encoding.Unicode);
                string text = sr.ReadToEnd();
                sr.Close();
                UpdateNow up = new UpdateNow(frm, ls[listBox1.SelectedIndex], adr[listBox1.SelectedIndex], text, Properties.Settings.Default.ServerPath + "" + "com.valinet.pseudocode" + "/" + "com.valinet.pseudocode", restart[listBox1.SelectedIndex]);
                up.ShowDialog();
                int a = listBox1.SelectedIndex;
                listBox1.SelectedIndex = 0;
                listBox1.SelectedIndex = a;
                CheckUpdates();
            }
            else
            {
                System.Net.WebClient wc = new System.Net.WebClient();
                wc.DownloadFile(button4.Tag + "/" + listBox1.Items[listBox1.SelectedIndex].Text + "/" + lang + "/" + "description" + ".txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt", Encoding.Unicode);
                string text = sr.ReadToEnd();
                sr.Close();
                UpdateNow up = new UpdateNow(frm, ls[listBox1.SelectedIndex], adr[listBox1.SelectedIndex], text, button4.Tag.ToString() + "/" + listBox1.Items[listBox1.SelectedIndex].Text + "/" + listBox1.Items[listBox1.SelectedIndex].Text, restart[listBox1.SelectedIndex]);
                up.ShowDialog();
                int a = listBox1.SelectedIndex;
                listBox1.SelectedIndex = 0;
                listBox1.SelectedIndex = a;
                CheckUpdates();
            }
        }

        private void four_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(four.Text);
            }
            catch {
                CasetaDeMesaj(frm, NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
                string key = listBox1.Items[listBox1.SelectedIndex].Text;
                if (_Plugins.ContainsKey(key))
                {
                    VPlugin plugin = _Plugins[key];
                    plugin.ExtensionOptions();
                }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Point pt = button6.PointToScreen(Point.Empty);
            contextMenuStrip1.Show(new Point(pt.X, pt.Y + button6.Height));
        }
        public static void UnZip(string zipFile, string folderPath)
        {
            if (!File.Exists(zipFile))
                throw new FileNotFoundException();

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            Shell32.Shell objShell = new Shell32.Shell();
            Shell32.Folder destinationFolder = objShell.NameSpace(folderPath);
            Shell32.Folder sourceFile = objShell.NameSpace(zipFile);

            foreach (var file in sourceFile.Items())
            {
                destinationFolder.CopyHere(file, 4 | 16);
            }
        }
        private static void DirectoryCopy(
        string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }


            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                file.CopyTo(temppath, true);
            }

            // If copySubDirs is true, copy the subdirectories.
            if (copySubDirs)
            {

                foreach (DirectoryInfo subdir in dirs)
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine(destDirName, subdir.Name);

                    // Copy the subdirectories.
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
        private void fromOfflinedebFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    File.Move(openFileDialog1.FileName, Path.GetDirectoryName(openFileDialog1.FileName) + "\\" + Path.GetFileNameWithoutExtension(openFileDialog1.FileName) + ".zip");
                    if (Directory.Exists(Application.StartupPath + "\\ext-temp")) Directory.Delete(Application.StartupPath + "\\ext-temp", true);
                    Directory.CreateDirectory(Application.StartupPath + "\\ext-temp");
                    UnZip(Path.GetDirectoryName(openFileDialog1.FileName) + "\\" + Path.GetFileNameWithoutExtension(openFileDialog1.FileName) + ".zip", Application.StartupPath + "\\ext-temp");
                    string folder = Directory.GetDirectories(Application.StartupPath + "\\ext-temp")[0];
                    StreamReader sr = new StreamReader(folder + "\\" + "version.txt");
                    string version = Regex.Split(sr.ReadToEnd(), "\r\n")[0];
                    sr.Close();
                    StreamReader sr2 = new StreamReader(Application.StartupPath + "\\package-list.txt");
                    string pkglist = sr2.ReadToEnd();
                    string[] pkg = Regex.Split(pkglist, "\r\n");
                    sr2.Close();
                    string oldversion = "";
                    try
                    {
                        StreamReader sr3 = new StreamReader(Application.StartupPath + "\\" + Path.GetFileName(folder) + "\\version.txt");
                        oldversion = Regex.Split(sr3.ReadToEnd(), "\r\n")[0];
                        sr3.Close();
                    }
                    catch
                    {

                    }
                    bool already = false;
                    foreach(string st in pkg)
                    {
                        if (st == Path.GetFileName(folder))
                        {
                            DialogResult dr = CasetaDeMesaj(frm, OverwriteInstall, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            already = true;
                            goto aici;
                           // if (dr == System.Windows.Forms.DialogResult.No) return;
                            
                           // break;
                        }
                    }
                    DirectoryCopy(Application.StartupPath + "\\ext-temp" + "\\" + Path.GetFileName(folder), Application.StartupPath + "\\" + Path.GetFileName(folder), true);
                    Directory.Delete(Application.StartupPath + "\\ext-temp", true);
                   // if (already == false)
                   // {
                        StreamWriter sw = new StreamWriter(Application.StartupPath + "\\package-list.txt");
                        sw.Write(pkglist + "\r\n" + Path.GetFileName(folder));
                        sw.Close();
                   // }
                    aici:
                    File.Move(Path.GetDirectoryName(openFileDialog1.FileName) + "\\" + Path.GetFileNameWithoutExtension(openFileDialog1.FileName) + ".zip", openFileDialog1.FileName);
                    if (already == false)
                    {
                        frm.New(frm.NewTabTitle + " " + (frm.dockPanel.Panes.Count + 1).ToString());
                        frm.closeFileToolStripMenuItem.PerformClick();
                        DialogResult dr2 = CasetaDeMesaj(frm, SuccessfullyInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                        /*if (dr2 == System.Windows.Forms.DialogResult.Yes)
                        {
                            Process.Start(Application.ExecutablePath);
                            Process.GetCurrentProcess().Kill();
                        }*/
                    }
                }
                catch (Exception ex)
                {
                    CasetaDeMesaj(this, ErrorInstalled + ex.Message , AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    listBox1.Items.Clear();
                    LoadS();
                    listBox1.SelectedIndex = 0;
                    listBox1.Focus();
                    CheckUpdates();
                }
            }
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void originatingFromAnOnlineRepositoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("http://www.valinet.ro/pseudocode/packages");
            }
            catch { }
        }
    }
}
