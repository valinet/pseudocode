﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;

namespace Pseudocode
{
    public partial class Config : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        Form1 form1;
        string unable;
        public Config(Form1 frm)
        {

            InitializeComponent();
            string CloseText = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                CloseText = "Close (Alt+F4)";
                unable = "Unable to open this section right now.\n\nPlease try again in a little while. Detailed error description: ";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                CloseText = "Închidere (Alt+F4)";
                unable = "Nu se poate deschide această secțiune în acest moment.\n\nÎncercați din nou mai târziu. Descriere detaliată eroare: ";
            }
            toolTip1.SetToolTip(Close, CloseText);
            form1 = frm;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            menuStrip1.Enabled = true;
            label1.Visible = false;
            pictureBox1.Visible = true;
            button1.Visible = false;
            pictureBox2.Visible = false;
        }

        private void Config_Load(object sender, EventArgs e)
        {
            Close.BackColor = Color.FromArgb(199, 80, 80);
            /*try
            {
                StreamReader sr = new StreamReader(Application.StartupPath + "\\config.rtf");
                string rtfText = sr.ReadToEnd();
                sr.Close();
                richTextBox1.Rtf = rtfText;
                Close.BackColor = Color.FromArgb(199, 80, 80);
                switchButton1.Value = Properties.Settings.Default.Position;
                comboBox1.SelectedIndex = Properties.Settings.Default.Language;
                numericUpDown1.Value = Properties.Settings.Default.AutoSaveInterval;
                switchButton2.Value = Properties.Settings.Default.AutoSignIn;
                comboBox2.SelectedIndex = Properties.Settings.Default.UILang;
                numericUpDown2.Value = Properties.Settings.Default.Zoom;
                textBox2.Text = Properties.Settings.Default.AppName;
                if (Properties.Settings.Default.UpdateInt == 0) numericUpDown3.Value = 1000;
                else numericUpDown3.Value = Properties.Settings.Default.UpdateInt;
                switchButton3.Value = !Properties.Settings.Default.WasWelcomeShown;
                switchButton4.Value = Properties.Settings.Default.AdvancedUpdatesWarningShown;
                textBox3.Text = Properties.Settings.Default.ServerPath;
                switchButton5.Value = Properties.Settings.Default.UpgradeRequired;
                switchButton6.Value = Properties.Settings.Default.DownloadImages;
                switchButton7.Value = Properties.Settings.Default.UseBar;
                switchButton8.Value = Properties.Settings.Default.ShowOldVBScript;
            }
            catch (Exception ex)
            {
                CasetaDeMesaj(form1, unable + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }*/
        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }
        private void rememberPositionAtStartupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
            int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Synced. Remember window position at start-up. Valid values are 1 to enable this policy (true), and 0 to disable this policy (false). Default value is 1 (true). Current value is " + Properties.Settings.Default.Position.ToString()));
            if (a == 0)
            {
                Properties.Settings.Default.Position = false;
                Properties.Settings.Default.Save();
                CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (a == 1)
            {
                Properties.Settings.Default.Position = true;
                Properties.Settings.Default.Save();
                CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void languageIntegerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Synced. Converted code language. Valid values are 1 for C++, 2 for Basic Classic, 3 for Pascal, 4 for C#, 5 for JavaScript and 6 for VBScript. Default value is 1 for C++. Current value is " + Properties.Settings.Default.Language.ToString()));
                if (a == 1 | a == 2 | a == 3 | a == 4 | a == 5 | a == 6)
                {
                    Properties.Settings.Default.Language = a;
                    Properties.Settings.Default.Save();
                    if (Properties.Settings.Default.Language == 1)
                    {
                            form1.button23.PerformClick();
                    }
                    if (Properties.Settings.Default.Language == 2)
                    {
                            form1.button22.PerformClick();
                    }
                    if (Properties.Settings.Default.Language == 3)
                    {
                            form1.button25.PerformClick();
                    }
                    if (Properties.Settings.Default.Language == 4)
                    {
                            form1.button24.PerformClick();
                    }
                    if (Properties.Settings.Default.Language == 5)
                    {
                            form1.button26.PerformClick();
                    }
                    if (Properties.Settings.Default.Language == 6)
                    {
                            form1.button27.PerformClick();
                    }
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void autoSaveIntervalIntegerToolStripMenuItem_Click(object sender, EventArgs e)
        {
                        try
            {
            int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Synced. AutoSave interval. Valid values are integers between 1 and 60. Default value is 1. Current value is " + Properties.Settings.Default.AutoSaveInterval.ToString()));
            if (a >= 1 && a <= 60)
            {
                Properties.Settings.Default.AutoSaveInterval = a;
                Properties.Settings.Default.Save();
                form1.autosaveint = a;
                CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void automaticallySignInAtApplicationStartupBooleanToolStripMenuItem_Click(object sender, EventArgs e)
        {
                        try
            {
            int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Local. Automatically sign in at application start-up. Valid values are 1 to enable this policy (true), and 0 to disable this policy (false). Default value is 1 (true). Current value is " + Properties.Settings.Default.AutoSignIn.ToString()));
            if (a == 0)
            {
                Properties.Settings.Default.AutoSignIn = false;
                Properties.Settings.Default.Save();
                CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (a == 1)
            {
                Properties.Settings.Default.AutoSignIn = true;
                Properties.Settings.Default.Save();
                CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void userInterfaceLanguageIntegerToolStripMenuItem_Click(object sender, EventArgs e)
        {
                        try
            {
            int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Local. UI language. Valid values are 0 for English/English (United Kingdom/United Kingdom) and 1 for Romanian/română (România/Romania). Default value is 1. Current value is " + Properties.Settings.Default.UILang.ToString()));
            if (a == 0)
            {
                Properties.Settings.Default.UILang = 0;
                Properties.Settings.Default.Save();
                CasetaDeMesaj(this, "Setting saved successfully.\n\nRestart application to see changes.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (a == 1)
            {
                Properties.Settings.Default.UILang = 1;
                Properties.Settings.Default.Save();
                CasetaDeMesaj(this, "Setting saved successfully.\n\nRestart application to see changes.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void zoomLevelIntegerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*            try
            {
            int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Synced. Zoom level. Valid values are integers between -8 and 8. Default value is 0. Current value is " + Properties.Settings.Default.Zoom.ToString()));
            if (a >= -8 && a <= 8)
            {
                Properties.Settings.Default.Zoom = a;
                Properties.Settings.Default.Save();
                form1.sliderItem1.Value = a;
                CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }*/

        }

        private void applicationNameIntegerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string a = Microsoft.VisualBasic.Interaction.InputBox("Local. Product name. Any valid string is allowed. Default value is Pseudocode for UI language 0 and Pseudocod for UI language 1. Leave blank to apply default setting. Current value is " + Properties.Settings.Default.AppName.ToString());
            Properties.Settings.Default.AppName = a;
            Properties.Settings.Default.Save();
            if (a != "") form1.AppName = a;
            else
            {
                if (Properties.Settings.Default.UILang == 0)
                {
                    form1.AppName = "Pseudocode";
                }
                else
                {
                    form1.AppName = "Pseudocod";
                }
            }
            CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void uiCodeConversionUpdateIntervalIntegerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
            int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Local. UI update interval. Valid values are integers between 1 and 30000. Type 0 to apply default setting. Default value is 0. Current value is " + Properties.Settings.Default.UpdateInt.ToString()));
            if (a >= 0 && a <= 30000)
            {
                Properties.Settings.Default.UpdateInt = a;
                Properties.Settings.Default.Save();
                if (a != 0) form1.timer1.Interval = a;
                else form1.timer1.Interval = 1000;
                CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void terminateNowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private void restartApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Application.ExecutablePath);
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.DoEvents();
            About about = new About(form1);
            about.ShowDialog();
        }

        private void killMeWithErrorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int a = 0;
            MessageBox.Show((1 / a).ToString());

            //System.Diagnostics.Debugger.Launch();
            //Environment.FailFast("Artificial (programatical) application crash. No need to worry.");

        }

        private void glassPane1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {

                    ReleaseCapture();

                    SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
                
            }
        }

        private void welcomeDialogWasShownUponSignInBooleanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Synced. Codeboard™ is shown on every application startup. Valid values are 1 to enable this policy (true), and 0 to disable this policy (false). Type 0 to apply default setting. Default value is 0 (False). Current value is " + Properties.Settings.Default.WasWelcomeShown.ToString()));
                if (a == 0)
                {
                    Properties.Settings.Default.WasWelcomeShown = false;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (a == 1)
                {
                    Properties.Settings.Default.WasWelcomeShown = true;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void warnUserWhenHesheIsAboutToAlterAdvancedSettingsBooleanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Local. User was warned when about to modify advanced settings. Valid values are 1 to enable this policy (true), and 0 to disable this policy (false). Type 0 to apply default setting. Default value is 0 (False). Current value is " + Properties.Settings.Default.AdvancedUpdatesWarningShown.ToString()));
                if (a == 0)
                {
                    Properties.Settings.Default.AdvancedUpdatesWarningShown  = false;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (a == 1)
                {
                    Properties.Settings.Default.AdvancedUpdatesWarningShown = true;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void updateServerStringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string a = Microsoft.VisualBasic.Interaction.InputBox("Local. Server address. Any valid string is allowed. Default value is http://www.valinet.ro/repo/official/. Leave blank to apply default setting. Current value is " + Properties.Settings.Default.ServerPath.ToString());
            if (a == "") Properties.Settings.Default.ServerPath = "http://www.valinet.ro/repo/official/";
            else Properties.Settings.Default.ServerPath = a;
            Properties.Settings.Default.Save();
            CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void upgradeSetăriLaUrmătoareaPornireBooleanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Local. Upgrade settings at next startup. Valid values are 1 to enable this policy (true), and 0 to disable this policy (false). Type 1 to apply default setting. Default value is 1 (True). Current value is " + Properties.Settings.Default.UpgradeRequired.ToString()));
                if (a == 0)
                {
                    Properties.Settings.Default.UpgradeRequired = false;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (a == 1)
                {
                    Properties.Settings.Default.UpgradeRequired = true;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void Config_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void downloadAndSetBingImageOfTheDayAsCodeboardBackgroundBooleanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Synced. Download and set Bing image of the day as background in Codeboard™. Valid values are 1 to enable this policy (true), and 0 to disable this policy (false). Type 1 to apply default setting. Default value is 1 (True). Current value is " + Properties.Settings.Default.DownloadImages.ToString()));
                if (a == 0)
                {
                    Properties.Settings.Default.DownloadImages = false;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (a == 1)
                {
                    Properties.Settings.Default.DownloadImages = true;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void useVisualBarInTheEditorBooleanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Synced. Use visual bar in the editor. Valid values are 1 to enable this policy (true), and 0 to disable this policy (false). Type 1 to apply default setting. Default value is 1 (True). Current value is " + Properties.Settings.Default.UseBar.ToString()));
                if (a == 0)
                {
                    Properties.Settings.Default.UseBar = false;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (a == 1)
                {
                    Properties.Settings.Default.UseBar = true;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void displayTheNowDeprecatedVBScriptLanguageInTheLanguageChooserInsteadOfLogicalSchemeBooleanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Local. * Requires restart. Display the now deprecated VBScript language in the language chooser instead of Logical scheme. Valid values are 1 to enable this policy (true), and 0 to disable this policy (false). Type 0 to apply default setting. Default value is 0 (False). Current value is " + Properties.Settings.Default.ShowOldVBScript.ToString()));
                if (a == 0)
                {
                    Properties.Settings.Default.ShowOldVBScript = false;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (a == 1)
                {
                    Properties.Settings.Default.ShowOldVBScript = true;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void richTextBox1_MouseEnter(object sender, EventArgs e)
        {
            panel1.Focus();
        }

        private void showWindowsLiveMailAndMicrosoftPowerPointViewerWarningIfNecessaryInWindowsXPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int a = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Local. Show Windows Live Mail and Microsoft PowerPoint Viewer warning if necessary in Windows XP. Valid values are 1 to enable this policy (true), and 0 to disable this policy (false). Type 0 to apply default setting. Default value is 0 (False). Current value is " + Properties.Settings.Default.WasWLMPPVShown.ToString()));
                if (a == 0)
                {
                    Properties.Settings.Default.WasWLMPPVShown = false;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (a == 1)
                {
                    Properties.Settings.Default.WasWLMPPVShown  = true;
                    Properties.Settings.Default.Save();
                    CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    CasetaDeMesaj(this, "Value is not valid.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                CasetaDeMesaj(this, "Unable to save setting.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Config_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;

        }
        Thread newThread = null;

        private void crashApplicationonNonUIThreadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ThreadStart newThreadStart = new ThreadStart(newThread_Execute);
            newThread = new Thread(newThreadStart);
            newThread.Start();
        }

        // The thread we start up to demonstrate non-UI exception handling.  
        void newThread_Execute()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private void minGWPathStringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string a = Microsoft.VisualBasic.Interaction.InputBox("Local. MinGW Path. Any valid string is allowed. Default value is (blank). Leave blank to apply default setting. Current value is " + Properties.Settings.Default.MinGWPath.ToString());
            if (a == "") Properties.Settings.Default.MinGWPath = Application.StartupPath + "\\MinGW";
            else Properties.Settings.Default.MinGWPath = a;
            Properties.Settings.Default.Save();
            CasetaDeMesaj(this, "Setting saved successfully.", "none", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.UITheme = 1;
            Properties.Settings.Default.Save();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.UITheme = 0;
            Properties.Settings.Default.Save();
        }

        private void automaticallyReportErrorsToValiNetIfApplicationCrashesBooleanToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

    }
}
