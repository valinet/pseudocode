﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class ZoomWB : Form
    {
        MainForm mf;
        public ZoomWB(MainForm m)
        {
            InitializeComponent();
            mf = m;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (Convert.ToInt32(numericUpDown1.Value))
            {
                case 50: mf.zoomLevel = 1; break;
                case 75: mf.zoomLevel = 2; break;
                case 100: mf.zoomLevel = 3; break;
                case 125: mf.zoomLevel = 4; break;
                case 150: mf.zoomLevel = 5; break;
                case 175: mf.zoomLevel = 6; break;
                case 200: mf.zoomLevel = 7; break;
                case 250: mf.zoomLevel = 8; break;
                case 300: mf.zoomLevel = 9; break;
                case 400: mf.zoomLevel = 10; break;
                default: mf.zoomLevel = 0 - Convert.ToInt32(numericUpDown1.Value); break;
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
