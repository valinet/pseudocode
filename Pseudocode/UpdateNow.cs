﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class UpdateNow : Form
    {
        MainForm frm;
        public UpdateNow(MainForm mf, string new_ver, string more, string desc, string downloadPath, bool needsRestart)
        {
            InitializeComponent();
            one.Text = new_ver;
            textBox1.Text = desc.Replace("\n", "\r\n");
            button1.Tag = downloadPath;
            button2.Tag = needsRestart.ToString();
            linkLabel1.Tag = more;
            frm = mf;
        }

        private void UpdateNow_Load(object sender, EventArgs e)
        {
            this.ActiveControl = button1;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start(linkLabel1.Tag.ToString());
            }
            catch
            {
                if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(frm, "There is no Internet browser installed on this computer.\n\nIn order to use this feature, a browser has to be installed on your machine. Visit www.browserchoice.eu to download a web browser for your computer if you are based in the European Union or, if located in any other country, visit www.whatbrowser.org.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (Properties.Settings.Default.UILang == 1) CasetaDeMesaj(frm, "Niciun browser Web nu este instalat pe acest computer.\n\nPentru a utiliza această caracteristică, un browser trebuie să fie instalat pe computer. Pentru a descărca un browser, vizitați www.browserchoice.eu dacă sunteți în Uniunea Europeană sau www.whatbrowser.org dacă sunteți din oricare altă țară.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode")) System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode");
            /*            if (dr == System.Windows.Forms.DialogResult.OK)
            {*/
            bool isAdmin = false;
          //  if (Application.StartupPath.Contains(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86))) isAdmin = true;
          //  else isAdmin = false;
            try
            {
                Directory.CreateDirectory(Application.StartupPath + "\\test");
                Directory.Delete(Application.StartupPath + "\\test");
                isAdmin = false;
            }
            catch
            {
                isAdmin = true;
            }
            try
            {
                if (isAdmin == true)
                {
                    System.IO.File.Copy(Application.StartupPath + "\\updater.exe", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\updaterO.exe", true);
                }
                else
                {
                    System.IO.File.Copy(Application.StartupPath + "\\updaterLIMITED.exe", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\updaterO.exe", true);
                }
                // System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\updaterO.exe", button1.Tag.ToString() + " " + Properties.Settings.Default.UILang.ToString());
                //  Process.GetCurrentProcess().Kill();
            }
            catch
            {
            }
            try
            {
              //  if (isAdmin == true)
             //   {
                if (Convert.ToBoolean(button2.Tag) == true)
                {
                    if (Properties.Settings.Default.UILang == 0)
                    {
                        DialogResult dr = CasetaDeMesaj(frm, "In order to install this update, the application has to restart. Do you want to proceed?\n\nSave your work before continuing.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == System.Windows.Forms.DialogResult.No) return;
                    }
                    if (Properties.Settings.Default.UILang == 1)
                    {
                        DialogResult dr = CasetaDeMesaj(frm, "Pentru a instala această actualizare, aplicația trebuie închisă. Continuați?\n\nSalvați-vă lucrul înainte de a continua.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == System.Windows.Forms.DialogResult.No) return;
                    }
                }
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\download.txt")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\download.txt");
                System.Net.WebClient wc = new System.Net.WebClient();
                wc.DownloadFile(button1.Tag + ".txt", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\download.txt");
                StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\download.txt");
                button1.Tag = sr.ReadToEnd();
                sr.Close();
                if (Properties.Settings.Default.UILang == 0)
                {
                    string text = "updaterO";
                    if (Environment.OSVersion.Version.Major >= 6)
                    {
                        text = "In order to update the application, administrative privileges are required. If you click No, the application will not be updated";
                        if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\" + text + ".exe")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\" + text + ".exe");
                        File.Move(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\updaterO.exe", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\" + text + ".exe");
                    }
                    Process p = System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\" + text + ".exe", button1.Tag + " " + Properties.Settings.Default.UILang.ToString() + " " + "\"" + Application.StartupPath + "\"");
                    if (Convert.ToBoolean(button2.Tag) == true) Process.GetCurrentProcess().Kill();
                    p.WaitForExit();
                    this.Close();
                }
                if (Properties.Settings.Default.UILang == 1)
                {
                    string text = "updaterO";
                    if (Environment.OSVersion.Version.Major >= 6)
                    {
                        text = "Pentru a actualiza produsul Pseudocode, sunt necesare privilegii administrative. Dacă faceți click pe Nu, aplicația nu va fi actualizată";
                        if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\" + text + ".exe")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\" + text + ".exe");
                        File.Move(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\updaterO.exe", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\" + text + ".exe");
                    }
                    Process p = System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\" + text + ".exe", button1.Tag + " " + Properties.Settings.Default.UILang.ToString() + " " + "\"" + Application.StartupPath + "\"");
                    if (Convert.ToBoolean(button2.Tag) == true) Process.GetCurrentProcess().Kill();
                    p.WaitForExit();
                    this.Close();
                }

                /*    }
                    else
                    {
                        Process p = System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\updaterO.exe", button1.Tag + "setupPSClimited.exe" + " " + Properties.Settings.Default.UILang.ToString());
                        if (Convert.ToBoolean(button2.Tag) == true) Process.GetCurrentProcess().Kill();
                        p.WaitForExit();
                        this.Close();
                    }*/
            }
            catch
            {
                if (Properties.Settings.Default.UILang == 0)
                {
                    CasetaDeMesaj(frm, "The application couldn't connect to the updates server.\n\nTry again in a little while, or contact ValiNet if the issue persists.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                if (Properties.Settings.Default.UILang == 0)
                {
                    CasetaDeMesaj(frm, "Aplicația nu s-a putut conecta la server.\n\nÎncercați din nou în scurt timp sau contactați ValiNet dacă problema persistă.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        [System.Runtime.InteropServices.DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }

    }
}
