﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using mshtml;
using System.IO;
namespace Pseudocode
{
    public partial class UploadToInfoarena : Form
    {
        string title;
        string inFileName;
        string outFileName;
        string source;
        MainForm mf;
        public UploadToInfoarena(MainForm main, string text)
        {
            mf = main;
            source = text;
            InitializeComponent();
            webBrowser1.DocumentTitleChanged += webBrowser1_DocumentTitleChanged;
        }

        void webBrowser1_DocumentTitleChanged(object sender, EventArgs e)
        {
           // try
           // {

            //}
          //  catch { }
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void wizardPage2_Commit(object sender, AeroWizard.WizardPageConfirmEventArgs e)
        {
            if (!textBox1.Text.Contains(".infoarena.ro"))
            {
                MessageBox.Show("Aceasta nu este o adresă Infoarena validă.\n\nIntorduceți o adresă Infoarena validă și încercați din nou.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
        }

        private void wizardPage3_VisibleChanged(object sender, EventArgs e)
        {
            if (wizardPage3.Visible)
            {
                webBrowser1.Navigate(textBox1.Text);
            }
        }

        private void webBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.ToString() == textBox1.Text)
            {
                title = webBrowser1.DocumentTitle;
                inFileName = webBrowser1.Document.GetElementsByTagName("th")[0].InnerText;
                outFileName = webBrowser1.Document.GetElementsByTagName("th")[1].InnerText;
                source = source.Replace("ifstream f(\"TEXT_TO_BE_REPLACED\");", "ifstream f(\"" + inFileName + "\");");
                source = source.Replace("ofstream g(\"TEXT_TO_BE_REPLACED\");", "ofstream g(\"" + outFileName + "\");");
                //MessageBox.Show(source);
                stepWizardControl1.NextPage();
            }
        }

        private void wizardPage4_VisibleChanged(object sender, EventArgs e)
        {
            if (wizardPage4.Visible)
            {
                webBrowser2.Navigate("http://www.infoarena.ro/account");
            }
        }

        private void webBrowser2_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.ToString() == "https://infoarena.ro/login")
            {
                panel1.BringToFront();
            }
            if (e.Url.ToString() == "http://www.infoarena.ro/account")
            {
                stepWizardControl1.NextPage();
            }
        }

        private void wizardPage5_VisibleChanged(object sender, EventArgs e)
        {
            if (wizardPage5.Visible)
            {
                webBrowser3.Navigate(textBox1.Text);
            }
        }
        public static Task Delay(double milliseconds)
        {
            var tcs = new TaskCompletionSource<bool>();
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Elapsed += (obj, args) =>
            {
                tcs.TrySetResult(true);
            };
            timer.Interval = milliseconds;
            timer.AutoReset = false;
            timer.Start();
            return tcs.Task;
        }
        async Task PopulateInputFile(HtmlElement file)
        {
            file.Focus();

            // delay the execution of SendKey to let the Choose File dialog show up
            var sendKeyTask = Delay(500).ContinueWith((_) =>
            {
                // this gets executed when the dialog is visible
                SendKeys.Send(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\infoarena.cpp" + "{ENTER}");
            }, TaskScheduler.FromCurrentSynchronizationContext());

            file.InvokeMember("Click"); // this shows up the dialog

            await sendKeyTask;

            // delay continuation to let the Choose File dialog hide
            await Delay(500);
        }

        async Task Populate()
        {
            var elements = webBrowser3.Document.GetElementsByTagName("input");
            foreach (HtmlElement file in elements)
            {
                if (file.GetAttribute("name") == "solution")
                {
                    file.Focus();
                    await PopulateInputFile(file);
                }
            }
        }
        private void webBrowser3_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.ToString() == textBox1.Text)
            {
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\infoarena.cpp")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\infoarena.cpp");
                StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\infoarena.cpp");
                sw.WriteLine(source);
                sw.Close();
                this.webBrowser3.DocumentCompleted -= webBrowser3_DocumentCompleted;
                Populate().ContinueWith((_) =>
                {
                    webBrowser3.Document.GetElementById("form_round").SetAttribute("value", "arhiva");
                    webBrowser3.Document.GetElementById("form_compiler").SetAttribute("value", "cpp");
                    this.webBrowser3.DocumentCompleted += webBrowser3_DocumentCompleted1;
                    webBrowser3.Document.GetElementById("form_submit").InvokeMember("click");
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        private void webBrowser3_DocumentCompleted1(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.ToString() == textBox1.Text)
            {
                HtmlElementCollection hc = webBrowser3.Document.GetElementsByTagName("a");
                foreach (HtmlElement ht in hc)
                {
                    if (ht.InnerText != null)
                    {
                        if (ht.InnerText.Contains("onitorul de evaluare"))
                        {
                            dynamic element = ht.DomElement;
                            button1.Tag = element.attributes("href").value.ToString();
                        }
                    }
                }
                stepWizardControl1.NextPage();
                this.webBrowser3.DocumentCompleted -= webBrowser3_DocumentCompleted1;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }

        private void wizardPage2_VisibleChanged(object sender, EventArgs e)
        {
            if (wizardPage2.Visible)
            {
                textBox1.Select();
                if (Clipboard.GetText().StartsWith("http://") || Clipboard.GetText().StartsWith("www.")) textBox1.Text = Clipboard.GetText();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(button1.Tag.ToString());
                this.Close();
            }
            catch
            {
                MessageBox.Show("Nu este instalată nicio aplicație pe sistem capabilă să finalizez această acțiune.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            if (keyData == (Keys.F1))
            {
                if (Directory.Exists(Application.StartupPath + "\\com.valinet.pseudocode.help"))
                {
                    if (mf.hp == null) mf.hp = new Help();
                    mf.hp.Show();
                    if (mf.WindowState != FormWindowState.Maximized)
                    {
                        mf.hp.Location = mf.Location;
                        mf.hp.Size = mf.Size;
                    }
                    mf.hp.WindowState = mf.WindowState;
                }
                else
                {
                    HelpOverview ho = new HelpOverview();
                    ho.ShowDialog();
                }
                return true;
            }
            return false;
        }

        private void label2_MouseEnter(object sender, EventArgs e)
        {
            label2.Font = new Font(label2.Font, FontStyle.Underline);
            label2.Cursor = Cursors.Hand;
        }

        private void label2_MouseLeave(object sender, EventArgs e)
        {
            label2.Font = new Font(label2.Font, FontStyle.Regular);
            label2.Cursor = Cursors.Default;

        }

        private void label2_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.infoarena.ro/despre-infoarena");
                this.Close();
            }
            catch
            {
                MessageBox.Show("Nu este instalată nicio aplicație pe sistem capabilă să finalizez această acțiune.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
