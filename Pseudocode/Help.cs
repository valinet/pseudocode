﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class Help : Form
    {
        public Help()
        {
            InitializeComponent();
        }

        private void bACKToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void fORWARDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.ShowPrintDialog();
        }

        private void printPreviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.ShowPrintPreviewDialog();
        }

        private void pageSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.ShowPageSetupDialog();
        }

        private void smallestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.Document.ExecCommand("FontSize", false, "1");
        }

        private void smallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.Document.ExecCommand("FontSize", false, "2");

        }

        private void mediumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.Document.ExecCommand("FontSize", false, "3");

        }

        private void biggerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.Document.ExecCommand("FontSize", false, "4");

        }

        private void biggestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.Document.ExecCommand("FontSize", false, "5");

        }

        private void findonThisPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.Select(); 
            SendKeys.Send("^f");
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void Help_Load(object sender, EventArgs e)
        {
            webBrowser1.DocumentTitleChanged += new EventHandler(webBrowser1_DocumentTitleChanged);
            string lang = "ro";
            if (Properties.Settings.Default.UILang == 0) lang = "en";
            if (Properties.Settings.Default.UILang == 1) lang = "ro";
            webBrowser1.Navigate(Application.StartupPath + "\\com.valinet.pseudocode.help\\" + lang + "\\Default\\webframe.html");
        }

        void webBrowser1_DocumentTitleChanged(object sender, EventArgs e)
        {
            this.Text = webBrowser1.Document.Title;
        }

        private void vizualizareSursăToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String source = (Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\source.txt");
            System.IO.StreamWriter writer = System.IO.File.CreateText(source);
            writer.Write(webBrowser1.DocumentText);
            writer.Close();
            System.Diagnostics.Process.Start("notepad.exe", source);
        }

        private void Help_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void knowledgeBaseHomeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate(Application.StartupPath + "\\com.valinet.pseudocode.help\\ro\\Default\\webframe.html");

        }

        private void webBrowser1_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (e.Url.OriginalString.StartsWith("http") || e.Url.OriginalString.StartsWith("https"))
            {
                try
                {
                    System.Diagnostics.Process.Start(e.Url.OriginalString.ToString());
                    e.Cancel = true;
                }
                catch
                {
                    if (Properties.Settings.Default.UILang == 0) MessageBox.Show("There is no application installed on this system which can handle a link of this type.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (Properties.Settings.Default.UILang == 1) MessageBox.Show("Nu este instalată nicio aplicație pe sistem care să poate urmări o legătură de acest tip.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
