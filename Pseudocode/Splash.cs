﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Pseudocode
{
    public partial class Splash : Form
    {
        string argum = "";
        public Splash()
        {
            InitializeComponent();
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        //Codeboard™
        private void Splash_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UILang == 0)
            {
                label1.Text = "Copyright (C) 2006-2016 Democratic Instruments Ltd. All rights reserved. The Pseudocode application, its concept, source code, ideas, the ^_^ icon, and any other aditional materials provided with this product are the exclusive property of Democratic Instruments or their respective owners and are used under a license. Check Help - About for more details.";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                label1.Text = "Drepturi de autor (C) 2006-2016 Democratic Instruments. Toate drepturile rezervate. Aplicația Pseudocode, conceptul, codul sursă, ideile, iconița ^_^ și orice alte materiale adiționale furnizate împreună cu acest produs sunt proprietatea Democratic Instruments sau a respectivilor lor autori și sunt folosite sub licență. Consultați Asistență - Despre pentru alte detalii.";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocodurile mele\\startup.psc"))
            {
                this.Close();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            
        }
    }
}
