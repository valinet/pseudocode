﻿namespace Pseudocode
{
    partial class Open
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Open));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.webBrowser2 = new System.Windows.Forms.WebBrowser();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.OKNew = new System.Windows.Forms.Button();
            this.CancelNew = new System.Windows.Forms.Button();
            this.TreePanel = new System.Windows.Forms.Panel();
            this.lvFiles = new System.Windows.Forms.ListView();
            this.labelNoFile = new System.Windows.Forms.Label();
            this.CodeRepPanel = new System.Windows.Forms.Panel();
            this.labelErase = new System.Windows.Forms.Label();
            this.labelInfo = new System.Windows.Forms.Label();
            this.RecentPanel = new System.Windows.Forms.Panel();
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.DefaultPanel = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.treeView1 = new ExtendedTreeView();
            this.imageListBoxItem2 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem3 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem1 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem20 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem4 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem5 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem6 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem7 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem8 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem9 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem10 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem11 = new Controls.Development.ImageListBoxItem();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.TreePanel.SuspendLayout();
            this.CodeRepPanel.SuspendLayout();
            this.RecentPanel.SuspendLayout();
            this.DefaultPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.button5);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Name = "panel1";
            this.toolTip1.SetToolTip(this.panel1, resources.GetString("panel1.ToolTip"));
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Name = "label2";
            this.toolTip1.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            // 
            // button5
            // 
            resources.ApplyResources(this.button5, "button5");
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Name = "button5";
            this.toolTip1.SetToolTip(this.button5, resources.GetString("button5.ToolTip"));
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            resources.ApplyResources(this.button6, "button6");
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.Purple;
            this.button6.FlatAppearance.BorderSize = 2;
            this.button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Purple;
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Violet;
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.Name = "button6";
            this.toolTip1.SetToolTip(this.button6, resources.GetString("button6.ToolTip"));
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.linkLabel1);
            this.panel2.Controls.Add(this.button2);
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Name = "panel2";
            this.toolTip1.SetToolTip(this.panel2, resources.GetString("panel2.ToolTip"));
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            this.panel2.MouseEnter += new System.EventHandler(this.panel2_MouseEnter);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Name = "label1";
            this.toolTip1.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
            // 
            // linkLabel1
            // 
            resources.ApplyResources(this.linkLabel1, "linkLabel1");
            this.linkLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel1.ForeColor = System.Drawing.Color.Black;
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.TabStop = true;
            this.toolTip1.SetToolTip(this.linkLabel1, resources.GetString("linkLabel1.ToolTip"));
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.BackColor = System.Drawing.Color.Silver;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Name = "button2";
            this.toolTip1.SetToolTip(this.button2, resources.GetString("button2.ToolTip"));
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Name = "label6";
            this.toolTip1.SetToolTip(this.label6, resources.GetString("label6.ToolTip"));
            // 
            // openFileDialog1
            // 
            resources.ApplyResources(this.openFileDialog1, "openFileDialog1");
            // 
            // panel3
            // 
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel3.Controls.Add(this.button4);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.button22);
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Name = "panel3";
            this.toolTip1.SetToolTip(this.panel3, resources.GetString("panel3.ToolTip"));
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.BackColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Name = "button4";
            this.toolTip1.SetToolTip(this.button4, resources.GetString("button4.ToolTip"));
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Name = "button3";
            this.toolTip1.SetToolTip(this.button3, resources.GetString("button3.ToolTip"));
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Name = "button1";
            this.toolTip1.SetToolTip(this.button1, resources.GetString("button1.ToolTip"));
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button22
            // 
            resources.ApplyResources(this.button22, "button22");
            this.button22.BackColor = System.Drawing.Color.White;
            this.button22.FlatAppearance.BorderSize = 0;
            this.button22.ForeColor = System.Drawing.Color.Black;
            this.button22.Name = "button22";
            this.toolTip1.SetToolTip(this.button22, resources.GetString("button22.ToolTip"));
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // panel4
            // 
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel4.Controls.Add(this.button6);
            this.panel4.ForeColor = System.Drawing.Color.Black;
            this.panel4.Name = "panel4";
            this.toolTip1.SetToolTip(this.panel4, resources.GetString("panel4.ToolTip"));
            // 
            // saveFileDialog1
            // 
            resources.ApplyResources(this.saveFileDialog1, "saveFileDialog1");
            // 
            // panel5
            // 
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel5.Controls.Add(this.panel6);
            this.panel5.ForeColor = System.Drawing.Color.Black;
            this.panel5.Name = "panel5";
            this.toolTip1.SetToolTip(this.panel5, resources.GetString("panel5.ToolTip"));
            // 
            // panel6
            // 
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Name = "panel6";
            this.toolTip1.SetToolTip(this.panel6, resources.GetString("panel6.ToolTip"));
            // 
            // webBrowser2
            // 
            resources.ApplyResources(this.webBrowser2, "webBrowser2");
            this.webBrowser2.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser2.Name = "webBrowser2";
            this.webBrowser2.ScriptErrorsSuppressed = true;
            this.toolTip1.SetToolTip(this.webBrowser2, resources.GetString("webBrowser2.ToolTip"));
            this.webBrowser2.Url = new System.Uri("", System.UriKind.Relative);
            this.webBrowser2.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.webBrowser2_Navigating);
            // 
            // webBrowser1
            // 
            resources.ApplyResources(this.webBrowser1, "webBrowser1");
            this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.toolTip1.SetToolTip(this.webBrowser1, resources.GetString("webBrowser1.ToolTip"));
            this.webBrowser1.Url = new System.Uri("", System.UriKind.Relative);
            this.webBrowser1.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.webBrowser1_Navigated);
            this.webBrowser1.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.webBrowser1_Navigating);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "clock.png");
            this.imageList1.Images.SetKeyName(1, "folder_open-internet.png");
            this.imageList1.Images.SetKeyName(2, "folder_open-open.png");
            this.imageList1.Images.SetKeyName(3, "Apps-Dropbox-Metro-icon.png");
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // OKNew
            // 
            resources.ApplyResources(this.OKNew, "OKNew");
            this.OKNew.BackColor = System.Drawing.SystemColors.Control;
            this.OKNew.Name = "OKNew";
            this.toolTip1.SetToolTip(this.OKNew, resources.GetString("OKNew.ToolTip"));
            this.OKNew.UseVisualStyleBackColor = false;
            this.OKNew.Click += new System.EventHandler(this.OKNew_Click);
            // 
            // CancelNew
            // 
            resources.ApplyResources(this.CancelNew, "CancelNew");
            this.CancelNew.BackColor = System.Drawing.SystemColors.Control;
            this.CancelNew.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelNew.Name = "CancelNew";
            this.toolTip1.SetToolTip(this.CancelNew, resources.GetString("CancelNew.ToolTip"));
            this.CancelNew.UseVisualStyleBackColor = false;
            // 
            // TreePanel
            // 
            resources.ApplyResources(this.TreePanel, "TreePanel");
            this.TreePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TreePanel.Controls.Add(this.lvFiles);
            this.TreePanel.Controls.Add(this.labelNoFile);
            this.TreePanel.Name = "TreePanel";
            this.toolTip1.SetToolTip(this.TreePanel, resources.GetString("TreePanel.ToolTip"));
            // 
            // lvFiles
            // 
            resources.ApplyResources(this.lvFiles, "lvFiles");
            this.lvFiles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lvFiles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvFiles.ForeColor = System.Drawing.Color.White;
            this.lvFiles.FullRowSelect = true;
            this.lvFiles.Name = "lvFiles";
            this.toolTip1.SetToolTip(this.lvFiles, resources.GetString("lvFiles.ToolTip"));
            this.lvFiles.UseCompatibleStateImageBehavior = false;
            this.lvFiles.View = System.Windows.Forms.View.Details;
            // 
            // labelNoFile
            // 
            resources.ApplyResources(this.labelNoFile, "labelNoFile");
            this.labelNoFile.BackColor = System.Drawing.Color.Transparent;
            this.labelNoFile.ForeColor = System.Drawing.Color.White;
            this.labelNoFile.Name = "labelNoFile";
            this.toolTip1.SetToolTip(this.labelNoFile, resources.GetString("labelNoFile.ToolTip"));
            // 
            // CodeRepPanel
            // 
            resources.ApplyResources(this.CodeRepPanel, "CodeRepPanel");
            this.CodeRepPanel.Controls.Add(this.webBrowser2);
            this.CodeRepPanel.Controls.Add(this.webBrowser1);
            this.CodeRepPanel.Name = "CodeRepPanel";
            this.toolTip1.SetToolTip(this.CodeRepPanel, resources.GetString("CodeRepPanel.ToolTip"));
            // 
            // labelErase
            // 
            resources.ApplyResources(this.labelErase, "labelErase");
            this.labelErase.BackColor = System.Drawing.Color.Transparent;
            this.labelErase.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelErase.ForeColor = System.Drawing.Color.White;
            this.labelErase.Name = "labelErase";
            this.toolTip1.SetToolTip(this.labelErase, resources.GetString("labelErase.ToolTip"));
            this.labelErase.Click += new System.EventHandler(this.labelErase_Click);
            // 
            // labelInfo
            // 
            resources.ApplyResources(this.labelInfo, "labelInfo");
            this.labelInfo.BackColor = System.Drawing.Color.Transparent;
            this.labelInfo.ForeColor = System.Drawing.Color.White;
            this.labelInfo.Name = "labelInfo";
            this.toolTip1.SetToolTip(this.labelInfo, resources.GetString("labelInfo.ToolTip"));
            // 
            // RecentPanel
            // 
            resources.ApplyResources(this.RecentPanel, "RecentPanel");
            this.RecentPanel.Controls.Add(this.panel2);
            this.RecentPanel.Name = "RecentPanel";
            this.toolTip1.SetToolTip(this.RecentPanel, resources.GetString("RecentPanel.ToolTip"));
            // 
            // SearchBox
            // 
            resources.ApplyResources(this.SearchBox, "SearchBox");
            this.SearchBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.SearchBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SearchBox.ForeColor = System.Drawing.Color.White;
            this.SearchBox.Name = "SearchBox";
            this.toolTip1.SetToolTip(this.SearchBox, resources.GetString("SearchBox.ToolTip"));
            this.SearchBox.TextChanged += new System.EventHandler(this.SearchBox_TextChanged);
            this.SearchBox.Enter += new System.EventHandler(this.SearchBox_Enter);
            this.SearchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SearchBox_KeyDown);
            this.SearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SearchBox_KeyUp);
            this.SearchBox.Leave += new System.EventHandler(this.SearchBox_Leave);
            // 
            // DefaultPanel
            // 
            resources.ApplyResources(this.DefaultPanel, "DefaultPanel");
            this.DefaultPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.DefaultPanel.Controls.Add(this.label4);
            this.DefaultPanel.Controls.Add(this.label3);
            this.DefaultPanel.Name = "DefaultPanel";
            this.toolTip1.SetToolTip(this.DefaultPanel, resources.GetString("DefaultPanel.ToolTip"));
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Name = "label4";
            this.toolTip1.SetToolTip(this.label4, resources.GetString("label4.ToolTip"));
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Name = "label3";
            this.toolTip1.SetToolTip(this.label3, resources.GetString("label3.ToolTip"));
            // 
            // treeView1
            // 
            resources.ApplyResources(this.treeView1, "treeView1");
            this.treeView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView1.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText;
            this.treeView1.ForeColor = System.Drawing.Color.White;
            this.treeView1.FullRowSelect = true;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Name = "treeView1";
            this.treeView1.ShowLines = false;
            this.toolTip1.SetToolTip(this.treeView1, resources.GetString("treeView1.ToolTip"));
            this.treeView1.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView1_BeforeExpand);
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            this.treeView1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseDoubleClick);
            // 
            // imageListBoxItem2
            // 
            resources.ApplyResources(this.imageListBoxItem2, "imageListBoxItem2");
            // 
            // imageListBoxItem3
            // 
            resources.ApplyResources(this.imageListBoxItem3, "imageListBoxItem3");
            // 
            // imageListBoxItem1
            // 
            resources.ApplyResources(this.imageListBoxItem1, "imageListBoxItem1");
            // 
            // imageListBoxItem20
            // 
            resources.ApplyResources(this.imageListBoxItem20, "imageListBoxItem20");
            // 
            // imageListBoxItem4
            // 
            resources.ApplyResources(this.imageListBoxItem4, "imageListBoxItem4");
            // 
            // imageListBoxItem5
            // 
            resources.ApplyResources(this.imageListBoxItem5, "imageListBoxItem5");
            // 
            // imageListBoxItem6
            // 
            resources.ApplyResources(this.imageListBoxItem6, "imageListBoxItem6");
            // 
            // imageListBoxItem7
            // 
            resources.ApplyResources(this.imageListBoxItem7, "imageListBoxItem7");
            // 
            // imageListBoxItem8
            // 
            resources.ApplyResources(this.imageListBoxItem8, "imageListBoxItem8");
            // 
            // imageListBoxItem9
            // 
            resources.ApplyResources(this.imageListBoxItem9, "imageListBoxItem9");
            // 
            // imageListBoxItem10
            // 
            resources.ApplyResources(this.imageListBoxItem10, "imageListBoxItem10");
            // 
            // imageListBoxItem11
            // 
            resources.ApplyResources(this.imageListBoxItem11, "imageListBoxItem11");
            // 
            // Open
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.CancelButton = this.button6;
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.labelErase);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.OKNew);
            this.Controls.Add(this.CancelNew);
            this.Controls.Add(this.RecentPanel);
            this.Controls.Add(this.CodeRepPanel);
            this.Controls.Add(this.DefaultPanel);
            this.Controls.Add(this.TreePanel);
            this.MinimizeBox = false;
            this.Name = "Open";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Open_FormClosing);
            this.Load += new System.EventHandler(this.Open_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.TreePanel.ResumeLayout(false);
            this.CodeRepPanel.ResumeLayout(false);
            this.RecentPanel.ResumeLayout(false);
            this.DefaultPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.WebBrowser webBrowser2;
        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ImageList imageList1;
        private Controls.Development.ImageListBoxItem imageListBoxItem2;
        private Controls.Development.ImageListBoxItem imageListBoxItem3;
        private Controls.Development.ImageListBoxItem imageListBoxItem1;
        private Controls.Development.ImageListBoxItem imageListBoxItem20;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button OKNew;
        private System.Windows.Forms.Button CancelNew;
        private Controls.Development.ImageListBoxItem imageListBoxItem4;
        private Controls.Development.ImageListBoxItem imageListBoxItem5;
        private Controls.Development.ImageListBoxItem imageListBoxItem6;
        private Controls.Development.ImageListBoxItem imageListBoxItem7;
        private System.Windows.Forms.Label labelErase;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Panel RecentPanel;
        private System.Windows.Forms.TextBox SearchBox;
        private System.Windows.Forms.Panel TreePanel;
        private System.Windows.Forms.ListView lvFiles;
        private System.Windows.Forms.Label labelNoFile;
        private System.Windows.Forms.Panel CodeRepPanel;
        private Controls.Development.ImageListBoxItem imageListBoxItem8;
        private Controls.Development.ImageListBoxItem imageListBoxItem9;
        private Controls.Development.ImageListBoxItem imageListBoxItem10;
        private Controls.Development.ImageListBoxItem imageListBoxItem11;
        private System.Windows.Forms.Panel DefaultPanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public ExtendedTreeView treeView1;
    }
}