﻿namespace Pseudocode
{
    partial class CreateEXEWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateEXEWizard));
            this.Step1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Step4 = new System.Windows.Forms.SplitContainer();
            this.label15 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Step2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.EXEVersion = new System.Windows.Forms.TextBox();
            this.EXECopyright = new System.Windows.Forms.TextBox();
            this.EXEInternalName = new System.Windows.Forms.TextBox();
            this.EXEProductName = new System.Windows.Forms.TextBox();
            this.EXECompanyName = new System.Windows.Forms.TextBox();
            this.EXEName = new System.Windows.Forms.TextBox();
            this.button40 = new System.Windows.Forms.Button();
            this.EXELocation = new System.Windows.Forms.TextBox();
            this.button39 = new System.Windows.Forms.Button();
            this.EXEIcon = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Step3 = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.Step1)).BeginInit();
            this.Step1.Panel1.SuspendLayout();
            this.Step1.Panel2.SuspendLayout();
            this.Step1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Step4)).BeginInit();
            this.Step4.Panel1.SuspendLayout();
            this.Step4.Panel2.SuspendLayout();
            this.Step4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.Step2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Step3.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // Step1
            // 
            resources.ApplyResources(this.Step1, "Step1");
            this.Step1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.Step1.Name = "Step1";
            // 
            // Step1.Panel1
            // 
            resources.ApplyResources(this.Step1.Panel1, "Step1.Panel1");
            this.Step1.Panel1.Controls.Add(this.label1);
            // 
            // Step1.Panel2
            // 
            this.Step1.Panel2.BackColor = System.Drawing.Color.White;
            this.Step1.Panel2.Controls.Add(this.label3);
            this.Step1.Panel2.Controls.Add(this.label2);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // Step4
            // 
            resources.ApplyResources(this.Step4, "Step4");
            this.Step4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.Step4.Name = "Step4";
            // 
            // Step4.Panel1
            // 
            resources.ApplyResources(this.Step4.Panel1, "Step4.Panel1");
            this.Step4.Panel1.Controls.Add(this.label15);
            // 
            // Step4.Panel2
            // 
            this.Step4.Panel2.BackColor = System.Drawing.Color.White;
            this.Step4.Panel2.Controls.Add(this.button6);
            this.Step4.Panel2.Controls.Add(this.button5);
            this.Step4.Panel2.Controls.Add(this.label16);
            this.Step4.Panel2.Controls.Add(this.label17);
            this.Step4.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.Step4_SplitterMoved);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // button6
            // 
            resources.ApplyResources(this.button6, "button6");
            this.button6.Name = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            resources.ApplyResources(this.button5, "button5");
            this.button5.Name = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Step2
            // 
            this.Step2.Controls.Add(this.panel4);
            this.Step2.Controls.Add(this.checkBox1);
            this.Step2.Controls.Add(this.button4);
            this.Step2.Controls.Add(this.panel3);
            resources.ApplyResources(this.Step2, "Step2");
            this.Step2.Name = "Step2";
            // 
            // panel4
            // 
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.EXEVersion);
            this.panel4.Controls.Add(this.EXECopyright);
            this.panel4.Controls.Add(this.EXEInternalName);
            this.panel4.Controls.Add(this.EXEProductName);
            this.panel4.Controls.Add(this.EXECompanyName);
            this.panel4.Controls.Add(this.EXEName);
            this.panel4.Controls.Add(this.button40);
            this.panel4.Controls.Add(this.EXELocation);
            this.panel4.Controls.Add(this.button39);
            this.panel4.Controls.Add(this.EXEIcon);
            this.panel4.Name = "panel4";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // EXEVersion
            // 
            this.EXEVersion.BackColor = System.Drawing.Color.White;
            this.EXEVersion.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXEVersion, "EXEVersion");
            this.EXEVersion.Name = "EXEVersion";
            // 
            // EXECopyright
            // 
            this.EXECopyright.BackColor = System.Drawing.Color.White;
            this.EXECopyright.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXECopyright, "EXECopyright");
            this.EXECopyright.Name = "EXECopyright";
            // 
            // EXEInternalName
            // 
            this.EXEInternalName.BackColor = System.Drawing.Color.White;
            this.EXEInternalName.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXEInternalName, "EXEInternalName");
            this.EXEInternalName.Name = "EXEInternalName";
            // 
            // EXEProductName
            // 
            this.EXEProductName.BackColor = System.Drawing.Color.White;
            this.EXEProductName.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXEProductName, "EXEProductName");
            this.EXEProductName.Name = "EXEProductName";
            // 
            // EXECompanyName
            // 
            this.EXECompanyName.BackColor = System.Drawing.Color.White;
            this.EXECompanyName.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXECompanyName, "EXECompanyName");
            this.EXECompanyName.Name = "EXECompanyName";
            // 
            // EXEName
            // 
            this.EXEName.BackColor = System.Drawing.Color.White;
            this.EXEName.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXEName, "EXEName");
            this.EXEName.Name = "EXEName";
            // 
            // button40
            // 
            this.button40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button40.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.button40, "button40");
            this.button40.Name = "button40";
            this.button40.UseVisualStyleBackColor = false;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // EXELocation
            // 
            this.EXELocation.BackColor = System.Drawing.Color.White;
            this.EXELocation.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXELocation, "EXELocation");
            this.EXELocation.Name = "EXELocation";
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button39.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.button39, "button39");
            this.button39.Name = "button39";
            this.button39.UseVisualStyleBackColor = false;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // EXEIcon
            // 
            this.EXEIcon.BackColor = System.Drawing.Color.White;
            this.EXEIcon.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXEIcon, "EXEIcon");
            this.EXEIcon.Name = "EXEIcon";
            // 
            // checkBox1
            // 
            resources.ApplyResources(this.checkBox1, "checkBox1");
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.pictureBox1);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // Step3
            // 
            this.Step3.Controls.Add(this.progressBar1);
            this.Step3.Controls.Add(this.panel6);
            resources.ApplyResources(this.Step3, "Step3");
            this.Step3.Name = "Step3";
            // 
            // progressBar1
            // 
            resources.ApplyResources(this.progressBar1, "progressBar1");
            this.progressBar1.Name = "progressBar1";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.pictureBox2);
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Name = "panel6";
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog1";
            resources.ApplyResources(this.openFileDialog2, "openFileDialog2");
            // 
            // saveFileDialog2
            // 
            resources.ApplyResources(this.saveFileDialog2, "saveFileDialog2");
            // 
            // CreateEXEWizard
            // 
            this.AcceptButton = this.button2;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button1;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Step1);
            this.Controls.Add(this.Step4);
            this.Controls.Add(this.Step3);
            this.Controls.Add(this.Step2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateEXEWizard";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.CreateEXEWizard_Load);
            this.Step1.Panel1.ResumeLayout(false);
            this.Step1.Panel1.PerformLayout();
            this.Step1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Step1)).EndInit();
            this.Step1.ResumeLayout(false);
            this.Step4.Panel1.ResumeLayout(false);
            this.Step4.Panel1.PerformLayout();
            this.Step4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Step4)).EndInit();
            this.Step4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.Step2.ResumeLayout(false);
            this.Step2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Step3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer Step1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel Step2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox EXEVersion;
        private System.Windows.Forms.TextBox EXECopyright;
        private System.Windows.Forms.TextBox EXEInternalName;
        private System.Windows.Forms.TextBox EXEProductName;
        private System.Windows.Forms.TextBox EXECompanyName;
        private System.Windows.Forms.TextBox EXEName;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.TextBox EXELocation;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.TextBox EXEIcon;
        private System.Windows.Forms.Panel Step3;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.SplitContainer Step4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private System.Windows.Forms.Button button1;
    }
}