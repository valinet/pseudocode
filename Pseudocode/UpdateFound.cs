﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class UpdateFound : Form
    {
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        string AppName;
        string NoBrowserInstalled;
        MainForm mf;
        public UpdateFound(MainForm m, int nr_up)
        {
            InitializeComponent();
            mf = m;
            if (Properties.Settings.Default.UILang == 0)
            {
                if (nr_up == 1) label1.Text = "There is one software update available for your installed packages.";
                else label1.Text = "There are " + nr_up.ToString() + " software updates available for your installed packages.";
                NoBrowserInstalled = "There is no Internet browser installed on this computer.\n\nIn order to use this feature, a browser has to be installed on your machine.";
                if (Properties.Settings.Default.AppName != "") AppName = Properties.Settings.Default.AppName;
                else AppName = "Pseudocode";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                if (nr_up == 1) label1.Text = "Există o actualizare software disponibilă pentru unul din pachetele instalate în cadrul Pseudocode.";
                else label1.Text = "Există " + nr_up.ToString() + " actualizări software pentru pachetele instalate în cadrul Pseudocode.";
                NoBrowserInstalled = "Niciun browser Web nu este instalat pe acest computer.\n\nPentru a utiliza această caracteristică, un browser trebuie să fie instalat pe computer.";
                if (Properties.Settings.Default.AppName != "") AppName = Properties.Settings.Default.AppName;
                else AppName = "Pseudocode";

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process p = Process.Start("http://www.valinet.ro/why-update");
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                {
                    CasetaDeMesaj(mf, NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }
    }
}
