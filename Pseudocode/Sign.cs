﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using AppLimit.CloudComputing.SharpBox;
using AppLimit.CloudComputing.SharpBox.StorageProvider.DropBox;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Pseudocode
{
    public partial class Sign : Form
    {
        string NoBrowserInstalled;
        string AppName;
        string ErrorMessage;
        string FinalTouches;
        MainForm form1;
        string AccountInfo;
        bool isManag;
        string Connecting;
        string Syncing;
        string Welcome;
        string SignIn;
        string UnableToConnect;
        MainForm mf;
        public Sign(MainForm frm, bool IsManagement)
        {
            InitializeComponent();
            mf = frm;
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            form1 = frm;
            isManag = IsManagement;
            if (Properties.Settings.Default.UILang == 0)
            {
                NoBrowserInstalled = "There is no Internet browser installed on this computer. In order to use this feature, a browser has to be installed on your machine. Visit www.browserchoice.eu to download a web browser for your computer if you are based in the European Union or, if located in any other country, visit www.whatbrowser.org.";
                AppName = "Pseudocode";
                ErrorMessage = "In order to connect with your Dropbox account, you have to provide a picture and your name to be associated with your ID.\n\nIf you do not want to do so, you can cancel the sign up process any time.";
                FinalTouches = "Final touches";
                AccountInfo = "Account info";
                Connecting = "Connecting...";
                Syncing = "Syncing...";
                Welcome = "Welcome";
                SignIn = "Sign In";
                UnableToConnect = "Unable to connect to Dropbox.\n\nPlease check your Internet connection and try again.";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                NoBrowserInstalled = "Niciun browser Web nu este instalat pe acest computer.\n\nPentru a utiliza această caracteristică, un browser trebuie să fie instalat pe computer. Pentru a descărca un browser, vizitați www.browserchoice.eu dacă sunteți în Uniunea Europeană sau www.whatbrowser.org dacă sunteți din oricare altă țară.";
                AppName = "Pseudocod";
                ErrorMessage = "Pentru a face sign in cu Dropbox, trebuie să furnizați o imagine și un nume spre asociere.\n\nDacă nu doriți acest lucru, puteți anula oricând acest proces.";
                FinalTouches = "Atingeri finale";
                AccountInfo = "Informații cont";
                Connecting = "Conectare...";
                Syncing = "Sincronizare...";
                Welcome = "Bun venit";
                SignIn = "Sign In";
                UnableToConnect = "Nu s-a putut realiza conectarea la Dropbox.\n\nVerificați conexiunea la Internet și încercați din nou.";
            }
            if (Properties.Settings.Default.AppName != "")
            {
                AppName = Properties.Settings.Default.AppName;
            }
            string CloseText = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                CloseText = "Close (Alt+F4)";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                CloseText = "Închidere (Alt+F4)";
            }
            //toolTip1.SetToolTip(Close, CloseText);
        }

        private void button44_Click(object sender, EventArgs e)
        {
            panel2.Visible = true;
            backgroundWorker1.RunWorkerAsync();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            if (openFileDialog1.FileName != "")
            {
                using (FileStream stream = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read))
                {
                    pictureBox2.Image = Image.FromStream(stream);
                }
                //pictureBox2.Image = Image.FromFile(openFileDialog1.FileName);
                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            panel2.Visible = true;
            backgroundWorker2.RunWorkerAsync();
            label1.Text = Syncing;
        }
        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DropBoxConfiguration config = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox) as DropBoxConfiguration;
            // set your own callback which will be called from DropBox after successful
            // authorization
            if (Properties.Settings.Default.UILang == 0) config.AuthorizationCallBack = new Uri("http://valinet.ro/en-about-signin.htm");
            if (Properties.Settings.Default.UILang == 1) config.AuthorizationCallBack = new Uri("http://valinet.ro/ro-about-signin.htm");
            // create a request token
            DropBoxRequestToken requestToken = DropBoxStorageProviderTools.GetDropBoxRequestToken(config,
            "1d4mgqplmw9svwb",
            "ckijxw69ttb3vn5");
            String AuthorizationUrl = "";
            try
            {
                AuthorizationUrl = DropBoxStorageProviderTools.GetDropBoxAuthorizationUrl(config, requestToken);
            }
            catch
            {
                                                DoOnUIThread(delegate()
                    {
                MessageBox.Show(UnableToConnect, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                backgroundWorker1.CancelAsync();
                this.Close();
                return;

                    });
                                                if (backgroundWorker1.CancellationPending == true) return;
            }
            try
            {
                Process p = Process.Start(AuthorizationUrl);
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                {
                    DoOnUIThread(delegate()
                    {
                    MessageBox.Show(NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                     });
                    return;
                }
            }
            CloudStorage dropBoxStorage = new CloudStorage();
            AppLimit.CloudComputing.SharpBox.ICloudStorageAccessToken token;
        linie:
            try
            {
                token = DropBoxStorageProviderTools.ExchangeDropBoxRequestTokenIntoAccessToken(config, "1d4mgqplmw9svwb", "ckijxw69ttb3vn5", requestToken);
                var dropBoxConfig = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox);
                var storageToken = dropBoxStorage.Open(dropBoxConfig, token);
            }
            catch
            {
                Application.DoEvents();
                goto linie;
            }
                                DoOnUIThread(delegate()
                    {
                        this.Activate();
            this.TopMost = true;
            this.TopMost = false;
                    });
            Stream stream = dropBoxStorage.SerializeSecurityToken(token);
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode")) Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
            FileStream fileStream = File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat", (int)stream.Length);
            // Initialize the bytes array with the stream length and then fill it with data
            byte[] bytesInStream = new byte[stream.Length];
            stream.Read(bytesInStream, 0, bytesInStream.Length);
            // Use write method to write to the file specified above
            fileStream.Write(bytesInStream, 0, bytesInStream.Length);
            fileStream.Close();
            StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
            string t = sr.ReadToEnd();
            sr.Close();
           // MessageBox.Show(t);
            StreamWriter sww = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
            sww.Write(EncryptDecrypt.EncryptString(t));
            sww.Close();
            //MessageBox.Show(EncryptDecrypt.EncryptString(t));
            //MessageBox.Show(token.ToString());
                                DoOnUIThread(delegate()
                    {
            
            panel1.Visible = true;
                    });
            try
            {
                dropBoxStorage.DownloadFile("/user.pic", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
                DoOnUIThread(delegate()
                {
                    using (FileStream stream2 = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                    {
                        pictureBox2.Image = Image.FromStream(stream2);
                    }
                //pictureBox2.Image = Image.FromFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic");
                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                openFileDialog1.FileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic";
                });
            }
            catch { }
            try
            {
                dropBoxStorage.DownloadFile("/user.dat", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
                DoOnUIThread(delegate()
                {
                textBox1.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                });
            }
            catch { }
                            DoOnUIThread(delegate()
                {
                    label1.Text = FinalTouches;
                    panel2.Visible = false;
                });
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            if (openFileDialog1.FileName != "" & textBox1.Text != "")
            {

                var dropBoxStorage = new CloudStorage();
                var dropBoxConfig = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox);
                ICloudStorageAccessToken accessToken;
                StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                string t = sr.ReadToEnd();
               // MessageBox.Show(t);
                sr.Close();
                StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                sw.Write(EncryptDecrypt.DecryptString(t));
                sw.Close();
               // MessageBox.Show(EncryptDecrypt.DecryptString(t));
                using (var fs = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat", FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    accessToken = dropBoxStorage.DeserializeSecurityToken(fs);
                }
                StreamWriter sww = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                sww.Write(t);
                sww.Close();
                var storageToken = dropBoxStorage.Open(dropBoxConfig, accessToken);
                //pictureBox2.Image.Dispose();
                //File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic");
                if (openFileDialog1.FileName != Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic") File.Copy(openFileDialog1.FileName, Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", true);
                dropBoxStorage.UploadFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", "/");
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat", textBox1.Text);
                dropBoxStorage.UploadFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat", "/");
                if (isManag == false) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                if (isManag == false) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic");
                                                            DoOnUIThread(delegate()
                {
                    //if (isManag == false) form1.backgroundWorker1.RunWorkerAsync();
                this.Close();
                });
            }
            else
            {
                MessageBox.Show(ErrorMessage, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process p = Process.Start("https://www.dropbox.com/forgot");
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                {
                    MessageBox.Show(NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process p = Process.Start("https://www.dropbox.com/register");
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                {
                    MessageBox.Show(NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void Sign_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.Black;
            label1.ForeColor = Color.White;
            //Close.BackColor = Color.FromArgb(199, 80, 80);
            button2.BackColor = Color.White;
            button44.BackColor = Color.White;
            panel2.BackColor = Color.Black;
            panel1.BackColor = Color.Black;
            /*if (Properties.Settings.Default.WasWelcomeShown == false)
            {
                panel3.Visible = true;
                this.Text = Welcome;
             * 
            }*/
            if (isManag == true)
            {
                panel2.Visible = true;
                this.ControlBox = true;
                backgroundWorker3.RunWorkerAsync();
                this.Text = AccountInfo;
                label1.Text = Connecting;
            }
        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }

        }
        private void backgroundWorker3_DoWork(object sender, DoWorkEventArgs e)
        {
            var dropBoxStorage = new CloudStorage();
            var dropBoxConfig = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox);
            ICloudStorageAccessToken accessToken;
            StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
            string t = sr.ReadToEnd();
            sr.Close();
            StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
            sw.Write(EncryptDecrypt.DecryptString(t));
            sw.Close();
            using (var fs = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat", FileMode.Open, FileAccess.Read, FileShare.None))
            {
                accessToken = dropBoxStorage.DeserializeSecurityToken(fs);
            }
            StreamWriter sww = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
            sww.Write(t);
            sww.Close();
            try
            {
                var storageToken = dropBoxStorage.Open(dropBoxConfig, accessToken);
            }
            catch { }
            //try
            //{
            try
            {
                dropBoxStorage.DownloadFile("/user.pic", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
            }
            catch
            {
                DoOnUIThread(delegate()
{
    CasetaDeMesaj(form1, UnableToConnect, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
    timer1.Enabled = true;
});

            }
                DoOnUIThread(delegate()
                {
                    using (FileStream stream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic", FileMode.Open, FileAccess.Read))
                    {
                        pictureBox2.Image = Image.FromStream(stream);
                    }
                    //pictureBox2.Image = Image.FromFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic");
                    pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                    openFileDialog1.FileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.pic";
                });
            //}
            //catch { }
            try
            {
                dropBoxStorage.DownloadFile("/user.dat", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode");
                DoOnUIThread(delegate()
                {
                    textBox1.Text = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\user.dat");
                });
            }
            catch { }
                            DoOnUIThread(delegate()
                {
                                        label1.Text = AccountInfo;
                    panel2.Visible = false;
                    panel1.Visible = true;
                    this.Text = AccountInfo;
                    linkLabel4.Visible = false;
                });
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            panel3.Visible = false;
            this.Text = SignIn;
            Properties.Settings.Default.WasWelcomeShown = true;
            Properties.Settings.Default.Save();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
            timer1.Enabled = false;
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Sign_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }
        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Sign_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.Visible = false;

        }
                protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            if (keyData == (Keys.F1))
            {
                if (Directory.Exists(Application.StartupPath + "\\com.valinet.pseudocode.help"))
                {
                    if (mf.hp == null) mf.hp = new Help();
                    mf.hp.Show();
                    if (mf.WindowState != FormWindowState.Maximized)
                    {
                        mf.hp.Location = mf.Location;
                        mf.hp.Size = mf.Size;
                    }
                    mf.hp.WindowState = mf.WindowState;
                }
                else
                {
                    HelpOverview ho = new HelpOverview();
                    ho.ShowDialog();
                }
                return true;
            }
            return false;
        }
    

    }
}
