﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class About : Form
    {
        static bool is64BitProcess = (IntPtr.Size == 8);
        static bool is64BitOperatingSystem = is64BitProcess || InternalCheckIsWow64();

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWow64Process(
            [In] IntPtr hProcess,
            [Out] out bool wow64Process
        );

        public static bool InternalCheckIsWow64()
        {
            if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                Environment.OSVersion.Version.Major >= 6)
            {
                using (Process p = Process.GetCurrentProcess())
                {
                    bool retVal;
                    if (!IsWow64Process(p.Handle, out retVal))
                    {
                        return false;
                    }
                    return retVal;
                }
            }
            else
            {
                return false;
            }
        }
        Form1 form1;
        public About(Form1 frm)
        {
            InitializeComponent();
            string CloseText = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                if (!is64BitOperatingSystem) label1.Text = "Pseudocode 2014 (" + Application.ProductVersion.ToString() + ") on 32-bit.\nPart of the Pseudocode software suite.\n\nCopyright (C) 2006-2014 ValiNet Romania and (C) 2012-2014 ValiNet International. All rights reserved.";
                else if (is64BitOperatingSystem) label1.Text = "Pseudocode 2014 (" + Application.ProductVersion.ToString() + ") on 64-bit.\nPart of the Pseudocode software suite.\n\nCopyright (C) 2006-2014 ValiNet Romania and (C) 2012-2014 ValiNet International. All rights reserved.";
                CloseText = "Close (Alt+F4)";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                if (!is64BitOperatingSystem) label1.Text = "Proiectul Liber Void Pseudocode (L3P), versiunea " + Application.ProductVersion.ToString() + ", pe 32-de biți.\n" + label1.Text ;
                else if (is64BitOperatingSystem) label1.Text = "Proiectul Liber Void Pseudocode (L3P), versiunea " + Application.ProductVersion.ToString() + ", pe 64-de biți.\n" + label1.Text ;

                CloseText = "Închidere (Alt+F4)";
            }
            //toolTip1.SetToolTip(Close, CloseText);
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            form1 = frm;

        }

        private void button44_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void About_Load(object sender, EventArgs e)
        {
            //Close.BackColor = Color.FromArgb(199, 80, 80);
            button44.BackColor = Color.FromArgb(255, 192, 255);
            panel1.BackColor = Color.White;
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://valinet.ro/w/?page_id=367");
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            form1.ThirdPartyInfo();
            this.Close();
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void About_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_MouseEnter(object sender, EventArgs e)
        {
            panel1.Focus();
        }
        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.valinet.ro/pseudocod");
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.valinet.ro/despre");

        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://blinkconceptprojects.blogspot.ro/");

        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            panel1.Visible = !panel1.Visible;
        }

        private void linkLabel1_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("mailto:vali@valinet.ro");
        }

        private void About_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;

        }
    }
}
