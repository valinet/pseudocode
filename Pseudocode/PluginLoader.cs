﻿/*
   Copyright 2013 Christoph Gattnar

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Pseudocode
{
    public static class GenericPluginLoader<VPlugin>
    {
        public static ICollection<VPlugin> LoadPlugins(string path)
        {
            try
            {
                string[] dllFileNames = null;

                if (Directory.Exists(path))
                {
                    StreamReader sr = new StreamReader(path + "\\package-list.txt");
                    string[] pkg = System.Text.RegularExpressions.Regex.Split(sr.ReadToEnd(), "\r\n");
                    sr.Close();
                    dllFileNames = new string[pkg.Length];
                    int i = 0;
                    foreach (string st in pkg)
                    {
                        if (st != "com.valinet.pseudocode" && st != "com.valinet.pseudocode.help" && st != "org.mingw.mingw" && st != "" && st != null)
                        {
                            dllFileNames[i] = path + "\\" + st + "\\" + st + ".dll";
                            i++;
                        }
                        /*dllFileNames = Directory.GetFiles(path, "*.dll");
                        foreach (string s in dllFileNames)
                        {
                            string hash = DTHasher.GetMD5Hash(s);
                            if (hash == "95E7269FC8ACD687FF1AE5974CE78EDF" || hash == "69FDE6012C3879E2397F4D1014CCCBA4")
                            {
                            }
                            else
                            {
                                if (Properties.Settings.Default.UILang == 0)
                                    throw new Exception("Due to Platform Security, the application cannot load add-ons which have not been digitally verified by ValiNet. The application tried to load an unrecognized add-on: " + s);
                                if (Properties.Settings.Default.UILang == 1)
                                    throw new Exception("Datorită Securității Platformei, aplicația nu poate încărca extensii neverificate digital de către ValiNet. S-a încercat încărcarea unei extensii necunoscute: " + s);
                            }
                        }*/
                    }
                    ICollection<Assembly> assemblies = new List<Assembly>(i);
                    foreach (string dllFile in dllFileNames)
                    {
                        if (dllFile != null)
                        {
                            System.Diagnostics.Debug.WriteLine(dllFile);
                            //AssemblyName an = AssemblyName.GetAssemblyName(dllFile);
                            byte[] AssmBytes = File.ReadAllBytes(dllFile);
                            Assembly assembly = Assembly.Load(AssmBytes);
                            assemblies.Add(assembly);
                        }
                    }

                    Type pluginType = typeof(VPlugin);
                    ICollection<Type> pluginTypes = new List<Type>();
                    foreach (Assembly assembly in assemblies)
                    {
                        if (assembly != null)
                        {
                            Type[] types = assembly.GetTypes();

                            foreach (Type type in types)
                            {
                                if (type.IsInterface || type.IsAbstract)
                                {
                                    continue;
                                }
                                else
                                {
                                    if (type.GetInterface(pluginType.FullName) != null)
                                    {
                                        pluginTypes.Add(type);
                                    }
                                }
                            }
                        }
                    }

                    ICollection<VPlugin> plugins = new List<VPlugin>(pluginTypes.Count);
                    foreach (Type type in pluginTypes)
                    {
                        VPlugin plugin = (VPlugin)Activator.CreateInstance(type);
                        plugins.Add(plugin);
                    }

                    return plugins;
                }
            }
            catch (Exception ex)
            {
                if (Properties.Settings.Default.UILang == 0)
                    Properties.Settings.Default.ErrorMessage = "One or more add-ons failed to load. Exception stack is shown bellow.\n\n" + ex.Message;
                if (Properties.Settings.Default.UILang == 1)
                    Properties.Settings.Default.ErrorMessage = "Una sau mai multe extensii nu au putut fi încărcate. Detalii mai jos.\n\n" + ex.Message;
            }

            return null;
        }
    }
}
