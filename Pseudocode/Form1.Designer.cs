﻿using ScintillaNET;
namespace Pseudocode
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.pseudocodeEd = new DoubleBufferedScintilla();
            this.semiTransparentPanel1 = new PseudocodeSyntaxHighlightDemo.SemiTransparentPanel();
            this.VisualScrollBar = new ScintillaNET.Scintilla();
            this.VisualSBPic = new System.Windows.Forms.PictureBox();
            this.SchemaLogica = new BufferedPanel();
            this.contextMenuStrip6 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.whatsThisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label32 = new System.Windows.Forms.Label();
            this.SchemaLogicaCont = new System.Windows.Forms.PictureBox();
            this.converterEd = new ScintillaNET.Scintilla();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.tabControl3 = new Pseudocode.CustomTabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button66 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button65 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.button64 = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button73 = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listBox3 = new Pseudocode.SyncListBox();
            this.listBox4 = new Pseudocode.SyncListBox();
            this.contextMenuStrip5 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.whatsThisToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.button67 = new System.Windows.Forms.Button();
            this.button69 = new System.Windows.Forms.Button();
            this.button70 = new System.Windows.Forms.Button();
            this.button68 = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.restoreDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maximizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button55 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CloseLayer = new System.Windows.Forms.Button();
            this.Restore = new System.Windows.Forms.Button();
            this.Minimize = new System.Windows.Forms.Button();
            this.Maximize = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.MinimizeWhite = new System.Windows.Forms.Button();
            this.MaximizeWhite = new System.Windows.Forms.Button();
            this.MinimizeBlack = new System.Windows.Forms.Button();
            this.MaximizeBlack = new System.Windows.Forms.Button();
            this.RestoreBlack = new System.Windows.Forms.Button();
            this.RestoreWhite = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.button63 = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.button48 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button27 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.docPanel = new System.Windows.Forms.Panel();
            this.FindPanel = new System.Windows.Forms.Panel();
            this.lblLine = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.RecoverPanel = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.ModulePanel = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.EXEPanel = new System.Windows.Forms.Panel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.EXEVersion = new System.Windows.Forms.TextBox();
            this.EXECopyright = new System.Windows.Forms.TextBox();
            this.EXEInternalName = new System.Windows.Forms.TextBox();
            this.EXEProductName = new System.Windows.Forms.TextBox();
            this.EXECompanyName = new System.Windows.Forms.TextBox();
            this.EXEName = new System.Windows.Forms.TextBox();
            this.EXELocation = new System.Windows.Forms.TextBox();
            this.EXEIcon = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label18 = new System.Windows.Forms.Label();
            this.UserPanel = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label27 = new System.Windows.Forms.Label();
            this.button54 = new System.Windows.Forms.Button();
            this.imageDisplay = new System.Windows.Forms.PictureBox();
            this.saveTimer = new System.Windows.Forms.Timer(this.components);
            this.printTimer = new System.Windows.Forms.Timer(this.components);
            this.graduateTimer = new System.Windows.Forms.Timer(this.components);
            this.DocumentEditToolsTimer = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip2 = new CustomizableContextMenuStrip();//(this.components);
            this.ceEsteAceastaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.addremoveBreakpointOnThisLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addremoveWatchOnThisVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addBreakpointsOnAllLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.highlightLineInLogicSchemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findAllReferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.goToErrorLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.lookForErrorOnlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpPanel = new System.Windows.Forms.Panel();
            this.UserDropPanel = new Pseudocode.SelectablePanel();
            this.button47 = new System.Windows.Forms.Button();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.label28 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip4 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.goToErrorLineToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.lookForErrorOnlineToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.button58 = new System.Windows.Forms.Button();
            this.button71 = new System.Windows.Forms.Button();
            this.BottomDown = new System.Windows.Forms.Panel();
            this.DocumentEditToolsTimerClose = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.startupTimer = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.userTimer = new System.Windows.Forms.Timer(this.components);
            this.optionsTimer = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.saveFileDialog3 = new System.Windows.Forms.SaveFileDialog();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.WhatsThis = new System.Windows.Forms.Panel();
            this.webBrowser2 = new System.Windows.Forms.WebBrowser();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.button72 = new System.Windows.Forms.Button();
            this.OldCommandsPanel = new System.Windows.Forms.Panel();
            this.TitleMax = new Pseudocode.PowerLabel();
            this.Title = new Pseudocode.PowerLabel();
            this.panel11 = new Pseudocode.SelectablePanel();
            this.label14 = new System.Windows.Forms.Label();
            this.button35 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.button34 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.button33 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button32 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.button29 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.runPanel = new Pseudocode.SelectablePanel();
            this.label35 = new System.Windows.Forms.Label();
            this.button61 = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.button62 = new System.Windows.Forms.Button();
            this.panel10 = new Pseudocode.SelectablePanel();
            this.label34 = new System.Windows.Forms.Label();
            this.button60 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.button30 = new System.Windows.Forms.Button();
            this.panel12 = new Pseudocode.SelectablePanel();
            this.button57 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.panel9 = new Pseudocode.SelectablePanel();
            this.label33 = new System.Windows.Forms.Label();
            this.button59 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.button28 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.statusBar1 = new CustomizableStatusStrip();
            this.appearanceControl1 = new AppearanceControl();
            this.buttonItem1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonItem2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonItem5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonItem6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressBarItem1 = new System.Windows.Forms.ToolStripProgressBar();
            this.buttonItem4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripTrackBar1 = new Pseudocode.ToolStripTrackBar();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.VS = new Pseudocode.TransparentPanel();
            this.borderSE = new Pseudocode.TransparentPanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.FallDownTimer = new System.Windows.Forms.Timer(this.components);
            this.RiseUpTimer = new System.Windows.Forms.Timer(this.components);
            this.UpdateChecker = new System.ComponentModel.BackgroundWorker();
            this.reidentTimer = new System.Windows.Forms.Timer(this.components);
            this.rangeSelector = new System.Windows.Forms.Timer(this.components);
            this.ShowCodeboardTimer = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog4 = new System.Windows.Forms.SaveFileDialog();
            this.backgroundWorker3 = new System.ComponentModel.BackgroundWorker();
            this.HideCompileStatus = new System.Windows.Forms.Timer(this.components);
            this.runTimer = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker4 = new System.ComponentModel.BackgroundWorker();
            this.markerTimer = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker5 = new System.ComponentModel.BackgroundWorker();
            this.UnminimizedWindow = new System.Windows.Forms.Timer(this.components);
            this.ResizeTimer = new System.Windows.Forms.Timer(this.components);
            this.MinimizeWaitABit = new System.Windows.Forms.Timer(this.components);
            this.RedrawTimer = new System.Windows.Forms.Timer(this.components);
            this.AutoCompile = new System.Windows.Forms.Timer(this.components);
            this.EnterTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pseudocodeEd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisualScrollBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisualSBPic)).BeginInit();
            this.SchemaLogica.SuspendLayout();
            this.contextMenuStrip6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SchemaLogicaCont)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.converterEd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.contextMenuStrip5.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.docPanel.SuspendLayout();
            this.FindPanel.SuspendLayout();
            this.RecoverPanel.SuspendLayout();
            this.ModulePanel.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.EXEPanel.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.UserPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDisplay)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip3.SuspendLayout();
            this.HelpPanel.SuspendLayout();
            this.UserDropPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.contextMenuStrip4.SuspendLayout();
            this.WhatsThis.SuspendLayout();
            this.panel15.SuspendLayout();
            this.OldCommandsPanel.SuspendLayout();
            this.panel11.SuspendLayout();
            this.runPanel.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel9.SuspendLayout();
            this.statusBar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer2
            // 
            resources.ApplyResources(this.splitContainer2, "splitContainer2");
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer4);
            this.helpProvider1.SetShowHelp(this.splitContainer2.Panel1, ((bool)(resources.GetObject("splitContainer2.Panel1.ShowHelp"))));
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.SchemaLogica);
            this.splitContainer2.Panel2.Controls.Add(this.converterEd);
            this.helpProvider1.SetShowHelp(this.splitContainer2.Panel2, ((bool)(resources.GetObject("splitContainer2.Panel2.ShowHelp"))));
            this.helpProvider1.SetShowHelp(this.splitContainer2, ((bool)(resources.GetObject("splitContainer2.ShowHelp"))));
            // 
            // splitContainer4
            // 
            resources.ApplyResources(this.splitContainer4, "splitContainer4");
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.pseudocodeEd);
            this.helpProvider1.SetShowHelp(this.splitContainer4.Panel1, ((bool)(resources.GetObject("splitContainer4.Panel1.ShowHelp"))));
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.semiTransparentPanel1);
            this.splitContainer4.Panel2.Controls.Add(this.VisualScrollBar);
            this.splitContainer4.Panel2.Controls.Add(this.VisualSBPic);
            this.helpProvider1.SetShowHelp(this.splitContainer4.Panel2, ((bool)(resources.GetObject("splitContainer4.Panel2.ShowHelp"))));
            this.helpProvider1.SetShowHelp(this.splitContainer4, ((bool)(resources.GetObject("splitContainer4.ShowHelp"))));
            // 
            // pseudocodeEd
            // 
            this.pseudocodeEd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pseudocodeEd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pseudocodeEd.Caret.CurrentLineBackgroundColor = System.Drawing.Color.DimGray;
            this.pseudocodeEd.Caret.HighlightCurrentLine = true;
            resources.ApplyResources(this.pseudocodeEd, "pseudocodeEd");
            this.pseudocodeEd.Margins.Margin0.Width = 30;
            this.pseudocodeEd.Name = "pseudocodeEd";
            this.helpProvider1.SetShowHelp(this.pseudocodeEd, ((bool)(resources.GetObject("pseudocodeEd.ShowHelp"))));
            this.pseudocodeEd.Styles.BraceBad.FontName = "Verdana\0";
            this.pseudocodeEd.Styles.BraceBad.Size = 9F;
            this.pseudocodeEd.Styles.BraceLight.FontName = "Verdana\0";
            this.pseudocodeEd.Styles.BraceLight.Size = 9F;
            this.pseudocodeEd.Styles.ControlChar.FontName = "Verdana\0";
            this.pseudocodeEd.Styles.ControlChar.Size = 9F;
            this.pseudocodeEd.Styles.Default.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pseudocodeEd.Styles.Default.FontName = "Verdana\0";
            this.pseudocodeEd.Styles.Default.Size = 9F;
            this.pseudocodeEd.Styles.IndentGuide.FontName = "Verdana\0";
            this.pseudocodeEd.Styles.IndentGuide.Size = 9F;
            this.pseudocodeEd.Styles.LastPredefined.FontName = "Verdana\0";
            this.pseudocodeEd.Styles.LastPredefined.Size = 9F;
            this.pseudocodeEd.Styles.LineNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pseudocodeEd.Styles.LineNumber.FontName = "Verdana\0";
            this.pseudocodeEd.Styles.LineNumber.ForeColor = System.Drawing.Color.Silver;
            this.pseudocodeEd.Styles.LineNumber.Size = 9F;
            this.pseudocodeEd.Styles.Max.FontName = "Verdana\0";
            this.pseudocodeEd.Styles.Max.Size = 9F;
            this.pseudocodeEd.AutoCompleteAccepted += new System.EventHandler<ScintillaNET.AutoCompleteAcceptedEventArgs>(this.pseudocodeEd_AutoCompleteAccepted);
            this.pseudocodeEd.Scroll += new System.EventHandler<System.Windows.Forms.ScrollEventArgs>(this.pseudocodeEd_Scroll);
            this.pseudocodeEd.SelectionChanged += new System.EventHandler(this.pseudocodeEd_SelectionChanged);
            this.pseudocodeEd.ZoomFactorChanged += new System.EventHandler(this.pseudocodeEd_ZoomFactorChanged);
            this.pseudocodeEd.TextChanged += new System.EventHandler(this.pseudocodeEd_TextChanged);
            // 
            // semiTransparentPanel1
            // 
            resources.ApplyResources(this.semiTransparentPanel1, "semiTransparentPanel1");
            this.semiTransparentPanel1.Name = "semiTransparentPanel1";
            this.helpProvider1.SetShowHelp(this.semiTransparentPanel1, ((bool)(resources.GetObject("semiTransparentPanel1.ShowHelp"))));
            this.semiTransparentPanel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.semiTransparentPanel1_MouseDown);
            this.semiTransparentPanel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.semiTransparentPanel1_MouseMove);
            // 
            // VisualScrollBar
            // 
            this.VisualScrollBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.VisualScrollBar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.VisualScrollBar.Caret.CurrentLineBackgroundColor = System.Drawing.Color.DimGray;
            this.VisualScrollBar.Caret.HighlightCurrentLine = true;
            resources.ApplyResources(this.VisualScrollBar, "VisualScrollBar");
            this.VisualScrollBar.Margins.Margin1.Width = 0;
            this.VisualScrollBar.Name = "VisualScrollBar";
            this.VisualScrollBar.Scrolling.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.helpProvider1.SetShowHelp(this.VisualScrollBar, ((bool)(resources.GetObject("VisualScrollBar.ShowHelp"))));
            this.VisualScrollBar.Styles.BraceBad.FontName = "Verdana\0\0\0\0";
            this.VisualScrollBar.Styles.BraceBad.Size = 9F;
            this.VisualScrollBar.Styles.BraceLight.FontName = "Verdana\0\0\0\0";
            this.VisualScrollBar.Styles.BraceLight.Size = 9F;
            this.VisualScrollBar.Styles.CallTip.FontName = "Segoe UI\0\0\0";
            this.VisualScrollBar.Styles.ControlChar.FontName = "Verdana\0\0\0\0";
            this.VisualScrollBar.Styles.ControlChar.Size = 9F;
            this.VisualScrollBar.Styles.Default.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.VisualScrollBar.Styles.Default.FontName = "Verdana\0\0\0\0";
            this.VisualScrollBar.Styles.Default.Size = 9F;
            this.VisualScrollBar.Styles.IndentGuide.FontName = "Verdana\0\0\0\0";
            this.VisualScrollBar.Styles.IndentGuide.Size = 9F;
            this.VisualScrollBar.Styles.LastPredefined.FontName = "Verdana\0\0\0\0";
            this.VisualScrollBar.Styles.LastPredefined.Size = 9F;
            this.VisualScrollBar.Styles.LineNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.VisualScrollBar.Styles.LineNumber.FontName = "Verdana\0\0\0\0";
            this.VisualScrollBar.Styles.LineNumber.ForeColor = System.Drawing.Color.Silver;
            this.VisualScrollBar.Styles.LineNumber.Size = 9F;
            this.VisualScrollBar.Styles.Max.FontName = "Verdana\0\0\0\0";
            this.VisualScrollBar.Styles.Max.Size = 9F;
            this.VisualScrollBar.ZoomFactor = -6;
            // 
            // VisualSBPic
            // 
            resources.ApplyResources(this.VisualSBPic, "VisualSBPic");
            this.VisualSBPic.Name = "VisualSBPic";
            this.helpProvider1.SetShowHelp(this.VisualSBPic, ((bool)(resources.GetObject("VisualSBPic.ShowHelp"))));
            this.VisualSBPic.TabStop = false;
            // 
            // SchemaLogica
            // 
            resources.ApplyResources(this.SchemaLogica, "SchemaLogica");
            this.SchemaLogica.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.SchemaLogica.ContextMenuStrip = this.contextMenuStrip6;
            this.SchemaLogica.Controls.Add(this.label32);
            this.SchemaLogica.Controls.Add(this.SchemaLogicaCont);
            this.SchemaLogica.ForeColor = System.Drawing.Color.Black;
            this.SchemaLogica.Name = "SchemaLogica";
            this.helpProvider1.SetShowHelp(this.SchemaLogica, ((bool)(resources.GetObject("SchemaLogica.ShowHelp"))));
            // 
            // contextMenuStrip6
            // 
            this.contextMenuStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.whatsThisToolStripMenuItem});
            this.contextMenuStrip6.Name = "contextMenuStrip6";
            this.helpProvider1.SetShowHelp(this.contextMenuStrip6, ((bool)(resources.GetObject("contextMenuStrip6.ShowHelp"))));
            resources.ApplyResources(this.contextMenuStrip6, "contextMenuStrip6");
            // 
            // whatsThisToolStripMenuItem
            // 
            resources.ApplyResources(this.whatsThisToolStripMenuItem, "whatsThisToolStripMenuItem");
            this.whatsThisToolStripMenuItem.Name = "whatsThisToolStripMenuItem";
            this.whatsThisToolStripMenuItem.Click += new System.EventHandler(this.whatsThisToolStripMenuItem_Click);
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Name = "label32";
            this.helpProvider1.SetShowHelp(this.label32, ((bool)(resources.GetObject("label32.ShowHelp"))));
            // 
            // SchemaLogicaCont
            // 
            this.SchemaLogicaCont.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.SchemaLogicaCont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SchemaLogicaCont.ContextMenuStrip = this.contextMenuStrip6;
            this.SchemaLogicaCont.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.SchemaLogicaCont, "SchemaLogicaCont");
            this.SchemaLogicaCont.Name = "SchemaLogicaCont";
            this.helpProvider1.SetShowHelp(this.SchemaLogicaCont, ((bool)(resources.GetObject("SchemaLogicaCont.ShowHelp"))));
            this.SchemaLogicaCont.TabStop = false;
            this.SchemaLogicaCont.Paint += new System.Windows.Forms.PaintEventHandler(this.SchemaLogicaCont_Paint);
            this.SchemaLogicaCont.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SchemaLogicaCont_MouseDown);
            this.SchemaLogicaCont.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SchemaLogicaCont_MouseMove);
            // 
            // converterEd
            // 
            this.converterEd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.converterEd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.converterEd.Caret.CurrentLineBackgroundColor = System.Drawing.Color.DimGray;
            this.converterEd.Caret.HighlightCurrentLine = true;
            this.converterEd.ContextMenuStrip = this.contextMenuStrip6;
            resources.ApplyResources(this.converterEd, "converterEd");
            this.converterEd.Margins.Margin0.Width = 30;
            this.converterEd.Name = "converterEd";
            this.helpProvider1.SetShowHelp(this.converterEd, ((bool)(resources.GetObject("converterEd.ShowHelp"))));
            this.converterEd.Styles.BraceBad.BackColor = System.Drawing.Color.White;
            this.converterEd.Styles.BraceBad.FontName = "Verdana\0";
            this.converterEd.Styles.BraceBad.Size = 9F;
            this.converterEd.Styles.BraceLight.FontName = "Verdana\0";
            this.converterEd.Styles.BraceLight.Size = 9F;
            this.converterEd.Styles.ControlChar.FontName = "Verdana\0";
            this.converterEd.Styles.ControlChar.Size = 9F;
            this.converterEd.Styles.Default.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.converterEd.Styles.Default.FontName = "Verdana\0";
            this.converterEd.Styles.Default.Size = 9F;
            this.converterEd.Styles.IndentGuide.FontName = "Verdana\0";
            this.converterEd.Styles.IndentGuide.Size = 9F;
            this.converterEd.Styles.LastPredefined.FontName = "Verdana\0";
            this.converterEd.Styles.LastPredefined.Size = 9F;
            this.converterEd.Styles.LineNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.converterEd.Styles.LineNumber.FontName = "Verdana\0";
            this.converterEd.Styles.LineNumber.ForeColor = System.Drawing.Color.Silver;
            this.converterEd.Styles.LineNumber.Size = 9F;
            this.converterEd.Styles.Max.FontName = "Verdana\0";
            this.converterEd.Styles.Max.Size = 9F;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            resources.ApplyResources(this.splitContainer3, "splitContainer3");
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer2);
            this.helpProvider1.SetShowHelp(this.splitContainer3.Panel1, ((bool)(resources.GetObject("splitContainer3.Panel1.ShowHelp"))));
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.splitContainer3.Panel2.Controls.Add(this.tabControl3);
            this.helpProvider1.SetShowHelp(this.splitContainer3.Panel2, ((bool)(resources.GetObject("splitContainer3.Panel2.ShowHelp"))));
            this.helpProvider1.SetShowHelp(this.splitContainer3, ((bool)(resources.GetObject("splitContainer3.ShowHelp"))));
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage3);
            this.tabControl3.Controls.Add(this.tabPage4);
            this.tabControl3.Controls.Add(this.tabPage5);
            resources.ApplyResources(this.tabControl3, "tabControl3");
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.helpProvider1.SetShowHelp(this.tabControl3, ((bool)(resources.GetObject("tabControl3.ShowHelp"))));
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Black;
            this.tabPage3.Controls.Add(this.dataGridView1);
            resources.ApplyResources(this.tabPage3, "tabPage3");
            this.tabPage3.Name = "tabPage3";
            this.helpProvider1.SetShowHelp(this.tabPage3, ((bool)(resources.GetObject("tabPage3.ShowHelp"))));
            // 
            // dataGridView1
            // 
            resources.ApplyResources(this.dataGridView1, "dataGridView1");
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip6;
            this.dataGridView1.Name = "dataGridView1";
            this.helpProvider1.SetShowHelp(this.dataGridView1, ((bool)(resources.GetObject("dataGridView1.ShowHelp"))));
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Black;
            this.tabPage4.Controls.Add(this.button66);
            this.tabPage4.Controls.Add(this.richTextBox1);
            this.tabPage4.Controls.Add(this.button65);
            this.tabPage4.Controls.Add(this.textBox6);
            this.tabPage4.Controls.Add(this.label37);
            this.tabPage4.Controls.Add(this.button64);
            resources.ApplyResources(this.tabPage4, "tabPage4");
            this.tabPage4.Name = "tabPage4";
            this.helpProvider1.SetShowHelp(this.tabPage4, ((bool)(resources.GetObject("tabPage4.ShowHelp"))));
            // 
            // button66
            // 
            resources.ApplyResources(this.button66, "button66");
            this.button66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button66.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button66.FlatAppearance.BorderSize = 0;
            this.button66.ForeColor = System.Drawing.Color.White;
            this.button66.Name = "button66";
            this.helpProvider1.SetShowHelp(this.button66, ((bool)(resources.GetObject("button66.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button66, resources.GetString("button66.ToolTip"));
            this.button66.UseVisualStyleBackColor = false;
            this.button66.Click += new System.EventHandler(this.button66_Click);
            // 
            // richTextBox1
            // 
            resources.ApplyResources(this.richTextBox1, "richTextBox1");
            this.richTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.ContextMenuStrip = this.contextMenuStrip6;
            this.richTextBox1.ForeColor = System.Drawing.Color.White;
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.helpProvider1.SetShowHelp(this.richTextBox1, ((bool)(resources.GetObject("richTextBox1.ShowHelp"))));
            // 
            // button65
            // 
            resources.ApplyResources(this.button65, "button65");
            this.button65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button65.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button65.FlatAppearance.BorderSize = 0;
            this.button65.ForeColor = System.Drawing.Color.White;
            this.button65.Name = "button65";
            this.helpProvider1.SetShowHelp(this.button65, ((bool)(resources.GetObject("button65.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button65, resources.GetString("button65.ToolTip"));
            this.button65.UseVisualStyleBackColor = false;
            this.button65.Click += new System.EventHandler(this.button65_Click);
            // 
            // textBox6
            // 
            resources.ApplyResources(this.textBox6, "textBox6");
            this.textBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox6.ContextMenuStrip = this.contextMenuStrip6;
            this.textBox6.ForeColor = System.Drawing.Color.White;
            this.textBox6.Name = "textBox6";
            this.helpProvider1.SetShowHelp(this.textBox6, ((bool)(resources.GetObject("textBox6.ShowHelp"))));
            this.textBox6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox6_KeyPress);
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Name = "label37";
            this.helpProvider1.SetShowHelp(this.label37, ((bool)(resources.GetObject("label37.ShowHelp"))));
            // 
            // button64
            // 
            resources.ApplyResources(this.button64, "button64");
            this.button64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button64.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button64.FlatAppearance.BorderSize = 0;
            this.button64.ForeColor = System.Drawing.Color.White;
            this.button64.Name = "button64";
            this.helpProvider1.SetShowHelp(this.button64, ((bool)(resources.GetObject("button64.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button64, resources.GetString("button64.ToolTip"));
            this.button64.UseVisualStyleBackColor = false;
            this.button64.Click += new System.EventHandler(this.button64_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.Black;
            this.tabPage5.Controls.Add(this.button73);
            this.tabPage5.Controls.Add(this.splitContainer1);
            this.tabPage5.Controls.Add(this.button67);
            this.tabPage5.Controls.Add(this.button69);
            this.tabPage5.Controls.Add(this.button70);
            this.tabPage5.Controls.Add(this.button68);
            this.tabPage5.Controls.Add(this.label38);
            this.tabPage5.Controls.Add(this.textBox7);
            resources.ApplyResources(this.tabPage5, "tabPage5");
            this.tabPage5.Name = "tabPage5";
            this.helpProvider1.SetShowHelp(this.tabPage5, ((bool)(resources.GetObject("tabPage5.ShowHelp"))));
            // 
            // button73
            // 
            resources.ApplyResources(this.button73, "button73");
            this.button73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button73.FlatAppearance.BorderSize = 0;
            this.button73.ForeColor = System.Drawing.Color.White;
            this.button73.Name = "button73";
            this.helpProvider1.SetShowHelp(this.button73, ((bool)(resources.GetObject("button73.ShowHelp"))));
            this.button73.UseVisualStyleBackColor = false;
            this.button73.Click += new System.EventHandler(this.button73_Click);
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.BackColor = System.Drawing.Color.Black;
            this.splitContainer1.ForeColor = System.Drawing.Color.Black;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.splitContainer1.Panel1.Controls.Add(this.listBox3);
            this.splitContainer1.Panel1.ForeColor = System.Drawing.Color.Black;
            this.helpProvider1.SetShowHelp(this.splitContainer1.Panel1, ((bool)(resources.GetObject("splitContainer1.Panel1.ShowHelp"))));
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.splitContainer1.Panel2.Controls.Add(this.listBox4);
            this.splitContainer1.Panel2.ForeColor = System.Drawing.Color.Black;
            this.helpProvider1.SetShowHelp(this.splitContainer1.Panel2, ((bool)(resources.GetObject("splitContainer1.Panel2.ShowHelp"))));
            this.helpProvider1.SetShowHelp(this.splitContainer1, ((bool)(resources.GetObject("splitContainer1.ShowHelp"))));
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // listBox3
            // 
            this.listBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.listBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox3.ContextMenuStrip = this.contextMenuStrip6;
            resources.ApplyResources(this.listBox3, "listBox3");
            this.listBox3.ForeColor = System.Drawing.Color.White;
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Name = "listBox3";
            this.helpProvider1.SetShowHelp(this.listBox3, ((bool)(resources.GetObject("listBox3.ShowHelp"))));
            this.listBox3.ShowScrollbar = false;
            this.listBox3.OnVerticalScroll += new System.Windows.Forms.ScrollEventHandler(this.listBox3_OnVerticalScroll_1);
            this.listBox3.SelectedIndexChanged += new System.EventHandler(this.listBox3_SelectedIndexChanged);
            this.listBox3.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox3_MouseDoubleClick);
            this.listBox3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox3_MouseDown);
            // 
            // listBox4
            // 
            this.listBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.listBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox4.ContextMenuStrip = this.contextMenuStrip5;
            resources.ApplyResources(this.listBox4, "listBox4");
            this.listBox4.ForeColor = System.Drawing.Color.White;
            this.listBox4.FormattingEnabled = true;
            this.listBox4.Name = "listBox4";
            this.helpProvider1.SetShowHelp(this.listBox4, ((bool)(resources.GetObject("listBox4.ShowHelp"))));
            this.listBox4.ShowScrollbar = false;
            this.listBox4.OnVerticalScroll += new System.Windows.Forms.ScrollEventHandler(this.listBox4_OnVerticalScroll);
            this.listBox4.SelectedIndexChanged += new System.EventHandler(this.listBox4_SelectedIndexChanged);
            // 
            // contextMenuStrip5
            // 
            this.contextMenuStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.whatsThisToolStripMenuItem1,
            this.copyToolStripMenuItem1});
            this.contextMenuStrip5.Name = "contextMenuStrip5";
            this.helpProvider1.SetShowHelp(this.contextMenuStrip5, ((bool)(resources.GetObject("contextMenuStrip5.ShowHelp"))));
            resources.ApplyResources(this.contextMenuStrip5, "contextMenuStrip5");
            // 
            // whatsThisToolStripMenuItem1
            // 
            resources.ApplyResources(this.whatsThisToolStripMenuItem1, "whatsThisToolStripMenuItem1");
            this.whatsThisToolStripMenuItem1.Name = "whatsThisToolStripMenuItem1";
            this.whatsThisToolStripMenuItem1.Click += new System.EventHandler(this.whatsThisToolStripMenuItem1_Click);
            // 
            // copyToolStripMenuItem1
            // 
            this.copyToolStripMenuItem1.Name = "copyToolStripMenuItem1";
            resources.ApplyResources(this.copyToolStripMenuItem1, "copyToolStripMenuItem1");
            this.copyToolStripMenuItem1.Click += new System.EventHandler(this.copyToolStripMenuItem1_Click);
            // 
            // button67
            // 
            resources.ApplyResources(this.button67, "button67");
            this.button67.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button67.FlatAppearance.BorderSize = 0;
            this.button67.ForeColor = System.Drawing.Color.White;
            this.button67.Name = "button67";
            this.helpProvider1.SetShowHelp(this.button67, ((bool)(resources.GetObject("button67.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button67, resources.GetString("button67.ToolTip"));
            this.button67.UseVisualStyleBackColor = false;
            this.button67.Click += new System.EventHandler(this.button67_Click);
            // 
            // button69
            // 
            resources.ApplyResources(this.button69, "button69");
            this.button69.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button69.FlatAppearance.BorderSize = 0;
            this.button69.ForeColor = System.Drawing.Color.White;
            this.button69.Name = "button69";
            this.helpProvider1.SetShowHelp(this.button69, ((bool)(resources.GetObject("button69.ShowHelp"))));
            this.button69.UseVisualStyleBackColor = false;
            this.button69.Click += new System.EventHandler(this.button66_Click);
            // 
            // button70
            // 
            resources.ApplyResources(this.button70, "button70");
            this.button70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button70.FlatAppearance.BorderSize = 0;
            this.button70.ForeColor = System.Drawing.Color.White;
            this.button70.Name = "button70";
            this.helpProvider1.SetShowHelp(this.button70, ((bool)(resources.GetObject("button70.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button70, resources.GetString("button70.ToolTip"));
            this.button70.UseVisualStyleBackColor = false;
            this.button70.Click += new System.EventHandler(this.button65_Click);
            // 
            // button68
            // 
            resources.ApplyResources(this.button68, "button68");
            this.button68.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button68.FlatAppearance.BorderSize = 0;
            this.button68.ForeColor = System.Drawing.Color.White;
            this.button68.Name = "button68";
            this.helpProvider1.SetShowHelp(this.button68, ((bool)(resources.GetObject("button68.ShowHelp"))));
            this.button68.UseVisualStyleBackColor = false;
            this.button68.Click += new System.EventHandler(this.button68_Click);
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Name = "label38";
            this.helpProvider1.SetShowHelp(this.label38, ((bool)(resources.GetObject("label38.ShowHelp"))));
            // 
            // textBox7
            // 
            resources.ApplyResources(this.textBox7, "textBox7");
            this.textBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox7.ForeColor = System.Drawing.Color.White;
            this.textBox7.Name = "textBox7";
            this.helpProvider1.SetShowHelp(this.textBox7, ((bool)(resources.GetObject("textBox7.ShowHelp"))));
            this.textBox7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox7_KeyPress);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.restoreDownToolStripMenuItem,
            this.moveToolStripMenuItem,
            this.sizeToolStripMenuItem,
            this.minimizeToolStripMenuItem,
            this.maximizeToolStripMenuItem,
            this.toolStripMenuItem1,
            this.closeToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.helpProvider1.SetShowHelp(this.contextMenuStrip1, ((bool)(resources.GetObject("contextMenuStrip1.ShowHelp"))));
            resources.ApplyResources(this.contextMenuStrip1, "contextMenuStrip1");
            // 
            // restoreDownToolStripMenuItem
            // 
            this.restoreDownToolStripMenuItem.Name = "restoreDownToolStripMenuItem";
            resources.ApplyResources(this.restoreDownToolStripMenuItem, "restoreDownToolStripMenuItem");
            this.restoreDownToolStripMenuItem.Click += new System.EventHandler(this.restoreDownToolStripMenuItem_Click);
            // 
            // moveToolStripMenuItem
            // 
            resources.ApplyResources(this.moveToolStripMenuItem, "moveToolStripMenuItem");
            this.moveToolStripMenuItem.Name = "moveToolStripMenuItem";
            // 
            // sizeToolStripMenuItem
            // 
            resources.ApplyResources(this.sizeToolStripMenuItem, "sizeToolStripMenuItem");
            this.sizeToolStripMenuItem.Name = "sizeToolStripMenuItem";
            // 
            // minimizeToolStripMenuItem
            // 
            this.minimizeToolStripMenuItem.Name = "minimizeToolStripMenuItem";
            resources.ApplyResources(this.minimizeToolStripMenuItem, "minimizeToolStripMenuItem");
            this.minimizeToolStripMenuItem.Click += new System.EventHandler(this.minimizeToolStripMenuItem_Click);
            // 
            // maximizeToolStripMenuItem
            // 
            this.maximizeToolStripMenuItem.Name = "maximizeToolStripMenuItem";
            resources.ApplyResources(this.maximizeToolStripMenuItem, "maximizeToolStripMenuItem");
            this.maximizeToolStripMenuItem.Click += new System.EventHandler(this.maximizeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            resources.ApplyResources(this.closeToolStripMenuItem, "closeToolStripMenuItem");
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel1.Controls.Add(this.button55);
            this.panel1.Controls.Add(this.button56);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.helpProvider1.SetShowHelp(this.panel1, ((bool)(resources.GetObject("panel1.ShowHelp"))));
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // button55
            // 
            this.button55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button55, "button55");
            this.button55.FlatAppearance.BorderSize = 0;
            this.button55.ForeColor = System.Drawing.Color.Black;
            this.button55.Name = "button55";
            this.helpProvider1.SetShowHelp(this.button55, ((bool)(resources.GetObject("button55.ShowHelp"))));
            this.button55.UseVisualStyleBackColor = false;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // button56
            // 
            this.button56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button56, "button56");
            this.button56.FlatAppearance.BorderSize = 0;
            this.button56.ForeColor = System.Drawing.Color.Black;
            this.button56.Name = "button56";
            this.helpProvider1.SetShowHelp(this.button56, ((bool)(resources.GetObject("button56.ShowHelp"))));
            this.button56.UseVisualStyleBackColor = false;
            this.button56.Click += new System.EventHandler(this.button54_Click);
            this.button56.MouseMove += new System.Windows.Forms.MouseEventHandler(this.button56_MouseMove);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.ForeColor = System.Drawing.Color.Black;
            this.pictureBox1.Name = "pictureBox1";
            this.helpProvider1.SetShowHelp(this.pictureBox1, ((bool)(resources.GetObject("pictureBox1.ShowHelp"))));
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel2.Controls.Add(this.CloseLayer);
            this.panel2.Controls.Add(this.Restore);
            this.panel2.Controls.Add(this.Minimize);
            this.panel2.Controls.Add(this.Maximize);
            this.panel2.Controls.Add(this.Close);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Name = "panel2";
            this.helpProvider1.SetShowHelp(this.panel2, ((bool)(resources.GetObject("panel2.ShowHelp"))));
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // CloseLayer
            // 
            this.CloseLayer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.CloseLayer, "CloseLayer");
            this.CloseLayer.FlatAppearance.BorderSize = 0;
            this.CloseLayer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.CloseLayer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.CloseLayer.ForeColor = System.Drawing.Color.Black;
            this.CloseLayer.Name = "CloseLayer";
            this.helpProvider1.SetShowHelp(this.CloseLayer, ((bool)(resources.GetObject("CloseLayer.ShowHelp"))));
            this.CloseLayer.UseVisualStyleBackColor = false;
            this.CloseLayer.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Restore
            // 
            resources.ApplyResources(this.Restore, "Restore");
            this.Restore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Restore.FlatAppearance.BorderSize = 0;
            this.Restore.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.Restore.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.Restore.ForeColor = System.Drawing.Color.Black;
            this.Restore.Name = "Restore";
            this.helpProvider1.SetShowHelp(this.Restore, ((bool)(resources.GetObject("Restore.ShowHelp"))));
            this.Restore.UseVisualStyleBackColor = false;
            this.Restore.Click += new System.EventHandler(this.Restore_Click);
            this.Restore.MouseEnter += new System.EventHandler(this.Restore_MouseEnter);
            this.Restore.MouseLeave += new System.EventHandler(this.Restore_MouseLeave);
            // 
            // Minimize
            // 
            resources.ApplyResources(this.Minimize, "Minimize");
            this.Minimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Minimize.FlatAppearance.BorderSize = 0;
            this.Minimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.Minimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.Minimize.ForeColor = System.Drawing.Color.Black;
            this.Minimize.Name = "Minimize";
            this.helpProvider1.SetShowHelp(this.Minimize, ((bool)(resources.GetObject("Minimize.ShowHelp"))));
            this.Minimize.UseVisualStyleBackColor = false;
            this.Minimize.Click += new System.EventHandler(this.Minimize_Click);
            this.Minimize.MouseEnter += new System.EventHandler(this.Minimize_MouseEnter);
            this.Minimize.MouseLeave += new System.EventHandler(this.Minimize_MouseLeave);
            // 
            // Maximize
            // 
            resources.ApplyResources(this.Maximize, "Maximize");
            this.Maximize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Maximize.FlatAppearance.BorderSize = 0;
            this.Maximize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.Maximize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.Maximize.ForeColor = System.Drawing.Color.Black;
            this.Maximize.Name = "Maximize";
            this.helpProvider1.SetShowHelp(this.Maximize, ((bool)(resources.GetObject("Maximize.ShowHelp"))));
            this.Maximize.UseVisualStyleBackColor = false;
            this.Maximize.Click += new System.EventHandler(this.Maximize_Click);
            this.Maximize.MouseEnter += new System.EventHandler(this.Maximize_MouseEnter);
            this.Maximize.MouseLeave += new System.EventHandler(this.Maximize_MouseLeave);
            // 
            // Close
            // 
            resources.ApplyResources(this.Close, "Close");
            this.Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Close.FlatAppearance.BorderSize = 0;
            this.Close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.Close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(67)))), ((int)(((byte)(67)))));
            this.Close.ForeColor = System.Drawing.Color.Black;
            this.Close.Name = "Close";
            this.helpProvider1.SetShowHelp(this.Close, ((bool)(resources.GetObject("Close.ShowHelp"))));
            this.Close.UseVisualStyleBackColor = false;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // MinimizeWhite
            // 
            this.MinimizeWhite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.MinimizeWhite, "MinimizeWhite");
            this.MinimizeWhite.FlatAppearance.BorderSize = 0;
            this.MinimizeWhite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.MinimizeWhite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.MinimizeWhite.ForeColor = System.Drawing.Color.Black;
            this.MinimizeWhite.Name = "MinimizeWhite";
            this.helpProvider1.SetShowHelp(this.MinimizeWhite, ((bool)(resources.GetObject("MinimizeWhite.ShowHelp"))));
            this.MinimizeWhite.UseVisualStyleBackColor = false;
            // 
            // MaximizeWhite
            // 
            this.MaximizeWhite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.MaximizeWhite, "MaximizeWhite");
            this.MaximizeWhite.FlatAppearance.BorderSize = 0;
            this.MaximizeWhite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.MaximizeWhite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.MaximizeWhite.ForeColor = System.Drawing.Color.Black;
            this.MaximizeWhite.Name = "MaximizeWhite";
            this.helpProvider1.SetShowHelp(this.MaximizeWhite, ((bool)(resources.GetObject("MaximizeWhite.ShowHelp"))));
            this.MaximizeWhite.UseVisualStyleBackColor = false;
            // 
            // MinimizeBlack
            // 
            this.MinimizeBlack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.MinimizeBlack, "MinimizeBlack");
            this.MinimizeBlack.FlatAppearance.BorderSize = 0;
            this.MinimizeBlack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.MinimizeBlack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.MinimizeBlack.ForeColor = System.Drawing.Color.Black;
            this.MinimizeBlack.Name = "MinimizeBlack";
            this.helpProvider1.SetShowHelp(this.MinimizeBlack, ((bool)(resources.GetObject("MinimizeBlack.ShowHelp"))));
            this.MinimizeBlack.UseVisualStyleBackColor = false;
            // 
            // MaximizeBlack
            // 
            this.MaximizeBlack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.MaximizeBlack, "MaximizeBlack");
            this.MaximizeBlack.FlatAppearance.BorderSize = 0;
            this.MaximizeBlack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.MaximizeBlack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.MaximizeBlack.ForeColor = System.Drawing.Color.Black;
            this.MaximizeBlack.Name = "MaximizeBlack";
            this.helpProvider1.SetShowHelp(this.MaximizeBlack, ((bool)(resources.GetObject("MaximizeBlack.ShowHelp"))));
            this.MaximizeBlack.UseVisualStyleBackColor = false;
            // 
            // RestoreBlack
            // 
            this.RestoreBlack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.RestoreBlack, "RestoreBlack");
            this.RestoreBlack.FlatAppearance.BorderSize = 0;
            this.RestoreBlack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.RestoreBlack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.RestoreBlack.ForeColor = System.Drawing.Color.Black;
            this.RestoreBlack.Name = "RestoreBlack";
            this.helpProvider1.SetShowHelp(this.RestoreBlack, ((bool)(resources.GetObject("RestoreBlack.ShowHelp"))));
            this.RestoreBlack.UseVisualStyleBackColor = false;
            // 
            // RestoreWhite
            // 
            this.RestoreWhite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.RestoreWhite, "RestoreWhite");
            this.RestoreWhite.FlatAppearance.BorderSize = 0;
            this.RestoreWhite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.RestoreWhite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.RestoreWhite.ForeColor = System.Drawing.Color.Black;
            this.RestoreWhite.Name = "RestoreWhite";
            this.helpProvider1.SetShowHelp(this.RestoreWhite, ((bool)(resources.GetObject("RestoreWhite.ShowHelp"))));
            this.RestoreWhite.UseVisualStyleBackColor = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel3
            // 
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel3.Controls.Add(this.button63);
            this.panel3.Controls.Add(this.label31);
            this.panel3.Controls.Add(this.button48);
            this.panel3.Controls.Add(this.button49);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.panel8);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.button21);
            this.panel3.Controls.Add(this.button20);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.button17);
            this.panel3.Controls.Add(this.button18);
            this.panel3.Controls.Add(this.button19);
            this.panel3.Controls.Add(this.button16);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.button15);
            this.panel3.Controls.Add(this.button14);
            this.panel3.Controls.Add(this.button13);
            this.panel3.Controls.Add(this.button12);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.button11);
            this.panel3.Controls.Add(this.button10);
            this.panel3.Controls.Add(this.button9);
            this.panel3.Controls.Add(this.button8);
            this.panel3.Controls.Add(this.button6);
            this.panel3.Controls.Add(this.button7);
            this.panel3.Controls.Add(this.button5);
            this.panel3.Controls.Add(this.button3);
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Name = "panel3";
            this.helpProvider1.SetShowHelp(this.panel3, ((bool)(resources.GetObject("panel3.ShowHelp"))));
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // button63
            // 
            this.button63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button63, "button63");
            this.button63.FlatAppearance.BorderSize = 0;
            this.button63.ForeColor = System.Drawing.Color.Black;
            this.button63.Name = "button63";
            this.helpProvider1.SetShowHelp(this.button63, ((bool)(resources.GetObject("button63.ShowHelp"))));
            this.button63.UseVisualStyleBackColor = false;
            this.button63.Click += new System.EventHandler(this.button63_Click);
            // 
            // label31
            // 
            resources.ApplyResources(this.label31, "label31");
            this.label31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Name = "label31";
            this.helpProvider1.SetShowHelp(this.label31, ((bool)(resources.GetObject("label31.ShowHelp"))));
            this.label31.Click += new System.EventHandler(this.label31_Click);
            this.label31.MouseEnter += new System.EventHandler(this.label31_MouseEnter);
            this.label31.MouseLeave += new System.EventHandler(this.label31_MouseLeave);
            // 
            // button48
            // 
            this.button48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button48, "button48");
            this.button48.FlatAppearance.BorderSize = 0;
            this.button48.ForeColor = System.Drawing.Color.Black;
            this.button48.Name = "button48";
            this.helpProvider1.SetShowHelp(this.button48, ((bool)(resources.GetObject("button48.ShowHelp"))));
            this.button48.UseVisualStyleBackColor = false;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            this.button48.MouseEnter += new System.EventHandler(this.button48_MouseEnter);
            this.button48.MouseLeave += new System.EventHandler(this.button48_MouseLeave);
            // 
            // button49
            // 
            this.button49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button49, "button49");
            this.button49.FlatAppearance.BorderSize = 0;
            this.button49.ForeColor = System.Drawing.Color.Black;
            this.button49.Name = "button49";
            this.helpProvider1.SetShowHelp(this.button49, ((bool)(resources.GetObject("button49.ShowHelp"))));
            this.button49.UseVisualStyleBackColor = false;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Name = "label2";
            this.helpProvider1.SetShowHelp(this.label2, ((bool)(resources.GetObject("label2.ShowHelp"))));
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button2, "button2");
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Name = "button2";
            this.helpProvider1.SetShowHelp(this.button2, ((bool)(resources.GetObject("button2.ShowHelp"))));
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button1, "button1");
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Name = "button1";
            this.helpProvider1.SetShowHelp(this.button1, ((bool)(resources.GetObject("button1.ShowHelp"))));
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            this.button1.MouseEnter += new System.EventHandler(this.button1_MouseEnter_1);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            this.button1.MouseHover += new System.EventHandler(this.button1_MouseHover);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel8.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel8, "panel8");
            this.panel8.Name = "panel8";
            this.helpProvider1.SetShowHelp(this.panel8, ((bool)(resources.GetObject("panel8.ShowHelp"))));
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Name = "label5";
            this.helpProvider1.SetShowHelp(this.label5, ((bool)(resources.GetObject("label5.ShowHelp"))));
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button21, "button21");
            this.button21.FlatAppearance.BorderSize = 0;
            this.button21.ForeColor = System.Drawing.Color.Black;
            this.button21.Name = "button21";
            this.helpProvider1.SetShowHelp(this.button21, ((bool)(resources.GetObject("button21.ShowHelp"))));
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button20, "button20");
            this.button20.FlatAppearance.BorderSize = 0;
            this.button20.ForeColor = System.Drawing.Color.Black;
            this.button20.Name = "button20";
            this.helpProvider1.SetShowHelp(this.button20, ((bool)(resources.GetObject("button20.ShowHelp"))));
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel6.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Name = "panel6";
            this.helpProvider1.SetShowHelp(this.panel6, ((bool)(resources.GetObject("panel6.ShowHelp"))));
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Name = "label3";
            this.helpProvider1.SetShowHelp(this.label3, ((bool)(resources.GetObject("label3.ShowHelp"))));
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button17, "button17");
            this.button17.FlatAppearance.BorderSize = 0;
            this.button17.ForeColor = System.Drawing.Color.Black;
            this.button17.Name = "button17";
            this.helpProvider1.SetShowHelp(this.button17, ((bool)(resources.GetObject("button17.ShowHelp"))));
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button18, "button18");
            this.button18.FlatAppearance.BorderSize = 0;
            this.button18.ForeColor = System.Drawing.Color.Black;
            this.button18.Name = "button18";
            this.helpProvider1.SetShowHelp(this.button18, ((bool)(resources.GetObject("button18.ShowHelp"))));
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button19, "button19");
            this.button19.FlatAppearance.BorderSize = 0;
            this.button19.ForeColor = System.Drawing.Color.Black;
            this.button19.Name = "button19";
            this.helpProvider1.SetShowHelp(this.button19, ((bool)(resources.GetObject("button19.ShowHelp"))));
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button16, "button16");
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.ForeColor = System.Drawing.Color.Black;
            this.button16.Name = "button16";
            this.helpProvider1.SetShowHelp(this.button16, ((bool)(resources.GetObject("button16.ShowHelp"))));
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel5.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            this.helpProvider1.SetShowHelp(this.panel5, ((bool)(resources.GetObject("panel5.ShowHelp"))));
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Name = "label1";
            this.helpProvider1.SetShowHelp(this.label1, ((bool)(resources.GetObject("label1.ShowHelp"))));
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button15, "button15");
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.ForeColor = System.Drawing.Color.Black;
            this.button15.Name = "button15";
            this.helpProvider1.SetShowHelp(this.button15, ((bool)(resources.GetObject("button15.ShowHelp"))));
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button14, "button14");
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.ForeColor = System.Drawing.Color.Black;
            this.button14.Name = "button14";
            this.helpProvider1.SetShowHelp(this.button14, ((bool)(resources.GetObject("button14.ShowHelp"))));
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button13, "button13");
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.ForeColor = System.Drawing.Color.Black;
            this.button13.Name = "button13";
            this.helpProvider1.SetShowHelp(this.button13, ((bool)(resources.GetObject("button13.ShowHelp"))));
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button12, "button12");
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.ForeColor = System.Drawing.Color.Black;
            this.button12.Name = "button12";
            this.helpProvider1.SetShowHelp(this.button12, ((bool)(resources.GetObject("button12.ShowHelp"))));
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel4.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            this.helpProvider1.SetShowHelp(this.panel4, ((bool)(resources.GetObject("panel4.ShowHelp"))));
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button11, "button11");
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.ForeColor = System.Drawing.Color.Black;
            this.button11.Name = "button11";
            this.helpProvider1.SetShowHelp(this.button11, ((bool)(resources.GetObject("button11.ShowHelp"))));
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button10, "button10");
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.ForeColor = System.Drawing.Color.Black;
            this.button10.Name = "button10";
            this.helpProvider1.SetShowHelp(this.button10, ((bool)(resources.GetObject("button10.ShowHelp"))));
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button9, "button9");
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.ForeColor = System.Drawing.Color.Black;
            this.button9.Name = "button9";
            this.helpProvider1.SetShowHelp(this.button9, ((bool)(resources.GetObject("button9.ShowHelp"))));
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            this.button9.MouseEnter += new System.EventHandler(this.button9_MouseEnter);
            this.button9.MouseLeave += new System.EventHandler(this.button9_MouseLeave);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button8, "button8");
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.ForeColor = System.Drawing.Color.Black;
            this.button8.Name = "button8";
            this.helpProvider1.SetShowHelp(this.button8, ((bool)(resources.GetObject("button8.ShowHelp"))));
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button6, "button6");
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.Name = "button6";
            this.helpProvider1.SetShowHelp(this.button6, ((bool)(resources.GetObject("button6.ShowHelp"))));
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button7, "button7");
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.ForeColor = System.Drawing.Color.Black;
            this.button7.Name = "button7";
            this.helpProvider1.SetShowHelp(this.button7, ((bool)(resources.GetObject("button7.ShowHelp"))));
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button5, "button5");
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Name = "button5";
            this.helpProvider1.SetShowHelp(this.button5, ((bool)(resources.GetObject("button5.ShowHelp"))));
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button3, "button3");
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Name = "button3";
            this.helpProvider1.SetShowHelp(this.button3, ((bool)(resources.GetObject("button3.ShowHelp"))));
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Name = "label4";
            this.helpProvider1.SetShowHelp(this.label4, ((bool)(resources.GetObject("label4.ShowHelp"))));
            // 
            // panel7
            // 
            resources.ApplyResources(this.panel7, "panel7");
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel7.Controls.Add(this.button27);
            this.panel7.Controls.Add(this.button26);
            this.panel7.Controls.Add(this.button25);
            this.panel7.Controls.Add(this.button24);
            this.panel7.Controls.Add(this.button23);
            this.panel7.Controls.Add(this.button22);
            this.panel7.ForeColor = System.Drawing.Color.Black;
            this.panel7.Name = "panel7";
            this.helpProvider1.SetShowHelp(this.panel7, ((bool)(resources.GetObject("panel7.ShowHelp"))));
            // 
            // button27
            // 
            resources.ApplyResources(this.button27, "button27");
            this.button27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button27.FlatAppearance.BorderSize = 0;
            this.button27.ForeColor = System.Drawing.Color.Black;
            this.button27.Name = "button27";
            this.helpProvider1.SetShowHelp(this.button27, ((bool)(resources.GetObject("button27.ShowHelp"))));
            this.button27.UseVisualStyleBackColor = false;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button26
            // 
            resources.ApplyResources(this.button26, "button26");
            this.button26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button26.FlatAppearance.BorderSize = 0;
            this.button26.ForeColor = System.Drawing.Color.Black;
            this.button26.Name = "button26";
            this.helpProvider1.SetShowHelp(this.button26, ((bool)(resources.GetObject("button26.ShowHelp"))));
            this.button26.UseVisualStyleBackColor = false;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button25
            // 
            resources.ApplyResources(this.button25, "button25");
            this.button25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button25.FlatAppearance.BorderSize = 0;
            this.button25.ForeColor = System.Drawing.Color.Black;
            this.button25.Name = "button25";
            this.helpProvider1.SetShowHelp(this.button25, ((bool)(resources.GetObject("button25.ShowHelp"))));
            this.button25.UseVisualStyleBackColor = false;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button24
            // 
            resources.ApplyResources(this.button24, "button24");
            this.button24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button24.FlatAppearance.BorderSize = 0;
            this.button24.ForeColor = System.Drawing.Color.Black;
            this.button24.Name = "button24";
            this.helpProvider1.SetShowHelp(this.button24, ((bool)(resources.GetObject("button24.ShowHelp"))));
            this.button24.UseVisualStyleBackColor = false;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button23
            // 
            resources.ApplyResources(this.button23, "button23");
            this.button23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button23.FlatAppearance.BorderSize = 0;
            this.button23.ForeColor = System.Drawing.Color.Black;
            this.button23.Name = "button23";
            this.helpProvider1.SetShowHelp(this.button23, ((bool)(resources.GetObject("button23.ShowHelp"))));
            this.button23.UseVisualStyleBackColor = false;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button22
            // 
            resources.ApplyResources(this.button22, "button22");
            this.button22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button22.FlatAppearance.BorderSize = 0;
            this.button22.ForeColor = System.Drawing.Color.Black;
            this.button22.Name = "button22";
            this.helpProvider1.SetShowHelp(this.button22, ((bool)(resources.GetObject("button22.ShowHelp"))));
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button36
            // 
            this.button36.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button36, "button36");
            this.button36.FlatAppearance.BorderSize = 0;
            this.button36.ForeColor = System.Drawing.Color.Black;
            this.button36.Name = "button36";
            this.helpProvider1.SetShowHelp(this.button36, ((bool)(resources.GetObject("button36.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button36, resources.GetString("button36.ToolTip"));
            this.button36.UseVisualStyleBackColor = false;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // button41
            // 
            this.button41.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button41, "button41");
            this.button41.FlatAppearance.BorderSize = 0;
            this.button41.ForeColor = System.Drawing.Color.Black;
            this.button41.Name = "button41";
            this.helpProvider1.SetShowHelp(this.button41, ((bool)(resources.GetObject("button41.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button41, resources.GetString("button41.ToolTip"));
            this.button41.UseVisualStyleBackColor = false;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button40
            // 
            this.button40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button40.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.button40, "button40");
            this.button40.Name = "button40";
            this.helpProvider1.SetShowHelp(this.button40, ((bool)(resources.GetObject("button40.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button40, resources.GetString("button40.ToolTip"));
            this.button40.UseVisualStyleBackColor = false;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button39.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.button39, "button39");
            this.button39.Name = "button39";
            this.helpProvider1.SetShowHelp(this.button39, ((bool)(resources.GetObject("button39.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button39, resources.GetString("button39.ToolTip"));
            this.button39.UseVisualStyleBackColor = false;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button46
            // 
            this.button46.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button46, "button46");
            this.button46.FlatAppearance.BorderSize = 0;
            this.button46.ForeColor = System.Drawing.Color.Black;
            this.button46.Name = "button46";
            this.helpProvider1.SetShowHelp(this.button46, ((bool)(resources.GetObject("button46.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button46, resources.GetString("button46.ToolTip"));
            this.button46.UseVisualStyleBackColor = false;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // button45
            // 
            this.button45.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button45, "button45");
            this.button45.FlatAppearance.BorderSize = 0;
            this.button45.ForeColor = System.Drawing.Color.Black;
            this.button45.Name = "button45";
            this.helpProvider1.SetShowHelp(this.button45, ((bool)(resources.GetObject("button45.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button45, resources.GetString("button45.ToolTip"));
            this.button45.UseVisualStyleBackColor = false;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button37, "button37");
            this.button37.FlatAppearance.BorderSize = 0;
            this.button37.ForeColor = System.Drawing.Color.Black;
            this.button37.Name = "button37";
            this.helpProvider1.SetShowHelp(this.button37, ((bool)(resources.GetObject("button37.ShowHelp"))));
            this.button37.UseVisualStyleBackColor = false;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button38, "button38");
            this.button38.FlatAppearance.BorderSize = 0;
            this.button38.ForeColor = System.Drawing.Color.Black;
            this.button38.Name = "button38";
            this.helpProvider1.SetShowHelp(this.button38, ((bool)(resources.GetObject("button38.ShowHelp"))));
            this.button38.UseVisualStyleBackColor = false;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // button42
            // 
            resources.ApplyResources(this.button42, "button42");
            this.button42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button42.FlatAppearance.BorderColor = System.Drawing.Color.Purple;
            this.button42.FlatAppearance.BorderSize = 2;
            this.button42.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Purple;
            this.button42.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Violet;
            this.button42.ForeColor = System.Drawing.Color.Black;
            this.button42.Name = "button42";
            this.helpProvider1.SetShowHelp(this.button42, ((bool)(resources.GetObject("button42.ShowHelp"))));
            this.button42.UseVisualStyleBackColor = false;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button43, "button43");
            this.button43.FlatAppearance.BorderColor = System.Drawing.Color.Purple;
            this.button43.FlatAppearance.BorderSize = 2;
            this.button43.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Purple;
            this.button43.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Violet;
            this.button43.ForeColor = System.Drawing.Color.Black;
            this.button43.Name = "button43";
            this.helpProvider1.SetShowHelp(this.button43, ((bool)(resources.GetObject("button43.ShowHelp"))));
            this.button43.UseVisualStyleBackColor = false;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button44
            // 
            resources.ApplyResources(this.button44, "button44");
            this.button44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button44.FlatAppearance.BorderColor = System.Drawing.Color.Purple;
            this.button44.FlatAppearance.BorderSize = 2;
            this.button44.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Purple;
            this.button44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Violet;
            this.button44.ForeColor = System.Drawing.Color.Black;
            this.button44.Name = "button44";
            this.helpProvider1.SetShowHelp(this.button44, ((bool)(resources.GetObject("button44.ShowHelp"))));
            this.button44.UseVisualStyleBackColor = false;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // docPanel
            // 
            resources.ApplyResources(this.docPanel, "docPanel");
            this.docPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.docPanel.Controls.Add(this.FindPanel);
            this.docPanel.Controls.Add(this.RecoverPanel);
            this.docPanel.Controls.Add(this.ModulePanel);
            this.docPanel.Controls.Add(this.EXEPanel);
            this.docPanel.ForeColor = System.Drawing.Color.Black;
            this.docPanel.Name = "docPanel";
            this.helpProvider1.SetShowHelp(this.docPanel, ((bool)(resources.GetObject("docPanel.ShowHelp"))));
            // 
            // FindPanel
            // 
            this.FindPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.FindPanel.Controls.Add(this.button38);
            this.FindPanel.Controls.Add(this.button37);
            this.FindPanel.Controls.Add(this.lblLine);
            this.FindPanel.Controls.Add(this.button36);
            this.FindPanel.Controls.Add(this.label19);
            this.FindPanel.Controls.Add(this.lblStatus);
            this.FindPanel.Controls.Add(this.comboBox1);
            this.FindPanel.Controls.Add(this.label17);
            this.FindPanel.Controls.Add(this.label16);
            this.FindPanel.Controls.Add(this.textBox1);
            this.FindPanel.Controls.Add(this.label15);
            resources.ApplyResources(this.FindPanel, "FindPanel");
            this.FindPanel.ForeColor = System.Drawing.Color.Black;
            this.FindPanel.Name = "FindPanel";
            this.helpProvider1.SetShowHelp(this.FindPanel, ((bool)(resources.GetObject("FindPanel.ShowHelp"))));
            // 
            // lblLine
            // 
            resources.ApplyResources(this.lblLine, "lblLine");
            this.lblLine.BackColor = System.Drawing.Color.Transparent;
            this.lblLine.ForeColor = System.Drawing.Color.Black;
            this.lblLine.Name = "lblLine";
            this.helpProvider1.SetShowHelp(this.lblLine, ((bool)(resources.GetObject("lblLine.ShowHelp"))));
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            this.helpProvider1.SetShowHelp(this.label19, ((bool)(resources.GetObject("label19.ShowHelp"))));
            // 
            // lblStatus
            // 
            resources.ApplyResources(this.lblStatus, "lblStatus");
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.ForeColor = System.Drawing.Color.Black;
            this.lblStatus.Name = "lblStatus";
            this.helpProvider1.SetShowHelp(this.lblStatus, ((bool)(resources.GetObject("lblStatus.ShowHelp"))));
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.comboBox1.ForeColor = System.Drawing.Color.Black;
            this.comboBox1.FormattingEnabled = true;
            resources.ApplyResources(this.comboBox1, "comboBox1");
            this.comboBox1.Name = "comboBox1";
            this.helpProvider1.SetShowHelp(this.comboBox1, ((bool)(resources.GetObject("comboBox1.ShowHelp"))));
            this.comboBox1.DropDown += new System.EventHandler(this.comboBox1_DropDown);
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox1_KeyPress);
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            this.helpProvider1.SetShowHelp(this.label17, ((bool)(resources.GetObject("label17.ShowHelp"))));
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            this.helpProvider1.SetShowHelp(this.label16, ((bool)(resources.GetObject("label16.ShowHelp"))));
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            this.helpProvider1.SetShowHelp(this.textBox1, ((bool)(resources.GetObject("textBox1.ShowHelp"))));
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Name = "label15";
            this.helpProvider1.SetShowHelp(this.label15, ((bool)(resources.GetObject("label15.ShowHelp"))));
            // 
            // RecoverPanel
            // 
            this.RecoverPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.RecoverPanel.Controls.Add(this.listBox1);
            this.RecoverPanel.Controls.Add(this.button44);
            this.RecoverPanel.Controls.Add(this.linkLabel3);
            this.RecoverPanel.Controls.Add(this.button45);
            this.RecoverPanel.Controls.Add(this.label25);
            this.RecoverPanel.Controls.Add(this.label26);
            resources.ApplyResources(this.RecoverPanel, "RecoverPanel");
            this.RecoverPanel.ForeColor = System.Drawing.Color.Black;
            this.RecoverPanel.Name = "RecoverPanel";
            this.helpProvider1.SetShowHelp(this.RecoverPanel, ((bool)(resources.GetObject("RecoverPanel.ShowHelp"))));
            // 
            // listBox1
            // 
            resources.ApplyResources(this.listBox1, "listBox1");
            this.listBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.listBox1.ForeColor = System.Drawing.Color.Black;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Name = "listBox1";
            this.helpProvider1.SetShowHelp(this.listBox1, ((bool)(resources.GetObject("listBox1.ShowHelp"))));
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // linkLabel3
            // 
            this.linkLabel3.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel3.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.linkLabel3, "linkLabel3");
            this.linkLabel3.LinkColor = System.Drawing.Color.Black;
            this.linkLabel3.Name = "linkLabel3";
            this.helpProvider1.SetShowHelp(this.linkLabel3, ((bool)(resources.GetObject("linkLabel3.ShowHelp"))));
            this.linkLabel3.TabStop = true;
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            this.helpProvider1.SetShowHelp(this.label25, ((bool)(resources.GetObject("label25.ShowHelp"))));
            // 
            // label26
            // 
            resources.ApplyResources(this.label26, "label26");
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Name = "label26";
            this.helpProvider1.SetShowHelp(this.label26, ((bool)(resources.GetObject("label26.ShowHelp"))));
            // 
            // ModulePanel
            // 
            this.ModulePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.ModulePanel.Controls.Add(this.tabControl1);
            this.ModulePanel.Controls.Add(this.linkLabel2);
            this.ModulePanel.Controls.Add(this.button46);
            this.ModulePanel.Controls.Add(this.label29);
            this.ModulePanel.Controls.Add(this.label30);
            resources.ApplyResources(this.ModulePanel, "ModulePanel");
            this.ModulePanel.ForeColor = System.Drawing.Color.Black;
            this.ModulePanel.Name = "ModulePanel";
            this.helpProvider1.SetShowHelp(this.ModulePanel, ((bool)(resources.GetObject("ModulePanel.ShowHelp"))));
            // 
            // tabControl1
            // 
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.ForeColor = System.Drawing.Color.Black;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.helpProvider1.SetShowHelp(this.tabControl1, ((bool)(resources.GetObject("tabControl1.ShowHelp"))));
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.TabIndexChanged += new System.EventHandler(this.tabControl1_TabIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button43);
            this.tabPage1.Controls.Add(this.listBox2);
            this.tabPage1.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            this.helpProvider1.SetShowHelp(this.tabPage1, ((bool)(resources.GetObject("tabPage1.ShowHelp"))));
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listBox2
            // 
            resources.ApplyResources(this.listBox2, "listBox2");
            this.listBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.listBox2.ForeColor = System.Drawing.Color.Black;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Name = "listBox2";
            this.helpProvider1.SetShowHelp(this.listBox2, ((bool)(resources.GetObject("listBox2.ShowHelp"))));
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.webBrowser1);
            this.tabPage2.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.Name = "tabPage2";
            this.helpProvider1.SetShowHelp(this.tabPage2, ((bool)(resources.GetObject("tabPage2.ShowHelp"))));
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            resources.ApplyResources(this.webBrowser1, "webBrowser1");
            this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.helpProvider1.SetShowHelp(this.webBrowser1, ((bool)(resources.GetObject("webBrowser1.ShowHelp"))));
            this.webBrowser1.Url = new System.Uri("http://www.valinet.ro/functii/index.htm", System.UriKind.Absolute);
            this.webBrowser1.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.webBrowser1_Navigated);
            // 
            // linkLabel2
            // 
            this.linkLabel2.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel2.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.linkLabel2, "linkLabel2");
            this.linkLabel2.LinkColor = System.Drawing.Color.Black;
            this.linkLabel2.Name = "linkLabel2";
            this.helpProvider1.SetShowHelp(this.linkLabel2, ((bool)(resources.GetObject("linkLabel2.ShowHelp"))));
            this.linkLabel2.TabStop = true;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked_1);
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            this.helpProvider1.SetShowHelp(this.label29, ((bool)(resources.GetObject("label29.ShowHelp"))));
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Name = "label30";
            this.helpProvider1.SetShowHelp(this.label30, ((bool)(resources.GetObject("label30.ShowHelp"))));
            // 
            // EXEPanel
            // 
            this.EXEPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.EXEPanel.Controls.Add(this.linkLabel1);
            this.EXEPanel.Controls.Add(this.panel13);
            this.EXEPanel.Controls.Add(this.button41);
            this.EXEPanel.Controls.Add(this.label23);
            this.EXEPanel.Controls.Add(this.label24);
            this.EXEPanel.Controls.Add(this.panel14);
            this.EXEPanel.Controls.Add(this.button42);
            resources.ApplyResources(this.EXEPanel, "EXEPanel");
            this.EXEPanel.ForeColor = System.Drawing.Color.Black;
            this.EXEPanel.Name = "EXEPanel";
            this.helpProvider1.SetShowHelp(this.EXEPanel, ((bool)(resources.GetObject("EXEPanel.ShowHelp"))));
            // 
            // linkLabel1
            // 
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.linkLabel1, "linkLabel1");
            this.linkLabel1.LinkColor = System.Drawing.Color.Black;
            this.linkLabel1.Name = "linkLabel1";
            this.helpProvider1.SetShowHelp(this.linkLabel1, ((bool)(resources.GetObject("linkLabel1.ShowHelp"))));
            this.linkLabel1.TabStop = true;
            // 
            // panel13
            // 
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel13.Controls.Add(this.EXEVersion);
            this.panel13.Controls.Add(this.EXECopyright);
            this.panel13.Controls.Add(this.EXEInternalName);
            this.panel13.Controls.Add(this.EXEProductName);
            this.panel13.Controls.Add(this.EXECompanyName);
            this.panel13.Controls.Add(this.EXEName);
            this.panel13.Controls.Add(this.button40);
            this.panel13.Controls.Add(this.EXELocation);
            this.panel13.Controls.Add(this.button39);
            this.panel13.Controls.Add(this.EXEIcon);
            this.panel13.Controls.Add(this.label22);
            this.panel13.ForeColor = System.Drawing.Color.Black;
            this.panel13.Name = "panel13";
            this.helpProvider1.SetShowHelp(this.panel13, ((bool)(resources.GetObject("panel13.ShowHelp"))));
            this.panel13.Scroll += new System.Windows.Forms.ScrollEventHandler(this.panel13_Scroll);
            // 
            // EXEVersion
            // 
            this.EXEVersion.BackColor = System.Drawing.Color.White;
            this.EXEVersion.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXEVersion, "EXEVersion");
            this.EXEVersion.Name = "EXEVersion";
            this.helpProvider1.SetShowHelp(this.EXEVersion, ((bool)(resources.GetObject("EXEVersion.ShowHelp"))));
            // 
            // EXECopyright
            // 
            this.EXECopyright.BackColor = System.Drawing.Color.White;
            this.EXECopyright.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXECopyright, "EXECopyright");
            this.EXECopyright.Name = "EXECopyright";
            this.helpProvider1.SetShowHelp(this.EXECopyright, ((bool)(resources.GetObject("EXECopyright.ShowHelp"))));
            // 
            // EXEInternalName
            // 
            this.EXEInternalName.BackColor = System.Drawing.Color.White;
            this.EXEInternalName.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXEInternalName, "EXEInternalName");
            this.EXEInternalName.Name = "EXEInternalName";
            this.helpProvider1.SetShowHelp(this.EXEInternalName, ((bool)(resources.GetObject("EXEInternalName.ShowHelp"))));
            // 
            // EXEProductName
            // 
            this.EXEProductName.BackColor = System.Drawing.Color.White;
            this.EXEProductName.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXEProductName, "EXEProductName");
            this.EXEProductName.Name = "EXEProductName";
            this.helpProvider1.SetShowHelp(this.EXEProductName, ((bool)(resources.GetObject("EXEProductName.ShowHelp"))));
            // 
            // EXECompanyName
            // 
            this.EXECompanyName.BackColor = System.Drawing.Color.White;
            this.EXECompanyName.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXECompanyName, "EXECompanyName");
            this.EXECompanyName.Name = "EXECompanyName";
            this.helpProvider1.SetShowHelp(this.EXECompanyName, ((bool)(resources.GetObject("EXECompanyName.ShowHelp"))));
            // 
            // EXEName
            // 
            this.EXEName.BackColor = System.Drawing.Color.White;
            this.EXEName.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXEName, "EXEName");
            this.EXEName.Name = "EXEName";
            this.helpProvider1.SetShowHelp(this.EXEName, ((bool)(resources.GetObject("EXEName.ShowHelp"))));
            // 
            // EXELocation
            // 
            this.EXELocation.BackColor = System.Drawing.Color.White;
            this.EXELocation.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXELocation, "EXELocation");
            this.EXELocation.Name = "EXELocation";
            this.helpProvider1.SetShowHelp(this.EXELocation, ((bool)(resources.GetObject("EXELocation.ShowHelp"))));
            // 
            // EXEIcon
            // 
            this.EXEIcon.BackColor = System.Drawing.Color.White;
            this.EXEIcon.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.EXEIcon, "EXEIcon");
            this.EXEIcon.Name = "EXEIcon";
            this.helpProvider1.SetShowHelp(this.EXEIcon, ((bool)(resources.GetObject("EXEIcon.ShowHelp"))));
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            this.helpProvider1.SetShowHelp(this.label22, ((bool)(resources.GetObject("label22.ShowHelp"))));
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            this.helpProvider1.SetShowHelp(this.label23, ((bool)(resources.GetObject("label23.ShowHelp"))));
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Name = "label24";
            this.helpProvider1.SetShowHelp(this.label24, ((bool)(resources.GetObject("label24.ShowHelp"))));
            // 
            // panel14
            // 
            resources.ApplyResources(this.panel14, "panel14");
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel14.Controls.Add(this.progressBar1);
            this.panel14.Controls.Add(this.label18);
            this.panel14.ForeColor = System.Drawing.Color.Black;
            this.panel14.Name = "panel14";
            this.helpProvider1.SetShowHelp(this.panel14, ((bool)(resources.GetObject("panel14.ShowHelp"))));
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.progressBar1.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.progressBar1, "progressBar1");
            this.progressBar1.Name = "progressBar1";
            this.helpProvider1.SetShowHelp(this.progressBar1, ((bool)(resources.GetObject("progressBar1.ShowHelp"))));
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            this.helpProvider1.SetShowHelp(this.label18, ((bool)(resources.GetObject("label18.ShowHelp"))));
            // 
            // UserPanel
            // 
            resources.ApplyResources(this.UserPanel, "UserPanel");
            this.UserPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.UserPanel.Controls.Add(this.pictureBox2);
            this.UserPanel.Controls.Add(this.label27);
            this.UserPanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UserPanel.ForeColor = System.Drawing.Color.Black;
            this.UserPanel.Name = "UserPanel";
            this.helpProvider1.SetShowHelp(this.UserPanel, ((bool)(resources.GetObject("UserPanel.ShowHelp"))));
            this.UserPanel.Click += new System.EventHandler(this.UserPanel_Click);
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pictureBox2.ForeColor = System.Drawing.Color.Black;
            this.pictureBox2.Name = "pictureBox2";
            this.helpProvider1.SetShowHelp(this.pictureBox2, ((bool)(resources.GetObject("pictureBox2.ShowHelp"))));
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.UserPanel_Click);
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Name = "label27";
            this.helpProvider1.SetShowHelp(this.label27, ((bool)(resources.GetObject("label27.ShowHelp"))));
            this.label27.Click += new System.EventHandler(this.UserPanel_Click);
            // 
            // button54
            // 
            this.button54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button54.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.button54, "button54");
            this.button54.Name = "button54";
            this.helpProvider1.SetShowHelp(this.button54, ((bool)(resources.GetObject("button54.ShowHelp"))));
            this.button54.UseVisualStyleBackColor = false;
            this.button54.Click += new System.EventHandler(this.button54_Click_1);
            // 
            // imageDisplay
            // 
            this.imageDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.imageDisplay.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.imageDisplay, "imageDisplay");
            this.imageDisplay.Name = "imageDisplay";
            this.helpProvider1.SetShowHelp(this.imageDisplay, ((bool)(resources.GetObject("imageDisplay.ShowHelp"))));
            this.imageDisplay.TabStop = false;
            // 
            // saveTimer
            // 
            this.saveTimer.Interval = 1;
            this.saveTimer.Tick += new System.EventHandler(this.saveTimer_Tick);
            // 
            // printTimer
            // 
            this.printTimer.Interval = 1;
            this.printTimer.Tick += new System.EventHandler(this.printTimer_Tick);
            // 
            // graduateTimer
            // 
            this.graduateTimer.Interval = 1;
            this.graduateTimer.Tick += new System.EventHandler(this.graduateTimer_Tick);
            // 
            // DocumentEditToolsTimer
            // 
            this.DocumentEditToolsTimer.Interval = 1;
            this.DocumentEditToolsTimer.Tick += new System.EventHandler(this.DocumentEditToolsTimer_Tick);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ceEsteAceastaToolStripMenuItem,
            this.toolStripMenuItem6,
            this.addremoveBreakpointOnThisLineToolStripMenuItem,
            this.addremoveWatchOnThisVariableToolStripMenuItem,
            this.addBreakpointsOnAllLinesToolStripMenuItem,
            this.highlightLineInLogicSchemaToolStripMenuItem,
            this.goToDefinitionToolStripMenuItem,
            this.findAllReferencesToolStripMenuItem,
            this.renameVariableToolStripMenuItem,
            this.toolStripMenuItem2,
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripMenuItem3,
            this.deleteToolStripMenuItem,
            this.selectAllToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.helpProvider1.SetShowHelp(this.contextMenuStrip2, ((bool)(resources.GetObject("contextMenuStrip2.ShowHelp"))));
            resources.ApplyResources(this.contextMenuStrip2, "contextMenuStrip2");
            // 
            // ceEsteAceastaToolStripMenuItem
            // 
            this.ceEsteAceastaToolStripMenuItem.Name = "ceEsteAceastaToolStripMenuItem";
            resources.ApplyResources(this.ceEsteAceastaToolStripMenuItem, "ceEsteAceastaToolStripMenuItem");
            this.ceEsteAceastaToolStripMenuItem.Click += new System.EventHandler(this.ceEsteAceastaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            resources.ApplyResources(this.toolStripMenuItem6, "toolStripMenuItem6");
            // 
            // addremoveBreakpointOnThisLineToolStripMenuItem
            // 
            this.addremoveBreakpointOnThisLineToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.addremoveBreakpointOnThisLineToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.addremoveBreakpointOnThisLineToolStripMenuItem.Name = "addremoveBreakpointOnThisLineToolStripMenuItem";
            resources.ApplyResources(this.addremoveBreakpointOnThisLineToolStripMenuItem, "addremoveBreakpointOnThisLineToolStripMenuItem");
            this.addremoveBreakpointOnThisLineToolStripMenuItem.Click += new System.EventHandler(this.addremoveBreakpointOnThisLineToolStripMenuItem_Click);
            // 
            // addremoveWatchOnThisVariableToolStripMenuItem
            // 
            this.addremoveWatchOnThisVariableToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.addremoveWatchOnThisVariableToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.addremoveWatchOnThisVariableToolStripMenuItem.Name = "addremoveWatchOnThisVariableToolStripMenuItem";
            resources.ApplyResources(this.addremoveWatchOnThisVariableToolStripMenuItem, "addremoveWatchOnThisVariableToolStripMenuItem");
            this.addremoveWatchOnThisVariableToolStripMenuItem.Click += new System.EventHandler(this.addremoveWatchOnThisVariableToolStripMenuItem_Click);
            // 
            // addBreakpointsOnAllLinesToolStripMenuItem
            // 
            this.addBreakpointsOnAllLinesToolStripMenuItem.Name = "addBreakpointsOnAllLinesToolStripMenuItem";
            resources.ApplyResources(this.addBreakpointsOnAllLinesToolStripMenuItem, "addBreakpointsOnAllLinesToolStripMenuItem");
            this.addBreakpointsOnAllLinesToolStripMenuItem.Click += new System.EventHandler(this.addBreakpointsOnAllLinesToolStripMenuItem_Click);
            // 
            // highlightLineInLogicSchemaToolStripMenuItem
            // 
            this.highlightLineInLogicSchemaToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.highlightLineInLogicSchemaToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.highlightLineInLogicSchemaToolStripMenuItem.Name = "highlightLineInLogicSchemaToolStripMenuItem";
            resources.ApplyResources(this.highlightLineInLogicSchemaToolStripMenuItem, "highlightLineInLogicSchemaToolStripMenuItem");
            // 
            // goToDefinitionToolStripMenuItem
            // 
            this.goToDefinitionToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.goToDefinitionToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.goToDefinitionToolStripMenuItem.Name = "goToDefinitionToolStripMenuItem";
            resources.ApplyResources(this.goToDefinitionToolStripMenuItem, "goToDefinitionToolStripMenuItem");
            this.goToDefinitionToolStripMenuItem.Click += new System.EventHandler(this.goToDefinitionToolStripMenuItem_Click);
            // 
            // findAllReferencesToolStripMenuItem
            // 
            this.findAllReferencesToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.findAllReferencesToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.findAllReferencesToolStripMenuItem.Name = "findAllReferencesToolStripMenuItem";
            resources.ApplyResources(this.findAllReferencesToolStripMenuItem, "findAllReferencesToolStripMenuItem");
            // 
            // renameVariableToolStripMenuItem
            // 
            this.renameVariableToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.renameVariableToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.renameVariableToolStripMenuItem.Name = "renameVariableToolStripMenuItem";
            resources.ApplyResources(this.renameVariableToolStripMenuItem, "renameVariableToolStripMenuItem");
            this.renameVariableToolStripMenuItem.Click += new System.EventHandler(this.renameVariableToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.undoToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.undoToolStripMenuItem, "undoToolStripMenuItem");
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.redoToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.redoToolStripMenuItem, "redoToolStripMenuItem");
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.cutToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.cutToolStripMenuItem, "cutToolStripMenuItem");
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.copyToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.copyToolStripMenuItem, "copyToolStripMenuItem");
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.pasteToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.pasteToolStripMenuItem, "pasteToolStripMenuItem");
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.deleteToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.deleteToolStripMenuItem, "deleteToolStripMenuItem");
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.selectAllToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.selectAllToolStripMenuItem, "selectAllToolStripMenuItem");
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToErrorLineToolStripMenuItem,
            this.toolStripMenuItem4,
            this.lookForErrorOnlineToolStripMenuItem});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            this.helpProvider1.SetShowHelp(this.contextMenuStrip3, ((bool)(resources.GetObject("contextMenuStrip3.ShowHelp"))));
            resources.ApplyResources(this.contextMenuStrip3, "contextMenuStrip3");
            // 
            // goToErrorLineToolStripMenuItem
            // 
            this.goToErrorLineToolStripMenuItem.Name = "goToErrorLineToolStripMenuItem";
            resources.ApplyResources(this.goToErrorLineToolStripMenuItem, "goToErrorLineToolStripMenuItem");
            this.goToErrorLineToolStripMenuItem.Click += new System.EventHandler(this.goToErrorLineToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
            // 
            // lookForErrorOnlineToolStripMenuItem
            // 
            this.lookForErrorOnlineToolStripMenuItem.Name = "lookForErrorOnlineToolStripMenuItem";
            resources.ApplyResources(this.lookForErrorOnlineToolStripMenuItem, "lookForErrorOnlineToolStripMenuItem");
            this.lookForErrorOnlineToolStripMenuItem.Click += new System.EventHandler(this.lookForErrorOnlineToolStripMenuItem_Click);
            // 
            // HelpPanel
            // 
            this.HelpPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.HelpPanel.Controls.Add(this.UserDropPanel);
            this.HelpPanel.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.HelpPanel, "HelpPanel");
            this.HelpPanel.Name = "HelpPanel";
            this.helpProvider1.SetShowHelp(this.HelpPanel, ((bool)(resources.GetObject("HelpPanel.ShowHelp"))));
            // 
            // UserDropPanel
            // 
            resources.ApplyResources(this.UserDropPanel, "UserDropPanel");
            this.UserDropPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.UserDropPanel.Controls.Add(this.button47);
            this.UserDropPanel.Controls.Add(this.linkLabel5);
            this.UserDropPanel.Controls.Add(this.linkLabel4);
            this.UserDropPanel.Controls.Add(this.label28);
            this.UserDropPanel.Controls.Add(this.pictureBox3);
            this.UserDropPanel.Controls.Add(this.pictureBox4);
            this.UserDropPanel.ForeColor = System.Drawing.Color.Black;
            this.UserDropPanel.Name = "UserDropPanel";
            this.helpProvider1.SetShowHelp(this.UserDropPanel, ((bool)(resources.GetObject("UserDropPanel.ShowHelp"))));
            this.UserDropPanel.TabStop = true;
            this.UserDropPanel.Leave += new System.EventHandler(this.UserDropPanel_Leave);
            // 
            // button47
            // 
            this.button47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button47, "button47");
            this.button47.FlatAppearance.BorderSize = 0;
            this.button47.ForeColor = System.Drawing.Color.Black;
            this.button47.Name = "button47";
            this.helpProvider1.SetShowHelp(this.button47, ((bool)(resources.GetObject("button47.ShowHelp"))));
            this.button47.UseVisualStyleBackColor = false;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // linkLabel5
            // 
            resources.ApplyResources(this.linkLabel5, "linkLabel5");
            this.linkLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.linkLabel5.ForeColor = System.Drawing.Color.Black;
            this.linkLabel5.LinkColor = System.Drawing.Color.Black;
            this.linkLabel5.Name = "linkLabel5";
            this.helpProvider1.SetShowHelp(this.linkLabel5, ((bool)(resources.GetObject("linkLabel5.ShowHelp"))));
            this.linkLabel5.TabStop = true;
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // linkLabel4
            // 
            resources.ApplyResources(this.linkLabel4, "linkLabel4");
            this.linkLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.linkLabel4.ForeColor = System.Drawing.Color.Black;
            this.linkLabel4.LinkColor = System.Drawing.Color.Black;
            this.linkLabel4.Name = "linkLabel4";
            this.helpProvider1.SetShowHelp(this.linkLabel4, ((bool)(resources.GetObject("linkLabel4.ShowHelp"))));
            this.linkLabel4.TabStop = true;
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.label28, "label28");
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Name = "label28";
            this.helpProvider1.SetShowHelp(this.label28, ((bool)(resources.GetObject("label28.ShowHelp"))));
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pictureBox3.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.pictureBox3, "pictureBox3");
            this.pictureBox3.Name = "pictureBox3";
            this.helpProvider1.SetShowHelp(this.pictureBox3, ((bool)(resources.GetObject("pictureBox3.ShowHelp"))));
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pictureBox4.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.pictureBox4, "pictureBox4");
            this.pictureBox4.Name = "pictureBox4";
            this.helpProvider1.SetShowHelp(this.pictureBox4, ((bool)(resources.GetObject("pictureBox4.ShowHelp"))));
            this.pictureBox4.TabStop = false;
            // 
            // contextMenuStrip4
            // 
            this.contextMenuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToErrorLineToolStripMenuItem1,
            this.toolStripMenuItem5,
            this.lookForErrorOnlineToolStripMenuItem1});
            this.contextMenuStrip4.Name = "contextMenuStrip4";
            this.helpProvider1.SetShowHelp(this.contextMenuStrip4, ((bool)(resources.GetObject("contextMenuStrip4.ShowHelp"))));
            resources.ApplyResources(this.contextMenuStrip4, "contextMenuStrip4");
            // 
            // goToErrorLineToolStripMenuItem1
            // 
            this.goToErrorLineToolStripMenuItem1.Name = "goToErrorLineToolStripMenuItem1";
            resources.ApplyResources(this.goToErrorLineToolStripMenuItem1, "goToErrorLineToolStripMenuItem1");
            this.goToErrorLineToolStripMenuItem1.Click += new System.EventHandler(this.goToErrorLineToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            resources.ApplyResources(this.toolStripMenuItem5, "toolStripMenuItem5");
            // 
            // lookForErrorOnlineToolStripMenuItem1
            // 
            this.lookForErrorOnlineToolStripMenuItem1.Name = "lookForErrorOnlineToolStripMenuItem1";
            resources.ApplyResources(this.lookForErrorOnlineToolStripMenuItem1, "lookForErrorOnlineToolStripMenuItem1");
            this.lookForErrorOnlineToolStripMenuItem1.Click += new System.EventHandler(this.lookForErrorOnlineToolStripMenuItem1_Click);
            // 
            // button58
            // 
            this.button58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button58.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.button58, "button58");
            this.button58.Name = "button58";
            this.helpProvider1.SetShowHelp(this.button58, ((bool)(resources.GetObject("button58.ShowHelp"))));
            this.button58.UseVisualStyleBackColor = false;
            this.button58.Click += new System.EventHandler(this.button58_Click);
            // 
            // button71
            // 
            this.button71.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button71.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.button71, "button71");
            this.button71.Name = "button71";
            this.helpProvider1.SetShowHelp(this.button71, ((bool)(resources.GetObject("button71.ShowHelp"))));
            this.button71.UseVisualStyleBackColor = false;
            this.button71.Click += new System.EventHandler(this.button71_Click);
            // 
            // BottomDown
            // 
            resources.ApplyResources(this.BottomDown, "BottomDown");
            this.BottomDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BottomDown.Name = "BottomDown";
            this.helpProvider1.SetShowHelp(this.BottomDown, ((bool)(resources.GetObject("BottomDown.ShowHelp"))));
            // 
            // DocumentEditToolsTimerClose
            // 
            this.DocumentEditToolsTimerClose.Interval = 1;
            this.DocumentEditToolsTimerClose.Tick += new System.EventHandler(this.DocumentEditToolsTimerClose_Tick);
            // 
            // saveFileDialog1
            // 
            resources.ApplyResources(this.saveFileDialog1, "saveFileDialog1");
            // 
            // startupTimer
            // 
            this.startupTimer.Enabled = true;
            this.startupTimer.Tick += new System.EventHandler(this.startupTimer_Tick);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // userTimer
            // 
            this.userTimer.Interval = 1;
            this.userTimer.Tick += new System.EventHandler(this.userTimer_Tick);
            // 
            // optionsTimer
            // 
            this.optionsTimer.Interval = 1;
            this.optionsTimer.Tick += new System.EventHandler(this.optionsTimer_Tick);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog1";
            resources.ApplyResources(this.openFileDialog2, "openFileDialog2");
            // 
            // saveFileDialog2
            // 
            resources.ApplyResources(this.saveFileDialog2, "saveFileDialog2");
            // 
            // saveFileDialog3
            // 
            resources.ApplyResources(this.saveFileDialog3, "saveFileDialog3");
            // 
            // WhatsThis
            // 
            this.WhatsThis.BackColor = System.Drawing.Color.LightGray;
            this.WhatsThis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WhatsThis.Controls.Add(this.webBrowser2);
            this.WhatsThis.Controls.Add(this.panel15);
            resources.ApplyResources(this.WhatsThis, "WhatsThis");
            this.WhatsThis.Name = "WhatsThis";
            this.helpProvider1.SetShowHelp(this.WhatsThis, ((bool)(resources.GetObject("WhatsThis.ShowHelp"))));
            this.WhatsThis.Leave += new System.EventHandler(this.WhatsThis_Leave);
            // 
            // webBrowser2
            // 
            resources.ApplyResources(this.webBrowser2, "webBrowser2");
            this.webBrowser2.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser2.Name = "webBrowser2";
            this.webBrowser2.ScriptErrorsSuppressed = true;
            this.helpProvider1.SetShowHelp(this.webBrowser2, ((bool)(resources.GetObject("webBrowser2.ShowHelp"))));
            this.webBrowser2.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.webBrowser2_Navigating);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.LightGray;
            this.panel15.Controls.Add(this.label20);
            this.panel15.Controls.Add(this.button72);
            resources.ApplyResources(this.panel15, "panel15");
            this.panel15.Name = "panel15";
            this.helpProvider1.SetShowHelp(this.panel15, ((bool)(resources.GetObject("panel15.ShowHelp"))));
            this.panel15.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label20_MouseDown);
            this.panel15.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label20_MouseMove);
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.label20, "label20");
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Name = "label20";
            this.helpProvider1.SetShowHelp(this.label20, ((bool)(resources.GetObject("label20.ShowHelp"))));
            this.label20.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label20_MouseDown);
            this.label20.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label20_MouseMove);
            // 
            // button72
            // 
            resources.ApplyResources(this.button72, "button72");
            this.button72.BackColor = System.Drawing.Color.Transparent;
            this.button72.FlatAppearance.BorderSize = 0;
            this.button72.ForeColor = System.Drawing.Color.Black;
            this.button72.Name = "button72";
            this.helpProvider1.SetShowHelp(this.button72, ((bool)(resources.GetObject("button72.ShowHelp"))));
            this.toolTip1.SetToolTip(this.button72, resources.GetString("button72.ToolTip"));
            this.button72.UseVisualStyleBackColor = false;
            this.button72.Click += new System.EventHandler(this.button72_Click);
            // 
            // OldCommandsPanel
            // 
            this.OldCommandsPanel.Controls.Add(this.panel3);
            this.OldCommandsPanel.Controls.Add(this.panel7);
            this.OldCommandsPanel.Controls.Add(this.TitleMax);
            this.OldCommandsPanel.Controls.Add(this.Title);
            this.OldCommandsPanel.Controls.Add(this.panel2);
            this.OldCommandsPanel.Controls.Add(this.panel1);
            this.OldCommandsPanel.Controls.Add(this.button58);
            this.OldCommandsPanel.Controls.Add(this.button71);
            this.OldCommandsPanel.Controls.Add(this.panel11);
            this.OldCommandsPanel.Controls.Add(this.runPanel);
            this.OldCommandsPanel.Controls.Add(this.HelpPanel);
            this.OldCommandsPanel.Controls.Add(this.panel10);
            this.OldCommandsPanel.Controls.Add(this.panel9);
            resources.ApplyResources(this.OldCommandsPanel, "OldCommandsPanel");
            this.OldCommandsPanel.Name = "OldCommandsPanel";
            this.helpProvider1.SetShowHelp(this.OldCommandsPanel, ((bool)(resources.GetObject("OldCommandsPanel.ShowHelp"))));
            // 
            // TitleMax
            // 
            resources.ApplyResources(this.TitleMax, "TitleMax");
            this.TitleMax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TitleMax.ForeColor = System.Drawing.Color.Black;
            this.TitleMax.Name = "TitleMax";
            this.helpProvider1.SetShowHelp(this.TitleMax, ((bool)(resources.GetObject("TitleMax.ShowHelp"))));
            this.TitleMax.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Title_MouseDown);
            // 
            // Title
            // 
            resources.ApplyResources(this.Title, "Title");
            this.Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Title.ForeColor = System.Drawing.Color.Black;
            this.Title.Name = "Title";
            this.helpProvider1.SetShowHelp(this.Title, ((bool)(resources.GetObject("Title.ShowHelp"))));
            this.Title.TextChanged += new System.EventHandler(this.Title_TextChanged);
            this.Title.Click += new System.EventHandler(this.Title_Click);
            this.Title.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Title_MouseDown);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel11.Controls.Add(this.label14);
            this.panel11.Controls.Add(this.button35);
            this.panel11.Controls.Add(this.label13);
            this.panel11.Controls.Add(this.button34);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.button33);
            this.panel11.Controls.Add(this.label10);
            this.panel11.Controls.Add(this.label11);
            this.panel11.Controls.Add(this.button32);
            this.panel11.Controls.Add(this.label8);
            this.panel11.Controls.Add(this.button29);
            this.panel11.Controls.Add(this.button31);
            this.panel11.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel11, "panel11");
            this.panel11.Name = "panel11";
            this.helpProvider1.SetShowHelp(this.panel11, ((bool)(resources.GetObject("panel11.ShowHelp"))));
            this.panel11.TabStop = true;
            this.panel11.Paint += new System.Windows.Forms.PaintEventHandler(this.selectablePanel1_Paint);
            this.panel11.Leave += new System.EventHandler(this.panel11_Leave);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label14.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            this.helpProvider1.SetShowHelp(this.label14, ((bool)(resources.GetObject("label14.ShowHelp"))));
            this.label14.Click += new System.EventHandler(this.button35_Click);
            this.label14.MouseEnter += new System.EventHandler(this.label14_MouseEnter);
            this.label14.MouseLeave += new System.EventHandler(this.label14_MouseLeave);
            // 
            // button35
            // 
            this.button35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button35, "button35");
            this.button35.FlatAppearance.BorderSize = 0;
            this.button35.ForeColor = System.Drawing.Color.Black;
            this.button35.Name = "button35";
            this.helpProvider1.SetShowHelp(this.button35, ((bool)(resources.GetObject("button35.ShowHelp"))));
            this.button35.UseVisualStyleBackColor = false;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            this.button35.MouseEnter += new System.EventHandler(this.button35_MouseEnter);
            this.button35.MouseLeave += new System.EventHandler(this.button35_MouseLeave);
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label13.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            this.helpProvider1.SetShowHelp(this.label13, ((bool)(resources.GetObject("label13.ShowHelp"))));
            this.label13.Click += new System.EventHandler(this.button34_Click);
            this.label13.MouseEnter += new System.EventHandler(this.label13_MouseEnter);
            this.label13.MouseLeave += new System.EventHandler(this.label13_MouseLeave);
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button34, "button34");
            this.button34.FlatAppearance.BorderSize = 0;
            this.button34.ForeColor = System.Drawing.Color.Black;
            this.button34.Name = "button34";
            this.helpProvider1.SetShowHelp(this.button34, ((bool)(resources.GetObject("button34.ShowHelp"))));
            this.button34.UseVisualStyleBackColor = false;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            this.button34.MouseEnter += new System.EventHandler(this.button34_MouseEnter);
            this.button34.MouseLeave += new System.EventHandler(this.button34_MouseLeave);
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label12.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            this.helpProvider1.SetShowHelp(this.label12, ((bool)(resources.GetObject("label12.ShowHelp"))));
            this.label12.Click += new System.EventHandler(this.button33_Click);
            this.label12.MouseEnter += new System.EventHandler(this.label12_MouseEnter);
            this.label12.MouseLeave += new System.EventHandler(this.label12_MouseLeave);
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button33, "button33");
            this.button33.FlatAppearance.BorderSize = 0;
            this.button33.ForeColor = System.Drawing.Color.Black;
            this.button33.Name = "button33";
            this.helpProvider1.SetShowHelp(this.button33, ((bool)(resources.GetObject("button33.ShowHelp"))));
            this.button33.UseVisualStyleBackColor = false;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            this.button33.MouseEnter += new System.EventHandler(this.button33_MouseEnter);
            this.button33.MouseLeave += new System.EventHandler(this.button33_MouseLeave);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label10.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            this.helpProvider1.SetShowHelp(this.label10, ((bool)(resources.GetObject("label10.ShowHelp"))));
            this.label10.Click += new System.EventHandler(this.button31_Click);
            this.label10.MouseEnter += new System.EventHandler(this.label10_MouseEnter);
            this.label10.MouseLeave += new System.EventHandler(this.label10_MouseLeave);
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label11.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            this.helpProvider1.SetShowHelp(this.label11, ((bool)(resources.GetObject("label11.ShowHelp"))));
            this.label11.Click += new System.EventHandler(this.button32_Click);
            this.label11.MouseEnter += new System.EventHandler(this.label11_MouseEnter);
            this.label11.MouseLeave += new System.EventHandler(this.label11_MouseLeave);
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button32, "button32");
            this.button32.FlatAppearance.BorderSize = 0;
            this.button32.ForeColor = System.Drawing.Color.Black;
            this.button32.Name = "button32";
            this.helpProvider1.SetShowHelp(this.button32, ((bool)(resources.GetObject("button32.ShowHelp"))));
            this.button32.UseVisualStyleBackColor = false;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            this.button32.MouseEnter += new System.EventHandler(this.button32_MouseEnter);
            this.button32.MouseLeave += new System.EventHandler(this.button32_MouseLeave);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label8.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            this.helpProvider1.SetShowHelp(this.label8, ((bool)(resources.GetObject("label8.ShowHelp"))));
            this.label8.Click += new System.EventHandler(this.button29_Click);
            this.label8.MouseEnter += new System.EventHandler(this.label8_MouseEnter);
            this.label8.MouseLeave += new System.EventHandler(this.label8_MouseLeave);
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button29, "button29");
            this.button29.FlatAppearance.BorderSize = 0;
            this.button29.ForeColor = System.Drawing.Color.Black;
            this.button29.Name = "button29";
            this.helpProvider1.SetShowHelp(this.button29, ((bool)(resources.GetObject("button29.ShowHelp"))));
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            this.button29.MouseEnter += new System.EventHandler(this.button29_MouseEnter);
            this.button29.MouseLeave += new System.EventHandler(this.button29_MouseLeave);
            // 
            // button31
            // 
            this.button31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button31, "button31");
            this.button31.FlatAppearance.BorderSize = 0;
            this.button31.ForeColor = System.Drawing.Color.Black;
            this.button31.Name = "button31";
            this.helpProvider1.SetShowHelp(this.button31, ((bool)(resources.GetObject("button31.ShowHelp"))));
            this.button31.UseVisualStyleBackColor = false;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            this.button31.MouseEnter += new System.EventHandler(this.button31_MouseEnter);
            this.button31.MouseLeave += new System.EventHandler(this.button31_MouseLeave);
            // 
            // runPanel
            // 
            this.runPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.runPanel.Controls.Add(this.label35);
            this.runPanel.Controls.Add(this.button61);
            this.runPanel.Controls.Add(this.label36);
            this.runPanel.Controls.Add(this.button62);
            this.runPanel.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.runPanel, "runPanel");
            this.runPanel.Name = "runPanel";
            this.helpProvider1.SetShowHelp(this.runPanel, ((bool)(resources.GetObject("runPanel.ShowHelp"))));
            this.runPanel.TabStop = true;
            this.runPanel.Leave += new System.EventHandler(this.runPanel_Leave);
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label35.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label35, "label35");
            this.label35.Name = "label35";
            this.helpProvider1.SetShowHelp(this.label35, ((bool)(resources.GetObject("label35.ShowHelp"))));
            this.label35.Click += new System.EventHandler(this.button61_Click);
            this.label35.MouseEnter += new System.EventHandler(this.label35_MouseEnter);
            this.label35.MouseLeave += new System.EventHandler(this.label35_MouseLeave);
            // 
            // button61
            // 
            this.button61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button61, "button61");
            this.button61.FlatAppearance.BorderSize = 0;
            this.button61.ForeColor = System.Drawing.Color.Black;
            this.button61.Name = "button61";
            this.helpProvider1.SetShowHelp(this.button61, ((bool)(resources.GetObject("button61.ShowHelp"))));
            this.button61.UseVisualStyleBackColor = false;
            this.button61.Click += new System.EventHandler(this.button61_Click);
            this.button61.MouseEnter += new System.EventHandler(this.button61_MouseEnter);
            this.button61.MouseLeave += new System.EventHandler(this.button61_MouseLeave);
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label36.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label36, "label36");
            this.label36.Name = "label36";
            this.helpProvider1.SetShowHelp(this.label36, ((bool)(resources.GetObject("label36.ShowHelp"))));
            this.label36.Click += new System.EventHandler(this.button62_Click);
            this.label36.MouseEnter += new System.EventHandler(this.label36_MouseEnter);
            this.label36.MouseLeave += new System.EventHandler(this.label36_MouseLeave);
            // 
            // button62
            // 
            this.button62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button62, "button62");
            this.button62.FlatAppearance.BorderSize = 0;
            this.button62.ForeColor = System.Drawing.Color.Black;
            this.button62.Name = "button62";
            this.helpProvider1.SetShowHelp(this.button62, ((bool)(resources.GetObject("button62.ShowHelp"))));
            this.button62.UseVisualStyleBackColor = false;
            this.button62.Click += new System.EventHandler(this.button62_Click);
            this.button62.MouseEnter += new System.EventHandler(this.button62_MouseEnter);
            this.button62.MouseLeave += new System.EventHandler(this.button62_MouseLeave);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel10.Controls.Add(this.label34);
            this.panel10.Controls.Add(this.button60);
            this.panel10.Controls.Add(this.label9);
            this.panel10.Controls.Add(this.button30);
            this.panel10.Controls.Add(this.panel12);
            this.panel10.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel10, "panel10");
            this.panel10.Name = "panel10";
            this.helpProvider1.SetShowHelp(this.panel10, ((bool)(resources.GetObject("panel10.ShowHelp"))));
            this.panel10.TabStop = true;
            this.panel10.Leave += new System.EventHandler(this.panel10_Leave);
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label34.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label34, "label34");
            this.label34.Name = "label34";
            this.helpProvider1.SetShowHelp(this.label34, ((bool)(resources.GetObject("label34.ShowHelp"))));
            this.label34.Click += new System.EventHandler(this.button60_Click);
            this.label34.MouseEnter += new System.EventHandler(this.label34_MouseEnter);
            this.label34.MouseLeave += new System.EventHandler(this.label34_MouseLeave);
            // 
            // button60
            // 
            this.button60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button60, "button60");
            this.button60.FlatAppearance.BorderSize = 0;
            this.button60.ForeColor = System.Drawing.Color.Black;
            this.button60.Name = "button60";
            this.helpProvider1.SetShowHelp(this.button60, ((bool)(resources.GetObject("button60.ShowHelp"))));
            this.button60.UseVisualStyleBackColor = false;
            this.button60.Click += new System.EventHandler(this.button60_Click);
            this.button60.MouseEnter += new System.EventHandler(this.button60_MouseEnter);
            this.button60.MouseLeave += new System.EventHandler(this.button60_MouseLeave);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label9.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            this.helpProvider1.SetShowHelp(this.label9, ((bool)(resources.GetObject("label9.ShowHelp"))));
            this.label9.Click += new System.EventHandler(this.label9_Click);
            this.label9.MouseEnter += new System.EventHandler(this.label9_MouseEnter);
            this.label9.MouseLeave += new System.EventHandler(this.label9_MouseLeave);
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button30, "button30");
            this.button30.FlatAppearance.BorderSize = 0;
            this.button30.ForeColor = System.Drawing.Color.Black;
            this.button30.Name = "button30";
            this.helpProvider1.SetShowHelp(this.button30, ((bool)(resources.GetObject("button30.ShowHelp"))));
            this.button30.UseVisualStyleBackColor = false;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            this.button30.MouseEnter += new System.EventHandler(this.button30_MouseEnter);
            this.button30.MouseLeave += new System.EventHandler(this.button30_MouseLeave);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel12.Controls.Add(this.button57);
            this.panel12.Controls.Add(this.button53);
            this.panel12.Controls.Add(this.button52);
            this.panel12.Controls.Add(this.button50);
            this.panel12.Controls.Add(this.button51);
            this.panel12.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel12, "panel12");
            this.panel12.Name = "panel12";
            this.helpProvider1.SetShowHelp(this.panel12, ((bool)(resources.GetObject("panel12.ShowHelp"))));
            this.panel12.TabStop = true;
            this.panel12.Leave += new System.EventHandler(this.panel12_Leave);
            // 
            // button57
            // 
            this.button57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button57, "button57");
            this.button57.FlatAppearance.BorderSize = 0;
            this.button57.ForeColor = System.Drawing.Color.Black;
            this.button57.Name = "button57";
            this.helpProvider1.SetShowHelp(this.button57, ((bool)(resources.GetObject("button57.ShowHelp"))));
            this.button57.UseVisualStyleBackColor = false;
            this.button57.Click += new System.EventHandler(this.button57_Click_2);
            // 
            // button53
            // 
            this.button53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button53, "button53");
            this.button53.FlatAppearance.BorderSize = 0;
            this.button53.ForeColor = System.Drawing.Color.Black;
            this.button53.Name = "button53";
            this.helpProvider1.SetShowHelp(this.button53, ((bool)(resources.GetObject("button53.ShowHelp"))));
            this.button53.UseVisualStyleBackColor = false;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // button52
            // 
            this.button52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button52, "button52");
            this.button52.FlatAppearance.BorderSize = 0;
            this.button52.ForeColor = System.Drawing.Color.Black;
            this.button52.Name = "button52";
            this.helpProvider1.SetShowHelp(this.button52, ((bool)(resources.GetObject("button52.ShowHelp"))));
            this.button52.UseVisualStyleBackColor = false;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // button50
            // 
            this.button50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button50, "button50");
            this.button50.FlatAppearance.BorderSize = 0;
            this.button50.ForeColor = System.Drawing.Color.Black;
            this.button50.Name = "button50";
            this.helpProvider1.SetShowHelp(this.button50, ((bool)(resources.GetObject("button50.ShowHelp"))));
            this.button50.UseVisualStyleBackColor = false;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // button51
            // 
            this.button51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button51, "button51");
            this.button51.FlatAppearance.BorderSize = 0;
            this.button51.ForeColor = System.Drawing.Color.Black;
            this.button51.Name = "button51";
            this.helpProvider1.SetShowHelp(this.button51, ((bool)(resources.GetObject("button51.ShowHelp"))));
            this.button51.UseVisualStyleBackColor = false;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel9.Controls.Add(this.label33);
            this.panel9.Controls.Add(this.button59);
            this.panel9.Controls.Add(this.label7);
            this.panel9.Controls.Add(this.button28);
            this.panel9.Controls.Add(this.label6);
            this.panel9.Controls.Add(this.button4);
            this.panel9.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel9, "panel9");
            this.panel9.Name = "panel9";
            this.helpProvider1.SetShowHelp(this.panel9, ((bool)(resources.GetObject("panel9.ShowHelp"))));
            this.panel9.TabStop = true;
            this.panel9.Leave += new System.EventHandler(this.panel9_Leave);
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label33.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label33, "label33");
            this.label33.Name = "label33";
            this.helpProvider1.SetShowHelp(this.label33, ((bool)(resources.GetObject("label33.ShowHelp"))));
            this.label33.Click += new System.EventHandler(this.label33_Click);
            this.label33.MouseEnter += new System.EventHandler(this.label33_MouseEnter);
            this.label33.MouseLeave += new System.EventHandler(this.label33_MouseLeave);
            // 
            // button59
            // 
            this.button59.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button59, "button59");
            this.button59.FlatAppearance.BorderSize = 0;
            this.button59.ForeColor = System.Drawing.Color.Black;
            this.button59.Name = "button59";
            this.helpProvider1.SetShowHelp(this.button59, ((bool)(resources.GetObject("button59.ShowHelp"))));
            this.button59.UseVisualStyleBackColor = false;
            this.button59.Click += new System.EventHandler(this.label33_Click);
            this.button59.MouseEnter += new System.EventHandler(this.button59_MouseEnter);
            this.button59.MouseLeave += new System.EventHandler(this.button59_MouseLeave);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label7.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            this.helpProvider1.SetShowHelp(this.label7, ((bool)(resources.GetObject("label7.ShowHelp"))));
            this.label7.Click += new System.EventHandler(this.button28_Click);
            this.label7.MouseEnter += new System.EventHandler(this.label7_MouseEnter);
            this.label7.MouseLeave += new System.EventHandler(this.label7_MouseLeave);
            // 
            // button28
            // 
            this.button28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button28, "button28");
            this.button28.FlatAppearance.BorderSize = 0;
            this.button28.ForeColor = System.Drawing.Color.Black;
            this.button28.Name = "button28";
            this.helpProvider1.SetShowHelp(this.button28, ((bool)(resources.GetObject("button28.ShowHelp"))));
            this.button28.UseVisualStyleBackColor = false;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            this.button28.MouseEnter += new System.EventHandler(this.button28_MouseEnter);
            this.button28.MouseLeave += new System.EventHandler(this.button28_MouseLeave);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label6.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            this.helpProvider1.SetShowHelp(this.label6, ((bool)(resources.GetObject("label6.ShowHelp"))));
            this.label6.Click += new System.EventHandler(this.button4_Click);
            this.label6.MouseEnter += new System.EventHandler(this.label6_MouseEnter);
            this.label6.MouseLeave += new System.EventHandler(this.label6_MouseLeave);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.button4, "button4");
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Name = "button4";
            this.helpProvider1.SetShowHelp(this.button4, ((bool)(resources.GetObject("button4.ShowHelp"))));
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            this.button4.MouseEnter += new System.EventHandler(this.button4_MouseEnter);
            this.button4.MouseLeave += new System.EventHandler(this.button4_MouseLeave);
            // 
            // statusBar1
            // 
            this.statusBar1.Appearance = this.appearanceControl1;
            resources.ApplyResources(this.statusBar1, "statusBar1");
            this.statusBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.statusBar1.ContextMenuStrip = this.contextMenuStrip6;
            this.statusBar1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonItem1,
            this.buttonItem2,
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel5,
            this.buttonItem5,
            this.buttonItem6,
            this.progressBarItem1,
            this.buttonItem4,
            this.toolStripStatusLabel3,
            this.toolStripTrackBar1,
            this.toolStripStatusLabel2});
            this.statusBar1.Name = "statusBar1";
            this.helpProvider1.SetShowHelp(this.statusBar1, ((bool)(resources.GetObject("statusBar1.ShowHelp"))));
            this.statusBar1.SizingGrip = false;
            // 
            // appearanceControl1
            // 
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intBackground = -16273;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intBorderHighlight = -13410648;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientBegin = -8294;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientEnd = -22964;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientMiddle = -15500;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intHighlight = -3878683;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intPressedBackground = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.CheckedAppearance.intSelectedBackground = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intBorder = -16777088;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intBorderHighlight = -13410648;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientBegin = -98242;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientEnd = -8294;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientMiddle = -20115;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.PressedAppearance.intHighlight = -6771246;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorder = -16777088;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorderHighlight = -16777088;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientBegin = -34;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientEnd = -13432;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientMiddle = -7764;
            this.appearanceControl1.CustomAppearance.ButtonAppearance.SelectedAppearance.intHighlight = -3878683;
            this.appearanceControl1.CustomAppearance.GripAppearance.intDark = -14204554;
            this.appearanceControl1.CustomAppearance.GripAppearance.intLight = -16777216;
            this.appearanceControl1.CustomAppearance.GripAppearance.Light = System.Drawing.Color.Black;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.intGradientBegin = -1839105;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.intGradientEnd = -8674080;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Normal.intGradientMiddle = -3415556;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientBegin = -3416586;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientEnd = -9266217;
            this.appearanceControl1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientMiddle = -6175239;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intBorder = -16777088;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intPressedGradientBegin = -1839105;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intPressedGradientEnd = -8674080;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intPressedGradientMiddle = -6175239;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intSelected = -4414;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intSelectedGradientBegin = -34;
            this.appearanceControl1.CustomAppearance.MenuItemAppearance.intSelectedGradientEnd = -13432;
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.intBorder = -16765546;
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.intGradientBegin = -6373643;
            this.appearanceControl1.CustomAppearance.MenuStripAppearance.intGradientEnd = -3876102;
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.intGradientBegin = -8408582;
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.intGradientEnd = -16763503;
            this.appearanceControl1.CustomAppearance.OverflowButtonAppearance.intGradientMiddle = -11370544;
            this.appearanceControl1.CustomAppearance.RaftingContainerAppearance.intGradientBegin = -6373643;
            this.appearanceControl1.CustomAppearance.RaftingContainerAppearance.intGradientEnd = -3876102;
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.intDark = -16777216;
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.intLight = -16777216;
            this.appearanceControl1.CustomAppearance.SeparatorAppearance.Light = System.Drawing.Color.Black;
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.intGradientBegin = -16777216;
            this.appearanceControl1.CustomAppearance.StatusStripAppearance.intGradientEnd = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intBorder = -16777216;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intContentPanelGradientBegin = -6373643;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intContentPanelGradientEnd = -3876102;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intDropDownBackground = -592138;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intGradientBegin = -1839105;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intGradientEnd = -8674080;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intGradientMiddle = -3415556;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intPanelGradientBegin = -6373643;
            this.appearanceControl1.CustomAppearance.ToolStripAppearance.intPanelGradientEnd = -3876102;
            this.appearanceControl1.Preset = AppearanceControl.enumPresetStyles.Custom;
            this.appearanceControl1.Renderer.RoundedEdges = true;
            // 
            // buttonItem1
            // 
            resources.ApplyResources(this.buttonItem1, "buttonItem1");
            this.buttonItem1.ForeColor = System.Drawing.Color.White;
            this.buttonItem1.Name = "buttonItem1";
            // 
            // buttonItem2
            // 
            resources.ApplyResources(this.buttonItem2, "buttonItem2");
            this.buttonItem2.ForeColor = System.Drawing.Color.White;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Click += new System.EventHandler(this.buttonItem2_Click_1);
            // 
            // toolStripStatusLabel1
            // 
            resources.ApplyResources(this.toolStripStatusLabel1, "toolStripStatusLabel1");
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            resources.ApplyResources(this.toolStripStatusLabel5, "toolStripStatusLabel5");
            this.toolStripStatusLabel5.Spring = true;
            // 
            // buttonItem5
            // 
            resources.ApplyResources(this.buttonItem5, "buttonItem5");
            this.buttonItem5.ForeColor = System.Drawing.Color.White;
            this.buttonItem5.Name = "buttonItem5";
            // 
            // buttonItem6
            // 
            resources.ApplyResources(this.buttonItem6, "buttonItem6");
            this.buttonItem6.ForeColor = System.Drawing.Color.White;
            this.buttonItem6.Name = "buttonItem6";
            // 
            // progressBarItem1
            // 
            this.progressBarItem1.Name = "progressBarItem1";
            resources.ApplyResources(this.progressBarItem1, "progressBarItem1");
            this.progressBarItem1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // buttonItem4
            // 
            resources.ApplyResources(this.buttonItem4, "buttonItem4");
            this.buttonItem4.ForeColor = System.Drawing.Color.White;
            this.buttonItem4.Name = "buttonItem4";
            // 
            // toolStripStatusLabel3
            // 
            resources.ApplyResources(this.toolStripStatusLabel3, "toolStripStatusLabel3");
            this.toolStripStatusLabel3.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Click += new System.EventHandler(this.toolStripStatusLabel3_Click);
            // 
            // toolStripTrackBar1
            // 
            resources.ApplyResources(this.toolStripTrackBar1, "toolStripTrackBar1");
            this.toolStripTrackBar1.Name = "toolStripTrackBar1";
            this.toolStripTrackBar1.Value = 0;
            this.toolStripTrackBar1.Scroll += new System.EventHandler(this.toolStripTrackBar1_Scroll);
            // 
            // toolStripStatusLabel2
            // 
            resources.ApplyResources(this.toolStripStatusLabel2, "toolStripStatusLabel2");
            this.toolStripStatusLabel2.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Click += new System.EventHandler(this.toolStripStatusLabel2_Click);
            // 
            // VS
            // 
            resources.ApplyResources(this.VS, "VS");
            this.VS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.VS.Cursor = System.Windows.Forms.Cursors.SizeNESW;
            this.VS.ForeColor = System.Drawing.Color.Black;
            this.VS.Name = "VS";
            this.helpProvider1.SetShowHelp(this.VS, ((bool)(resources.GetObject("VS.ShowHelp"))));
            this.VS.MouseDown += new System.Windows.Forms.MouseEventHandler(this.borderSW_MouseDown);
            this.VS.MouseMove += new System.Windows.Forms.MouseEventHandler(this.borderSW_MouseMove);
            this.VS.MouseUp += new System.Windows.Forms.MouseEventHandler(this.borderSW_MouseUp);
            // 
            // borderSE
            // 
            resources.ApplyResources(this.borderSE, "borderSE");
            this.borderSE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.borderSE.Cursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.borderSE.ForeColor = System.Drawing.Color.Black;
            this.borderSE.Name = "borderSE";
            this.helpProvider1.SetShowHelp(this.borderSE, ((bool)(resources.GetObject("borderSE.ShowHelp"))));
            this.borderSE.MouseDown += new System.Windows.Forms.MouseEventHandler(this.borderSE_MouseDown_1);
            this.borderSE.MouseMove += new System.Windows.Forms.MouseEventHandler(this.borderSE_MouseMove);
            this.borderSE.MouseUp += new System.Windows.Forms.MouseEventHandler(this.borderSE_MouseUp_1);
            // 
            // toolTip1
            // 
            this.toolTip1.ShowAlways = true;
            // 
            // backgroundWorker2
            // 
            this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);
            // 
            // timer2
            // 
            this.timer2.Interval = 150;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // FallDownTimer
            // 
            this.FallDownTimer.Interval = 1;
            this.FallDownTimer.Tick += new System.EventHandler(this.FallDownTimer_Tick);
            // 
            // RiseUpTimer
            // 
            this.RiseUpTimer.Interval = 1;
            this.RiseUpTimer.Tick += new System.EventHandler(this.RiseUpTimer_Tick);
            // 
            // UpdateChecker
            // 
            this.UpdateChecker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker3_DoWork);
            // 
            // reidentTimer
            // 
            this.reidentTimer.Enabled = true;
            this.reidentTimer.Interval = 1;
            this.reidentTimer.Tick += new System.EventHandler(this.reidentTimer_Tick);
            // 
            // rangeSelector
            // 
            this.rangeSelector.Tick += new System.EventHandler(this.rangeSelector_Tick);
            // 
            // ShowCodeboardTimer
            // 
            this.ShowCodeboardTimer.Interval = 1;
            this.ShowCodeboardTimer.Tick += new System.EventHandler(this.ShowCodeboardTimer_Tick);
            // 
            // saveFileDialog4
            // 
            resources.ApplyResources(this.saveFileDialog4, "saveFileDialog4");
            // 
            // backgroundWorker3
            // 
            this.backgroundWorker3.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker3_DoWork_1);
            // 
            // HideCompileStatus
            // 
            this.HideCompileStatus.Interval = 3000;
            this.HideCompileStatus.Tick += new System.EventHandler(this.HideCompileStatus_Tick);
            // 
            // runTimer
            // 
            this.runTimer.Interval = 1;
            this.runTimer.Tick += new System.EventHandler(this.runTimer_Tick);
            // 
            // backgroundWorker4
            // 
            this.backgroundWorker4.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker4_DoWork);
            // 
            // markerTimer
            // 
            this.markerTimer.Tick += new System.EventHandler(this.markerTimer_Tick);
            // 
            // backgroundWorker5
            // 
            this.backgroundWorker5.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker5_DoWork);
            // 
            // UnminimizedWindow
            // 
            this.UnminimizedWindow.Interval = 1;
            this.UnminimizedWindow.Tick += new System.EventHandler(this.UnminimizedWindow_Tick);
            // 
            // ResizeTimer
            // 
            this.ResizeTimer.Interval = 1;
            this.ResizeTimer.Tick += new System.EventHandler(this.ResizeTimer_Tick);
            // 
            // MinimizeWaitABit
            // 
            this.MinimizeWaitABit.Tick += new System.EventHandler(this.MinimizeWaitABit_Tick);
            // 
            // RedrawTimer
            // 
            this.RedrawTimer.Interval = 500;
            this.RedrawTimer.Tick += new System.EventHandler(this.RedrawTimer_Tick);
            // 
            // AutoCompile
            // 
            this.AutoCompile.Enabled = true;
            this.AutoCompile.Tick += new System.EventHandler(this.AutoCompile_Tick);
            // 
            // EnterTimer
            // 
            this.EnterTimer.Tick += new System.EventHandler(this.EnterTimer_Tick);
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            //this.CloseButtonVisible = false;
            this.Controls.Add(this.OldCommandsPanel);
            this.Controls.Add(this.WhatsThis);
            this.Controls.Add(this.BottomDown);
            this.Controls.Add(this.splitContainer3);
            this.Controls.Add(this.statusBar1);
            this.Controls.Add(this.VS);
            this.Controls.Add(this.borderSE);
            this.Controls.Add(this.imageDisplay);
            this.Controls.Add(this.button54);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.RestoreWhite);
            this.Controls.Add(this.RestoreBlack);
            this.Controls.Add(this.MinimizeBlack);
            this.Controls.Add(this.MaximizeBlack);
            this.Controls.Add(this.MinimizeWhite);
            this.Controls.Add(this.MaximizeWhite);
            this.Controls.Add(this.UserPanel);
            this.Controls.Add(this.docPanel);
            this.DockAreas = WeifenLuo.WinFormsUI.Docking.DockAreas.Document;
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.helpProvider1.SetShowHelp(this, ((bool)(resources.GetObject("$this.ShowHelp"))));
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.ResizeBegin += new System.EventHandler(this.Form1_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.Form1_ResizeEnd);
            this.LocationChanged += new System.EventHandler(this.Form1_LocationChanged);
            this.TextChanged += new System.EventHandler(this.Form1_TextChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseEnter += new System.EventHandler(this.Form1_MouseEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.Move += new System.EventHandler(this.Form1_Move);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pseudocodeEd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisualScrollBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisualSBPic)).EndInit();
            this.SchemaLogica.ResumeLayout(false);
            this.SchemaLogica.PerformLayout();
            this.contextMenuStrip6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SchemaLogicaCont)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.converterEd)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.contextMenuStrip5.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.docPanel.ResumeLayout(false);
            this.FindPanel.ResumeLayout(false);
            this.FindPanel.PerformLayout();
            this.RecoverPanel.ResumeLayout(false);
            this.RecoverPanel.PerformLayout();
            this.ModulePanel.ResumeLayout(false);
            this.ModulePanel.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.EXEPanel.ResumeLayout(false);
            this.EXEPanel.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.UserPanel.ResumeLayout(false);
            this.UserPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDisplay)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip3.ResumeLayout(false);
            this.HelpPanel.ResumeLayout(false);
            this.UserDropPanel.ResumeLayout(false);
            this.UserDropPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.contextMenuStrip4.ResumeLayout(false);
            this.WhatsThis.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.OldCommandsPanel.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.runPanel.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.statusBar1.ResumeLayout(false);
            this.statusBar1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //private DevComponents.DotNetBar.LabelItem labelItem1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button Minimize;
        private System.Windows.Forms.Button Maximize;
        private System.Windows.Forms.Button MinimizeWhite;
        private System.Windows.Forms.Button MaximizeWhite;
        private System.Windows.Forms.Button MinimizeBlack;
        private System.Windows.Forms.Button MaximizeBlack;
        private System.Windows.Forms.Button Restore;
        private System.Windows.Forms.Button RestoreBlack;
        private System.Windows.Forms.Button RestoreWhite;
        private System.Windows.Forms.Button CloseLayer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label4;
        private PowerLabel TitleMax;
        private System.Windows.Forms.ToolStripMenuItem restoreDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maximizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private TransparentPanel VS;
        private System.Windows.Forms.Timer saveTimer;
        private SelectablePanel panel9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private SelectablePanel panel10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Timer printTimer;
        private SelectablePanel panel11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Timer graduateTimer;
        private System.Windows.Forms.Panel docPanel;
        private System.Windows.Forms.Timer DocumentEditToolsTimer;
        private System.Windows.Forms.Panel FindPanel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblLine;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Timer DocumentEditToolsTimerClose;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Panel EXEPanel;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.TextBox EXELocation;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.TextBox EXEIcon;
        private System.Windows.Forms.TextBox EXEName;
        private System.Windows.Forms.Panel ModulePanel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel RecoverPanel;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Timer startupTimer;
        private System.Windows.Forms.Panel UserPanel;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox pictureBox2;
        private SelectablePanel UserDropPanel;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Timer userTimer;
        private System.Windows.Forms.PictureBox pictureBox4;
        public System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Label label31;
        private SelectablePanel panel12;
        private System.Windows.Forms.Timer optionsTimer;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog3;
        private System.Windows.Forms.PictureBox imageDisplay;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToErrorLineToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem lookForErrorOnlineToolStripMenuItem;
        private TransparentPanel borderSE;
        private System.Windows.Forms.Timer timer2;
        public System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Button button27;
        public System.Windows.Forms.Button button26;
        public System.Windows.Forms.Button button25;
        public System.Windows.Forms.Button button24;
        public System.Windows.Forms.Button button23;
        public System.Windows.Forms.Button button22;
        private System.Windows.Forms.Panel HelpPanel;
        private System.Windows.Forms.Timer FallDownTimer;
        private System.Windows.Forms.Timer RiseUpTimer;
        private System.ComponentModel.BackgroundWorker UpdateChecker;
        private System.Windows.Forms.Timer reidentTimer;
        private System.Windows.Forms.ToolStripMenuItem goToErrorLineToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem lookForErrorOnlineToolStripMenuItem1;
        private System.Windows.Forms.Timer rangeSelector;
        public System.Windows.Forms.Button button3;
        public System.Windows.Forms.Button button47;
        private BufferedPanel SchemaLogica;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.PictureBox SchemaLogicaCont;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.SaveFileDialog saveFileDialog4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Timer HideCompileStatus;
        private System.Windows.Forms.Button button63;
        private SelectablePanel runPanel;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Timer runTimer;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Timer markerTimer;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox EXEVersion;
        private System.Windows.Forms.TextBox EXECopyright;
        private System.Windows.Forms.TextBox EXEInternalName;
        private System.Windows.Forms.TextBox EXEProductName;
        private System.Windows.Forms.TextBox EXECompanyName;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label18;
       // private DevComponents.DotNetBar.Controls.Line line1;
       // private DevComponents.DotNetBar.Controls.Line line2;
        private System.Windows.Forms.Timer UnminimizedWindow;
        private System.Windows.Forms.Timer MinimizeWaitABit;
        private System.Windows.Forms.Timer RedrawTimer;
        private System.Windows.Forms.Button button69;
        public System.Windows.Forms.Timer ResizeTimer;
        private PowerLabel Title;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private SyncListBox listBox3;
        private SyncListBox listBox4;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem1;
        private System.Windows.Forms.Button button71;
        public DoubleBufferedScintilla pseudocodeEd;  //DoubleBuffered
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ToolStripStatusLabel buttonItem1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel buttonItem5;
        private System.Windows.Forms.ToolStripStatusLabel buttonItem6;
        private System.Windows.Forms.ToolStripProgressBar progressBarItem1;
        private System.Windows.Forms.ToolStripStatusLabel buttonItem4;
        private AppearanceControl appearanceControl1;
        public ScintillaNET.Scintilla converterEd;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        public System.Windows.Forms.Button button21;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button button5;
        public System.Windows.Forms.Button button8;
        public System.Windows.Forms.Button button7;
        public System.Windows.Forms.Button button11;
        public System.Windows.Forms.Button button10;
        public System.Windows.Forms.Button button12;
        public System.Windows.Forms.Button button15;
        public System.Windows.Forms.Button button14;
        public System.Windows.Forms.Button button13;
        public System.Windows.Forms.Button button17;
        public System.Windows.Forms.Button button18;
        public System.Windows.Forms.Button button19;
        public System.Windows.Forms.Button button16;
        public System.Windows.Forms.Button button20;
        public System.Windows.Forms.Button button28;
        public System.Windows.Forms.Button button4;
        public System.Windows.Forms.Button button30;
        public System.Windows.Forms.Button button29;
        public System.Windows.Forms.Button button32;
        public System.Windows.Forms.Button button31;
        public System.Windows.Forms.Button button33;
        public System.Windows.Forms.Button button35;
        public System.Windows.Forms.Button button34;
        public System.Windows.Forms.Button button49;
        public System.Windows.Forms.Button button53;
        public System.Windows.Forms.Button button52;
        public System.Windows.Forms.Button button50;
        public System.Windows.Forms.Button button51;
        public System.Windows.Forms.Button button57;
        public System.Windows.Forms.Button button60;
        public System.Windows.Forms.Button button61;
        public System.Windows.Forms.Button button62;
        public System.Windows.Forms.Button button59;
        public System.Windows.Forms.Button button55;
        public System.Windows.Forms.Button button56;
        public System.Windows.Forms.ToolStripMenuItem highlightLineInLogicSchemaToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem goToDefinitionToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem findAllReferencesToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem renameVariableToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem addremoveBreakpointOnThisLineToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem addremoveWatchOnThisVariableToolStripMenuItem;
        public System.Windows.Forms.ToolStripStatusLabel buttonItem2;
        public System.Windows.Forms.Button button66;
        public CustomTabControl tabControl3;
        public System.Windows.Forms.TextBox textBox6;
        public System.Windows.Forms.Button button65;
        private System.Windows.Forms.Timer AutoCompile;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        public System.Windows.Forms.ToolStripMenuItem addBreakpointsOnAllLinesToolStripMenuItem;
        public CustomizableStatusStrip statusBar1;
        public System.Windows.Forms.SplitContainer splitContainer3;
        public System.Windows.Forms.SplitContainer splitContainer2;
        public System.ComponentModel.BackgroundWorker backgroundWorker2;
        public System.ComponentModel.BackgroundWorker backgroundWorker3;
        public System.ComponentModel.BackgroundWorker backgroundWorker4;
        public System.ComponentModel.BackgroundWorker backgroundWorker5;
        public ScintillaNET.Scintilla VisualScrollBar;
        public System.Windows.Forms.SplitContainer splitContainer4;
        public System.Windows.Forms.Panel BottomDown;
        private System.Windows.Forms.PictureBox VisualSBPic;
        private System.Windows.Forms.ToolStripMenuItem ceEsteAceastaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.Panel WhatsThis;
        private System.Windows.Forms.WebBrowser webBrowser2;
        private System.Windows.Forms.ToolStripMenuItem whatsThisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem whatsThisToolStripMenuItem1;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button button72;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel OldCommandsPanel;
        public System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        public CustomizableContextMenuStrip contextMenuStrip2;
        public System.Windows.Forms.ContextMenuStrip contextMenuStrip3;
        public System.Windows.Forms.ContextMenuStrip contextMenuStrip4;
        public System.Windows.Forms.ContextMenuStrip contextMenuStrip5;
        public System.Windows.Forms.ContextMenuStrip contextMenuStrip6;
        private System.Windows.Forms.Button button73;
        public System.Windows.Forms.Timer ShowCodeboardTimer;
        public ToolStripTrackBar toolStripTrackBar1;
        public PseudocodeSyntaxHighlightDemo.SemiTransparentPanel semiTransparentPanel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Timer EnterTimer;

    }
}

