﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Pseudocode
{
    [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip | ToolStripItemDesignerAvailability.StatusStrip)]
    //Declare a class that inherits from ToolStripControlHost.
    public class ToolStripTrackBar : MyToolStripControlHost
    {
        // Call the base constructor passing in a TrackBar instance.
        public ToolStripTrackBar()
            : base(new TrackBar())
        {
            // Starting Values properties I want when the control is initialized
            TrackBarControl.TickFrequency = 1;
            this.TrackBarControl.Maximum = 20;
            this.TrackBarControl.Minimum = -20;
            this.TrackBarControl.TickStyle = TickStyle.None;
            this.TrackBarControl.AutoSize = false;
            this.TrackBarControl.Height = 20;
            this.AutoSize = false;
            this.Height = 20;
            //this.TrackBarControl.BackColor = Color.FromKnownColor(KnownColor.ControlDark);
        }

        public TrackBar TrackBarControl
        {
            get { return (TrackBar)Control; }
        }

        // Expose the TrackBar's Value without using property above
        public int Value
        {
            get { return TrackBarControl.Value; }
            set { TrackBarControl.Value = value; }
        }

        // Subscribe and unsubscribe the control events you wish to expose.
        protected override void OnSubscribeControlEvents(Control c)
        {
            // Call the base so the base events are connected.
            base.OnSubscribeControlEvents(c);
            // Cast the control to a Track control.
            TrackBar TrackBar = (TrackBar)c;
            // Add the event.
            TrackBar.Scroll += HandleScroll;
        }

        protected override void OnUnsubscribeControlEvents(Control c)
        {
            // Call the base method so the basic events are unsubscribed.
            base.OnUnsubscribeControlEvents(c);
            // Cast the control to a Track control.
            TrackBar TrackBar = (TrackBar)c;
            // Remove the event.
            TrackBar.Scroll -= HandleScroll;
        }

        // Declare the ValueChanged event.
        public event EventHandler Scroll;
        // Raise the DateChanged event.
        private void HandleScroll(object sender, EventArgs e)
        {
            if (Scroll != null)
            {
                Scroll(this, e);
            }
        }
    }
    public class MyToolStripControlHost : ToolStripControlHost
    {
        public MyToolStripControlHost()
            : base(new Control())
        {
        }
        public MyToolStripControlHost(Control c)
            : base(c)
        {
        }
    }
}