﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Web;
using System.Text.RegularExpressions;

namespace Pseudocode
{
    public partial class ShareCode : Form
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int X;
            public int Y;
            public int Width;
            public int Height;
        }
        string Code;

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        IntPtr hWnd;
        Form frm;
        DialogResult final;
        public ShareCode(Form frma, string mesaj)
        {
            InitializeComponent();
            frm = frma;
            Code = mesaj;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Yes;
            this.Close();
        }

        private void No_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.No;
            this.Close();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        FormWindowState state;
        private void MessageBoxI_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            hWnd = frm.Handle;

            this.Size = frm.Size;
            if (frm.WindowState == FormWindowState.Maximized) this.Width += 5;
            this.Location = new Point(frm.Location.X, frm.Location.Y);
            this.Owner = frm;
            state = frm.WindowState;
            if (frm.WindowState == FormWindowState.Maximized) this.WindowState = FormWindowState.Maximized;

            backgroundWorker1.RunWorkerAsync();
        }

        private void Yes_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Yes;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (a != 1) this.Opacity += 0.1;
            if (a == 1) this.Opacity -= 0.1;
            if (a == 1 & this.Opacity <= 0)
            {
                a = 2;
                this.Close();
            }
            RECT rect;
            GetWindowRect(hWnd, out rect);
            if (rect.X == -32000)
            {
                // the game is minimized
                this.WindowState = FormWindowState.Minimized;
            }
            else
            {
                bool ok = false;
                if (state == FormWindowState.Maximized) ok = true;
                this.WindowState = FormWindowState.Normal;
                if (ok == true) this.Location = new Point(rect.X + 5, rect.Y - 7);
                else this.Location = new Point(rect.X, rect.Y);

            }
        }

        private void MessageBoxI_Paint(object sender, PaintEventArgs e)
        {
            if (Environment.OSVersion.Version.Major <= 5)
            {

                this.TransparencyKey = this.BackColor;
            }
            else
            {

                var hb = new HatchBrush(HatchStyle.Percent50, this.TransparencyKey);

                if (this.WindowState == FormWindowState.Maximized) e.Graphics.FillRectangle(hb, new Rectangle(this.DisplayRectangle.X, this.DisplayRectangle.Y, this.DisplayRectangle.Width + 10, this.DisplayRectangle.Height));// this.DisplayRectangle);
                else e.Graphics.FillRectangle(hb, this.DisplayRectangle);
            }
        }

        private void MessageBoxI_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Yes;
            this.Close();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
        int a = 0;
        private void MessageBoxI_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (a == 2)
            {
               // this.Visible = false;
                DialogResult = final;
            }
            else
            {
                a = 1;
                e.Cancel = true;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
        }
        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string returnare = HttpPost("http://paste.ee/api", "key=public&paste=" + Code + "&format=simple&return=link");
                            DoOnUIThread(delegate()
                {
                    pictureBox2.Visible = false;
            if (returnare.StartsWith("http://paste.ee"))
            {
                OK.Text = "&OK";
                textBox1.Text = returnare.Replace("http://paste.ee/p/", "");
                label3.Visible = false;
                label1.Visible = true;
                textBox1.Visible = true;
                button1.Visible = true;
                //label2.Visible = true;
                //button3.Visible = true;
                //button2.Visible = true;
                //button4.Visible = true;
                OK.Visible = true;
                textBox1.SelectAll();
                textBox1.Focus();
            }
            else
            {
                if (Code == "")
                {
                    if (Properties.Settings.Default.UILang == 0) label3.Text = "An empty pseudocode file cannot be shared using a #code.";
                    if (Properties.Settings.Default.UILang == 1) label3.Text = "Un fișier pseudocod gol nu poate fi partajat folosind un #cod.";
                }
                else
                {
                    if (Properties.Settings.Default.UILang == 0) label3.Text = "This service is unavailable at the moment, please try again later.";
                    if (Properties.Settings.Default.UILang == 1) label3.Text = "Acest serviciu este indisponibil în acest moment, încercați mai târziu.";
                }
                    OK.Visible = true;
            }
                });
        }
        public static string HttpPost(string URI, string Parameters)
        {
            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
                //req.Proxy = new System.Net.WebProxy(ProxyString, true);
                //Add these, as we're doing a POST
                req.ContentType = "application/x-www-form-urlencoded";
                req.Method = "POST";
                //We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
                req.ContentLength = bytes.Length;
                System.IO.Stream os = req.GetRequestStream();
                os.Write(bytes, 0, bytes.Length); //Push it out there
                os.Close();
                System.Net.WebResponse resp = req.GetResponse();
                if (resp == null) return null;
                System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
            catch
            {
                return "UNREACHABLE";
            }
        }

        private void OK_Click_1(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Yes;
            this.Close();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox1.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.facebook.com/sharer/sharer.php?u=" + HttpUtility.UrlEncode("http://www.valinet.ro/dl") + "&t=" + HttpUtility.UrlEncode("Check out this cool pseudocode - use the following #code to view it: #" + textBox1.Text));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.twitter.com/share?url=http%3A%2F%2Fpaste.ee%2Fr%2F" + textBox1.Text);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UILang == 0) Process.Start("mailto:?to=&subject=Check out this cool pseudcode!&body=Hi! Check out this cool pseudocode - use the following #code to view it: #" + textBox1.Text + "%0D%0A%0D%0ADon't how to use this code? It's simple: download the Pseudocode application and start being a programmer today: http://www.valinet.ro/");

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process.Start("https://plus.google.com/share?url=http%3A%2F%2Fpaste.ee%2Fr%2F" + textBox1.Text);

        }
    }
}


