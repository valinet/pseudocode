﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace Pseudocode
{
    public partial class UseCode : Form
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int X;
            public int Y;
            public int Width;
            public int Height;
        }
        string Code;

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        IntPtr hWnd;
        Form frm;
        DialogResult final;
        public UseCode(Form frma)
        {
            InitializeComponent();
            frm = frma;
            //Code = mesaj;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Yes;
            this.Close();
        }

        private void No_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.No;
            this.Close();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        FormWindowState state;
        private void MessageBoxI_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            hWnd = frm.Handle;

            this.Size = frm.Size;
            if (frm.WindowState == FormWindowState.Maximized) this.Width += 5;
            this.Location = new Point(frm.Location.X, frm.Location.Y);
            this.Owner = frm;
            state = frm.WindowState;
            if (frm.WindowState == FormWindowState.Maximized) this.WindowState = FormWindowState.Maximized;
            textBox1.Text = "";
            textBox1.Focus();
            textBox1.Select(0, 0);
            //backgroundWorker1.RunWorkerAsync();
        }

        private void Yes_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Yes;
            this.Close();
        }
        bool wasFocusSet = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (a != 1) this.Opacity += 0.1;
            if (a == 1) this.Opacity -= 0.1;
            if (a == 1 & this.Opacity <= 0)
            {
                a = 2;
                this.Close();
            }
            RECT rect;
            GetWindowRect(hWnd, out rect);
            if (rect.X == -32000)
            {
                // the game is minimized
                this.WindowState = FormWindowState.Minimized;
            }
            else
            {
                bool ok = false;
                if (state == FormWindowState.Maximized) ok = true;
                this.WindowState = FormWindowState.Normal;
                if (ok == true) this.Location = new Point(rect.X + 5, rect.Y - 7);
                else this.Location = new Point(rect.X, rect.Y);

            }
            if (wasFocusSet == false)
            {
                textBox1.Focus();
                wasFocusSet = true;
            }
            //textBox1.Focus();
        }

        private void MessageBoxI_Paint(object sender, PaintEventArgs e)
        {
            if (Environment.OSVersion.Version.Major <= 5)
            {

                this.TransparencyKey = this.BackColor;
            }
            else
            {

                var hb = new HatchBrush(HatchStyle.Percent50, this.TransparencyKey);

                if (this.WindowState == FormWindowState.Maximized) e.Graphics.FillRectangle(hb, new Rectangle(this.DisplayRectangle.X, this.DisplayRectangle.Y, this.DisplayRectangle.Width + 10, this.DisplayRectangle.Height));// this.DisplayRectangle);
                else e.Graphics.FillRectangle(hb, this.DisplayRectangle);
            }
        }

        private void MessageBoxI_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.No;
            this.Close();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            pictureBox2.Visible = true;
            if (Properties.Settings.Default.UILang == 0) Titlu.Text = "Downloading the requested file, please wait...";
            if (Properties.Settings.Default.UILang == 1) Titlu.Text = "Se descarcă pseudocodul solicitat, așteptați...";
            label1.Visible = false;
            textBox1.Visible = false;
            OK.Visible = false;
            button5.Visible = false;
            backgroundWorker1.RunWorkerAsync();
        }
        int a = 0;
        private void MessageBoxI_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (a == 2)
            {
                //this.Visible = false;
                DialogResult = final;
            }
            else
            {
                a = 1;
                e.Cancel = true;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
        }
        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                var webClient = new WebClient();
                string readHtml = HttpUtility.UrlDecode(webClient.DownloadString("http://paste.ee/r/" + textBox1.Text));
                readHtml = Regex.Replace(readHtml, "#ENCODED_PLUS_SIGN#", "+");
                DoOnUIThread(delegate()
{
    this.Tag = readHtml;
    pictureBox2.Visible = false;
    if (readHtml == "paste not found") Properties.Settings.Default.LastCode = "paste not found";
    else Properties.Settings.Default.LastCode = textBox1.Text;
    final = System.Windows.Forms.DialogResult.Yes;
    this.Close();
});
            }
            catch (Exception ex)
            {
                this.Tag = ex.Message;
                final = System.Windows.Forms.DialogResult.Cancel;
                                DoOnUIThread(delegate()
{
                this.Close();
});
            }
            /*finally
            {
                final = System.Windows.Forms.DialogResult.Yes;
                this.Close();
            }*/
            /*//string returnare = HttpPost("http://paste.ee/api", "key=public&paste=" + Code + "&format=simple&return=link");
            DoOnUIThread(delegate()
            {
                pictureBox2.Visible = true;
                if (returnare.StartsWith("http://paste.ee"))
                {
                    textBox1.Text = returnare.Replace("http://paste.ee/r/", "");
                    //label3.Visible = false;
                    label1.Visible = true;
                    textBox1.Visible = true;
                    //button1.Visible = true;
                    //label2.Visible = true;
                    //button3.Visible = true;
                    //button2.Visible = true;
                    //button4.Visible = true;
                    OK.Visible = true;
                    textBox1.SelectAll();
                    textBox1.Focus();
                }
                else
                {
                    //if (Properties.Settings.Default.UILang == 0) label3.Text = "This service is unavailable at the moment, please try again later.";
                    //if (Properties.Settings.Default.UILang == 1) label3.Text = "Acest serviciu este indisponibil în acest moment, încercați mai târziu.";
                    OK.Visible = true;
                }
            });*/
        }
        public static string HttpPost(string URI, string Parameters)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
            //req.Proxy = new System.Net.WebProxy(ProxyString, true);
            //Add these, as we're doing a POST
            req.ContentType = "application/x-www-form-urlencoded";
            req.Method = "POST";
            //We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length); //Push it out there
            os.Close();
            System.Net.WebResponse resp = req.GetResponse();
            if (resp == null) return null;
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }



        private void button1_Click_2(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox1.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("da");
            final = System.Windows.Forms.DialogResult.No;
            this.Close();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            
        }
    }
}


