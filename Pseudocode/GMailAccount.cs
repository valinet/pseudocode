﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace Pseudocode
{
    public class GmailAccount
    {
        public string Username;
        public string Password;
        public string DisplayName;

        public string Address
        {
            get
            {
                return Username + "@gmail.com";
            }
        }

        private SmtpClient client;

        public GmailAccount(string username, string password, string displayName = null)
        {
            Username = username;
            Password = password;
            DisplayName = displayName;

            client = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(Address, password)
            };
        }

        public void SendMessage(string targetAddress, string subject, string body, params string[] files)
        {
            MailMessage message = new MailMessage(new MailAddress(Address, DisplayName), new MailAddress(targetAddress))
            {
                Subject = subject,
                Body = body
            };

            if (files[0] != "c")
            {
                foreach (string file in files)
                {
                    Attachment attachment = new Attachment(file);
                    message.Attachments.Add(attachment);
                }
            }
            client.Send(message);
        }
    }

}
