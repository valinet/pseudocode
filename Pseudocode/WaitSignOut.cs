﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class WaitSignOut : Form
    {
        MainForm form1;
        bool wh;
        public WaitSignOut(MainForm frm, bool what)
        {
            InitializeComponent();
            form1 = frm;
            wh = what;
            string CloseText = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                CloseText = "Close (Alt+F4)";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                CloseText = "Închidere (Alt+F4)";
            }
            //toolTip1.SetToolTip(Close, CloseText);
        }

        private void webBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            //MessageBox.Show(e.Url.ToString());
            if (e.Url.ToString() == "https://www.dropbox.com/login")
            {
               // Sign sg = new Sign(form1, wh);
               // sg.ShowDialog();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
                public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void WaitSignOut_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }
        private void WaitSignOut_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.Visible = false;
        }

        private void Close_Click(object sender, EventArgs e)
        {
            button1.PerformClick();
        }

        private void WaitSignOut_Load(object sender, EventArgs e)
        {
           // Close.BackColor = Color.FromArgb(199, 80, 80);
        }
       /* private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }*/
    }
}
