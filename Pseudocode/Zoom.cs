﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class Zoom : Form
    {
        MainForm mf;
        public Zoom(MainForm main)
        {
            InitializeComponent();
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            mf = main;
        }

        private void Zoom_Load(object sender, EventArgs e)
        {

        }

        public void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                int x = trackBar1.Value;
                Properties.Settings.Default.Zoom = x;
                foreach (Form frmx in mf.MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                        if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                        frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                        frm.pseudocodeEd.ZoomFactor = x;
                        frm.converterEd.ZoomFactor = x;
                    }
                mf.zoom25.Checked = false;
                mf.zoom50.Checked = false;
                mf.zoom75.Checked = false;
                mf.zoom100.Checked = false;
                mf.zoom200.Checked = false;
                mf.zoom300.Checked = false;
                mf.zoom400.Checked = false;
                mf.customizeToolStripMenuItem.Checked = true;
                if (x == -3)
                {
                    mf.zoom25.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == -2)
                {
                    mf.zoom50.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == -1)
                {
                    mf.zoom75.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == 0)
                {
                    mf.zoom100.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == 1)
                {
                    mf.zoom200.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == 2)
                {
                    mf.zoom300.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
                if (x == 3)
                {
                    mf.zoom400.Checked = true;
                    mf.customizeToolStripMenuItem.Checked = false;
                }
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int x = 0;
                Properties.Settings.Default.Zoom = x;
                foreach (Form frmx in mf.MdiChildren)
                    if (frmx is Form1)
                    {
                        Form1 frm = (Form1)frmx;
                        frm.toolStripTrackBar1.Scroll -= frm.toolStripTrackBar1_Scroll;
                    if (Properties.Settings.Default.Zoom >= 0) frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom; else frm.toolStripTrackBar1.Value = Properties.Settings.Default.Zoom * 2;
                    frm.toolStripTrackBar1.Scroll += frm.toolStripTrackBar1_Scroll;
                    frm.pseudocodeEd.ZoomFactor = x;
                    frm.converterEd.ZoomFactor = x;

                }
                mf.zoom25.Checked = false;
                mf.zoom50.Checked = false;
                mf.zoom75.Checked = false;
                mf.zoom100.Checked = true;
                mf.zoom200.Checked = false;
                mf.zoom300.Checked = false;
                mf.zoom400.Checked = false;
                mf.customizeToolStripMenuItem.Checked = false;
                trackBar1.ValueChanged -= trackBar1_ValueChanged;
                trackBar1.Value = 0;
                trackBar1.ValueChanged += trackBar1_ValueChanged;
            }
            catch { }
        }

        private void Zoom_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (Form frmx in mf.MdiChildren)
                if (frmx is Form1)
                {
                    Form1 frmt = (Form1)frmx;
                    frmt.pseudocodeEd.ZoomFactorChanged += frmt.pseudocodeEd_ZoomFactorChanged;
                }
            e.Cancel = true;
            this.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
