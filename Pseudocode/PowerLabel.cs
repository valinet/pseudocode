﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class PowerLabel : Label
    {
        public PowerLabel()
        {
            this.SetStyle(ControlStyles.UserPaint, true); //Call in constructor, Use UserPaint
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (!Enabled)
            {
                Color c = new Color();
                if (Properties.Settings.Default.UITheme == 1) c = Color.Black;
                else c = Color.Gray;
                using (SolidBrush drawBrush = new SolidBrush(c))
                {//Choose colour
                    e.Graphics.DrawString(Text, Font, drawBrush, Width / 2 - e.Graphics.MeasureString(Text, Font).Width / 2, 0f); //Dra whatever text was on the label
                }
                
            }
            else
            {
                base.OnPaint(e); //Default Forecolours
            }
        }
    }
}
