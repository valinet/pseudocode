﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using ValiNet.PseudocodePluginsFramework;

namespace Pseudocode
{
    public partial class ExtensionManager : Form
    {
        Dictionary<string, VPlugin> _Plugins;
        Form1 form1;
        string Erase;
        string Error;
        public ExtensionManager()
        {
            InitializeComponent();
            if (Properties.Settings.Default.UILang == 0)
            {
                Erase = "Are you sure you want to uninstall this add-on?\n\nThe application will restart to complete changes.";
                Error = "An error has occured.\n\nThe add-on was not uninstalled successfully.";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                Erase = "Sigur ștergeți următoarea extensie?\n\nAplicația va reporni pentru a aplica modificările.";
                Error = "A avut loc o eroare.\n\nExtensia nu a fost dezinstalată cu succes.";
            }
            string CloseText = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                CloseText = "Close (Alt+F4)";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                CloseText = "Închidere (Alt+F4)";
            }
         //   toolTip1.SetToolTip(Close, CloseText);
            //load plugins
            _Plugins = new Dictionary<string, VPlugin>();
            //ICollection<IPlugin> plugins = PluginLoader.LoadPlugins("Plugins");
            ICollection<VPlugin> plugins = GenericPluginLoader<VPlugin>.LoadPlugins(Application.StartupPath);
            int i = 0;
            Button[] b = new Button[100];
            foreach (var item in plugins)
            {
                _Plugins.Add(item.Name, item);
                b[i] = new Button();
                b[i].Text = item.Name;
                b[i].Click += b_Click;
                b[i].Image = button2.Image;
                b[i].ImageAlign = ContentAlignment.MiddleRight;
                b[i].Size = button2.Size;
                try
                {
                    b[i].Location = new Point(b[i - 1].Location.X, b[i - 1].Location.Y + b[i - 1].Height);
                }
                catch
                {
                    b[i].Location = button2.Location;
                }
                b[i].FlatStyle = FlatStyle.Flat;
                b[i].FlatAppearance.BorderSize = 0;
                b[i].FlatAppearance.MouseOverBackColor = Color.FromArgb(224, 224, 224);
                b[i].FlatAppearance.MouseDownBackColor = Color.Silver;
                b[i].Font = button2.Font;
                b[i].TextAlign = ContentAlignment.MiddleLeft;
                b[i].Tag = Application.StartupPath + "\\plugins\\" + item.AssemblyName + ".dll";
                b[i].Anchor = button2.Anchor;
                b[i].AutoEllipsis = true;
                b[i].Show();
                b[i].BringToFront();
                panel2.Controls.Add(b[i]);
            }
            //end load plugins
            //form1 = frm;
        }

        private void ExtensionManager_Load(object sender, EventArgs e)
        {
            button4.BackColor = Color.FromArgb(255, 192, 255);
            panel2.BackColor = Color.White;
            panel1.BackColor = Color.White;
            button22.BackColor = Color.FromArgb(224, 224, 224);

           // Close.BackColor = Color.FromArgb(199, 80, 80);
           // if (Properties.Settings.Default.UILang == 0) toolTip1.SetToolTip(Close, "Close (Alt+F4)");
         //   if (Properties.Settings.Default.UILang == 1) toolTip1.SetToolTip(Close, "Închidere (Alt+F4)");

        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }
        private void b_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (b != null)
            {
                string key = b.Text.ToString();
                if (_Plugins.ContainsKey(key))
                {
                    if (button22.BackColor == Color.FromArgb(224, 224, 224))
                    {
                        VPlugin plugin = _Plugins[key];
                        plugin.ExtensionOptions();
                    }
                    if (button1.BackColor == Color.FromArgb(224, 224, 224))
                    {
                        VPlugin plugin = _Plugins[key];
                        plugin.About();
                    }
                    if (button3.BackColor == Color.FromArgb(224, 224, 224))
                    {
                        DialogResult dr = CasetaDeMesaj(form1, Erase, "", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (dr == System.Windows.Forms.DialogResult.Yes)
                        {
                            Properties.Settings.Default.AddonToBeErased = b.Tag.ToString();
                            Properties.Settings.Default.Save();
                            System.Diagnostics.Process.Start(Application.ExecutablePath);
                            System.Diagnostics.Process.GetCurrentProcess().Kill();
                            //try
                            //{
                                //System.IO.File.Delete(b.Tag.ToString());
                            //    System.Diagnostics.Process.Start(Application.ExecutablePath);
                            //    Process.GetCurrentProcess().Kill();
                            //}
                            //catch
                            //{
                            //    CasetaDeMesaj(form1, Error, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //}
                        }   
                    }
                }
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            button22.BackColor = Color.FromArgb(224, 224, 224);
            button1.BackColor = Color.White;
            button3.BackColor = Color.White;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.BackColor = Color.FromArgb(224, 224, 224);
            button22.BackColor = Color.White;
            button3.BackColor = Color.White;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button3.BackColor = Color.FromArgb(224, 224, 224);
            button1.BackColor = Color.White;
            button22.BackColor = Color.White;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void ExtensionManager_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }
        */
        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }
    }
}
