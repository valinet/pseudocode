﻿namespace Pseudocode
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Options));
            this.Application0Startup = new System.Windows.Forms.Panel();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.Application0Interface = new System.Windows.Forms.Panel();
            this.button17 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.Application0Sync = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Application0FileAssociations = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.Application0ExtensionManager = new System.Windows.Forms.Panel();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Application0Codeboard = new System.Windows.Forms.Panel();
            this.Application0ImportExportSettings = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.Editor0AutoSave = new System.Windows.Forms.Panel();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.Editor0VisualExperience = new System.Windows.Forms.Panel();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Editor0Findreplace = new System.Windows.Forms.Panel();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.CodeConversion0Headers = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CodeConversion0LogicSchema = new System.Windows.Forms.Panel();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.Debugging = new System.Windows.Forms.Panel();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.linkLabel6 = new System.Windows.Forms.LinkLabel();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.button12 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.button15 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.ErrorReporting = new System.Windows.Forms.Panel();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.AutomaticUpdates = new System.Windows.Forms.Panel();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.button13 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.ResetSettings = new System.Windows.Forms.Panel();
            this.button14 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.listBox1 = new Controls.Development.ImageListBox();
            this.imageListBoxItem1 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem2 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem4 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem5 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem6 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem7 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem8 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem9 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem10 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem11 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem12 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem17 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem18 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem19 = new Controls.Development.ImageListBoxItem();
            this.imageListBoxItem3 = new Controls.Development.ImageListBoxItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button16 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.Application0Startup.SuspendLayout();
            this.Application0Interface.SuspendLayout();
            this.Application0Sync.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.Application0FileAssociations.SuspendLayout();
            this.Application0ExtensionManager.SuspendLayout();
            this.Application0ImportExportSettings.SuspendLayout();
            this.Editor0AutoSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.Editor0VisualExperience.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Editor0Findreplace.SuspendLayout();
            this.CodeConversion0Headers.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.CodeConversion0LogicSchema.SuspendLayout();
            this.Debugging.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.ErrorReporting.SuspendLayout();
            this.AutomaticUpdates.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.ResetSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // Application0Startup
            // 
            resources.ApplyResources(this.Application0Startup, "Application0Startup");
            this.Application0Startup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Application0Startup.Controls.Add(this.checkBox3);
            this.Application0Startup.Controls.Add(this.checkBox2);
            this.Application0Startup.Controls.Add(this.checkBox1);
            this.Application0Startup.Name = "Application0Startup";
            this.Application0Startup.Tag = "Aplicație → Pornire";
            // 
            // checkBox3
            // 
            resources.ApplyResources(this.checkBox3, "checkBox3");
            this.checkBox3.ForeColor = System.Drawing.Color.White;
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            resources.ApplyResources(this.checkBox2, "checkBox2");
            this.checkBox2.ForeColor = System.Drawing.Color.White;
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            resources.ApplyResources(this.checkBox1, "checkBox1");
            this.checkBox1.ForeColor = System.Drawing.Color.White;
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.BackColor = System.Drawing.SystemColors.Control;
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // SearchBox
            // 
            resources.ApplyResources(this.SearchBox, "SearchBox");
            this.SearchBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.SearchBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SearchBox.ForeColor = System.Drawing.Color.White;
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.TextChanged += new System.EventHandler(this.SearchBox_TextChanged);
            this.SearchBox.Enter += new System.EventHandler(this.SearchBox_Enter);
            this.SearchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SearchBox_KeyDown);
            this.SearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SearchBox_KeyUp);
            this.SearchBox.Leave += new System.EventHandler(this.SearchBox_Leave);
            // 
            // Application0Interface
            // 
            resources.ApplyResources(this.Application0Interface, "Application0Interface");
            this.Application0Interface.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Application0Interface.Controls.Add(this.button17);
            this.Application0Interface.Controls.Add(this.label20);
            this.Application0Interface.Controls.Add(this.button4);
            this.Application0Interface.Controls.Add(this.button3);
            this.Application0Interface.Controls.Add(this.checkBox12);
            this.Application0Interface.Controls.Add(this.checkBox13);
            this.Application0Interface.Name = "Application0Interface";
            this.Application0Interface.Tag = "Aplicație → Sincronizare cloud";
            // 
            // button17
            // 
            resources.ApplyResources(this.button17, "button17");
            this.button17.ForeColor = System.Drawing.Color.Black;
            this.button17.Name = "button17";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Name = "label20";
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            resources.ApplyResources(this.checkBox12, "checkBox12");
            this.checkBox12.ForeColor = System.Drawing.Color.White;
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            resources.ApplyResources(this.checkBox13, "checkBox13");
            this.checkBox13.ForeColor = System.Drawing.Color.White;
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // Application0Sync
            // 
            resources.ApplyResources(this.Application0Sync, "Application0Sync");
            this.Application0Sync.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Application0Sync.Controls.Add(this.groupBox3);
            this.Application0Sync.Controls.Add(this.groupBox4);
            this.Application0Sync.Name = "Application0Sync";
            this.Application0Sync.Tag = "Aplicație → Sincronizare Dropbox";
            // 
            // groupBox3
            // 
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Controls.Add(this.checkBox23);
            this.groupBox3.Controls.Add(this.comboBox4);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.checkBox22);
            this.groupBox3.Controls.Add(this.checkBox25);
            this.groupBox3.Controls.Add(this.checkBox11);
            this.groupBox3.Controls.Add(this.checkBox8);
            this.groupBox3.Controls.Add(this.checkBox9);
            this.groupBox3.Controls.Add(this.checkBox10);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // checkBox23
            // 
            resources.ApplyResources(this.checkBox23, "checkBox23");
            this.checkBox23.ForeColor = System.Drawing.Color.White;
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // comboBox4
            // 
            resources.ApplyResources(this.comboBox4, "comboBox4");
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            resources.GetString("comboBox4.Items"),
            resources.GetString("comboBox4.Items1")});
            this.comboBox4.Name = "comboBox4";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // checkBox22
            // 
            resources.ApplyResources(this.checkBox22, "checkBox22");
            this.checkBox22.ForeColor = System.Drawing.Color.White;
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.UseVisualStyleBackColor = true;
            // 
            // checkBox25
            // 
            resources.ApplyResources(this.checkBox25, "checkBox25");
            this.checkBox25.ForeColor = System.Drawing.Color.White;
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            resources.ApplyResources(this.checkBox11, "checkBox11");
            this.checkBox11.ForeColor = System.Drawing.Color.White;
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            resources.ApplyResources(this.checkBox8, "checkBox8");
            this.checkBox8.ForeColor = System.Drawing.Color.White;
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            resources.ApplyResources(this.checkBox9, "checkBox9");
            this.checkBox9.ForeColor = System.Drawing.Color.White;
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            resources.ApplyResources(this.checkBox10, "checkBox10");
            this.checkBox10.ForeColor = System.Drawing.Color.White;
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Controls.Add(this.comboBox3);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.comboBox2);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // comboBox3
            // 
            resources.ApplyResources(this.comboBox3, "comboBox3");
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            resources.GetString("comboBox3.Items"),
            resources.GetString("comboBox3.Items1")});
            this.comboBox3.Name = "comboBox3";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // comboBox2
            // 
            resources.ApplyResources(this.comboBox2, "comboBox2");
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            resources.GetString("comboBox2.Items"),
            resources.GetString("comboBox2.Items1")});
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // Application0FileAssociations
            // 
            resources.ApplyResources(this.Application0FileAssociations, "Application0FileAssociations");
            this.Application0FileAssociations.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Application0FileAssociations.Controls.Add(this.button7);
            this.Application0FileAssociations.Controls.Add(this.label3);
            this.Application0FileAssociations.Controls.Add(this.label1);
            this.Application0FileAssociations.Controls.Add(this.button6);
            this.Application0FileAssociations.Name = "Application0FileAssociations";
            this.Application0FileAssociations.Tag = "Aplicație → Asocieri fișiere";
            // 
            // button7
            // 
            resources.ApplyResources(this.button7, "button7");
            this.button7.ForeColor = System.Drawing.Color.Black;
            this.button7.Name = "button7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Name = "label3";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Name = "label1";
            // 
            // button6
            // 
            resources.ApplyResources(this.button6, "button6");
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.Name = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Application0ExtensionManager
            // 
            resources.ApplyResources(this.Application0ExtensionManager, "Application0ExtensionManager");
            this.Application0ExtensionManager.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Application0ExtensionManager.Controls.Add(this.button9);
            this.Application0ExtensionManager.Controls.Add(this.button8);
            this.Application0ExtensionManager.Controls.Add(this.button5);
            this.Application0ExtensionManager.Controls.Add(this.label4);
            this.Application0ExtensionManager.Name = "Application0ExtensionManager";
            this.Application0ExtensionManager.Tag = "Aplicație → Extensii și Actualizări";
            // 
            // button9
            // 
            resources.ApplyResources(this.button9, "button9");
            this.button9.Name = "button9";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            resources.ApplyResources(this.button8, "button8");
            this.button8.Name = "button8";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            resources.ApplyResources(this.button5, "button5");
            this.button5.Name = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Name = "label4";
            // 
            // Application0Codeboard
            // 
            resources.ApplyResources(this.Application0Codeboard, "Application0Codeboard");
            this.Application0Codeboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Application0Codeboard.Name = "Application0Codeboard";
            this.Application0Codeboard.Tag = "Aplicație → Sincronizare cu cloud";
            // 
            // Application0ImportExportSettings
            // 
            resources.ApplyResources(this.Application0ImportExportSettings, "Application0ImportExportSettings");
            this.Application0ImportExportSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Application0ImportExportSettings.Controls.Add(this.button11);
            this.Application0ImportExportSettings.Controls.Add(this.button10);
            this.Application0ImportExportSettings.Controls.Add(this.label5);
            this.Application0ImportExportSettings.Name = "Application0ImportExportSettings";
            this.Application0ImportExportSettings.Tag = "Aplicație → Import/export setări";
            // 
            // button11
            // 
            resources.ApplyResources(this.button11, "button11");
            this.button11.Name = "button11";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            resources.ApplyResources(this.button10, "button10");
            this.button10.Name = "button10";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Name = "label5";
            // 
            // Editor0AutoSave
            // 
            resources.ApplyResources(this.Editor0AutoSave, "Editor0AutoSave");
            this.Editor0AutoSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Editor0AutoSave.Controls.Add(this.numericUpDown1);
            this.Editor0AutoSave.Controls.Add(this.label6);
            this.Editor0AutoSave.Name = "Editor0AutoSave";
            this.Editor0AutoSave.Tag = "Editor → AutoSalvare";
            // 
            // numericUpDown1
            // 
            resources.ApplyResources(this.numericUpDown1, "numericUpDown1");
            this.numericUpDown1.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Name = "label6";
            // 
            // Editor0VisualExperience
            // 
            resources.ApplyResources(this.Editor0VisualExperience, "Editor0VisualExperience");
            this.Editor0VisualExperience.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Editor0VisualExperience.Controls.Add(this.checkBox4);
            this.Editor0VisualExperience.Controls.Add(this.checkBox14);
            this.Editor0VisualExperience.Controls.Add(this.checkBox7);
            this.Editor0VisualExperience.Controls.Add(this.groupBox1);
            this.Editor0VisualExperience.Controls.Add(this.groupBox2);
            this.Editor0VisualExperience.Name = "Editor0VisualExperience";
            this.Editor0VisualExperience.Tag = "Editor → Experiență vizuală";
            // 
            // checkBox4
            // 
            resources.ApplyResources(this.checkBox4, "checkBox4");
            this.checkBox4.ForeColor = System.Drawing.Color.White;
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // checkBox14
            // 
            resources.ApplyResources(this.checkBox14, "checkBox14");
            this.checkBox14.ForeColor = System.Drawing.Color.White;
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            resources.ApplyResources(this.checkBox7, "checkBox7");
            this.checkBox7.ForeColor = System.Drawing.Color.White;
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // Editor0Findreplace
            // 
            resources.ApplyResources(this.Editor0Findreplace, "Editor0Findreplace");
            this.Editor0Findreplace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Editor0Findreplace.Controls.Add(this.checkBox5);
            this.Editor0Findreplace.Name = "Editor0Findreplace";
            this.Editor0Findreplace.Tag = "Editor → Găsire/înlocuire";
            // 
            // checkBox5
            // 
            resources.ApplyResources(this.checkBox5, "checkBox5");
            this.checkBox5.ForeColor = System.Drawing.Color.White;
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // CodeConversion0Headers
            // 
            resources.ApplyResources(this.CodeConversion0Headers, "CodeConversion0Headers");
            this.CodeConversion0Headers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CodeConversion0Headers.Controls.Add(this.groupBox7);
            this.CodeConversion0Headers.Controls.Add(this.groupBox5);
            this.CodeConversion0Headers.Controls.Add(this.groupBox6);
            this.CodeConversion0Headers.Name = "CodeConversion0Headers";
            this.CodeConversion0Headers.Tag = "Convertire cod → Antete";
            // 
            // groupBox7
            // 
            resources.ApplyResources(this.groupBox7, "groupBox7");
            this.groupBox7.Controls.Add(this.linkLabel3);
            this.groupBox7.Controls.Add(this.textBox3);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.ForeColor = System.Drawing.Color.White;
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.TabStop = false;
            // 
            // linkLabel3
            // 
            resources.ApplyResources(this.linkLabel3, "linkLabel3");
            this.linkLabel3.LinkColor = System.Drawing.Color.White;
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.TabStop = true;
            // 
            // textBox3
            // 
            resources.ApplyResources(this.textBox3, "textBox3");
            this.textBox3.BackColor = System.Drawing.Color.Gray;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.ForeColor = System.Drawing.Color.White;
            this.textBox3.Name = "textBox3";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // groupBox5
            // 
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Controls.Add(this.linkLabel2);
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // linkLabel2
            // 
            resources.ApplyResources(this.linkLabel2, "linkLabel2");
            this.linkLabel2.LinkColor = System.Drawing.Color.White;
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.TabStop = true;
            // 
            // textBox2
            // 
            resources.ApplyResources(this.textBox2, "textBox2");
            this.textBox2.BackColor = System.Drawing.Color.Gray;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.ForeColor = System.Drawing.Color.White;
            this.textBox2.Name = "textBox2";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // groupBox6
            // 
            resources.ApplyResources(this.groupBox6, "groupBox6");
            this.groupBox6.Controls.Add(this.linkLabel1);
            this.groupBox6.Controls.Add(this.textBox1);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.ForeColor = System.Drawing.Color.White;
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.TabStop = false;
            // 
            // linkLabel1
            // 
            resources.ApplyResources(this.linkLabel1, "linkLabel1");
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.TabStop = true;
            // 
            // textBox1
            // 
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.BackColor = System.Drawing.Color.Gray;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Name = "textBox1";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // CodeConversion0LogicSchema
            // 
            resources.ApplyResources(this.CodeConversion0LogicSchema, "CodeConversion0LogicSchema");
            this.CodeConversion0LogicSchema.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CodeConversion0LogicSchema.Controls.Add(this.checkBox6);
            this.CodeConversion0LogicSchema.Name = "CodeConversion0LogicSchema";
            this.CodeConversion0LogicSchema.Tag = "Convertire cod  → Schemă logică";
            // 
            // checkBox6
            // 
            resources.ApplyResources(this.checkBox6, "checkBox6");
            this.checkBox6.ForeColor = System.Drawing.Color.White;
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // Debugging
            // 
            resources.ApplyResources(this.Debugging, "Debugging");
            this.Debugging.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Debugging.Controls.Add(this.groupBox15);
            this.Debugging.Controls.Add(this.groupBox13);
            this.Debugging.Controls.Add(this.groupBox8);
            this.Debugging.Controls.Add(this.groupBox14);
            this.Debugging.Controls.Add(this.label16);
            this.Debugging.Controls.Add(this.groupBox9);
            this.Debugging.Controls.Add(this.groupBox10);
            this.Debugging.Name = "Debugging";
            this.Debugging.Tag = "Advanced options";
            // 
            // groupBox15
            // 
            resources.ApplyResources(this.groupBox15, "groupBox15");
            this.groupBox15.Controls.Add(this.linkLabel6);
            this.groupBox15.Controls.Add(this.textBox5);
            this.groupBox15.Controls.Add(this.label21);
            this.groupBox15.ForeColor = System.Drawing.Color.White;
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.TabStop = false;
            // 
            // linkLabel6
            // 
            resources.ApplyResources(this.linkLabel6, "linkLabel6");
            this.linkLabel6.LinkColor = System.Drawing.Color.White;
            this.linkLabel6.Name = "linkLabel6";
            this.linkLabel6.TabStop = true;
            this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel6_LinkClicked);
            // 
            // textBox5
            // 
            resources.ApplyResources(this.textBox5, "textBox5");
            this.textBox5.Name = "textBox5";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Name = "label21";
            // 
            // groupBox13
            // 
            resources.ApplyResources(this.groupBox13, "groupBox13");
            this.groupBox13.Controls.Add(this.radioButton3);
            this.groupBox13.Controls.Add(this.radioButton2);
            this.groupBox13.Controls.Add(this.radioButton1);
            this.groupBox13.Controls.Add(this.label19);
            this.groupBox13.ForeColor = System.Drawing.Color.White;
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.TabStop = false;
            // 
            // radioButton3
            // 
            resources.ApplyResources(this.radioButton3, "radioButton3");
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.TabStop = true;
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            resources.ApplyResources(this.radioButton2, "radioButton2");
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.TabStop = true;
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            resources.ApplyResources(this.radioButton1, "radioButton1");
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.TabStop = true;
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Name = "label19";
            // 
            // groupBox8
            // 
            resources.ApplyResources(this.groupBox8, "groupBox8");
            this.groupBox8.Controls.Add(this.linkLabel4);
            this.groupBox8.Controls.Add(this.button12);
            this.groupBox8.Controls.Add(this.textBox4);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.ForeColor = System.Drawing.Color.White;
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.TabStop = false;
            // 
            // linkLabel4
            // 
            resources.ApplyResources(this.linkLabel4, "linkLabel4");
            this.linkLabel4.LinkColor = System.Drawing.Color.White;
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.TabStop = true;
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // button12
            // 
            resources.ApplyResources(this.button12, "button12");
            this.button12.ForeColor = System.Drawing.Color.Black;
            this.button12.Name = "button12";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // textBox4
            // 
            resources.ApplyResources(this.textBox4, "textBox4");
            this.textBox4.Name = "textBox4";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Name = "label11";
            // 
            // groupBox14
            // 
            resources.ApplyResources(this.groupBox14, "groupBox14");
            this.groupBox14.Controls.Add(this.button15);
            this.groupBox14.Controls.Add(this.label17);
            this.groupBox14.ForeColor = System.Drawing.Color.White;
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.TabStop = false;
            // 
            // button15
            // 
            resources.ApplyResources(this.button15, "button15");
            this.button15.ForeColor = System.Drawing.Color.Black;
            this.button15.Name = "button15";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Name = "label17";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Name = "label16";
            // 
            // groupBox9
            // 
            resources.ApplyResources(this.groupBox9, "groupBox9");
            this.groupBox9.Controls.Add(this.checkBox15);
            this.groupBox9.ForeColor = System.Drawing.Color.White;
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.TabStop = false;
            // 
            // checkBox15
            // 
            resources.ApplyResources(this.checkBox15, "checkBox15");
            this.checkBox15.ForeColor = System.Drawing.Color.White;
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            resources.ApplyResources(this.groupBox10, "groupBox10");
            this.groupBox10.Controls.Add(this.checkBox16);
            this.groupBox10.ForeColor = System.Drawing.Color.White;
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.TabStop = false;
            // 
            // checkBox16
            // 
            resources.ApplyResources(this.checkBox16, "checkBox16");
            this.checkBox16.ForeColor = System.Drawing.Color.White;
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // ErrorReporting
            // 
            resources.ApplyResources(this.ErrorReporting, "ErrorReporting");
            this.ErrorReporting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ErrorReporting.Controls.Add(this.linkLabel5);
            this.ErrorReporting.Controls.Add(this.checkBox21);
            this.ErrorReporting.Controls.Add(this.checkBox20);
            this.ErrorReporting.Controls.Add(this.checkBox19);
            this.ErrorReporting.Controls.Add(this.checkBox17);
            this.ErrorReporting.Controls.Add(this.checkBox18);
            this.ErrorReporting.Name = "ErrorReporting";
            this.ErrorReporting.Tag = "Raportarea erorilor";
            // 
            // linkLabel5
            // 
            resources.ApplyResources(this.linkLabel5, "linkLabel5");
            this.linkLabel5.LinkColor = System.Drawing.Color.White;
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.TabStop = true;
            // 
            // checkBox21
            // 
            resources.ApplyResources(this.checkBox21, "checkBox21");
            this.checkBox21.ForeColor = System.Drawing.Color.White;
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // checkBox20
            // 
            resources.ApplyResources(this.checkBox20, "checkBox20");
            this.checkBox20.ForeColor = System.Drawing.Color.White;
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            resources.ApplyResources(this.checkBox19, "checkBox19");
            this.checkBox19.ForeColor = System.Drawing.Color.White;
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            resources.ApplyResources(this.checkBox17, "checkBox17");
            this.checkBox17.ForeColor = System.Drawing.Color.White;
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            resources.ApplyResources(this.checkBox18, "checkBox18");
            this.checkBox18.ForeColor = System.Drawing.Color.White;
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.CheckedChanged += new System.EventHandler(this.checkBox18_CheckedChanged);
            // 
            // AutomaticUpdates
            // 
            resources.ApplyResources(this.AutomaticUpdates, "AutomaticUpdates");
            this.AutomaticUpdates.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.AutomaticUpdates.Controls.Add(this.groupBox11);
            this.AutomaticUpdates.Controls.Add(this.groupBox12);
            this.AutomaticUpdates.Name = "AutomaticUpdates";
            this.AutomaticUpdates.Tag = "Actualizări aplicație";
            // 
            // groupBox11
            // 
            resources.ApplyResources(this.groupBox11, "groupBox11");
            this.groupBox11.Controls.Add(this.comboBox1);
            this.groupBox11.Controls.Add(this.label13);
            this.groupBox11.ForeColor = System.Drawing.Color.White;
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.TabStop = false;
            // 
            // comboBox1
            // 
            resources.ApplyResources(this.comboBox1, "comboBox1");
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            resources.GetString("comboBox1.Items"),
            resources.GetString("comboBox1.Items1"),
            resources.GetString("comboBox1.Items2")});
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // groupBox12
            // 
            resources.ApplyResources(this.groupBox12, "groupBox12");
            this.groupBox12.Controls.Add(this.button13);
            this.groupBox12.Controls.Add(this.label12);
            this.groupBox12.ForeColor = System.Drawing.Color.White;
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.TabStop = false;
            // 
            // button13
            // 
            resources.ApplyResources(this.button13, "button13");
            this.button13.ForeColor = System.Drawing.Color.Black;
            this.button13.Name = "button13";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // ResetSettings
            // 
            resources.ApplyResources(this.ResetSettings, "ResetSettings");
            this.ResetSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ResetSettings.Controls.Add(this.button14);
            this.ResetSettings.Controls.Add(this.label15);
            this.ResetSettings.Name = "ResetSettings";
            this.ResetSettings.Tag = "Restaurare setări";
            // 
            // button14
            // 
            resources.ApplyResources(this.button14, "button14");
            this.button14.ForeColor = System.Drawing.Color.Black;
            this.button14.Name = "button14";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Name = "label15";
            // 
            // listBox1
            // 
            resources.ApplyResources(this.listBox1, "listBox1");
            this.listBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.listBox1.ForeColor = System.Drawing.Color.White;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new Controls.Development.ImageListBoxItem[] {
            this.imageListBoxItem1,
            this.imageListBoxItem2,
            this.imageListBoxItem4,
            this.imageListBoxItem5,
            this.imageListBoxItem6,
            this.imageListBoxItem7,
            this.imageListBoxItem8,
            this.imageListBoxItem9,
            this.imageListBoxItem10,
            this.imageListBoxItem11,
            this.imageListBoxItem12,
            this.imageListBoxItem17,
            this.imageListBoxItem18,
            this.imageListBoxItem19,
            this.imageListBoxItem3});
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // imageListBoxItem1
            // 
            resources.ApplyResources(this.imageListBoxItem1, "imageListBoxItem1");
            // 
            // imageListBoxItem2
            // 
            resources.ApplyResources(this.imageListBoxItem2, "imageListBoxItem2");
            // 
            // imageListBoxItem4
            // 
            resources.ApplyResources(this.imageListBoxItem4, "imageListBoxItem4");
            // 
            // imageListBoxItem5
            // 
            resources.ApplyResources(this.imageListBoxItem5, "imageListBoxItem5");
            // 
            // imageListBoxItem6
            // 
            resources.ApplyResources(this.imageListBoxItem6, "imageListBoxItem6");
            // 
            // imageListBoxItem7
            // 
            resources.ApplyResources(this.imageListBoxItem7, "imageListBoxItem7");
            // 
            // imageListBoxItem8
            // 
            resources.ApplyResources(this.imageListBoxItem8, "imageListBoxItem8");
            // 
            // imageListBoxItem9
            // 
            resources.ApplyResources(this.imageListBoxItem9, "imageListBoxItem9");
            // 
            // imageListBoxItem10
            // 
            resources.ApplyResources(this.imageListBoxItem10, "imageListBoxItem10");
            // 
            // imageListBoxItem11
            // 
            resources.ApplyResources(this.imageListBoxItem11, "imageListBoxItem11");
            // 
            // imageListBoxItem12
            // 
            resources.ApplyResources(this.imageListBoxItem12, "imageListBoxItem12");
            // 
            // imageListBoxItem17
            // 
            resources.ApplyResources(this.imageListBoxItem17, "imageListBoxItem17");
            // 
            // imageListBoxItem18
            // 
            resources.ApplyResources(this.imageListBoxItem18, "imageListBoxItem18");
            // 
            // imageListBoxItem19
            // 
            resources.ApplyResources(this.imageListBoxItem19, "imageListBoxItem19");
            // 
            // imageListBoxItem3
            // 
            resources.ApplyResources(this.imageListBoxItem3, "imageListBoxItem3");
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // folderBrowserDialog1
            // 
            resources.ApplyResources(this.folderBrowserDialog1, "folderBrowserDialog1");
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // button16
            // 
            resources.ApplyResources(this.button16, "button16");
            this.button16.BackColor = System.Drawing.SystemColors.Control;
            this.button16.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button16.Name = "button16";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // openFileDialog1
            // 
            resources.ApplyResources(this.openFileDialog1, "openFileDialog1");
            // 
            // saveFileDialog1
            // 
            resources.ApplyResources(this.saveFileDialog1, "saveFileDialog1");
            // 
            // Options
            // 
            this.AcceptButton = this.button1;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.CancelButton = this.button2;
            this.Controls.Add(this.button16);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Debugging);
            this.Controls.Add(this.CodeConversion0LogicSchema);
            this.Controls.Add(this.Editor0Findreplace);
            this.Controls.Add(this.ErrorReporting);
            this.Controls.Add(this.Application0Startup);
            this.Controls.Add(this.Editor0VisualExperience);
            this.Controls.Add(this.Editor0AutoSave);
            this.Controls.Add(this.Application0ExtensionManager);
            this.Controls.Add(this.Application0FileAssociations);
            this.Controls.Add(this.CodeConversion0Headers);
            this.Controls.Add(this.Application0ImportExportSettings);
            this.Controls.Add(this.Application0Codeboard);
            this.Controls.Add(this.Application0Interface);
            this.Controls.Add(this.Application0Sync);
            this.Controls.Add(this.ResetSettings);
            this.Controls.Add(this.AutomaticUpdates);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Options";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Tag = "Configurări - Rezultatele căutării";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Options_FormClosing);
            this.Load += new System.EventHandler(this.Options_Load);
            this.Application0Startup.ResumeLayout(false);
            this.Application0Startup.PerformLayout();
            this.Application0Interface.ResumeLayout(false);
            this.Application0Interface.PerformLayout();
            this.Application0Sync.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.Application0FileAssociations.ResumeLayout(false);
            this.Application0ExtensionManager.ResumeLayout(false);
            this.Application0ImportExportSettings.ResumeLayout(false);
            this.Editor0AutoSave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.Editor0VisualExperience.ResumeLayout(false);
            this.Editor0VisualExperience.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.Editor0Findreplace.ResumeLayout(false);
            this.Editor0Findreplace.PerformLayout();
            this.CodeConversion0Headers.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.CodeConversion0LogicSchema.ResumeLayout(false);
            this.CodeConversion0LogicSchema.PerformLayout();
            this.Debugging.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.ErrorReporting.ResumeLayout(false);
            this.ErrorReporting.PerformLayout();
            this.AutomaticUpdates.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.ResetSettings.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Application0Startup;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox SearchBox;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel Application0Interface;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.Panel Application0Sync;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel Application0FileAssociations;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel Application0ExtensionManager;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel Application0Codeboard;
        private System.Windows.Forms.Panel Application0ImportExportSettings;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel Editor0AutoSave;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel Editor0VisualExperience;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.Panel Editor0Findreplace;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.Panel CodeConversion0Headers;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel CodeConversion0LogicSchema;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel Debugging;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.Panel ErrorReporting;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.Panel AutomaticUpdates;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel ResetSettings;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label15;
        private Controls.Development.ImageListBox listBox1;
        private Controls.Development.ImageListBoxItem imageListBoxItem1;
        private Controls.Development.ImageListBoxItem imageListBoxItem2;
        private Controls.Development.ImageListBoxItem imageListBoxItem4;
        private Controls.Development.ImageListBoxItem imageListBoxItem5;
        private Controls.Development.ImageListBoxItem imageListBoxItem6;
        private Controls.Development.ImageListBoxItem imageListBoxItem8;
        private Controls.Development.ImageListBoxItem imageListBoxItem9;
        private Controls.Development.ImageListBoxItem imageListBoxItem10;
        private Controls.Development.ImageListBoxItem imageListBoxItem11;
        private Controls.Development.ImageListBoxItem imageListBoxItem12;
        private Controls.Development.ImageListBoxItem imageListBoxItem17;
        private Controls.Development.ImageListBoxItem imageListBoxItem18;
        private Controls.Development.ImageListBoxItem imageListBoxItem19;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label14;
        private Controls.Development.ImageListBoxItem imageListBoxItem3;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label19;
        private Controls.Development.ImageListBoxItem imageListBoxItem7;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.LinkLabel linkLabel6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label21;
    }
}