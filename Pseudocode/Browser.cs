﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Pseudocode
{
    public partial class Browser : DockContent
    {
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        string AppName;
        string NoBrowserInstalled;
        MainForm m;
        public Browser(MainForm mf, string ap, string Url)
        {
            InitializeComponent();
            m = mf;
            AppName = ap;
            if (Properties.Settings.Default.UILang == 0)
            {
                NoBrowserInstalled = "There is no Internet browser installed on this computer.\n\nIn order to use this feature, a browser has to be installed on your machine. Visit www.browserchoice.eu to download a web browser for your computer if you are based in the European Union or, if located in any other country, visit www.whatbrowser.org.";
            }
            else
            {
                NoBrowserInstalled = "Niciun browser Web nu este instalat pe acest computer.\n\nPentru a utiliza această caracteristică, un browser trebuie să fie instalat pe computer. Pentru a descărca un browser, vizitați www.browserchoice.eu dacă sunteți în Uniunea Europeană sau www.whatbrowser.org dacă sunteți din oricare altă țară.";
            }
            webBrowser1.DocumentTitleChanged += WebBrowser1_DocumentTitleChanged;
            webBrowser1.Navigated += WebBrowser1_Navigated;
            webBrowser1.NewWindow3 += WebBrowser1_NewWindow3;
            webBrowser1.Navigate(Url);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F1)
            {
                string lang2 = "ro";
                if (Properties.Settings.Default.UILang == 0) lang2 = "en";
                if (Properties.Settings.Default.UILang == 1) lang2 = "ro";
                webBrowser1.Navigate(Application.StartupPath + "\\com.valinet.pseudocode.help\\" + lang2 + "\\Default\\webframe.html");
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void WebBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            m.toolStripButton30.Text = webBrowser1.Url.ToString();
        }
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }

        private void WebBrowser1_NewWindow3(object sender, WebBrowserNewWindowEventArgs e)
        {
            try
            {
                e.Cancel = true;
                m.ShowBrowser(e.Url.ToString());
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                {
                    CasetaDeMesaj(this, NoBrowserInstalled, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void WebBrowser1_DocumentTitleChanged(object sender, EventArgs e)
        {
            this.Text = webBrowser1.DocumentTitle;
            m.Text = webBrowser1.DocumentTitle + " - " + AppName;
            m.toolStripButton30.Text = webBrowser1.Url.ToString();
        }
    }
}
