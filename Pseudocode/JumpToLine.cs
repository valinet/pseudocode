﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class JumpToLine : Form
    {
        Form1 fm;
        public JumpToLine(Form1 frm)
        {
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            InitializeComponent();
            fm = frm;
            textBox1.Text = (fm.pseudocodeEd.Lines.Current.Number + 1).ToString();
            textBox2.Text = fm.pseudocodeEd.Lines.Count.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fm.pseudocodeEd.GoTo.Line(Convert.ToInt32(comboBox1.Text) - 1);
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void JumpToLine_Load(object sender, EventArgs e)
        {

        }
    }
}
