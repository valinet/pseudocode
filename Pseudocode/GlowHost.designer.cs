﻿namespace Pseudocode
{
    partial class GlowHost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GlowHost));
            this.Restore = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.MinimizeLightPressed = new System.Windows.Forms.Button();
            this.RestoreLightPressed = new System.Windows.Forms.Button();
            this.CloseLightPressed = new System.Windows.Forms.Button();
            this.MinimizeLightHover = new System.Windows.Forms.Button();
            this.RestoreLightHover = new System.Windows.Forms.Button();
            this.CloseLightHover = new System.Windows.Forms.Button();
            this.MinimizeLight = new System.Windows.Forms.Button();
            this.RestoreLight = new System.Windows.Forms.Button();
            this.CloseLight = new System.Windows.Forms.Button();
            this.MinimizeBlackPressed = new System.Windows.Forms.Button();
            this.RestoreBlackPressed = new System.Windows.Forms.Button();
            this.CloseBlackPressed = new System.Windows.Forms.Button();
            this.MinimizeBlackHover = new System.Windows.Forms.Button();
            this.RestoreBlackHover = new System.Windows.Forms.Button();
            this.CloseBlackHover = new System.Windows.Forms.Button();
            this.MinimizeBlack = new System.Windows.Forms.Button();
            this.RestoreBlack = new System.Windows.Forms.Button();
            this.CloseBlack = new System.Windows.Forms.Button();
            this.EE = new System.Windows.Forms.Panel();
            this.NE = new System.Windows.Forms.Panel();
            this.NN = new System.Windows.Forms.Panel();
            this.E = new System.Windows.Forms.Panel();
            this.SE = new System.Windows.Forms.Panel();
            this.S = new System.Windows.Forms.Panel();
            this.SV = new System.Windows.Forms.Panel();
            this.V = new System.Windows.Forms.Panel();
            this.NV = new System.Windows.Forms.Panel();
            this.Minimize = new System.Windows.Forms.Button();
            this.N = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuIcon = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Title = new Pseudocode.PowerLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // Restore
            // 
            this.Restore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Restore.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Restore.BackgroundImage")));
            this.Restore.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Restore.FlatAppearance.BorderSize = 0;
            this.Restore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Restore.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Restore.Location = new System.Drawing.Point(663, 0);
            this.Restore.Name = "Restore";
            this.Restore.Size = new System.Drawing.Size(34, 26);
            this.Restore.TabIndex = 140;
            this.Restore.UseVisualStyleBackColor = true;
            this.Restore.Click += new System.EventHandler(this.Restore_Click);
            this.Restore.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Restore_MouseDown);
            this.Restore.MouseEnter += new System.EventHandler(this.Restore_MouseEnter);
            this.Restore.MouseLeave += new System.EventHandler(this.Restore_MouseLeave);
            // 
            // Close
            // 
            this.Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Close.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Close.BackgroundImage")));
            this.Close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Close.FlatAppearance.BorderSize = 0;
            this.Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Close.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Close.Location = new System.Drawing.Point(697, 0);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(34, 26);
            this.Close.TabIndex = 139;
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            this.Close.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Close_MouseDown);
            this.Close.MouseEnter += new System.EventHandler(this.Close_MouseEnter);
            this.Close.MouseLeave += new System.EventHandler(this.Close_MouseLeave);
            // 
            // MinimizeLightPressed
            // 
            this.MinimizeLightPressed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MinimizeLightPressed.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimizeLightPressed.BackgroundImage")));
            this.MinimizeLightPressed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeLightPressed.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.MinimizeLightPressed.Location = new System.Drawing.Point(373, 219);
            this.MinimizeLightPressed.Name = "MinimizeLightPressed";
            this.MinimizeLightPressed.Size = new System.Drawing.Size(34, 26);
            this.MinimizeLightPressed.TabIndex = 169;
            this.MinimizeLightPressed.UseVisualStyleBackColor = true;
            this.MinimizeLightPressed.Visible = false;
            // 
            // RestoreLightPressed
            // 
            this.RestoreLightPressed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RestoreLightPressed.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RestoreLightPressed.BackgroundImage")));
            this.RestoreLightPressed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestoreLightPressed.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.RestoreLightPressed.Location = new System.Drawing.Point(407, 219);
            this.RestoreLightPressed.Name = "RestoreLightPressed";
            this.RestoreLightPressed.Size = new System.Drawing.Size(34, 26);
            this.RestoreLightPressed.TabIndex = 168;
            this.RestoreLightPressed.UseVisualStyleBackColor = true;
            this.RestoreLightPressed.Visible = false;
            // 
            // CloseLightPressed
            // 
            this.CloseLightPressed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseLightPressed.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CloseLightPressed.BackgroundImage")));
            this.CloseLightPressed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseLightPressed.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CloseLightPressed.Location = new System.Drawing.Point(441, 219);
            this.CloseLightPressed.Name = "CloseLightPressed";
            this.CloseLightPressed.Size = new System.Drawing.Size(34, 26);
            this.CloseLightPressed.TabIndex = 167;
            this.CloseLightPressed.UseVisualStyleBackColor = true;
            this.CloseLightPressed.Visible = false;
            // 
            // MinimizeLightHover
            // 
            this.MinimizeLightHover.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MinimizeLightHover.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimizeLightHover.BackgroundImage")));
            this.MinimizeLightHover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeLightHover.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.MinimizeLightHover.Location = new System.Drawing.Point(373, 187);
            this.MinimizeLightHover.Name = "MinimizeLightHover";
            this.MinimizeLightHover.Size = new System.Drawing.Size(34, 26);
            this.MinimizeLightHover.TabIndex = 166;
            this.MinimizeLightHover.UseVisualStyleBackColor = true;
            this.MinimizeLightHover.Visible = false;
            // 
            // RestoreLightHover
            // 
            this.RestoreLightHover.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RestoreLightHover.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RestoreLightHover.BackgroundImage")));
            this.RestoreLightHover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestoreLightHover.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.RestoreLightHover.Location = new System.Drawing.Point(407, 187);
            this.RestoreLightHover.Name = "RestoreLightHover";
            this.RestoreLightHover.Size = new System.Drawing.Size(34, 26);
            this.RestoreLightHover.TabIndex = 165;
            this.RestoreLightHover.UseVisualStyleBackColor = true;
            this.RestoreLightHover.Visible = false;
            // 
            // CloseLightHover
            // 
            this.CloseLightHover.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseLightHover.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CloseLightHover.BackgroundImage")));
            this.CloseLightHover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseLightHover.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CloseLightHover.Location = new System.Drawing.Point(441, 187);
            this.CloseLightHover.Name = "CloseLightHover";
            this.CloseLightHover.Size = new System.Drawing.Size(34, 26);
            this.CloseLightHover.TabIndex = 164;
            this.CloseLightHover.UseVisualStyleBackColor = true;
            this.CloseLightHover.Visible = false;
            // 
            // MinimizeLight
            // 
            this.MinimizeLight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MinimizeLight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimizeLight.BackgroundImage")));
            this.MinimizeLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeLight.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.MinimizeLight.Location = new System.Drawing.Point(373, 155);
            this.MinimizeLight.Name = "MinimizeLight";
            this.MinimizeLight.Size = new System.Drawing.Size(34, 26);
            this.MinimizeLight.TabIndex = 163;
            this.MinimizeLight.UseVisualStyleBackColor = true;
            this.MinimizeLight.Visible = false;
            // 
            // RestoreLight
            // 
            this.RestoreLight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RestoreLight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RestoreLight.BackgroundImage")));
            this.RestoreLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestoreLight.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.RestoreLight.Location = new System.Drawing.Point(407, 155);
            this.RestoreLight.Name = "RestoreLight";
            this.RestoreLight.Size = new System.Drawing.Size(34, 26);
            this.RestoreLight.TabIndex = 162;
            this.RestoreLight.UseVisualStyleBackColor = true;
            this.RestoreLight.Visible = false;
            // 
            // CloseLight
            // 
            this.CloseLight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseLight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CloseLight.BackgroundImage")));
            this.CloseLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseLight.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CloseLight.Location = new System.Drawing.Point(441, 155);
            this.CloseLight.Name = "CloseLight";
            this.CloseLight.Size = new System.Drawing.Size(34, 26);
            this.CloseLight.TabIndex = 161;
            this.CloseLight.UseVisualStyleBackColor = true;
            this.CloseLight.Visible = false;
            // 
            // MinimizeBlackPressed
            // 
            this.MinimizeBlackPressed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MinimizeBlackPressed.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimizeBlackPressed.BackgroundImage")));
            this.MinimizeBlackPressed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeBlackPressed.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.MinimizeBlackPressed.Location = new System.Drawing.Point(481, 219);
            this.MinimizeBlackPressed.Name = "MinimizeBlackPressed";
            this.MinimizeBlackPressed.Size = new System.Drawing.Size(34, 26);
            this.MinimizeBlackPressed.TabIndex = 160;
            this.MinimizeBlackPressed.UseVisualStyleBackColor = true;
            this.MinimizeBlackPressed.Visible = false;
            // 
            // RestoreBlackPressed
            // 
            this.RestoreBlackPressed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RestoreBlackPressed.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RestoreBlackPressed.BackgroundImage")));
            this.RestoreBlackPressed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestoreBlackPressed.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.RestoreBlackPressed.Location = new System.Drawing.Point(515, 219);
            this.RestoreBlackPressed.Name = "RestoreBlackPressed";
            this.RestoreBlackPressed.Size = new System.Drawing.Size(34, 26);
            this.RestoreBlackPressed.TabIndex = 159;
            this.RestoreBlackPressed.UseVisualStyleBackColor = true;
            this.RestoreBlackPressed.Visible = false;
            // 
            // CloseBlackPressed
            // 
            this.CloseBlackPressed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseBlackPressed.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CloseBlackPressed.BackgroundImage")));
            this.CloseBlackPressed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseBlackPressed.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CloseBlackPressed.Location = new System.Drawing.Point(549, 219);
            this.CloseBlackPressed.Name = "CloseBlackPressed";
            this.CloseBlackPressed.Size = new System.Drawing.Size(34, 26);
            this.CloseBlackPressed.TabIndex = 158;
            this.CloseBlackPressed.UseVisualStyleBackColor = true;
            this.CloseBlackPressed.Visible = false;
            // 
            // MinimizeBlackHover
            // 
            this.MinimizeBlackHover.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MinimizeBlackHover.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimizeBlackHover.BackgroundImage")));
            this.MinimizeBlackHover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeBlackHover.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.MinimizeBlackHover.Location = new System.Drawing.Point(481, 187);
            this.MinimizeBlackHover.Name = "MinimizeBlackHover";
            this.MinimizeBlackHover.Size = new System.Drawing.Size(34, 26);
            this.MinimizeBlackHover.TabIndex = 157;
            this.MinimizeBlackHover.UseVisualStyleBackColor = true;
            this.MinimizeBlackHover.Visible = false;
            // 
            // RestoreBlackHover
            // 
            this.RestoreBlackHover.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RestoreBlackHover.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RestoreBlackHover.BackgroundImage")));
            this.RestoreBlackHover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestoreBlackHover.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.RestoreBlackHover.Location = new System.Drawing.Point(515, 187);
            this.RestoreBlackHover.Name = "RestoreBlackHover";
            this.RestoreBlackHover.Size = new System.Drawing.Size(34, 26);
            this.RestoreBlackHover.TabIndex = 156;
            this.RestoreBlackHover.UseVisualStyleBackColor = true;
            this.RestoreBlackHover.Visible = false;
            // 
            // CloseBlackHover
            // 
            this.CloseBlackHover.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseBlackHover.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CloseBlackHover.BackgroundImage")));
            this.CloseBlackHover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseBlackHover.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CloseBlackHover.Location = new System.Drawing.Point(549, 187);
            this.CloseBlackHover.Name = "CloseBlackHover";
            this.CloseBlackHover.Size = new System.Drawing.Size(34, 26);
            this.CloseBlackHover.TabIndex = 155;
            this.CloseBlackHover.UseVisualStyleBackColor = true;
            this.CloseBlackHover.Visible = false;
            // 
            // MinimizeBlack
            // 
            this.MinimizeBlack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MinimizeBlack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimizeBlack.BackgroundImage")));
            this.MinimizeBlack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeBlack.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.MinimizeBlack.Location = new System.Drawing.Point(481, 155);
            this.MinimizeBlack.Name = "MinimizeBlack";
            this.MinimizeBlack.Size = new System.Drawing.Size(34, 26);
            this.MinimizeBlack.TabIndex = 154;
            this.MinimizeBlack.UseVisualStyleBackColor = true;
            this.MinimizeBlack.Visible = false;
            // 
            // RestoreBlack
            // 
            this.RestoreBlack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RestoreBlack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RestoreBlack.BackgroundImage")));
            this.RestoreBlack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestoreBlack.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.RestoreBlack.Location = new System.Drawing.Point(515, 155);
            this.RestoreBlack.Name = "RestoreBlack";
            this.RestoreBlack.Size = new System.Drawing.Size(34, 26);
            this.RestoreBlack.TabIndex = 153;
            this.RestoreBlack.UseVisualStyleBackColor = true;
            this.RestoreBlack.Visible = false;
            // 
            // CloseBlack
            // 
            this.CloseBlack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseBlack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CloseBlack.BackgroundImage")));
            this.CloseBlack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseBlack.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CloseBlack.Location = new System.Drawing.Point(549, 155);
            this.CloseBlack.Name = "CloseBlack";
            this.CloseBlack.Size = new System.Drawing.Size(34, 26);
            this.CloseBlack.TabIndex = 152;
            this.CloseBlack.UseVisualStyleBackColor = true;
            this.CloseBlack.Visible = false;
            // 
            // EE
            // 
            this.EE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EE.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.EE.Location = new System.Drawing.Point(453, 31);
            this.EE.Name = "EE";
            this.EE.Size = new System.Drawing.Size(2, 34);
            this.EE.TabIndex = 151;
            this.EE.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EE_MouseDown);
            this.EE.MouseMove += new System.Windows.Forms.MouseEventHandler(this.EE_MouseMove);
            this.EE.MouseUp += new System.Windows.Forms.MouseEventHandler(this.EE_MouseUp);
            // 
            // NE
            // 
            this.NE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NE.Cursor = System.Windows.Forms.Cursors.SizeNESW;
            this.NE.Location = new System.Drawing.Point(732, 1);
            this.NE.Name = "NE";
            this.NE.Size = new System.Drawing.Size(5, 5);
            this.NE.TabIndex = 150;
            this.NE.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NE_MouseDown);
            this.NE.MouseMove += new System.Windows.Forms.MouseEventHandler(this.NE_MouseMove);
            this.NE.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NE_MouseUp);
            // 
            // NN
            // 
            this.NN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NN.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.NN.Location = new System.Drawing.Point(226, 93);
            this.NN.Name = "NN";
            this.NN.Size = new System.Drawing.Size(429, 2);
            this.NN.TabIndex = 149;
            this.NN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NN_MouseDown);
            this.NN.MouseMove += new System.Windows.Forms.MouseEventHandler(this.NN_MouseMove);
            this.NN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NN_MouseUp);
            // 
            // E
            // 
            this.E.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.E.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.E.Location = new System.Drawing.Point(733, 26);
            this.E.Name = "E";
            this.E.Size = new System.Drawing.Size(5, 481);
            this.E.TabIndex = 148;
            this.E.MouseDown += new System.Windows.Forms.MouseEventHandler(this.E_MouseDown);
            this.E.MouseMove += new System.Windows.Forms.MouseEventHandler(this.E_MouseMove);
            this.E.MouseUp += new System.Windows.Forms.MouseEventHandler(this.E_MouseUp);
            // 
            // SE
            // 
            this.SE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SE.Cursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.SE.Location = new System.Drawing.Point(733, 507);
            this.SE.Name = "SE";
            this.SE.Size = new System.Drawing.Size(5, 5);
            this.SE.TabIndex = 147;
            this.SE.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SE_MouseDown);
            this.SE.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SE_MouseMove);
            this.SE.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SE_MouseUp);
            // 
            // S
            // 
            this.S.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.S.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.S.Location = new System.Drawing.Point(5, 507);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(728, 5);
            this.S.TabIndex = 146;
            this.S.MouseDown += new System.Windows.Forms.MouseEventHandler(this.S_MouseDown);
            this.S.MouseMove += new System.Windows.Forms.MouseEventHandler(this.S_MouseMove);
            this.S.MouseUp += new System.Windows.Forms.MouseEventHandler(this.S_MouseUp);
            // 
            // SV
            // 
            this.SV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SV.Cursor = System.Windows.Forms.Cursors.SizeNESW;
            this.SV.Location = new System.Drawing.Point(0, 507);
            this.SV.Name = "SV";
            this.SV.Size = new System.Drawing.Size(5, 5);
            this.SV.TabIndex = 145;
            this.SV.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SV_MouseDown);
            this.SV.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SV_MouseMove);
            this.SV.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SV_MouseUp);
            // 
            // V
            // 
            this.V.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.V.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.V.Location = new System.Drawing.Point(42, 61);
            this.V.Name = "V";
            this.V.Size = new System.Drawing.Size(5, 28);
            this.V.TabIndex = 144;
            this.V.MouseDown += new System.Windows.Forms.MouseEventHandler(this.V_MouseDown);
            this.V.MouseMove += new System.Windows.Forms.MouseEventHandler(this.V_MouseMove);
            this.V.MouseUp += new System.Windows.Forms.MouseEventHandler(this.V_MouseUp);
            // 
            // NV
            // 
            this.NV.Cursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.NV.Location = new System.Drawing.Point(5, 5);
            this.NV.Name = "NV";
            this.NV.Size = new System.Drawing.Size(5, 5);
            this.NV.TabIndex = 143;
            this.NV.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NV_MouseDown);
            this.NV.MouseMove += new System.Windows.Forms.MouseEventHandler(this.NV_MouseMove);
            this.NV.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NV_MouseUp);
            // 
            // Minimize
            // 
            this.Minimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Minimize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Minimize.BackgroundImage")));
            this.Minimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Minimize.FlatAppearance.BorderSize = 0;
            this.Minimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Minimize.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Minimize.Location = new System.Drawing.Point(629, 0);
            this.Minimize.Name = "Minimize";
            this.Minimize.Size = new System.Drawing.Size(34, 26);
            this.Minimize.TabIndex = 141;
            this.Minimize.UseVisualStyleBackColor = true;
            this.Minimize.Click += new System.EventHandler(this.Minimize_Click_1);
            this.Minimize.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Minimize_MouseDown);
            this.Minimize.MouseEnter += new System.EventHandler(this.Minimize_MouseEnter);
            this.Minimize.MouseLeave += new System.EventHandler(this.Minimize_MouseLeave);
            // 
            // N
            // 
            this.N.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.N.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.N.Location = new System.Drawing.Point(5, 0);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(627, 5);
            this.N.TabIndex = 142;
            this.N.MouseDown += new System.Windows.Forms.MouseEventHandler(this.N_MouseDown);
            this.N.MouseMove += new System.Windows.Forms.MouseEventHandler(this.N_MouseMove);
            this.N.MouseUp += new System.Windows.Forms.MouseEventHandler(this.N_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(0, 31);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(739, 486);
            this.pictureBox1.TabIndex = 170;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // menuIcon
            // 
            this.menuIcon.Image = ((System.Drawing.Image)(resources.GetObject("menuIcon.Image")));
            this.menuIcon.Location = new System.Drawing.Point(17, 17);
            this.menuIcon.Name = "menuIcon";
            this.menuIcon.Size = new System.Drawing.Size(16, 16);
            this.menuIcon.TabIndex = 171;
            this.menuIcon.TabStop = false;
            this.menuIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuIcon_MouseDown);
            // 
            // Title
            // 
            this.Title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Title.BackColor = System.Drawing.Color.Black;
            this.Title.Enabled = false;
            this.Title.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Title.Location = new System.Drawing.Point(36, 6);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(587, 18);
            this.Title.TabIndex = 172;
            this.Title.Text = "Pseudocode";
            this.Title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // GlowHost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(738, 512);
            this.ControlBox = false;
            this.Controls.Add(this.Close);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.menuIcon);
            this.Controls.Add(this.NE);
            this.Controls.Add(this.Restore);
            this.Controls.Add(this.MinimizeLightPressed);
            this.Controls.Add(this.RestoreLightPressed);
            this.Controls.Add(this.CloseLightPressed);
            this.Controls.Add(this.MinimizeLightHover);
            this.Controls.Add(this.RestoreLightHover);
            this.Controls.Add(this.CloseLightHover);
            this.Controls.Add(this.MinimizeLight);
            this.Controls.Add(this.RestoreLight);
            this.Controls.Add(this.CloseLight);
            this.Controls.Add(this.MinimizeBlackPressed);
            this.Controls.Add(this.RestoreBlackPressed);
            this.Controls.Add(this.CloseBlackPressed);
            this.Controls.Add(this.MinimizeBlackHover);
            this.Controls.Add(this.RestoreBlackHover);
            this.Controls.Add(this.CloseBlackHover);
            this.Controls.Add(this.MinimizeBlack);
            this.Controls.Add(this.RestoreBlack);
            this.Controls.Add(this.CloseBlack);
            this.Controls.Add(this.SE);
            this.Controls.Add(this.SV);
            this.Controls.Add(this.NV);
            this.Controls.Add(this.Minimize);
            this.Controls.Add(this.N);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.S);
            this.Controls.Add(this.E);
            this.Controls.Add(this.V);
            this.Controls.Add(this.NN);
            this.Controls.Add(this.EE);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(700, 481);
            this.Name = "GlowHost";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResizeBegin += new System.EventHandler(this.GlowHost_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.GlowHost_ResizeEnd);
            this.LocationChanged += new System.EventHandler(this.Form1_LocationChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseEnter += new System.EventHandler(this.GlowHost_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.GlowHost_MouseLeave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.Move += new System.EventHandler(this.GlowHost_Move);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button MinimizeLightPressed;
        private System.Windows.Forms.Button RestoreLightPressed;
        private System.Windows.Forms.Button CloseLightPressed;
        private System.Windows.Forms.Button MinimizeLightHover;
        private System.Windows.Forms.Button RestoreLightHover;
        private System.Windows.Forms.Button CloseLightHover;
        private System.Windows.Forms.Button MinimizeBlackPressed;
        private System.Windows.Forms.Button RestoreBlackPressed;
        private System.Windows.Forms.Button CloseBlackPressed;
        private System.Windows.Forms.Button MinimizeBlackHover;
        private System.Windows.Forms.Button RestoreBlackHover;
        private System.Windows.Forms.Button CloseBlackHover;
        private System.Windows.Forms.Panel EE;
        private System.Windows.Forms.Panel NE;
        private System.Windows.Forms.Panel NN;
        private System.Windows.Forms.Panel E;
        private System.Windows.Forms.Panel SE;
        private System.Windows.Forms.Panel S;
        private System.Windows.Forms.Panel SV;
        private System.Windows.Forms.Panel V;
        private System.Windows.Forms.Panel NV;
        private System.Windows.Forms.Panel N;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Button Restore;
        private System.Windows.Forms.PictureBox menuIcon;
        public PowerLabel Title;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Button Close;
        public System.Windows.Forms.Button MinimizeLight;
        public System.Windows.Forms.Button RestoreLight;
        public System.Windows.Forms.Button CloseLight;
        public System.Windows.Forms.Button MinimizeBlack;
        public System.Windows.Forms.Button RestoreBlack;
        public System.Windows.Forms.Button CloseBlack;
        public System.Windows.Forms.Button Minimize;

    }
}

