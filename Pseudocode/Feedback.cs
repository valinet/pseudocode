﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;

namespace Pseudocode
{
    public partial class Feedback : Form
    {
        string NoBrowserInstalled;
        string AppName;
        string Missing;
        string ServiceUnavailable;
        string Success;
        string cale;
        public Feedback(string c)
        {
            InitializeComponent();
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            if (Properties.Settings.Default.UILang == 0)
            {
                AppName = "Pseudocode";
                Missing = "Error\n\nSome mandatory fields are blank or the CAPTCHA verification failed.\n\nPlease review the issues and try again.";
                ServiceUnavailable = "Error\n\nThis service is unavailable right now.\n\nDebug information: ";
                Success = "Success\n\nYour feedback has been successfully sent.";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                AppName = "Pseudocod";
                Missing = "Eroare\n\nCâmpuri obligatorii sunt goale sau protecția AntiSpam nu s-a verificat.\n\nRevizuiți problemele și încercați din nou.";
                ServiceUnavailable = "Eroare\n\nServiciul nu e disponibil în acest moment.\n\nInformații de debug: ";
                Success = "Succes\n\nFeedback-ul dvs. a fost expediat cu succes.";
            }
            if (Properties.Settings.Default.AppName != "")
            {
                AppName = Properties.Settings.Default.AppName;
            }
            string CloseText = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                CloseText = "Close (Alt+F4)";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                CloseText = "Închidere (Alt+F4)";
            }
           // toolTip1.SetToolTip(Close, CloseText);
            cale = c;
        }
        string captcha;
        private void Feedback_Load(object sender, EventArgs e)
        {
            Bitmap bmp = new Bitmap(1240, 60);
            Graphics gImage = Graphics.FromImage(bmp);
            gImage.FillRectangle(Brushes.Wheat, 0, 0, bmp.Width, bmp.Height);

            // Odredjujemo font, boju teksta i deklarisemo DrawString metodu.
            Font CAPTCHAfont = new Font("Chiller", 40);
            Brush cetka = Brushes.Black;
            captcha = GetRandomString();
            gImage.DrawString(captcha, CAPTCHAfont, cetka, 0, 0);

            // Dodajemo linije preko teksta, da je (SPAM)programima teze procitati tekst.
            Pen p = new Pen(Color.Black, 3);
            gImage.DrawLine(p, 230, 15, 10, 15);
            gImage.DrawLine(p, 230, 30, 10, 30);
            gImage.DrawLine(p, 230, 45, 10, 45);


            // Dodjeljujemo ime bmp fajlu.
            //string imeFajla = "VALI";

            // Sacuvavamo bmp file.


            //Prikazujemo bmp file u pictureBox kontroli.
            pictureBox1.Image = bmp;
            //Close.BackColor = Color.FromArgb(199, 80, 80);
            //button1.BackColor = Color.FromArgb(255, 192, 255);
           // button6.BackColor = Color.FromArgb(255, 192, 255);
        }
        public static string GetRandomString()
        {
            string path = Path.GetRandomFileName();
            path = path.Replace(".", ""); // Remove period.
            return path;
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            if (textBox1.Text == "" | textBox2.Text == "" | textBox3.Text == "")
            {
                CasetaDeMesaj(this, Missing, "Feedback", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                if (textBox5.Text == captcha)
                {
                    Application.DoEvents();
                    try
                    {
                        GmailAccount acc = new GmailAccount("valinetmail", "vali2011net", textBox1.Text);
                        acc.SendMessage("vali@valinet.ro", textBox2.Text, textBox3.Text, cale);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        CasetaDeMesaj(this, ServiceUnavailable + ex.Message, "Feedback", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                else
                {
                    CasetaDeMesaj(this, Missing, "Feedback", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Kreiramo bmp file (sliku koja ce se kasnije prikazati u pictureBox1 kontroli).
            Bitmap bmp = new Bitmap(1240, 60);
            Graphics gImage = Graphics.FromImage(bmp);
            gImage.FillRectangle(Brushes.Wheat, 0, 0, bmp.Width, bmp.Height);

            // Odredjujemo font, boju teksta i deklarisemo DrawString metodu.
            Font CAPTCHAfont = new Font("Chiller", 40);
            Brush cetka = Brushes.Black;
            captcha = GetRandomString();
            gImage.DrawString(captcha, CAPTCHAfont, cetka, 0, 0);

            // Dodajemo linije preko teksta, da je (SPAM)programima teze procitati tekst.
            Pen p = new Pen(Color.Black, 3);
            gImage.DrawLine(p, 230, 15, 10, 15);
            gImage.DrawLine(p, 230, 30, 10, 30);
            gImage.DrawLine(p, 230, 45, 10, 45);


            // Dodjeljujemo ime bmp fajlu.
            //string imeFajla = "VALI";

            // Sacuvavamo bmp file.


            //Prikazujemo bmp file u pictureBox kontroli.
            pictureBox1.Image = bmp;
        }


        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /*private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }*/

        private void Close_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

        private void Feedback_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
