﻿namespace Pseudocode
{
    partial class MessageBoxI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBoxI));
            this.panel1 = new System.Windows.Forms.Panel();
            this.OK = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.No = new System.Windows.Forms.Button();
            this.Yes = new System.Windows.Forms.Button();
            this.Mesaj = new System.Windows.Forms.Label();
            this.Titlu = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.OK);
            this.panel1.Controls.Add(this.Cancel);
            this.panel1.Controls.Add(this.No);
            this.panel1.Controls.Add(this.Yes);
            this.panel1.Controls.Add(this.Mesaj);
            this.panel1.Controls.Add(this.Titlu);
            this.panel1.Name = "panel1";
            // 
            // OK
            // 
            resources.ApplyResources(this.OK, "OK");
            this.OK.BackColor = System.Drawing.Color.White;
            this.OK.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.OK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.OK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.OK.ForeColor = System.Drawing.Color.Black;
            this.OK.Name = "OK";
            this.OK.UseVisualStyleBackColor = false;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // Cancel
            // 
            resources.ApplyResources(this.Cancel, "Cancel");
            this.Cancel.BackColor = System.Drawing.Color.Silver;
            this.Cancel.FlatAppearance.BorderSize = 2;
            this.Cancel.Name = "Cancel";
            this.Cancel.UseVisualStyleBackColor = false;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // No
            // 
            resources.ApplyResources(this.No, "No");
            this.No.BackColor = System.Drawing.Color.Silver;
            this.No.FlatAppearance.BorderSize = 2;
            this.No.Name = "No";
            this.No.UseVisualStyleBackColor = false;
            this.No.Click += new System.EventHandler(this.No_Click);
            // 
            // Yes
            // 
            resources.ApplyResources(this.Yes, "Yes");
            this.Yes.BackColor = System.Drawing.Color.White;
            this.Yes.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Yes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.Yes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.Yes.ForeColor = System.Drawing.Color.Black;
            this.Yes.Name = "Yes";
            this.Yes.UseVisualStyleBackColor = false;
            this.Yes.Click += new System.EventHandler(this.Yes_Click);
            // 
            // Mesaj
            // 
            resources.ApplyResources(this.Mesaj, "Mesaj");
            this.Mesaj.ForeColor = System.Drawing.Color.White;
            this.Mesaj.Name = "Mesaj";
            // 
            // Titlu
            // 
            resources.ApplyResources(this.Titlu, "Titlu");
            this.Titlu.ForeColor = System.Drawing.Color.White;
            this.Titlu.Name = "Titlu";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // MessageBoxI
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button1;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MessageBoxI";
            this.Opacity = 0D;
            this.ShowInTaskbar = false;
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(254)))));
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MessageBoxI_FormClosing);
            this.Load += new System.EventHandler(this.MessageBoxI_Load);
            this.Click += new System.EventHandler(this.MessageBoxI_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MessageBoxI_Paint);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button No;
        private System.Windows.Forms.Button Yes;
        private System.Windows.Forms.Label Mesaj;
        private System.Windows.Forms.Label Titlu;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button OK;
        public System.Windows.Forms.Panel panel1;
    }
}