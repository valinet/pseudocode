﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Pseudocode
{
    public partial class MessageBoxI : Form
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int X;
            public int Y;
            public int Width;
            public int Height;
        }
        MainForm mf;
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        IntPtr hWnd;
        FormWindowState state;
        DialogResult final;

        [DllImport("user32.dll")]
        private static extern int ShowWindow(IntPtr hWnd, uint Msg);

        private const uint SW_RESTORE = 0x09;

        public static void RestoreA(Form form)
        {
            if (form.WindowState == FormWindowState.Minimized)
            {
                ShowWindow(form.Handle, SW_RESTORE);
            }
        }
        public MessageBoxI(Form frm, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            InitializeComponent();
            try
            {
                mf = frm as MainForm;
            }
            catch { }
            if (frm.IsMdiChild == true)
            {
                RestoreA(frm.MdiParent);
            }
            else RestoreA(frm);
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            string[] iesire = mesaj.Split('\n');
            try
            {
                Titlu.Text = iesire[0];
                for (int i = 1; i < iesire.Length; i++)
                {
                    Mesaj.Text += iesire[i] + "\n";
                }
            }
            catch { }
            if (butoane == MessageBoxButtons.YesNoCancel)
            {
                Yes.Visible = true;
                No.Visible = true;
                Cancel.Visible = true;
            }
            if (butoane == MessageBoxButtons.YesNo)
            {
                Yes.Visible = true;
                No.Visible = true;
                Cancel.Visible = true;
                Yes.Location = No.Location;
                No.Location = Cancel.Location;
                Cancel.Visible = false;
            }
            if (butoane == MessageBoxButtons.OK)
            {
                OK.Visible = true;
            }
            if (butoane == MessageBoxButtons.OKCancel)
            {
                No.Visible = true;
                OK.Visible = true;
                OK.Location = No.Location;
                No.Visible = false;
                Cancel.Visible = true;
            }
            hWnd = frm.Handle;

            if (Mesaj.Text == "") Titlu.BringToFront();
            this.Size = frm.Size;
            if (frm.WindowState == FormWindowState.Maximized) this.Width += 5;

            this.Owner = frm;

            if (frm.WindowState == FormWindowState.Maximized)
            {
                state = FormWindowState.Maximized;
                this.WindowState = FormWindowState.Maximized;
            }
            if (Environment.OSVersion.Version.Major <= 5 || CheckIfRemoteSession() == true)
            {
                
                this.Opacity = 1;
                
            }
        }
        public static bool CheckIfRemoteSession()
        {
            bool IsRemoteSession = false;
            //Constant used to check for remote sessions
            const int SM_REMOTESESSION = 0x2001;
            //This function will return 0 if the session is local
            int Value = GetSystemMetrics(SM_REMOTESESSION);

            IsRemoteSession = (Value != 0);

            return IsRemoteSession;
        }

        [DllImport("user32.dll", EntryPoint = ("GetSystemMetrics"))]
        private static extern int GetSystemMetrics(int nIndex);
        private void button1_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Yes;
            this.Close();
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.C))
            {
                Clipboard.SetText(Titlu.Text + "\r\n---\r\n" + Mesaj.Text);
                return true;
            }
            if (keyData == (Keys.F1))
            {
                if (System.IO.Directory.Exists(Application.StartupPath + "\\com.valinet.pseudocode.help"))
                {
                    try
                    {
                        if (mf.hp == null) mf.hp = new Help();
                        mf.hp.Show();
                        if (mf.WindowState != FormWindowState.Maximized)
                        {
                            mf.hp.Location = mf.Location;
                            mf.hp.Size = mf.Size;
                        }
                        mf.hp.WindowState = mf.WindowState;
                    }
                    catch { }
                }
                else
                {
                    HelpOverview ho = new HelpOverview();
                    ho.ShowDialog();
                }
                return true;
            }
            return false;
        }
        private void No_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.No;
            this.Close();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void MessageBoxI_Load(object sender, EventArgs e)
        {

        }

        private void Yes_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.Yes;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!(Environment.OSVersion.Version.Major <= 5 || CheckIfRemoteSession() == true))
            {
                if (a != 1) this.Opacity += 0.1;
                if (a == 1)
                {
                    this.Opacity -= 0.1;
                }
                if (a == 1 & this.Opacity <= 0.1)
                {
                    a = 2;
                    this.Close();
                }
            }
            //this.Location = new Point(frm.Location.X, frm.Location.Y);
            RECT rect;
            GetWindowRect(hWnd, out rect);
            if (rect.X == -32000)
            {
                // the game is minimized
                this.WindowState = FormWindowState.Minimized;
            }
            else
            {
                bool ok = false;
                if (state == FormWindowState.Maximized) ok = true;
                this.WindowState = FormWindowState.Normal;
                if (ok == true) this.Location = new Point(rect.X + 5, rect.Y - 7);
                else this.Location = new Point(rect.X, rect.Y);
            }
        }

        private void MessageBoxI_Paint(object sender, PaintEventArgs e)
        {
            if (Environment.OSVersion.Version.Major <= 5)
            {
                
                this.TransparencyKey = this.BackColor;
            }
            else
            {
                var hb = new HatchBrush(HatchStyle.Percent50, this.TransparencyKey);

                e.Graphics.FillRectangle(hb, this.DisplayRectangle);
            }
        }

        private void MessageBoxI_Click(object sender, EventArgs e)
        {
            if (Cancel.Visible == true) final = System.Windows.Forms.DialogResult.Cancel;
            if (No.Visible == true && Cancel.Visible == false) final = System.Windows.Forms.DialogResult.No;
            if (OK.Visible == true && Cancel.Visible == false) final = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
        int a = 0;
        private void MessageBoxI_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Environment.OSVersion.Version.Major <= 5 || CheckIfRemoteSession() == true)
            {
                a = 2;
            }
            if (a == 2)
            {
               // this.Visible = false;

                DialogResult = final;
            }
            else
            {
                a = 1;
                e.Cancel = true;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (Cancel.Visible == true) final = System.Windows.Forms.DialogResult.Cancel;
            if (No.Visible == true && Cancel.Visible == false) final = System.Windows.Forms.DialogResult.No;
            if (OK.Visible == true && Cancel.Visible == false) final = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}


