﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class DependenciesInstalled : Form 
    {
        public DependenciesInstalled()
        {
            InitializeComponent();
        }

        private void DependenciesInstalled_Load(object sender, EventArgs e)
        {
            //Close.BackColor = Color.FromArgb(199, 80, 80);
            pictureBox1.Image = System.Drawing.SystemIcons.Information.ToBitmap();
            System.Media.SystemSounds.Asterisk.Play();
        }

        private void DependenciesInstalled_FormClosing(object sender, FormClosingEventArgs e)
        {

            Properties.Settings.Default.WasWLMPPVShown = checkBox1.Checked;
            Properties.Settings.Default.Save();
            this.Visible = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

    }
}
