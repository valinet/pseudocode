
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms.Design;
using System.Drawing.Design;
using System.ComponentModel;
using System.Windows.Forms;

public class CustomAppearancePropertyEditor : System.Drawing.Design.UITypeEditor
{

	private frmAppearanceEditor _appearanceEditor;
	protected IWindowsFormsEditorService IEditorService;
	private Control m_EditControl;

	private bool m_EscapePressed;
	public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
	{
		return UITypeEditorEditStyle.Modal;
	}

	public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
	{
		try {

			if (context != null && provider != null) {
				//Uses the IWindowsFormsEditorService to display a modal dialog form
				IEditorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

				if (IEditorService != null) {
					string PropName = context.PropertyDescriptor.Name;
					m_EditControl = this.GetEditControl(PropName, value);
					if (m_EditControl != null) {
						IEditorService.ShowDialog((Form)m_EditControl);

						//Notify that our control has changed; otherwise changes are not stored
						context.OnComponentChanged();

						return this.GetEditedValue(m_EditControl, PropName, value);
					}
				}
			}
		} catch (Exception ex) {
		}
		return base.EditValue(context, provider, value);
	}

	private System.Windows.Forms.Control GetEditControl(string PropertyName, object CurrentValue)
	{
		AppearanceControl.AppearanceProperties ap = CurrentValue as AppearanceControl.AppearanceProperties;
		if (ap != null) {
			_appearanceEditor = new frmAppearanceEditor(ap);
			return _appearanceEditor;
		} else {
			return null;
		}
	}

	private object GetEditedValue(System.Windows.Forms.Control EditControl, string PropertyName, object OldValue)
	{
		if (_appearanceEditor == null || _appearanceEditor.DialogResult == DialogResult.Cancel) {
			return OldValue;
		} else {
			return _appearanceEditor.CustomAppearance;
		}
	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
