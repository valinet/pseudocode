
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms.Design;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Xml.Serialization;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Drawing;

public class AppearanceControl : Component
{

	public event EventHandler AppearanceChanged;

    private ToolStripProfessionalRenderer customRenderer = null;
	private ToolStripProfessionalRenderer office2007Renderer = null;
	private ToolStripProfessionalRenderer blueRenderer = null;
	private ToolStripProfessionalRenderer silverRenderer = null;
	private ToolStripProfessionalRenderer oliveRenderer = null;
	private ToolStripProfessionalRenderer xpRenderer = null;
	private ToolStripProfessionalRenderer classicRenderer = null;

	private ToolStripProfessionalRenderer blackRenderer = null;
	public enum enumPresetStyles
	{
		Custom = 0,
		Office2007 = 1,
		Office2003Blue = 2,
		Office2003Silver = 3,
		Office2003Olive = 4,
		OfficeXP = 5,
		OfficeClassic = 6,
		Office2007Black = 7
	}
    public class MyRenderer : ToolStripProfessionalRenderer
    {
        public MyRenderer(ProfessionalColorTable professionalColorTable)
            : base(professionalColorTable)
        {
            
        }
        protected override void OnRenderArrow(ToolStripArrowRenderEventArgs e)
        {
            if (Pseudocode.Properties.Settings.Default.UITheme == 1) e.ArrowColor = Color.Black;
            else e.ArrowColor = Color.White;
            base.OnRenderArrow(e);
        }

    }
	public AppearanceControl()
	{
		customRenderer = new MyRenderer(new CustomColorTable(this));
        
		office2007Renderer = new ToolStripProfessionalRenderer(new Office2007ColorTable());
		blueRenderer = new ToolStripProfessionalRenderer(new Office2003BlueColorTable());
		silverRenderer = new ToolStripProfessionalRenderer(new Office2003SilverColorTable());
		oliveRenderer = new ToolStripProfessionalRenderer(new Office2003OliveColorTable());
		xpRenderer = new ToolStripProfessionalRenderer(new OfficeXPColorTable());
		classicRenderer = new ToolStripProfessionalRenderer(new OfficeClassicColorTable());
		blackRenderer = new ToolStripProfessionalRenderer(new Office2007BlackColorTable());

		_Renderer = customRenderer;
		_CustomAppearance = new AppearanceProperties(this);
	}

	private enumPresetStyles _Preset;
	[Category("Appearance")]
	public enumPresetStyles Preset {
		get { return _Preset; }
		set {
			_Preset = value;

			switch (value) {
				case enumPresetStyles.Custom:
					this.Renderer = customRenderer;
					break;
				case enumPresetStyles.Office2003Blue:
					this.Renderer = blueRenderer;
					break;
				case enumPresetStyles.Office2003Olive:
					this.Renderer = oliveRenderer;
					break;
				case enumPresetStyles.Office2003Silver:
					this.Renderer = silverRenderer;
					break;
				case enumPresetStyles.Office2007:
					this.Renderer = office2007Renderer;
					break;
				case enumPresetStyles.OfficeClassic:
					this.Renderer = classicRenderer;
					break;
				case enumPresetStyles.OfficeXP:
					this.Renderer = xpRenderer;
					break;
				case enumPresetStyles.Office2007Black:
					this.Renderer = blackRenderer;
					break;
			}

			this.OnAppearanceChanged(EventArgs.Empty);
		}
	}

	private ToolStripProfessionalRenderer _Renderer = null;
	[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
	[Browsable(false)]
	public ToolStripProfessionalRenderer Renderer {
		get { return _Renderer; }
		set { _Renderer = value; }
	}

	private AppearanceProperties _CustomAppearance;
	[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
	[Category("Appearance")]
	[Editor(typeof(CustomAppearancePropertyEditor), typeof(UITypeEditor))]
	public AppearanceProperties CustomAppearance {
		get { return _CustomAppearance; }
		set { _CustomAppearance = value; }
	}

	[Serializable()]
	public class AppearanceProperties
	{

		//Parameterless ctor required for serialization
		public AppearanceProperties()
		{
		}

		public void SetAppearanceControl(AppearanceControl ap)
		{
			_ButtonAppearance = new ButtonAppearanceProperties(ap);
			_ButtonAppearance.SelectedAppearance.SetAppearanceControl(ap);
			_ButtonAppearance.PressedAppearance.SetAppearanceControl(ap);
			_ButtonAppearance.CheckedAppearance.SetAppearanceControl(ap);
			_GripAppearance.SetAppearanceControl(ap);
			_ImageMarginAppearance = new ImageMarginAppearanceProperties(ap);
			_ImageMarginAppearance.Normal.SetAppearanceControl(ap);
			_ImageMarginAppearance.Revealed.SetAppearanceControl(ap);
			_MenuStripAppearance.SetAppearanceControl(ap);
			_MenuItemAppearance.SetAppearanceControl(ap);
			_RaftingContainerAppearance.SetAppearanceControl(ap);
			_SeparatorAppearance.SetAppearanceControl(ap);
			_StatusStripAppearance.SetAppearanceControl(ap);
			_ToolStripAppearance.SetAppearanceControl(ap);
			_OverflowButtonAppearance.SetAppearanceControl(ap);
		}

		public AppearanceProperties(AppearanceControl appearanceControl)
		{
			_ButtonAppearance = new ButtonAppearanceProperties(appearanceControl);
			_GripAppearance = new GripAppearanceProperties(appearanceControl);
			_ImageMarginAppearance = new ImageMarginAppearanceProperties(appearanceControl);
			_MenuStripAppearance = new MenustripAppearanceProperties(appearanceControl);
			_MenuItemAppearance = new MenuItemAppearanceProperties(appearanceControl);
			_RaftingContainerAppearance = new RaftingContainerAppearanceProperties(appearanceControl);
			_SeparatorAppearance = new SeparatorAppearanceProperties(appearanceControl);
			_StatusStripAppearance = new StatusStripAppearanceProperties(appearanceControl);
			_ToolStripAppearance = new ToolstripAppearanceProperties(appearanceControl);
			_OverflowButtonAppearance = new OverflowButtonAppearanceProperties(appearanceControl);
		}

		private ButtonAppearanceProperties _ButtonAppearance;
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Appearance")]
		public ButtonAppearanceProperties ButtonAppearance {
			get { return _ButtonAppearance; }
			set { _ButtonAppearance = value; }
		}

		private GripAppearanceProperties _GripAppearance;
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Appearance")]
		public GripAppearanceProperties GripAppearance {
			get { return _GripAppearance; }
			set { _GripAppearance = value; }
		}

		private ImageMarginAppearanceProperties _ImageMarginAppearance;
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Appearance")]
		public ImageMarginAppearanceProperties ImageMarginAppearance {
			get { return _ImageMarginAppearance; }
			set { _ImageMarginAppearance = value; }
		}

		private MenustripAppearanceProperties _MenuStripAppearance;
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Appearance")]
		public MenustripAppearanceProperties MenuStripAppearance {
			get { return _MenuStripAppearance; }
			set { _MenuStripAppearance = value; }
		}

		private MenuItemAppearanceProperties _MenuItemAppearance;
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Appearance")]
		public MenuItemAppearanceProperties MenuItemAppearance {
			get { return _MenuItemAppearance; }
			set { _MenuItemAppearance = value; }
		}

		private RaftingContainerAppearanceProperties _RaftingContainerAppearance;
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Appearance")]
		public RaftingContainerAppearanceProperties RaftingContainerAppearance {
			get { return _RaftingContainerAppearance; }
			set { _RaftingContainerAppearance = value; }
		}

		private SeparatorAppearanceProperties _SeparatorAppearance;
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Appearance")]
		public SeparatorAppearanceProperties SeparatorAppearance {
			get { return _SeparatorAppearance; }
			set { _SeparatorAppearance = value; }
		}

		private StatusStripAppearanceProperties _StatusStripAppearance;
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Appearance")]
		public StatusStripAppearanceProperties StatusStripAppearance {
			get { return _StatusStripAppearance; }
			set { _StatusStripAppearance = value; }
		}

		private ToolstripAppearanceProperties _ToolStripAppearance;
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Appearance")]
		public ToolstripAppearanceProperties ToolStripAppearance {
			get { return _ToolStripAppearance; }
			set { _ToolStripAppearance = value; }
		}

		private OverflowButtonAppearanceProperties _OverflowButtonAppearance;
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Appearance")]
		public OverflowButtonAppearanceProperties OverflowButtonAppearance {
			get { return _OverflowButtonAppearance; }
			set { _OverflowButtonAppearance = value; }
		}

		[Serializable()]
		public class ButtonAppearanceProperties
		{
			//Parameterless ctor required for serialization
			public ButtonAppearanceProperties()
			{
			}

			public ButtonAppearanceProperties(AppearanceControl appearanceControl)
			{
				_SelectedAppearance = new SelectedButtonAppearanceProperties(appearanceControl);
				_CheckedAppearance = new CheckedButtonAppearanceProperties(appearanceControl);
				_PressedAppearance = new PressedButtonAppearanceProperties(appearanceControl);
			}

			private SelectedButtonAppearanceProperties _SelectedAppearance;
			[TypeConverter(typeof(ExpandableObjectConverter))]
			[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
			public SelectedButtonAppearanceProperties SelectedAppearance {
				get { return _SelectedAppearance; }
				set { _SelectedAppearance = value; }
			}

			private CheckedButtonAppearanceProperties _CheckedAppearance;
			[TypeConverter(typeof(ExpandableObjectConverter))]
			[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
			public CheckedButtonAppearanceProperties CheckedAppearance {
				get { return _CheckedAppearance; }
				set { _CheckedAppearance = value; }
			}

			private PressedButtonAppearanceProperties _PressedAppearance;
			[TypeConverter(typeof(ExpandableObjectConverter))]
			[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
			public PressedButtonAppearanceProperties PressedAppearance {
				get { return _PressedAppearance; }
				set { _PressedAppearance = value; }
			}

			public override string ToString()
			{
				return string.Empty;
			}
		}

		[Serializable()]
		public class ImageMarginAppearanceProperties
		{

			//Parameterless ctor required for serialization
			public ImageMarginAppearanceProperties()
			{
			}

			public ImageMarginAppearanceProperties(AppearanceControl appearanceControl)
			{
				_Normal = new ImageMarginNormalAppearanceProperties(appearanceControl);
				_Revealed = new ImageMarginRevealedAppearanceProperties(appearanceControl);
			}

			private ImageMarginNormalAppearanceProperties _Normal;
			[TypeConverter(typeof(ExpandableObjectConverter))]
			[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
			public ImageMarginNormalAppearanceProperties Normal {
				get { return _Normal; }
				set { _Normal = value; }
			}

			private ImageMarginRevealedAppearanceProperties _Revealed;
			[TypeConverter(typeof(ExpandableObjectConverter))]
			[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
			public ImageMarginRevealedAppearanceProperties Revealed {
				get { return _Revealed; }
				set { _Revealed = value; }
			}

			public override string ToString()
			{
				return string.Empty;
			}
		}

		#region " Property Group Classes "

		[Serializable()]
		public class SelectedButtonAppearanceProperties
		{

			public SelectedButtonAppearanceProperties()
			{
			}

			private AppearanceControl ap;
			public SelectedButtonAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			private Color _GradientBegin = Color.FromArgb(255, 255, 222);
			[DefaultValue(typeof(Color), "255, 255, 222")]
			[XmlIgnore()]
			public Color GradientBegin {
				get { return _GradientBegin; }
				set {
					_GradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientMiddle = Color.FromArgb(255, 225, 172);
			[DefaultValue(typeof(Color), "255, 225, 172")]
			[XmlIgnore()]
			public Color GradientMiddle {
				get { return _GradientMiddle; }
				set {
					_GradientMiddle = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientEnd = Color.FromArgb(255, 203, 136);
			[DefaultValue(typeof(Color), "255, 203, 136")]
			[XmlIgnore()]
			public Color GradientEnd {
				get { return _GradientEnd; }
				set {
					_GradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _Highlight = Color.FromArgb(196, 208, 229);
			[DefaultValue(typeof(Color), "196, 208, 229")]
			[XmlIgnore()]
			public Color Highlight {
				get { return _Highlight; }
				set {
					_Highlight = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _BorderHighlight = Color.FromArgb(0, 0, 128);
			[DefaultValue(typeof(Color), "0, 0, 128")]
			[XmlIgnore()]
			public Color BorderHighlight {
				get { return _BorderHighlight; }
				set {
					_BorderHighlight = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _Border = Color.FromArgb(0, 0, 128);
			[DefaultValue(typeof(Color), "0, 0, 128")]
			[XmlIgnore()]
			public Color Border {
				get { return _Border; }
				set {
					_Border = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}
			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intGradientBegin {
				get { return this.GradientBegin.ToArgb(); }
				set { this.GradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientMiddle {
				get { return this.GradientMiddle.ToArgb(); }
				set { this.GradientMiddle = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientEnd {
				get { return this.GradientEnd.ToArgb(); }
				set { this.GradientEnd = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intHighlight {
				get { return this.Highlight.ToArgb(); }
				set { this.Highlight = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intBorderHighlight {
				get { return this.BorderHighlight.ToArgb(); }
				set { this.BorderHighlight = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intBorder {
				get { return this.Border.ToArgb(); }
				set { this.Border = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class PressedButtonAppearanceProperties
		{
			public PressedButtonAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public PressedButtonAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			private Color _GradientBegin = Color.FromArgb(254, 128, 62);
			[DefaultValue(typeof(Color), "254, 128, 62")]
			[XmlIgnore()]
			public Color GradientBegin {
				get { return _GradientBegin; }
				set {
					_GradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientMiddle = Color.FromArgb(255, 177, 109);
			[DefaultValue(typeof(Color), "255, 177, 109")]
			[XmlIgnore()]
			public Color GradientMiddle {
				get { return _GradientMiddle; }
				set {
					_GradientMiddle = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientEnd = Color.FromArgb(255, 223, 154);
			[DefaultValue(typeof(Color), "255, 223, 154")]
			[XmlIgnore()]
			public Color GradientEnd {
				get { return _GradientEnd; }
				set {
					_GradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _Highlight = Color.FromArgb(152, 173, 210);
			[DefaultValue(typeof(Color), "152, 173, 210")]
			[XmlIgnore()]
			public Color Highlight {
				get { return _Highlight; }
				set {
					_Highlight = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _BorderHighlight = Color.FromArgb(51, 94, 168);
			[DefaultValue(typeof(Color), "51, 94, 168")]
			[XmlIgnore()]
			public Color BorderHighlight {
				get { return _BorderHighlight; }
				set {
					_BorderHighlight = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _Border = Color.FromArgb(0, 0, 128);
			[DefaultValue(typeof(Color), "0, 0, 128")]
			[XmlIgnore()]
			public Color Border {
				get { return _Border; }
				set {
					_Border = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}
			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intGradientBegin {
				get { return this.GradientBegin.ToArgb(); }
				set { this.GradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientMiddle {
				get { return this.GradientMiddle.ToArgb(); }
				set { this.GradientMiddle = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientEnd {
				get { return this.GradientEnd.ToArgb(); }
				set { this.GradientEnd = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intHighlight {
				get { return this.Highlight.ToArgb(); }
				set { this.Highlight = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intBorderHighlight {
				get { return this.BorderHighlight.ToArgb(); }
				set { this.BorderHighlight = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intBorder {
				get { return this.Border.ToArgb(); }
				set { this.Border = Color.FromArgb(value); }
			}


		}

		[Serializable()]
		public class CheckedButtonAppearanceProperties
		{
			public CheckedButtonAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public CheckedButtonAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}
			private Color _GradientBegin = Color.FromArgb(255, 223, 154);
			[DefaultValue(typeof(Color), "255, 223, 154")]
			[XmlIgnore()]
			public Color GradientBegin {
				get { return _GradientBegin; }
				set {
					_GradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientMiddle = Color.FromArgb(255, 195, 116);
			[DefaultValue(typeof(Color), "255, 195, 116")]
			[XmlIgnore()]
			public Color GradientMiddle {
				get { return _GradientMiddle; }
				set {
					_GradientMiddle = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientEnd = Color.FromArgb(255, 166, 76);
			[DefaultValue(typeof(Color), "255, 166, 76")]
			[XmlIgnore()]
			public Color GradientEnd {
				get { return _GradientEnd; }
				set {
					_GradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _Highlight = Color.FromArgb(196, 208, 229);
			[DefaultValue(typeof(Color), "196, 208, 229")]
			[XmlIgnore()]
			public Color Highlight {
				get { return _Highlight; }
				set {
					_Highlight = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _BorderHighlight = Color.FromArgb(51, 94, 168);
			[DefaultValue(typeof(Color), "51, 94, 168")]
			[XmlIgnore()]
			public Color BorderHighlight {
				get { return _BorderHighlight; }
				set {
					_BorderHighlight = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _Background = Color.FromArgb(255, 192, 111);
			[DefaultValue(typeof(Color), "255, 192, 111")]
			[XmlIgnore()]
			public Color Background {
				get { return _Background; }
				set {
					_Background = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _SelectedBackground = Color.FromArgb(254, 128, 62);
			[DefaultValue(typeof(Color), "254, 128, 62")]
			[XmlIgnore()]
			public Color SelectedBackground {
				get { return _SelectedBackground; }
				set {
					_SelectedBackground = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _PressedBackground = Color.FromArgb(254, 128, 62);
			[DefaultValue(typeof(Color), "254, 128, 62")]
			[XmlIgnore()]
			public Color PressedBackground {
				get { return _PressedBackground; }
				set {
					_PressedBackground = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intGradientBegin {
				get { return this.GradientBegin.ToArgb(); }
				set { this.GradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientMiddle {
				get { return this.GradientMiddle.ToArgb(); }
				set { this.GradientMiddle = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientEnd {
				get { return this.GradientEnd.ToArgb(); }
				set { this.GradientEnd = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intHighlight {
				get { return this.Highlight.ToArgb(); }
				set { this.Highlight = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intBorderHighlight {
				get { return this.BorderHighlight.ToArgb(); }
				set { this.BorderHighlight = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intBackground {
				get { return this.Background.ToArgb(); }
				set { this.Background = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intSelectedBackground {
				get { return this.SelectedBackground.ToArgb(); }
				set { this.SelectedBackground = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intPressedBackground {
				get { return this.PressedBackground.ToArgb(); }
				set { this.PressedBackground = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class GripAppearanceProperties
		{
			public GripAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public GripAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}
			private Color _Dark = Color.FromArgb(39, 65, 118);
			[DefaultValue(typeof(Color), "39, 65, 118")]
			[XmlIgnore()]
			public Color Dark {
				get { return _Dark; }
				set {
					_Dark = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _Light = Color.FromArgb(255, 255, 255);
			[DefaultValue(typeof(Color), "255, 255, 255")]
			[XmlIgnore()]
			public Color Light {
				get { return _Light; }
				set {
					_Light = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intDark {
				get { return this.Dark.ToArgb(); }
				set { this.Dark = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intLight {
				get { return this.Light.ToArgb(); }
				set { this.Light = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class MenustripAppearanceProperties
		{
			public MenustripAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public MenustripAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}
			private Color _Border = Color.FromArgb(0, 45, 150);
			[DefaultValue(typeof(Color), "0, 45, 150")]
			[XmlIgnore()]
			public Color Border {
				get { return _Border; }
				set {
					_Border = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientBegin = Color.FromArgb(158, 190, 245);
			[DefaultValue(typeof(Color), "158, 190, 245")]
			[XmlIgnore()]
			public Color GradientBegin {
				get { return _GradientBegin; }
				set {
					_GradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientEnd = Color.FromArgb(196, 218, 250);
			[DefaultValue(typeof(Color), "196, 218, 250")]
			[XmlIgnore()]
			public Color GradientEnd {
				get { return _GradientEnd; }
				set {
					_GradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intBorder {
				get { return this.Border.ToArgb(); }
				set { this.Border = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientBegin {
				get { return this.GradientBegin.ToArgb(); }
				set { this.GradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientEnd {
				get { return this.GradientEnd.ToArgb(); }
				set { this.GradientEnd = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class MenuItemAppearanceProperties
		{
			public MenuItemAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public MenuItemAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}
			private Color _Selected = Color.FromArgb(255, 238, 194);
			[DefaultValue(typeof(Color), "255, 238, 194")]
			[XmlIgnore()]
			public Color Selected {
				get { return _Selected; }
				set {
					_Selected = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _Border = Color.FromArgb(0, 0, 128);
			[DefaultValue(typeof(Color), "0, 0, 128")]
			[XmlIgnore()]
			public Color Border {
				get { return _Border; }
				set {
					_Border = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _SelectedGradientBegin = Color.FromArgb(255, 255, 222);
			[DefaultValue(typeof(Color), "255, 255, 222")]
			[XmlIgnore()]
			public Color SelectedGradientBegin {
				get { return _SelectedGradientBegin; }
				set {
					_SelectedGradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _SelectedGradientEnd = Color.FromArgb(255, 203, 136);
			[DefaultValue(typeof(Color), "255, 203, 136")]
			[XmlIgnore()]
			public Color SelectedGradientEnd {
				get { return _SelectedGradientEnd; }
				set {
					_SelectedGradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _PressedGradientBegin = Color.FromArgb(227, 239, 255);
			[DefaultValue(typeof(Color), "227, 239, 255")]
			[XmlIgnore()]
			public Color PressedGradientBegin {
				get { return _PressedGradientBegin; }
				set {
					_PressedGradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _PressedGradientMiddle = Color.FromArgb(161, 197, 249);
			[DefaultValue(typeof(Color), "161, 197, 249")]
			[XmlIgnore()]
			public Color PressedGradientMiddle {
				get { return _PressedGradientMiddle; }
				set {
					_PressedGradientMiddle = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _PressedGradientEnd = Color.FromArgb(123, 164, 224);
			[DefaultValue(typeof(Color), "123, 164, 224")]
			[XmlIgnore()]
			public Color PressedGradientEnd {
				get { return _PressedGradientEnd; }
				set {
					_PressedGradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intSelected {
				get { return this.Selected.ToArgb(); }
				set { this.Selected = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intBorder {
				get { return this.Border.ToArgb(); }
				set { this.Border = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intSelectedGradientBegin {
				get { return this.SelectedGradientBegin.ToArgb(); }
				set { this.SelectedGradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intSelectedGradientEnd {
				get { return this.SelectedGradientEnd.ToArgb(); }
				set { this.SelectedGradientEnd = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intPressedGradientBegin {
				get { return this.PressedGradientBegin.ToArgb(); }
				set { this.PressedGradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intPressedGradientMiddle {
				get { return this.PressedGradientMiddle.ToArgb(); }
				set { this.PressedGradientMiddle = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intPressedGradientEnd {
				get { return this.PressedGradientEnd.ToArgb(); }
				set { this.PressedGradientEnd = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class RaftingContainerAppearanceProperties
		{
			public RaftingContainerAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public RaftingContainerAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}
			private Color _GradientBegin = Color.FromArgb(158, 190, 245);
			[DefaultValue(typeof(Color), "158, 190, 245")]
			[XmlIgnore()]
			public Color GradientBegin {
				get { return _GradientBegin; }
				set {
					_GradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientEnd = Color.FromArgb(196, 218, 250);
			[DefaultValue(typeof(Color), "196, 218, 250")]
			[XmlIgnore()]
			public Color GradientEnd {
				get { return _GradientEnd; }
				set {
					_GradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intGradientBegin {
				get { return this.GradientBegin.ToArgb(); }
				set { this.GradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientEnd {
				get { return this.GradientEnd.ToArgb(); }
				set { this.GradientEnd = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class StatusStripAppearanceProperties
		{
			public StatusStripAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public StatusStripAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}
			private Color _GradientBegin = Color.FromArgb(158, 190, 245);
			[DefaultValue(typeof(Color), "158, 190, 245")]
			[XmlIgnore()]
			public Color GradientBegin {
				get { return _GradientBegin; }
				set {
					_GradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientEnd = Color.FromArgb(196, 218, 250);
			[DefaultValue(typeof(Color), "196, 218, 250")]
			[XmlIgnore()]
			public Color GradientEnd {
				get { return _GradientEnd; }
				set {
					_GradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intGradientBegin {
				get { return this.GradientBegin.ToArgb(); }
				set { this.GradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientEnd {
				get { return this.GradientEnd.ToArgb(); }
				set { this.GradientEnd = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class SeparatorAppearanceProperties
		{
			public SeparatorAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public SeparatorAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}
			private Color _Dark = Color.FromArgb(106, 140, 203);
			[DefaultValue(typeof(Color), "106, 140, 203")]
			[XmlIgnore()]
			public Color Dark {
				get { return _Dark; }
				set {
					_Dark = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _Light = Color.FromArgb(241, 249, 255);
			[DefaultValue(typeof(Color), "241, 249, 255")]
			[XmlIgnore()]
			public Color Light {
				get { return _Light; }
				set {
					_Light = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intDark {
				get { return this.Dark.ToArgb(); }
				set { this.Dark = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intLight {
				get { return this.Light.ToArgb(); }
				set { this.Light = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class ToolstripAppearanceProperties
		{

			public ToolstripAppearanceProperties()
			{
			}

			private AppearanceControl ap;
			public ToolstripAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			private Color _GradientBegin = Color.FromArgb(227, 239, 255);
			[DefaultValue(typeof(Color), "227, 239, 255")]
			[XmlIgnore()]
			public Color GradientBegin {
				get { return _GradientBegin; }
				set {
					_GradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientMiddle = Color.FromArgb(203, 225, 252);
			[DefaultValue(typeof(Color), "203, 225, 252")]
			[XmlIgnore()]
			public Color GradientMiddle {
				get { return _GradientMiddle; }
				set {
					_GradientMiddle = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientEnd = Color.FromArgb(123, 164, 224);
			[DefaultValue(typeof(Color), "123, 164, 224")]
			[XmlIgnore()]
			public Color GradientEnd {
				get { return _GradientEnd; }
				set {
					_GradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _Border = Color.FromArgb(59, 97, 156);
			[DefaultValue(typeof(Color), "59, 97, 156")]
			[XmlIgnore()]
			public Color Border {
				get { return _Border; }
				set {
					_Border = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _DropDownBackground = Color.FromArgb(246, 246, 246);
			[DefaultValue(typeof(Color), "246, 246, 246")]
			[XmlIgnore()]
			public Color DropDownBackground {
				get { return _DropDownBackground; }
				set {
					_DropDownBackground = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _ContentPanelGradientBegin = Color.FromArgb(158, 190, 245);
			[DefaultValue(typeof(Color), "158, 190, 245")]
			[XmlIgnore()]
			public Color ContentPanelGradientBegin {
				get { return _ContentPanelGradientBegin; }
				set {
					_ContentPanelGradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _ContentPanelGradientEnd = Color.FromArgb(196, 218, 250);
			[DefaultValue(typeof(Color), "196, 218, 250")]
			[XmlIgnore()]
			public Color ContentPanelGradientEnd {
				get { return _ContentPanelGradientEnd; }
				set {
					_ContentPanelGradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _PanelGradientBegin = Color.FromArgb(158, 190, 245);
			[DefaultValue(typeof(Color), "158, 190, 245")]
			[XmlIgnore()]
			public Color PanelGradientBegin {
				get { return _PanelGradientBegin; }
				set {
					_PanelGradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _PanelGradientEnd = Color.FromArgb(196, 218, 250);
			[DefaultValue(typeof(Color), "196, 218, 250")]
			[XmlIgnore()]
			public Color PanelGradientEnd {
				get { return _PanelGradientEnd; }
				set {
					_PanelGradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intGradientBegin {
				get { return this.GradientBegin.ToArgb(); }
				set { this.GradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientMiddle {
				get { return this.GradientMiddle.ToArgb(); }
				set { this.GradientMiddle = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientEnd {
				get { return this.GradientEnd.ToArgb(); }
				set { this.GradientEnd = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intBorder {
				get { return this.Border.ToArgb(); }
				set { this.Border = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intDropDownBackground {
				get { return this.DropDownBackground.ToArgb(); }
				set { this.DropDownBackground = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intContentPanelGradientBegin {
				get { return this.ContentPanelGradientBegin.ToArgb(); }
				set { this.ContentPanelGradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intContentPanelGradientEnd {
				get { return this.ContentPanelGradientEnd.ToArgb(); }
				set { this.ContentPanelGradientEnd = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intPanelGradientBegin {
				get { return this.PanelGradientBegin.ToArgb(); }
				set { this.PanelGradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intPanelGradientEnd {
				get { return this.PanelGradientEnd.ToArgb(); }
				set { this.PanelGradientEnd = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class OverflowButtonAppearanceProperties
		{
			public OverflowButtonAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public OverflowButtonAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}
			private Color _GradientBegin = Color.FromArgb(127, 177, 250);
			[DefaultValue(typeof(Color), "127, 177, 250")]
			[XmlIgnore()]
			public Color GradientBegin {
				get { return _GradientBegin; }
				set {
					_GradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientMiddle = Color.FromArgb(82, 127, 208);
			[DefaultValue(typeof(Color), "82, 127, 208")]
			[XmlIgnore()]
			public Color GradientMiddle {
				get { return _GradientMiddle; }
				set {
					_GradientMiddle = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientEnd = Color.FromArgb(0, 53, 145);
			[DefaultValue(typeof(Color), "0, 53, 145")]
			[XmlIgnore()]
			public Color GradientEnd {
				get { return _GradientEnd; }
				set {
					_GradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intGradientBegin {
				get { return this.GradientBegin.ToArgb(); }
				set { this.GradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientMiddle {
				get { return this.GradientMiddle.ToArgb(); }
				set { this.GradientMiddle = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientEnd {
				get { return this.GradientEnd.ToArgb(); }
				set { this.GradientEnd = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class ImageMarginNormalAppearanceProperties
		{
			public ImageMarginNormalAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public ImageMarginNormalAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}
			private Color _GradientBegin = Color.FromArgb(227, 239, 255);
			[DefaultValue(typeof(Color), "227, 239, 255")]
			[XmlIgnore()]
			public Color GradientBegin {
				get { return _GradientBegin; }
				set {
					_GradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientMiddle = Color.FromArgb(203, 225, 252);
			[DefaultValue(typeof(Color), "203, 225, 252")]
			[XmlIgnore()]
			public Color GradientMiddle {
				get { return _GradientMiddle; }
				set {
					_GradientMiddle = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientEnd = Color.FromArgb(123, 164, 224);
			[DefaultValue(typeof(Color), "123, 164, 224")]
			[XmlIgnore()]
			public Color GradientEnd {
				get { return _GradientEnd; }
				set {
					_GradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intGradientBegin {
				get { return this.GradientBegin.ToArgb(); }
				set { this.GradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientMiddle {
				get { return this.GradientMiddle.ToArgb(); }
				set { this.GradientMiddle = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientEnd {
				get { return this.GradientEnd.ToArgb(); }
				set { this.GradientEnd = Color.FromArgb(value); }
			}

		}

		[Serializable()]
		public class ImageMarginRevealedAppearanceProperties
		{
			public ImageMarginRevealedAppearanceProperties()
			{
			}
			private AppearanceControl ap;
			public ImageMarginRevealedAppearanceProperties(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}

			public void SetAppearanceControl(AppearanceControl appearanceControl)
			{
				ap = appearanceControl;
			}
			private Color _GradientBegin = Color.FromArgb(203, 221, 246);
			[DefaultValue(typeof(Color), "203, 221, 246")]
			[XmlIgnore()]
			public Color GradientBegin {
				get { return _GradientBegin; }
				set {
					_GradientBegin = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientMiddle = Color.FromArgb(161, 197, 249);
			[DefaultValue(typeof(Color), "161, 197, 249")]
			[XmlIgnore()]
			public Color GradientMiddle {
				get { return _GradientMiddle; }
				set {
					_GradientMiddle = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			private Color _GradientEnd = Color.FromArgb(114, 155, 215);
			[DefaultValue(typeof(Color), "114, 155, 215")]
			[XmlIgnore()]
			public Color GradientEnd {
				get { return _GradientEnd; }
				set {
					_GradientEnd = value;
					if (ap != null)
						ap.OnAppearanceChanged(EventArgs.Empty);
				}
			}

			public override string ToString()
			{
				return string.Empty;
			}

			[Browsable(false)]
			public int intGradientBegin {
				get { return this.GradientBegin.ToArgb(); }
				set { this.GradientBegin = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientMiddle {
				get { return this.GradientMiddle.ToArgb(); }
				set { this.GradientMiddle = Color.FromArgb(value); }
			}

			[Browsable(false)]
			public int intGradientEnd {
				get { return this.GradientEnd.ToArgb(); }
				set { this.GradientEnd = Color.FromArgb(value); }
			}

		}

		#endregion

		public override string ToString()
		{
			return string.Empty;
		}

	}

	#region " Color Tables "
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public class CustomColorTable : ProfessionalColorTable
	{


		private AppearanceControl ac = null;
		public CustomColorTable(AppearanceControl appearanceControl)
		{
			ac = appearanceControl;
		}

		public override Color ButtonSelectedHighlight {
			get { return ac.CustomAppearance.ButtonAppearance.SelectedAppearance.Highlight; }
		}

		public override Color ButtonSelectedHighlightBorder {
			get { return ac.CustomAppearance.ButtonAppearance.SelectedAppearance.BorderHighlight; }
		}

		public override Color ButtonPressedHighlight {
			get { return ac.CustomAppearance.ButtonAppearance.PressedAppearance.Highlight; }
		}

		public override Color ButtonPressedHighlightBorder {
			get { return ac.CustomAppearance.ButtonAppearance.PressedAppearance.BorderHighlight; }
		}

		public override Color ButtonCheckedHighlight {
			get { return ac.CustomAppearance.ButtonAppearance.CheckedAppearance.Highlight; }
		}

		public override Color ButtonCheckedHighlightBorder {
			get { return ac.CustomAppearance.ButtonAppearance.CheckedAppearance.BorderHighlight; }
		}

		public override Color ButtonPressedBorder {
			get { return ac.CustomAppearance.ButtonAppearance.PressedAppearance.Border; }
		}

		public override Color ButtonSelectedBorder {
			get { return ac.CustomAppearance.ButtonAppearance.SelectedAppearance.Border; }
		}

		public override Color ButtonCheckedGradientBegin {
			get { return ac.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientBegin; }
		}

		public override Color ButtonCheckedGradientMiddle {
			get { return ac.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientMiddle; }
		}

		public override Color ButtonCheckedGradientEnd {
			get { return ac.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientEnd; }
		}

		public override Color ButtonSelectedGradientBegin {
			get { return ac.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientBegin; }
		}

		public override Color ButtonSelectedGradientMiddle {
			get { return ac.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientMiddle; }
		}

		public override Color ButtonSelectedGradientEnd {
			get { return ac.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientEnd; }
		}

		public override Color ButtonPressedGradientBegin {
			get { return ac.CustomAppearance.ButtonAppearance.PressedAppearance.GradientBegin; }
		}

		public override Color ButtonPressedGradientMiddle {
			get { return ac.CustomAppearance.ButtonAppearance.PressedAppearance.GradientMiddle; }
		}

		public override Color ButtonPressedGradientEnd {
			get { return ac.CustomAppearance.ButtonAppearance.PressedAppearance.GradientEnd; }
		}

		public override Color CheckBackground {
			get { return ac.CustomAppearance.ButtonAppearance.CheckedAppearance.Background; }
		}

		public override Color CheckSelectedBackground {
			get { return ac.CustomAppearance.ButtonAppearance.CheckedAppearance.SelectedBackground; }
		}

		public override Color CheckPressedBackground {
			get { return ac.CustomAppearance.ButtonAppearance.CheckedAppearance.PressedBackground; }
		}

		public override Color GripDark {
			get { return ac.CustomAppearance.GripAppearance.Dark; }
		}

		public override Color GripLight {
			get { return ac.CustomAppearance.GripAppearance.Light; }
		}

		public override Color ImageMarginGradientBegin {
			get { return ac.CustomAppearance.ImageMarginAppearance.Normal.GradientBegin; }
		}

		public override Color ImageMarginGradientMiddle {
			get { return ac.CustomAppearance.ImageMarginAppearance.Normal.GradientMiddle; }
		}

		public override Color ImageMarginGradientEnd {
			get { return ac.CustomAppearance.ImageMarginAppearance.Normal.GradientEnd; }
		}

		public override Color ImageMarginRevealedGradientBegin {
			get { return ac.CustomAppearance.ImageMarginAppearance.Revealed.GradientBegin; }
		}

		public override Color ImageMarginRevealedGradientMiddle {
			get { return ac.CustomAppearance.ImageMarginAppearance.Revealed.GradientMiddle; }
		}

		public override Color ImageMarginRevealedGradientEnd {
			get { return ac.CustomAppearance.ImageMarginAppearance.Revealed.GradientEnd; }
		}

		public override Color MenuStripGradientBegin {
			get { return ac.CustomAppearance.MenuStripAppearance.GradientBegin; }
		}

		public override Color MenuStripGradientEnd {
			get { return ac.CustomAppearance.MenuStripAppearance.GradientEnd; }
		}

		public override Color MenuItemSelected {
			get { return ac.CustomAppearance.MenuItemAppearance.Selected; }
		}

		public override Color MenuItemBorder {
			get { return ac.CustomAppearance.MenuItemAppearance.Border; }
		}

		public override Color MenuBorder {
			get { return ac.CustomAppearance.MenuStripAppearance.Border; }
		}

		public override Color MenuItemSelectedGradientBegin {
			get { return ac.CustomAppearance.MenuItemAppearance.SelectedGradientBegin; }
		}

		public override Color MenuItemSelectedGradientEnd {
			get { return ac.CustomAppearance.MenuItemAppearance.SelectedGradientEnd; }
		}

		public override Color MenuItemPressedGradientBegin {
			get { return ac.CustomAppearance.MenuItemAppearance.PressedGradientBegin; }
		}

		public override Color MenuItemPressedGradientMiddle {
			get { return ac.CustomAppearance.MenuItemAppearance.PressedGradientMiddle; }
		}

		public override Color MenuItemPressedGradientEnd {
			get { return ac.CustomAppearance.MenuItemAppearance.PressedGradientEnd; }
		}

		public override Color RaftingContainerGradientBegin {
			get { return ac.CustomAppearance.RaftingContainerAppearance.GradientBegin; }
		}

		public override Color RaftingContainerGradientEnd {
			get { return ac.CustomAppearance.RaftingContainerAppearance.GradientEnd; }
		}

		public override Color SeparatorDark {
			get { return ac.CustomAppearance.SeparatorAppearance.Dark; }
		}

		public override Color SeparatorLight {
			get { return ac.CustomAppearance.SeparatorAppearance.Light; }
		}

		public override Color StatusStripGradientBegin {
			get { return ac.CustomAppearance.StatusStripAppearance.GradientBegin; }
		}

		public override Color StatusStripGradientEnd {
			get { return ac.CustomAppearance.StatusStripAppearance.GradientEnd; }
		}

		public override Color ToolStripBorder {
			get { return ac.CustomAppearance.ToolStripAppearance.Border; }
		}

		public override Color ToolStripDropDownBackground {
			get { return ac.CustomAppearance.ToolStripAppearance.DropDownBackground; }
		}

		public override Color ToolStripGradientBegin {
			get { return ac.CustomAppearance.ToolStripAppearance.GradientBegin; }
		}

		public override Color ToolStripGradientMiddle {
			get { return ac.CustomAppearance.ToolStripAppearance.GradientMiddle; }
		}

		public override Color ToolStripGradientEnd {
			get { return ac.CustomAppearance.ToolStripAppearance.GradientEnd; }
		}

		public override Color ToolStripContentPanelGradientBegin {
			get { return ac.CustomAppearance.ToolStripAppearance.ContentPanelGradientBegin; }
		}

		public override Color ToolStripContentPanelGradientEnd {
			get { return ac.CustomAppearance.ToolStripAppearance.ContentPanelGradientEnd; }
		}

		public override Color ToolStripPanelGradientBegin {
			get { return ac.CustomAppearance.ToolStripAppearance.PanelGradientBegin; }
		}

		public override Color ToolStripPanelGradientEnd {
			get { return ac.CustomAppearance.ToolStripAppearance.PanelGradientEnd; }
		}

		public override Color OverflowButtonGradientBegin {
			get { return ac.CustomAppearance.OverflowButtonAppearance.GradientBegin; }
		}

		public override Color OverflowButtonGradientMiddle {
			get { return ac.CustomAppearance.OverflowButtonAppearance.GradientMiddle; }
		}

		public override Color OverflowButtonGradientEnd {
			get { return ac.CustomAppearance.OverflowButtonAppearance.GradientEnd; }
		}

	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public class Office2007ColorTable : ProfessionalColorTable
	{

		public override Color ButtonSelectedHighlight {
			get { return Color.White; }
		}

		public override Color ButtonSelectedHighlightBorder {
			get { return Color.White; }
		}

		public override Color ButtonPressedHighlight {
			get { return Color.White; }
		}

		public override Color ButtonPressedHighlightBorder {
			get { return Color.White; }
		}

		public override Color ButtonCheckedHighlight {
			get { return Color.White; }
		}

		public override Color ButtonCheckedHighlightBorder {
			get { return Color.White; }
		}

		public override Color ButtonPressedBorder {
			get { return Color.FromArgb(251, 140, 60); }
		}

		public override Color ButtonSelectedBorder {
			get { return Color.FromArgb(255, 189, 105); }
		}

		public override Color ButtonCheckedGradientBegin {
			get { return Color.FromArgb(255, 207, 146); }
		}

		public override Color ButtonCheckedGradientMiddle {
			get { return Color.FromArgb(255, 189, 105); }
		}

		public override Color ButtonCheckedGradientEnd {
			get { return Color.FromArgb(255, 175, 73); }
		}

		public override Color ButtonSelectedGradientBegin {
			get { return Color.FromArgb(255, 245, 204); }
		}

		public override Color ButtonSelectedGradientMiddle {
			get { return Color.FromArgb(255, 230, 162); }
		}

		public override Color ButtonSelectedGradientEnd {
			get { return Color.FromArgb(255, 218, 117); }
		}

		public override Color ButtonPressedGradientBegin {
			get { return Color.FromArgb(252, 151, 61); }
		}

		public override Color ButtonPressedGradientMiddle {
			get { return Color.FromArgb(255, 171, 63); }
		}

		public override Color ButtonPressedGradientEnd {
			get { return Color.FromArgb(255, 184, 94); }
		}

		public override Color CheckBackground {
				//UNSURE
			get { return Color.FromArgb(255, 171, 63); }
		}

		public override Color CheckSelectedBackground {
				//UNSURE
			get { return this.ButtonPressedGradientBegin; }
		}

		public override Color CheckPressedBackground {
			get { return this.CheckSelectedBackground; }
		}

		public override Color GripDark {
			get { return Color.FromArgb(111, 157, 217); }
		}

		public override Color GripLight {
			get { return Color.White; }
		}

		public override Color ImageMarginGradientBegin {
			get { return Color.FromArgb(233, 238, 238); }
		}

		public override Color ImageMarginGradientMiddle {
			get { return this.ImageMarginGradientBegin; }
		}

		public override Color ImageMarginGradientEnd {
			get { return this.ImageMarginGradientBegin; }
		}

		public override Color ImageMarginRevealedGradientBegin {
				//UNSURE
			get { return this.ImageMarginGradientBegin; }
		}

		public override Color ImageMarginRevealedGradientMiddle {
				//UNSURE
			get { return this.ImageMarginRevealedGradientBegin; }
		}

		public override Color ImageMarginRevealedGradientEnd {
				//UNSURE
			get { return ImageMarginRevealedGradientBegin; }
		}

		public override Color MenuStripGradientBegin {
			get { return Color.FromArgb(191, 219, 255); }
		}

		public override Color MenuStripGradientEnd {
			get { return this.MenuStripGradientBegin; }
		}

		public override Color MenuItemSelected {
			get { return Color.FromArgb(255, 231, 162); }
		}

		public override Color MenuItemBorder {
			get { return Color.FromArgb(255, 189, 105); }
		}

		public override Color MenuBorder {
			get { return Color.FromArgb(101, 147, 207); }
		}

		public override Color MenuItemSelectedGradientBegin {
			get { return this.ButtonSelectedGradientBegin; }
		}

		public override Color MenuItemSelectedGradientEnd {
			get { return this.ButtonSelectedGradientEnd; }
		}

		public override Color MenuItemPressedGradientBegin {
			get { return Color.FromArgb(226, 239, 255); }
		}

		public override Color MenuItemPressedGradientMiddle {
			get { return Color.FromArgb(190, 215, 247); }
		}

		public override Color MenuItemPressedGradientEnd {
			get { return Color.FromArgb(153, 191, 240); }
		}

		public override Color RaftingContainerGradientBegin {
				//UNSURE
			get { return Color.White; }
		}

		public override Color RaftingContainerGradientEnd {
				//UNSURE
			get { return Color.White; }
		}

		public override Color SeparatorDark {
			get { return Color.FromArgb(154, 198, 255); }
		}

		public override Color SeparatorLight {
			get { return Color.White; }
		}

		public override Color ToolStripBorder {
			get { return Color.FromArgb(111, 157, 217); }
		}

		public override Color ToolStripDropDownBackground {
			get { return Color.FromArgb(246, 246, 246); }
		}

		public override Color ToolStripGradientBegin {
			get { return Color.FromArgb(227, 239, 255); }
		}

		public override Color ToolStripGradientMiddle {
			get { return Color.FromArgb(218, 234, 255); }
		}

		public override Color ToolStripGradientEnd {
			get { return Color.FromArgb(177, 211, 255); }
		}

		public override Color ToolStripContentPanelGradientBegin {
				//UNSURE
			get { return Color.FromArgb(215, 232, 255); }
		}

		public override Color ToolStripContentPanelGradientEnd {
				//UNSURE
			get { return Color.FromArgb(111, 157, 217); }
		}

		public override Color ToolStripPanelGradientBegin {
			get { return this.MenuStripGradientBegin; }
		}

		public override Color ToolStripPanelGradientEnd {
			get { return this.MenuStripGradientEnd; }
		}

		public override Color OverflowButtonGradientBegin {
			get { return Color.FromArgb(215, 232, 255); }
		}

		public override Color OverflowButtonGradientMiddle {
			get { return Color.FromArgb(167, 204, 251); }
		}

		public override Color OverflowButtonGradientEnd {
			get { return Color.FromArgb(111, 157, 217); }
		}

	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public class Office2007BlackColorTable : ProfessionalColorTable
	{

		public override Color ButtonSelectedHighlight {
				//*
			get { return Color.FromArgb(223, 227, 213); }
		}

		public override Color ButtonSelectedHighlightBorder {
				//
			get { return Color.FromArgb(255, 189, 105); }
		}

		public override Color ButtonPressedHighlight {
				//*
			get { return Color.FromArgb(200, 206, 182); }
		}

		public override Color ButtonPressedHighlightBorder {
				//*
			get { return Color.FromArgb(147, 160, 112); }
		}

		public override Color ButtonCheckedHighlight {
				//*
			get { return Color.FromArgb(223, 227, 213); }
		}

		public override Color ButtonCheckedHighlightBorder {
				//*
			get { return Color.FromArgb(147, 160, 112); }
		}

		public override Color ButtonPressedBorder {
				//
			get { return Color.FromArgb(255, 189, 105); }
		}

		public override Color ButtonSelectedBorder {
				//
			get { return Color.FromArgb(255, 189, 105); }
		}

		public override Color ButtonCheckedGradientBegin {
				//*
			get { return Color.FromArgb(255, 223, 154); }
		}

		public override Color ButtonCheckedGradientMiddle {
				//*
			get { return Color.FromArgb(255, 195, 116); }
		}

		public override Color ButtonCheckedGradientEnd {
				//*
			get { return Color.FromArgb(255, 166, 76); }
		}

		public override Color ButtonSelectedGradientBegin {
				//
			get { return Color.FromArgb(255, 245, 204); }
		}

		public override Color ButtonSelectedGradientMiddle {
				//
			get { return Color.FromArgb(255, 231, 162); }
		}

		public override Color ButtonSelectedGradientEnd {
				//
			get { return Color.FromArgb(255, 219, 117); }
		}

		public override Color ButtonPressedGradientBegin {
				//
			get { return Color.FromArgb(248, 181, 106); }
		}

		public override Color ButtonPressedGradientMiddle {
				//
			get { return Color.FromArgb(251, 140, 60); }
		}

		public override Color ButtonPressedGradientEnd {
				//
			get { return Color.FromArgb(255, 208, 134); }
		}

		public override Color CheckBackground {
				//
			get { return Color.FromArgb(255, 227, 149); }
		}

		public override Color CheckSelectedBackground {
				//
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color CheckPressedBackground {
				//*
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color GripDark {
				//
			get { return Color.FromArgb(145, 153, 164); }
		}

		public override Color GripLight {
				//
			get { return Color.FromArgb(221, 224, 227); }
		}

		public override Color ImageMarginGradientBegin {
//Return Color.FromArgb(239, 239, 239) '
			get { return Color.FromArgb(255, 223, 154); }
		}

		public override Color ImageMarginGradientMiddle {
//Return Color.FromArgb(239, 239, 239) '*
			get { return Color.FromArgb(255, 223, 154); }
		}

		public override Color ImageMarginGradientEnd {
//Return Color.FromArgb(239, 239, 239) '*
			get { return Color.FromArgb(255, 223, 154); }
		}

		public override Color ImageMarginRevealedGradientBegin {
				//*
			get { return Color.FromArgb(230, 230, 209); }
		}

		public override Color ImageMarginRevealedGradientMiddle {
				//*
			get { return Color.FromArgb(186, 201, 143); }
		}

		public override Color ImageMarginRevealedGradientEnd {
				//*
			get { return Color.FromArgb(160, 177, 116); }
		}

		public override Color MenuStripGradientBegin {
				//
			get { return Color.FromArgb(83, 83, 83); }
		}

		public override Color MenuStripGradientEnd {
				//
			get { return Color.FromArgb(83, 83, 83); }
		}

		public override Color MenuItemSelected {
				//
			get { return Color.FromArgb(255, 238, 194); }
		}

		public override Color MenuItemBorder {
				//
			get { return Color.FromArgb(255, 189, 105); }
		}

		public override Color MenuBorder {
				//
			get { return Color.FromArgb(145, 153, 164); }
		}

		public override Color MenuItemSelectedGradientBegin {
				//
			get { return Color.FromArgb(255, 245, 204); }
		}

		public override Color MenuItemSelectedGradientEnd {
				//
			get { return Color.FromArgb(255, 223, 132); }
		}

		public override Color MenuItemPressedGradientBegin {
				//
			get { return Color.FromArgb(145, 153, 164); }
		}

		public override Color MenuItemPressedGradientMiddle {
				//
			get { return Color.FromArgb(126, 135, 146); }
		}

		public override Color MenuItemPressedGradientEnd {
				//
			get { return Color.FromArgb(108, 117, 128); }
		}

		public override Color RaftingContainerGradientBegin {
				//
			get { return Color.FromArgb(83, 83, 83); }
		}

		public override Color RaftingContainerGradientEnd {
				//
			get { return Color.FromArgb(83, 83, 83); }
		}

		public override Color SeparatorDark {
				//
			get { return Color.FromArgb(145, 153, 164); }
		}

		public override Color SeparatorLight {
				//
			get { return Color.FromArgb(221, 224, 227); }
		}

		public override Color StatusStripGradientBegin {
				//
			get { return Color.FromArgb(76, 83, 92); }
		}

		public override Color StatusStripGradientEnd {
				//
			get { return Color.FromArgb(35, 38, 42); }
		}

		public override Color ToolStripBorder {
				//
			get { return Color.FromArgb(76, 83, 92); }
		}

		public override Color ToolStripDropDownBackground {
				//
			get { return Color.FromArgb(250, 250, 250); }
		}

		public override Color ToolStripGradientBegin {
				//
			get { return Color.FromArgb(205, 208, 213); }
		}

		public override Color ToolStripGradientMiddle {
				//
			get { return Color.FromArgb(188, 193, 200); }
		}

		public override Color ToolStripGradientEnd {
				//
			get { return Color.FromArgb(148, 156, 166); }
		}

		public override Color ToolStripContentPanelGradientBegin {
				//
			get { return Color.FromArgb(82, 82, 82); }
		}

		public override Color ToolStripContentPanelGradientEnd {
				//
			get { return Color.FromArgb(10, 10, 10); }
		}

		public override Color ToolStripPanelGradientBegin {
				//
			get { return Color.FromArgb(83, 83, 83); }
		}

		public override Color ToolStripPanelGradientEnd {
				//
			get { return Color.FromArgb(83, 83, 83); }
		}

		public override Color OverflowButtonGradientBegin {
				//
			get { return Color.FromArgb(178, 183, 191); }
		}

		public override Color OverflowButtonGradientMiddle {
				//
			get { return Color.FromArgb(145, 153, 164); }
		}

		public override Color OverflowButtonGradientEnd {
				//
			get { return Color.FromArgb(81, 88, 98); }
		}

	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public class Office2003BlueColorTable : ProfessionalColorTable
	{

		public override Color ButtonSelectedHighlight {
			get { return Color.FromArgb(195, 211, 237); }
		}

		public override Color ButtonSelectedHighlightBorder {
			get { return Color.FromArgb(0, 0, 128); }
		}

		public override Color ButtonPressedHighlight {
			get { return Color.FromArgb(150, 179, 225); }
		}

		public override Color ButtonPressedHighlightBorder {
			get { return Color.FromArgb(49, 106, 197); }
		}

		public override Color ButtonCheckedHighlight {
			get { return Color.FromArgb(195, 211, 237); }
		}

		public override Color ButtonCheckedHighlightBorder {
			get { return Color.FromArgb(49, 106, 197); }
		}

		public override Color ButtonPressedBorder {
			get { return Color.FromArgb(0, 0, 128); }
		}

		public override Color ButtonSelectedBorder {
			get { return Color.FromArgb(0, 0, 128); }
		}

		public override Color ButtonCheckedGradientBegin {
			get { return Color.FromArgb(255, 223, 154); }
		}

		public override Color ButtonCheckedGradientMiddle {
			get { return Color.FromArgb(255, 195, 116); }
		}

		public override Color ButtonCheckedGradientEnd {
			get { return Color.FromArgb(255, 166, 76); }
		}

		public override Color ButtonSelectedGradientBegin {
			get { return Color.FromArgb(255, 255, 222); }
		}

		public override Color ButtonSelectedGradientMiddle {
			get { return Color.FromArgb(255, 225, 172); }
		}

		public override Color ButtonSelectedGradientEnd {
			get { return Color.FromArgb(255, 203, 136); }
		}

		public override Color ButtonPressedGradientBegin {
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color ButtonPressedGradientMiddle {
			get { return Color.FromArgb(255, 177, 109); }
		}

		public override Color ButtonPressedGradientEnd {
			get { return Color.FromArgb(255, 223, 154); }
		}

		public override Color CheckBackground {
			get { return Color.FromArgb(255, 192, 111); }
		}

		public override Color CheckSelectedBackground {
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color CheckPressedBackground {
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color GripDark {
			get { return Color.FromArgb(39, 65, 118); }
		}

		public override Color GripLight {
			get { return Color.FromArgb(255, 255, 255); }
		}

		public override Color ImageMarginGradientBegin {
			get { return Color.FromArgb(227, 239, 255); }
		}

		public override Color ImageMarginGradientMiddle {
			get { return Color.FromArgb(203, 225, 252); }
		}

		public override Color ImageMarginGradientEnd {
			get { return Color.FromArgb(123, 164, 224); }
		}

		public override Color ImageMarginRevealedGradientBegin {
			get { return Color.FromArgb(203, 221, 246); }
		}

		public override Color ImageMarginRevealedGradientMiddle {
			get { return Color.FromArgb(161, 197, 249); }
		}

		public override Color ImageMarginRevealedGradientEnd {
			get { return Color.FromArgb(114, 155, 215); }
		}

		public override Color MenuStripGradientBegin {
			get { return Color.FromArgb(158, 190, 245); }
		}

		public override Color MenuStripGradientEnd {
			get { return Color.FromArgb(196, 218, 250); }
		}

		public override Color MenuItemSelected {
			get { return Color.FromArgb(255, 238, 194); }
		}

		public override Color MenuItemBorder {
			get { return Color.FromArgb(0, 0, 128); }
		}

		public override Color MenuBorder {
			get { return Color.FromArgb(0, 45, 150); }
		}

		public override Color MenuItemSelectedGradientBegin {
			get { return Color.FromArgb(255, 255, 222); }
		}

		public override Color MenuItemSelectedGradientEnd {
			get { return Color.FromArgb(255, 203, 136); }
		}

		public override Color MenuItemPressedGradientBegin {
			get { return Color.FromArgb(227, 239, 255); }
		}

		public override Color MenuItemPressedGradientMiddle {
			get { return Color.FromArgb(161, 197, 249); }
		}

		public override Color MenuItemPressedGradientEnd {
			get { return Color.FromArgb(123, 164, 224); }
		}

		public override Color RaftingContainerGradientBegin {
			get { return Color.FromArgb(158, 190, 245); }
		}

		public override Color RaftingContainerGradientEnd {
			get { return Color.FromArgb(196, 218, 250); }
		}

		public override Color SeparatorDark {
			get { return Color.FromArgb(106, 140, 203); }
		}

		public override Color SeparatorLight {
			get { return Color.FromArgb(241, 249, 255); }
		}

		public override Color StatusStripGradientBegin {
			get { return Color.FromArgb(158, 190, 245); }
		}

		public override Color StatusStripGradientEnd {
			get { return Color.FromArgb(196, 218, 250); }
		}

		public override Color ToolStripBorder {
			get { return Color.FromArgb(59, 97, 156); }
		}

		public override Color ToolStripDropDownBackground {
			get { return Color.FromArgb(246, 246, 246); }
		}

		public override Color ToolStripGradientBegin {
			get { return Color.FromArgb(227, 239, 255); }
		}

		public override Color ToolStripGradientMiddle {
			get { return Color.FromArgb(203, 225, 252); }
		}

		public override Color ToolStripGradientEnd {
			get { return Color.FromArgb(123, 164, 224); }
		}

		public override Color ToolStripContentPanelGradientBegin {
			get { return Color.FromArgb(158, 190, 245); }
		}

		public override Color ToolStripContentPanelGradientEnd {
			get { return Color.FromArgb(196, 218, 250); }
		}

		public override Color ToolStripPanelGradientBegin {
			get { return Color.FromArgb(158, 190, 245); }
		}

		public override Color ToolStripPanelGradientEnd {
			get { return Color.FromArgb(196, 218, 250); }
		}

		public override Color OverflowButtonGradientBegin {
			get { return Color.FromArgb(127, 177, 250); }
		}

		public override Color OverflowButtonGradientMiddle {
			get { return Color.FromArgb(82, 127, 208); }
		}

		public override Color OverflowButtonGradientEnd {
			get { return Color.FromArgb(0, 53, 145); }
		}

	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public class Office2003SilverColorTable : ProfessionalColorTable
	{

		public override Color ButtonSelectedHighlight {
			get { return Color.FromArgb(231, 232, 235); }
		}

		public override Color ButtonSelectedHighlightBorder {
			get { return Color.FromArgb(75, 75, 111); }
		}

		public override Color ButtonPressedHighlight {
			get { return Color.FromArgb(215, 216, 222); }
		}

		public override Color ButtonPressedHighlightBorder {
			get { return Color.FromArgb(178, 180, 191); }
		}

		public override Color ButtonCheckedHighlight {
			get { return Color.FromArgb(231, 232, 235); }
		}

		public override Color ButtonCheckedHighlightBorder {
			get { return Color.FromArgb(178, 180, 191); }
		}

		public override Color ButtonPressedBorder {
			get { return Color.FromArgb(75, 75, 111); }
		}

		public override Color ButtonSelectedBorder {
			get { return Color.FromArgb(75, 75, 111); }
		}

		public override Color ButtonCheckedGradientBegin {
			get { return Color.FromArgb(255, 223, 154); }
		}

		public override Color ButtonCheckedGradientMiddle {
			get { return Color.FromArgb(255, 195, 116); }
		}

		public override Color ButtonCheckedGradientEnd {
			get { return Color.FromArgb(255, 166, 76); }
		}

		public override Color ButtonSelectedGradientBegin {
			get { return Color.FromArgb(255, 255, 222); }
		}

		public override Color ButtonSelectedGradientMiddle {
			get { return Color.FromArgb(255, 225, 172); }
		}

		public override Color ButtonSelectedGradientEnd {
			get { return Color.FromArgb(255, 203, 136); }
		}

		public override Color ButtonPressedGradientBegin {
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color ButtonPressedGradientMiddle {
			get { return Color.FromArgb(255, 177, 109); }
		}

		public override Color ButtonPressedGradientEnd {
			get { return Color.FromArgb(255, 223, 154); }
		}

		public override Color CheckBackground {
			get { return Color.FromArgb(255, 192, 111); }
		}

		public override Color CheckSelectedBackground {
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color CheckPressedBackground {
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color GripDark {
			get { return Color.FromArgb(84, 84, 117); }
		}

		public override Color GripLight {
			get { return Color.FromArgb(255, 255, 255); }
		}

		public override Color ImageMarginGradientBegin {
			get { return Color.FromArgb(249, 249, 255); }
		}

		public override Color ImageMarginGradientMiddle {
			get { return Color.FromArgb(225, 226, 236); }
		}

		public override Color ImageMarginGradientEnd {
			get { return Color.FromArgb(147, 145, 176); }
		}

		public override Color ImageMarginRevealedGradientBegin {
			get { return Color.FromArgb(215, 215, 226); }
		}

		public override Color ImageMarginRevealedGradientMiddle {
			get { return Color.FromArgb(184, 185, 202); }
		}

		public override Color ImageMarginRevealedGradientEnd {
			get { return Color.FromArgb(118, 116, 151); }
		}

		public override Color MenuStripGradientBegin {
			get { return Color.FromArgb(215, 215, 229); }
		}

		public override Color MenuStripGradientEnd {
			get { return Color.FromArgb(243, 243, 247); }
		}

		public override Color MenuItemSelected {
			get { return Color.FromArgb(255, 238, 194); }
		}

		public override Color MenuItemBorder {
			get { return Color.FromArgb(75, 75, 111); }
		}

		public override Color MenuBorder {
			get { return Color.FromArgb(124, 124, 148); }
		}

		public override Color MenuItemSelectedGradientBegin {
			get { return Color.FromArgb(255, 255, 222); }
		}

		public override Color MenuItemSelectedGradientEnd {
			get { return Color.FromArgb(255, 203, 136); }
		}

		public override Color MenuItemPressedGradientBegin {
			get { return Color.FromArgb(232, 233, 242); }
		}

		public override Color MenuItemPressedGradientMiddle {
			get { return Color.FromArgb(184, 185, 202); }
		}

		public override Color MenuItemPressedGradientEnd {
			get { return Color.FromArgb(172, 170, 194); }
		}

		public override Color RaftingContainerGradientBegin {
			get { return Color.FromArgb(215, 215, 229); }
		}

		public override Color RaftingContainerGradientEnd {
			get { return Color.FromArgb(243, 243, 247); }
		}

		public override Color SeparatorDark {
			get { return Color.FromArgb(110, 109, 143); }
		}

		public override Color SeparatorLight {
			get { return Color.FromArgb(255, 255, 255); }
		}

		public override Color StatusStripGradientBegin {
			get { return Color.FromArgb(215, 215, 229); }
		}

		public override Color StatusStripGradientEnd {
			get { return Color.FromArgb(243, 243, 247); }
		}

		public override Color ToolStripBorder {
			get { return Color.FromArgb(124, 124, 148); }
		}

		public override Color ToolStripDropDownBackground {
			get { return Color.FromArgb(253, 250, 255); }
		}

		public override Color ToolStripGradientBegin {
			get { return Color.FromArgb(249, 249, 255); }
		}

		public override Color ToolStripGradientMiddle {
			get { return Color.FromArgb(225, 226, 236); }
		}

		public override Color ToolStripGradientEnd {
			get { return Color.FromArgb(147, 145, 176); }
		}

		public override Color ToolStripContentPanelGradientBegin {
			get { return Color.FromArgb(215, 215, 229); }
		}

		public override Color ToolStripContentPanelGradientEnd {
			get { return Color.FromArgb(243, 243, 247); }
		}

		public override Color ToolStripPanelGradientBegin {
			get { return Color.FromArgb(215, 215, 229); }
		}

		public override Color ToolStripPanelGradientEnd {
			get { return Color.FromArgb(243, 243, 247); }
		}

		public override Color OverflowButtonGradientBegin {
			get { return Color.FromArgb(186, 185, 206); }
		}

		public override Color OverflowButtonGradientMiddle {
			get { return Color.FromArgb(156, 155, 180); }
		}

		public override Color OverflowButtonGradientEnd {
			get { return Color.FromArgb(118, 116, 146); }
		}

	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public class Office2003OliveColorTable : ProfessionalColorTable
	{
		public override Color ButtonSelectedHighlight {
			get { return Color.FromArgb(223, 227, 213); }
		}

		public override Color ButtonSelectedHighlightBorder {
			get { return Color.FromArgb(63, 93, 56); }
		}

		public override Color ButtonPressedHighlight {
			get { return Color.FromArgb(200, 206, 182); }
		}

		public override Color ButtonPressedHighlightBorder {
			get { return Color.FromArgb(147, 160, 112); }
		}

		public override Color ButtonCheckedHighlight {
			get { return Color.FromArgb(223, 227, 213); }
		}

		public override Color ButtonCheckedHighlightBorder {
			get { return Color.FromArgb(147, 160, 112); }
		}

		public override Color ButtonPressedBorder {
			get { return Color.FromArgb(63, 93, 56); }
		}

		public override Color ButtonSelectedBorder {
			get { return Color.FromArgb(63, 93, 56); }
		}

		public override Color ButtonCheckedGradientBegin {
			get { return Color.FromArgb(255, 223, 154); }
		}

		public override Color ButtonCheckedGradientMiddle {
			get { return Color.FromArgb(255, 195, 116); }
		}

		public override Color ButtonCheckedGradientEnd {
			get { return Color.FromArgb(255, 166, 76); }
		}

		public override Color ButtonSelectedGradientBegin {
			get { return Color.FromArgb(255, 255, 222); }
		}

		public override Color ButtonSelectedGradientMiddle {
			get { return Color.FromArgb(255, 225, 172); }
		}

		public override Color ButtonSelectedGradientEnd {
			get { return Color.FromArgb(255, 203, 136); }
		}

		public override Color ButtonPressedGradientBegin {
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color ButtonPressedGradientMiddle {
			get { return Color.FromArgb(255, 177, 109); }
		}

		public override Color ButtonPressedGradientEnd {
			get { return Color.FromArgb(255, 223, 154); }
		}

		public override Color CheckBackground {
			get { return Color.FromArgb(255, 192, 111); }
		}

		public override Color CheckSelectedBackground {
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color CheckPressedBackground {
			get { return Color.FromArgb(254, 128, 62); }
		}

		public override Color GripDark {
			get { return Color.FromArgb(81, 94, 51); }
		}

		public override Color GripLight {
			get { return Color.FromArgb(255, 255, 255); }
		}

		public override Color ImageMarginGradientBegin {
			get { return Color.FromArgb(255, 255, 237); }
		}

		public override Color ImageMarginGradientMiddle {
			get { return Color.FromArgb(206, 220, 167); }
		}

		public override Color ImageMarginGradientEnd {
			get { return Color.FromArgb(181, 196, 143); }
		}

		public override Color ImageMarginRevealedGradientBegin {
			get { return Color.FromArgb(230, 230, 209); }
		}

		public override Color ImageMarginRevealedGradientMiddle {
			get { return Color.FromArgb(186, 201, 143); }
		}

		public override Color ImageMarginRevealedGradientEnd {
			get { return Color.FromArgb(160, 177, 116); }
		}

		public override Color MenuStripGradientBegin {
			get { return Color.FromArgb(217, 217, 167); }
		}

		public override Color MenuStripGradientEnd {
			get { return Color.FromArgb(242, 241, 228); }
		}

		public override Color MenuItemSelected {
			get { return Color.FromArgb(255, 238, 194); }
		}

		public override Color MenuItemBorder {
			get { return Color.FromArgb(63, 93, 56); }
		}

		public override Color MenuBorder {
			get { return Color.FromArgb(117, 141, 94); }
		}

		public override Color MenuItemSelectedGradientBegin {
			get { return Color.FromArgb(255, 255, 222); }
		}

		public override Color MenuItemSelectedGradientEnd {
			get { return Color.FromArgb(255, 203, 136); }
		}

		public override Color MenuItemPressedGradientBegin {
			get { return Color.FromArgb(237, 240, 214); }
		}

		public override Color MenuItemPressedGradientMiddle {
			get { return Color.FromArgb(186, 201, 143); }
		}

		public override Color MenuItemPressedGradientEnd {
			get { return Color.FromArgb(181, 196, 143); }
		}

		public override Color RaftingContainerGradientBegin {
			get { return Color.FromArgb(217, 217, 167); }
		}

		public override Color RaftingContainerGradientEnd {
			get { return Color.FromArgb(242, 241, 228); }
		}

		public override Color SeparatorDark {
			get { return Color.FromArgb(96, 128, 88); }
		}

		public override Color SeparatorLight {
			get { return Color.FromArgb(244, 247, 222); }
		}

		public override Color StatusStripGradientBegin {
			get { return Color.FromArgb(217, 217, 167); }
		}

		public override Color StatusStripGradientEnd {
			get { return Color.FromArgb(242, 241, 228); }
		}

		public override Color ToolStripBorder {
			get { return Color.FromArgb(96, 128, 88); }
		}

		public override Color ToolStripDropDownBackground {
			get { return Color.FromArgb(244, 244, 238); }
		}

		public override Color ToolStripGradientBegin {
			get { return Color.FromArgb(255, 255, 237); }
		}

		public override Color ToolStripGradientMiddle {
			get { return Color.FromArgb(206, 220, 167); }
		}

		public override Color ToolStripGradientEnd {
			get { return Color.FromArgb(181, 196, 143); }
		}

		public override Color ToolStripContentPanelGradientBegin {
			get { return Color.FromArgb(217, 217, 167); }
		}

		public override Color ToolStripContentPanelGradientEnd {
			get { return Color.FromArgb(242, 241, 228); }
		}

		public override Color ToolStripPanelGradientBegin {
			get { return Color.FromArgb(217, 217, 167); }
		}

		public override Color ToolStripPanelGradientEnd {
			get { return Color.FromArgb(242, 241, 228); }
		}

		public override Color OverflowButtonGradientBegin {
			get { return Color.FromArgb(186, 204, 150); }
		}

		public override Color OverflowButtonGradientMiddle {
			get { return Color.FromArgb(141, 160, 107); }
		}

		public override Color OverflowButtonGradientEnd {
			get { return Color.FromArgb(96, 119, 107); }
		}

	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public class OfficeXPColorTable : ProfessionalColorTable
	{
		public override Color ButtonSelectedHighlight {
			get { return Color.FromArgb(196, 208, 229); }
		}

		public override Color ButtonSelectedHighlightBorder {
			get { return Color.FromArgb(51, 94, 168); }
		}

		public override Color ButtonPressedHighlight {
			get { return Color.FromArgb(152, 173, 210); }
		}

		public override Color ButtonPressedHighlightBorder {
			get { return Color.FromArgb(51, 94, 168); }
		}

		public override Color ButtonCheckedHighlight {
			get { return Color.FromArgb(196, 208, 229); }
		}

		public override Color ButtonCheckedHighlightBorder {
			get { return Color.FromArgb(51, 94, 168); }
		}

		public override Color ButtonPressedBorder {
			get { return Color.FromArgb(51, 94, 168); }
		}

		public override Color ButtonSelectedBorder {
			get { return Color.FromArgb(51, 94, 168); }
		}

		public override Color ButtonCheckedGradientBegin {
			get { return Color.FromArgb(226, 229, 238); }
		}

		public override Color ButtonCheckedGradientMiddle {
			get { return Color.FromArgb(226, 229, 238); }
		}

		public override Color ButtonCheckedGradientEnd {
			get { return Color.FromArgb(226, 229, 238); }
		}

		public override Color ButtonSelectedGradientBegin {
			get { return Color.FromArgb(194, 207, 229); }
		}

		public override Color ButtonSelectedGradientMiddle {
			get { return Color.FromArgb(194, 207, 229); }
		}

		public override Color ButtonSelectedGradientEnd {
			get { return Color.FromArgb(194, 207, 229); }
		}

		public override Color ButtonPressedGradientBegin {
			get { return Color.FromArgb(153, 175, 212); }
		}

		public override Color ButtonPressedGradientMiddle {
			get { return Color.FromArgb(153, 175, 212); }
		}

		public override Color ButtonPressedGradientEnd {
			get { return Color.FromArgb(153, 175, 212); }
		}

		public override Color CheckBackground {
			get { return Color.FromArgb(226, 229, 238); }
		}

		public override Color CheckSelectedBackground {
			get { return Color.FromArgb(51, 94, 168); }
		}

		public override Color CheckPressedBackground {
			get { return Color.FromArgb(51, 94, 168); }
		}

		public override Color GripDark {
			get { return Color.FromArgb(189, 188, 191); }
		}

		public override Color GripLight {
			get { return Color.FromArgb(255, 255, 255); }
		}

		public override Color ImageMarginGradientBegin {
			get { return Color.FromArgb(252, 252, 252); }
		}

		public override Color ImageMarginGradientMiddle {
			get { return Color.FromArgb(245, 244, 246); }
		}

		public override Color ImageMarginGradientEnd {
			get { return Color.FromArgb(235, 233, 237); }
		}

		public override Color ImageMarginRevealedGradientBegin {
			get { return Color.FromArgb(247, 246, 248); }
		}

		public override Color ImageMarginRevealedGradientMiddle {
			get { return Color.FromArgb(241, 240, 242); }
		}

		public override Color ImageMarginRevealedGradientEnd {
			get { return Color.FromArgb(228, 226, 230); }
		}

		public override Color MenuStripGradientBegin {
			get { return Color.FromArgb(235, 233, 237); }
		}

		public override Color MenuStripGradientEnd {
			get { return Color.FromArgb(251, 250, 251); }
		}

		public override Color MenuItemSelected {
			get { return Color.FromArgb(194, 207, 229); }
		}

		public override Color MenuItemBorder {
			get { return Color.FromArgb(51, 94, 168); }
		}

		public override Color MenuBorder {
			get { return Color.FromArgb(134, 133, 136); }
		}

		public override Color MenuItemSelectedGradientBegin {
			get { return Color.FromArgb(194, 207, 229); }
		}

		public override Color MenuItemSelectedGradientEnd {
			get { return Color.FromArgb(194, 207, 229); }
		}

		public override Color MenuItemPressedGradientBegin {
			get { return Color.FromArgb(252, 252, 252); }
		}

		public override Color MenuItemPressedGradientMiddle {
			get { return Color.FromArgb(241, 240, 242); }
		}

		public override Color MenuItemPressedGradientEnd {
			get { return Color.FromArgb(245, 244, 246); }
		}

		public override Color RaftingContainerGradientBegin {
			get { return Color.FromArgb(235, 233, 237); }
		}

		public override Color RaftingContainerGradientEnd {
			get { return Color.FromArgb(251, 250, 251); }
		}

		public override Color SeparatorDark {
			get { return Color.FromArgb(193, 193, 196); }
		}

		public override Color SeparatorLight {
			get { return Color.FromArgb(255, 255, 255); }
		}

		public override Color StatusStripGradientBegin {
			get { return Color.FromArgb(235, 233, 237); }
		}

		public override Color StatusStripGradientEnd {
			get { return Color.FromArgb(251, 250, 251); }
		}

		public override Color ToolStripBorder {
			get { return Color.FromArgb(238, 237, 240); }
		}

		public override Color ToolStripDropDownBackground {
			get { return Color.FromArgb(252, 252, 252); }
		}

		public override Color ToolStripGradientBegin {
			get { return Color.FromArgb(252, 252, 252); }
		}

		public override Color ToolStripGradientMiddle {
			get { return Color.FromArgb(245, 244, 246); }
		}

		public override Color ToolStripGradientEnd {
			get { return Color.FromArgb(235, 233, 237); }
		}

		public override Color ToolStripContentPanelGradientBegin {
			get { return Color.FromArgb(235, 233, 237); }
		}

		public override Color ToolStripContentPanelGradientEnd {
			get { return Color.FromArgb(251, 250, 251); }
		}

		public override Color ToolStripPanelGradientBegin {
			get { return Color.FromArgb(235, 233, 237); }
		}

		public override Color ToolStripPanelGradientEnd {
			get { return Color.FromArgb(251, 250, 251); }
		}

		public override Color OverflowButtonGradientBegin {
			get { return Color.FromArgb(242, 242, 242); }
		}

		public override Color OverflowButtonGradientMiddle {
			get { return Color.FromArgb(224, 224, 225); }
		}

		public override Color OverflowButtonGradientEnd {
			get { return Color.FromArgb(167, 166, 170); }
		}


	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public class OfficeClassicColorTable : ProfessionalColorTable
	{
		public override Color ButtonSelectedHighlight {
			get { return Color.FromArgb(184, 191, 211); }
		}

		public override Color ButtonSelectedHighlightBorder {
			get { return Color.FromArgb(10, 36, 106); }
		}

		public override Color ButtonPressedHighlight {
			get { return Color.FromArgb(131, 144, 179); }
		}

		public override Color ButtonPressedHighlightBorder {
			get { return Color.FromArgb(10, 36, 106); }
		}

		public override Color ButtonCheckedHighlight {
			get { return Color.FromArgb(184, 191, 211); }
		}

		public override Color ButtonCheckedHighlightBorder {
			get { return Color.FromArgb(10, 36, 106); }
		}

		public override Color ButtonPressedBorder {
			get { return Color.FromArgb(10, 36, 106); }
		}

		public override Color ButtonSelectedBorder {
			get { return Color.FromArgb(10, 36, 106); }
		}

		public override Color ButtonCheckedGradientBegin {
			get { return Color.FromArgb(0, 0, 0); }
		}

		public override Color ButtonCheckedGradientMiddle {
			get { return Color.FromArgb(0, 0, 0); }
		}

		public override Color ButtonCheckedGradientEnd {
			get { return Color.FromArgb(0, 0, 0); }
		}

		public override Color ButtonSelectedGradientBegin {
			get { return Color.FromArgb(182, 189, 210); }
		}

		public override Color ButtonSelectedGradientMiddle {
			get { return Color.FromArgb(182, 189, 210); }
		}

		public override Color ButtonSelectedGradientEnd {
			get { return Color.FromArgb(182, 189, 210); }
		}

		public override Color ButtonPressedGradientBegin {
			get { return Color.FromArgb(133, 146, 181); }
		}

		public override Color ButtonPressedGradientMiddle {
			get { return Color.FromArgb(133, 146, 181); }
		}

		public override Color ButtonPressedGradientEnd {
			get { return Color.FromArgb(133, 146, 181); }
		}

		public override Color CheckBackground {
			get { return Color.FromArgb(10, 36, 106); }
		}

		public override Color CheckSelectedBackground {
			get { return Color.FromArgb(133, 146, 181); }
		}

		public override Color CheckPressedBackground {
			get { return Color.FromArgb(133, 146, 181); }
		}

		public override Color GripDark {
			get { return Color.FromArgb(160, 160, 160); }
		}

		public override Color GripLight {
			get { return Color.FromArgb(255, 255, 255); }
		}

		public override Color ImageMarginGradientBegin {
			get { return Color.FromArgb(245, 244, 242); }
		}

		public override Color ImageMarginGradientMiddle {
			get { return Color.FromArgb(234, 232, 228); }
		}

		public override Color ImageMarginGradientEnd {
			get { return Color.FromArgb(212, 208, 200); }
		}

		public override Color ImageMarginRevealedGradientBegin {
			get { return Color.FromArgb(238, 236, 233); }
		}

		public override Color ImageMarginRevealedGradientMiddle {
			get { return Color.FromArgb(225, 222, 217); }
		}

		public override Color ImageMarginRevealedGradientEnd {
			get { return Color.FromArgb(216, 213, 206); }
		}

		public override Color MenuStripGradientBegin {
			get { return Color.FromArgb(212, 208, 200); }
		}

		public override Color MenuStripGradientEnd {
			get { return Color.FromArgb(246, 245, 244); }
		}

		public override Color MenuItemSelected {
			get { return Color.FromArgb(255, 255, 255); }
		}

		public override Color MenuItemBorder {
			get { return Color.FromArgb(10, 36, 106); }
		}

		public override Color MenuBorder {
			get { return Color.FromArgb(102, 102, 102); }
		}

		public override Color MenuItemSelectedGradientBegin {
			get { return Color.FromArgb(182, 189, 210); }
		}

		public override Color MenuItemSelectedGradientEnd {
			get { return Color.FromArgb(182, 189, 210); }
		}

		public override Color MenuItemPressedGradientBegin {
			get { return Color.FromArgb(245, 244, 242); }
		}

		public override Color MenuItemPressedGradientMiddle {
			get { return Color.FromArgb(225, 222, 217); }
		}

		public override Color MenuItemPressedGradientEnd {
			get { return Color.FromArgb(234, 232, 228); }
		}

		public override Color RaftingContainerGradientBegin {
			get { return Color.FromArgb(212, 208, 200); }
		}

		public override Color RaftingContainerGradientEnd {
			get { return Color.FromArgb(246, 245, 244); }
		}

		public override Color SeparatorDark {
			get { return Color.FromArgb(166, 166, 166); }
		}

		public override Color SeparatorLight {
			get { return Color.FromArgb(255, 255, 255); }
		}

		public override Color StatusStripGradientBegin {
			get { return Color.FromArgb(212, 208, 200); }
		}

		public override Color StatusStripGradientEnd {
			get { return Color.FromArgb(246, 245, 244); }
		}

		public override Color ToolStripBorder {
			get { return Color.FromArgb(219, 216, 209); }
		}

		public override Color ToolStripDropDownBackground {
			get { return Color.FromArgb(249, 248, 247); }
		}

		public override Color ToolStripGradientBegin {
			get { return Color.FromArgb(245, 244, 242); }
		}

		public override Color ToolStripGradientMiddle {
			get { return Color.FromArgb(234, 232, 228); }
		}

		public override Color ToolStripGradientEnd {
			get { return Color.FromArgb(212, 208, 200); }
		}

		public override Color ToolStripContentPanelGradientBegin {
			get { return Color.FromArgb(212, 208, 200); }
		}

		public override Color ToolStripContentPanelGradientEnd {
			get { return Color.FromArgb(246, 245, 244); }
		}

		public override Color ToolStripPanelGradientBegin {
			get { return Color.FromArgb(212, 208, 200); }
		}

		public override Color ToolStripPanelGradientEnd {
			get { return Color.FromArgb(246, 245, 244); }
		}

		public override Color OverflowButtonGradientBegin {
			get { return Color.FromArgb(225, 222, 217); }
		}

		public override Color OverflowButtonGradientMiddle {
			get { return Color.FromArgb(216, 213, 206); }
		}

		public override Color OverflowButtonGradientEnd {
			get { return Color.FromArgb(128, 128, 128); }
		}

	}

	#endregion

	public virtual void OnAppearanceChanged(EventArgs e)
	{
		if (AppearanceChanged != null) {
			AppearanceChanged(this, e);
		}
	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
