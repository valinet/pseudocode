﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace Pseudocode
{
    public partial class CrashReporter : Form
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int X;
            public int Y;
            public int Width;
            public int Height;
        }
        string Code;

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);
        bool allowclosing;
        IntPtr hWnd;
        Form frm;
        DialogResult final;
        string AdvancedReport;
        string Sending;
        string MailErr;
        string AppName;
        int lg;

        public CrashReporter(Form frma, bool allowclose, string AdvancedErrorReport, int lang)
        {
            InitializeComponent();
            lg = lang;
            if (lg == 0)
            {
            //    DoOnUIThread(delegate()
//{
    System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
//});
                AppName = "Pseudocode";
                Sending = "Sending error report...";
                MailErr = "The e-mail address field must either be blank, either contain a valid e-mail address.";
            }
            if (lg == 1)
            {
                   //                    DoOnUIThread(delegate()
                   //    {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
                  //     });
                AppName = "Pseudocode";
                Sending = "Raportul este în curs de expediere...";
                MailErr = "Câmpul \"E-mail\" este necompletat sau conține date invalide.\n\nPentru a raporta eroarea către ValiNet, este necesar să introduceți o adresă de e-mail validă pentru a putea fi contactat în vederea unor eventuale clarificări.";
            }
            frm = frma;
            AdvancedReport = AdvancedErrorReport;
            allowclosing = allowclose;

            //Code = mesaj;
        }




        FormWindowState state;
        private void MessageBoxI_Load(object sender, EventArgs e)
        {

            try
            {
                hWnd = frm.Handle;
            }
            catch { }

            this.Size = frm.Size;
            if (frm.WindowState == FormWindowState.Maximized) this.Width += 5;
            this.Location = new Point(frm.Location.X, frm.Location.Y);
            this.Owner = frm;
            state = frm.WindowState;
            if (frm.WindowState == FormWindowState.Maximized) this.WindowState = FormWindowState.Maximized;
            textBox1.Text = "";
            textBox1.Focus();
            textBox1.Select(0, 0);
            if (Properties.Settings.Default.ReportByDefault == false)
            {
                checkBox1.Checked = false;
                button1.Visible = false;
                label3.Visible = false;
                label2.Visible = false;
                textBox1.Visible = false;
                textBox2.Visible = false;
            }
            if (Properties.Settings.Default.ContinueExecutionOnCrash == true && allowclosing == true)
            {
                button2.Visible = true;
            }
            //backgroundWorker1.RunWorkerAsync();
        }


        bool wasFocusSet = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (a != 1) this.Opacity += 0.1;
            if (a == 1) this.Opacity -= 0.1;
            if (a == 1 & this.Opacity <= 0)
            {
                a = 2;
                this.Close();
            }
            RECT rect;
            GetWindowRect(hWnd, out rect);
            if (rect.X == -32000)
            {
                // the game is minimized
                this.WindowState = FormWindowState.Minimized;
            }
            else
            {
                bool ok = false;
                if (state == FormWindowState.Maximized) ok = true;
                this.WindowState = FormWindowState.Normal;
                if (ok == true) this.Location = new Point(rect.X + 5, rect.Y - 7);
                else this.Location = new Point(rect.X, rect.Y);

            }
            if (wasFocusSet == false)
            {
                textBox1.Focus();
                wasFocusSet = true;
            }
            //textBox1.Focus();
        }

        private void MessageBoxI_Paint(object sender, PaintEventArgs e)
        {
            if (Environment.OSVersion.Version.Major <= 5)
            {

                this.TransparencyKey = this.BackColor;
            }
            else
            {

                var hb = new HatchBrush(HatchStyle.Percent50, this.TransparencyKey);

                if (this.WindowState == FormWindowState.Maximized) e.Graphics.FillRectangle(hb, new Rectangle(this.DisplayRectangle.X, this.DisplayRectangle.Y, this.DisplayRectangle.Width + 10, this.DisplayRectangle.Height));// this.DisplayRectangle);
                else e.Graphics.FillRectangle(hb, this.DisplayRectangle);
            }
        }

        private void MessageBoxI_Click(object sender, EventArgs e)
        {
            final = System.Windows.Forms.DialogResult.OK;
            //button1.PerformClick();
            this.Close();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                if (!textBox2.Text.Contains("@") || !textBox2.Text.Contains("."))
                {
                    MessageBox.Show(MailErr, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);


                }
                else
                {
                    Titlu.Text = Sending;
                    label1.Visible = false;
                    checkBox1.Visible = false;
                    button1.Visible = false;
                    label3.Visible = false;
                    label2.Visible = false;
                    textBox1.Visible = false;
                    textBox2.Visible = false;
                    OK.Visible = false;
                    button2.Visible = false;
                    //pictureBox2.Visible = true;
                    Application.DoEvents();
                    try
                    {
                        GmailAccount acc = new GmailAccount("valinetmail", "vali2011net", "Pseudcode");
                        acc.SendMessage("vali@valinet.ro", "Crash report for Pseudocode", "Comment: " + textBox1.Text + "\n\nAuthor e-mail: " + textBox2.Text + "\n\nBody: " + AdvancedReport, "c");
                    }
                    /*catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }*/
                    finally
                    {
                        final = System.Windows.Forms.DialogResult.OK;
                        this.Close();
                        
                    }
                    //backgroundWorker1.RunWorkerAsync();
                }
            }
            else
            {
                final = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }
        int a = 0;
        private void MessageBoxI_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (a == 2)
            {
                this.Visible = false;
                DialogResult = final;
            }
            else
            {
                a = 1;
                e.Cancel = true;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
        }
        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(AdvancedReport, AppName, MessageBoxButtons.OK);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                if (!textBox2.Text.Contains("@") || !textBox2.Text.Contains("."))
                {
                    MessageBox.Show(MailErr, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);


                }
                else
                {
                    Titlu.Text = Sending;
                    label1.Visible = false;
                    checkBox1.Visible = false;
                    button1.Visible = false;
                    label3.Visible = false;
                    label2.Visible = false;
                    textBox1.Visible = false;
                    textBox2.Visible = false;
                    OK.Visible = false;
                    button2.Visible = false;
                    //pictureBox2.Visible = true;
                    Application.DoEvents();
                    try
                    {
                        GmailAccount acc = new GmailAccount("valinetmail", "vali2011net", "Pseudcode");
                        acc.SendMessage("vali@valinet.ro", "Crash report for Pseudocode", "Comment: " + textBox1.Text + "\n\nAuthor e-mail: " + textBox2.Text + "\n\nBody: " + AdvancedReport, "c");
                    }
                    /*catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }*/
                    finally
                    {
                        final = System.Windows.Forms.DialogResult.Ignore;
                        this.Close();
                    }
                    //backgroundWorker1.RunWorkerAsync();
                }
            }
            else
            {
                final = System.Windows.Forms.DialogResult.Ignore;
                this.Close();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            button1.Visible = !button1.Visible;
            label3.Visible = !label3.Visible;
            label2.Visible = !label2.Visible;
            textBox1.Visible = !textBox1.Visible;
            textBox2.Visible = !textBox2.Visible;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                GmailAccount acc = new GmailAccount("valinetmail", "vali2011net");
                acc.SendMessage("vali@valinet.ro", "Crash report for Pseudocode", "Comment: " + textBox1.Text + "\n\nAuthor e-mail: " + textBox2.Text + "\n\nBody: " + AdvancedReport);
            }
            finally
            {
                DoOnUIThread(delegate()
                {
                    final = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                });
            }
        }
    }
}


