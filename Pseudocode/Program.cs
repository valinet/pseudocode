﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace Pseudocode
{
    static class Program
    {
        #region Get Font DPI

        private static readonly int LOGPIXELSX = 88;    // Used for GetDeviceCaps().
        private static readonly int LOGPIXELSY = 90;    // Used for GetDeviceCaps().

        /// <summary>Determines the current screen resolution in DPI.</summary>
        /// <returns>Point.X is the X DPI, Point.Y is the Y DPI.</returns>
        public static Point GetSystemDpi()
        {
            Point result = new Point();

            IntPtr hDC = GetDC(IntPtr.Zero);

            result.X = GetDeviceCaps(hDC, LOGPIXELSX);
            result.Y = GetDeviceCaps(hDC, LOGPIXELSY);

            ReleaseDC(IntPtr.Zero, hDC);

            return result;
        }

        /// <summary>
        /// Checks if font is not default.
        /// </summary>
        /// <returns>True if font DPI is not 96.</returns>
        public static bool IsDifferentFont()
        {
            Point result = GetSystemDpi();

            return result.X != 96 || result.Y != 96;
        }

        [DllImport("gdi32.dll")]
        private static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);

        #endregion
        public static bool CheckIfRemoteSession()
        {
            bool IsRemoteSession = false;
            //Constant used to check for remote sessions
            const int SM_REMOTESESSION = 0x2001;
            //This function will return 0 if the session is local
            int Value = GetSystemMetrics(SM_REMOTESESSION);

            IsRemoteSession = (Value != 0);

            return IsRemoteSession;
        }

        [DllImport("user32.dll", EntryPoint = ("GetSystemMetrics"))]
        private static extern int GetSystemMetrics(int nIndex);

        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetProcessDPIAware();
        public static Form frm;
        //public static MainForm frm;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                if (Properties.Settings.Default.DisableCrashReporter == false)
                {
                    Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
                    Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                    AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
                }
                if (Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Platform == PlatformID.Win32NT) SetProcessDPIAware();
                Properties.Settings.Default.EnableNewUI = true;
                GDITest gdi = new GDITest();
                Graphics g = gdi.CreateGraphics();
                if (Properties.Settings.Default.PerfOptions == 0)
                {
                    if (Environment.OSVersion.Version.Major <= 5)
                    {
                        Properties.Settings.Default.EnableNewUI = false;
                    }
                    if (CheckIfRemoteSession() == true)
                    {
                        Properties.Settings.Default.EnableNewUI = false;
                    }
                    if (!(g.DpiX == 96 && g.DpiY == 96))
                    {
                        Properties.Settings.Default.EnableNewUI = false;
                    }
                }
                if (Properties.Settings.Default.PerfOptions == 1)
                {
                    Properties.Settings.Default.EnableNewUI = true;
                }
                if (Properties.Settings.Default.PerfOptions == 2)
                {
                    Properties.Settings.Default.EnableNewUI = false;
                }
                Properties.Settings.Default.DisableEnhancedUI = true;
                Properties.Settings.Default.Save();
                frm = new MainForm(null);
                Application.Run(frm);
                gdi.Close();
        }
        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Properties.Settings.Default.HasCrashed = true;
            Properties.Settings.Default.Save();
            {
                DialogResult dr = CasetaDeMesaj(frm, true, "Unhandled exception has occured in the Pseudocode application. The following information will be sent along with the information you provide to ValiNet:\r\n\r\n" + "App version: " + Application.ProductVersion.ToString() + "\r\nOperating System: " + Environment.OSVersion.ToString() + "\r\nNo. of processors: " + Environment.ProcessorCount + "\r\nIs 64-bit process: " + Environment.Is64BitProcess.ToString() + "\r\nIs 64-bit Operating System: " + Environment.Is64BitOperatingSystem + "\r\nSystem directory: " + Environment.SystemDirectory + "\r\nFramework version: " + Environment.Version + "\r\nRAM footprint: " + Environment.WorkingSet + "\r\n\r\n" + "Message: " + e.Exception.Message + "\r\n\r\nSource: " + e.Exception.Source + "\r\n\r\nData: " + e.Exception.Data + "\r\n\r\nStack Trace: " + e.Exception.StackTrace + "\r\n\r\nTarget Site: " + e.Exception.TargetSite + "\r\n\r\nInner Exception: " + e.Exception.InnerException + "\r\n\r\nHelp Link: " + e.Exception.HelpLink + "\r\n\r\nPlease report the problem to ValiNet to help solve this immediatly.", Properties.Settings.Default.UILang);
                
                if (dr == DialogResult.OK)
                {
                    if (Properties.Settings.Default.ReportToMER == true) Environment.FailFast(e.Exception.Message, e.Exception);
                    else System.Diagnostics.Process.GetCurrentProcess().Kill();
                }

            }

        }
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Properties.Settings.Default.HasCrashed = true;
            Properties.Settings.Default.Save();
            {
                DialogResult dr = CasetaDeMesaj(frm, !e.IsTerminating, "Unhandled exception has occured in the Pseudocode application. The following information will be sent along with the information you provide to ValiNet:\r\n\r\n" + "Operating System: " + Environment.OSVersion.ToString() + "\r\nNo. of processors: " + Environment.ProcessorCount + "\r\nIs 64-bit process: " + Environment.Is64BitProcess.ToString() + "\r\nIs 64-bit Operating System: " + Environment.Is64BitOperatingSystem + "\r\nSystem directory: " + Environment.SystemDirectory + "\r\nFramework version: " + Environment.Version + "\r\nRAM footprint: " + Environment.WorkingSet + "\r\n\r\n" + "Message: " + e.ExceptionObject.ToString() + "\r\n\r\nPlease report the problem to ValiNet to help solve this immediatly.", Properties.Settings.Default.UILang);

                if (dr == DialogResult.OK)
                {
                    Exception ex = (Exception)e.ExceptionObject;
                    if (Properties.Settings.Default.ReportToMER == true) Environment.FailFast(ex.Message, ex);
                    else System.Diagnostics.Process.GetCurrentProcess().Kill();
                }

            }
        }
        public static DialogResult CasetaDeMesaj(Form form, bool allowclose, string mesaj, int lang)
        {
            CrashReporter msg = new CrashReporter(form, allowclose, mesaj, lang);
            msg.ShowDialog();
            return msg.DialogResult;
        }
    }
}
