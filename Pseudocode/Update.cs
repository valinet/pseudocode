﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Principal;

namespace Pseudocode
{
    public partial class Update : Form
    {
        string add;
        public bool IsUserAdministrator()
        {
            if (Application.StartupPath.Contains("Program Files"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Update(string versiune, string text, string link, string descarcare)
        {
            InitializeComponent();
            if (Properties.Settings.Default.UILang == 0)
            {
                add = " and you have to be signed in to Windows using an administrative account (elevation takes place automatically in Windows Vista, 7 and 8).";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                add = " și trebuie să fi făcut sign in pe sistem folosind un cont administrativ (elevarea se face automat în Windows Vista, 7 și 8).";
            }
            label2.Text += versiune;
            textBox1.Text = text;
            linkLabel1.Tag = link;
            button1.Tag = descarcare;
            if (IsUserAdministrator() == true)
            {
                button1.Tag += ".exe";
                label3.Text += add;
            }
            else
            {
                button1.Tag += "limited.exe";
                label3.Text += ".";
            }
        }

        private void Update_Load(object sender, EventArgs e)
        {

        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(linkLabel1.Tag.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode")) System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode");
            /*            if (dr == System.Windows.Forms.DialogResult.OK)
            {*/
            try
            {
                if (IsUserAdministrator() == true)
                {
                    System.IO.File.Copy(Application.StartupPath + "\\updater.exe", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\updaterO.exe", true);
                }
                else
                {
                    System.IO.File.Copy(Application.StartupPath + "\\updaterLIMITED.exe", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\updaterO.exe", true);
                }
                System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Pseudocode\\updaterO.exe", button1.Tag.ToString() + " " + Properties.Settings.Default.UILang.ToString());
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            catch
            {
            }
            // }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.TopMost = false;
            timer1.Enabled = false;
        }
    }
}
