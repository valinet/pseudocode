﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class AddCompiler : Form
    {
        MainForm mf;
        string AppName;
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public AddCompiler(string a, int b, string c, string d, MainForm m, string app, string e)
        {
            InitializeComponent();
            textBox1.Text = a;
            comboBox1.SelectedIndex = b;
            textBox2.Text = c;
            textBox3.Text = d;
            mf = m;
            AppName = app;
            textBox4.Text = e;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                textBox2.Text = openFileDialog1.FileName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string delete = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                delete = "A compiler with the same name is already in the list.\n\nType a different name and try again.";
            }
            else if (Properties.Settings.Default.UILang == 1)
            {
                delete = "Un compilator cu același nume se află deja în listă.\n\nTastați alt nume și încercați din nou.";
            }
            bool cancel = false;
            foreach (string s in Properties.Settings.Default.Compilers)
            {
                if (s.Split(';')[0] == textBox1.Text)
                {
                    cancel = true;
                    break;
                }
            }
            if (textBox1.Text == "Local Debugger" || textBox1.Text == "Built-in Web Browser") cancel = true;
            if (cancel == true)
            {
                CasetaDeMesaj(mf, delete, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string lang = "";
            switch (comboBox1.SelectedIndex)
            {
                case 0: lang = "CPP"; break;
                case 1: lang = "PASCAL"; break;
                case 2: lang = "C#"; break;
                case 3: lang = "JAVASCRIPT"; break;
                case 4: lang = "VB6"; break;
            }
            Properties.Settings.Default.Compilers.Add(textBox1.Text + ";" + lang + ";" + textBox2.Text + ";" + textBox3.Text + ";" + textBox4.Text);
            Properties.Settings.Default.Save();
            this.Close();
        }
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }
    }
}
