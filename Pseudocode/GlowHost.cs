﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Integration; 

namespace Pseudocode
{
    public partial class GlowHost : ShakeForm //, IMessageFilter
    {
        /*public bool PreFilterMessage(ref Message m)
        {
            bool ret = true;
            if (m.Msg == 0x202)
            {
                if (Cursor.Position.Y == 0 && (GetForegroundWindow() == this.Handle || GetForegroundWindow() == frm.Handle))
               /* Keys key = (Keys)(int)m.WParam & Keys.KeyCode;
                if (key == Keys.Y)
                {
                    if ((int)Control.ModifierKeys == ((int)Keys.Control + (int)Keys.Alt))
                    {
                        //got it......
                    }
                    else
                    {
                        ret = false;
                    }

                }*/
               /* MessageBox.Show("da");
                return ret;
            }
            else
            {
                return false;
            }
        }*/
        public GlowHost()
        {
           // Application.AddMessageFilter(this);
            InitializeComponent();
            this.DoubleBuffered = true;
            this.FormShaken += new EventHandler(Form1_FormShaken);
        }
        public Glow wpfwindow;
        MainForm frm;
        private bool minimize = true;

        void Form1_FormShaken(object sender, EventArgs e)
        {
            if (minimize && Properties.Settings.Default.AeroShake)
                WindowStateManager.MinimizeAll(this.Handle, frm.Handle, wpfwindow.GetHandle());
          //  else
                //WindowStateManager.MaximizeAll(this.Handle);

         //   minimize = !minimize;
        }
        public bool IsOnScreen(Form form)
        {
            Screen[] screens = Screen.AllScreens;
            foreach (Screen screen in screens)
            {
                Rectangle formRectangle = new Rectangle(form.Left, form.Top, form.Width, form.Height);

                if (screen.WorkingArea.Contains(formRectangle))
                {
                    return true;
                }
            }

            return false;
        }
        protected override void OnLoad(EventArgs e)
        {
            frm = new MainForm(this);
            this.Opacity = 0;
            frm.Opacity = 0;
            if (Properties.Settings.Default.UILang == 0)
            {
                toolTip1.SetToolTip(Close, "Close (Alt+F4)");
                toolTip1.SetToolTip(Restore, "Maximize");
                toolTip1.SetToolTip(Minimize, "Minimize");
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                toolTip1.SetToolTip(Close, "Închidere (Alt+F4)");
                toolTip1.SetToolTip(Restore, "Maximizare");
                toolTip1.SetToolTip(Minimize, "Minimizare");
            }
            Close.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            Restore.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            Minimize.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            this.Padding = new Padding((this.ClientSize.Width - this.Size.Width) / 2);
            //  SetWindowLongPtr(this.Handle, -16, (IntPtr)(0x80000000L | 0x00C00000L | 0x00040000L | 0x00010000L | 0x00020000L));
            //   SetWindowPos(this.Handle, IntPtr.Zero, 0, 0, 0, 0, SetWindowPosFlags.FrameChanged | SetWindowPosFlags.IgnoreMove | SetWindowPosFlags.IgnoreResize);
            wpfwindow = new Glow();
            wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
            ElementHost.EnableModelessKeyboardInterop(wpfwindow);
            //wpfwindow.Show();
            //frm.Opacity = 0;
            //this.Opacity = 0;
            frm.Show();

            Application.DoEvents();
            ContinueCode();
            base.OnLoad(e);
        }
        public void ContinueCode()
        {
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            if (Properties.Settings.Default.Position == true)
            {
                if (Properties.Settings.Default.WindowsSize != new Size(0, 0))
                {
                    this.Size = Properties.Settings.Default.WindowsSize;
                }
                if (Properties.Settings.Default.WindowsPos != new Point(0, 0))
                {
                    this.Location = new Point(Properties.Settings.Default.WindowsPos.X, Properties.Settings.Default.WindowsPos.Y - Close.Height - 5);
                }
                if (IsOnScreen(this) == false)
                {
                    this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
                    this.Size = new Size(1258, 697);
                }
            }
            else
            {
                this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width / 2 - this.Width / 2, Screen.PrimaryScreen.WorkingArea.Height / 2 - this.Height / 2);
                this.Size = new Size(1258, 697);
            }
        }
        protected override void OnShown(EventArgs e)
        {
            if (Properties.Settings.Default.IsMaximized == true)
            {
                Restore.PerformClick();
                Size = Properties.Settings.Default.WindowsSize;
            }
            if (GetForegroundWindow() == this.Handle) SetForegroundWindow(frm.Handle);
            this.Opacity = 1;
            frm.Opacity = 1;
            wpfwindow.Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 90, 8, 140));
            frm.unminimizedCode = true;
            frm.done = true;
            base.OnShown(e);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
        }
        bool Active = false;
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(long dwFlags, long dx, long dy, long cButtons, long dwExtraInfo);

        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;
        const int WS_MINIMIZEBOX = 0x20000;
        const int WS_MAXIMIZEBOX = 0x10000;
        const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams param = base.CreateParams;
                param.Style |= (WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);

                return param;
            }
        }
        public static void DoMouseClick()
        {
            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
        }
        [DllImport("user32.dll")]
        static extern bool SetCursorPos(int X, int Y);
        private void N_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks == 2 && e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                frm.Size = new Size(frm.Size.Width, Screen.GetWorkingArea(this).Height);
                return;
            }
            if (e.Clicks == 1 && e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                SetCursorPos(Cursor.Position.X, PointToScreen(new Point(0, Close.Height + this.Padding.Top)).Y + 5);
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
            }
            //Active = true; Snap();
        }

        private void N_MouseMove(object sender, MouseEventArgs e)
        {
          /*  if (Active && frm.Size.Width > frm.MinimumSize.Width && frm.Size.Height > frm.MinimumSize.Height)
            {
                if (e.Y > 0)
                {
                    frm.Size = new Size(frm.Width, frm.Height - e.Y);
                    frm.Location = new Point(frm.Left, frm.Top + e.Y);
                }
                else
                {
                    frm.Location = new Point(frm.Left, frm.Top + e.Y);
                    frm.Size = new Size(frm.Width, frm.Height - e.Y);
                }
                frm.Refresh();
            }*/
        }

        private void N_MouseUp(object sender, MouseEventArgs e)
        {
           // Snap();
          //  Active = false;
        }

        private void NV_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks == 1 && e.Button == System.Windows.Forms.MouseButtons.Left)
            {

                SetCursorPos(PointToScreen(new Point(this.Padding.Left, Close.Height)).X + 5, PointToScreen(new Point(0, Close.Height + this.Padding.Top)).Y + 5);
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                //Active = true; Snap();
            }
        }

        private void NV_MouseMove(object sender, MouseEventArgs e)
        {
           /* if (Active)
            {
                if (e.Y < 0 || e.X < 0)
                {
                    frm.Location = new Point(frm.Left + e.X, frm.Top + e.Y);
                    frm.Size = new Size(frm.Width - e.X, frm.Height - e.Y);
                }
                else
                {
                    frm.Size = new Size(frm.Width - e.X, frm.Height - e.Y);
                    frm.Location = new Point(frm.Left + e.X, frm.Top + e.Y);
                }
                frm.Refresh();
            }*/
        }

        private void NV_MouseUp(object sender, MouseEventArgs e)
        {
            //Snap();
           // Active = false;
        }

        private void V_MouseDown(object sender, MouseEventArgs e)
        {
            //SetCursorPos(PointToScreen(new Point(0, Close.Height)).X + 5, PointToScreen(new Point(0, Close.Height)).Y + 20);
            //mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
            //mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
            //Active = true; Snap();
        }

        private void V_MouseMove(object sender, MouseEventArgs e)
        {
           /* if (Active)
            {
                frm.Refresh();
                if (e.X < 0)
                {
                    frm.Location = new Point(frm.Left + e.X, frm.Top);
                    frm.Size = new Size(frm.Width - e.X, frm.Height);
                }
                else
                {
                    frm.Size = new Size(frm.Width - e.X, frm.Height);
                    frm.Location = new Point(frm.Left + e.X, frm.Top);
                }
                frm.Refresh();
            }*/
        }

        private void V_MouseUp(object sender, MouseEventArgs e)
        {

            //Active = false;
        }

        private void SV_MouseDown(object sender, MouseEventArgs e)
        {

            Active = true; Snap();
        }

        private void SV_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                if (e.X < 0)
                {
                    this.Location = new Point(this.Left + e.Location.X, this.Top);
                    this.Size = new Size(this.Width - e.Location.X, this.Height + e.Location.Y);
                }
                else
                {
                    this.Size = new Size(this.Width - e.Location.X, this.Height + e.Location.Y);
                    this.Location = new Point(this.Left + e.Location.X, this.Top);
                }
                this.Refresh();
            }
        }

        private void SV_MouseUp(object sender, MouseEventArgs e)
        {

            Active = false;
        }

        private void S_MouseDown(object sender, MouseEventArgs e)
        {

            Active = true; Snap();
        }

        private void S_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                this.Size = new Size(this.Width, this.Height + e.Y);
                this.Refresh();
            }
        }

        private void S_MouseUp(object sender, MouseEventArgs e)
        {

            Active = false;
        }

        private void SE_MouseDown(object sender, MouseEventArgs e)
        {

            Active = true; Snap();
        }

        private void SE_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                this.Size = new Size(this.Width + e.X, this.Height + e.Y);
                this.Refresh();
            }
        }

        private void SE_MouseUp(object sender, MouseEventArgs e)
        {

            Active = false;
        }

        private void E_MouseDown(object sender, MouseEventArgs e)
        {

            Active = true; Snap();
        }

        private void E_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                this.Size = new Size(this.Width + e.Location.X, this.Height);
                this.Refresh();

            }
        }

        private void E_MouseUp(object sender, MouseEventArgs e)
        {

            Active = false;
        }

        private void EE_MouseDown(object sender, MouseEventArgs e)
        {

            Active = true; Snap();
        }

        private void EE_MouseMove(object sender, MouseEventArgs e)
        {
            if (Active)
            {
                this.Size = new Size(this.Width + e.Location.X, this.Height);
                this.Refresh();

            }
        }

        private void EE_MouseUp(object sender, MouseEventArgs e)
        {

            Active = false;
        }

        private void NE_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks == 1 && e.Button == System.Windows.Forms.MouseButtons.Left)
            {

                SetCursorPos(PointToScreen(new Point(this.Width + this.Padding.Right, Close.Height)).X - 1, PointToScreen(new Point(0, Close.Height + this.Padding.Top)).Y + 5);
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                //Active = true; Snap();
            }
        }

        private void NE_MouseMove(object sender, MouseEventArgs e)
        {
            /*if (Active)
            {
                if (e.Y < 0)
                {
                    frm.Location = new Point(frm.Left, frm.Top + e.Y);
                    frm.Size = new Size(frm.Width + e.X, frm.Height - e.Y);
                }
                else
                {
                    frm.Size = new Size(frm.Width + e.X, frm.Height - e.Y);
                    frm.Location = new Point(frm.Left, frm.Top + e.Y);
                }
                frm.Refresh();
            }*/
        }

        private void NE_MouseUp(object sender, MouseEventArgs e)
        {
            //Snap();
            //Active = false;
        }

        private void NN_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks == 1 && e.Button == System.Windows.Forms.MouseButtons.Left)
            {

                SetCursorPos(Cursor.Position.X, PointToScreen(new Point(0, Close.Height)).Y + 5);
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                //Active = true; Snap();
            }
        }

        private void NN_MouseMove(object sender, MouseEventArgs e)
        {
           /* if (Active)
            {
                if (e.Y > 0)
                {
                    frm.Size = new Size(frm.Width, frm.Height - e.Y);
                    frm.Location = new Point(frm.Left, frm.Top + e.Y);
                }
                else
                {
                    frm.Location = new Point(frm.Left, frm.Top + e.Y);
                    frm.Size = new Size(frm.Width, frm.Height - e.Y);
                }
                frm.Refresh();
            }*/
        }

        private void NN_MouseUp(object sender, MouseEventArgs e)
        {
            //Snap();
           // Active = false;
        }
        private void Close_MouseEnter(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Close.BackgroundImage = CloseBlackHover.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Close.BackgroundImage = CloseLightHover.BackgroundImage;

        }

        private void Close_MouseDown(object sender, MouseEventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Close.BackgroundImage = CloseBlackPressed.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Close.BackgroundImage = CloseLightPressed.BackgroundImage;

        }

        private void Close_MouseLeave(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Close.BackgroundImage = CloseBlack.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Close.BackgroundImage = CloseLight.BackgroundImage;

        }

        private void Restore_MouseEnter(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = RestoreBlackHover.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = RestoreLightHover.BackgroundImage;
        }

        private void Restore_MouseDown(object sender, MouseEventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = RestoreBlackPressed.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = RestoreLightPressed.BackgroundImage;
        }

        private void Restore_MouseLeave(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Restore.BackgroundImage = RestoreBlack.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Restore.BackgroundImage = RestoreLight.BackgroundImage;
        }

        private void Minimize_MouseEnter(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Minimize.BackgroundImage = MinimizeBlackHover.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Minimize.BackgroundImage = MinimizeLightHover.BackgroundImage;
        }

        private void Minimize_MouseDown(object sender, MouseEventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Minimize.BackgroundImage = MinimizeBlackPressed.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Minimize.BackgroundImage = MinimizeLightPressed.BackgroundImage;

        }

        private void Minimize_MouseLeave(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UITheme == 0) Minimize.BackgroundImage = MinimizeBlack.BackgroundImage;
            if (Properties.Settings.Default.UITheme == 1) Minimize.BackgroundImage = MinimizeLight.BackgroundImage;
        }

        private void Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        const int WM_NCLBUTTONDOWN = 0xA1;
        const int WM_NCHITTEST = 0x84;
        const int HT_CAPTION = 0x2;
        const int WM_NCCALCSIZE = 0x83;

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public int left, top, right, bottom;

            public RECT(Rectangle rc)
            {
                this.left = rc.Left;
                this.top = rc.Top;
                this.right = rc.Right;
                this.bottom = rc.Bottom;
            }

            public Rectangle ToRectangle()
            {
                return Rectangle.FromLTRB(left, top, right, bottom);
            }

        }

        [StructLayout(LayoutKind.Sequential)]
        private struct NCCALCSIZE_PARAMS
        {
            public RECT rgrc0, rgrc1, rgrc2;
            public WINDOWPOS lppos;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct WINDOWPOS
        {
            public IntPtr hWnd, hWndInsertAfter;
            public int x, y, cx, cy, flags;
        }
        
        const int WM_SYSCOMMAND= 0x0112;
        const int SC_MAXIMIZE= 0xF030;
        [DllImport("user32.dll")]
        static extern IntPtr DefWindowProc(IntPtr hWnd, int uMsg, IntPtr wParam, IntPtr lParam);
        bool aero_snap = false;
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCCALCSIZE:
                    if (m.WParam.Equals(IntPtr.Zero))
                    {

                        RECT rc = (RECT)m.GetLParam(typeof(RECT));
                        Rectangle r = rc.ToRectangle();
                        r.Inflate(8, 8);
                        Marshal.StructureToPtr(new RECT(r), m.LParam, true);
                    }
                    else
                    {
                        NCCALCSIZE_PARAMS csp = (NCCALCSIZE_PARAMS)m.GetLParam(typeof(NCCALCSIZE_PARAMS));
                        Rectangle r = csp.rgrc0.ToRectangle();
                        r.Inflate(8, 8);
                        csp.rgrc0 = new RECT(r);
                        Marshal.StructureToPtr(csp, m.LParam, true);
                    }
                    m.Result = IntPtr.Zero;
                    return;
            }
            /*if (m.Msg == 0x0005) //WM_SIZE
            {
                if ((int)m.WParam == 2) //SIZE_MAXIMIZED
                {
                    
                    MessageBox.Show("Maximized");
                    return;
                }
            }*/
            base.WndProc(ref m);
        }

        private void Close_Click(object sender, EventArgs e)
        {
            frm.Close();
        }
        bool drag = false;
        // This static method is required because legacy OSes do not support
        // SetWindowLongPtr 
        public static IntPtr SetWindowLongPtr(IntPtr hWnd, int nIndex, IntPtr dwNewLong)
        {
            if (IntPtr.Size == 8)
                return SetWindowLongPtr64(hWnd, nIndex, dwNewLong);
            else
                return new IntPtr(SetWindowLong32(hWnd, nIndex, dwNewLong.ToInt32()));
        }

        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        private static extern int SetWindowLong32(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr")]
        private static extern IntPtr SetWindowLongPtr64(IntPtr hWnd, int nIndex, IntPtr dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);

        [Flags()]
        private enum SetWindowPosFlags : uint
        {
            /// <summary>If the calling thread and the thread that owns the window are attached to different input queues, 
            /// the system posts the request to the thread that owns the window. This prevents the calling thread from 
            /// blocking its execution while other threads process the request.</summary>
            /// <remarks>SWP_ASYNCWINDOWPOS</remarks>
            AsynchronousWindowPosition = 0x4000,
            /// <summary>Prevents generation of the WM_SYNCPAINT message.</summary>
            /// <remarks>SWP_DEFERERASE</remarks>
            DeferErase = 0x2000,
            /// <summary>Draws a frame (defined in the window's class description) around the window.</summary>
            /// <remarks>SWP_DRAWFRAME</remarks>
            DrawFrame = 0x0020,
            /// <summary>Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to
            /// the window, even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE
            /// is sent only when the window's size is being changed.</summary>
            /// <remarks>SWP_FRAMECHANGED</remarks>
            FrameChanged = 0x0020,
            /// <summary>Hides the window.</summary>
            /// <remarks>SWP_HIDEWINDOW</remarks>
            HideWindow = 0x0080,
            /// <summary>Does not activate the window. If this flag is not set, the window is activated and moved to the
            /// top of either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter 
            /// parameter).</summary>
            /// <remarks>SWP_NOACTIVATE</remarks>
            DoNotActivate = 0x0010,
            /// <summary>Discards the entire contents of the client area. If this flag is not specified, the valid 
            /// contents of the client area are saved and copied back into the client area after the window is sized or 
            /// repositioned.</summary>
            /// <remarks>SWP_NOCOPYBITS</remarks>
            DoNotCopyBits = 0x0100,
            /// <summary>Retains the current position (ignores X and Y parameters).</summary>
            /// <remarks>SWP_NOMOVE</remarks>
            IgnoreMove = 0x0002,
            /// <summary>Does not change the owner window's position in the Z order.</summary>
            /// <remarks>SWP_NOOWNERZORDER</remarks>
            DoNotChangeOwnerZOrder = 0x0200,
            /// <summary>Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to
            /// the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent 
            /// window uncovered as a result of the window being moved. When this flag is set, the application must 
            /// explicitly invalidate or redraw any parts of the window and parent window that need redrawing.</summary>
            /// <remarks>SWP_NOREDRAW</remarks>
            DoNotRedraw = 0x0008,
            /// <summary>Same as the SWP_NOOWNERZORDER flag.</summary>
            /// <remarks>SWP_NOREPOSITION</remarks>
            DoNotReposition = 0x0200,
            /// <summary>Prevents the window from receiving the WM_WINDOWPOSCHANGING message.</summary>
            /// <remarks>SWP_NOSENDCHANGING</remarks>
            DoNotSendChangingEvent = 0x0400,
            /// <summary>Retains the current size (ignores the cx and cy parameters).</summary>
            /// <remarks>SWP_NOSIZE</remarks>
            IgnoreResize = 0x0001,
            /// <summary>Retains the current Z order (ignores the hWndInsertAfter parameter).</summary>
            /// <remarks>SWP_NOZORDER</remarks>
            IgnoreZOrder = 0x0004,
            /// <summary>Displays the window.</summary>
            /// <remarks>SWP_SHOWWINDOW</remarks>
            ShowWindow = 0x0040,
        }
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            Snap();
            if (drag == true && e.Button == System.Windows.Forms.MouseButtons.Left)
            {
             //   Point p = PointToScreen(e.Location);
            //    if (p.X == 0 || p.Y == 0 || p.X == Screen.FromHandle(this.Handle).Bounds.Width - 1 || p.Y == Screen.FromHandle(this.Handle).Bounds.Height - 1)
            //    {
           
                    try
                    {
                        SetWindowLongPtr(this.Handle, -16, (IntPtr)(0x80000000L | 0x00C00000L | 0x00040000L | 0x00010000L | 0x00020000L));
                        SetWindowPos(this.Handle, IntPtr.Zero, 0, 0, 0, 0, SetWindowPosFlags.FrameChanged | SetWindowPosFlags.IgnoreMove | SetWindowPosFlags.IgnoreResize);
                    }
                    catch { }
                ReleaseCapture();

                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
                //this.Capture = false;
                 //   Message msg = Message.Create(Handle, WM_NCLBUTTONDOWN, (IntPtr)HT_CAPTION, IntPtr.Zero);
                  //  WndProc(ref msg);
                    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    SetWindowPos(this.Handle, IntPtr.Zero, 0, 0, 0, 0, SetWindowPosFlags.FrameChanged | SetWindowPosFlags.IgnoreMove | SetWindowPosFlags.IgnoreResize);
               // }
             //   else
             //   {
            //        this.Location = new Point(p.X - loc.X, p.Y - loc.Y);
             //   }
                return;
            }
        }
        public void Snap()
        {
            try
            {
                ScreenCapture sc = new ScreenCapture();
                pictureBox1.Image = sc.CaptureWindow(frm.Handle);
                pictureBox1.Image = sc.CaptureWindow(frm.Handle);
            }
            catch { }
        }
        Point loc;
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            loc = e.Location;
            Snap();
            this.BringToFront();
            this.Refresh();
            if (e.Button == System.Windows.Forms.MouseButtons.Left && e.Clicks == 1) drag = true;
            if (e.Button == System.Windows.Forms.MouseButtons.Left && e.Clicks == 2)
            {
                Restore.PerformClick();
                drag = false;
            }
            if (e.Button == System.Windows.Forms.MouseButtons.Right && e.Clicks == 1)
            {
                frm.ShowSystemMenu(PointToClient(Cursor.Position));
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
          
          //  Point p = Cursor.Position;
          //  SetCursorPos(PointToScreen(new Point(this.Width / 2, Close.Height)).X - 1, PointToScreen(new Point(0, Close.Height)).Y + 10);
          //  DoMouseClick();
          //  SetCursorPos(p.X,p.Y);
            //drag = false;
        }
        [StructLayout(LayoutKind.Sequential)]
        struct POINT
        {
            public Int32 x;
            public Int32 y;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct CURSORINFO
        {
            public Int32 cbSize;        // Specifies the size, in bytes, of the structure. 
            // The caller must set this to Marshal.SizeOf(typeof(CURSORINFO)).
            public Int32 flags;         // Specifies the cursor state. This parameter can be one of the following values:
            //    0             The cursor is hidden.
            //    CURSOR_SHOWING    The cursor is showing.
            public IntPtr hCursor;          // Handle to the cursor. 
            public POINT ptScreenPos;       // A POINT structure that receives the screen coordinates of the cursor. 
        }

        [DllImport("user32.dll")]
        static extern bool GetCursorInfo(out CURSORINFO pci);

        private const Int32 CURSOR_SHOWING = 0x00000001;
        public bool doNotAlterLocation = false;
        Point oldLoc;
        private void Form1_LocationChanged(object sender, EventArgs e)
        {
       
                this.Refresh();
                try
                {
                    
                    if (Cursor.Position.Y == 0) this.MaximumSize = this.Size;
                    else this.MaximumSize = new Size(0, 0);
                    wpfwindow.Left = this.Left - 10;
                    wpfwindow.Top = this.Top - 10;
                    wpfwindow.Width = this.Width + 20;
                    wpfwindow.Height = this.Height + 25 + Close.Height;
                        var h = Cursors.WaitCursor.Handle;

                        if (doNotAlterLocation == true)
                        {
                           // if (Cursor.Position.X > PointToScreen(this.Location).X + 5)
                          //  {
                          //      frm.Location = new Point(this.Location.X, this.Location.Y + Close.Height + 5);
                           // }
                        }
                        else
                        {
                            frm.Location = new Point(this.Location.X, this.Location.Y + Close.Height + 5);
                        }
                    //frm.Size = new Size(this.Width, this.Height - Close.Height);
                        oldLoc = this.Location;

                }
                catch { }
            
        }
        public bool nuLua = false;
        public void CallSize(EventArgs e)
        {
            OnClientSizeChanged(e);
        }
        protected override void OnClientSizeChanged(EventArgs e)
        {
            try
            {
                Snap();
                if (this.WindowState == FormWindowState.Maximized)
                {
                    this.WindowState = FormWindowState.Normal;
                    Application.DoEvents();
                    Restore.PerformClick();
                }
                /*if (this.WindowState == FormWindowState.Maximized && nuLua == false)
                {
                    doNotAlterLocation = true;
                    nuLua = true;
                    this.WindowState = FormWindowState.Normal;
                    this.Location = new Point(frm.Location.X, frm.Location.Y - Close.Height - 5);
                    this.Size = frm.Size;
                  /*  frm.ResizeBegin -= frm.MainForm_ResizeBegin;
                    frm.Resize -= frm.MainForm_Resize;
                    frm.ResizeEnd -= frm.MainForm_ResizeEnd;*/
                   /* HideGlow();
                    this.Hide();
                    frm.WindowState = FormWindowState.Maximized;
                    doNotAlterLocation = false;
                    nuLua = false;
                  //  frm.MaximizeCode();
                  //  nuLua = false;
                  //  doNotAlterLocation = false;
                 //   return;
                  /*  frm.ResizeBegin += frm.MainForm_ResizeBegin;
                    frm.Resize += frm.MainForm_Resize;
                    frm.ResizeEnd += frm.MainForm_ResizeEnd;*/
                   
             //   }
                if (wpfwindow.Visibility == System.Windows.Visibility.Visible)
                {
                    wpfwindow.Width = this.Width + 20;
                    wpfwindow.Height = this.Height + 25 + Close.Height;
                }
                if (this.Size != frm.Size && nuLua == false)
                {
                    nuLua = true;
                    this.Height = this.Height - Close.Height - 5;
                    nuLua = false;
                    frm.Size = this.Size;//new Size(this.Width, this.Height - Close.Height - 5);
                }
            }
            catch { }
           // try
           // {
            //    if (this.Location.X == 0 && this.Location.Y == 0 && (this.Width == Screen.GetWorkingArea(this).Width / 2 || this.Width == this.MinimumSize.Width))
            //    {
                   /* SetForegroundWindow(frm.Handle);
                    wpfwindow.Left = this.Left - 10;
                    wpfwindow.Top = this.Top - 10;
                    wpfwindow.Width = this.Width + 20;
                    wpfwindow.Height = this.Height + 25;
                    frm.Resize -= frm.MainForm_Resize;
                    frm.ResizeBegin -= frm.MainForm_ResizeBegin;
                    frm.ResizeEnd -= frm.MainForm_ResizeEnd;
                    frm.Location = new Point(this.Location.X, this.Location.Y + Close.Height + 5);*/
                  //  frm.Size = new Size(this.Width, this.Height - Close.Height - 10);
                    /*frm.Resize += frm.MainForm_Resize;
                    frm.ResizeBegin += frm.MainForm_ResizeBegin;
                    frm.ResizeEnd += frm.MainForm_ResizeEnd;*/
            //    }
          //  }
         //   catch { }
        }
        public void HideGlow()
        {
            wpfwindow.OuterGlow.BorderThickness = new System.Windows.Thickness(0);
            wpfwindow.OuterGlow.BorderBrush.Opacity = 0;
           // wpfwindow.Visibility = System.Windows.Visibility.Hidden;
        }
        public void ShowGlow()
        {
            //BeginControlUpdate(this);
            wpfwindow.Left = this.Left - 10;
            wpfwindow.Top = this.Top - 10;
            wpfwindow.Width = this.Width + 20;
            wpfwindow.Height = this.Height + 25 + Close.Height ;

            wpfwindow.Visibility = System.Windows.Visibility.Visible;

            Snap();
            //SetForegroundWindow(this.Handle);
            //SetForegroundWindow(frm.Handle);
            timer1.Enabled = true;
            this.Refresh();
            try
            {
                frm.Refresh();
            }
            catch { }
        }

        private void GlowHost_ResizeBegin(object sender, EventArgs e)
        {
           // if (Cursor.Position.Y == 0) MessageBox.Show("da");
        }
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        private void GlowHost_ResizeEnd(object sender, EventArgs e)
        {
            OnClientSizeChanged(e);
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            drag = false;
            SetForegroundWindow(wpfwindow.GetHandle());
            SetForegroundWindow(this.Handle);
            SetForegroundWindow(frm.Handle);
            this.Refresh();
            pictureBox1.Image = null;
            timer1.Enabled = false;
            wpfwindow.OuterGlow.BorderThickness = new System.Windows.Thickness(10);
            wpfwindow.OuterGlow.BorderBrush.Opacity = 100;
            //EndControlUpdate(this);
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            drag = false;
            SetForegroundWindow(frm.Handle);
            this.Refresh();
            pictureBox1.Image = null;
        }

        /// <summary>
        /// An application sends the WM_SETREDRAW message to a window to allow changes in that 
        /// window to be redrawn or to prevent changes in that window from being redrawn.
        /// </summary>
        private const int WM_SETREDRAW = 11;

        /// <summary>
        /// Suspends painting for the target control. Do NOT forget to call EndControlUpdate!!!
        /// </summary>
        /// <param name="control">visual control</param>
        public static void BeginControlUpdate(Control control)
        {
            Message msgSuspendUpdate = Message.Create(control.Handle, WM_SETREDRAW, IntPtr.Zero,
                  IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgSuspendUpdate);
        }

        /// <summary>
        /// Resumes painting for the target control. Intended to be called following a call to BeginControlUpdate()
        /// </summary>
        /// <param name="control">visual control</param>
        public static void EndControlUpdate(Control control)
        {
            // Create a C "true" boolean as an IntPtr
            IntPtr wparam = new IntPtr(1);
            Message msgResumeUpdate = Message.Create(control.Handle, WM_SETREDRAW, wparam,
                  IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgResumeUpdate);
            control.Invalidate();
            control.Refresh();
        }

        private void Minimize_Click_1(object sender, EventArgs e)
        {
            frm.nuIntra = true;
            HideGlow();

         //   Application.DoEvents();
            //this.pictureBox1.Image = null;


           // frm.ClientSize = new Size(frm.ClientSize.Width + Close.Width / 2 - 1, frm.ClientSize.Height + Close.Height * 2 - Close.Height / 2 - 1);
            frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            SetWindowPos(frm.Handle, IntPtr.Zero, 0, 0, 0, 0, SetWindowPosFlags.FrameChanged | SetWindowPosFlags.IgnoreMove | SetWindowPosFlags.IgnoreResize);

          //  SetWindowLongPtr(frm.Handle, -16, (IntPtr)(0x80000000L | 0x00C00000L | 0x00040000L | 0x00010000L | 0x00020000L));

            //frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            //this.WindowState = FormWindowState.Minimized;
            frm.WindowState = FormWindowState.Minimized;
            //frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Hide();


            frm.nuIntra = false;
        }

        private void Restore_Click(object sender, EventArgs e)
        {
            this.Hide();
            HideGlow();
            frm.MaximizeCode();
        }

        private void GlowHost_MouseEnter(object sender, EventArgs e)
        {
           /* Snap();
            this.Refresh();
            SetForegroundWindow(this.Handle);
            this.Refresh();*/
        }

        private void GlowHost_MouseLeave(object sender, EventArgs e)
        {
          /*  Snap();
            this.Refresh();
            SetForegroundWindow(frm.Handle);
            this.Refresh();*/
        }

        private void menuIcon_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks == 1 && e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                frm.ShowSystemMenu(new Point(0, menuIcon.Height));
            }
            if (e.Clicks == 2 && e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                frm.Close();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            
        }

        private void GlowHost_Move(object sender, EventArgs e)
        {

        }

        private void MovingTimer_Tick(object sender, EventArgs e)
        {

        }
    }
}
