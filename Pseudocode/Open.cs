﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic.Devices;
using AppLimit.CloudComputing.SharpBox;
using AppLimit.CloudComputing.SharpBox.StorageProvider.DropBox;
using System.Globalization;
using ValiNet.PseudocodePluginsFramework;
//using SHDocVw;
//using CefSharp;
//using CefSharp.WinForms;

namespace Pseudocode
{
    public partial class Open : Form
    {
        Form1 Form1;
        string AppName;
        string SureToEmptyList;
        string NoSuchFile;
        List<Button> button = new List<Button>();
        StringCollection col = new StringCollection();
        string DownloadModule;
        string Error;
        string FileNotForOpen;
        string ErrorDownloadingCode;
        string Downloaded;
        string UnpreparedDrive;
        string UnnamedDrive;
        string AvailableDisks;
        string FrequentPlaces;
        string PseudocodeFolderText;
        string DesktopText;
        string DocumentsText;
        string UseCodeText;
        string FromCodeRepText;
        string FromMyDropboxText;
        string RecentEditsText;
        string Name;
        string Created;
        string Modified;
        string Size;
        string DefaultSearchBoxText;
        string DefaultText;
        string DoesNotExist;
        string AccessDenied;
        string Eroare;
        string UnavailableName;
        public string doAfterLoad;
        //WebView webView;
        Dictionary<string, VPlugin> _Plugins;
        public Open(Form1 form)
        {

            //var settings = new Settings();
            //settings.CachePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode";
            //CEF.Initialize(settings);
            InitializeComponent();

            DefaultText = this.Text;
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
            DefaultSearchBoxText = SearchBox.Text;
            Form1 = form;
            if (Properties.Settings.Default.UILang == 0)
            {
                AppName = "Pseudocode";
                SureToEmptyList = "Erase the contents of the recent files list?\n\nJust the recent files list will be cleared off data, the actual files will stay at their respective places on the disk.";
                NoSuchFile = "The specified file was not found on the disk.\n\nThe file may have been deleted or moved.";
                DownloadModule = "Download and open a pseudocode file?\n\nFile name: ";
                Error = "An error has occured while processing your request.\n\nSign in to Dropbox using the same credintals you provided in Pseudocode.";
                FileNotForOpen = "The file could not be downloaded.\n\nThe file you requested is used by the Pseudocode application and is not meant to be opened from here.";
                ErrorDownloadingCode = "An error has occured when trying to download the #code.\n\nMake sure you are connected to the Internet, check the #code for typing errors and try again in a little while.\nError description: ";
                Downloaded = "(unsaved on the local storage)";
                AvailableDisks = "This PC";
                UnnamedDrive = "Untitled drive";
                UnpreparedDrive = "Unprepared drive";
                FrequentPlaces = "Frequent locations on this PC";
                PseudocodeFolderText = "Pseudocode folder";
                DesktopText = "Desktop";
                DocumentsText = "Documents";
                UseCodeText = "Use #code";
                FromCodeRepText = "From the Code Repository";
                FromMyDropboxText = "From my Dropbox";
                RecentEditsText = "Pseudocode files recently edited";
                Name = "Name";
                Created = "Date created";
                Modified = "Date modified";
                Size = "Size";
                DoesNotExist = "An error has occured while performing the operation.\n\nThe requested resource does not exist.";
                AccessDenied = "An error has occured while performing the operation.\n\nAccess to the requested resource is denied.";
                Eroare = "An error has occured while performing the operation.\n\n";
                UnavailableName = "Unavailable name";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                AppName = "Pseudocod";
                SureToEmptyList = "Curățați conținutul listei de fișiere recente?\n\nDoar lista va fi ștearsă, fișierele conținute într-aceasta vor rămâne intacte pe disc.";
                NoSuchFile = "Nu s-a găsit fișierul specificat pe disc.\n\nAcesta ar fi putut să fi fost mutat sau șters.";
                DownloadModule = "Descărcați și deschideți un fișier pseudocod?\n\nNume: ";
                Error = "A apărut o eroare la procesarea cererii dvs..\n\nFaceți sign in la Dropbox aici folosind aceleași acreditări folosite și în Pseudocod.";
                FileNotForOpen = "Fișierul nu a putut fi descărcat.\n\nFișierul cerut este folosit de către aplicația Pseudocod și nu este proiectat pentru a fi deschis de aici.";
                ErrorDownloadingCode = "A apărut o eroare la descărcarea #codului.\n\nVerificați conexiunea la Internet, revizuiți #codul pentru eventuale greșeli de scriere și încercați din nou mai târziu.\nDescriere eroare: ";
                Downloaded = "(nesalvat local)";
                AvailableDisks = "Acest PC";
                UnnamedDrive = "Unitate neintitulată";
                UnpreparedDrive = "Unitate nepregătită";
                FrequentPlaces = "Locații accesate frecvent pe acest PC";
                PseudocodeFolderText = "Folder Pseudocode";
                DesktopText = "Desktop";
                DocumentsText = "Documente";
                UseCodeText = "Folosire #cod";
                FromCodeRepText = "Din Depozitul de Cod";
                FromMyDropboxText = "Din Dropboxul meu";
                RecentEditsText = "Fișiere pseudocod editate recent";
                Name = "Nume";
                Created = "Data creării";
                Modified = "Data modificării";
                Size = "Dimensiune";
                DoesNotExist = "A apărut o eroare la procesarea cererii dvs.\n\nResursa solicitată nu există.";
                AccessDenied = "A apărut o eroare la procesarea cererii dvs.\n\nAccesul la resursa solicitată nu este permis.";
                Eroare = "A apărut o eroare la procesarea cererii dvs.\n\n";
                UnavailableName = "Nume indisponibil";
            }
            //load plugins
            try
            {
                _Plugins = new Dictionary<string, VPlugin>();
                //ICollection<IPlugin> plugins = PluginLoader.LoadPlugins("Plugins");
                ICollection<VPlugin> plugins = GenericPluginLoader<VPlugin>.LoadPlugins(Application.StartupPath);
                try
                {
                    foreach (var item in plugins)
                    {
                        _Plugins.Add(item.Name, item);
                    }
                }
                catch { }
            }
            catch { }
            //end load plugins
            if (Properties.Settings.Default.AppName != "")
            {
                AppName = Properties.Settings.Default.AppName;
            }
            string CloseText = "";
            if (Properties.Settings.Default.UILang == 0)
            {
                CloseText = "Close (Alt+F4)";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                CloseText = "Închidere (Alt+F4)";
            }
            //toolTip1.SetToolTip(Close, CloseText);
            /*var set = new BrowserSettings();
            set.DatabasesDisabled = false;
            set.ApplicationCacheDisabled = false;
            webView = new WebView("https://www.dropbox.com/home/Apps/InfoEdu", set);
            webView.PropertyChanged += new PropertyChangedEventHandler(webView_PropertyChanged);
            this.Controls.Add(webView);
            webView.Height = webBrowser2.Height - 20;
            webView.Width = webBrowser2.Width + 1;
            webView.Location = new Point(webBrowser2.Location.X, webBrowser2.Location.Y + 74);*/

        }
        // Get all propertys from https://github.com/chillitom/CefSharp/blob/master/CefSharp/BrowserCore.h
        /*void webView_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            BrowserCore core = (BrowserCore)sender;
            switch (e.PropertyName)
            {
                case "IsBrowserInitialized":
                    //core.IsBrowserInitialized
                    break;
                case "Title":
                    //core.Title
                    break;
                case "Address":
                    //core.Address
                    break;
                case "CanGoBack":
                    //core.CanGoBack
                    break;
                case "CanGoForward":
                    //core.CanGoForward;
                    break;
                case "IsLoading":
                    //core.IsLoading
                    break;
            }
        }*/
        private void superTabItem2_Click(object sender, EventArgs e)
        {

        }
        public void OpenFile(bool isRecentAlready)
        {
            try
            {
                System.IO.TextReader tw = new System.IO.StreamReader(openFileDialog1.FileName);
                //main.New();
                this.DialogResult = System.Windows.Forms.DialogResult.Ignore;
                string citit = tw.ReadToEnd();
                string[] parcurgere = citit.Split('\n');
                bool incepProp = false;
                Form1.fileProperties.Clear();
                for (int i = 0; i < parcurgere.Length; i++)
                {
                    if (parcurgere[i] == "<properties>") incepProp = true;
                    if (incepProp == true && parcurgere[i] != "<properties>") Form1.fileProperties.Add(parcurgere[i]);
                    else if (parcurgere[i] != "<properties>")
                        if (i == 0) Form1.pseudocodeEd.Text += parcurgere[i];
                        else Form1.pseudocodeEd.Text += "\n" + parcurgere[i];
                }
                //Form1.pseudocodeEd.Text = tw.ReadToEnd();
                Form1.pseudocodeEd.UndoRedo.EmptyUndoBuffer();
                ExTag oldEX = (ExTag)Form1.pseudocodeEd.Tag;
                ExTag exTag = new ExTag();
                exTag.Add("issaved", true);
                exTag.Add("filename", openFileDialog1.FileName);
                exTag.Add("uniqueString", oldEX.Get("uniqueString"));
                Form1.pseudocodeEd.Tag = exTag;
                tw.Close();
                FileInfo fi = new FileInfo(openFileDialog1.FileName);
                Form1.Text = Path.GetFileNameWithoutExtension(fi.Name);
               // Form1.superTabControl1.Tabs[Form1.superTabControl1.Tabs.Count - 1].Text = Path.GetFileNameWithoutExtension(fi.Name);
                if (isRecentAlready == false)
                {
                    StringCollection col = Properties.Settings.Default.RecentFiles;
                    bool isAdded = false;
                    foreach (string s in col)
                    {
                        if (s == openFileDialog1.FileName)
                        {
                            isAdded = true;
                        }
                    }
                    if (isAdded == false) col.Add(openFileDialog1.FileName);
                    Properties.Settings.Default.RecentFiles = col;
                    Properties.Settings.Default.Save();
                    AddFileToRecentFilesList(openFileDialog1.FileName);
                }
                else
                {
                    StringCollection col = Properties.Settings.Default.RecentFiles;
                    col.Remove(openFileDialog1.FileName);
                    col.Add(openFileDialog1.FileName);
                    Properties.Settings.Default.RecentFiles = col;
                    Properties.Settings.Default.Save();
                    AddFileToRecentFilesList(openFileDialog1.FileName);
                }
                Form1.flagReident = true;
                this.Close();
            }
            catch
            {
                CasetaDeMesaj(Form1, NoSuchFile, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                col.Remove(openFileDialog1.FileName);
                this.Close();
            }
        }
        private void Open_Load(object sender, EventArgs e)
        {
            col = Properties.Settings.Default.RecentFiles;
            int x = 0;
                for (int i = col.Count - 1; i >= 0; i--)
                {
                    
                    Button buttoni = new Button();
                    buttoni.Image = button2.Image;
                    using (Graphics g = this.CreateGraphics())
                    {
                        if (g.MeasureString(Path.GetFileNameWithoutExtension(col[i]), button2.Font).Width + 2 > button2.Width - 70)
                        {
                            string a = Path.GetFileNameWithoutExtension(col[i]);
                            while (g.MeasureString(a, button2.Font).Width + 2 > button2.Width - 70) a = a.Substring(0, a.Length - 1);
                            buttoni.Text = "          " + a + "...\n          " + col[i];
                        }
                        else buttoni.Text = "          " + Path.GetFileNameWithoutExtension(col[i]) + "\n          " + col[i];
                    }
                    buttoni.ForeColor = Color.White;
                    buttoni.BackColor = Color.FromArgb(64,64,64);
                    buttoni.ImageAlign = ContentAlignment.MiddleLeft;
                    buttoni.Size = button2.Size;
                    try
                    {
                        buttoni.Location = new Point(button[x - 1].Location.X, button[x - 1].Location.Y + button[x - 1].Height);
                    }
                    catch
                    {
                        buttoni.Location = button2.Location;
                    }
                    buttoni.FlatStyle = FlatStyle.Flat;
                    buttoni.FlatAppearance.BorderSize = 0;
                    buttoni.FlatAppearance.MouseOverBackColor = Color.Gray;
                    buttoni.FlatAppearance.MouseDownBackColor = Color.Gray;
                    buttoni.Font = button2.Font;
                    buttoni.TextAlign = ContentAlignment.MiddleLeft;
                    buttoni.Tag = col[i];
                    buttoni.Anchor = button2.Anchor;
                    buttoni.Click +=new EventHandler(button_Click);
                    buttoni.AutoEllipsis = true;
                    buttoni.Show();
                    buttoni.BringToFront();
                    label1.Visible = false;
                    panel2.Controls.Add(buttoni);
                    button.Add(buttoni);
                    x++;
                }
                //((SHDocVw.DWebBrowserEvents2_Event)webBrowser2.ActiveXInstance).NewWindow3 += MainWindow_NewWindow3;
                panel3.BackColor = Color.White;
                panel1.BackColor = Color.White;
                //panel2.BackColor = Color.White;
                panel4.BackColor = Color.White;
                //Close.BackColor = Color.FromArgb(199, 80, 80);
                button6.BackColor = Color.FromArgb(255, 192, 255);
                button22.BackColor = Color.FromArgb(224, 224, 224);
                treeView1.Nodes.Clear();
                TreeNode RecentEdits = new TreeNode();
                RecentEdits.Text = RecentEditsText;
                RecentEdits.Tag = "RecentEdits";
                RecentEdits.ImageIndex = 0;
                RecentEdits.SelectedImageIndex = RecentEdits.ImageIndex;
                treeView1.Nodes.Add(RecentEdits);
                TreeNode UseCode = new TreeNode();
                UseCode.Text = UseCodeText;
                UseCode.Tag = "UseCode";
                UseCode.ImageIndex = 1;
                UseCode.SelectedImageIndex = UseCode.ImageIndex;
                treeView1.Nodes.Add(UseCode);
                TreeNode FromCodeRep = new TreeNode();
                FromCodeRep.Text = FromCodeRepText;
                FromCodeRep.Tag = "FromCodeRep";
                FromCodeRep.ImageIndex = 2;
                FromCodeRep.SelectedImageIndex = FromCodeRep.ImageIndex;
                treeView1.Nodes.Add(FromCodeRep);
                TreeNode FromMyDropbox = new TreeNode();
                FromMyDropbox.Text = FromMyDropboxText;
                FromMyDropbox.Tag = "FromMyDropbox";
                FromMyDropbox.ImageIndex = 3;
                FromMyDropbox.SelectedImageIndex = FromMyDropbox.ImageIndex;
                treeView1.Nodes.Add(FromMyDropbox);
                TreeNode freq = new TreeNode();
                freq.Text = FrequentPlaces;
                freq.Tag = "/";
                freq.ImageIndex = GetSystemIcon(CSIDL.CSIDL_RECENT);
                freq.SelectedImageIndex = freq.ImageIndex;
                treeView1.Nodes.Add(freq);
                TreeNode PseudocodeFolder = new TreeNode();
                PseudocodeFolder.Text = PseudocodeFolderText;
                PseudocodeFolder.Tag = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode";
                PseudocodeFolder.ImageIndex = GetSystemIcon(CSIDL.CSIDL_SYSTEM);
                PseudocodeFolder.SelectedImageIndex = PseudocodeFolder.ImageIndex;
                PseudocodeFolder.Nodes.Add(" ");
                freq.Nodes.Add(PseudocodeFolder);
                TreeNode Desktop = new TreeNode();
                Desktop.Text = DesktopText;
                Desktop.Tag = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                Desktop.ImageIndex = GetSystemIcon(CSIDL.CSIDL_DESKTOP);
                Desktop.SelectedImageIndex = Desktop.ImageIndex;
                Desktop.Nodes.Add(" ");
                freq.Nodes.Add(Desktop);
                TreeNode Documents = new TreeNode();
                Documents.Text = DocumentsText;
                Documents.Tag = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                Documents.ImageIndex = GetSystemIcon(CSIDL.CSIDL_PERSONAL);
                Documents.SelectedImageIndex = Documents.ImageIndex;
                Documents.Nodes.Add(" ");
                freq.Nodes.Add(Documents);
                freq.Expand();
                TreeNode rootTree = new TreeNode();
                rootTree.Text = AvailableDisks;
                rootTree.Tag = "/";
                rootTree.ImageIndex = GetSystemIcon(CSIDL.CSIDL_DRIVES);
                rootTree.SelectedImageIndex = rootTree.ImageIndex;
                //rootTree.Nodes.Add(" "); //Placeholder to enable expanding (+)
                treeView1.Nodes.Add(rootTree);
                GetSystemIcon(CSIDL.CSIDL_SYSTEM);
                GetAllDrives(rootTree);
                rootTree.Expand();
                treeView1.ForeColor = Color.White;
                treeView1.SelectedNode = treeView1.Nodes[0];
                if (doAfterLoad == "pseudocode-folder") treeView1.SelectedNode = treeView1.Nodes[4].Nodes[0];
                if (doAfterLoad == "code-rep") treeView1.SelectedNode = treeView1.Nodes[2];
                if (doAfterLoad == "use-code") treeView1.SelectedNode = treeView1.Nodes[1];

        }

        public void button_Click(object sender, EventArgs e)
        {
            var Button = (Button)sender;
            openFileDialog1.FileName = (string)Button.Tag;
            OpenFile(true);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //CEF.Shutdown();
            this.Close();
        }
        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogResult dr = CasetaDeMesaj(Form1, SureToEmptyList, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                StringCollection col = new StringCollection();
                Properties.Settings.Default.RecentFiles = col;
                Properties.Settings.Default.Save();
                this.Close();
            }
        }
        void AddFileToRecentFilesList(string fileName)
        {
            SHAddToRecentDocs((uint)ShellAddRecentDocs.SHARD_PATHW, fileName);
        }

        /// <summary>
        /// Native call to add the file to windows' recent file list
        /// </summary>
        /// <param name="uFlags">Always use (uint)ShellAddRecentDocs.SHARD_PATHW</param>
        /// <param name="pv">path to file</param>
        [DllImport("shell32.dll")]
        public static extern void SHAddToRecentDocs(UInt32 uFlags,
            [MarshalAs(UnmanagedType.LPWStr)] String pv);

        enum ShellAddRecentDocs
        {
            SHARD_PIDL = 0x00000001,
            SHARD_PATHA = 0x00000002,
            SHARD_PATHW = 0x00000003
        }

        private void button22_Click(object sender, EventArgs e)
        {
            button22.BackColor = Color.FromArgb(224, 224, 224);
            button1.BackColor = Color.White;
            button3.BackColor = Color.White;
            panel1.Visible = true;
            panel2.Visible = true;
            panel5.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.BackColor = Color.FromArgb(224, 224, 224);
            button22.BackColor = Color.White;
            button3.BackColor = Color.White;
            panel1.Visible = false;
            panel2.Visible = false;
            panel5.Visible = false;
            if (Properties.Settings.Default.UILang == 0) webBrowser1.Navigate("http://www.valinet.ro/coderep/en-uk");
            if (Properties.Settings.Default.UILang == 1) webBrowser1.Navigate("http://www.valinet.ro/coderep/ro-ro");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    OpenFile(false);
                }
                catch
                {
                    //uperTabItem1.RaiseClick();
                }
            }
            else
            {
                //superTabControl1.SelectedTabIndex = 0;
            }
        }
        string filename;
        private void webBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {

        }

        private void webBrowser1_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (e.Url.ToString().EndsWith(".psc"))
            {
                Uri uri = new Uri(e.Url.ToString());
                filename = System.IO.Path.GetFileName(uri.LocalPath);
                e.Cancel = true;
                DialogResult dr = CasetaDeMesaj(Form1, DownloadModule + filename, "Pseudocod", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == System.Windows.Forms.DialogResult.Yes)
                {

                    Computer c = new Computer();
                    try
                    {
                        saveFileDialog1.FileName = filename;
                        if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            c.Network.DownloadFile(e.Url.ToString(), saveFileDialog1.FileName);
                            openFileDialog1.FileName = saveFileDialog1.FileName;
                            OpenFile(false);
                        }
                    }
                    catch { }

                }
                else
                {

                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {


        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            button3.BackColor = Color.FromArgb(224, 224, 224);
            button1.BackColor = Color.White;
            button22.BackColor = Color.White;
            panel1.Visible = false;
            panel2.Visible = false;
            panel5.Visible = true;
            webBrowser2.Navigate("https://www.dropbox.com/m/home?path=/Apps/InfoEdu");
            webBrowser2.Focus();
            /*webView.BringToFront();
            panel3.BringToFront();
            panel4.BringToFront();
            webView.Focus();*/
        }

        private void webBrowser2_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            //MessageBox.Show(e.Url.ToString());
            if (e.Url.ToString().StartsWith("https://www.dropbox.com/m/setregular?cont=/home/") | e.Url.ToString().StartsWith("https://www.dropbox.com/m/login?cont=https://www.dropbox.com/m/home?path=/Apps/InfoEdu#") | e.Url.ToString() == "https://www.dropbox.com/m/forgot" | e.Url.ToString().StartsWith("https://www.dropbox.com/m/setregular?cont=/login?display=desktop"))
            {
                e.Cancel = true;
                System.Diagnostics.Process.Start(e.Url.ToString());
            }
            if (e.Url.ToString().StartsWith("https://dl-web.dropbox.com/get/Apps/InfoEdu/"))
            {
                Uri uri = new Uri(e.Url.ToString());
                filename = System.IO.Path.GetFileName(uri.LocalPath);
                if (filename == "user.dat" | filename == "user.pic" | filename == "user.config")
                {
                    CasetaDeMesaj(Form1, FileNotForOpen, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = true;
                    DialogResult dr = CasetaDeMesaj(Form1, DownloadModule + filename, "Pseudocod", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {

                        Computer c = new Computer();
                        try
                        {
                            saveFileDialog1.FileName = filename;
                            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                var dropBoxStorage = new CloudStorage();
                                var dropBoxConfig = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox);
                                ICloudStorageAccessToken accessToken;
                                StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                                string t = sr.ReadToEnd();
                                sr.Close();
                                StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                                sw.Write(EncryptDecrypt.DecryptString(t));
                                sw.Close();
                                using (var fs = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat", FileMode.Open, FileAccess.Read, FileShare.None))
                                {
                                    accessToken = dropBoxStorage.DeserializeSecurityToken(fs);
                                }
                                StreamWriter sww = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                                sww.Write(t);
                                sww.Close();
                                try
                                {
                                    var storageToken = dropBoxStorage.Open(dropBoxConfig, accessToken);
                                }
                                catch { }
                                //try
                                //{
                                dropBoxStorage.DownloadFile("/" + filename, Path.GetDirectoryName(saveFileDialog1.FileName));
                                openFileDialog1.FileName = saveFileDialog1.FileName;
                                OpenFile(false);
                            }
                        }
                        catch
                        {
                            CasetaDeMesaj(Form1, Error, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            webBrowser2.Navigate("https://www.dropbox.com/logout");
                        }


                    }
                    else
                    {

                    }
                }
            }

        }
        private void MainWindow_NewWindow3(ref object ppDisp, ref bool Cancel, uint dwFlags, string bstrUrlContext, string bstrUrl)
        {
            //MessageBox.Show(e.Url.ToString());
            if (bstrUrl.ToString().StartsWith("https://dl-web.dropbox.com/get/Apps/InfoEdu/"))
            {
                Uri uri = new Uri(bstrUrl.ToString());
                filename = System.IO.Path.GetFileName(uri.LocalPath);
                if (filename == "user.dat" | filename == "user.pic" | filename == "user.config")
                {
                    CasetaDeMesaj(Form1, FileNotForOpen, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Cancel = true;
                }
                else
                {
                    Cancel = true;
                    DialogResult dr = CasetaDeMesaj(Form1, DownloadModule + filename, "Pseudocod", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {

                        Computer c = new Computer();
                        try
                        {
                            saveFileDialog1.FileName = filename;
                            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                var dropBoxStorage = new CloudStorage();
                                var dropBoxConfig = CloudStorage.GetCloudConfigurationEasy(nSupportedCloudConfigurations.DropBox);
                                ICloudStorageAccessToken accessToken;
                                StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                                string t = sr.ReadToEnd();
                                sr.Close();
                                StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                                sw.Write(EncryptDecrypt.DecryptString(t));
                                sw.Close();
                                using (var fs = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat", FileMode.Open, FileAccess.Read, FileShare.None))
                                {
                                    accessToken = dropBoxStorage.DeserializeSecurityToken(fs);
                                }
                                StreamWriter sww = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode" + "\\token.dat");
                                sww.Write(t);
                                sww.Close();
                                try
                                {
                                    var storageToken = dropBoxStorage.Open(dropBoxConfig, accessToken);
                                }
                                catch { }
                                //try
                                //{
                                dropBoxStorage.DownloadFile("/" + filename, Path.GetDirectoryName(saveFileDialog1.FileName));
                                openFileDialog1.FileName = saveFileDialog1.FileName;
                                OpenFile(false);
                            }
                        }
                        catch
                        {
                            CasetaDeMesaj(Form1, Error, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            webBrowser2.Navigate("https://www.dropbox.com/logout");
                        }


                    }
                    else
                    {

                    }
                }
            }
        }

        private void panel2_MouseEnter(object sender, EventArgs e)
        {
            //panel2.Focus();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (this.Tag.ToString() == "rep")
                {
                    button1.PerformClick();
                    webBrowser1.Focus();
                }
                if (this.Tag.ToString() == "drp")
                {
                    button3.PerformClick();
                    webBrowser2.Focus();
                }
            }
            catch
            { }
            timer1.Enabled = false;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }


        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
     /*   private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }*/

        private void button4_Click(object sender, EventArgs e)
        {
            UseCode uc = new UseCode(Form1.MdiParent);
            DialogResult nc = uc.ShowDialog();
            if (nc == System.Windows.Forms.DialogResult.Cancel)
            {
                if (uc.Tag != null) if (uc.Tag.ToString() != "") CasetaDeMesaj(Form1, ErrorDownloadingCode + uc.Tag, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if (nc == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    if (Properties.Settings.Default.LastCode == "paste not found")
                    {
                        if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(Form1, "The #code does not exist or does not point to a valid pseudocode file.\n\nOperation has been aborted.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        if (Properties.Settings.Default.UILang == 1) CasetaDeMesaj(Form1, "#Codul specificat de dvs. nu există sau nu conduce către un pseudocod valid.\n\nOperația a fost anulată.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    //System.IO.TextReader tw = new System.IO.StreamReader(openFileDialog1.FileName);
                    //Form1.New();
                    this.DialogResult = System.Windows.Forms.DialogResult.Ignore;
                    Form1.pseudocodeEd.Text = uc.Tag.ToString();
                    Form1.pseudocodeEd.UndoRedo.EmptyUndoBuffer();
                    ExTag oldEX = (ExTag)Form1.pseudocodeEd.Tag;
                    ExTag exTag = new ExTag();
                    exTag.Add("issaved", false);
                    exTag.Add("filename", "");
                    exTag.Add("uniqueString", oldEX.Get("uniqueString"));
                    Form1.pseudocodeEd.Tag = exTag;
                    Form1.Text = "#" + Properties.Settings.Default.LastCode + " " + Downloaded;
                    //Form1.superTabControl1.Tabs[Form1.superTabControl1.Tabs.Count - 1].Text = "#" + Properties.Settings.Default.LastCode + " " + Downloaded;
                    Form1.flagReident = true;
                    this.Close();
                }
                catch
                {
                    CasetaDeMesaj(Form1, NoSuchFile, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    col.Remove(openFileDialog1.FileName);
                    this.Close();
                }
            }
            else if (doAfterLoad == "use-code") { this.Visible = false; this.Close(); }
        }

        private void Open_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.Visible = false;


        }

        private void Close_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();

            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SHFILEINFO
        {
            public IntPtr hIcon;
            public IntPtr iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;
        };

        class Win32
        {
            public const uint SHGFI_ICON = 0x100;
            public const uint SHGFI_LARGEICON = 0x0;    // 'Large icon
            public const uint SHGFI_SMALLICON = 0x1;    // 'Small icon
            public const uint SHGFI_PIDL = 0x000000008;

            [DllImport("shell32.dll")]
            public static extern IntPtr SHGetFileInfo(string pszPath,
                                        uint dwFileAttributes,
                                        ref SHFILEINFO psfi,
                                        uint cbSizeFileInfo,
                                        uint uFlags);
            [DllImport("Shell32.dll")]
            public static extern IntPtr SHGetFileInfo(IntPtr pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbFileInfo, uint uFlags);
        }

        private int GetIconOfFile_Folder(string Path)
        {
            IntPtr hImgSmall;    //the handle to the system image list
            IntPtr hImgLarge;    //the handle to the system image list
            string fName;        // 'the file name to get icon from
            try
            {
                SHFILEINFO shinfo = new SHFILEINFO();

                //Use this to get the small Icon
                hImgSmall = Win32.SHGetFileInfo(Path, 0, ref shinfo,
                                               (uint)Marshal.SizeOf(shinfo),
                                                Win32.SHGFI_ICON |
                                                Win32.SHGFI_SMALLICON);

                //Use this to get the large Icon
                //hImgLarge = SHGetFileInfo(fName, 0,
                //ref shinfo, (uint)Marshal.SizeOf(shinfo),
                //Win32.SHGFI_ICON | Win32.SHGFI_LARGEICON);
                //The icon is returned in the hIcon member of the shinfo
                //struct
                System.Drawing.Icon myIcon =
                       System.Drawing.Icon.FromHandle(shinfo.hIcon);

                imageList1.Images.Add(myIcon);
            }
            catch
            {
                GetSystemIcon(CSIDL.CSIDL_SYSTEM);
            }
            return imageList1.Images.Count - 1;
        }
        [DllImport("shell32.dll", SetLastError = true)]
        static extern int SHGetSpecialFolderLocation(IntPtr hwndOwner, CSIDL nFolder,
           ref IntPtr ppidl);
        public enum CSIDL
        {
            CSIDL_DESKTOP = 0x0000,    // <desktop>
            CSIDL_INTERNET = 0x0001,    // Internet Explorer (icon on desktop)
            CSIDL_PROGRAMS = 0x0002,    // Start Menu\Programs
            CSIDL_CONTROLS = 0x0003,    // My Computer\Control Panel
            CSIDL_PRINTERS = 0x0004,    // My Computer\Printers
            CSIDL_PERSONAL = 0x0005,    // My Documents
            CSIDL_FAVORITES = 0x0006,    // <user name>\Favorites
            CSIDL_STARTUP = 0x0007,    // Start Menu\Programs\Startup
            CSIDL_RECENT = 0x0008,    // <user name>\Recent
            CSIDL_SENDTO = 0x0009,    // <user name>\SendTo
            CSIDL_BITBUCKET = 0x000a,    // <desktop>\Recycle Bin
            CSIDL_STARTMENU = 0x000b,    // <user name>\Start Menu
            CSIDL_MYDOCUMENTS = 0x000c,    // logical "My Documents" desktop icon
            CSIDL_MYMUSIC = 0x000d,    // "My Music" folder
            CSIDL_MYVIDEO = 0x000e,    // "My Videos" folder
            CSIDL_DESKTOPDIRECTORY = 0x0010,    // <user name>\Desktop
            CSIDL_DRIVES = 0x0011,    // My Computer
            CSIDL_NETWORK = 0x0012,    // Network Neighborhood (My Network Places)
            CSIDL_NETHOOD = 0x0013,    // <user name>\nethood
            CSIDL_FONTS = 0x0014,    // windows\fonts
            CSIDL_TEMPLATES = 0x0015,
            CSIDL_COMMON_STARTMENU = 0x0016,    // All Users\Start Menu
            CSIDL_COMMON_PROGRAMS = 0X0017,    // All Users\Start Menu\Programs
            CSIDL_COMMON_STARTUP = 0x0018,    // All Users\Startup
            CSIDL_COMMON_DESKTOPDIRECTORY = 0x0019,    // All Users\Desktop
            CSIDL_APPDATA = 0x001a,    // <user name>\Application Data
            CSIDL_PRINTHOOD = 0x001b,    // <user name>\PrintHood
            CSIDL_LOCAL_APPDATA = 0x001c,    // <user name>\Local Settings\Applicaiton Data (non roaming)
            CSIDL_ALTSTARTUP = 0x001d,    // non localized startup
            CSIDL_COMMON_ALTSTARTUP = 0x001e,    // non localized common startup
            CSIDL_COMMON_FAVORITES = 0x001f,
            CSIDL_INTERNET_CACHE = 0x0020,
            CSIDL_COOKIES = 0x0021,
            CSIDL_HISTORY = 0x0022,
            CSIDL_COMMON_APPDATA = 0x0023,    // All Users\Application Data
            CSIDL_WINDOWS = 0x0024,    // GetWindowsDirectory()
            CSIDL_SYSTEM = 0x0025,    // GetSystemDirectory()
            CSIDL_PROGRAM_FILES = 0x0026,    // C:\Program Files
            CSIDL_MYPICTURES = 0x0027,    // C:\Program Files\My Pictures
            CSIDL_PROFILE = 0x0028,    // USERPROFILE
            CSIDL_SYSTEMX86 = 0x0029,    // x86 system directory on RISC
            CSIDL_PROGRAM_FILESX86 = 0x002a,    // x86 C:\Program Files on RISC
            CSIDL_PROGRAM_FILES_COMMON = 0x002b,    // C:\Program Files\Common
            CSIDL_PROGRAM_FILES_COMMONX86 = 0x002c,    // x86 Program Files\Common on RISC
            CSIDL_COMMON_TEMPLATES = 0x002d,    // All Users\Templates
            CSIDL_COMMON_DOCUMENTS = 0x002e,    // All Users\Documents
            CSIDL_COMMON_ADMINTOOLS = 0x002f,    // All Users\Start Menu\Programs\Administrative Tools
            CSIDL_ADMINTOOLS = 0x0030,    // <user name>\Start Menu\Programs\Administrative Tools
            CSIDL_CONNECTIONS = 0x0031,    // Network and Dial-up Connections
            CSIDL_COMMON_MUSIC = 0x0035,    // All Users\My Music
            CSIDL_COMMON_PICTURES = 0x0036,    // All Users\My Pictures
            CSIDL_COMMON_VIDEO = 0x0037,    // All Users\My Video
            CSIDL_CDBURN_AREA = 0x003b    // USERPROFILE\Local Settings\Application Data\Microsoft\CD Burning
        }
        private int GetSystemIcon(CSIDL cs)
        {
            IntPtr hImgSmall;    //the handle to the system image list
            IntPtr hImgLarge;    //the handle to the system image list
            string fName;        // 'the file name to get icon from
            SHFILEINFO shinfo = new SHFILEINFO();

            //Use this to get the small Icon
            IntPtr ip = IntPtr.Zero;
            SHGetSpecialFolderLocation(IntPtr.Zero, cs, ref ip);
            hImgSmall = Win32.SHGetFileInfo(ip, 0, ref shinfo,
                                           (uint)Marshal.SizeOf(shinfo),
                                            Win32.SHGFI_ICON |
                                            Win32.SHGFI_SMALLICON |
                                            Win32.SHGFI_PIDL);

            //Use this to get the large Icon
            //hImgLarge = SHGetFileInfo(fName, 0,
            //ref shinfo, (uint)Marshal.SizeOf(shinfo),
            //Win32.SHGFI_ICON | Win32.SHGFI_LARGEICON);
            //The icon is returned in the hIcon member of the shinfo
            //struct
            System.Drawing.Icon myIcon =
                   System.Drawing.Icon.FromHandle(shinfo.hIcon);

            imageList1.Images.Add(myIcon);

            return imageList1.Images.Count - 1;
        }
        private void GetAllDrives(TreeNode rootTree)
        {
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (var drive in drives)
            {
                TreeNode rootTreeNode = new TreeNode();
                if (drive.IsReady == true)
                {
                    try
                    {
                        if (drive.VolumeLabel != "") rootTreeNode.Text = drive.VolumeLabel + " (" + drive.Name + ")";
                        else rootTreeNode.Text = UnnamedDrive + " (" + drive.Name + ")";
                    }
                    catch
                    {
                        rootTreeNode.Text = UnnamedDrive + " (" + drive.Name + ")";
                    }
                }
                else
                {
                    rootTreeNode.Text = UnpreparedDrive + " (" + drive.Name + ")";
                }
                rootTreeNode.Tag = drive.Name;
                rootTreeNode.ImageIndex = GetIconOfFile_Folder(drive.Name);
                rootTreeNode.SelectedImageIndex = rootTreeNode.ImageIndex;
                rootTreeNode.Nodes.Add(" "); //Placeholder to enable expanding (+)
                rootTree.Nodes.Add(rootTreeNode);
            }
        }

        private void GetFilesAndFolder(TreeNode tn, string Path)
        {
            try
            {
                string[] Directories = Directory.GetDirectories(Path);
                string[] Files = Directory.GetFiles(Path);

                foreach (string dir in Directories)
                {
                    TreeNode dirTreeNode = new TreeNode();
                    dirTreeNode.Tag = dir;
                    dirTreeNode.Text = new DirectoryInfo(dir).Name;
                    dirTreeNode.ImageIndex = 5;
                    dirTreeNode.SelectedImageIndex = dirTreeNode.ImageIndex;
                    dirTreeNode.Nodes.Add(" ");
                    tn.Nodes.Add(dirTreeNode);
                }

                /*  foreach (string file in Files)
                  {
                      TreeNode fileTreeNode = new TreeNode();
                      fileTreeNode.Tag = file;
                      fileTreeNode.Text = new FileInfo(file).Name;
                      fileTreeNode.ImageIndex = 1;
                      fileTreeNode.SelectedImageIndex = fileTreeNode.ImageIndex;
                      tn.Nodes.Add(fileTreeNode);
                  }*/
            }
            catch (Exception ex)
            {
                //  MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool CheckIfPathIsFile(string Path)
        {
            try
            {
                // get the file attributes for file or directory
                FileAttributes attr = File.GetAttributes(Path);

                //detect whether its a directory or file
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }
        protected void InitListView()
        {
            //init ListView control
            lvFiles.Clear();		//clear control
            //create column header for ListView
            lvFiles.Columns.Add(Name, 150, System.Windows.Forms.HorizontalAlignment.Left);
            lvFiles.Columns.Add(Size, 75, System.Windows.Forms.HorizontalAlignment.Right);
            lvFiles.Columns.Add(Created, 120, System.Windows.Forms.HorizontalAlignment.Left);
            lvFiles.Columns.Add(Modified, 120, System.Windows.Forms.HorizontalAlignment.Left);

        }
        protected void PopulateFilesFiltered(TreeNode nodeCurrent, string Filter)
        {
            //Populate listview with files
            string[] lvData = new string[4];

            //clear list
            InitListView();

            if (nodeCurrent.SelectedImageIndex != 0)
            {
                //check path
                if (Directory.Exists(nodeCurrent.Tag.ToString()) == false)
                {
                    //   MessageBox.Show("Directory or path " + nodeCurrent.Tag.ToString() + " does not exist.");
                }
                else
                {
                    try
                    {
                        string[] stringFiles = Directory.GetFiles(getFullPath(nodeCurrent.Tag.ToString()));
                        string stringFileName = "";
                        DateTime dtCreateDate, dtModifyDate;
                        Int64 lFileSize = 0;

                        //loop throught all files
                        foreach (string stringFile in stringFiles)
                        {
                            if ((stringFile.EndsWith(".psc") || stringFile.EndsWith(".pscx")) && stringFile.ToLower().Contains(Filter.ToLower()))
                            {
                                stringFileName = stringFile;
                                FileInfo objFileSize = new FileInfo(stringFileName);
                                lFileSize = objFileSize.Length;
                                dtCreateDate = objFileSize.CreationTime; //GetCreationTime(stringFileName);
                                dtModifyDate = objFileSize.LastWriteTime; //GetLastWriteTime(stringFileName);

                                //create listview data
                                lvData[0] = GetPathName(stringFileName);
                                lvData[1] = formatSize(lFileSize);

                                //check if file is in local current day light saving time
                                if (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dtCreateDate) == false)
                                {
                                    //not in day light saving time adjust time
                                    lvData[2] = formatDate(dtCreateDate.AddHours(1));
                                }
                                else
                                {
                                    //is in day light saving time adjust time
                                    lvData[2] = formatDate(dtCreateDate);
                                }

                                //check if file is in local current day light saving time
                                if (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dtModifyDate) == false)
                                {
                                    //not in day light saving time adjust time
                                    lvData[3] = formatDate(dtModifyDate.AddHours(1));
                                }
                                else
                                {
                                    //not in day light saving time adjust time
                                    lvData[3] = formatDate(dtModifyDate);
                                }


                                //Create actual list item
                                ListViewItem lvItem = new ListViewItem(lvData, 0);
                                lvFiles.Items.Add(lvItem);
                            }

                        }
                    }
                    catch (IOException e)
                    {
                        CasetaDeMesaj(this, DoesNotExist, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (UnauthorizedAccessException e)
                    {
                        CasetaDeMesaj(this, AccessDenied, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception e)
                    {
                        CasetaDeMesaj(this, Eroare + e.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        protected void PopulateFiles(TreeNode nodeCurrent)
        {
            //Populate listview with files
            string[] lvData = new string[4];

            //clear list
            InitListView();
            try
            {
                if (nodeCurrent.SelectedImageIndex != 0)
                {
                    //check path
                    if (Directory.Exists(nodeCurrent.Tag.ToString()) == false)
                    {
                        //   MessageBox.Show("Directory or path " + nodeCurrent.Tag.ToString() + " does not exist.");
                    }
                    else
                    {
                        try
                        {
                            string[] stringFiles = Directory.GetFiles(getFullPath(nodeCurrent.Tag.ToString()));
                            string stringFileName = "";
                            DateTime dtCreateDate, dtModifyDate;
                            Int64 lFileSize = 0;

                            //loop throught all files
                            foreach (string stringFile in stringFiles)
                            {
                                if (stringFile.EndsWith(".psc") || stringFile.EndsWith(".pscx"))
                                {
                                    stringFileName = stringFile;
                                    FileInfo objFileSize = new FileInfo(stringFileName);
                                    lFileSize = objFileSize.Length;
                                    dtCreateDate = objFileSize.CreationTime; //GetCreationTime(stringFileName);
                                    dtModifyDate = objFileSize.LastWriteTime; //GetLastWriteTime(stringFileName);

                                    //create listview data
                                    lvData[0] = GetPathName(stringFileName);
                                    lvData[1] = formatSize(lFileSize);

                                    //check if file is in local current day light saving time
                                    if (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dtCreateDate) == false)
                                    {
                                        //not in day light saving time adjust time
                                        lvData[2] = formatDate(dtCreateDate.AddHours(1));
                                    }
                                    else
                                    {
                                        //is in day light saving time adjust time
                                        lvData[2] = formatDate(dtCreateDate);
                                    }

                                    //check if file is in local current day light saving time
                                    if (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dtModifyDate) == false)
                                    {
                                        //not in day light saving time adjust time
                                        lvData[3] = formatDate(dtModifyDate.AddHours(1));
                                    }
                                    else
                                    {
                                        //not in day light saving time adjust time
                                        lvData[3] = formatDate(dtModifyDate);
                                    }


                                    //Create actual list item
                                    ListViewItem lvItem = new ListViewItem(lvData, 0);
                                    lvFiles.Items.Add(lvItem);
                                }

                            }
                        }
                        catch (IOException e)
                        {
                            CasetaDeMesaj(this, DoesNotExist, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (UnauthorizedAccessException e)
                        {
                            CasetaDeMesaj(this, AccessDenied, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (Exception e)
                        {
                            CasetaDeMesaj(this, Eroare + e.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch { }
        }
        protected string GetPathName(string stringPath)
        {
            //Get Name of folder
            string[] stringSplit = stringPath.Split('\\');
            int _maxIndex = stringSplit.Length;
            return stringSplit[_maxIndex - 1];
        }
        protected string formatDate(DateTime dtDate)
        {
            //Get date and time in short format
            string stringDate = "";

            stringDate = dtDate.ToShortDateString().ToString() + " " + dtDate.ToShortTimeString().ToString();

            return stringDate;
        }
        protected void PopulateDirectory(TreeNode nodeCurrent, TreeNodeCollection nodeCurrentCollection)
        {
            TreeNode nodeDir;
            int imageIndex = 2;		//unselected image index
            int selectIndex = 3;	//selected image index

            if (nodeCurrent.SelectedImageIndex != 0)
            {
                //populate treeview with folders
                try
                {
                    //check path
                    if (Directory.Exists(nodeCurrent.Tag.ToString()) == false)
                    {
                        //    MessageBox.Show("Directory or path " + nodeCurrent.Tag.ToString() + " does not exist.");
                    }
                    else
                    {
                        //populate files
                        if (SearchBox.Text == DefaultSearchBoxText) PopulateFiles(nodeCurrent);
                        else PopulateFilesFiltered(nodeCurrent, SearchBox.Text);

                        /*  string[] stringDirectories = Directory.GetDirectories(getFullPath(nodeCurrent.FullPath));
                          string stringFullPath = "";
                          string stringPathName = "";

                          //loop throught all directories
                          foreach (string stringDir in stringDirectories)
                          {
                              stringFullPath = stringDir;
                              stringPathName = GetPathName(stringFullPath);

                              //create node for directories
                              nodeDir = new TreeNode(stringPathName.ToString(), imageIndex, selectIndex);
                              nodeCurrentCollection.Add(nodeDir);
                          }*/
                    }
                }
                catch (IOException e)
                {
                    CasetaDeMesaj(this, DoesNotExist, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (UnauthorizedAccessException e)
                {
                    CasetaDeMesaj(this, AccessDenied, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception e)
                {
                    CasetaDeMesaj(this, Eroare + e.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        protected string formatSize(Int64 lSize)
        {
            //Format number to KB
            string stringSize = "";
            NumberFormatInfo myNfi = new NumberFormatInfo();

            Int64 lKBSize = 0;

            if (lSize < 1024)
            {
                if (lSize == 0)
                {
                    //zero byte
                    stringSize = "0";
                }
                else
                {
                    //less than 1K but not zero byte
                    stringSize = "1";
                }
            }
            else
            {
                //convert to KB
                lKBSize = lSize / 1024;
                //format number with default format
                stringSize = lKBSize.ToString("n", myNfi);
                //remove decimal
                stringSize = stringSize.Replace(".00", "");
            }

            return stringSize + " KB";

        }
        protected string getFullPath(string stringPath)
        {
            //Get Full path
            string stringParse = "";
            //remove My Computer from path.
            stringParse = stringPath.Replace(AvailableDisks + "\\", "");

            return stringParse;
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag.ToString() != "/" && e.Node.Tag.ToString() != "UseCode" && e.Node.Tag.ToString() != "FromCodeRep" && e.Node.Tag.ToString() != "FromMyDropbox" && e.Node.Tag.ToString() != "RecentEdits")
            {
                OKNew.Visible = true;
                TreePanel.Visible = true;
                CodeRepPanel.Visible = false;
                RecentPanel.Visible = false;
                TreePanel.BringToFront();
                PopulateDirectory(e.Node, e.Node.Nodes);
                if (lvFiles.Items.Count == 0) labelNoFile.BringToFront();
                else labelNoFile.SendToBack();
            }
            else
            {
                OKNew.Visible = false;
                TreePanel.Visible = false;
                CodeRepPanel.Visible = false;
                DefaultPanel.Visible = true;
                DefaultPanel.BringToFront();
                if (e.Node.Tag.ToString() == "RecentEdits")
                {
                    DefaultPanel.Visible = false;
                    TreePanel.Visible = false;
                    CodeRepPanel.Visible = false;
                    RecentPanel.Visible = true;
                    RecentPanel.BringToFront();
                }
                if (e.Node.Tag.ToString() == "UseCode")
                {
                    //plugin executor
                    foreach (KeyValuePair<string, VPlugin> pair in _Plugins)
                    {
                        if (_Plugins.ContainsKey(pair.Key))
                        {
                            VPlugin plugin = _Plugins[pair.Key];
                            string rez = plugin.Perform("com.valinet.pseudocode.open-code-tag", "");
                            if (rez == "true")
                            {
                                return;
                            }
                        }
                    }
                    //plugin executor finish
                    button4.PerformClick();
                }
                if (e.Node.Tag.ToString() == "FromCodeRep")
                {
                    OKNew.Visible = false;
                    TreePanel.Visible = false;
                    CodeRepPanel.Visible = true;
                    RecentPanel.Visible = false;
                    DefaultPanel.Visible = false;
                    CodeRepPanel.BringToFront();
                    webBrowser1.BringToFront();
                    if (Properties.Settings.Default.UILang == 0) webBrowser1.Navigate("http://www.valinet.ro/coderep/en-uk");
                    if (Properties.Settings.Default.UILang == 1) webBrowser1.Navigate("http://www.valinet.ro/coderep/ro-ro");
                }
                if (e.Node.Tag.ToString() == "FromMyDropbox")
                {
                    OKNew.Visible = false;
                    TreePanel.Visible = false;
                    CodeRepPanel.Visible = true;
                    RecentPanel.Visible = false;
                    DefaultPanel.Visible = false;
                    CodeRepPanel.BringToFront();
                    webBrowser2.BringToFront();
                    webBrowser2.Navigate("https://www.dropbox.com/m/home?path=/Apps/InfoEdu");
                }
            }
        }

        private void SearchBox_Enter(object sender, EventArgs e)
        {
            if (CodeRepPanel.Visible == true)
            {
                webBrowser1.Select();
                SendKeys.Send("^f");
                //return;
            }
            else if (SearchBox.Text == DefaultSearchBoxText) SearchBox.Text = "";

        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText)
            {
                SearchBox.Text = "";
                this.Text = DefaultText;
            }
            if (e.KeyCode == Keys.Enter) e.SuppressKeyPress = true;
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                SearchBox.Text = DefaultSearchBoxText;
                this.Text = DefaultText;
            }
        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (TreePanel.Visible == true)
            {
                if (e.KeyCode == Keys.Down)
                {
                    try
                    {
                        lvFiles.Items[0].Selected = true;
                    }
                    catch { }
                    lvFiles.Focus();
                    e.SuppressKeyPress = true;
                    return;
                }
                if (SearchBox.Text != "" && SearchBox.Text != DefaultSearchBoxText)
                {
                    PopulateFilesFiltered(treeView1.SelectedNode, SearchBox.Text);
                }
            }
            if (RecentPanel.Visible == true)
            {
                //button = new Button[100];
                foreach (Button b in button) b.Dispose();
                button = new List<Button>();
                col = Properties.Settings.Default.RecentFiles;
                int x = 0;
                for (int i = col.Count - 1; i >= 0; i--)
                {
                    if (Path.GetFileNameWithoutExtension(col[i]).ToLower().Contains(SearchBox.Text.ToLower()))
                    {
                        Button buttoni = new Button();
                        buttoni.Image = button2.Image;
                        buttoni.Text = "          " + Path.GetFileNameWithoutExtension(col[i]) + "\n          " + col[i];
                        buttoni.ForeColor = Color.White;
                        buttoni.BackColor = Color.FromArgb(64, 64, 64);
                        buttoni.ImageAlign = ContentAlignment.MiddleLeft;
                        buttoni.Size = button2.Size;
                        try
                        {
                            buttoni.Location = new Point(button[x - 1].Location.X, button[x - 1].Location.Y + button[x - 1].Height);
                        }
                        catch
                        {
                            buttoni.Location = button2.Location;
                        }
                        buttoni.FlatStyle = FlatStyle.Flat;
                        buttoni.FlatAppearance.BorderSize = 0;
                        buttoni.FlatAppearance.MouseOverBackColor = Color.Gray;
                        buttoni.FlatAppearance.MouseDownBackColor = Color.Gray;
                        buttoni.Font = button2.Font;
                        buttoni.TextAlign = ContentAlignment.MiddleLeft;
                        buttoni.Tag = col[i];
                        buttoni.Anchor = button2.Anchor;
                        buttoni.Click += new EventHandler(button_Click);
                        buttoni.AutoEllipsis = true;
                        buttoni.Show();
                        buttoni.BringToFront();
                        label1.Visible = false;
                        panel2.Controls.Add(buttoni);
                        button.Add(buttoni);
                        x++;
                    }
                }
            }
            if (SearchBox.Text == "")
            {
                SearchBox.Text = DefaultSearchBoxText;
            }


        }

        private void SearchBox_Leave(object sender, EventArgs e)
        {
        /*    foreach (Control ctl in this.Controls)
            {
                if (ctl.RectangleToScreen(ctl.ClientRectangle).Contains(Cursor.Position))
                {
                    MessageBox.Show(ctl.Name);
                }
            }*/
        }

        private void label3_Click(object sender, EventArgs e)
        {
            SearchBox.Text = DefaultSearchBoxText;
            PopulateFiles(treeView1.SelectedNode);
            if (lvFiles.Items.Count == 0) labelNoFile.BringToFront();
            else labelNoFile.SendToBack();
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {

            if (e.Node.Tag.ToString() != "/" && e.Node.Tag.ToString() != "UseCode" && e.Node.Tag.ToString() != "FromCodeRep" && e.Node.Tag.ToString() != "FromMyDropbox" && e.Node.Tag.ToString() != "RecentEdits")
            {
                e.Node.Nodes.Clear();
                GetFilesAndFolder(e.Node, (string)e.Node.Tag);
                PopulateDirectory(e.Node, e.Node.Nodes);
            }
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (CheckIfPathIsFile(e.Node.Tag.ToString()) == true) //If the Tag (Path) is a File
            {
                //Do something with the Path (close this Form + return Path)
            }
        }

        private void labelErase_Click(object sender, EventArgs e)
        {
            SearchBox.Text = DefaultSearchBoxText;
            PopulateFiles(treeView1.SelectedNode);
            if (lvFiles.Items.Count == 0) labelNoFile.BringToFront();
            else labelNoFile.SendToBack();
            //if (RecentPanel.Visible == true)
           // {
                //button = new Button[100];
                foreach (Button b in button) b.Dispose();
                button = new List<Button>();
                col = Properties.Settings.Default.RecentFiles;
                int x = 0;
                for (int i = col.Count - 1; i >= 0; i--)
                {

                        Button buttoni = new Button();
                        buttoni.Image = button2.Image;
                        buttoni.Text = "          " + Path.GetFileNameWithoutExtension(col[i]) + "\n          " + col[i];
                        buttoni.ForeColor = Color.White;
                        buttoni.BackColor = Color.FromArgb(64, 64, 64);
                        buttoni.ImageAlign = ContentAlignment.MiddleLeft;
                        buttoni.Size = button2.Size;
                        try
                        {
                            buttoni.Location = new Point(button[x - 1].Location.X, button[x - 1].Location.Y + button[x - 1].Height);
                        }
                        catch
                        {
                            buttoni.Location = button2.Location;
                        }
                        buttoni.FlatStyle = FlatStyle.Flat;
                        buttoni.FlatAppearance.BorderSize = 0;
                        buttoni.FlatAppearance.MouseOverBackColor = Color.Gray;
                        buttoni.FlatAppearance.MouseDownBackColor = Color.Gray;
                        buttoni.Font = button2.Font;
                        buttoni.TextAlign = ContentAlignment.MiddleLeft;
                        buttoni.Tag = col[i];
                        buttoni.Anchor = button2.Anchor;
                        buttoni.Click += new EventHandler(button_Click);
                        buttoni.AutoEllipsis = true;
                        buttoni.Show();
                        buttoni.BringToFront();
                        label1.Visible = false;
                        panel2.Controls.Add(buttoni);
                        button.Add(buttoni);
                        x++;
                    
                }
           // }
        }

        private void OKNew_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.FileName = getFullPath(treeView1.SelectedNode.Tag.ToString()) + "\\" + lvFiles.SelectedItems[0].Text;
                OpenFile(false);
            }
            catch { }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.Q))
            {
                //SearchBox.Text = "";
                SearchBox.Select();
                return true;
            }
            if (keyData == (Keys.F1))
            {
                if (Directory.Exists(Application.StartupPath + "\\com.valinet.pseudocode.help"))
                {
                    MainForm mf = Form1.MdiParent as MainForm;
                    if (mf.hp == null) mf.hp = new Help();
                    mf.hp.Show();
                    if (mf.WindowState != FormWindowState.Maximized)
                    {
                        mf.hp.Location = mf.Location;
                        mf.hp.Size = mf.Size;
                    }
                    mf.hp.WindowState = mf.WindowState;
                }
                else
                {
                    HelpOverview ho = new HelpOverview();
                    ho.ShowDialog();
                }
                return true;
            }
            return false;
        }
    }
}
