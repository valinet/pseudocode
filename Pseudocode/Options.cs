﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class Options : Form
    {
        string DefaultSearchBoxText;
        string DefaultText;
        string MesajEroare;
        string NoBrowserInstalled;
        string AppName;
        string FileAscSuccess;
        string WarningAdvancedUSersOnly;
        string ServerAddress;
        string AppBusy;
        string RestoreComplete;
        string RestartNeeded;
        string ErrorImportExport;
        string ImportSuccess;
        MainForm mf;
        public Options(MainForm frm)
        {
            if (Properties.Settings.Default.UILang == 0) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            if (Properties.Settings.Default.UILang == 1) System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");

            InitializeComponent();
            DefaultSearchBoxText = SearchBox.Text;
            DefaultText = this.Text;
            foreach (Controls.Development.ImageListBoxItem lb in listBox1.Items)
            {
                lista.Add(lb);
            }
            mf = frm;
            if (Properties.Settings.Default.UILang == 0)
            {
                MesajEroare = "An error has occured and the operation did not complete!\n\n";
                NoBrowserInstalled = "There is no Internet browser installed on this computer. In order to use this feature, a browser has to be installed on your machine. Visit www.whatbrowser.org to download a web browser for your computer.";
                AppName = "Pseudocode";
                FileAscSuccess = "Pseudocode is now the default viewer for PSC and PSCPACKAGE files.\n\nYou may need to do a one-time process of right-clicking a PSC file and then clicking Open with-Choose default application and selecting Pseudocode, if you have had a default application set for handling PSC or PSCPACKAGE files.";
                WarningAdvancedUSersOnly = "Kepp in mind that the following settings are for advanced users only.\n\nShould the application warn you again in the future about this?";
                ServerAddress = "Write new server address here. Keep in mind that Pseudocode will try to get updates from that address instead the default one.";
                AppBusy = "The application is busy right now, trying to fullfill a previous request.\n\nWait some time and try again.";
                RestoreComplete = "The settings have been successfully restored.\n\nPress OK to restart the application.";
                RestartNeeded = "Restart the application?\n\nIn order to apply the new settings, the application needs to restart. Save your work before proceeding.";
                ErrorImportExport = "An error has occured while importingexporting the settings file.\n\n";
                ImportSuccess = "The settings have been successfully imported.\n\nIn order for the settings to become effective, close the application, and then reopen it again.";
            }
            if (Properties.Settings.Default.UILang == 1)
            {
                MesajEroare = "A apărut o eroare și operațiunea nu s-a finalizat!\n\n";
                NoBrowserInstalled = "Niciun browser Web nu este instalat pe acest computer.\n\nPentru a utiliza această caracteristică, un browser trebuie să fie instalat pe computer. Pentru a descărca un browser, vizitați www.whatbrowser.org.";
                AppName = "Pseudocod";
                FileAscSuccess = "Pseudocode este acum editorul implicit pentru fișiere PSC și PSCPACKAGE.\n\nÎn continuare, ar putea fi nevoie să faceți o singură dată, click-dreapta pe fișierul pe care doriți să îl deschideți, să alegeți Deschidere cu-Alegere aplicație implicită și să alegeți Pseudocode ca aplicație implicită pentru deschiderea fișierelor PSC sau PSCPACKAGE.";
                WarningAdvancedUSersOnly = "Rețineți că următoarele setări sunt destinate spre a fi editate numai de către utilizatori avansați.\n\nVi se amintește pe viitor acest lucru când accesați aceste setări?";
                ServerAddress = "Scrieți noua adresă. Pseudocod va descărca actualizările viitoare de la această adresă.";
                AppBusy = "Aplicația este ocupată în acest moment, încercând îndeplinirea unei cereri precedente.\n\nAșteptați un timp și încercați din nou.";
                RestoreComplete = "Setările au fost restaurate cu succes.\n\nApăsați OK pentru a reporni aplicația.";
                RestartNeeded = "Reporniți aplicația?\n\nPentru a aplica noile setări, aplicația trebuie repornită. Salvați-vă munca înainte de a continua.";
                ErrorImportExport = "A apărut o eroare în procesul de import/export.\n\n";
                ImportSuccess = "Setările au fost importate cu succes.\n\nPentru ca noile setări să fie efective, închideți aplicația, apoi redeschideți-o.";
            }
        }

        private void Options_Load(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = 0;
            checkBox4.Checked = Properties.Settings.Default.DownloadImages;
            checkBox1.Checked
 = Properties.Settings.Default.Position;
            checkBox2.Checked
             = Properties.Settings.Default.AutoSignIn;
            checkBox3.Checked
             = !Properties.Settings.Default.WasWelcomeShown;
            checkBox10.Checked
             = Properties.Settings.Default.ShowStatusBar;
            checkBox9.Checked
             = Properties.Settings.Default.ShowCommandBar;
            checkBox8.Checked
             = Properties.Settings.Default.ShowAdvancedPanel;
            checkBox11.Checked
             = Properties.Settings.Default.ShowConvertedCode;
            checkBox25.Checked
             = Properties.Settings.Default.ShowQuickSearch;
            numericUpDown1.Value
             = Properties.Settings.Default.AutoSaveInterval;
            checkBox14.Checked
             = Properties.Settings.Default.UseBar;
            checkBox7.Checked
             = Properties.Settings.Default.ShowVisualScrollBar;
            checkBox5.Checked
             = Properties.Settings.Default.PopulateFind;
            checkBox17.Checked
             = Properties.Settings.Default.ReportByDefault;
            checkBox18.Checked
             = Properties.Settings.Default.RestoreByDefault;
            checkBox19.Checked
             = Properties.Settings.Default.ReportToMER;
            checkBox20.Checked
             = Properties.Settings.Default.ContinueExecutionOnCrash;
            checkBox21.Checked = Properties.Settings.Default.DisableCrashReporter;
            comboBox2.SelectedIndex = Properties.Settings.Default.UILang;
            comboBox3.SelectedIndex = Properties.Settings.Default.PseudocodeLang;
            comboBox4.SelectedIndex = Properties.Settings.Default.UITheme;
            checkBox23.Checked = Properties.Settings.Default.DynamicColor;
            textBox5.Text = Properties.Settings.Default.HomePage;
            if (Properties.Settings.Default.ServerPath == "http://www.valinet.ro/repo/official/")
            {
                comboBox1.SelectedIndex = 0;
            }
            else
            {
                if (Properties.Settings.Default.ServerPath == "http://www.valinet.ro/repo/viabeta/")
                {
                    comboBox1.SelectedIndex = 1;
                }
                else
                {
                    comboBox1.SelectedIndex = 2;
                }
            }
            foreach (Control ct in this.Controls)
            {
                if (ct.Parent == this && ct.GetType() == typeof(Panel))
                {
                    ct.Height = listBox1.Height + listBox1.Top - ct.Top;
                }
            }
            textBox4.Text = Properties.Settings.Default.MinGWPath;
            checkBox22.Checked = Properties.Settings.Default.AeroShake;
            if (Properties.Settings.Default.PerfOptions == 0)
            {
                radioButton1.Checked = true;
                radioButton2.Checked = false;
                radioButton3.Checked = false;
            }
            if (Properties.Settings.Default.PerfOptions == 1)
            {
                radioButton2.Checked = true;
                radioButton1.Checked = false;
                radioButton3.Checked = false;
            }
            if (Properties.Settings.Default.PerfOptions == 2)
            {
                radioButton3.Checked = true;
                radioButton2.Checked = false;
                radioButton1.Checked = false;
            }
        }

        private void checkBox18_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void treeView1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

        }
        Control ctrl;
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText)
            {
                if (listBox1.SelectedIndex == 0)
                {
                    listBox1.SelectedIndex = 1;
                    Application0Startup.BringToFront();
                }
                if (listBox1.SelectedIndex == 1) Application0Startup.BringToFront();
                if (listBox1.SelectedIndex == 5) Application0Interface.BringToFront();
                if (listBox1.SelectedIndex == 2) Application0Sync.BringToFront();
                if (listBox1.SelectedIndex == 3) Application0FileAssociations.BringToFront();
                if (listBox1.SelectedIndex == 4) Application0ExtensionManager.BringToFront();
                //if (listBox1.SelectedIndex == 6) Application0Codeboard.BringToFront();
                if (listBox1.SelectedIndex == 6) Application0ImportExportSettings.BringToFront();
                if (listBox1.SelectedIndex == 7)
                {
                    listBox1.SelectedIndex = 8;
                    Editor0AutoSave.BringToFront();
                }
                if (listBox1.SelectedIndex == 8) Editor0AutoSave.BringToFront();
                if (listBox1.SelectedIndex == 9) Editor0VisualExperience.BringToFront();
                if (listBox1.SelectedIndex == 10) Editor0Findreplace.BringToFront();
               /* if (listBox1.SelectedIndex == 10)
                {
                    listBox1.SelectedIndex = 11;
                    CodeConversion0Headers.BringToFront();
                }
                if (listBox1.SelectedIndex == 11) CodeConversion0Headers.BringToFront();
                if (listBox1.SelectedIndex == 12) CodeConversion0LogicSchema.BringToFront();*/
                //if (listBox1.SelectedIndex == 10) Debugging.BringToFront();
                if (listBox1.SelectedIndex == 11) ErrorReporting.BringToFront();
                if (listBox1.SelectedIndex == 12) AutomaticUpdates.BringToFront();
                if (listBox1.SelectedIndex == 13) ResetSettings.BringToFront();
                if (listBox1.SelectedIndex == 14) Debugging.BringToFront();

            }
            else
            {
                try
                {
                    //if (ctrl != null) ctrl.Font = new Font(ctrl.Font, FontStyle.Regular);
                    if (controale[listBox1.SelectedIndex].Parent.GetType() == typeof(GroupBox))
                    {
                        controale[listBox1.SelectedIndex].Parent.Parent.BringToFront();
                       /* controale[listBox1.SelectedIndex].Visible = false;
                        Application.DoEvents();
                        Thread.Sleep(200);
                        controale[listBox1.SelectedIndex].Visible = true;
                        Application.DoEvents();
                        Thread.Sleep(100);
                        controale[listBox1.SelectedIndex].Visible = false;
                        Application.DoEvents();
                        Thread.Sleep(200);
                        controale[listBox1.SelectedIndex].Visible = true;*/
                        controale[listBox1.SelectedIndex].Font = new Font(controale[listBox1.SelectedIndex].Font, FontStyle.Underline);
                    }
                    else
                    {
                        controale[listBox1.SelectedIndex].Parent.BringToFront();
                        /*controale[listBox1.SelectedIndex].Visible = false;
                        Application.DoEvents();
                        Thread.Sleep(200);
                        controale[listBox1.SelectedIndex].Visible = true;
                        Application.DoEvents();
                        Thread.Sleep(100);
                        controale[listBox1.SelectedIndex].Visible = false;
                        Application.DoEvents();
                        Thread.Sleep(200);
                        controale[listBox1.SelectedIndex].Visible = true;*/
                        controale[listBox1.SelectedIndex].Font = new Font(controale[listBox1.SelectedIndex].Font, FontStyle.Underline);
                    }
                    ctrl = controale[listBox1.SelectedIndex];
                }
                catch { }
            }

        }
        Controls.Development.ImageListBox.ImageListBoxItemCollection lista = new Controls.Development.ImageListBox.ImageListBoxItemCollection(new Controls.Development.ImageListBox());
        List<Control> controale = new List<Control>();
        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                try
                {
                    listBox1.SelectedIndex = 0;
                }
                catch { }
                listBox1.Focus();
                e.SuppressKeyPress = true;
                return;
            }
            listBox1.Tag = SearchBox.Text;
            if (SearchBox.Text != "" && SearchBox.Text != DefaultSearchBoxText)
            {
                listBox1.HorizontalScrollbar = true;
                listBox1.Items.Clear();
                controale.Clear();
                int i = 0;
                foreach (Control ct in this.Controls)
                {
                    if (ct.GetType() == typeof(Panel))
                    {
                        foreach(Control con in ct.Controls)
                        {
                            if (con.GetType() == typeof(GroupBox))
                            {
                                foreach (Control ctrl in con.Controls)
                                {
                                    if ((ctrl.GetType() == typeof(Label) || ctrl.GetType() == typeof(CheckBox) || ctrl.GetType() == typeof(Button) || ctrl.GetType() == typeof(ComboBox) || ctrl.GetType() == typeof(LinkLabel)) && ctrl.Text.ToLower().Contains(SearchBox.Text.ToLower()) && ctrl.Visible == true && ctrl.Parent.Visible == true)
                                    {
                                        listBox1.Items.Add(new Controls.Development.ImageListBoxItem(ct.Tag.ToString() + " → " + con.Text.ToString() + " → " + ctrl.Text.ToString()));
                                        controale.Add(ctrl);
                                        i++;
                                    }
                                }
                            }
                            else if ((con.GetType() == typeof(Label) || con.GetType() == typeof(CheckBox) || con.GetType() == typeof(Button) || con.GetType() == typeof(ComboBox) || con.GetType() == typeof(LinkLabel)) && con.Text.ToLower().Contains(SearchBox.Text.ToLower()) && con.Visible == true && con.Parent.Visible == true)
                            {
                                listBox1.Items.Add(new Controls.Development.ImageListBoxItem(ct.Tag.ToString() + " → " + con.Text.ToString()));
                                controale.Add(con);
                                i++;
                            }
                        }
                    }
                }
                this.Text = this.Tag.ToString() + " (" + i + ")";
                try
                {
                    listBox1.SelectedIndex = 0;
                }
                catch { }
                DisplayHScroll();
            }
            else
            {
                this.Text = DefaultText;
                //SearchBox.Text = DefaultSearchBoxText;
                listBox1.Items.Clear();
                foreach (Controls.Development.ImageListBoxItem lb in lista)
                {
                    listBox1.Items.Add(lb);
                }
                listBox1.SelectedIndex = 1;
                DisplayHScroll();
                foreach (Control ct in this.Controls)
                {
                    if (ct.GetType() == typeof(Panel))
                    {
                        foreach (Control con in ct.Controls)
                        {
                            if (con.GetType() == typeof(GroupBox))
                            {
                                foreach (Control ctrl in con.Controls)
                                {
                                    ctrl.Font = new Font(ctrl.Font, FontStyle.Regular);
                                }
                            }
                            else
                            {
                                con.Font = new Font(con.Font, FontStyle.Regular);
                            }
                        }
                    }
                }
            }
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText)
            {
                SearchBox.Text = "";
                this.Text = DefaultText;
                DisplayHScroll();
            }
            if (e.KeyCode == Keys.Enter) e.SuppressKeyPress = true;
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                SearchBox.Text = DefaultSearchBoxText;
                this.Text = DefaultText;
                DisplayHScroll();
            }
            if (e.KeyCode == Keys.Back) 
                foreach (Control ct in this.Controls)
                {
                    if (ct.GetType() == typeof(Panel))
                    {
                        foreach (Control con in ct.Controls)
                        {
                            if (con.GetType() == typeof(GroupBox))
                            {
                                foreach (Control ctrl in con.Controls)
                                {
                                    ctrl.Font = new Font(ctrl.Font, FontStyle.Regular);
                                }
                            }
                            else
                            {
                                con.Font = new Font(con.Font, FontStyle.Regular);
                            }
                        }
                    }
                }

        }

        private void SearchBox_Enter(object sender, EventArgs e)
        {
            if (SearchBox.Text == DefaultSearchBoxText) SearchBox.Text = "";
        }

        private void SearchBox_Leave(object sender, EventArgs e)
        {
            try
            {
                if (listBox1.Focused == false || (listBox1.Focused == true && !listBox1.Items[0].Text.Contains("→")))
                {
                    this.Text = DefaultText;
                    SearchBox.Text = DefaultSearchBoxText;
                    listBox1.Items.Clear();
                    foreach (Controls.Development.ImageListBoxItem lb in lista)
                    {
                        listBox1.Items.Add(lb);
                    }
                    listBox1.SelectedIndex = 0;
                    DisplayHScroll();
                }
            }
            catch
            {
                if (listBox1.Focused == false)
                {
                    this.Text = DefaultText;
                    SearchBox.Text = DefaultSearchBoxText;
                    listBox1.Items.Clear();
                    foreach (Controls.Development.ImageListBoxItem lb in lista)
                    {
                        listBox1.Items.Add(lb);
                    }
                    listBox1.SelectedIndex = 0;
                    DisplayHScroll();
                }
            }
        }
        private void DisplayHScroll()
        {
            // Make no partial items are displayed vertically.
            listBox1.IntegralHeight = true;



            // Display a horizontal scroll bar.
            listBox1.HorizontalScrollbar = true;

            // Create a Graphics object to use when determining the size of the largest item in the ListBox.
            Graphics g = listBox1.CreateGraphics();
            int maxim = 0;
            foreach (Controls.Development.ImageListBoxItem it in listBox1.Items)
            {
                if (maxim < (int)g.MeasureString(it.ToString(), listBox1.Font).Width) maxim = (int)g.MeasureString(it.ToString(), listBox1.Font).Width;
            }
            // Determine the size for HorizontalExtent using the MeasureString method using the last item in the list. 
            // Set the HorizontalExtent property.
            listBox1.HorizontalExtent = maxim;
            g.Dispose();
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {

        }
        public void SaveSettings()
        {
            Properties.Settings.Default.Position = checkBox1.Checked;
            Properties.Settings.Default.AutoSignIn = checkBox2.Checked;
            Properties.Settings.Default.WasWelcomeShown = !checkBox3.Checked;
            Properties.Settings.Default.ShowStatusBar = checkBox10.Checked;
            Properties.Settings.Default.ShowCommandBar = checkBox9.Checked;
            Properties.Settings.Default.ShowAdvancedPanel = checkBox8.Checked;
            Properties.Settings.Default.ShowConvertedCode = checkBox11.Checked;
            Properties.Settings.Default.ShowQuickSearch = checkBox25.Checked;
            Properties.Settings.Default.AutoSaveInterval = (int)numericUpDown1.Value;
            Properties.Settings.Default.UseBar = checkBox14.Checked;
            Properties.Settings.Default.ShowVisualScrollBar = checkBox7.Checked;
            Properties.Settings.Default.PopulateFind = checkBox5.Checked;
            Properties.Settings.Default.ReportByDefault = checkBox17.Checked;
            Properties.Settings.Default.RestoreByDefault = checkBox18.Checked;
            Properties.Settings.Default.ReportToMER = checkBox19.Checked;
            Properties.Settings.Default.ContinueExecutionOnCrash = checkBox20.Checked;
            Properties.Settings.Default.DisableCrashReporter = checkBox21.Checked;
            Properties.Settings.Default.PseudocodeLang = comboBox3.SelectedIndex;
            Properties.Settings.Default.MinGWPath = textBox4.Text;
            Properties.Settings.Default.DownloadImages = checkBox4.Checked;
            Properties.Settings.Default.DynamicColor = checkBox23.Checked;
            Properties.Settings.Default.HomePage = textBox5.Text;
            if (Properties.Settings.Default.UILang != comboBox2.SelectedIndex)
            {
                if (CasetaDeMesaj(mf, RestartNeeded, AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Properties.Settings.Default.UILang = comboBox2.SelectedIndex;
                    Properties.Settings.Default.Save();
                    Process.Start(Application.ExecutablePath);
                    Process.GetCurrentProcess().Kill();
                }
            }
            Properties.Settings.Default.PseudocodeLang = comboBox3.SelectedIndex;
            Properties.Settings.Default.UITheme = comboBox4.SelectedIndex;
            Properties.Settings.Default.AeroShake = checkBox22.Checked;
            if (radioButton1.Checked == true)
            {
                Properties.Settings.Default.PerfOptions = 0;
            }
            else if (radioButton2.Checked == true)
            {
                Properties.Settings.Default.PerfOptions = 1;
            }
            else if (radioButton3.Checked == true)
            {
                Properties.Settings.Default.PerfOptions = 2;
            }
            Properties.Settings.Default.Save();

        }
        private void button1_Click(object sender, EventArgs e)
        {
            SaveSettings();
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FileAssociation.Associate(".psc", "Pseudocode", "Pseudocode file", Application.StartupPath + @"\ico.ico", Application.StartupPath + "\\Pseudocode.exe");
            FileAssociation.Associate(".pscpackage", "Pseudocode", "Pseudocode file", Application.StartupPath + @"\ico.ico", Application.StartupPath + "\\Pseudocode.exe");
            CasetaDeMesaj(mf, FileAscSuccess, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        [System.Runtime.InteropServices.DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();
        public DialogResult CasetaDeMesaj(Form form, string mesaj, string titlu, MessageBoxButtons butoane, MessageBoxIcon icon)
        {
            if (Properties.Settings.Default.EnableNewUI == true && DwmIsCompositionEnabled() == true)
            {
                MessageBoxI msg = new MessageBoxI(form, mesaj, titlu, butoane, icon);
                msg.ShowDialog();
                return msg.DialogResult;
            }
            else
            {
                return MessageBox.Show(mesaj, titlu, butoane, icon);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", "shell:::{17cd9488-1228-4b2f-88ce-4298e93e0966}");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ManageRepos ex = new ManageRepos(mf);
            ex.ShowDialog();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            try
            {
                backgroundWorker1.RunWorkerAsync();
            }
            catch
            {
                CasetaDeMesaj(mf, AppBusy, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int nr_up = 0;
            int nr_err = 0;
            StreamReader sr = new StreamReader(Application.StartupPath + "\\" + "package-list.txt");
            string[] text = Regex.Split(sr.ReadToEnd(), "\r\n");
            sr.Close();
            System.Net.WebClient wc = new System.Net.WebClient();
            foreach (string it in text)
            {
                if (it != "" && it != null)
                {
                    try
                    {
                        StreamReader sr2 = new StreamReader(Application.StartupPath + "\\" + it + "\\version.txt");
                        string[] infos = Regex.Split(sr2.ReadToEnd(), "\r\n");
                        sr2.Close();
                        try
                        {
                            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt")) File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                        }
                        catch { }
                        if (it == "com.valinet.pseudocode") wc.DownloadFile(Properties.Settings.Default.ServerPath + "/" + it + "/version.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                        else wc.DownloadFile(infos[2] + "/" + it + "/version.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                        StreamReader srx = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\version-temp.txt");
                        string[] citit = Regex.Split(srx.ReadToEnd(), "\n");
                        srx.Close();
                        if (citit[0] != infos[0])
                        {
                            if (citit[0].StartsWith("0") || citit[0].StartsWith("1") || citit[0].StartsWith("2") || citit[0].StartsWith("3") || citit[0].StartsWith("4") || citit[0].StartsWith("5") || citit[0].StartsWith("6") || citit[0].StartsWith("7") || citit[0].StartsWith("8") || citit[0].StartsWith("9"))
                            {
                                nr_up++;
                            }
                            else
                            {
                                nr_err++;
                            }
                        }
                        else
                        {
                            // nr_up++;
                        }
                    }
                    catch
                    {
                        nr_err++;
                    }
                }
            }
            DoOnUIThread(delegate()
            {
                if (nr_err != 0)
                {
                    if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(mf, "Unable to check for updates for one of your installed packages.\n\nUse Extensions and Updates to manually resolve the problems regarding the packages' repositories.", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else CasetaDeMesaj(mf, "Nu s-a putut verifica dacă există actualizări pentru unul sau mai multe pachete instalate.\n\nFolosiți Extensii și Actualizpri pentru a corecta manual erorile legate de depozitele de la care provin pachetele.", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (nr_up == 0)
                {
                   // if (showInfo == true)
                   // {
                        if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(mf, "Good news!\n\nAll installed packages are updated!", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else CasetaDeMesaj(mf, "Vești bune!\n\nToate pachetele instalate sunt la zi!", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                   // }
                }
                else
                {
                    UpdateFound up = new UpdateFound(mf, nr_up);
                    if (up.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        ManageRepos ex = new ManageRepos(mf);
                        ex.ShowDialog();
                    }
                }

            });

            /*System.Net.WebClient wc = new System.Net.WebClient();
            try
            {
                wc.DownloadFile(Properties.Settings.Default.ServerPath + Properties.Settings.Default.UILang.ToString() + "/update.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\update.txt");

                System.IO.TextReader tw = new System.IO.StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Pseudocode\\update.txt", Encoding.UTF8);
                string text = tw.ReadToEnd();
                tw.Close();
                string[] info = System.Text.RegularExpressions.Regex.Split(text, "\n");
                string text2 = "";
                for (int i = 3; i < info.Length; i++)
                {
                    text2 = text2 + info[i] + "\r\n";
                }
                if (Application.ProductVersion != info[0])
                {
                    DoOnUIThread(delegate()
                    {
                        Update(info[0], text2, info[2], info[1]);
                    });

                    //Update(info[0], text2, info[2], info[1]);
                }
                else
                {
                    DoOnUIThread(delegate()
                    {
                        if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(mf, "Good news!\n\nThe ValiNet product you are using is updated!", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else CasetaDeMesaj(mf, "Vești bune!\n\nProdusul ValiNet folosit de dvs. este la zi!", AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    });
                }
            }
            catch
            {
                DoOnUIThread(delegate()
                {
                    if (Properties.Settings.Default.UILang == 0) CasetaDeMesaj(mf, "You are offline.\n\nWe cannot check for updates if there is no Internet connection. Connect to a network and try again.", AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else CasetaDeMesaj(mf, "Conexiunea la Internet pare să fie offline.\n\nNu se pot căuta actualizări fără o conexiune la Internet. Conectați-vă la o rețea și încercați din nou.", AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                });
            }*/
        }
        public bool Update(string a1, string a2, string a3, string a4)
        {
            Update up = new Update(a1, a2, a3, a4);
            up.Show();
            return true;
        }
        private void DoOnUIThread(MethodInvoker d)
        {
            if (this.InvokeRequired) { this.Invoke(d); } else { d(); }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                Properties.Settings.Default.ServerPath = "http://www.valinet.ro/repo/official/";
                Properties.Settings.Default.Save();
            }
            if (comboBox1.SelectedIndex == 1)
            {
                Properties.Settings.Default.ServerPath = "http://www.valinet.ro/repo/viabeta/";
                Properties.Settings.Default.Save();
            }
            if (comboBox1.SelectedIndex == 2 && listBox1.SelectedIndex == 11)
            {
                string var = Microsoft.VisualBasic.Interaction.InputBox(ServerAddress);
                Properties.Settings.Default.ServerPath = var;
                Properties.Settings.Default.Save();
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Position = true;
            Properties.Settings.Default.AutoSignIn = true;
            Properties.Settings.Default.WasWelcomeShown = true;
            Properties.Settings.Default.ShowStatusBar = true;
            Properties.Settings.Default.ShowCommandBar = true;
            Properties.Settings.Default.ShowAdvancedPanel = true;
            Properties.Settings.Default.ShowConvertedCode = true;
            Properties.Settings.Default.ShowQuickSearch = true;
            Properties.Settings.Default.AutoSaveInterval = 1;
            Properties.Settings.Default.UseBar = true;
            Properties.Settings.Default.ShowVisualScrollBar = true;
            Properties.Settings.Default.PopulateFind = true;
            Properties.Settings.Default.ReportByDefault = true;
            Properties.Settings.Default.RestoreByDefault = true;
            Properties.Settings.Default.ReportToMER = true;
            Properties.Settings.Default.ContinueExecutionOnCrash = true;
            Properties.Settings.Default.DisableCrashReporter = false;
            Properties.Settings.Default.Save();
            CasetaDeMesaj(mf, RestoreComplete, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            System.Diagnostics.Process.Start(Application.ExecutablePath);
            Process.GetCurrentProcess().Kill();
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.Q))
            {
                SearchBox.Text = "";
                SearchBox.Focus();
                return true;
            }
            if (keyData == (Keys.F1))
            {
                if (Directory.Exists(Application.StartupPath + "\\com.valinet.pseudocode.help"))
                {
                    if (mf.hp == null) mf.hp = new Help();
                    mf.hp.Show();
                    if (mf.WindowState != FormWindowState.Maximized)
                    {
                        mf.hp.Location = mf.Location;
                        mf.hp.Size = mf.Size;
                    }
                    mf.hp.WindowState = mf.WindowState;
                }
                else
                {
                    HelpOverview ho = new HelpOverview();
                    ho.ShowDialog();
                }
                return true;
            }
            return false;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            textBox4.Text = Application.StartupPath + "\\MinGW";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox4.Text = folderBrowserDialog1.SelectedPath;
            }
        }
       bool CancelClose = false;
        private void button16_Click(object sender, EventArgs e)
        {
            CancelClose = true;
            SaveSettings();
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    SettingsIO.Import(openFileDialog1.FileName);
                    CasetaDeMesaj(mf, ImportSuccess, AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    CasetaDeMesaj(mf, ErrorImportExport + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    SettingsIO.Export(saveFileDialog1.FileName);
                }
                catch (Exception ex)
                {
                    CasetaDeMesaj(mf, ErrorImportExport + ex.Message, AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Options_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CancelClose == true)
            {
                CancelClose = false;
                e.Cancel = true;
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            button2.PerformClick();
            mf.signInToolStripMenuItem.PerformClick();
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            textBox5.Text = "http://www.valinet.ro/pseudocodedev";
        }
    }
}
