﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pseudocode
{
    public partial class UnidentifiedLanguage : Form
    {
        public string IntegerKeyword;
        public string DoubleKeyword;
        public string CharKeyword;
        public string StringKeyoword;
        public string DivKeyword;
        public string ModKeyword;
        public string DifferentKeyword;
        public string AndKeyword;
        public string OrKeyword;
        public string ReadKeyword;
        public string WriteKeyword;
        public string IfKeyword;
        public string ThenKeyword;
        public string ElseKeyword;
        public string EndIfKeyword;
        public string WhileKeyword;
        public string EndWhileKeyword;
        public string ForKeyword;
        public string EndForKeyword;
        public string DoKeyword;
        public string LoopUntilKeyword;
        public string InputText;
        public string YES;
        public string NO;
        public int PseudocodeLang;
        public UnidentifiedLanguage()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == 0)
            {
                IntegerKeyword = "integer";
                DoubleKeyword = "double";
                CharKeyword = "charcater";
                StringKeyoword = "string";
                DivKeyword = "div";
                ModKeyword = "mod";
                DifferentKeyword = "different";
                AndKeyword = "and";
                OrKeyword = "or";
                ReadKeyword = "read";
                WriteKeyword = "write";
                IfKeyword = "if";
                ThenKeyword = "then";
                ElseKeyword = "else";
                EndIfKeyword = "end_if";
                WhileKeyword = "while";
                EndWhileKeyword = "end_while";
                ForKeyword = "for";
                EndForKeyword = "end_for";
                DoKeyword = "do";
                LoopUntilKeyword = "loop_until";
                InputText = "Use the keyboard to type charcaters which will represent the value of the following variable: ";
                YES = "YES";
                NO = "NO";
                PseudocodeLang = 0;
            }
            if (listBox1.SelectedIndex == 1)
            {
                IntegerKeyword = "intreg";
                DoubleKeyword = "real";
                CharKeyword = "caracter";
                StringKeyoword = "sir";
                DivKeyword = "div";
                ModKeyword = "mod";
                DifferentKeyword = "diferit";
                AndKeyword = "si";
                OrKeyword = "sau";
                ReadKeyword = "citeste";
                WriteKeyword = "scrie";
                IfKeyword = "daca";
                ThenKeyword = "atunci";
                ElseKeyword = "altfel";
                EndIfKeyword = "sfarsit_daca";
                WhileKeyword = "cat_timp";
                EndWhileKeyword = "sfarsit_cat_timp";
                ForKeyword = "pentru";
                EndForKeyword = "sfarsit_pentru";
                DoKeyword = "executa";
                LoopUntilKeyword = "pana_cand";
                InputText = "Introduceti de la tastatura caractere. Acestea vor reprezenta continutul variabilei ";
                YES = "DA";
                NO = "NU";
                PseudocodeLang = 1;
            }
            pseudocodeEd.Lexing.Lexer = ScintillaNET.Lexer.Cpp;
            pseudocodeEd.Lexing.Keywords[0] = ReadKeyword + " " + WriteKeyword + " " + IfKeyword + " " + ThenKeyword + " " + ElseKeyword + " " + EndIfKeyword + " " + WhileKeyword + " " + EndWhileKeyword + " " + ForKeyword + " " + EndForKeyword + " " + DoKeyword + " " + LoopUntilKeyword;
            pseudocodeEd.Lexing.Keywords[1] = IntegerKeyword + " " + DoubleKeyword + " " + CharKeyword + " " + StringKeyoword + " " + DivKeyword + " " + ModKeyword + " " + DifferentKeyword + " " + AndKeyword + " " + OrKeyword;
            pseudocodeEd.Lexing.LineCommentPrefix = "//";
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["DOCUMENT_DEFAULT"]].ForeColor = System.Drawing.Color.White;
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["NUMBER"]].ForeColor = System.Drawing.Color.FromArgb(174, 129, 221);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD2"]].ForeColor = System.Drawing.Color.FromArgb(102, 217, 239);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["WORD"]].ForeColor = Color.Orange;//System.Drawing.Color.FromArgb(166, 246, 66);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["STRING"]].ForeColor = System.Drawing.Color.FromArgb(230, 219, 116);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["CHARACTER"]].ForeColor = System.Drawing.Color.FromArgb(230, 219, 116);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["PREPROCESSOR"]].ForeColor = System.Drawing.Color.White;
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["OPERATOR"]].ForeColor = System.Drawing.Color.FromArgb(127, 226, 242);
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["IDENTIFIER"]].ForeColor = System.Drawing.Color.White;
            pseudocodeEd.Styles[pseudocodeEd.Lexing.StyleNameMap["COMMENT"]].ForeColor = System.Drawing.Color.FromArgb(117, 113, 75);
            pseudocodeEd.Styles.LineNumber.BackColor = Color.FromArgb(64, 64, 64);
            pseudocodeEd.Styles.LineNumber.ForeColor = Color.Silver;
            int KeyWords = 0;
            int line = pseudocodeEd.Lines.Current.Number;
            int pos = pseudocodeEd.CurrentPos;
            int add = 0;
            bool en;
            //if (currentLineBeforeEnter + 1 == numberOfLinesBeforeEnter)
                en = true;
            //else
               // en = false;
            string[] spl = pseudocodeEd.Text.Split('\n');
            for (int j = 0; j < spl.Length; j++)
            {
                spl[j] = spl[j].TrimStart();
                for (int i = 0; i < KeyWords; i++)
                {
                    spl[j] = "    " + spl[j];
                    if (j < line + 2) add += 4;


                }
                if (spl[j].TrimStart().StartsWith(IfKeyword) | spl[j].TrimStart().StartsWith(ForKeyword) | spl[j].TrimStart().StartsWith(DoKeyword) | spl[j].TrimStart().StartsWith(WhileKeyword)) KeyWords += 1;
                if (spl[j].TrimStart().StartsWith(EndIfKeyword) | spl[j].TrimStart().StartsWith(EndForKeyword) | spl[j].TrimStart().StartsWith(LoopUntilKeyword) | spl[j].TrimStart().StartsWith(EndWhileKeyword))
                {
                    KeyWords -= 1;
                    if (spl[j].StartsWith("    ")) spl[j] = spl[j].Substring(4, spl[j].Length - 4);
                }
                if (spl[j].TrimStart().StartsWith(ElseKeyword))
                {
                    if (spl[j].StartsWith("    ")) spl[j] = spl[j].Substring(4, spl[j].Length - 4);
                }

            }
            // Debug.WriteLine(currentLineBeforeEnter.ToString() + " " + numberOfLinesBeforeEnter.ToString());
            string final = "";
            foreach (string x in spl)
            {
                final += x + "\n";
            }
            final = final.Substring(0, final.Length - 1);
            pseudocodeEd.Text = final;
           /* if (flagEnterWasPressed == true)
            {
                if (en == true)
                {
                    pseudocodeEd.GoTo.Line(line);
                    var poz = pseudocodeEd.NativeInterface.GetCurrentPos();
                    pseudocodeEd.GoTo.Position(poz + pseudocodeEd.Lines[line - 1].Length - 1);
                }
                else
                {
                    pseudocodeEd.GoTo.Line(line - 1);
                    var poz = pseudocodeEd.NativeInterface.GetCurrentPos();
                    pseudocodeEd.GoTo.Position(poz + pseudocodeEd.Lines[line - 1].Length - 1);
                }
            }
            else
            {
                try
                {

                    pseudocodeEd.GoTo.Position((int)converterEd.Tag);
                }
                catch { }
            }*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UnidentifiedLanguage_Load(object sender, EventArgs e)
        {
            pseudocodeEd.LineWrapping.Mode = ScintillaNET.LineWrappingMode.None;
            pseudocodeEd.Margins[0].Width = 20;
            pseudocodeEd.Margins[0].Width = 20;
            pseudocodeEd.Font = new Font("Courier New", 10);
            pseudocodeEd.ConfigurationManager.CustomLocation = Application.StartupPath + "\\Scintilla.XML";
            pseudocodeEd.ConfigurationManager.Language = "pascal";
            pseudocodeEd.ConfigurationManager.Configure();
            pseudocodeEd.Caret.HighlightCurrentLine = true;
            pseudocodeEd.Caret.Color = Color.White;
            pseudocodeEd.Indentation.IndentWidth = 4;
            pseudocodeEd.Indentation.TabIndents = true;
            pseudocodeEd.Indentation.UseTabs = false;
            pseudocodeEd.Indentation.ShowGuides = true;
            pseudocodeEd.Scrolling.ScrollBars = ScrollBars.Both;
            listBox1.SelectedIndex = Properties.Settings.Default.PseudocodeLang;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PseudocodeLang = -1;
            this.Close();
        }
    }
}
