﻿namespace Pseudocode
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            this.MaximizeWhite = new System.Windows.Forms.Button();
            this.RestoreWhite = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Close = new System.Windows.Forms.Button();
            this.Restore = new System.Windows.Forms.Button();
            this.Minimize = new System.Windows.Forms.Button();
            this.toolStrip1 = new CustomizableToolStrip();
            this.blackApparance1 = new AppearanceControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ceEsteAceastaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton13 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton14 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton15 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton16 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton17 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton18 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton19 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton20 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton21 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton22 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton23 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton24 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton25 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton26 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton27 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton28 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton29 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton31 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton32 = new System.Windows.Forms.ToolStripSplitButton();
            this.runUsingLocalDebuggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runUsingBuiltinBrowserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem54 = new System.Windows.Forms.ToolStripSeparator();
            this.manageCustomCompilersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem55 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton33 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton34 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton35 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton36 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton37 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton38 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton39 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton40 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton41 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton42 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton43 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton44 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton45 = new System.Windows.Forms.ToolStripButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new CustomizableMenuStrip();
            this.menuIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.fILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveLogicSchemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.openPseudocodeUsingAcodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharePseudocodeUsingAcodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.printPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printConvertedCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printLogicSchemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.recoverLostunsavedFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toBasicClassicScriptFilevbsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCSourceFilecppToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCSourceFilecsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toPascalSourceFilepasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toJavaScriptpoweredWebPagehtmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.removePersonalInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.codeboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eDITToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoActionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.jumpToLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jumpToBreakpointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jumpToBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.replaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vIEWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commandBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedCommandsPanelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertedCodeLogicSchemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualScrollBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickMenuSearchBoxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.zoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.zoom400 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoom300 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoom200 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoom100 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoom75 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoom50 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoom25 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripSeparator();
            this.statisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.browserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cONVERTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toBasicClassicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toPascalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toJavaScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toVBScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripSeparator();
            this.toLogicSchemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripSeparator();
            this.limbaDeScriereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fromEnglishCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fromRomanianPseudoccodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userdefinedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripSeparator();
            this.translatePseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishToRomanianPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.romanianToEnglishPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripSeparator();
            this.analizeExecutedLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dEBUGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compileAndRunToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.justCompileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startDebuggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripSeparator();
            this.continueDebuggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.watchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendCommandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripSeparator();
            this.addremoveBreakpointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addremoveBreakpointsOnLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addremoveWatchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripSeparator();
            this.endPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tOOLSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearCoverageDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripSeparator();
            this.createApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadProblemToInfoarenaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripSeparator();
            this.testCurrentConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extensionsManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageCustomCompilersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.administrativeConfigurationsManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem33 = new System.Windows.Forms.ToolStripSeparator();
            this.customizeToolbarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wINDOWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripSeparator();
            this.closeAllTabsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeCurrentTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripSeparator();
            this.casacdeWindowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileWindowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showWindowsSideBySideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripSeparator();
            this.restoreDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maximizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripSeparator();
            this.hELPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpSupportAndTipsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripSeparator();
            this.feedbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextualHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.whatsThisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlineAssistanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.valiNetOnTheInternetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripSeparator();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.valiNetCustomerExperienceImprovementProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.licenseAgreementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderPseudocodeOnADVDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fILEToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.newBrowserWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newBrowserWindowToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.duplicateBrowserWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePageAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closePageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripSeparator();
            this.pageSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripSeparator();
            this.sendPageByEmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createShortcutToDesktopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripSeparator();
            this.quitPseudocodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eDITToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem34 = new System.Windows.Forms.ToolStripSeparator();
            this.findonThisPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vIEWToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.navigateBackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.navigateForwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openHomePageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopLoadingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem35 = new System.Windows.Forms.ToolStripSeparator();
            this.zoomToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomInToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomOutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem42 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem44 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem45 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem46 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem47 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem48 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem49 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem50 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem51 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem52 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem53 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem43 = new System.Windows.Forms.ToolStripSeparator();
            this.customizeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.textSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smallestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smallToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.largeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.largestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem36 = new System.Windows.Forms.ToolStripSeparator();
            this.sourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fAVORITESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToFavoritesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.organizeFavoritesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem37 = new System.Windows.Forms.ToolStripSeparator();
            this.valiNetWebSiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tOOLSToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSearchToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem38 = new System.Windows.Forms.ToolStripSeparator();
            this.manageCustomCompilersToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.packageManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hELPToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpSupportAndTipsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem39 = new System.Windows.Forms.ToolStripSeparator();
            this.feedbackToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem40 = new System.Windows.Forms.ToolStripSeparator();
            this.checkForUpdatesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.licenseAgreementToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem41 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.transparentControl1 = new Pseudocode.TransparentControl();
            this.Top = new System.Windows.Forms.Panel();
            this.Right = new System.Windows.Forms.Panel();
            this.Left = new System.Windows.Forms.Panel();
            this.Bottom = new System.Windows.Forms.Panel();
            this.dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.TopCover = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new CustomizableStatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.BottomDown = new System.Windows.Forms.Panel();
            this.listBox1 = new Controls.Development.ImageListBox();
            this.SearchPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SearchText = new System.Windows.Forms.LinkLabel();
            this.UserDropPanel = new Pseudocode.SelectablePanel();
            this.button47 = new System.Windows.Forms.Button();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.label28 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.UserPanel = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label27 = new System.Windows.Forms.Label();
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.imageDisplay = new System.Windows.Forms.PictureBox();
            this.MinimizeBlack = new System.Windows.Forms.Button();
            this.RestoreBlack = new System.Windows.Forms.Button();
            this.CloseBlack = new System.Windows.Forms.Button();
            this.MinimizeBlackHover = new System.Windows.Forms.Button();
            this.RestoreBlackHover = new System.Windows.Forms.Button();
            this.CloseBlackHover = new System.Windows.Forms.Button();
            this.MinimizeBlackPressed = new System.Windows.Forms.Button();
            this.RestoreBlackPressed = new System.Windows.Forms.Button();
            this.CloseBlackPressed = new System.Windows.Forms.Button();
            this.Title = new Pseudocode.PowerLabel();
            this.MinimizeLightPressed = new System.Windows.Forms.Button();
            this.RestoreLightPressed = new System.Windows.Forms.Button();
            this.CloseLightPressed = new System.Windows.Forms.Button();
            this.MinimizeLightHover = new System.Windows.Forms.Button();
            this.RestoreLightHover = new System.Windows.Forms.Button();
            this.CloseLightHover = new System.Windows.Forms.Button();
            this.MinimizeLight = new System.Windows.Forms.Button();
            this.RestoreLight = new System.Windows.Forms.Button();
            this.CloseLight = new System.Windows.Forms.Button();
            this.MaximizeLightPressed = new System.Windows.Forms.Button();
            this.MaximizeLightHover = new System.Windows.Forms.Button();
            this.MaximizeLight = new System.Windows.Forms.Button();
            this.MaximizeBlackPressed = new System.Windows.Forms.Button();
            this.MaximizeBlackHover = new System.Windows.Forms.Button();
            this.MaximizeBlack = new System.Windows.Forms.Button();
            this.customizableToolStrip1 = new CustomizableToolStrip();
            this.toolStripButton51 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton52 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton50 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton30 = new ToolStripSpringTextBox();
            this.whiteApparence1 = new AppearanceControl();
            this.appearanceControl2 = new AppearanceControl();
            this.UpdateChecker = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.startupTimer = new System.Windows.Forms.Timer(this.components);
            this.userTimer = new System.Windows.Forms.Timer(this.components);
            this.Loop = new System.Windows.Forms.Timer(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.signInNow = new System.Windows.Forms.Timer(this.components);
            this.ShowCodeboardTimer = new System.Windows.Forms.Timer(this.components);
            this.resizeWorker = new System.ComponentModel.BackgroundWorker();
            this.showSystemMenuTimer = new System.Windows.Forms.Timer(this.components);
            this.XPResizeTimer = new System.Windows.Forms.Timer(this.components);
            this.unTop = new System.Windows.Forms.Timer(this.components);
            this.BorderLoop = new System.Windows.Forms.Timer(this.components);
            this.WrapUp = new System.Windows.Forms.Timer(this.components);
            this.colorTimer = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SearchPanel.SuspendLayout();
            this.UserDropPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.UserPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDisplay)).BeginInit();
            this.customizableToolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MaximizeWhite
            // 
            resources.ApplyResources(this.MaximizeWhite, "MaximizeWhite");
            this.MaximizeWhite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.MaximizeWhite.FlatAppearance.BorderSize = 0;
            this.MaximizeWhite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.MaximizeWhite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.MaximizeWhite.ForeColor = System.Drawing.Color.Black;
            this.MaximizeWhite.Name = "MaximizeWhite";
            this.toolTip1.SetToolTip(this.MaximizeWhite, resources.GetString("MaximizeWhite.ToolTip"));
            this.MaximizeWhite.UseVisualStyleBackColor = false;
            // 
            // RestoreWhite
            // 
            resources.ApplyResources(this.RestoreWhite, "RestoreWhite");
            this.RestoreWhite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.RestoreWhite.FlatAppearance.BorderSize = 0;
            this.RestoreWhite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(96)))), ((int)(((byte)(153)))));
            this.RestoreWhite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(101)))), ((int)(((byte)(179)))));
            this.RestoreWhite.ForeColor = System.Drawing.Color.Black;
            this.RestoreWhite.Name = "RestoreWhite";
            this.toolTip1.SetToolTip(this.RestoreWhite, resources.GetString("RestoreWhite.ToolTip"));
            this.RestoreWhite.UseVisualStyleBackColor = false;
            // 
            // Close
            // 
            resources.ApplyResources(this.Close, "Close");
            this.Close.FlatAppearance.BorderSize = 0;
            this.Close.Name = "Close";
            this.toolTip1.SetToolTip(this.Close, resources.GetString("Close.ToolTip"));
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            this.Close.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Close_MouseDown);
            this.Close.MouseEnter += new System.EventHandler(this.Close_MouseEnter);
            this.Close.MouseLeave += new System.EventHandler(this.Close_MouseLeave);
            // 
            // Restore
            // 
            resources.ApplyResources(this.Restore, "Restore");
            this.Restore.FlatAppearance.BorderSize = 0;
            this.Restore.Name = "Restore";
            this.toolTip1.SetToolTip(this.Restore, resources.GetString("Restore.ToolTip"));
            this.Restore.UseVisualStyleBackColor = true;
            this.Restore.Click += new System.EventHandler(this.Restore_Click);
            this.Restore.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Restore_MouseDown);
            this.Restore.MouseEnter += new System.EventHandler(this.Restore_MouseEnter);
            this.Restore.MouseLeave += new System.EventHandler(this.Restore_MouseLeave);
            // 
            // Minimize
            // 
            resources.ApplyResources(this.Minimize, "Minimize");
            this.Minimize.FlatAppearance.BorderSize = 0;
            this.Minimize.Name = "Minimize";
            this.toolTip1.SetToolTip(this.Minimize, resources.GetString("Minimize.ToolTip"));
            this.Minimize.UseVisualStyleBackColor = true;
            this.Minimize.Click += new System.EventHandler(this.Minimize_Click);
            this.Minimize.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Minimize_MouseDown);
            this.Minimize.MouseEnter += new System.EventHandler(this.Minimize_MouseEnter);
            this.Minimize.MouseLeave += new System.EventHandler(this.Minimize_MouseLeave);
            // 
            // toolStrip1
            // 
            resources.ApplyResources(this.toolStrip1, "toolStrip1");
            this.toolStrip1.Appearance = this.blackApparance1;
            this.toolStrip1.BackColor = System.Drawing.Color.Black;
            this.toolStrip1.ContextMenuStrip = this.contextMenuStrip1;
            this.toolStrip1.DefaultButton = true;
            this.toolStrip1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageSizeSelection = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripButton12,
            this.toolStripSeparator1,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripSeparator2,
            this.toolStripButton9,
            this.toolStripButton10,
            this.toolStripButton11,
            this.toolStripSeparator3,
            this.toolStripButton13,
            this.toolStripButton14,
            this.toolStripButton15,
            this.toolStripButton16,
            this.toolStripSeparator4,
            this.toolStripButton17,
            this.toolStripButton18,
            this.toolStripSeparator5,
            this.toolStripButton19,
            this.toolStripButton20,
            this.toolStripButton21,
            this.toolStripButton22,
            this.toolStripButton23,
            this.toolStripSeparator6,
            this.toolStripButton24,
            this.toolStripButton25,
            this.toolStripButton26,
            this.toolStripButton27,
            this.toolStripSeparator7,
            this.toolStripButton28,
            this.toolStripButton29,
            this.toolStripSeparator8,
            this.toolStripButton31,
            this.toolStripSeparator9,
            this.toolStripButton32,
            this.toolStripButton33,
            this.toolStripButton34,
            this.toolStripButton35,
            this.toolStripButton36,
            this.toolStripButton37,
            this.toolStripButton38,
            this.toolStripButton39,
            this.toolStripButton40,
            this.toolStripSeparator10,
            this.toolStripButton41,
            this.toolStripButton42,
            this.toolStripButton43,
            this.toolStripSeparator11,
            this.toolStripButton44,
            this.toolStripButton45});
            this.toolStrip1.LargeIcons = false;
            this.toolStrip1.LargeImageList = this.imageList1;
            this.toolStrip1.LargeImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.ListViewDisplayStyle = ToolStripCustomCtrls.ListViewDisplayStyle.Tiles;
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RoundedEdges = false;
            this.toolStrip1.SmallImageScalingSize = new System.Drawing.Size(16, 16);
            this.toolTip1.SetToolTip(this.toolStrip1, resources.GetString("toolStrip1.ToolTip"));
            this.toolStrip1.UserData = ((ToolStripCustomCtrls.ToolStripData)(resources.GetObject("toolStrip1.UserData")));
            // 
            // blackApparance1
            // 
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.Background = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.intBackground = -12566464;
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.intBorderHighlight = -12566464;
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientBegin = -12566464;
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientEnd = -12566464;
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientMiddle = -12566464;
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.intHighlight = -3878683;
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.intPressedBackground = -98242;
            this.blackApparance1.CustomAppearance.ButtonAppearance.CheckedAppearance.intSelectedBackground = -98242;
            this.blackApparance1.CustomAppearance.ButtonAppearance.PressedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.PressedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(128)))), ((int)(((byte)(62)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.PressedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(128)))), ((int)(((byte)(62)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.PressedAppearance.intBorder = -16777088;
            this.blackApparance1.CustomAppearance.ButtonAppearance.PressedAppearance.intBorderHighlight = -13410648;
            this.blackApparance1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientBegin = -98242;
            this.blackApparance1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientEnd = -98242;
            this.blackApparance1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientMiddle = -98242;
            this.blackApparance1.CustomAppearance.ButtonAppearance.PressedAppearance.intHighlight = -6771246;
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorder = -8355712;
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorderHighlight = -8355712;
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientBegin = -12566464;
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientEnd = -12566464;
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientMiddle = -12566464;
            this.blackApparance1.CustomAppearance.ButtonAppearance.SelectedAppearance.intHighlight = -3878683;
            this.blackApparance1.CustomAppearance.GripAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.blackApparance1.CustomAppearance.GripAppearance.intDark = -1;
            this.blackApparance1.CustomAppearance.GripAppearance.intLight = -1;
            this.blackApparance1.CustomAppearance.GripAppearance.Light = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.blackApparance1.CustomAppearance.ImageMarginAppearance.Normal.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.ImageMarginAppearance.Normal.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.ImageMarginAppearance.Normal.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.ImageMarginAppearance.Normal.intGradientBegin = -16777216;
            this.blackApparance1.CustomAppearance.ImageMarginAppearance.Normal.intGradientEnd = -16777216;
            this.blackApparance1.CustomAppearance.ImageMarginAppearance.Normal.intGradientMiddle = -16777216;
            this.blackApparance1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientBegin = -3416586;
            this.blackApparance1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientEnd = -9266217;
            this.blackApparance1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientMiddle = -6175239;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.MenuItemAppearance.intBorder = -16777216;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.intPressedGradientBegin = -16777216;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.intPressedGradientEnd = -16777216;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.intPressedGradientMiddle = -16777216;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.intSelected = -12566464;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.intSelectedGradientBegin = -12566464;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.intSelectedGradientEnd = -12566464;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.PressedGradientBegin = System.Drawing.Color.Black;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.PressedGradientEnd = System.Drawing.Color.Black;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.PressedGradientMiddle = System.Drawing.Color.Black;
            this.blackApparance1.CustomAppearance.MenuItemAppearance.Selected = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.MenuItemAppearance.SelectedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.MenuItemAppearance.SelectedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.blackApparance1.CustomAppearance.MenuStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.blackApparance1.CustomAppearance.MenuStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.MenuStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.MenuStripAppearance.intBorder = -1;
            this.blackApparance1.CustomAppearance.MenuStripAppearance.intGradientBegin = -16777216;
            this.blackApparance1.CustomAppearance.MenuStripAppearance.intGradientEnd = -16777216;
            this.blackApparance1.CustomAppearance.OverflowButtonAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.OverflowButtonAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.OverflowButtonAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.OverflowButtonAppearance.intGradientBegin = -16777216;
            this.blackApparance1.CustomAppearance.OverflowButtonAppearance.intGradientEnd = -16777216;
            this.blackApparance1.CustomAppearance.OverflowButtonAppearance.intGradientMiddle = -16777216;
            this.blackApparance1.CustomAppearance.RaftingContainerAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.RaftingContainerAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.RaftingContainerAppearance.intGradientBegin = -16777216;
            this.blackApparance1.CustomAppearance.RaftingContainerAppearance.intGradientEnd = -16777216;
            this.blackApparance1.CustomAppearance.SeparatorAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.blackApparance1.CustomAppearance.SeparatorAppearance.intDark = -1;
            this.blackApparance1.CustomAppearance.SeparatorAppearance.intLight = -1;
            this.blackApparance1.CustomAppearance.SeparatorAppearance.Light = System.Drawing.Color.White;
            this.blackApparance1.CustomAppearance.StatusStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.StatusStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.StatusStripAppearance.intGradientBegin = -16777216;
            this.blackApparance1.CustomAppearance.StatusStripAppearance.intGradientEnd = -16777216;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.ToolStripAppearance.ContentPanelGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.ToolStripAppearance.ContentPanelGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.ToolStripAppearance.DropDownBackground = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.ToolStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.ToolStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.ToolStripAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.blackApparance1.CustomAppearance.ToolStripAppearance.intBorder = -16777216;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.intContentPanelGradientBegin = -16777216;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.intContentPanelGradientEnd = -16777216;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.intDropDownBackground = -16777216;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.intGradientBegin = -16777216;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.intGradientEnd = -16777216;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.intGradientMiddle = -16777216;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.intPanelGradientBegin = -16777216;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.intPanelGradientEnd = -16777216;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.PanelGradientBegin = System.Drawing.Color.Black;
            this.blackApparance1.CustomAppearance.ToolStripAppearance.PanelGradientEnd = System.Drawing.Color.Black;
            this.blackApparance1.Preset = AppearanceControl.enumPresetStyles.Custom;
            this.blackApparance1.Renderer.RoundedEdges = false;
            // 
            // contextMenuStrip1
            // 
            resources.ApplyResources(this.contextMenuStrip1, "contextMenuStrip1");
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ceEsteAceastaToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.toolTip1.SetToolTip(this.contextMenuStrip1, resources.GetString("contextMenuStrip1.ToolTip"));
            // 
            // ceEsteAceastaToolStripMenuItem
            // 
            resources.ApplyResources(this.ceEsteAceastaToolStripMenuItem, "ceEsteAceastaToolStripMenuItem");
            this.ceEsteAceastaToolStripMenuItem.Name = "ceEsteAceastaToolStripMenuItem";
            this.ceEsteAceastaToolStripMenuItem.Click += new System.EventHandler(this.ceEsteAceastaToolStripMenuItem_Click);
            // 
            // toolStripButton1
            // 
            resources.ApplyResources(this.toolStripButton1, "toolStripButton1");
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            resources.ApplyResources(this.toolStripButton2, "toolStripButton2");
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Click += new System.EventHandler(this.openPseudocodeToolStripMenuItem_Click);
            // 
            // toolStripButton3
            // 
            resources.ApplyResources(this.toolStripButton3, "toolStripButton3");
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripButton4
            // 
            resources.ApplyResources(this.toolStripButton4, "toolStripButton4");
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripButton5
            // 
            resources.ApplyResources(this.toolStripButton5, "toolStripButton5");
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Click += new System.EventHandler(this.saveAllToolStripMenuItem_Click);
            // 
            // toolStripButton6
            // 
            resources.ApplyResources(this.toolStripButton6, "toolStripButton6");
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Click += new System.EventHandler(this.saveLogicSchemaToolStripMenuItem_Click);
            // 
            // toolStripButton12
            // 
            resources.ApplyResources(this.toolStripButton12, "toolStripButton12");
            this.toolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton12.Name = "toolStripButton12";
            this.toolStripButton12.Click += new System.EventHandler(this.toolStripButton12_Click);
            // 
            // toolStripSeparator1
            // 
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            // 
            // toolStripButton7
            // 
            resources.ApplyResources(this.toolStripButton7, "toolStripButton7");
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Click += new System.EventHandler(this.openPseudocodeUsingAcodeToolStripMenuItem_Click);
            // 
            // toolStripButton8
            // 
            resources.ApplyResources(this.toolStripButton8, "toolStripButton8");
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Click += new System.EventHandler(this.sharePseudocodeUsingAcodeToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            // 
            // toolStripButton9
            // 
            resources.ApplyResources(this.toolStripButton9, "toolStripButton9");
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Click += new System.EventHandler(this.printPseudocodeToolStripMenuItem_Click);
            // 
            // toolStripButton10
            // 
            resources.ApplyResources(this.toolStripButton10, "toolStripButton10");
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Click += new System.EventHandler(this.printConvertedCodeToolStripMenuItem_Click);
            // 
            // toolStripButton11
            // 
            resources.ApplyResources(this.toolStripButton11, "toolStripButton11");
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.Click += new System.EventHandler(this.printLogicSchemeToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            // 
            // toolStripButton13
            // 
            resources.ApplyResources(this.toolStripButton13, "toolStripButton13");
            this.toolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton13.Name = "toolStripButton13";
            // 
            // toolStripButton14
            // 
            resources.ApplyResources(this.toolStripButton14, "toolStripButton14");
            this.toolStripButton14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton14.Name = "toolStripButton14";
            // 
            // toolStripButton15
            // 
            resources.ApplyResources(this.toolStripButton15, "toolStripButton15");
            this.toolStripButton15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton15.Name = "toolStripButton15";
            // 
            // toolStripButton16
            // 
            resources.ApplyResources(this.toolStripButton16, "toolStripButton16");
            this.toolStripButton16.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton16.Name = "toolStripButton16";
            // 
            // toolStripSeparator4
            // 
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            // 
            // toolStripButton17
            // 
            resources.ApplyResources(this.toolStripButton17, "toolStripButton17");
            this.toolStripButton17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton17.Name = "toolStripButton17";
            this.toolStripButton17.Click += new System.EventHandler(this.undoActionToolStripMenuItem_Click);
            // 
            // toolStripButton18
            // 
            resources.ApplyResources(this.toolStripButton18, "toolStripButton18");
            this.toolStripButton18.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton18.Name = "toolStripButton18";
            this.toolStripButton18.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            // 
            // toolStripButton19
            // 
            resources.ApplyResources(this.toolStripButton19, "toolStripButton19");
            this.toolStripButton19.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton19.Name = "toolStripButton19";
            this.toolStripButton19.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // toolStripButton20
            // 
            resources.ApplyResources(this.toolStripButton20, "toolStripButton20");
            this.toolStripButton20.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton20.Name = "toolStripButton20";
            this.toolStripButton20.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // toolStripButton21
            // 
            resources.ApplyResources(this.toolStripButton21, "toolStripButton21");
            this.toolStripButton21.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton21.Name = "toolStripButton21";
            this.toolStripButton21.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // toolStripButton22
            // 
            resources.ApplyResources(this.toolStripButton22, "toolStripButton22");
            this.toolStripButton22.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton22.Name = "toolStripButton22";
            this.toolStripButton22.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // toolStripButton23
            // 
            resources.ApplyResources(this.toolStripButton23, "toolStripButton23");
            this.toolStripButton23.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton23.Name = "toolStripButton23";
            this.toolStripButton23.Click += new System.EventHandler(this.deleteSelectionToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            resources.ApplyResources(this.toolStripSeparator6, "toolStripSeparator6");
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            // 
            // toolStripButton24
            // 
            resources.ApplyResources(this.toolStripButton24, "toolStripButton24");
            this.toolStripButton24.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton24.Name = "toolStripButton24";
            this.toolStripButton24.Click += new System.EventHandler(this.jumpToLineToolStripMenuItem_Click);
            // 
            // toolStripButton25
            // 
            resources.ApplyResources(this.toolStripButton25, "toolStripButton25");
            this.toolStripButton25.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton25.Name = "toolStripButton25";
            this.toolStripButton25.Click += new System.EventHandler(this.goToDefinitionToolStripMenuItem_Click);
            // 
            // toolStripButton26
            // 
            resources.ApplyResources(this.toolStripButton26, "toolStripButton26");
            this.toolStripButton26.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton26.Name = "toolStripButton26";
            this.toolStripButton26.Click += new System.EventHandler(this.jumpToBreakpointToolStripMenuItem_Click);
            // 
            // toolStripButton27
            // 
            resources.ApplyResources(this.toolStripButton27, "toolStripButton27");
            this.toolStripButton27.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton27.Name = "toolStripButton27";
            // 
            // toolStripSeparator7
            // 
            resources.ApplyResources(this.toolStripSeparator7, "toolStripSeparator7");
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            // 
            // toolStripButton28
            // 
            resources.ApplyResources(this.toolStripButton28, "toolStripButton28");
            this.toolStripButton28.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton28.Name = "toolStripButton28";
            this.toolStripButton28.Click += new System.EventHandler(this.findToolStripMenuItem_Click);
            // 
            // toolStripButton29
            // 
            resources.ApplyResources(this.toolStripButton29, "toolStripButton29");
            this.toolStripButton29.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton29.Name = "toolStripButton29";
            this.toolStripButton29.Click += new System.EventHandler(this.replaceToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            resources.ApplyResources(this.toolStripSeparator8, "toolStripSeparator8");
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            // 
            // toolStripButton31
            // 
            resources.ApplyResources(this.toolStripButton31, "toolStripButton31");
            this.toolStripButton31.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton31.Name = "toolStripButton31";
            this.toolStripButton31.Click += new System.EventHandler(this.statisticsToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            resources.ApplyResources(this.toolStripSeparator9, "toolStripSeparator9");
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            // 
            // toolStripButton32
            // 
            resources.ApplyResources(this.toolStripButton32, "toolStripButton32");
            this.toolStripButton32.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runUsingLocalDebuggerToolStripMenuItem,
            this.runUsingBuiltinBrowserToolStripMenuItem,
            this.toolStripMenuItem54,
            this.manageCustomCompilersToolStripMenuItem,
            this.toolStripMenuItem55});
            this.toolStripButton32.ForeColor = System.Drawing.Color.White;
            this.toolStripButton32.Name = "toolStripButton32";
            this.toolStripButton32.ButtonClick += new System.EventHandler(this.compileAndRunToolStripMenuItem_Click);
            // 
            // runUsingLocalDebuggerToolStripMenuItem
            // 
            resources.ApplyResources(this.runUsingLocalDebuggerToolStripMenuItem, "runUsingLocalDebuggerToolStripMenuItem");
            this.runUsingLocalDebuggerToolStripMenuItem.Checked = true;
            this.runUsingLocalDebuggerToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.runUsingLocalDebuggerToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.runUsingLocalDebuggerToolStripMenuItem.Name = "runUsingLocalDebuggerToolStripMenuItem";
            this.runUsingLocalDebuggerToolStripMenuItem.Click += new System.EventHandler(this.runUsingLocalDebuggerToolStripMenuItem_Click);
            // 
            // runUsingBuiltinBrowserToolStripMenuItem
            // 
            resources.ApplyResources(this.runUsingBuiltinBrowserToolStripMenuItem, "runUsingBuiltinBrowserToolStripMenuItem");
            this.runUsingBuiltinBrowserToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.runUsingBuiltinBrowserToolStripMenuItem.Name = "runUsingBuiltinBrowserToolStripMenuItem";
            this.runUsingBuiltinBrowserToolStripMenuItem.Click += new System.EventHandler(this.runUsingBuiltinBrowserToolStripMenuItem_Click);
            // 
            // toolStripMenuItem54
            // 
            resources.ApplyResources(this.toolStripMenuItem54, "toolStripMenuItem54");
            this.toolStripMenuItem54.Name = "toolStripMenuItem54";
            // 
            // manageCustomCompilersToolStripMenuItem
            // 
            resources.ApplyResources(this.manageCustomCompilersToolStripMenuItem, "manageCustomCompilersToolStripMenuItem");
            this.manageCustomCompilersToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.manageCustomCompilersToolStripMenuItem.Name = "manageCustomCompilersToolStripMenuItem";
            this.manageCustomCompilersToolStripMenuItem.Click += new System.EventHandler(this.manageCustomCompilersToolStripMenuItem_Click);
            // 
            // toolStripMenuItem55
            // 
            resources.ApplyResources(this.toolStripMenuItem55, "toolStripMenuItem55");
            this.toolStripMenuItem55.Name = "toolStripMenuItem55";
            // 
            // toolStripButton33
            // 
            resources.ApplyResources(this.toolStripButton33, "toolStripButton33");
            this.toolStripButton33.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton33.Name = "toolStripButton33";
            this.toolStripButton33.Click += new System.EventHandler(this.justCompileToolStripMenuItem_Click);
            // 
            // toolStripButton34
            // 
            resources.ApplyResources(this.toolStripButton34, "toolStripButton34");
            this.toolStripButton34.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton34.Name = "toolStripButton34";
            this.toolStripButton34.Click += new System.EventHandler(this.continueDebuggingToolStripMenuItem_Click);
            // 
            // toolStripButton35
            // 
            resources.ApplyResources(this.toolStripButton35, "toolStripButton35");
            this.toolStripButton35.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton35.Name = "toolStripButton35";
            this.toolStripButton35.Click += new System.EventHandler(this.watchesToolStripMenuItem_Click);
            // 
            // toolStripButton36
            // 
            resources.ApplyResources(this.toolStripButton36, "toolStripButton36");
            this.toolStripButton36.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton36.Name = "toolStripButton36";
            this.toolStripButton36.Click += new System.EventHandler(this.sendCommandToolStripMenuItem_Click);
            // 
            // toolStripButton37
            // 
            resources.ApplyResources(this.toolStripButton37, "toolStripButton37");
            this.toolStripButton37.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton37.Name = "toolStripButton37";
            this.toolStripButton37.Click += new System.EventHandler(this.addremoveBreakpointToolStripMenuItem_Click);
            // 
            // toolStripButton38
            // 
            resources.ApplyResources(this.toolStripButton38, "toolStripButton38");
            this.toolStripButton38.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton38.Name = "toolStripButton38";
            // 
            // toolStripButton39
            // 
            resources.ApplyResources(this.toolStripButton39, "toolStripButton39");
            this.toolStripButton39.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton39.Name = "toolStripButton39";
            this.toolStripButton39.Click += new System.EventHandler(this.endPseudocodeToolStripMenuItem_Click);
            // 
            // toolStripButton40
            // 
            resources.ApplyResources(this.toolStripButton40, "toolStripButton40");
            this.toolStripButton40.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton40.Name = "toolStripButton40";
            this.toolStripButton40.Click += new System.EventHandler(this.restartPseudocodeToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            resources.ApplyResources(this.toolStripSeparator10, "toolStripSeparator10");
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            // 
            // toolStripButton41
            // 
            resources.ApplyResources(this.toolStripButton41, "toolStripButton41");
            this.toolStripButton41.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton41.Name = "toolStripButton41";
            this.toolStripButton41.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // toolStripButton42
            // 
            resources.ApplyResources(this.toolStripButton42, "toolStripButton42");
            this.toolStripButton42.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton42.Name = "toolStripButton42";
            this.toolStripButton42.Click += new System.EventHandler(this.createApplicationToolStripMenuItem_Click);
            // 
            // toolStripButton43
            // 
            resources.ApplyResources(this.toolStripButton43, "toolStripButton43");
            this.toolStripButton43.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton43.Name = "toolStripButton43";
            // 
            // toolStripSeparator11
            // 
            resources.ApplyResources(this.toolStripSeparator11, "toolStripSeparator11");
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            // 
            // toolStripButton44
            // 
            resources.ApplyResources(this.toolStripButton44, "toolStripButton44");
            this.toolStripButton44.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton44.Name = "toolStripButton44";
            this.toolStripButton44.Click += new System.EventHandler(this.helpSupportAndTipsToolStripMenuItem_Click);
            // 
            // toolStripButton45
            // 
            resources.ApplyResources(this.toolStripButton45, "toolStripButton45");
            this.toolStripButton45.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton45.Name = "toolStripButton45";
            this.toolStripButton45.Click += new System.EventHandler(this.checkForUpdatesToolStripMenuItem_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "document.png");
            // 
            // menuStrip1
            // 
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Appearance = this.blackApparance1;
            this.menuStrip1.BackColor = System.Drawing.Color.Black;
            this.menuStrip1.ContextMenuStrip = this.contextMenuStrip1;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuIcon,
            this.fILEToolStripMenuItem,
            this.eDITToolStripMenuItem,
            this.vIEWToolStripMenuItem,
            this.cONVERTToolStripMenuItem,
            this.tOOLSToolStripMenuItem,
            this.dEBUGToolStripMenuItem,
            this.wINDOWToolStripMenuItem,
            this.hELPToolStripMenuItem,
            this.fILEToolStripMenuItem1,
            this.eDITToolStripMenuItem1,
            this.vIEWToolStripMenuItem1,
            this.fAVORITESToolStripMenuItem,
            this.tOOLSToolStripMenuItem1,
            this.hELPToolStripMenuItem1});
            this.menuStrip1.Name = "menuStrip1";
            this.toolTip1.SetToolTip(this.menuStrip1, resources.GetString("menuStrip1.ToolTip"));
            this.menuStrip1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseDown);
            this.menuStrip1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseMove);
            // 
            // menuIcon
            // 
            resources.ApplyResources(this.menuIcon, "menuIcon");
            this.menuIcon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.menuIcon.Name = "menuIcon";
            this.menuIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuIcon_MouseDown);
            // 
            // fILEToolStripMenuItem
            // 
            resources.ApplyResources(this.fILEToolStripMenuItem, "fILEToolStripMenuItem");
            this.fILEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newPseudocodeToolStripMenuItem,
            this.openPseudocodeToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.saveAllToolStripMenuItem,
            this.saveLogicSchemaToolStripMenuItem,
            this.uploadPseudocodeToolStripMenuItem,
            this.closeFileToolStripMenuItem,
            this.toolStripMenuItem1,
            this.openPseudocodeUsingAcodeToolStripMenuItem,
            this.sharePseudocodeUsingAcodeToolStripMenuItem,
            this.toolStripMenuItem2,
            this.printPseudocodeToolStripMenuItem,
            this.printConvertedCodeToolStripMenuItem,
            this.printLogicSchemeToolStripMenuItem,
            this.toolStripMenuItem3,
            this.recoverLostunsavedFilesToolStripMenuItem,
            this.exportCodeToolStripMenuItem,
            this.toolStripMenuItem4,
            this.removePersonalInformationToolStripMenuItem,
            this.propertiesToolStripMenuItem,
            this.toolStripMenuItem5,
            this.codeboardToolStripMenuItem,
            this.exitPseudocodeToolStripMenuItem});
            this.fILEToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fILEToolStripMenuItem.Name = "fILEToolStripMenuItem";
            // 
            // newPseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.newPseudocodeToolStripMenuItem, "newPseudocodeToolStripMenuItem");
            this.newPseudocodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.newPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.newPseudocodeToolStripMenuItem.Name = "newPseudocodeToolStripMenuItem";
            this.newPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.newPseudocodeToolStripMenuItem_Click);
            // 
            // openPseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.openPseudocodeToolStripMenuItem, "openPseudocodeToolStripMenuItem");
            this.openPseudocodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.openPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.openPseudocodeToolStripMenuItem.Name = "openPseudocodeToolStripMenuItem";
            this.openPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.openPseudocodeToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            resources.ApplyResources(this.saveToolStripMenuItem, "saveToolStripMenuItem");
            this.saveToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.saveToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            resources.ApplyResources(this.saveAsToolStripMenuItem, "saveAsToolStripMenuItem");
            this.saveAsToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.saveAsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // saveAllToolStripMenuItem
            // 
            resources.ApplyResources(this.saveAllToolStripMenuItem, "saveAllToolStripMenuItem");
            this.saveAllToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.saveAllToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.saveAllToolStripMenuItem.Name = "saveAllToolStripMenuItem";
            this.saveAllToolStripMenuItem.Click += new System.EventHandler(this.saveAllToolStripMenuItem_Click);
            // 
            // saveLogicSchemaToolStripMenuItem
            // 
            resources.ApplyResources(this.saveLogicSchemaToolStripMenuItem, "saveLogicSchemaToolStripMenuItem");
            this.saveLogicSchemaToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.saveLogicSchemaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.saveLogicSchemaToolStripMenuItem.Name = "saveLogicSchemaToolStripMenuItem";
            this.saveLogicSchemaToolStripMenuItem.Click += new System.EventHandler(this.saveLogicSchemaToolStripMenuItem_Click);
            // 
            // uploadPseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.uploadPseudocodeToolStripMenuItem, "uploadPseudocodeToolStripMenuItem");
            this.uploadPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.uploadPseudocodeToolStripMenuItem.Name = "uploadPseudocodeToolStripMenuItem";
            this.uploadPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.uploadPseudocodeToolStripMenuItem_Click);
            // 
            // closeFileToolStripMenuItem
            // 
            resources.ApplyResources(this.closeFileToolStripMenuItem, "closeFileToolStripMenuItem");
            this.closeFileToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.closeFileToolStripMenuItem.Name = "closeFileToolStripMenuItem";
            this.closeFileToolStripMenuItem.Click += new System.EventHandler(this.closeFileToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            this.toolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            // 
            // openPseudocodeUsingAcodeToolStripMenuItem
            // 
            resources.ApplyResources(this.openPseudocodeUsingAcodeToolStripMenuItem, "openPseudocodeUsingAcodeToolStripMenuItem");
            this.openPseudocodeUsingAcodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.openPseudocodeUsingAcodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.openPseudocodeUsingAcodeToolStripMenuItem.Name = "openPseudocodeUsingAcodeToolStripMenuItem";
            this.openPseudocodeUsingAcodeToolStripMenuItem.Click += new System.EventHandler(this.openPseudocodeUsingAcodeToolStripMenuItem_Click);
            // 
            // sharePseudocodeUsingAcodeToolStripMenuItem
            // 
            resources.ApplyResources(this.sharePseudocodeUsingAcodeToolStripMenuItem, "sharePseudocodeUsingAcodeToolStripMenuItem");
            this.sharePseudocodeUsingAcodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.sharePseudocodeUsingAcodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.sharePseudocodeUsingAcodeToolStripMenuItem.Name = "sharePseudocodeUsingAcodeToolStripMenuItem";
            this.sharePseudocodeUsingAcodeToolStripMenuItem.Click += new System.EventHandler(this.sharePseudocodeUsingAcodeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            this.toolStripMenuItem2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            // 
            // printPseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.printPseudocodeToolStripMenuItem, "printPseudocodeToolStripMenuItem");
            this.printPseudocodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.printPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.printPseudocodeToolStripMenuItem.Name = "printPseudocodeToolStripMenuItem";
            this.printPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.printPseudocodeToolStripMenuItem_Click);
            // 
            // printConvertedCodeToolStripMenuItem
            // 
            resources.ApplyResources(this.printConvertedCodeToolStripMenuItem, "printConvertedCodeToolStripMenuItem");
            this.printConvertedCodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.printConvertedCodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.printConvertedCodeToolStripMenuItem.Name = "printConvertedCodeToolStripMenuItem";
            this.printConvertedCodeToolStripMenuItem.Click += new System.EventHandler(this.printConvertedCodeToolStripMenuItem_Click);
            // 
            // printLogicSchemeToolStripMenuItem
            // 
            resources.ApplyResources(this.printLogicSchemeToolStripMenuItem, "printLogicSchemeToolStripMenuItem");
            this.printLogicSchemeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.printLogicSchemeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.printLogicSchemeToolStripMenuItem.Name = "printLogicSchemeToolStripMenuItem";
            this.printLogicSchemeToolStripMenuItem.Click += new System.EventHandler(this.printLogicSchemeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
            this.toolStripMenuItem3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            // 
            // recoverLostunsavedFilesToolStripMenuItem
            // 
            resources.ApplyResources(this.recoverLostunsavedFilesToolStripMenuItem, "recoverLostunsavedFilesToolStripMenuItem");
            this.recoverLostunsavedFilesToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.recoverLostunsavedFilesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.recoverLostunsavedFilesToolStripMenuItem.Name = "recoverLostunsavedFilesToolStripMenuItem";
            this.recoverLostunsavedFilesToolStripMenuItem.Click += new System.EventHandler(this.recoverLostunsavedFilesToolStripMenuItem_Click);
            // 
            // exportCodeToolStripMenuItem
            // 
            resources.ApplyResources(this.exportCodeToolStripMenuItem, "exportCodeToolStripMenuItem");
            this.exportCodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.exportCodeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toBasicClassicScriptFilevbsToolStripMenuItem,
            this.toCSourceFilecppToolStripMenuItem,
            this.toCSourceFilecsToolStripMenuItem,
            this.toPascalSourceFilepasToolStripMenuItem,
            this.toJavaScriptpoweredWebPagehtmToolStripMenuItem});
            this.exportCodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.exportCodeToolStripMenuItem.Name = "exportCodeToolStripMenuItem";
            // 
            // toBasicClassicScriptFilevbsToolStripMenuItem
            // 
            resources.ApplyResources(this.toBasicClassicScriptFilevbsToolStripMenuItem, "toBasicClassicScriptFilevbsToolStripMenuItem");
            this.toBasicClassicScriptFilevbsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toBasicClassicScriptFilevbsToolStripMenuItem.Name = "toBasicClassicScriptFilevbsToolStripMenuItem";
            this.toBasicClassicScriptFilevbsToolStripMenuItem.Click += new System.EventHandler(this.toBasicClassicScriptFilevbsToolStripMenuItem_Click);
            // 
            // toCSourceFilecppToolStripMenuItem
            // 
            resources.ApplyResources(this.toCSourceFilecppToolStripMenuItem, "toCSourceFilecppToolStripMenuItem");
            this.toCSourceFilecppToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toCSourceFilecppToolStripMenuItem.Name = "toCSourceFilecppToolStripMenuItem";
            this.toCSourceFilecppToolStripMenuItem.Click += new System.EventHandler(this.toCSourceFilecppToolStripMenuItem_Click);
            // 
            // toCSourceFilecsToolStripMenuItem
            // 
            resources.ApplyResources(this.toCSourceFilecsToolStripMenuItem, "toCSourceFilecsToolStripMenuItem");
            this.toCSourceFilecsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toCSourceFilecsToolStripMenuItem.Name = "toCSourceFilecsToolStripMenuItem";
            this.toCSourceFilecsToolStripMenuItem.Click += new System.EventHandler(this.toCSourceFilecsToolStripMenuItem_Click);
            // 
            // toPascalSourceFilepasToolStripMenuItem
            // 
            resources.ApplyResources(this.toPascalSourceFilepasToolStripMenuItem, "toPascalSourceFilepasToolStripMenuItem");
            this.toPascalSourceFilepasToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toPascalSourceFilepasToolStripMenuItem.Name = "toPascalSourceFilepasToolStripMenuItem";
            this.toPascalSourceFilepasToolStripMenuItem.Click += new System.EventHandler(this.toPascalSourceFilepasToolStripMenuItem_Click);
            // 
            // toJavaScriptpoweredWebPagehtmToolStripMenuItem
            // 
            resources.ApplyResources(this.toJavaScriptpoweredWebPagehtmToolStripMenuItem, "toJavaScriptpoweredWebPagehtmToolStripMenuItem");
            this.toJavaScriptpoweredWebPagehtmToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toJavaScriptpoweredWebPagehtmToolStripMenuItem.Name = "toJavaScriptpoweredWebPagehtmToolStripMenuItem";
            this.toJavaScriptpoweredWebPagehtmToolStripMenuItem.Click += new System.EventHandler(this.toJavaScriptpoweredWebPagehtmToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
            this.toolStripMenuItem4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            // 
            // removePersonalInformationToolStripMenuItem
            // 
            resources.ApplyResources(this.removePersonalInformationToolStripMenuItem, "removePersonalInformationToolStripMenuItem");
            this.removePersonalInformationToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.removePersonalInformationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.removePersonalInformationToolStripMenuItem.Name = "removePersonalInformationToolStripMenuItem";
            // 
            // propertiesToolStripMenuItem
            // 
            resources.ApplyResources(this.propertiesToolStripMenuItem, "propertiesToolStripMenuItem");
            this.propertiesToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.propertiesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            // 
            // toolStripMenuItem5
            // 
            resources.ApplyResources(this.toolStripMenuItem5, "toolStripMenuItem5");
            this.toolStripMenuItem5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            // 
            // codeboardToolStripMenuItem
            // 
            resources.ApplyResources(this.codeboardToolStripMenuItem, "codeboardToolStripMenuItem");
            this.codeboardToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.codeboardToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.codeboardToolStripMenuItem.Name = "codeboardToolStripMenuItem";
            this.codeboardToolStripMenuItem.Click += new System.EventHandler(this.codeboardToolStripMenuItem_Click);
            // 
            // exitPseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.exitPseudocodeToolStripMenuItem, "exitPseudocodeToolStripMenuItem");
            this.exitPseudocodeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.exitPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.exitPseudocodeToolStripMenuItem.Name = "exitPseudocodeToolStripMenuItem";
            this.exitPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.exitPseudocodeToolStripMenuItem_Click);
            // 
            // eDITToolStripMenuItem
            // 
            resources.ApplyResources(this.eDITToolStripMenuItem, "eDITToolStripMenuItem");
            this.eDITToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoActionToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripMenuItem6,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.selectAllToolStripMenuItem,
            this.deleteSelectionToolStripMenuItem,
            this.copyCodeToolStripMenuItem,
            this.toolStripMenuItem7,
            this.jumpToLineToolStripMenuItem,
            this.goToDefinitionToolStripMenuItem,
            this.renameVariableToolStripMenuItem,
            this.jumpToBreakpointToolStripMenuItem,
            this.jumpToBookmarkToolStripMenuItem,
            this.toolStripMenuItem8,
            this.findToolStripMenuItem,
            this.replaceToolStripMenuItem});
            this.eDITToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.eDITToolStripMenuItem.Name = "eDITToolStripMenuItem";
            // 
            // undoActionToolStripMenuItem
            // 
            resources.ApplyResources(this.undoActionToolStripMenuItem, "undoActionToolStripMenuItem");
            this.undoActionToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.undoActionToolStripMenuItem.Name = "undoActionToolStripMenuItem";
            this.undoActionToolStripMenuItem.Click += new System.EventHandler(this.undoActionToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            resources.ApplyResources(this.redoToolStripMenuItem, "redoToolStripMenuItem");
            this.redoToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            resources.ApplyResources(this.toolStripMenuItem6, "toolStripMenuItem6");
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            // 
            // cutToolStripMenuItem
            // 
            resources.ApplyResources(this.cutToolStripMenuItem, "cutToolStripMenuItem");
            this.cutToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            resources.ApplyResources(this.copyToolStripMenuItem, "copyToolStripMenuItem");
            this.copyToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            resources.ApplyResources(this.pasteToolStripMenuItem, "pasteToolStripMenuItem");
            this.pasteToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // selectAllToolStripMenuItem
            // 
            resources.ApplyResources(this.selectAllToolStripMenuItem, "selectAllToolStripMenuItem");
            this.selectAllToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // deleteSelectionToolStripMenuItem
            // 
            resources.ApplyResources(this.deleteSelectionToolStripMenuItem, "deleteSelectionToolStripMenuItem");
            this.deleteSelectionToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.deleteSelectionToolStripMenuItem.Name = "deleteSelectionToolStripMenuItem";
            this.deleteSelectionToolStripMenuItem.Click += new System.EventHandler(this.deleteSelectionToolStripMenuItem_Click);
            // 
            // copyCodeToolStripMenuItem
            // 
            resources.ApplyResources(this.copyCodeToolStripMenuItem, "copyCodeToolStripMenuItem");
            this.copyCodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.copyCodeToolStripMenuItem.Name = "copyCodeToolStripMenuItem";
            this.copyCodeToolStripMenuItem.Click += new System.EventHandler(this.copyCodeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem7
            // 
            resources.ApplyResources(this.toolStripMenuItem7, "toolStripMenuItem7");
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            // 
            // jumpToLineToolStripMenuItem
            // 
            resources.ApplyResources(this.jumpToLineToolStripMenuItem, "jumpToLineToolStripMenuItem");
            this.jumpToLineToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.jumpToLineToolStripMenuItem.Name = "jumpToLineToolStripMenuItem";
            this.jumpToLineToolStripMenuItem.Click += new System.EventHandler(this.jumpToLineToolStripMenuItem_Click);
            // 
            // goToDefinitionToolStripMenuItem
            // 
            resources.ApplyResources(this.goToDefinitionToolStripMenuItem, "goToDefinitionToolStripMenuItem");
            this.goToDefinitionToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.goToDefinitionToolStripMenuItem.Name = "goToDefinitionToolStripMenuItem";
            this.goToDefinitionToolStripMenuItem.Click += new System.EventHandler(this.goToDefinitionToolStripMenuItem_Click);
            // 
            // renameVariableToolStripMenuItem
            // 
            resources.ApplyResources(this.renameVariableToolStripMenuItem, "renameVariableToolStripMenuItem");
            this.renameVariableToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.renameVariableToolStripMenuItem.Name = "renameVariableToolStripMenuItem";
            this.renameVariableToolStripMenuItem.Click += new System.EventHandler(this.renameVariableToolStripMenuItem_Click);
            // 
            // jumpToBreakpointToolStripMenuItem
            // 
            resources.ApplyResources(this.jumpToBreakpointToolStripMenuItem, "jumpToBreakpointToolStripMenuItem");
            this.jumpToBreakpointToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.jumpToBreakpointToolStripMenuItem.Name = "jumpToBreakpointToolStripMenuItem";
            this.jumpToBreakpointToolStripMenuItem.Click += new System.EventHandler(this.jumpToBreakpointToolStripMenuItem_Click);
            // 
            // jumpToBookmarkToolStripMenuItem
            // 
            resources.ApplyResources(this.jumpToBookmarkToolStripMenuItem, "jumpToBookmarkToolStripMenuItem");
            this.jumpToBookmarkToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.jumpToBookmarkToolStripMenuItem.Name = "jumpToBookmarkToolStripMenuItem";
            // 
            // toolStripMenuItem8
            // 
            resources.ApplyResources(this.toolStripMenuItem8, "toolStripMenuItem8");
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            // 
            // findToolStripMenuItem
            // 
            resources.ApplyResources(this.findToolStripMenuItem, "findToolStripMenuItem");
            this.findToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.findToolStripMenuItem.Name = "findToolStripMenuItem";
            this.findToolStripMenuItem.Click += new System.EventHandler(this.findToolStripMenuItem_Click);
            // 
            // replaceToolStripMenuItem
            // 
            resources.ApplyResources(this.replaceToolStripMenuItem, "replaceToolStripMenuItem");
            this.replaceToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.replaceToolStripMenuItem.Name = "replaceToolStripMenuItem";
            this.replaceToolStripMenuItem.Click += new System.EventHandler(this.replaceToolStripMenuItem_Click);
            // 
            // vIEWToolStripMenuItem
            // 
            resources.ApplyResources(this.vIEWToolStripMenuItem, "vIEWToolStripMenuItem");
            this.vIEWToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commandBarToolStripMenuItem,
            this.advancedCommandsPanelToolStripMenuItem,
            this.convertedCodeLogicSchemaToolStripMenuItem,
            this.visualScrollBarToolStripMenuItem,
            this.statusBarToolStripMenuItem,
            this.quickMenuSearchBoxToolStripMenuItem,
            this.toolStripMenuItem9,
            this.zoomToolStripMenuItem,
            this.toolStripMenuItem16,
            this.statisticsToolStripMenuItem,
            this.browserToolStripMenuItem});
            this.vIEWToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.vIEWToolStripMenuItem.Name = "vIEWToolStripMenuItem";
            // 
            // commandBarToolStripMenuItem
            // 
            resources.ApplyResources(this.commandBarToolStripMenuItem, "commandBarToolStripMenuItem");
            this.commandBarToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.commandBarToolStripMenuItem.Name = "commandBarToolStripMenuItem";
            this.commandBarToolStripMenuItem.Click += new System.EventHandler(this.commandBarToolStripMenuItem_Click);
            // 
            // advancedCommandsPanelToolStripMenuItem
            // 
            resources.ApplyResources(this.advancedCommandsPanelToolStripMenuItem, "advancedCommandsPanelToolStripMenuItem");
            this.advancedCommandsPanelToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.advancedCommandsPanelToolStripMenuItem.Name = "advancedCommandsPanelToolStripMenuItem";
            this.advancedCommandsPanelToolStripMenuItem.Click += new System.EventHandler(this.advancedCommandsPanelToolStripMenuItem_Click);
            // 
            // convertedCodeLogicSchemaToolStripMenuItem
            // 
            resources.ApplyResources(this.convertedCodeLogicSchemaToolStripMenuItem, "convertedCodeLogicSchemaToolStripMenuItem");
            this.convertedCodeLogicSchemaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.convertedCodeLogicSchemaToolStripMenuItem.Name = "convertedCodeLogicSchemaToolStripMenuItem";
            this.convertedCodeLogicSchemaToolStripMenuItem.Click += new System.EventHandler(this.convertedCodeLogicSchemaToolStripMenuItem_Click);
            // 
            // visualScrollBarToolStripMenuItem
            // 
            resources.ApplyResources(this.visualScrollBarToolStripMenuItem, "visualScrollBarToolStripMenuItem");
            this.visualScrollBarToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.visualScrollBarToolStripMenuItem.Name = "visualScrollBarToolStripMenuItem";
            this.visualScrollBarToolStripMenuItem.Click += new System.EventHandler(this.visualScrollBarToolStripMenuItem_Click);
            // 
            // statusBarToolStripMenuItem
            // 
            resources.ApplyResources(this.statusBarToolStripMenuItem, "statusBarToolStripMenuItem");
            this.statusBarToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.statusBarToolStripMenuItem_Click);
            // 
            // quickMenuSearchBoxToolStripMenuItem
            // 
            resources.ApplyResources(this.quickMenuSearchBoxToolStripMenuItem, "quickMenuSearchBoxToolStripMenuItem");
            this.quickMenuSearchBoxToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.quickMenuSearchBoxToolStripMenuItem.Name = "quickMenuSearchBoxToolStripMenuItem";
            this.quickMenuSearchBoxToolStripMenuItem.Click += new System.EventHandler(this.quickMenuSearchBoxToolStripMenuItem_Click);
            // 
            // toolStripMenuItem9
            // 
            resources.ApplyResources(this.toolStripMenuItem9, "toolStripMenuItem9");
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            // 
            // zoomToolStripMenuItem
            // 
            resources.ApplyResources(this.zoomToolStripMenuItem, "zoomToolStripMenuItem");
            this.zoomToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zoomInToolStripMenuItem,
            this.zoomOutToolStripMenuItem,
            this.toolStripMenuItem10,
            this.zoom400,
            this.zoom300,
            this.zoom200,
            this.zoom100,
            this.zoom75,
            this.zoom50,
            this.zoom25,
            this.toolStripMenuItem11,
            this.customizeToolStripMenuItem});
            this.zoomToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.zoomToolStripMenuItem.Name = "zoomToolStripMenuItem";
            // 
            // zoomInToolStripMenuItem
            // 
            resources.ApplyResources(this.zoomInToolStripMenuItem, "zoomInToolStripMenuItem");
            this.zoomInToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.zoomInToolStripMenuItem.Name = "zoomInToolStripMenuItem";
            this.zoomInToolStripMenuItem.Click += new System.EventHandler(this.zoomInToolStripMenuItem_Click);
            // 
            // zoomOutToolStripMenuItem
            // 
            resources.ApplyResources(this.zoomOutToolStripMenuItem, "zoomOutToolStripMenuItem");
            this.zoomOutToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.zoomOutToolStripMenuItem.Name = "zoomOutToolStripMenuItem";
            this.zoomOutToolStripMenuItem.Click += new System.EventHandler(this.zoomOutToolStripMenuItem_Click);
            // 
            // toolStripMenuItem10
            // 
            resources.ApplyResources(this.toolStripMenuItem10, "toolStripMenuItem10");
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // zoom400
            // 
            resources.ApplyResources(this.zoom400, "zoom400");
            this.zoom400.ForeColor = System.Drawing.Color.White;
            this.zoom400.Name = "zoom400";
            this.zoom400.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // zoom300
            // 
            resources.ApplyResources(this.zoom300, "zoom300");
            this.zoom300.ForeColor = System.Drawing.Color.White;
            this.zoom300.Name = "zoom300";
            this.zoom300.Click += new System.EventHandler(this.zoom300_Click);
            // 
            // zoom200
            // 
            resources.ApplyResources(this.zoom200, "zoom200");
            this.zoom200.ForeColor = System.Drawing.Color.White;
            this.zoom200.Name = "zoom200";
            this.zoom200.Click += new System.EventHandler(this.zoom200_Click);
            // 
            // zoom100
            // 
            resources.ApplyResources(this.zoom100, "zoom100");
            this.zoom100.ForeColor = System.Drawing.Color.White;
            this.zoom100.Name = "zoom100";
            this.zoom100.Click += new System.EventHandler(this.zoom100_Click);
            // 
            // zoom75
            // 
            resources.ApplyResources(this.zoom75, "zoom75");
            this.zoom75.ForeColor = System.Drawing.Color.White;
            this.zoom75.Name = "zoom75";
            this.zoom75.Click += new System.EventHandler(this.zoom75_Click);
            // 
            // zoom50
            // 
            resources.ApplyResources(this.zoom50, "zoom50");
            this.zoom50.ForeColor = System.Drawing.Color.White;
            this.zoom50.Name = "zoom50";
            this.zoom50.Click += new System.EventHandler(this.zoom50_Click);
            // 
            // zoom25
            // 
            resources.ApplyResources(this.zoom25, "zoom25");
            this.zoom25.ForeColor = System.Drawing.Color.White;
            this.zoom25.Name = "zoom25";
            this.zoom25.Click += new System.EventHandler(this.zoom25_Click);
            // 
            // toolStripMenuItem11
            // 
            resources.ApplyResources(this.toolStripMenuItem11, "toolStripMenuItem11");
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            // 
            // customizeToolStripMenuItem
            // 
            resources.ApplyResources(this.customizeToolStripMenuItem, "customizeToolStripMenuItem");
            this.customizeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            this.customizeToolStripMenuItem.Click += new System.EventHandler(this.customizeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem16
            // 
            resources.ApplyResources(this.toolStripMenuItem16, "toolStripMenuItem16");
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            // 
            // statisticsToolStripMenuItem
            // 
            resources.ApplyResources(this.statisticsToolStripMenuItem, "statisticsToolStripMenuItem");
            this.statisticsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.statisticsToolStripMenuItem.Name = "statisticsToolStripMenuItem";
            this.statisticsToolStripMenuItem.Click += new System.EventHandler(this.statisticsToolStripMenuItem_Click);
            // 
            // browserToolStripMenuItem
            // 
            resources.ApplyResources(this.browserToolStripMenuItem, "browserToolStripMenuItem");
            this.browserToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.browserToolStripMenuItem.Name = "browserToolStripMenuItem";
            this.browserToolStripMenuItem.Click += new System.EventHandler(this.browserToolStripMenuItem_Click);
            // 
            // cONVERTToolStripMenuItem
            // 
            resources.ApplyResources(this.cONVERTToolStripMenuItem, "cONVERTToolStripMenuItem");
            this.cONVERTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toCToolStripMenuItem,
            this.toBasicClassicToolStripMenuItem,
            this.toPascalToolStripMenuItem,
            this.toCToolStripMenuItem1,
            this.toJavaScriptToolStripMenuItem,
            this.toVBScriptToolStripMenuItem,
            this.toolStripMenuItem20,
            this.toLogicSchemeToolStripMenuItem,
            this.toolStripMenuItem12,
            this.limbaDeScriereToolStripMenuItem,
            this.toolStripMenuItem13,
            this.translatePseudocodeToolStripMenuItem,
            this.toolStripMenuItem14,
            this.analizeExecutedLinesToolStripMenuItem});
            this.cONVERTToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.cONVERTToolStripMenuItem.Name = "cONVERTToolStripMenuItem";
            // 
            // toCToolStripMenuItem
            // 
            resources.ApplyResources(this.toCToolStripMenuItem, "toCToolStripMenuItem");
            this.toCToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toCToolStripMenuItem.Name = "toCToolStripMenuItem";
            this.toCToolStripMenuItem.Click += new System.EventHandler(this.toCToolStripMenuItem_Click);
            // 
            // toBasicClassicToolStripMenuItem
            // 
            resources.ApplyResources(this.toBasicClassicToolStripMenuItem, "toBasicClassicToolStripMenuItem");
            this.toBasicClassicToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toBasicClassicToolStripMenuItem.Name = "toBasicClassicToolStripMenuItem";
            this.toBasicClassicToolStripMenuItem.Click += new System.EventHandler(this.toBasicClassicToolStripMenuItem_Click);
            // 
            // toPascalToolStripMenuItem
            // 
            resources.ApplyResources(this.toPascalToolStripMenuItem, "toPascalToolStripMenuItem");
            this.toPascalToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toPascalToolStripMenuItem.Name = "toPascalToolStripMenuItem";
            this.toPascalToolStripMenuItem.Click += new System.EventHandler(this.toPascalToolStripMenuItem_Click);
            // 
            // toCToolStripMenuItem1
            // 
            resources.ApplyResources(this.toCToolStripMenuItem1, "toCToolStripMenuItem1");
            this.toCToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.toCToolStripMenuItem1.Name = "toCToolStripMenuItem1";
            this.toCToolStripMenuItem1.Click += new System.EventHandler(this.toCToolStripMenuItem1_Click);
            // 
            // toJavaScriptToolStripMenuItem
            // 
            resources.ApplyResources(this.toJavaScriptToolStripMenuItem, "toJavaScriptToolStripMenuItem");
            this.toJavaScriptToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toJavaScriptToolStripMenuItem.Name = "toJavaScriptToolStripMenuItem";
            this.toJavaScriptToolStripMenuItem.Click += new System.EventHandler(this.toJavaScriptToolStripMenuItem_Click);
            // 
            // toVBScriptToolStripMenuItem
            // 
            resources.ApplyResources(this.toVBScriptToolStripMenuItem, "toVBScriptToolStripMenuItem");
            this.toVBScriptToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toVBScriptToolStripMenuItem.Name = "toVBScriptToolStripMenuItem";
            // 
            // toolStripMenuItem20
            // 
            resources.ApplyResources(this.toolStripMenuItem20, "toolStripMenuItem20");
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            // 
            // toLogicSchemeToolStripMenuItem
            // 
            resources.ApplyResources(this.toLogicSchemeToolStripMenuItem, "toLogicSchemeToolStripMenuItem");
            this.toLogicSchemeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toLogicSchemeToolStripMenuItem.Name = "toLogicSchemeToolStripMenuItem";
            this.toLogicSchemeToolStripMenuItem.Click += new System.EventHandler(this.toLogicSchemeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem12
            // 
            resources.ApplyResources(this.toolStripMenuItem12, "toolStripMenuItem12");
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            // 
            // limbaDeScriereToolStripMenuItem
            // 
            resources.ApplyResources(this.limbaDeScriereToolStripMenuItem, "limbaDeScriereToolStripMenuItem");
            this.limbaDeScriereToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fromEnglishCodeToolStripMenuItem,
            this.fromRomanianPseudoccodeToolStripMenuItem,
            this.userdefinedToolStripMenuItem});
            this.limbaDeScriereToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.limbaDeScriereToolStripMenuItem.Name = "limbaDeScriereToolStripMenuItem";
            // 
            // fromEnglishCodeToolStripMenuItem
            // 
            resources.ApplyResources(this.fromEnglishCodeToolStripMenuItem, "fromEnglishCodeToolStripMenuItem");
            this.fromEnglishCodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fromEnglishCodeToolStripMenuItem.Name = "fromEnglishCodeToolStripMenuItem";
            this.fromEnglishCodeToolStripMenuItem.Click += new System.EventHandler(this.fromEnglishCodeToolStripMenuItem_Click);
            // 
            // fromRomanianPseudoccodeToolStripMenuItem
            // 
            resources.ApplyResources(this.fromRomanianPseudoccodeToolStripMenuItem, "fromRomanianPseudoccodeToolStripMenuItem");
            this.fromRomanianPseudoccodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fromRomanianPseudoccodeToolStripMenuItem.Name = "fromRomanianPseudoccodeToolStripMenuItem";
            this.fromRomanianPseudoccodeToolStripMenuItem.Click += new System.EventHandler(this.fromRomanianPseudoccodeToolStripMenuItem_Click);
            // 
            // userdefinedToolStripMenuItem
            // 
            resources.ApplyResources(this.userdefinedToolStripMenuItem, "userdefinedToolStripMenuItem");
            this.userdefinedToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.userdefinedToolStripMenuItem.Name = "userdefinedToolStripMenuItem";
            this.userdefinedToolStripMenuItem.Click += new System.EventHandler(this.userdefinedToolStripMenuItem_Click);
            // 
            // toolStripMenuItem13
            // 
            resources.ApplyResources(this.toolStripMenuItem13, "toolStripMenuItem13");
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            // 
            // translatePseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.translatePseudocodeToolStripMenuItem, "translatePseudocodeToolStripMenuItem");
            this.translatePseudocodeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.englishToRomanianPseudocodeToolStripMenuItem,
            this.romanianToEnglishPseudocodeToolStripMenuItem});
            this.translatePseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.translatePseudocodeToolStripMenuItem.Name = "translatePseudocodeToolStripMenuItem";
            // 
            // englishToRomanianPseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.englishToRomanianPseudocodeToolStripMenuItem, "englishToRomanianPseudocodeToolStripMenuItem");
            this.englishToRomanianPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.englishToRomanianPseudocodeToolStripMenuItem.Name = "englishToRomanianPseudocodeToolStripMenuItem";
            this.englishToRomanianPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.englishToRomanianPseudocodeToolStripMenuItem_Click);
            // 
            // romanianToEnglishPseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.romanianToEnglishPseudocodeToolStripMenuItem, "romanianToEnglishPseudocodeToolStripMenuItem");
            this.romanianToEnglishPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.romanianToEnglishPseudocodeToolStripMenuItem.Name = "romanianToEnglishPseudocodeToolStripMenuItem";
            this.romanianToEnglishPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.romanianToEnglishPseudocodeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem14
            // 
            resources.ApplyResources(this.toolStripMenuItem14, "toolStripMenuItem14");
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            // 
            // analizeExecutedLinesToolStripMenuItem
            // 
            resources.ApplyResources(this.analizeExecutedLinesToolStripMenuItem, "analizeExecutedLinesToolStripMenuItem");
            this.analizeExecutedLinesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.analizeExecutedLinesToolStripMenuItem.Name = "analizeExecutedLinesToolStripMenuItem";
            this.analizeExecutedLinesToolStripMenuItem.Click += new System.EventHandler(this.analizeExecutedLinesToolStripMenuItem_Click);
            // 
            // dEBUGToolStripMenuItem
            // 
            resources.ApplyResources(this.dEBUGToolStripMenuItem, "dEBUGToolStripMenuItem");
            this.dEBUGToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compileAndRunToolStripMenuItem,
            this.justCompileToolStripMenuItem,
            this.startDebuggingToolStripMenuItem,
            this.toolStripMenuItem21,
            this.continueDebuggingToolStripMenuItem,
            this.watchesToolStripMenuItem,
            this.sendCommandToolStripMenuItem,
            this.toolStripMenuItem22,
            this.addremoveBreakpointToolStripMenuItem,
            this.addremoveBreakpointsOnLinesToolStripMenuItem,
            this.addremoveWatchToolStripMenuItem,
            this.toggleBookmarkToolStripMenuItem,
            this.toolStripMenuItem23,
            this.endPseudocodeToolStripMenuItem,
            this.restartPseudocodeToolStripMenuItem});
            this.dEBUGToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.dEBUGToolStripMenuItem.Name = "dEBUGToolStripMenuItem";
            // 
            // compileAndRunToolStripMenuItem
            // 
            resources.ApplyResources(this.compileAndRunToolStripMenuItem, "compileAndRunToolStripMenuItem");
            this.compileAndRunToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.compileAndRunToolStripMenuItem.Name = "compileAndRunToolStripMenuItem";
            this.compileAndRunToolStripMenuItem.Click += new System.EventHandler(this.compileAndRunToolStripMenuItem_Click);
            // 
            // justCompileToolStripMenuItem
            // 
            resources.ApplyResources(this.justCompileToolStripMenuItem, "justCompileToolStripMenuItem");
            this.justCompileToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.justCompileToolStripMenuItem.Name = "justCompileToolStripMenuItem";
            this.justCompileToolStripMenuItem.Click += new System.EventHandler(this.justCompileToolStripMenuItem_Click);
            // 
            // startDebuggingToolStripMenuItem
            // 
            resources.ApplyResources(this.startDebuggingToolStripMenuItem, "startDebuggingToolStripMenuItem");
            this.startDebuggingToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.startDebuggingToolStripMenuItem.Name = "startDebuggingToolStripMenuItem";
            this.startDebuggingToolStripMenuItem.Click += new System.EventHandler(this.startDebuggingToolStripMenuItem_Click);
            // 
            // toolStripMenuItem21
            // 
            resources.ApplyResources(this.toolStripMenuItem21, "toolStripMenuItem21");
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            // 
            // continueDebuggingToolStripMenuItem
            // 
            resources.ApplyResources(this.continueDebuggingToolStripMenuItem, "continueDebuggingToolStripMenuItem");
            this.continueDebuggingToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.continueDebuggingToolStripMenuItem.Name = "continueDebuggingToolStripMenuItem";
            this.continueDebuggingToolStripMenuItem.Click += new System.EventHandler(this.continueDebuggingToolStripMenuItem_Click);
            // 
            // watchesToolStripMenuItem
            // 
            resources.ApplyResources(this.watchesToolStripMenuItem, "watchesToolStripMenuItem");
            this.watchesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.watchesToolStripMenuItem.Name = "watchesToolStripMenuItem";
            this.watchesToolStripMenuItem.Click += new System.EventHandler(this.watchesToolStripMenuItem_Click);
            // 
            // sendCommandToolStripMenuItem
            // 
            resources.ApplyResources(this.sendCommandToolStripMenuItem, "sendCommandToolStripMenuItem");
            this.sendCommandToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.sendCommandToolStripMenuItem.Name = "sendCommandToolStripMenuItem";
            this.sendCommandToolStripMenuItem.Click += new System.EventHandler(this.sendCommandToolStripMenuItem_Click);
            // 
            // toolStripMenuItem22
            // 
            resources.ApplyResources(this.toolStripMenuItem22, "toolStripMenuItem22");
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            // 
            // addremoveBreakpointToolStripMenuItem
            // 
            resources.ApplyResources(this.addremoveBreakpointToolStripMenuItem, "addremoveBreakpointToolStripMenuItem");
            this.addremoveBreakpointToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.addremoveBreakpointToolStripMenuItem.Name = "addremoveBreakpointToolStripMenuItem";
            this.addremoveBreakpointToolStripMenuItem.Click += new System.EventHandler(this.addremoveBreakpointToolStripMenuItem_Click);
            // 
            // addremoveBreakpointsOnLinesToolStripMenuItem
            // 
            resources.ApplyResources(this.addremoveBreakpointsOnLinesToolStripMenuItem, "addremoveBreakpointsOnLinesToolStripMenuItem");
            this.addremoveBreakpointsOnLinesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.addremoveBreakpointsOnLinesToolStripMenuItem.Name = "addremoveBreakpointsOnLinesToolStripMenuItem";
            this.addremoveBreakpointsOnLinesToolStripMenuItem.Click += new System.EventHandler(this.addremoveBreakpointsOnLinesToolStripMenuItem_Click);
            // 
            // addremoveWatchToolStripMenuItem
            // 
            resources.ApplyResources(this.addremoveWatchToolStripMenuItem, "addremoveWatchToolStripMenuItem");
            this.addremoveWatchToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.addremoveWatchToolStripMenuItem.Name = "addremoveWatchToolStripMenuItem";
            this.addremoveWatchToolStripMenuItem.Click += new System.EventHandler(this.addremoveWatchToolStripMenuItem_Click);
            // 
            // toggleBookmarkToolStripMenuItem
            // 
            resources.ApplyResources(this.toggleBookmarkToolStripMenuItem, "toggleBookmarkToolStripMenuItem");
            this.toggleBookmarkToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toggleBookmarkToolStripMenuItem.Name = "toggleBookmarkToolStripMenuItem";
            // 
            // toolStripMenuItem23
            // 
            resources.ApplyResources(this.toolStripMenuItem23, "toolStripMenuItem23");
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            // 
            // endPseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.endPseudocodeToolStripMenuItem, "endPseudocodeToolStripMenuItem");
            this.endPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.endPseudocodeToolStripMenuItem.Name = "endPseudocodeToolStripMenuItem";
            this.endPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.endPseudocodeToolStripMenuItem_Click);
            // 
            // restartPseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.restartPseudocodeToolStripMenuItem, "restartPseudocodeToolStripMenuItem");
            this.restartPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.restartPseudocodeToolStripMenuItem.Name = "restartPseudocodeToolStripMenuItem";
            this.restartPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.restartPseudocodeToolStripMenuItem_Click);
            // 
            // tOOLSToolStripMenuItem
            // 
            resources.ApplyResources(this.tOOLSToolStripMenuItem, "tOOLSToolStripMenuItem");
            this.tOOLSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationToolStripMenuItem,
            this.signInToolStripMenuItem,
            this.menuSearchToolStripMenuItem,
            this.clearCoverageDataToolStripMenuItem,
            this.toolStripMenuItem24,
            this.createApplicationToolStripMenuItem,
            this.uploadProblemToInfoarenaToolStripMenuItem,
            this.insertModuleToolStripMenuItem,
            this.toolStripMenuItem25,
            this.testCurrentConfigurationToolStripMenuItem,
            this.extensionsManagerToolStripMenuItem,
            this.manageCustomCompilersToolStripMenuItem1,
            this.administrativeConfigurationsManagerToolStripMenuItem,
            this.toolStripMenuItem33,
            this.customizeToolbarToolStripMenuItem});
            this.tOOLSToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.tOOLSToolStripMenuItem.Name = "tOOLSToolStripMenuItem";
            // 
            // configurationToolStripMenuItem
            // 
            resources.ApplyResources(this.configurationToolStripMenuItem, "configurationToolStripMenuItem");
            this.configurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // signInToolStripMenuItem
            // 
            resources.ApplyResources(this.signInToolStripMenuItem, "signInToolStripMenuItem");
            this.signInToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.signInToolStripMenuItem.Name = "signInToolStripMenuItem";
            this.signInToolStripMenuItem.Click += new System.EventHandler(this.signInToolStripMenuItem_Click);
            // 
            // menuSearchToolStripMenuItem
            // 
            resources.ApplyResources(this.menuSearchToolStripMenuItem, "menuSearchToolStripMenuItem");
            this.menuSearchToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.menuSearchToolStripMenuItem.Name = "menuSearchToolStripMenuItem";
            this.menuSearchToolStripMenuItem.Click += new System.EventHandler(this.menuSearchToolStripMenuItem_Click);
            // 
            // clearCoverageDataToolStripMenuItem
            // 
            resources.ApplyResources(this.clearCoverageDataToolStripMenuItem, "clearCoverageDataToolStripMenuItem");
            this.clearCoverageDataToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.clearCoverageDataToolStripMenuItem.Name = "clearCoverageDataToolStripMenuItem";
            this.clearCoverageDataToolStripMenuItem.Click += new System.EventHandler(this.clearCoverageDataToolStripMenuItem_Click);
            // 
            // toolStripMenuItem24
            // 
            resources.ApplyResources(this.toolStripMenuItem24, "toolStripMenuItem24");
            this.toolStripMenuItem24.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            // 
            // createApplicationToolStripMenuItem
            // 
            resources.ApplyResources(this.createApplicationToolStripMenuItem, "createApplicationToolStripMenuItem");
            this.createApplicationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.createApplicationToolStripMenuItem.Name = "createApplicationToolStripMenuItem";
            this.createApplicationToolStripMenuItem.Click += new System.EventHandler(this.createApplicationToolStripMenuItem_Click);
            // 
            // uploadProblemToInfoarenaToolStripMenuItem
            // 
            resources.ApplyResources(this.uploadProblemToInfoarenaToolStripMenuItem, "uploadProblemToInfoarenaToolStripMenuItem");
            this.uploadProblemToInfoarenaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.uploadProblemToInfoarenaToolStripMenuItem.Name = "uploadProblemToInfoarenaToolStripMenuItem";
            this.uploadProblemToInfoarenaToolStripMenuItem.Click += new System.EventHandler(this.uploadProblemToInfoarenaToolStripMenuItem_Click);
            // 
            // insertModuleToolStripMenuItem
            // 
            resources.ApplyResources(this.insertModuleToolStripMenuItem, "insertModuleToolStripMenuItem");
            this.insertModuleToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.insertModuleToolStripMenuItem.Name = "insertModuleToolStripMenuItem";
            this.insertModuleToolStripMenuItem.Click += new System.EventHandler(this.insertModuleToolStripMenuItem_Click);
            // 
            // toolStripMenuItem25
            // 
            resources.ApplyResources(this.toolStripMenuItem25, "toolStripMenuItem25");
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            // 
            // testCurrentConfigurationToolStripMenuItem
            // 
            resources.ApplyResources(this.testCurrentConfigurationToolStripMenuItem, "testCurrentConfigurationToolStripMenuItem");
            this.testCurrentConfigurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.testCurrentConfigurationToolStripMenuItem.Name = "testCurrentConfigurationToolStripMenuItem";
            // 
            // extensionsManagerToolStripMenuItem
            // 
            resources.ApplyResources(this.extensionsManagerToolStripMenuItem, "extensionsManagerToolStripMenuItem");
            this.extensionsManagerToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.extensionsManagerToolStripMenuItem.Name = "extensionsManagerToolStripMenuItem";
            this.extensionsManagerToolStripMenuItem.Click += new System.EventHandler(this.extensionsManagerToolStripMenuItem_Click);
            // 
            // manageCustomCompilersToolStripMenuItem1
            // 
            resources.ApplyResources(this.manageCustomCompilersToolStripMenuItem1, "manageCustomCompilersToolStripMenuItem1");
            this.manageCustomCompilersToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.manageCustomCompilersToolStripMenuItem1.Name = "manageCustomCompilersToolStripMenuItem1";
            this.manageCustomCompilersToolStripMenuItem1.Click += new System.EventHandler(this.manageCustomCompilersToolStripMenuItem1_Click);
            // 
            // administrativeConfigurationsManagerToolStripMenuItem
            // 
            resources.ApplyResources(this.administrativeConfigurationsManagerToolStripMenuItem, "administrativeConfigurationsManagerToolStripMenuItem");
            this.administrativeConfigurationsManagerToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.administrativeConfigurationsManagerToolStripMenuItem.Name = "administrativeConfigurationsManagerToolStripMenuItem";
            // 
            // toolStripMenuItem33
            // 
            resources.ApplyResources(this.toolStripMenuItem33, "toolStripMenuItem33");
            this.toolStripMenuItem33.Name = "toolStripMenuItem33";
            // 
            // customizeToolbarToolStripMenuItem
            // 
            resources.ApplyResources(this.customizeToolbarToolStripMenuItem, "customizeToolbarToolStripMenuItem");
            this.customizeToolbarToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.customizeToolbarToolStripMenuItem.Name = "customizeToolbarToolStripMenuItem";
            this.customizeToolbarToolStripMenuItem.Click += new System.EventHandler(this.customizeToolbarToolStripMenuItem_Click);
            // 
            // wINDOWToolStripMenuItem
            // 
            resources.ApplyResources(this.wINDOWToolStripMenuItem, "wINDOWToolStripMenuItem");
            this.wINDOWToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.wINDOWToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newWindowToolStripMenuItem,
            this.toolStripMenuItem28,
            this.closeAllTabsToolStripMenuItem,
            this.closeCurrentTabToolStripMenuItem,
            this.toolStripMenuItem26,
            this.casacdeWindowsToolStripMenuItem,
            this.tileWindowsToolStripMenuItem,
            this.showWindowsSideBySideToolStripMenuItem,
            this.toolStripMenuItem27,
            this.restoreDownToolStripMenuItem,
            this.moveToolStripMenuItem,
            this.sizeToolStripMenuItem,
            this.minimizeToolStripMenuItem,
            this.maximizeToolStripMenuItem,
            this.toolStripMenuItem29});
            this.wINDOWToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.wINDOWToolStripMenuItem.Name = "wINDOWToolStripMenuItem";
            // 
            // newWindowToolStripMenuItem
            // 
            resources.ApplyResources(this.newWindowToolStripMenuItem, "newWindowToolStripMenuItem");
            this.newWindowToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.newWindowToolStripMenuItem.Name = "newWindowToolStripMenuItem";
            // 
            // toolStripMenuItem28
            // 
            resources.ApplyResources(this.toolStripMenuItem28, "toolStripMenuItem28");
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            // 
            // closeAllTabsToolStripMenuItem
            // 
            resources.ApplyResources(this.closeAllTabsToolStripMenuItem, "closeAllTabsToolStripMenuItem");
            this.closeAllTabsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.closeAllTabsToolStripMenuItem.Name = "closeAllTabsToolStripMenuItem";
            // 
            // closeCurrentTabToolStripMenuItem
            // 
            resources.ApplyResources(this.closeCurrentTabToolStripMenuItem, "closeCurrentTabToolStripMenuItem");
            this.closeCurrentTabToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.closeCurrentTabToolStripMenuItem.Name = "closeCurrentTabToolStripMenuItem";
            // 
            // toolStripMenuItem26
            // 
            resources.ApplyResources(this.toolStripMenuItem26, "toolStripMenuItem26");
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            // 
            // casacdeWindowsToolStripMenuItem
            // 
            resources.ApplyResources(this.casacdeWindowsToolStripMenuItem, "casacdeWindowsToolStripMenuItem");
            this.casacdeWindowsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.casacdeWindowsToolStripMenuItem.Name = "casacdeWindowsToolStripMenuItem";
            // 
            // tileWindowsToolStripMenuItem
            // 
            resources.ApplyResources(this.tileWindowsToolStripMenuItem, "tileWindowsToolStripMenuItem");
            this.tileWindowsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.tileWindowsToolStripMenuItem.Name = "tileWindowsToolStripMenuItem";
            // 
            // showWindowsSideBySideToolStripMenuItem
            // 
            resources.ApplyResources(this.showWindowsSideBySideToolStripMenuItem, "showWindowsSideBySideToolStripMenuItem");
            this.showWindowsSideBySideToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.showWindowsSideBySideToolStripMenuItem.Name = "showWindowsSideBySideToolStripMenuItem";
            // 
            // toolStripMenuItem27
            // 
            resources.ApplyResources(this.toolStripMenuItem27, "toolStripMenuItem27");
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            // 
            // restoreDownToolStripMenuItem
            // 
            resources.ApplyResources(this.restoreDownToolStripMenuItem, "restoreDownToolStripMenuItem");
            this.restoreDownToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.restoreDownToolStripMenuItem.Name = "restoreDownToolStripMenuItem";
            // 
            // moveToolStripMenuItem
            // 
            resources.ApplyResources(this.moveToolStripMenuItem, "moveToolStripMenuItem");
            this.moveToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.moveToolStripMenuItem.Name = "moveToolStripMenuItem";
            // 
            // sizeToolStripMenuItem
            // 
            resources.ApplyResources(this.sizeToolStripMenuItem, "sizeToolStripMenuItem");
            this.sizeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.sizeToolStripMenuItem.Name = "sizeToolStripMenuItem";
            // 
            // minimizeToolStripMenuItem
            // 
            resources.ApplyResources(this.minimizeToolStripMenuItem, "minimizeToolStripMenuItem");
            this.minimizeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.minimizeToolStripMenuItem.Name = "minimizeToolStripMenuItem";
            // 
            // maximizeToolStripMenuItem
            // 
            resources.ApplyResources(this.maximizeToolStripMenuItem, "maximizeToolStripMenuItem");
            this.maximizeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.maximizeToolStripMenuItem.Name = "maximizeToolStripMenuItem";
            // 
            // toolStripMenuItem29
            // 
            resources.ApplyResources(this.toolStripMenuItem29, "toolStripMenuItem29");
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            // 
            // hELPToolStripMenuItem
            // 
            resources.ApplyResources(this.hELPToolStripMenuItem, "hELPToolStripMenuItem");
            this.hELPToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.hELPToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpSupportAndTipsToolStripMenuItem,
            this.toolStripMenuItem30,
            this.feedbackToolStripMenuItem,
            this.contextualHelpToolStripMenuItem,
            this.whatsThisToolStripMenuItem,
            this.onlineAssistanceToolStripMenuItem,
            this.valiNetOnTheInternetToolStripMenuItem,
            this.toolStripMenuItem31,
            this.checkForUpdatesToolStripMenuItem,
            this.valiNetCustomerExperienceImprovementProgramToolStripMenuItem,
            this.licenseAgreementToolStripMenuItem,
            this.orderPseudocodeOnADVDToolStripMenuItem,
            this.toolStripMenuItem32,
            this.aboutToolStripMenuItem});
            this.hELPToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.hELPToolStripMenuItem.Name = "hELPToolStripMenuItem";
            // 
            // helpSupportAndTipsToolStripMenuItem
            // 
            resources.ApplyResources(this.helpSupportAndTipsToolStripMenuItem, "helpSupportAndTipsToolStripMenuItem");
            this.helpSupportAndTipsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.helpSupportAndTipsToolStripMenuItem.Name = "helpSupportAndTipsToolStripMenuItem";
            this.helpSupportAndTipsToolStripMenuItem.Click += new System.EventHandler(this.helpSupportAndTipsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem30
            // 
            resources.ApplyResources(this.toolStripMenuItem30, "toolStripMenuItem30");
            this.toolStripMenuItem30.Name = "toolStripMenuItem30";
            // 
            // feedbackToolStripMenuItem
            // 
            resources.ApplyResources(this.feedbackToolStripMenuItem, "feedbackToolStripMenuItem");
            this.feedbackToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.feedbackToolStripMenuItem.Name = "feedbackToolStripMenuItem";
            this.feedbackToolStripMenuItem.Click += new System.EventHandler(this.feedbackToolStripMenuItem_Click);
            // 
            // contextualHelpToolStripMenuItem
            // 
            resources.ApplyResources(this.contextualHelpToolStripMenuItem, "contextualHelpToolStripMenuItem");
            this.contextualHelpToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.contextualHelpToolStripMenuItem.Name = "contextualHelpToolStripMenuItem";
            // 
            // whatsThisToolStripMenuItem
            // 
            resources.ApplyResources(this.whatsThisToolStripMenuItem, "whatsThisToolStripMenuItem");
            this.whatsThisToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.whatsThisToolStripMenuItem.Name = "whatsThisToolStripMenuItem";
            // 
            // onlineAssistanceToolStripMenuItem
            // 
            resources.ApplyResources(this.onlineAssistanceToolStripMenuItem, "onlineAssistanceToolStripMenuItem");
            this.onlineAssistanceToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.onlineAssistanceToolStripMenuItem.Name = "onlineAssistanceToolStripMenuItem";
            // 
            // valiNetOnTheInternetToolStripMenuItem
            // 
            resources.ApplyResources(this.valiNetOnTheInternetToolStripMenuItem, "valiNetOnTheInternetToolStripMenuItem");
            this.valiNetOnTheInternetToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.valiNetOnTheInternetToolStripMenuItem.Name = "valiNetOnTheInternetToolStripMenuItem";
            // 
            // toolStripMenuItem31
            // 
            resources.ApplyResources(this.toolStripMenuItem31, "toolStripMenuItem31");
            this.toolStripMenuItem31.Name = "toolStripMenuItem31";
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            resources.ApplyResources(this.checkForUpdatesToolStripMenuItem, "checkForUpdatesToolStripMenuItem");
            this.checkForUpdatesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            this.checkForUpdatesToolStripMenuItem.Click += new System.EventHandler(this.checkForUpdatesToolStripMenuItem_Click);
            // 
            // valiNetCustomerExperienceImprovementProgramToolStripMenuItem
            // 
            resources.ApplyResources(this.valiNetCustomerExperienceImprovementProgramToolStripMenuItem, "valiNetCustomerExperienceImprovementProgramToolStripMenuItem");
            this.valiNetCustomerExperienceImprovementProgramToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.valiNetCustomerExperienceImprovementProgramToolStripMenuItem.Name = "valiNetCustomerExperienceImprovementProgramToolStripMenuItem";
            // 
            // licenseAgreementToolStripMenuItem
            // 
            resources.ApplyResources(this.licenseAgreementToolStripMenuItem, "licenseAgreementToolStripMenuItem");
            this.licenseAgreementToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.licenseAgreementToolStripMenuItem.Name = "licenseAgreementToolStripMenuItem";
            this.licenseAgreementToolStripMenuItem.Click += new System.EventHandler(this.licenseAgreementToolStripMenuItem_Click);
            // 
            // orderPseudocodeOnADVDToolStripMenuItem
            // 
            resources.ApplyResources(this.orderPseudocodeOnADVDToolStripMenuItem, "orderPseudocodeOnADVDToolStripMenuItem");
            this.orderPseudocodeOnADVDToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.orderPseudocodeOnADVDToolStripMenuItem.Name = "orderPseudocodeOnADVDToolStripMenuItem";
            // 
            // toolStripMenuItem32
            // 
            resources.ApplyResources(this.toolStripMenuItem32, "toolStripMenuItem32");
            this.toolStripMenuItem32.Name = "toolStripMenuItem32";
            // 
            // aboutToolStripMenuItem
            // 
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // fILEToolStripMenuItem1
            // 
            resources.ApplyResources(this.fILEToolStripMenuItem1, "fILEToolStripMenuItem1");
            this.fILEToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newBrowserWindowToolStripMenuItem,
            this.newBrowserWindowToolStripMenuItem1,
            this.duplicateBrowserWindowToolStripMenuItem,
            this.openToolStripMenuItem,
            this.savePageAsToolStripMenuItem,
            this.closePageToolStripMenuItem,
            this.toolStripMenuItem15,
            this.pageSetupToolStripMenuItem,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.toolStripMenuItem17,
            this.sendPageByEmailToolStripMenuItem,
            this.createShortcutToDesktopToolStripMenuItem,
            this.toolStripMenuItem18,
            this.quitPseudocodeToolStripMenuItem});
            this.fILEToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.fILEToolStripMenuItem1.Name = "fILEToolStripMenuItem1";
            // 
            // newBrowserWindowToolStripMenuItem
            // 
            resources.ApplyResources(this.newBrowserWindowToolStripMenuItem, "newBrowserWindowToolStripMenuItem");
            this.newBrowserWindowToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.newBrowserWindowToolStripMenuItem.Name = "newBrowserWindowToolStripMenuItem";
            this.newBrowserWindowToolStripMenuItem.Click += new System.EventHandler(this.newBrowserWindowToolStripMenuItem_Click);
            // 
            // newBrowserWindowToolStripMenuItem1
            // 
            resources.ApplyResources(this.newBrowserWindowToolStripMenuItem1, "newBrowserWindowToolStripMenuItem1");
            this.newBrowserWindowToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.newBrowserWindowToolStripMenuItem1.Name = "newBrowserWindowToolStripMenuItem1";
            this.newBrowserWindowToolStripMenuItem1.Click += new System.EventHandler(this.newBrowserWindowToolStripMenuItem1_Click);
            // 
            // duplicateBrowserWindowToolStripMenuItem
            // 
            resources.ApplyResources(this.duplicateBrowserWindowToolStripMenuItem, "duplicateBrowserWindowToolStripMenuItem");
            this.duplicateBrowserWindowToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.duplicateBrowserWindowToolStripMenuItem.Name = "duplicateBrowserWindowToolStripMenuItem";
            this.duplicateBrowserWindowToolStripMenuItem.Click += new System.EventHandler(this.duplicateBrowserWindowToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            resources.ApplyResources(this.openToolStripMenuItem, "openToolStripMenuItem");
            this.openToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // savePageAsToolStripMenuItem
            // 
            resources.ApplyResources(this.savePageAsToolStripMenuItem, "savePageAsToolStripMenuItem");
            this.savePageAsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.savePageAsToolStripMenuItem.Name = "savePageAsToolStripMenuItem";
            this.savePageAsToolStripMenuItem.Click += new System.EventHandler(this.savePageAsToolStripMenuItem_Click);
            // 
            // closePageToolStripMenuItem
            // 
            resources.ApplyResources(this.closePageToolStripMenuItem, "closePageToolStripMenuItem");
            this.closePageToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.closePageToolStripMenuItem.Name = "closePageToolStripMenuItem";
            this.closePageToolStripMenuItem.Click += new System.EventHandler(this.closePageToolStripMenuItem_Click);
            // 
            // toolStripMenuItem15
            // 
            resources.ApplyResources(this.toolStripMenuItem15, "toolStripMenuItem15");
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            // 
            // pageSetupToolStripMenuItem
            // 
            resources.ApplyResources(this.pageSetupToolStripMenuItem, "pageSetupToolStripMenuItem");
            this.pageSetupToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.pageSetupToolStripMenuItem.Name = "pageSetupToolStripMenuItem";
            this.pageSetupToolStripMenuItem.Click += new System.EventHandler(this.pageSetupToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            resources.ApplyResources(this.printToolStripMenuItem, "printToolStripMenuItem");
            this.printToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // printPreviewToolStripMenuItem
            // 
            resources.ApplyResources(this.printPreviewToolStripMenuItem, "printPreviewToolStripMenuItem");
            this.printPreviewToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Click += new System.EventHandler(this.printPreviewToolStripMenuItem_Click);
            // 
            // toolStripMenuItem17
            // 
            resources.ApplyResources(this.toolStripMenuItem17, "toolStripMenuItem17");
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            // 
            // sendPageByEmailToolStripMenuItem
            // 
            resources.ApplyResources(this.sendPageByEmailToolStripMenuItem, "sendPageByEmailToolStripMenuItem");
            this.sendPageByEmailToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.sendPageByEmailToolStripMenuItem.Name = "sendPageByEmailToolStripMenuItem";
            this.sendPageByEmailToolStripMenuItem.Click += new System.EventHandler(this.sendPageByEmailToolStripMenuItem_Click);
            // 
            // createShortcutToDesktopToolStripMenuItem
            // 
            resources.ApplyResources(this.createShortcutToDesktopToolStripMenuItem, "createShortcutToDesktopToolStripMenuItem");
            this.createShortcutToDesktopToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.createShortcutToDesktopToolStripMenuItem.Name = "createShortcutToDesktopToolStripMenuItem";
            this.createShortcutToDesktopToolStripMenuItem.Click += new System.EventHandler(this.createShortcutToDesktopToolStripMenuItem_Click);
            // 
            // toolStripMenuItem18
            // 
            resources.ApplyResources(this.toolStripMenuItem18, "toolStripMenuItem18");
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            // 
            // quitPseudocodeToolStripMenuItem
            // 
            resources.ApplyResources(this.quitPseudocodeToolStripMenuItem, "quitPseudocodeToolStripMenuItem");
            this.quitPseudocodeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.quitPseudocodeToolStripMenuItem.Name = "quitPseudocodeToolStripMenuItem";
            this.quitPseudocodeToolStripMenuItem.Click += new System.EventHandler(this.quitPseudocodeToolStripMenuItem_Click);
            // 
            // eDITToolStripMenuItem1
            // 
            resources.ApplyResources(this.eDITToolStripMenuItem1, "eDITToolStripMenuItem1");
            this.eDITToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem1,
            this.copyToolStripMenuItem1,
            this.pasteToolStripMenuItem1,
            this.toolStripMenuItem19,
            this.selectAllToolStripMenuItem1,
            this.toolStripMenuItem34,
            this.findonThisPageToolStripMenuItem});
            this.eDITToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.eDITToolStripMenuItem1.Name = "eDITToolStripMenuItem1";
            // 
            // cutToolStripMenuItem1
            // 
            resources.ApplyResources(this.cutToolStripMenuItem1, "cutToolStripMenuItem1");
            this.cutToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.cutToolStripMenuItem1.Name = "cutToolStripMenuItem1";
            this.cutToolStripMenuItem1.Click += new System.EventHandler(this.cutToolStripMenuItem1_Click);
            // 
            // copyToolStripMenuItem1
            // 
            resources.ApplyResources(this.copyToolStripMenuItem1, "copyToolStripMenuItem1");
            this.copyToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.copyToolStripMenuItem1.Name = "copyToolStripMenuItem1";
            this.copyToolStripMenuItem1.Click += new System.EventHandler(this.copyToolStripMenuItem1_Click);
            // 
            // pasteToolStripMenuItem1
            // 
            resources.ApplyResources(this.pasteToolStripMenuItem1, "pasteToolStripMenuItem1");
            this.pasteToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.pasteToolStripMenuItem1.Name = "pasteToolStripMenuItem1";
            this.pasteToolStripMenuItem1.Click += new System.EventHandler(this.pasteToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem19
            // 
            resources.ApplyResources(this.toolStripMenuItem19, "toolStripMenuItem19");
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            // 
            // selectAllToolStripMenuItem1
            // 
            resources.ApplyResources(this.selectAllToolStripMenuItem1, "selectAllToolStripMenuItem1");
            this.selectAllToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.selectAllToolStripMenuItem1.Name = "selectAllToolStripMenuItem1";
            this.selectAllToolStripMenuItem1.Click += new System.EventHandler(this.selectAllToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem34
            // 
            resources.ApplyResources(this.toolStripMenuItem34, "toolStripMenuItem34");
            this.toolStripMenuItem34.Name = "toolStripMenuItem34";
            // 
            // findonThisPageToolStripMenuItem
            // 
            resources.ApplyResources(this.findonThisPageToolStripMenuItem, "findonThisPageToolStripMenuItem");
            this.findonThisPageToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.findonThisPageToolStripMenuItem.Name = "findonThisPageToolStripMenuItem";
            this.findonThisPageToolStripMenuItem.Click += new System.EventHandler(this.findonThisPageToolStripMenuItem_Click);
            // 
            // vIEWToolStripMenuItem1
            // 
            resources.ApplyResources(this.vIEWToolStripMenuItem1, "vIEWToolStripMenuItem1");
            this.vIEWToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.navigateBackToolStripMenuItem,
            this.navigateForwardToolStripMenuItem,
            this.openHomePageToolStripMenuItem,
            this.stopLoadingToolStripMenuItem,
            this.refreshToolStripMenuItem,
            this.toolStripMenuItem35,
            this.zoomToolStripMenuItem1,
            this.textSizeToolStripMenuItem,
            this.toolStripMenuItem36,
            this.sourceToolStripMenuItem});
            this.vIEWToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.vIEWToolStripMenuItem1.Name = "vIEWToolStripMenuItem1";
            // 
            // navigateBackToolStripMenuItem
            // 
            resources.ApplyResources(this.navigateBackToolStripMenuItem, "navigateBackToolStripMenuItem");
            this.navigateBackToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.navigateBackToolStripMenuItem.Name = "navigateBackToolStripMenuItem";
            this.navigateBackToolStripMenuItem.Click += new System.EventHandler(this.navigateBackToolStripMenuItem_Click);
            // 
            // navigateForwardToolStripMenuItem
            // 
            resources.ApplyResources(this.navigateForwardToolStripMenuItem, "navigateForwardToolStripMenuItem");
            this.navigateForwardToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.navigateForwardToolStripMenuItem.Name = "navigateForwardToolStripMenuItem";
            this.navigateForwardToolStripMenuItem.Click += new System.EventHandler(this.navigateForwardToolStripMenuItem_Click);
            // 
            // openHomePageToolStripMenuItem
            // 
            resources.ApplyResources(this.openHomePageToolStripMenuItem, "openHomePageToolStripMenuItem");
            this.openHomePageToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.openHomePageToolStripMenuItem.Name = "openHomePageToolStripMenuItem";
            this.openHomePageToolStripMenuItem.Click += new System.EventHandler(this.openHomePageToolStripMenuItem_Click);
            // 
            // stopLoadingToolStripMenuItem
            // 
            resources.ApplyResources(this.stopLoadingToolStripMenuItem, "stopLoadingToolStripMenuItem");
            this.stopLoadingToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.stopLoadingToolStripMenuItem.Name = "stopLoadingToolStripMenuItem";
            this.stopLoadingToolStripMenuItem.Click += new System.EventHandler(this.stopLoadingToolStripMenuItem_Click);
            // 
            // refreshToolStripMenuItem
            // 
            resources.ApplyResources(this.refreshToolStripMenuItem, "refreshToolStripMenuItem");
            this.refreshToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // toolStripMenuItem35
            // 
            resources.ApplyResources(this.toolStripMenuItem35, "toolStripMenuItem35");
            this.toolStripMenuItem35.Name = "toolStripMenuItem35";
            // 
            // zoomToolStripMenuItem1
            // 
            resources.ApplyResources(this.zoomToolStripMenuItem1, "zoomToolStripMenuItem1");
            this.zoomToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zoomInToolStripMenuItem1,
            this.zoomOutToolStripMenuItem1,
            this.toolStripMenuItem42,
            this.toolStripMenuItem44,
            this.toolStripMenuItem45,
            this.toolStripMenuItem46,
            this.toolStripMenuItem47,
            this.toolStripMenuItem48,
            this.toolStripMenuItem49,
            this.toolStripMenuItem50,
            this.toolStripMenuItem51,
            this.toolStripMenuItem52,
            this.toolStripMenuItem53,
            this.toolStripMenuItem43,
            this.customizeToolStripMenuItem1});
            this.zoomToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.zoomToolStripMenuItem1.Name = "zoomToolStripMenuItem1";
            // 
            // zoomInToolStripMenuItem1
            // 
            resources.ApplyResources(this.zoomInToolStripMenuItem1, "zoomInToolStripMenuItem1");
            this.zoomInToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.zoomInToolStripMenuItem1.Name = "zoomInToolStripMenuItem1";
            this.zoomInToolStripMenuItem1.Click += new System.EventHandler(this.zoomInToolStripMenuItem1_Click);
            // 
            // zoomOutToolStripMenuItem1
            // 
            resources.ApplyResources(this.zoomOutToolStripMenuItem1, "zoomOutToolStripMenuItem1");
            this.zoomOutToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.zoomOutToolStripMenuItem1.Name = "zoomOutToolStripMenuItem1";
            this.zoomOutToolStripMenuItem1.Click += new System.EventHandler(this.zoomOutToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem42
            // 
            resources.ApplyResources(this.toolStripMenuItem42, "toolStripMenuItem42");
            this.toolStripMenuItem42.Name = "toolStripMenuItem42";
            // 
            // toolStripMenuItem44
            // 
            resources.ApplyResources(this.toolStripMenuItem44, "toolStripMenuItem44");
            this.toolStripMenuItem44.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem44.Name = "toolStripMenuItem44";
            this.toolStripMenuItem44.Click += new System.EventHandler(this.toolStripMenuItem44_Click);
            // 
            // toolStripMenuItem45
            // 
            resources.ApplyResources(this.toolStripMenuItem45, "toolStripMenuItem45");
            this.toolStripMenuItem45.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem45.Name = "toolStripMenuItem45";
            this.toolStripMenuItem45.Click += new System.EventHandler(this.toolStripMenuItem45_Click);
            // 
            // toolStripMenuItem46
            // 
            resources.ApplyResources(this.toolStripMenuItem46, "toolStripMenuItem46");
            this.toolStripMenuItem46.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem46.Name = "toolStripMenuItem46";
            this.toolStripMenuItem46.Click += new System.EventHandler(this.toolStripMenuItem46_Click);
            // 
            // toolStripMenuItem47
            // 
            resources.ApplyResources(this.toolStripMenuItem47, "toolStripMenuItem47");
            this.toolStripMenuItem47.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem47.Name = "toolStripMenuItem47";
            this.toolStripMenuItem47.Click += new System.EventHandler(this.toolStripMenuItem47_Click);
            // 
            // toolStripMenuItem48
            // 
            resources.ApplyResources(this.toolStripMenuItem48, "toolStripMenuItem48");
            this.toolStripMenuItem48.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem48.Name = "toolStripMenuItem48";
            this.toolStripMenuItem48.Click += new System.EventHandler(this.toolStripMenuItem48_Click);
            // 
            // toolStripMenuItem49
            // 
            resources.ApplyResources(this.toolStripMenuItem49, "toolStripMenuItem49");
            this.toolStripMenuItem49.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem49.Name = "toolStripMenuItem49";
            this.toolStripMenuItem49.Click += new System.EventHandler(this.toolStripMenuItem49_Click);
            // 
            // toolStripMenuItem50
            // 
            resources.ApplyResources(this.toolStripMenuItem50, "toolStripMenuItem50");
            this.toolStripMenuItem50.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem50.Name = "toolStripMenuItem50";
            this.toolStripMenuItem50.Click += new System.EventHandler(this.toolStripMenuItem50_Click);
            // 
            // toolStripMenuItem51
            // 
            resources.ApplyResources(this.toolStripMenuItem51, "toolStripMenuItem51");
            this.toolStripMenuItem51.Checked = true;
            this.toolStripMenuItem51.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem51.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem51.Name = "toolStripMenuItem51";
            this.toolStripMenuItem51.Click += new System.EventHandler(this.toolStripMenuItem51_Click);
            // 
            // toolStripMenuItem52
            // 
            resources.ApplyResources(this.toolStripMenuItem52, "toolStripMenuItem52");
            this.toolStripMenuItem52.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem52.Name = "toolStripMenuItem52";
            this.toolStripMenuItem52.Click += new System.EventHandler(this.toolStripMenuItem52_Click);
            // 
            // toolStripMenuItem53
            // 
            resources.ApplyResources(this.toolStripMenuItem53, "toolStripMenuItem53");
            this.toolStripMenuItem53.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem53.Name = "toolStripMenuItem53";
            this.toolStripMenuItem53.Click += new System.EventHandler(this.toolStripMenuItem53_Click);
            // 
            // toolStripMenuItem43
            // 
            resources.ApplyResources(this.toolStripMenuItem43, "toolStripMenuItem43");
            this.toolStripMenuItem43.Name = "toolStripMenuItem43";
            // 
            // customizeToolStripMenuItem1
            // 
            resources.ApplyResources(this.customizeToolStripMenuItem1, "customizeToolStripMenuItem1");
            this.customizeToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.customizeToolStripMenuItem1.Name = "customizeToolStripMenuItem1";
            this.customizeToolStripMenuItem1.Click += new System.EventHandler(this.customizeToolStripMenuItem1_Click);
            // 
            // textSizeToolStripMenuItem
            // 
            resources.ApplyResources(this.textSizeToolStripMenuItem, "textSizeToolStripMenuItem");
            this.textSizeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smallestToolStripMenuItem,
            this.smallToolStripMenuItem,
            this.defaultToolStripMenuItem,
            this.largeToolStripMenuItem,
            this.largestToolStripMenuItem});
            this.textSizeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.textSizeToolStripMenuItem.Name = "textSizeToolStripMenuItem";
            // 
            // smallestToolStripMenuItem
            // 
            resources.ApplyResources(this.smallestToolStripMenuItem, "smallestToolStripMenuItem");
            this.smallestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.smallestToolStripMenuItem.Name = "smallestToolStripMenuItem";
            this.smallestToolStripMenuItem.Click += new System.EventHandler(this.smallestToolStripMenuItem_Click);
            // 
            // smallToolStripMenuItem
            // 
            resources.ApplyResources(this.smallToolStripMenuItem, "smallToolStripMenuItem");
            this.smallToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.smallToolStripMenuItem.Name = "smallToolStripMenuItem";
            this.smallToolStripMenuItem.Click += new System.EventHandler(this.smallToolStripMenuItem_Click);
            // 
            // defaultToolStripMenuItem
            // 
            resources.ApplyResources(this.defaultToolStripMenuItem, "defaultToolStripMenuItem");
            this.defaultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.defaultToolStripMenuItem.Name = "defaultToolStripMenuItem";
            this.defaultToolStripMenuItem.Click += new System.EventHandler(this.defaultToolStripMenuItem_Click);
            // 
            // largeToolStripMenuItem
            // 
            resources.ApplyResources(this.largeToolStripMenuItem, "largeToolStripMenuItem");
            this.largeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.largeToolStripMenuItem.Name = "largeToolStripMenuItem";
            this.largeToolStripMenuItem.Click += new System.EventHandler(this.largeToolStripMenuItem_Click);
            // 
            // largestToolStripMenuItem
            // 
            resources.ApplyResources(this.largestToolStripMenuItem, "largestToolStripMenuItem");
            this.largestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.largestToolStripMenuItem.Name = "largestToolStripMenuItem";
            this.largestToolStripMenuItem.Click += new System.EventHandler(this.largestToolStripMenuItem_Click);
            // 
            // toolStripMenuItem36
            // 
            resources.ApplyResources(this.toolStripMenuItem36, "toolStripMenuItem36");
            this.toolStripMenuItem36.Name = "toolStripMenuItem36";
            // 
            // sourceToolStripMenuItem
            // 
            resources.ApplyResources(this.sourceToolStripMenuItem, "sourceToolStripMenuItem");
            this.sourceToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.sourceToolStripMenuItem.Name = "sourceToolStripMenuItem";
            this.sourceToolStripMenuItem.Click += new System.EventHandler(this.sourceToolStripMenuItem_Click);
            // 
            // fAVORITESToolStripMenuItem
            // 
            resources.ApplyResources(this.fAVORITESToolStripMenuItem, "fAVORITESToolStripMenuItem");
            this.fAVORITESToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToFavoritesToolStripMenuItem,
            this.organizeFavoritesToolStripMenuItem,
            this.toolStripMenuItem37,
            this.valiNetWebSiteToolStripMenuItem});
            this.fAVORITESToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fAVORITESToolStripMenuItem.Name = "fAVORITESToolStripMenuItem";
            // 
            // addToFavoritesToolStripMenuItem
            // 
            resources.ApplyResources(this.addToFavoritesToolStripMenuItem, "addToFavoritesToolStripMenuItem");
            this.addToFavoritesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.addToFavoritesToolStripMenuItem.Name = "addToFavoritesToolStripMenuItem";
            this.addToFavoritesToolStripMenuItem.Click += new System.EventHandler(this.addToFavoritesToolStripMenuItem_Click);
            // 
            // organizeFavoritesToolStripMenuItem
            // 
            resources.ApplyResources(this.organizeFavoritesToolStripMenuItem, "organizeFavoritesToolStripMenuItem");
            this.organizeFavoritesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.organizeFavoritesToolStripMenuItem.Name = "organizeFavoritesToolStripMenuItem";
            this.organizeFavoritesToolStripMenuItem.Click += new System.EventHandler(this.organizeFavoritesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem37
            // 
            resources.ApplyResources(this.toolStripMenuItem37, "toolStripMenuItem37");
            this.toolStripMenuItem37.Name = "toolStripMenuItem37";
            // 
            // valiNetWebSiteToolStripMenuItem
            // 
            resources.ApplyResources(this.valiNetWebSiteToolStripMenuItem, "valiNetWebSiteToolStripMenuItem");
            this.valiNetWebSiteToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.valiNetWebSiteToolStripMenuItem.Name = "valiNetWebSiteToolStripMenuItem";
            this.valiNetWebSiteToolStripMenuItem.Click += new System.EventHandler(this.valiNetWebSiteToolStripMenuItem_Click);
            // 
            // tOOLSToolStripMenuItem1
            // 
            resources.ApplyResources(this.tOOLSToolStripMenuItem1, "tOOLSToolStripMenuItem1");
            this.tOOLSToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationToolStripMenuItem1,
            this.menuSearchToolStripMenuItem1,
            this.toolStripMenuItem38,
            this.manageCustomCompilersToolStripMenuItem2,
            this.packageManagerToolStripMenuItem});
            this.tOOLSToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.tOOLSToolStripMenuItem1.Name = "tOOLSToolStripMenuItem1";
            // 
            // configurationToolStripMenuItem1
            // 
            resources.ApplyResources(this.configurationToolStripMenuItem1, "configurationToolStripMenuItem1");
            this.configurationToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.configurationToolStripMenuItem1.Name = "configurationToolStripMenuItem1";
            this.configurationToolStripMenuItem1.Click += new System.EventHandler(this.configurationToolStripMenuItem1_Click);
            // 
            // menuSearchToolStripMenuItem1
            // 
            resources.ApplyResources(this.menuSearchToolStripMenuItem1, "menuSearchToolStripMenuItem1");
            this.menuSearchToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.menuSearchToolStripMenuItem1.Name = "menuSearchToolStripMenuItem1";
            this.menuSearchToolStripMenuItem1.Click += new System.EventHandler(this.menuSearchToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem38
            // 
            resources.ApplyResources(this.toolStripMenuItem38, "toolStripMenuItem38");
            this.toolStripMenuItem38.Name = "toolStripMenuItem38";
            // 
            // manageCustomCompilersToolStripMenuItem2
            // 
            resources.ApplyResources(this.manageCustomCompilersToolStripMenuItem2, "manageCustomCompilersToolStripMenuItem2");
            this.manageCustomCompilersToolStripMenuItem2.ForeColor = System.Drawing.Color.White;
            this.manageCustomCompilersToolStripMenuItem2.Name = "manageCustomCompilersToolStripMenuItem2";
            this.manageCustomCompilersToolStripMenuItem2.Click += new System.EventHandler(this.manageCustomCompilersToolStripMenuItem2_Click);
            // 
            // packageManagerToolStripMenuItem
            // 
            resources.ApplyResources(this.packageManagerToolStripMenuItem, "packageManagerToolStripMenuItem");
            this.packageManagerToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.packageManagerToolStripMenuItem.Name = "packageManagerToolStripMenuItem";
            this.packageManagerToolStripMenuItem.Click += new System.EventHandler(this.packageManagerToolStripMenuItem_Click);
            // 
            // hELPToolStripMenuItem1
            // 
            resources.ApplyResources(this.hELPToolStripMenuItem1, "hELPToolStripMenuItem1");
            this.hELPToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpSupportAndTipsToolStripMenuItem1,
            this.toolStripMenuItem39,
            this.feedbackToolStripMenuItem1,
            this.toolStripMenuItem40,
            this.checkForUpdatesToolStripMenuItem1,
            this.licenseAgreementToolStripMenuItem1,
            this.toolStripMenuItem41,
            this.aboutToolStripMenuItem1});
            this.hELPToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.hELPToolStripMenuItem1.Name = "hELPToolStripMenuItem1";
            // 
            // helpSupportAndTipsToolStripMenuItem1
            // 
            resources.ApplyResources(this.helpSupportAndTipsToolStripMenuItem1, "helpSupportAndTipsToolStripMenuItem1");
            this.helpSupportAndTipsToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.helpSupportAndTipsToolStripMenuItem1.Name = "helpSupportAndTipsToolStripMenuItem1";
            this.helpSupportAndTipsToolStripMenuItem1.Click += new System.EventHandler(this.helpSupportAndTipsToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem39
            // 
            resources.ApplyResources(this.toolStripMenuItem39, "toolStripMenuItem39");
            this.toolStripMenuItem39.Name = "toolStripMenuItem39";
            // 
            // feedbackToolStripMenuItem1
            // 
            resources.ApplyResources(this.feedbackToolStripMenuItem1, "feedbackToolStripMenuItem1");
            this.feedbackToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.feedbackToolStripMenuItem1.Name = "feedbackToolStripMenuItem1";
            this.feedbackToolStripMenuItem1.Click += new System.EventHandler(this.feedbackToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem40
            // 
            resources.ApplyResources(this.toolStripMenuItem40, "toolStripMenuItem40");
            this.toolStripMenuItem40.Name = "toolStripMenuItem40";
            // 
            // checkForUpdatesToolStripMenuItem1
            // 
            resources.ApplyResources(this.checkForUpdatesToolStripMenuItem1, "checkForUpdatesToolStripMenuItem1");
            this.checkForUpdatesToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.checkForUpdatesToolStripMenuItem1.Name = "checkForUpdatesToolStripMenuItem1";
            this.checkForUpdatesToolStripMenuItem1.Click += new System.EventHandler(this.checkForUpdatesToolStripMenuItem1_Click);
            // 
            // licenseAgreementToolStripMenuItem1
            // 
            resources.ApplyResources(this.licenseAgreementToolStripMenuItem1, "licenseAgreementToolStripMenuItem1");
            this.licenseAgreementToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.licenseAgreementToolStripMenuItem1.Name = "licenseAgreementToolStripMenuItem1";
            this.licenseAgreementToolStripMenuItem1.Click += new System.EventHandler(this.licenseAgreementToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem41
            // 
            resources.ApplyResources(this.toolStripMenuItem41, "toolStripMenuItem41");
            this.toolStripMenuItem41.Name = "toolStripMenuItem41";
            // 
            // aboutToolStripMenuItem1
            // 
            resources.ApplyResources(this.aboutToolStripMenuItem1, "aboutToolStripMenuItem1");
            this.aboutToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // transparentControl1
            // 
            resources.ApplyResources(this.transparentControl1, "transparentControl1");
            this.transparentControl1.Name = "transparentControl1";
            this.toolTip1.SetToolTip(this.transparentControl1, resources.GetString("transparentControl1.ToolTip"));
            // 
            // Top
            // 
            resources.ApplyResources(this.Top, "Top");
            this.Top.Name = "Top";
            this.toolTip1.SetToolTip(this.Top, resources.GetString("Top.ToolTip"));
            // 
            // Right
            // 
            resources.ApplyResources(this.Right, "Right");
            this.Right.Name = "Right";
            this.toolTip1.SetToolTip(this.Right, resources.GetString("Right.ToolTip"));
            // 
            // Left
            // 
            resources.ApplyResources(this.Left, "Left");
            this.Left.Name = "Left";
            this.toolTip1.SetToolTip(this.Left, resources.GetString("Left.ToolTip"));
            // 
            // Bottom
            // 
            resources.ApplyResources(this.Bottom, "Bottom");
            this.Bottom.BackColor = System.Drawing.Color.Black;
            this.Bottom.Name = "Bottom";
            this.toolTip1.SetToolTip(this.Bottom, resources.GetString("Bottom.ToolTip"));
            // 
            // dockPanel
            // 
            resources.ApplyResources(this.dockPanel, "dockPanel");
            this.dockPanel.Name = "dockPanel";
            dockPanelGradient1.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dockPanelGradient1.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            tabGradient1.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            tabGradient1.TextColor = System.Drawing.Color.White;
            autoHideStripSkin1.TabGradient = tabGradient1;
            autoHideStripSkin1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            tabGradient2.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            tabGradient2.TextColor = System.Drawing.Color.White;
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.Color.Black;
            dockPanelGradient2.StartColor = System.Drawing.Color.Black;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            tabGradient3.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            tabGradient3.TextColor = System.Drawing.Color.White;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            dockPaneStripSkin1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.SystemColors.Control;
            tabGradient5.StartColor = System.Drawing.SystemColors.Control;
            tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.Color.Black;
            dockPanelGradient3.StartColor = System.Drawing.Color.Black;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.SystemColors.InactiveCaption;
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.TextColor = System.Drawing.SystemColors.InactiveCaptionText;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.Transparent;
            tabGradient7.StartColor = System.Drawing.Color.Transparent;
            tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            this.dockPanel.Skin = dockPanelSkin1;
            this.dockPanel.SupportDeeplyNestedContent = true;
            this.toolTip1.SetToolTip(this.dockPanel, resources.GetString("dockPanel.ToolTip"));
            this.dockPanel.ActiveContentChanged += new System.EventHandler(this.dockPanel_ActiveContentChanged);
            this.dockPanel.ActivePaneChanged += new System.EventHandler(this.dockPanel_ActivePaneChanged);
            this.dockPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.dockPanel_Paint);
            this.dockPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dockPanel_MouseUp);
            // 
            // TopCover
            // 
            resources.ApplyResources(this.TopCover, "TopCover");
            this.TopCover.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TopCover.Name = "TopCover";
            this.toolTip1.SetToolTip(this.TopCover, resources.GetString("TopCover.ToolTip"));
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, resources.GetString("pictureBox1.ToolTip"));
            // 
            // statusStrip1
            // 
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Appearance = this.blackApparance1;
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Name = "statusStrip1";
            this.toolTip1.SetToolTip(this.statusStrip1, resources.GetString("statusStrip1.ToolTip"));
            // 
            // toolStripStatusLabel1
            // 
            resources.ApplyResources(this.toolStripStatusLabel1, "toolStripStatusLabel1");
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            // 
            // BottomDown
            // 
            resources.ApplyResources(this.BottomDown, "BottomDown");
            this.BottomDown.BackColor = System.Drawing.Color.Black;
            this.BottomDown.Name = "BottomDown";
            this.toolTip1.SetToolTip(this.BottomDown, resources.GetString("BottomDown.ToolTip"));
            // 
            // listBox1
            // 
            resources.ApplyResources(this.listBox1, "listBox1");
            this.listBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.listBox1.ForeColor = System.Drawing.Color.White;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Name = "listBox1";
            this.toolTip1.SetToolTip(this.listBox1, resources.GetString("listBox1.ToolTip"));
            this.listBox1.Click += new System.EventHandler(this.listBox1_Click);
            this.listBox1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBox1_DrawItem);
            this.listBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.listBox1_KeyUp);
            this.listBox1.Leave += new System.EventHandler(this.listBox1_Leave);
            this.listBox1.MouseEnter += new System.EventHandler(this.listBox1_MouseEnter);
            this.listBox1.MouseLeave += new System.EventHandler(this.listBox1_MouseLeave);
            this.listBox1.MouseHover += new System.EventHandler(this.listBox1_MouseHover);
            this.listBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseMove);
            // 
            // SearchPanel
            // 
            resources.ApplyResources(this.SearchPanel, "SearchPanel");
            this.SearchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.SearchPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SearchPanel.Controls.Add(this.panel1);
            this.SearchPanel.Controls.Add(this.panel2);
            this.SearchPanel.Controls.Add(this.SearchText);
            this.SearchPanel.Controls.Add(this.listBox1);
            this.SearchPanel.Name = "SearchPanel";
            this.toolTip1.SetToolTip(this.SearchPanel, resources.GetString("SearchPanel.ToolTip"));
            this.SearchPanel.VisibleChanged += new System.EventHandler(this.SearchPanel_VisibleChanged);
            this.SearchPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.SearchPanel_Paint);
            this.SearchPanel.Enter += new System.EventHandler(this.SearchPanel_Enter);
            this.SearchPanel.Leave += new System.EventHandler(this.SearchPanel_Leave);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.toolTip1.SetToolTip(this.panel1, resources.GetString("panel1.ToolTip"));
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Name = "panel2";
            this.toolTip1.SetToolTip(this.panel2, resources.GetString("panel2.ToolTip"));
            // 
            // SearchText
            // 
            resources.ApplyResources(this.SearchText, "SearchText");
            this.SearchText.LinkColor = System.Drawing.Color.White;
            this.SearchText.Name = "SearchText";
            this.SearchText.TabStop = true;
            this.SearchText.Tag = "Rezultatele căutării";
            this.toolTip1.SetToolTip(this.SearchText, resources.GetString("SearchText.ToolTip"));
            // 
            // UserDropPanel
            // 
            resources.ApplyResources(this.UserDropPanel, "UserDropPanel");
            this.UserDropPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.UserDropPanel.Controls.Add(this.button47);
            this.UserDropPanel.Controls.Add(this.linkLabel5);
            this.UserDropPanel.Controls.Add(this.linkLabel4);
            this.UserDropPanel.Controls.Add(this.label28);
            this.UserDropPanel.Controls.Add(this.pictureBox3);
            this.UserDropPanel.Controls.Add(this.pictureBox4);
            this.UserDropPanel.ForeColor = System.Drawing.Color.Black;
            this.UserDropPanel.Name = "UserDropPanel";
            this.UserDropPanel.TabStop = true;
            this.toolTip1.SetToolTip(this.UserDropPanel, resources.GetString("UserDropPanel.ToolTip"));
            this.UserDropPanel.VisibleChanged += new System.EventHandler(this.UserDropPanel_VisibleChanged);
            this.UserDropPanel.Leave += new System.EventHandler(this.UserDropPanel_Leave);
            // 
            // button47
            // 
            resources.ApplyResources(this.button47, "button47");
            this.button47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button47.FlatAppearance.BorderSize = 0;
            this.button47.ForeColor = System.Drawing.Color.Black;
            this.button47.Name = "button47";
            this.toolTip1.SetToolTip(this.button47, resources.GetString("button47.ToolTip"));
            this.button47.UseVisualStyleBackColor = false;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // linkLabel5
            // 
            resources.ApplyResources(this.linkLabel5, "linkLabel5");
            this.linkLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.linkLabel5.ForeColor = System.Drawing.Color.Black;
            this.linkLabel5.LinkColor = System.Drawing.Color.Black;
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.TabStop = true;
            this.toolTip1.SetToolTip(this.linkLabel5, resources.GetString("linkLabel5.ToolTip"));
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // linkLabel4
            // 
            resources.ApplyResources(this.linkLabel4, "linkLabel4");
            this.linkLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.linkLabel4.ForeColor = System.Drawing.Color.Black;
            this.linkLabel4.LinkColor = System.Drawing.Color.Black;
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.TabStop = true;
            this.toolTip1.SetToolTip(this.linkLabel4, resources.GetString("linkLabel4.ToolTip"));
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Name = "label28";
            this.toolTip1.SetToolTip(this.label28, resources.GetString("label28.ToolTip"));
            // 
            // pictureBox3
            // 
            resources.ApplyResources(this.pictureBox3, "pictureBox3");
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pictureBox3.ForeColor = System.Drawing.Color.Black;
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox3, resources.GetString("pictureBox3.ToolTip"));
            // 
            // pictureBox4
            // 
            resources.ApplyResources(this.pictureBox4, "pictureBox4");
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pictureBox4.ForeColor = System.Drawing.Color.Black;
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox4, resources.GetString("pictureBox4.ToolTip"));
            // 
            // UserPanel
            // 
            resources.ApplyResources(this.UserPanel, "UserPanel");
            this.UserPanel.BackColor = System.Drawing.Color.Black;
            this.UserPanel.Controls.Add(this.pictureBox2);
            this.UserPanel.Controls.Add(this.label27);
            this.UserPanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UserPanel.ForeColor = System.Drawing.Color.Black;
            this.UserPanel.Name = "UserPanel";
            this.toolTip1.SetToolTip(this.UserPanel, resources.GetString("UserPanel.ToolTip"));
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pictureBox2.ForeColor = System.Drawing.Color.Black;
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox2, resources.GetString("pictureBox2.ToolTip"));
            this.pictureBox2.Click += new System.EventHandler(this.UserPanel_Click);
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Name = "label27";
            this.toolTip1.SetToolTip(this.label27, resources.GetString("label27.ToolTip"));
            this.label27.Click += new System.EventHandler(this.UserPanel_Click);
            // 
            // SearchBox
            // 
            resources.ApplyResources(this.SearchBox, "SearchBox");
            this.SearchBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.SearchBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SearchBox.ContextMenuStrip = this.contextMenuStrip1;
            this.SearchBox.ForeColor = System.Drawing.Color.Silver;
            this.SearchBox.Name = "SearchBox";
            this.toolTip1.SetToolTip(this.SearchBox, resources.GetString("SearchBox.ToolTip"));
            this.SearchBox.Click += new System.EventHandler(this.SearchBox_Click);
            this.SearchBox.VisibleChanged += new System.EventHandler(this.SearchBox_VisibleChanged);
            this.SearchBox.Enter += new System.EventHandler(this.SearchBox_Enter);
            this.SearchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SearchBox_KeyDown);
            this.SearchBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SearchBox_KeyPress);
            this.SearchBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SearchBox_KeyUp);
            this.SearchBox.Leave += new System.EventHandler(this.SearchBox_Leave);
            this.SearchBox.MouseEnter += new System.EventHandler(this.SearchBox_MouseEnter);
            this.SearchBox.MouseLeave += new System.EventHandler(this.SearchBox_MouseLeave);
            // 
            // imageDisplay
            // 
            resources.ApplyResources(this.imageDisplay, "imageDisplay");
            this.imageDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.imageDisplay.ForeColor = System.Drawing.Color.Black;
            this.imageDisplay.Name = "imageDisplay";
            this.imageDisplay.TabStop = false;
            this.toolTip1.SetToolTip(this.imageDisplay, resources.GetString("imageDisplay.ToolTip"));
            // 
            // MinimizeBlack
            // 
            resources.ApplyResources(this.MinimizeBlack, "MinimizeBlack");
            this.MinimizeBlack.Name = "MinimizeBlack";
            this.toolTip1.SetToolTip(this.MinimizeBlack, resources.GetString("MinimizeBlack.ToolTip"));
            this.MinimizeBlack.UseVisualStyleBackColor = true;
            // 
            // RestoreBlack
            // 
            resources.ApplyResources(this.RestoreBlack, "RestoreBlack");
            this.RestoreBlack.Name = "RestoreBlack";
            this.toolTip1.SetToolTip(this.RestoreBlack, resources.GetString("RestoreBlack.ToolTip"));
            this.RestoreBlack.UseVisualStyleBackColor = true;
            // 
            // CloseBlack
            // 
            resources.ApplyResources(this.CloseBlack, "CloseBlack");
            this.CloseBlack.Name = "CloseBlack";
            this.toolTip1.SetToolTip(this.CloseBlack, resources.GetString("CloseBlack.ToolTip"));
            this.CloseBlack.UseVisualStyleBackColor = true;
            // 
            // MinimizeBlackHover
            // 
            resources.ApplyResources(this.MinimizeBlackHover, "MinimizeBlackHover");
            this.MinimizeBlackHover.Name = "MinimizeBlackHover";
            this.toolTip1.SetToolTip(this.MinimizeBlackHover, resources.GetString("MinimizeBlackHover.ToolTip"));
            this.MinimizeBlackHover.UseVisualStyleBackColor = true;
            // 
            // RestoreBlackHover
            // 
            resources.ApplyResources(this.RestoreBlackHover, "RestoreBlackHover");
            this.RestoreBlackHover.Name = "RestoreBlackHover";
            this.toolTip1.SetToolTip(this.RestoreBlackHover, resources.GetString("RestoreBlackHover.ToolTip"));
            this.RestoreBlackHover.UseVisualStyleBackColor = true;
            // 
            // CloseBlackHover
            // 
            resources.ApplyResources(this.CloseBlackHover, "CloseBlackHover");
            this.CloseBlackHover.Name = "CloseBlackHover";
            this.toolTip1.SetToolTip(this.CloseBlackHover, resources.GetString("CloseBlackHover.ToolTip"));
            this.CloseBlackHover.UseVisualStyleBackColor = true;
            // 
            // MinimizeBlackPressed
            // 
            resources.ApplyResources(this.MinimizeBlackPressed, "MinimizeBlackPressed");
            this.MinimizeBlackPressed.Name = "MinimizeBlackPressed";
            this.toolTip1.SetToolTip(this.MinimizeBlackPressed, resources.GetString("MinimizeBlackPressed.ToolTip"));
            this.MinimizeBlackPressed.UseVisualStyleBackColor = true;
            // 
            // RestoreBlackPressed
            // 
            resources.ApplyResources(this.RestoreBlackPressed, "RestoreBlackPressed");
            this.RestoreBlackPressed.Name = "RestoreBlackPressed";
            this.toolTip1.SetToolTip(this.RestoreBlackPressed, resources.GetString("RestoreBlackPressed.ToolTip"));
            this.RestoreBlackPressed.UseVisualStyleBackColor = true;
            // 
            // CloseBlackPressed
            // 
            resources.ApplyResources(this.CloseBlackPressed, "CloseBlackPressed");
            this.CloseBlackPressed.Name = "CloseBlackPressed";
            this.toolTip1.SetToolTip(this.CloseBlackPressed, resources.GetString("CloseBlackPressed.ToolTip"));
            this.CloseBlackPressed.UseVisualStyleBackColor = true;
            // 
            // Title
            // 
            resources.ApplyResources(this.Title, "Title");
            this.Title.BackColor = System.Drawing.Color.Black;
            this.Title.ForeColor = System.Drawing.Color.White;
            this.Title.Name = "Title";
            this.toolTip1.SetToolTip(this.Title, resources.GetString("Title.ToolTip"));
            // 
            // MinimizeLightPressed
            // 
            resources.ApplyResources(this.MinimizeLightPressed, "MinimizeLightPressed");
            this.MinimizeLightPressed.Name = "MinimizeLightPressed";
            this.toolTip1.SetToolTip(this.MinimizeLightPressed, resources.GetString("MinimizeLightPressed.ToolTip"));
            this.MinimizeLightPressed.UseVisualStyleBackColor = true;
            // 
            // RestoreLightPressed
            // 
            resources.ApplyResources(this.RestoreLightPressed, "RestoreLightPressed");
            this.RestoreLightPressed.Name = "RestoreLightPressed";
            this.toolTip1.SetToolTip(this.RestoreLightPressed, resources.GetString("RestoreLightPressed.ToolTip"));
            this.RestoreLightPressed.UseVisualStyleBackColor = true;
            // 
            // CloseLightPressed
            // 
            resources.ApplyResources(this.CloseLightPressed, "CloseLightPressed");
            this.CloseLightPressed.Name = "CloseLightPressed";
            this.toolTip1.SetToolTip(this.CloseLightPressed, resources.GetString("CloseLightPressed.ToolTip"));
            this.CloseLightPressed.UseVisualStyleBackColor = true;
            // 
            // MinimizeLightHover
            // 
            resources.ApplyResources(this.MinimizeLightHover, "MinimizeLightHover");
            this.MinimizeLightHover.Name = "MinimizeLightHover";
            this.toolTip1.SetToolTip(this.MinimizeLightHover, resources.GetString("MinimizeLightHover.ToolTip"));
            this.MinimizeLightHover.UseVisualStyleBackColor = true;
            // 
            // RestoreLightHover
            // 
            resources.ApplyResources(this.RestoreLightHover, "RestoreLightHover");
            this.RestoreLightHover.Name = "RestoreLightHover";
            this.toolTip1.SetToolTip(this.RestoreLightHover, resources.GetString("RestoreLightHover.ToolTip"));
            this.RestoreLightHover.UseVisualStyleBackColor = true;
            // 
            // CloseLightHover
            // 
            resources.ApplyResources(this.CloseLightHover, "CloseLightHover");
            this.CloseLightHover.Name = "CloseLightHover";
            this.toolTip1.SetToolTip(this.CloseLightHover, resources.GetString("CloseLightHover.ToolTip"));
            this.CloseLightHover.UseVisualStyleBackColor = true;
            // 
            // MinimizeLight
            // 
            resources.ApplyResources(this.MinimizeLight, "MinimizeLight");
            this.MinimizeLight.Name = "MinimizeLight";
            this.toolTip1.SetToolTip(this.MinimizeLight, resources.GetString("MinimizeLight.ToolTip"));
            this.MinimizeLight.UseVisualStyleBackColor = true;
            // 
            // RestoreLight
            // 
            resources.ApplyResources(this.RestoreLight, "RestoreLight");
            this.RestoreLight.Name = "RestoreLight";
            this.toolTip1.SetToolTip(this.RestoreLight, resources.GetString("RestoreLight.ToolTip"));
            this.RestoreLight.UseVisualStyleBackColor = true;
            // 
            // CloseLight
            // 
            resources.ApplyResources(this.CloseLight, "CloseLight");
            this.CloseLight.Name = "CloseLight";
            this.toolTip1.SetToolTip(this.CloseLight, resources.GetString("CloseLight.ToolTip"));
            this.CloseLight.UseVisualStyleBackColor = true;
            // 
            // MaximizeLightPressed
            // 
            resources.ApplyResources(this.MaximizeLightPressed, "MaximizeLightPressed");
            this.MaximizeLightPressed.Name = "MaximizeLightPressed";
            this.toolTip1.SetToolTip(this.MaximizeLightPressed, resources.GetString("MaximizeLightPressed.ToolTip"));
            this.MaximizeLightPressed.UseVisualStyleBackColor = true;
            // 
            // MaximizeLightHover
            // 
            resources.ApplyResources(this.MaximizeLightHover, "MaximizeLightHover");
            this.MaximizeLightHover.Name = "MaximizeLightHover";
            this.toolTip1.SetToolTip(this.MaximizeLightHover, resources.GetString("MaximizeLightHover.ToolTip"));
            this.MaximizeLightHover.UseVisualStyleBackColor = true;
            // 
            // MaximizeLight
            // 
            resources.ApplyResources(this.MaximizeLight, "MaximizeLight");
            this.MaximizeLight.Name = "MaximizeLight";
            this.toolTip1.SetToolTip(this.MaximizeLight, resources.GetString("MaximizeLight.ToolTip"));
            this.MaximizeLight.UseVisualStyleBackColor = true;
            // 
            // MaximizeBlackPressed
            // 
            resources.ApplyResources(this.MaximizeBlackPressed, "MaximizeBlackPressed");
            this.MaximizeBlackPressed.Name = "MaximizeBlackPressed";
            this.toolTip1.SetToolTip(this.MaximizeBlackPressed, resources.GetString("MaximizeBlackPressed.ToolTip"));
            this.MaximizeBlackPressed.UseVisualStyleBackColor = true;
            // 
            // MaximizeBlackHover
            // 
            resources.ApplyResources(this.MaximizeBlackHover, "MaximizeBlackHover");
            this.MaximizeBlackHover.Name = "MaximizeBlackHover";
            this.toolTip1.SetToolTip(this.MaximizeBlackHover, resources.GetString("MaximizeBlackHover.ToolTip"));
            this.MaximizeBlackHover.UseVisualStyleBackColor = true;
            // 
            // MaximizeBlack
            // 
            resources.ApplyResources(this.MaximizeBlack, "MaximizeBlack");
            this.MaximizeBlack.Name = "MaximizeBlack";
            this.toolTip1.SetToolTip(this.MaximizeBlack, resources.GetString("MaximizeBlack.ToolTip"));
            this.MaximizeBlack.UseVisualStyleBackColor = true;
            // 
            // customizableToolStrip1
            // 
            resources.ApplyResources(this.customizableToolStrip1, "customizableToolStrip1");
            this.customizableToolStrip1.Appearance = this.blackApparance1;
            this.customizableToolStrip1.BackColor = System.Drawing.Color.Black;
            this.customizableToolStrip1.ContextMenuStrip = this.contextMenuStrip1;
            this.customizableToolStrip1.DefaultButton = true;
            this.customizableToolStrip1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.customizableToolStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.customizableToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.customizableToolStrip1.ImageSizeSelection = false;
            this.customizableToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton51,
            this.toolStripButton52,
            this.toolStripButton50,
            this.toolStripButton30});
            this.customizableToolStrip1.LargeIcons = false;
            this.customizableToolStrip1.LargeImageList = this.imageList1;
            this.customizableToolStrip1.LargeImageScalingSize = new System.Drawing.Size(24, 24);
            this.customizableToolStrip1.ListViewDisplayStyle = ToolStripCustomCtrls.ListViewDisplayStyle.Tiles;
            this.customizableToolStrip1.Name = "customizableToolStrip1";
            this.customizableToolStrip1.RoundedEdges = false;
            this.customizableToolStrip1.SmallImageScalingSize = new System.Drawing.Size(16, 16);
            this.toolTip1.SetToolTip(this.customizableToolStrip1, resources.GetString("customizableToolStrip1.ToolTip"));
            this.customizableToolStrip1.UserData = ((ToolStripCustomCtrls.ToolStripData)(resources.GetObject("customizableToolStrip1.UserData")));
            // 
            // toolStripButton51
            // 
            resources.ApplyResources(this.toolStripButton51, "toolStripButton51");
            this.toolStripButton51.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton51.Name = "toolStripButton51";
            this.toolStripButton51.Click += new System.EventHandler(this.toolStripButton51_Click);
            // 
            // toolStripButton52
            // 
            resources.ApplyResources(this.toolStripButton52, "toolStripButton52");
            this.toolStripButton52.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton52.Name = "toolStripButton52";
            this.toolStripButton52.Click += new System.EventHandler(this.toolStripButton52_Click);
            // 
            // toolStripButton50
            // 
            resources.ApplyResources(this.toolStripButton50, "toolStripButton50");
            this.toolStripButton50.Name = "toolStripButton50";
            // 
            // toolStripButton30
            // 
            resources.ApplyResources(this.toolStripButton30, "toolStripButton30");
            this.toolStripButton30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toolStripButton30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripButton30.ForeColor = System.Drawing.Color.White;
            this.toolStripButton30.Margin = new System.Windows.Forms.Padding(1, 2, 1, 0);
            this.toolStripButton30.Name = "toolStripButton30";
            this.toolStripButton30.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripButton30_KeyPress);
            this.toolStripButton30.Paint += new System.Windows.Forms.PaintEventHandler(this.toolStripButton30_Paint);
            // 
            // whiteApparence1
            // 
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.Background = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.intBackground = -4144960;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.intBorderHighlight = -4144960;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientBegin = -4144960;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientEnd = -4144960;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientMiddle = -4144960;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.intHighlight = -3878683;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.intPressedBackground = -98242;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.CheckedAppearance.intSelectedBackground = -98242;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.PressedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.PressedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(128)))), ((int)(((byte)(62)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.PressedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(128)))), ((int)(((byte)(62)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.PressedAppearance.intBorder = -16777088;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.PressedAppearance.intBorderHighlight = -13410648;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientBegin = -98242;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientEnd = -98242;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientMiddle = -98242;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.PressedAppearance.intHighlight = -6771246;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(105)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(105)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(244)))), ((int)(((byte)(200)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(220)))), ((int)(((byte)(125)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(233)))), ((int)(((byte)(165)))));
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorder = -17047;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorderHighlight = -17047;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientBegin = -2872;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientEnd = -9091;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientMiddle = -5723;
            this.whiteApparence1.CustomAppearance.ButtonAppearance.SelectedAppearance.intHighlight = -3878683;
            this.whiteApparence1.CustomAppearance.GripAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.whiteApparence1.CustomAppearance.GripAppearance.intDark = -2236963;
            this.whiteApparence1.CustomAppearance.GripAppearance.intLight = -6633729;
            this.whiteApparence1.CustomAppearance.GripAppearance.Light = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(198)))), ((int)(((byte)(255)))));
            this.whiteApparence1.CustomAppearance.ImageMarginAppearance.Normal.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.whiteApparence1.CustomAppearance.ImageMarginAppearance.Normal.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.whiteApparence1.CustomAppearance.ImageMarginAppearance.Normal.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.whiteApparence1.CustomAppearance.ImageMarginAppearance.Normal.intGradientBegin = -1;
            this.whiteApparence1.CustomAppearance.ImageMarginAppearance.Normal.intGradientEnd = -1;
            this.whiteApparence1.CustomAppearance.ImageMarginAppearance.Normal.intGradientMiddle = -1;
            this.whiteApparence1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientBegin = -3416586;
            this.whiteApparence1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientEnd = -9266217;
            this.whiteApparence1.CustomAppearance.ImageMarginAppearance.Revealed.intGradientMiddle = -6175239;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.intBorder = -2039584;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.intPressedGradientBegin = -1;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.intPressedGradientEnd = -1;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.intPressedGradientMiddle = -1;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.intSelected = -2039584;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.intSelectedGradientBegin = -1;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.intSelectedGradientEnd = -1;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.PressedGradientBegin = System.Drawing.Color.White;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.PressedGradientEnd = System.Drawing.Color.White;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.PressedGradientMiddle = System.Drawing.Color.White;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.Selected = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.SelectedGradientBegin = System.Drawing.Color.White;
            this.whiteApparence1.CustomAppearance.MenuItemAppearance.SelectedGradientEnd = System.Drawing.Color.White;
            this.whiteApparence1.CustomAppearance.MenuStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.whiteApparence1.CustomAppearance.MenuStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.whiteApparence1.CustomAppearance.MenuStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.whiteApparence1.CustomAppearance.MenuStripAppearance.intBorder = -16777216;
            this.whiteApparence1.CustomAppearance.MenuStripAppearance.intGradientBegin = -2236963;
            this.whiteApparence1.CustomAppearance.MenuStripAppearance.intGradientEnd = -2236963;
            this.whiteApparence1.CustomAppearance.OverflowButtonAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.whiteApparence1.CustomAppearance.OverflowButtonAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.whiteApparence1.CustomAppearance.OverflowButtonAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.whiteApparence1.CustomAppearance.OverflowButtonAppearance.intGradientBegin = -16777216;
            this.whiteApparence1.CustomAppearance.OverflowButtonAppearance.intGradientEnd = -16777216;
            this.whiteApparence1.CustomAppearance.OverflowButtonAppearance.intGradientMiddle = -16777216;
            this.whiteApparence1.CustomAppearance.RaftingContainerAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.whiteApparence1.CustomAppearance.RaftingContainerAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.whiteApparence1.CustomAppearance.RaftingContainerAppearance.intGradientBegin = -16777216;
            this.whiteApparence1.CustomAppearance.RaftingContainerAppearance.intGradientEnd = -16777216;
            this.whiteApparence1.CustomAppearance.SeparatorAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.whiteApparence1.CustomAppearance.SeparatorAppearance.intDark = -2236963;
            this.whiteApparence1.CustomAppearance.SeparatorAppearance.intLight = -6633729;
            this.whiteApparence1.CustomAppearance.SeparatorAppearance.Light = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(198)))), ((int)(((byte)(255)))));
            this.whiteApparence1.CustomAppearance.StatusStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.whiteApparence1.CustomAppearance.StatusStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.whiteApparence1.CustomAppearance.StatusStripAppearance.intGradientBegin = -16777216;
            this.whiteApparence1.CustomAppearance.StatusStripAppearance.intGradientEnd = -16777216;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.ContentPanelGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.ContentPanelGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.DropDownBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.intBorder = -2696215;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.intContentPanelGradientBegin = -1;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.intContentPanelGradientEnd = -1;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.intDropDownBackground = -1;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.intGradientBegin = -2236963;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.intGradientEnd = -2236963;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.intGradientMiddle = -2236963;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.intPanelGradientBegin = -1;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.intPanelGradientEnd = -1;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.PanelGradientBegin = System.Drawing.Color.White;
            this.whiteApparence1.CustomAppearance.ToolStripAppearance.PanelGradientEnd = System.Drawing.Color.White;
            this.whiteApparence1.Preset = AppearanceControl.enumPresetStyles.Custom;
            this.whiteApparence1.Renderer.RoundedEdges = false;
            // 
            // appearanceControl2
            // 
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intBackground = -16273;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intBorderHighlight = -13410648;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientBegin = -8294;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientEnd = -22964;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intGradientMiddle = -15500;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intHighlight = -3878683;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intPressedBackground = -98242;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.CheckedAppearance.intSelectedBackground = -98242;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intBorder = -16777088;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intBorderHighlight = -13410648;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientBegin = -98242;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientEnd = -8294;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intGradientMiddle = -20115;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.PressedAppearance.intHighlight = -6771246;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.BorderHighlight = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorder = -16777088;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intBorderHighlight = -16777088;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientBegin = -34;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientEnd = -13432;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intGradientMiddle = -7764;
            this.appearanceControl2.CustomAppearance.ButtonAppearance.SelectedAppearance.intHighlight = -3878683;
            this.appearanceControl2.CustomAppearance.GripAppearance.intDark = -14204554;
            this.appearanceControl2.CustomAppearance.GripAppearance.intLight = -1;
            this.appearanceControl2.CustomAppearance.GripAppearance.Light = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(254)))));
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(254)))));
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.GradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(254)))));
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.intGradientBegin = -854786;
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.intGradientEnd = -854786;
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Normal.intGradientMiddle = -854786;
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Revealed.intGradientBegin = -3416586;
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Revealed.intGradientEnd = -9266217;
            this.appearanceControl2.CustomAppearance.ImageMarginAppearance.Revealed.intGradientMiddle = -6175239;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(195)))), ((int)(((byte)(101)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intBorder = -1719451;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intPressedGradientBegin = -1380097;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intPressedGradientEnd = -1380097;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intPressedGradientMiddle = -1380097;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intSelected = -133953;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intSelectedGradientBegin = -133953;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.intSelectedGradientEnd = -133953;
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.PressedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.PressedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.PressedGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.Selected = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(244)))), ((int)(((byte)(191)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.SelectedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(244)))), ((int)(((byte)(191)))));
            this.appearanceControl2.CustomAppearance.MenuItemAppearance.SelectedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(244)))), ((int)(((byte)(191)))));
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.Border = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.GradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.GradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(219)))), ((int)(((byte)(233)))));
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.intBorder = -2696215;
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.intGradientBegin = -2696215;
            this.appearanceControl2.CustomAppearance.MenuStripAppearance.intGradientEnd = -2696215;
            this.appearanceControl2.CustomAppearance.OverflowButtonAppearance.intGradientBegin = -8408582;
            this.appearanceControl2.CustomAppearance.OverflowButtonAppearance.intGradientEnd = -16763503;
            this.appearanceControl2.CustomAppearance.OverflowButtonAppearance.intGradientMiddle = -11370544;
            this.appearanceControl2.CustomAppearance.RaftingContainerAppearance.intGradientBegin = -6373643;
            this.appearanceControl2.CustomAppearance.RaftingContainerAppearance.intGradientEnd = -3876102;
            this.appearanceControl2.CustomAppearance.SeparatorAppearance.Dark = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(195)))), ((int)(((byte)(203)))));
            this.appearanceControl2.CustomAppearance.SeparatorAppearance.intDark = -4275253;
            this.appearanceControl2.CustomAppearance.SeparatorAppearance.intLight = -919041;
            this.appearanceControl2.CustomAppearance.StatusStripAppearance.intGradientBegin = -6373643;
            this.appearanceControl2.CustomAppearance.StatusStripAppearance.intGradientEnd = -3876102;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intBorder = -12885604;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intContentPanelGradientBegin = -6373643;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intContentPanelGradientEnd = -3876102;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intDropDownBackground = -592138;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intGradientBegin = -1839105;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intGradientEnd = -8674080;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intGradientMiddle = -3415556;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intPanelGradientBegin = -6373643;
            this.appearanceControl2.CustomAppearance.ToolStripAppearance.intPanelGradientEnd = -3876102;
            this.appearanceControl2.Preset = AppearanceControl.enumPresetStyles.Custom;
            this.appearanceControl2.Renderer.RoundedEdges = true;
            // 
            // UpdateChecker
            // 
            this.UpdateChecker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.UpdateChecker_DoWork);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // startupTimer
            // 
            this.startupTimer.Enabled = true;
            this.startupTimer.Interval = 1;
            this.startupTimer.Tick += new System.EventHandler(this.startupTimer_Tick);
            // 
            // userTimer
            // 
            this.userTimer.Interval = 1;
            this.userTimer.Tick += new System.EventHandler(this.userTimer_Tick);
            // 
            // Loop
            // 
            this.Loop.Enabled = true;
            this.Loop.Interval = 1000;
            this.Loop.Tick += new System.EventHandler(this.Loop_Tick);
            // 
            // folderBrowserDialog1
            // 
            resources.ApplyResources(this.folderBrowserDialog1, "folderBrowserDialog1");
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // signInNow
            // 
            this.signInNow.Interval = 1000;
            this.signInNow.Tick += new System.EventHandler(this.signInNow_Tick);
            // 
            // ShowCodeboardTimer
            // 
            this.ShowCodeboardTimer.Enabled = true;
            this.ShowCodeboardTimer.Interval = 1;
            this.ShowCodeboardTimer.Tick += new System.EventHandler(this.ShowCodeboardTimer_Tick);
            // 
            // resizeWorker
            // 
            this.resizeWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.resizeWorker_DoWork);
            // 
            // showSystemMenuTimer
            // 
            this.showSystemMenuTimer.Interval = 10;
            this.showSystemMenuTimer.Tick += new System.EventHandler(this.showSystemMenuTimer_Tick);
            // 
            // XPResizeTimer
            // 
            this.XPResizeTimer.Interval = 1;
            this.XPResizeTimer.Tick += new System.EventHandler(this.XPResizeTimer_Tick);
            // 
            // unTop
            // 
            this.unTop.Interval = 1;
            this.unTop.Tick += new System.EventHandler(this.unTop_Tick);
            // 
            // BorderLoop
            // 
            this.BorderLoop.Enabled = true;
            this.BorderLoop.Interval = 1;
            this.BorderLoop.Tick += new System.EventHandler(this.BorderLoop_Tick);
            // 
            // WrapUp
            // 
            this.WrapUp.Interval = 1;
            this.WrapUp.Tick += new System.EventHandler(this.WrapUp_Tick);
            // 
            // colorTimer
            // 
            this.colorTimer.Enabled = true;
            this.colorTimer.Interval = 1000;
            this.colorTimer.Tick += new System.EventHandler(this.colorTimer_Tick);
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.customizableToolStrip1);
            this.Controls.Add(this.MaximizeLightPressed);
            this.Controls.Add(this.MaximizeLightHover);
            this.Controls.Add(this.MaximizeLight);
            this.Controls.Add(this.MaximizeBlackPressed);
            this.Controls.Add(this.MaximizeBlackHover);
            this.Controls.Add(this.MaximizeBlack);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.MinimizeLightPressed);
            this.Controls.Add(this.RestoreLightPressed);
            this.Controls.Add(this.CloseLightPressed);
            this.Controls.Add(this.MinimizeLightHover);
            this.Controls.Add(this.RestoreLightHover);
            this.Controls.Add(this.CloseLightHover);
            this.Controls.Add(this.MinimizeLight);
            this.Controls.Add(this.RestoreLight);
            this.Controls.Add(this.CloseLight);
            this.Controls.Add(this.MinimizeBlackPressed);
            this.Controls.Add(this.RestoreBlackPressed);
            this.Controls.Add(this.CloseBlackPressed);
            this.Controls.Add(this.MinimizeBlackHover);
            this.Controls.Add(this.RestoreBlackHover);
            this.Controls.Add(this.CloseBlackHover);
            this.Controls.Add(this.MinimizeBlack);
            this.Controls.Add(this.RestoreBlack);
            this.Controls.Add(this.CloseBlack);
            this.Controls.Add(this.Minimize);
            this.Controls.Add(this.Restore);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.Right);
            this.Controls.Add(this.Bottom);
            this.Controls.Add(this.imageDisplay);
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.UserPanel);
            this.Controls.Add(this.UserDropPanel);
            this.Controls.Add(this.SearchPanel);
            this.Controls.Add(this.BottomDown);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.TopCover);
            this.Controls.Add(this.Left);
            this.Controls.Add(this.Top);
            this.Controls.Add(this.dockPanel);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.RestoreWhite);
            this.Controls.Add(this.MaximizeWhite);
            this.Controls.Add(this.transparentControl1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.Deactivate += new System.EventHandler(this.Form1_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.LocationChanged += new System.EventHandler(this.MainForm_LocationChanged);
            this.TextChanged += new System.EventHandler(this.MainForm_TextChanged);
            this.Move += new System.EventHandler(this.MainForm_Move);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.SearchPanel.ResumeLayout(false);
            this.SearchPanel.PerformLayout();
            this.UserDropPanel.ResumeLayout(false);
            this.UserDropPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.UserPanel.ResumeLayout(false);
            this.UserPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDisplay)).EndInit();
            this.customizableToolStrip1.ResumeLayout(false);
            this.customizableToolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MainMenu mainMenu1;
        private TransparentControl transparentControl1;
        private CustomizableMenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveLogicSchemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openPseudocodeUsingAcodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharePseudocodeUsingAcodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem printPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printConvertedCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printLogicSchemeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem recoverLostunsavedFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem removePersonalInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem exitPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eDITToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoActionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem jumpToLineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jumpToBreakpointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jumpToBookmarkToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem vIEWToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem zoomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem11;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem statisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cONVERTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toVBScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem dEBUGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compileAndRunToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem justCompileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startDebuggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem continueDebuggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem watchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendCommandToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem addremoveBreakpointToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem endPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tOOLSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem insertModuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem25;
        private System.Windows.Forms.ToolStripMenuItem testCurrentConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extensionsManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrativeConfigurationsManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wINDOWToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem closeAllTabsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeCurrentTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem casacdeWindowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tileWindowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showWindowsSideBySideToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem restoreDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maximizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem29;
        private AppearanceControl appearanceControl2;
        private System.Windows.Forms.Button MaximizeWhite;
        private System.Windows.Forms.Button RestoreWhite;
        private System.Windows.Forms.ToolTip toolTip1;
        private CustomizableToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.ToolStripButton toolStripButton13;
        private System.Windows.Forms.ToolStripButton toolStripButton14;
        private System.Windows.Forms.ToolStripButton toolStripButton15;
        private System.Windows.Forms.ToolStripButton toolStripButton16;
        private System.Windows.Forms.ToolStripButton toolStripButton19;
        private System.Windows.Forms.ToolStripButton toolStripButton20;
        private System.Windows.Forms.ToolStripButton toolStripButton21;
        private System.Windows.Forms.ToolStripButton toolStripButton22;
        private System.Windows.Forms.ToolStripButton toolStripButton23;
        private System.Windows.Forms.ToolStripButton toolStripButton24;
        private System.Windows.Forms.ToolStripButton toolStripButton25;
        private System.Windows.Forms.ToolStripButton toolStripButton26;
        private System.Windows.Forms.ToolStripButton toolStripButton27;
        private System.Windows.Forms.ToolStripButton toolStripButton28;
        private System.Windows.Forms.ToolStripButton toolStripButton29;
        private System.Windows.Forms.ToolStripButton toolStripButton31;
        private System.Windows.Forms.ToolStripButton toolStripButton33;
        private System.Windows.Forms.ToolStripButton toolStripButton34;
        private System.Windows.Forms.ToolStripButton toolStripButton35;
        private System.Windows.Forms.ToolStripButton toolStripButton36;
        private System.Windows.Forms.ToolStripButton toolStripButton37;
        private System.Windows.Forms.ToolStripButton toolStripButton38;
        private System.Windows.Forms.ToolStripButton toolStripButton39;
        private System.Windows.Forms.ToolStripButton toolStripButton40;
        private System.Windows.Forms.ToolStripButton toolStripButton41;
        private System.Windows.Forms.ToolStripButton toolStripButton42;
        private System.Windows.Forms.ToolStripButton toolStripButton43;
        private System.Windows.Forms.ToolStripButton toolStripButton44;
        private System.Windows.Forms.ToolStripMenuItem toggleBookmarkToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton45;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem33;
        private System.Windows.Forms.ToolStripMenuItem customizeToolbarToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel Top;
        private System.Windows.Forms.Panel Left;
        private System.Windows.Forms.Panel Right;
        private System.Windows.Forms.ToolStripMenuItem renameVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addremoveWatchToolStripMenuItem;
        private System.Windows.Forms.Panel TopCover;
        private System.Windows.Forms.ToolStripMenuItem hELPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpSupportAndTipsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem30;
        private System.Windows.Forms.ToolStripMenuItem contextualHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem whatsThisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlineAssistanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem valiNetOnTheInternetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem31;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem valiNetCustomerExperienceImprovementProgramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem licenseAgreementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderPseudocodeOnADVDToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem32;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private CustomizableStatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Panel BottomDown;
        private Controls.Development.ImageListBox listBox1;
        private System.Windows.Forms.Panel SearchPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.LinkLabel SearchText;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem advancedCommandsPanelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker UpdateChecker;
        private SelectablePanel UserDropPanel;
        public System.Windows.Forms.Button button47;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Timer userTimer;
        public System.Windows.Forms.TextBox SearchBox;
        private System.Windows.Forms.ToolStripMenuItem toBasicClassicScriptFilevbsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCSourceFilecppToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCSourceFilecsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toPascalSourceFilepasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toJavaScriptpoweredWebPagehtmToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton17;
        private System.Windows.Forms.ToolStripButton toolStripButton18;
        private System.Windows.Forms.ToolStripMenuItem copyCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feedbackToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem newPseudocodeToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem openPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addremoveBreakpointsOnLinesToolStripMenuItem;
        private System.Windows.Forms.PictureBox imageDisplay;
        private System.Windows.Forms.ToolStripMenuItem commandBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertedCodeLogicSchemaToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem toCToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem toBasicClassicToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem toPascalToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem toCToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem toJavaScriptToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem toLogicSchemeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visualScrollBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickMenuSearchBoxToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton12;
        private System.Windows.Forms.ToolStripMenuItem uploadPseudocodeToolStripMenuItem;
        public System.Windows.Forms.Panel UserPanel;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ceEsteAceastaToolStripMenuItem;
        public System.Windows.Forms.Panel Bottom;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem12;
        public System.Windows.Forms.ToolStripMenuItem fromEnglishCodeToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem fromRomanianPseudoccodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem englishToRomanianPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem romanianToEnglishPseudocodeToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        public System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer signInNow;
        public System.Windows.Forms.Timer Loop;
        private System.Windows.Forms.ToolStripMenuItem limbaDeScriereToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem translatePseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem analizeExecutedLinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearCoverageDataToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem userdefinedToolStripMenuItem;
        public WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
        public System.Windows.Forms.ToolStripMenuItem closeFileToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem uploadProblemToInfoarenaToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem createApplicationToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem findToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem replaceToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem codeboardToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem zoom300;
        public System.Windows.Forms.ToolStripMenuItem zoom200;
        public System.Windows.Forms.ToolStripMenuItem zoom100;
        public System.Windows.Forms.ToolStripMenuItem zoom75;
        public System.Windows.Forms.ToolStripMenuItem zoom50;
        public System.Windows.Forms.ToolStripMenuItem zoom25;
        public System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem zoom400;
        private System.Windows.Forms.Button Restore;
        private System.Windows.Forms.Button Minimize;
        private System.Windows.Forms.Button MinimizeBlack;
        private System.Windows.Forms.Button RestoreBlack;
        private System.Windows.Forms.Button CloseBlack;
        private System.Windows.Forms.Button MinimizeBlackHover;
        private System.Windows.Forms.Button RestoreBlackHover;
        private System.Windows.Forms.Button CloseBlackHover;
        private System.Windows.Forms.Button MinimizeBlackPressed;
        private System.Windows.Forms.Button RestoreBlackPressed;
        private System.Windows.Forms.Button CloseBlackPressed;
        private System.Windows.Forms.ToolStripMenuItem menuIcon;
        private PowerLabel Title;
        private System.Windows.Forms.Button MinimizeLightPressed;
        private System.Windows.Forms.Button RestoreLightPressed;
        private System.Windows.Forms.Button CloseLightPressed;
        private System.Windows.Forms.Button MinimizeLightHover;
        private System.Windows.Forms.Button RestoreLightHover;
        private System.Windows.Forms.Button CloseLightHover;
        private System.Windows.Forms.Button MinimizeLight;
        private System.Windows.Forms.Button RestoreLight;
        private System.Windows.Forms.Button CloseLight;
        public System.Windows.Forms.Button Close;
        private System.ComponentModel.BackgroundWorker resizeWorker;
        private System.Windows.Forms.Timer showSystemMenuTimer;
        private System.Windows.Forms.Timer XPResizeTimer;
        public System.Windows.Forms.Timer startupTimer;
        public System.Windows.Forms.Timer ShowCodeboardTimer;
        private System.Windows.Forms.Timer unTop;
        private System.Windows.Forms.Button MaximizeLightPressed;
        private System.Windows.Forms.Button MaximizeLightHover;
        public System.Windows.Forms.Button MaximizeLight;
        private System.Windows.Forms.Button MaximizeBlackPressed;
        private System.Windows.Forms.Button MaximizeBlackHover;
        public System.Windows.Forms.Button MaximizeBlack;
        public System.Windows.Forms.ToolStripMenuItem signInToolStripMenuItem;
        private System.Windows.Forms.Timer BorderLoop;
        private System.Windows.Forms.Timer WrapUp;
        private System.Windows.Forms.Timer colorTimer;
        private System.Windows.Forms.ToolStripMenuItem fILEToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem newBrowserWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem duplicateBrowserWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePageAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closePageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem pageSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem sendPageByEmailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createShortcutToDesktopToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem quitPseudocodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eDITToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem34;
        private System.Windows.Forms.ToolStripMenuItem findonThisPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vIEWToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem navigateBackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem navigateForwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openHomePageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopLoadingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem35;
        private System.Windows.Forms.ToolStripMenuItem zoomToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem textSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem36;
        private System.Windows.Forms.ToolStripMenuItem sourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fAVORITESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToFavoritesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem organizeFavoritesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem37;
        private System.Windows.Forms.ToolStripMenuItem hELPToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpSupportAndTipsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem39;
        private System.Windows.Forms.ToolStripMenuItem feedbackToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem40;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem licenseAgreementToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem41;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem newBrowserWindowToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem smallestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smallToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem defaultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem largeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem largestToolStripMenuItem;
        private CustomizableToolStrip customizableToolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton51;
        private System.Windows.Forms.ToolStripButton toolStripButton52;
        private System.Windows.Forms.ToolStripSeparator toolStripButton50;
        public ToolStripSpringTextBox toolStripButton30;
        private System.Windows.Forms.ToolStripMenuItem zoomInToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem zoomOutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem42;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem44;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem45;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem46;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem47;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem48;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem49;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem50;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem51;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem52;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem53;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem43;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem browserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem valiNetWebSiteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton toolStripButton32;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem54;
        private System.Windows.Forms.ToolStripMenuItem manageCustomCompilersToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem runUsingLocalDebuggerToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem runUsingBuiltinBrowserToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem55;
        private System.Windows.Forms.ToolStripMenuItem manageCustomCompilersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tOOLSToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem menuSearchToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem38;
        private System.Windows.Forms.ToolStripMenuItem manageCustomCompilersToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem packageManagerToolStripMenuItem;
        public AppearanceControl blackApparance1;
        public AppearanceControl whiteApparence1;
    }
}

