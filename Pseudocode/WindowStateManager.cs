//====================================================
//| Downloaded From                                  |
//| Visual C# Kicks - http://www.vcskicks.com/       |
//| License - http://www.vcskicks.com/license.html   |
//====================================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Pseudocode
{
    static class WindowStateManager
    {
        [DllImport("user32.dll")]
        static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        [DllImport("user32.dll")]
        public static extern int ShowWindow(IntPtr hwnd, int nCmdShow);

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hWnd);	

        private struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public int showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;
        }

        private enum MainWindowState
        {
            Normal = 1,
            Minimized,
            Maximized
        }
        [DllImport("user32.dll")]
        static extern Int32 EnumWindows(EnumWindowsCallback lpEnumFunc, Int32 lParam);
        public delegate Boolean EnumWindowsCallback(IntPtr hwnd, Int32 lParam);
        private static List<IntPtr> NormalWindows;
        private static List<IntPtr> MaximizedWindows;
        public static readonly Int32 GWL_STYLE = -16;
        public static readonly UInt64 WS_VISIBLE = 0x10000000L;
        public static readonly UInt64 WS_BORDER = 0x00800000L;
        public static readonly UInt64 DESIRED_WS = WS_BORDER | WS_VISIBLE;

        [DllImport("user32.dll")]
        static extern UInt64 GetWindowLongA(IntPtr hWnd, Int32 nIndex);
        public static void MinimizeAll(IntPtr exclude, IntPtr exclude2, IntPtr exclude3)
        {
            NormalWindows = new List<IntPtr>();
            MaximizedWindows = new List<IntPtr>();
            EnumWindows(delegate(IntPtr hwnd, Int32 lParam)
        {
            if ((GetWindowLongA(hwnd, GWL_STYLE) & DESIRED_WS) == DESIRED_WS)
            {
                WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
                placement.length = Marshal.SizeOf(placement);
                GetWindowPlacement(hwnd, ref placement);
                MainWindowState state = (MainWindowState)placement.showCmd;
                if (state != MainWindowState.Minimized)
                {
                    //Store what the previous state of the window was
                    if (state == MainWindowState.Normal)
                        NormalWindows.Add(hwnd);
                    else
                        MaximizedWindows.Add(hwnd);

                    //Minimize it
                    ShowWindow(hwnd, (int)MainWindowState.Minimized);
                }
            }
            return true;
        }, 0);
            return;
            foreach (Process prc in Process.GetProcesses())
            {
                if (prc.MainWindowTitle != string.Empty &&
                    prc.MainWindowHandle != exclude &&
                    prc.MainWindowHandle != exclude2 &&
                    prc.MainWindowHandle != exclude3) //window to exclude
                {
                    //Get the window's current state
                    WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
                    placement.length = Marshal.SizeOf(placement);
                    GetWindowPlacement(prc.MainWindowHandle, ref placement);
                    MainWindowState state = (MainWindowState)placement.showCmd;

                    if (state != MainWindowState.Minimized)
                    {
                        //Store what the previous state of the window was
                        if (state == MainWindowState.Normal)
                            NormalWindows.Add(prc.MainWindowHandle);
                        else
                            MaximizedWindows.Add(prc.MainWindowHandle);

                        //Minimize it
                        ShowWindow(prc.MainWindowHandle, (int)MainWindowState.Minimized);
                    }
                }
            }
        }

        public static void MaximizeAll(IntPtr topWindow)
        {
            if (MaximizedWindows != null)
            {
                //Restore previously maximized windows
                foreach (IntPtr handle in MaximizedWindows)
                {
                    ShowWindow(handle, (int)MainWindowState.Maximized);
                }
            }

            if (NormalWindows != null)
            {
                //Restore previously normal windows
                foreach (IntPtr handle in NormalWindows)
                {
                    ShowWindow(handle, (int)MainWindowState.Normal);
                }
            }

            SetForegroundWindow(topWindow);
        }

    }
}
