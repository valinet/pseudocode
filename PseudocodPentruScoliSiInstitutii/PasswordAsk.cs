﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace PseudocodePentruScoliSiInstitutii
{
    public partial class PasswordAsk : Form
    {
        public PasswordAsk()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!File.Exists(Application.StartupPath + "\\encryptedPass.pass"))
            {
                if (textBox1.Text == "0000")
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Parola introdusă este greșită. Vă rugăm reîncercați.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                StreamReader sr = new StreamReader(Application.StartupPath + "\\encryptedPass.pass");
                string pass = sr.ReadToEnd();
                sr.Close();
                if (textBox1.Text != "")
                {
                    if (EncryptDecrypt.Crypto.EncryptString(textBox1.Text) == pass)
                    {
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Parola introdusă este greșită. Vă rugăm reîncercați.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Parola nu poate fi goală.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PasswordAsk_Load(object sender, EventArgs e)
        {
            textBox1.Focus();
        }
    }
}
