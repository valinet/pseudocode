﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace PseudocodePentruScoliSiInstitutii
{
    public partial class NewPassword : Form
    {
        public NewPassword()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                StreamReader sr = new StreamReader(Application.StartupPath + "\\encryptedPass.pass");
                string pass = sr.ReadToEnd();
                sr.Close();
                if (pass == EncryptDecrypt.Crypto.EncryptString(textBox1.Text))
                {
                    if (textBox2.Text == textBox3.Text)
                    {
                        StreamWriter sw = new StreamWriter(Application.StartupPath + "\\encryptedPass.pass");
                        sw.Write(EncryptDecrypt.Crypto.EncryptString(textBox2.Text));
                        sw.Close();
                        MessageBox.Show("Noua parolă a fost memorată cu succes.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Parola confirmată nu se potrivește cu noua parolă. Verificați câmpurile și încercați din nou.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Parola curentă introdusă de dvs. este greșită. Vă rugăm reintroduceți-o și reîncercați.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            catch
            {
                StreamWriter sw = new StreamWriter(Application.StartupPath + "\\encryptedPass.pass");
                sw.Write(EncryptDecrypt.Crypto.EncryptString(textBox2.Text));
                sw.Close();
                MessageBox.Show("Noua parolă a fost memorată cu succes.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();

            }
        }
    }
}
