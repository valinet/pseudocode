﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ValiNet.PseudocodePluginsFramework;
using System.IO;

namespace PseudocodePentruScoliSiInstitutii
{
    public class MainClass : VPlugin
    {
        public string Name
        {
            get
            {
                return "com.valinet.pseudocode.restrictions";
            }
        }
        public string AssemblyName
        {
            get
            {
                return "com.valinet.pseudocode.restrictions";
            }
        }
        public string PseudocodeeFileIsConverted(string textToBeConverted, string convertedText, string lang)
        {
            return "";
        }
        public bool RunningPseudocodeeFile(string ConvertedCode, string lang)
        {
            return false;
        }
        public bool UserIsAboutToSignIntoDropbox()
        {
            return false;
        }
        public bool Options()
        {
            return false;
        }
        public void About()
        {
            MessageBox.Show("Extensia Pseudocode pentru Școli și Instituții 1.0.0.0\n\nDezvoltată de Centrul de R&D al ValiNet România.\nPublicată de ValiNet România.\n\nDrepturi de autor (C) 2006-2013 ValiNet România. Toate drepturile rezervate.\nVizitați www.valinet.ro pentru actualizări și informații suplimentare despre versiune.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public void ExtensionOptions()
        {
                    PasswordAsk pa = new PasswordAsk();
                    DialogResult dr = pa.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        Options op = new Options();
                        op.ShowDialog();
                    }
        }
        public string Perform(string actionID, string contextInformation)
        {
            if (actionID == "com.valinet.pseudocode.tools.options" || actionID == "com.valinet.pseudocode.tools.config")
            {
                try
                {
                    StreamReader sr = new StreamReader(Application.StartupPath + "\\encryptedOptions.pass");
                    string st = sr.ReadToEnd();
                    sr.Close();
                    string[] spl = st.Split('\n');
                    //MessageBox.Show(spl[1]);
                    if (Convert.ToInt32(spl[1]) == 1)
                    {
                        PasswordAsk pa = new PasswordAsk();
                        DialogResult dr = pa.ShowDialog();
                        if (dr == DialogResult.OK)
                        {
                            return "false";
                        }
                        else
                        {
                            return "true";
                        }
                    }
                    else return "false";
                }
                catch
                {
                    MessageBox.Show("Extensia Pseudocode pentru Școli și Instituții este pregătită pentru a fi utilizată. Mergeți la Unelte, alegeți Extensii și Actualizări, apoi faceți click pe extensia com.valinet.pseudocode.restrictions și apăsați Configurare pachet pentru a configura opțiunile acesteia.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return "false";
                }
            }
            if (actionID == "com.valinet.pseudocode.tools.sign-in")
            {
                try
                {
                    StreamReader sr = new StreamReader(Application.StartupPath + "\\encryptedOptions.pass");
                    string st = sr.ReadToEnd();
                    sr.Close();
                    string[] spl = st.Split('\n');
                    if (Convert.ToInt32(spl[0]) == 1)
                    {
                        PasswordAsk pa = new PasswordAsk();
                        DialogResult dr = pa.ShowDialog();
                        if (dr == DialogResult.OK)
                        {
                            return "false";
                        }
                        else
                        {
                            return "true";
                        }
                    }
                    else return "false";
                }
                catch
                {
                    return "false";
                }
            }
            if (actionID == "com.valinet.pseudocode.open-code-tag" || actionID == "com.valinet.pseudocode.share-code-tag")
            {
                try
                {
                    StreamReader sr = new StreamReader(Application.StartupPath + "\\encryptedOptions.pass");
                    string st = sr.ReadToEnd();
                    sr.Close();
                    string[] spl = st.Split('\n');
                    if (Convert.ToInt32(spl[2]) == 1)
                    {
                        PasswordAsk pa = new PasswordAsk();
                        DialogResult dr = pa.ShowDialog();
                        if (dr == DialogResult.OK)
                        {
                            return "false";
                        }
                        else
                        {
                            return "true";
                        }
                    }
                    else return "false";
                }
                catch
                {
                    return "false";
                }
            }
            return contextInformation;
        }
    }
}
