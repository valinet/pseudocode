﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PseudocodePentruScoliSiInstitutii
{
    public partial class Options : Form
    {
        public Options()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter(Application.StartupPath + "\\encryptedOptions.pass");
            sw.WriteLine(comboBox1.SelectedIndex.ToString());
            sw.WriteLine(comboBox2.SelectedIndex.ToString());
            sw.WriteLine(comboBox3.SelectedIndex.ToString());
            sw.Close();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Extensia Pseudocode pentru Școli și Instituții 1.0.0.0\n\nDezvoltată de Centrul de R&D al ValiNet România.\nPublicată de ValiNet România.\n\nDrepturi de autor (C) 2006-2013 ValiNet România. Toate drepturile rezervate.\nVizitați www.valinet.ro pentru actualizări și informații suplimentare despre versiune.", "Pseudocode", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Options_Load(object sender, EventArgs e)
        {
            try
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(Application.StartupPath + "\\encryptedOptions.pass");
                string st = sr.ReadToEnd();
                sr.Close();
                string[] spl = st.Split('\n');
                if (Convert.ToInt32(spl[0]) == 0)
                {
                    comboBox1.SelectedIndex = 0;
                }
                else
                {
                    comboBox1.SelectedIndex = 1;
                }
                if (Convert.ToInt32(spl[1]) == 0)
                {
                    comboBox2.SelectedIndex = 0;
                }
                else
                {
                    comboBox2.SelectedIndex = 1;
                }
                if (Convert.ToInt32(spl[2]) == 0)
                {
                    comboBox3.SelectedIndex = 0;
                }
                else
                {
                    comboBox3.SelectedIndex = 1;
                }
            }
            catch
            {
                comboBox1.SelectedIndex = 0;
                comboBox2.SelectedIndex = 0;
                comboBox3.SelectedIndex = 0;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            NewPassword nep = new NewPassword();
            nep.ShowDialog();
        }
    }
}
